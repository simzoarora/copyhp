<?php

namespace App\Http\Requests\Frontend\User;

use App\Http\Requests\Request;

/**
 * Class ChangePasswordRequest
 * @package App\Http\Requests\Frontend\Access
 */
class ChangePasswordRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return access()->user()->canChangePassword();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'old_password' => 'required',
            'password' => 'required|min:8|confirmed'
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'old_password.required' => trans('validation.attributes.frontend.auth.old_password'),
            'password.required' => trans('validation.attributes.frontend.auth.password_required'),
            'password.confirmed' => trans('validation.attributes.frontend.auth.password_confirmed')
        ];
    }

}
