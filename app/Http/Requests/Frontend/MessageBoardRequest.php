<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;
use App\Models\Pool;
use Auth;

/**
 * Class PoolRequest
 * @package App\Http\Requests\Frontend\User
 */
class MessageBoardRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Pool::active()->where('id', $this->pool_id)->has('poolTeam')->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $validation_array = [
            'title' => 'required',
            'description' => 'required',
        ];

        return $validation_array;
    }

}
