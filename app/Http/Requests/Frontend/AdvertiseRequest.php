<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;

class AdvertiseRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'information' => 'required',
            'g-recaptcha-response' => 'required|captcha',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'name.required' => trans('pages.advertise.validations.name'),
            'email.required' => trans('pages.advertise.validations.email'),
            'information.required' => trans('pages.advertise.validations.information'),
            'g-recaptcha-response.required' => trans('pages.advertise.validations.captcha')
        ];
    }

}
