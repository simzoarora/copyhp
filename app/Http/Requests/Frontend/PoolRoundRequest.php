<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;
use App\Models\Pool;
use Auth;

/**
 * Class PoolTeamRequest
 * @package App\Http\Requests\Frontend\User
 */
class PoolRoundRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Pool::where('id', $this->pool_id)->where('user_id', Auth::id())->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'pool_id' => 'required',
            'round.*.name' => 'required',
            'round.*.players' => 'required',
            'pool_team_data.*.pool_team_players.*.player_id' => 'required',
            'pool_team_data.*.pool_team_players.*.round_id' => 'required',
        ];
    }
    
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'pool_team_data.*.pool_team_players.*.player_id.required' => trans('messages.pool.select_player_from_dropdown'),
        ];
    }

}
