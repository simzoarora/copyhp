<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;
use App\Models\Pool;
use Auth;

/**
 * Class PoolTeamRequest
 * @package App\Http\Requests\Frontend\User
 */
class PoolRosterRequest extends Request {

    public function __construct() {
        $this->validator = app('validator');
        $this->validateSum($this->validator);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Pool::where('id', $this->pool_id)->where('user_id', Auth::id())->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'player_position_id.*' => 'required',
            'roster.*.value' => 'integer|min:0|max:30',
            'total_team_size' => 'integer|min:0|max:30|validate_sum',
        ];
    }

    public function messages() {
        return [
            'roster.*.value.min' => 'Minimum Value should be Zero',
            'roster.*.value.max' => 'Maximum Value should be 30',
            'total_team_size.validate_sum' => 'Actual Sum Does not Match with total team size provided',
        ];
    }

    public function validateSum($validator) {
        $validator->extend('validate_sum', function($attribute, $value, $parameters) {
            $actual_sum = array_sum(request()->input('roster.*.value'));
            return $actual_sum == $value;
        });
    }

}
