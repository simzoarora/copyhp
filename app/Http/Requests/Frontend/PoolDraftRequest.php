<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;
use App\Models\Pool;
use Auth;

/**
 * Class PoolTeamRequest
 * @package App\Http\Requests\Frontend\User
 */
class PoolDraftRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Pool::where('id', $this->pool_id)->where('user_id', Auth::id())->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
        ];
    }

}
