<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;

class SupportRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'type' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'question' => 'required',
            'g-recaptcha-response' => 'required|captcha',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'type.required' => trans('pages.contact.validations.type'),
            'name.required' => trans('pages.contact.validations.name'),
            'email.required' => trans('pages.contact.validations.email'),
            'question.required' => trans('pages.contact.validations.question'),
            'g-recaptcha-response.required' => trans('pages.contact.validations.captcha')
        ];
    }

}
