<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;
use Auth;

/**
 * Class PoolRequest
 * @package App\Http\Requests\Frontend\User
 */
class MessageBoardReplyRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
//        if (isset($this->id)) {
//            return Pool::where('id', $this->id)->where('user_id', Auth::id())->exists();
//        } else {
//            return true;
//        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        $validation_array = [
            'message_board_id' => 'required',
            'reply' => 'required',
        ];

        return $validation_array;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
//    public function messages() {
//        return [
////            'name.required' => 'Pool name is required.',
////            'logo.name.required' => 'The logo is required.',
//        ];
//    }

}
