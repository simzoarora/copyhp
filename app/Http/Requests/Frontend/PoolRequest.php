<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;
use App\Models\Pool;
use App\Models\Season;
use Auth;
use Config;
use Carbon\Carbon;

/**
 * Class PoolRequest
 * @package App\Http\Requests\Frontend\User
 */
class PoolRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (isset($this->id)) {
            return Pool::where('id', $this->id)->where('user_id', Auth::id())->exists();
        } else {
            return true;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $validation_array = [
            'pool_type' => 'required',
            'name' => 'required',
        ];

        if (isset($this->live_draft) && $this->live_draft == 1) {
            $validation_array += [
                'live_draft_time_zone' => 'required',
            ];

            if (isset($this->live_draft_date) && isset($this->live_draft_time_zone) && $this->live_draft_date == Carbon::now($this->live_draft_time_zone)->toDateString()) {
                $validation_array += [ 'live_draft_time' => 'required|date_format:H:i:s|after:' . Carbon::now($this->live_draft_time_zone)->toTimeString()];
            } else {
                $season_end_date = Season::where('current', 1)->first(['end_date']);
                $timezone_name = config('app.timezone');

                if (isset($this->live_draft_time_zone)) {
                    $timezone_name = $this->live_draft_time_zone;
                }
                $validation_array += [
                    'live_draft_date' => 'bail|required|after:' . Carbon::now($timezone_name)->toDateString() . '|before:' . Carbon::parse($season_end_date->end_date)->addDay()->timezone(config('app.timezone')),
                    'live_draft_time' => 'bail|required|date_format:H:i:s'];
            }
        }



        return $validation_array;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $timezone_name = config('app.timezone');
        if (isset($this->live_draft_time_zone)) {
            $timezone_name = $this->live_draft_time_zone;
        }
        return [
            'name.required' => 'Pool name is required.',
            'live_draft_time.after' => 'The live draft time must be after ' . Carbon::now($timezone_name)->toTimeString(),
        ];
    }

    public function all()
    {
        $data = parent::all();
        if (empty($data['live_draft_time_zone'])) {
            unset($data['live_draft_time_zone']);
        }
        if (empty($data['live_draft_time'])) {
            unset($data['live_draft_time']);
        }
        if (isset($data['live_draft_date'])) {
            $data['live_draft_date'] = Carbon::parse($data['live_draft_date'])->toDateString();
        }
        if (isset($data['live_draft_time'])) {
            $data['live_draft_time'] = Carbon::parse($data['live_draft_time'])->toTimeString();
        }
        return $data;
    }

}
