<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class MessageRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:60',
            'message' => 'required|min:10|max:80',
            'link' => 'url',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => trans('messages.backend.messages.titleRequired'),
            'title.min' => trans('messages.backend.messages.titleMinLimit'),
            'title.max' => trans('messages.backend.messages.titleMAxLimit'),
            'message.required' => trans('messages.backend.messages.messageRequired'),
            'message.min' => trans('messages.backend.messages.messageMinLimit'),
            'message.max' => trans('messages.backend.messages.messageMaxLimit'),
        ];
    }

}
