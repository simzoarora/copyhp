<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

/**
 * Class BlogRequest
 */
class BlogRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title' => 'required|max:75',
//            'slug' => 'alpha_dash|max:100',
            'body' => 'required',
            'thumbnail_image' => 'required',
            'featured_image' => 'required'
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'title.required' => trans('blog.blog.validations.title'),
            'body.required' => trans('blog.blog.validations.body'),
            'thumbnail_image.required' => trans('blog.blog.validations.thumbnail_image'),
            'featured_image.required' => trans('blog.blog.validations.featured_image'),
        ];
    }

}
