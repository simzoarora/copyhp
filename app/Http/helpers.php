<?php

use Carbon\Carbon;

function timezones()
{
    return [
        'America/New_York' => 'Eastern',
        'America/Chicago' => 'Central',
        'America/Denver' => 'Mountain',
        'America/Phoenix' => 'Mountain no DST',
        'America/Los_Angeles' => 'Pacific',
        'America/Anchorage' => 'Alaska',
        'America/Adak' => 'Hawaii',
        'Pacific/Honolulu' => 'Hawaii no DST',
    ];
}

function liveDraftTimeIntervals()
{
    $live_draft_intervals = [];
    $live_draft_intervals[''] = 'Please Select Time';
    $pool_draft_start_time = Carbon::parse('00:00');

    for ($x = 1; $x <= 48; $x++) {
        $live_draft_intervals[$pool_draft_start_time->toTimeString()] = $pool_draft_start_time->format('h:i A');
        $pool_draft_start_time = $pool_draft_start_time->addMinutes(30);
    }
    return $live_draft_intervals;
}
