<?php


Route::get('blog', 'BlogController@index')->name('frontend.blog.index');
Route::get('blog_widget', 'BlogController@blogsWidget')->name('frontend.blog.widget');
Route::get('blog/{id}', 'BlogController@blogPost')->name('frontend.blog.post');
Route::post('blogCommentsSubmit', 'BlogController@blogCommentsSubmit')->name('frontend.blog.commentsSubmit');