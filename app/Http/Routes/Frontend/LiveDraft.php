<?php
Route::patch('live-draft/queue/changeSortOrder', 'LiveDraftQueuesController@changeSortOrder')->name('frontend.live_draft.queue.changeSortOrder');

Route::resource('live-draft/queue', 'LiveDraftQueuesController', [
    'only' => [
        'index', 'store', 'destroy'
    ],
    'names' => [
        'index' => 'frontend.live_draft.queue.index',
        'store' => 'frontend.live_draft.queue.store',
        'destroy' => 'frontend.live_draft.queue.destroy',
    ]
]);

Route::get('live-draft/{pool_id}/getTeamsTabData', 'LiveDraftsController@getTeamsTabData')->name('frontend.live_draft.getTeamsTabData');
Route::get('live-draft/{pool_id}/getDraftResultsTabData', 'LiveDraftsController@getDraftResultsTabData')->name('frontend.live_draft.getDraftResultsTabData');
Route::get('live-draft/{pool_id}/getMyTeam', 'LiveDraftsController@getMyTeam')->name('frontend.live_draft.getMyTeam');
Route::post('live-draft/{pool_id}/draftPlayer', 'LiveDraftsController@draftPlayer')->name('frontend.live_draft.draftPlayer');
Route::get('live-draft/{pool_id}/getDraftTiming', 'LiveDraftsController@getDraftTiming')->name('frontend.live_draft.getDraftTiming');
Route::post('live-draft/{pool_id}/changeAutomaticSelection', 'LiveDraftsController@changeAutomaticSelection')->name('frontend.live_draft.changeAutomaticSelection');
