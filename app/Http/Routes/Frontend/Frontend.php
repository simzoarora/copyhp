<?php

/**
 * Frontend Controllers
 */
Route::get('/landing', 'LandingController@index')->name('landing');
Route::post('/mailchimpsubscribe', 'LandingController@mailchimpSubscribe')->name('mailchimpsubscribe');
Route::get('/', 'HomeController@index')->name('frontend.home.index');
Route::get('/scores', 'ScoresController@index')->name('frontend.scores.index');
Route::get('/scores/{competition_id}', 'ScoresController@gameDetails')->name('frontend.scores.details');
Route::get('/getCompetitionScore/{competition_id}', 'ScoresController@getCompetitionScore')->name('frontend.scores.gameScores');
Route::get('/getDayScore/{date}', 'ScoresController@getDayScores')->name('frontend.scores.dayScores');
Route::post('/twitterCommentsSubmit', 'HomeController@twitterCommentsSubmit')->name('frontend.twitter.commentsSubmit');
Route::get('/pool_creator', 'HomeController@poolCreator')->name('poolCreator');
Route::get('/my-pools', 'HomeController@index')->name('frontend.home.myPool');
/**
 * These frontend controllers require the user to be logged in
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User'], function() {
//        Route::get('dashboard', 'DashboardController@index')->name('frontend.user.dashboard');
//        Route::get('profile/edit', 'ProfileController@edit')->name('frontend.user.profile.edit');
//        Route::patch('profile/update', 'ProfileController@update')->name('frontend.user.profile.update');
    });
});

Route::post('/saveLiveDraftMessage', 'LiveDraftsController@saveLiveDraftMessages')->name('frontend.live_draft.saveChatMessages');
Route::get('/getLiveDraftMessages', 'LiveDraftsController@getLiveDraftMessages')->name('frontend.live_draft.getChatMessages');
