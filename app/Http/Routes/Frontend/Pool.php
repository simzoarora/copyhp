<?php

Route::get('matchup/getDataForMatchupIndividual/{matchup_id}', 'PoolsController@getDataForMatchupIndividual')->name('frontend.pool.getDataForMatchupIndividual');
Route::get('matchup/matchup-individual/{matchup_id}', 'PoolsController@matchupIndividual')->name('frontend.pool.matchupIndividual');
Route::get('pool/{pool_id}/matchups', 'PoolsController@matchups')->name('frontend.pool.matchups');

Route::get('pool/getMyPoolData', 'PoolsController@getMyPoolData')->name('frontend.pool.getMyPoolData');

Route::get('pool/getDataForSidebarPoolTeams/{pool_id}', 'PoolsController@getDataForSidebarPoolTeams')->name('frontend.pool.getDataForSidebarPoolTeams');

Route::get('pool/{pool_id}/player-stats', 'PoolsController@playerStats')->name('frontend.pool.playerStats');
Route::get('pool/{pool_id}/getDataForPlayerStats', 'PoolsController@getDataForPlayerStats')->name('frontend.pool.getDataForPlayerStats');

Route::get('pool/{pool_id}/league-leader', 'PoolsController@leagueLeader')->name('frontend.pool.leagueLeader');
Route::get('pool/{pool_id}/getDataForLeagueLeader', 'PoolsController@getDataForLeagueLeader')->name('frontend.pool.getDataForLeagueLeader');

Route::get('pool/{pool_id}/draft-result', 'PoolsController@draftResult')->name('frontend.pool.draftResult');
Route::get('pool/{pool_id}/getDataForDraftResult', 'PoolsController@getDataForDraftResult')->name('frontend.pool.getDataForDraftResult');

Route::get('pool/{pool_id}/standings', 'PoolsController@standings')->name('frontend.pool.standings');
Route::get('pool/{pool_id}/getDataForStandings', 'PoolsController@getDataForStandings')->name('frontend.pool.getDataForStandings');

Route::get('pool/{pool_id}/my-team', 'PoolsController@myTeam')->name('frontend.pool.myTeam');
Route::get('pool/{pool_id}/getDataForMyTeam', 'PoolsController@getDataForMyTeam')->name('frontend.pool.getDataForMyTeam');
Route::get('pool/{pool_id}/my-team/team/{team_id}', 'PoolsController@myTeam')->name('frontend.pool.myTeamWithTeam');
Route::get('pool/{pool_id}/getDataForMyTeam/team/{team_id}', 'PoolsController@getDataForMyTeam')->name('frontend.pool.getDataForMyTeamWithTeam');

Route::get('pool/{pool_id}/live-stats', 'PoolsController@liveStats')->name('frontend.pool.liveStats');
Route::get('pool/{pool_id}/getDataForLiveStats', 'PoolsController@getDataForLiveStats')->name('frontend.pool.getDataForLiveStats');

Route::get('pool/{pool_id}/in-action', 'PoolsController@inActionTonight')->name('frontend.pool.inActionTonight');
Route::get('pool/{pool_id}/getDataForInActionTonight', 'PoolsController@getDataForInActionTonight')->name('frontend.pool.getDataForInActionTonight');

Route::get('pool/{pool_id}/getDataForDashboard', 'PoolsController@getDataForDashboard')->name('frontend.pool.getPoolDataForDashboard');
Route::get('pool/{pool_id}/dashboard', 'PoolsController@dashboard')->name('frontend.pool.dashboard');

Route::post('pool/addBoxPlayers', 'PoolsController@addBoxPlayers')->name('frontend.pool.addBoxPlayers');
Route::post('pool/addRoundPlayers', 'PoolsController@addRoundPlayers')->name('frontend.pool.addRoundPlayers');
Route::post('pool/addDraft', 'PoolsController@addDraft')->name('frontend.pool.addDraft');
Route::post('pool/addRosters', 'PoolsController@addRosters')->name('frontend.pool.addRosters');
Route::post('pool/addTeams', 'PoolsController@addTeams')->name('frontend.pool.addTeams');
Route::post('pool/addPoolScoring', 'PoolsController@addPoolScoring')->name('frontend.pool.addPoolScoring');

Route::post('pool/{pool_id}/changeTeamLineup', 'PoolsController@changeTeamLineup')->name('frontend.pool.changeTeamLineup');
//Route::get('pool/changeTeamLineup/{pool_id}', 'PoolsController@changeTeamLineup')->name('frontend.pool.changeTeamLineup');

Route::get('pool/{pool_id}/getPoolHeader', 'PoolsController@getPoolHeader')->name('frontend.pool.getPoolHeader');
Route::get('pool/{pool_id}/getPoolHeader/team/{pool_team_id?}', 'PoolsController@getPoolHeader')->name('frontend.pool.getPoolHeaderWithTeam');

Route::get('pool/getUserPools', 'PoolsController@getUserPools')->name('frontend.pool.getUserPools');


Route::get('pool/{pool_id}/getPoolStatForPlayers', 'PoolsController@getPoolStatForPlayers')->name('frontend.pool.getPoolStatForPlayers');

Route::get('pool/{pool_id}/drop-setup', 'PoolsController@dropSetup')->name('frontend.pool.dropSetup');
Route::post('pool/confirmDropPlayers', 'PoolTeamsController@confirmDropPlayers')->name('frontend.pool.confirmDropPlayers');

Route::get('pool/{pool_id}/add-setup', 'PoolsController@addSetup')->name('frontend.pool.addSetup');
Route::post('pool/confirmAddPlayers', 'PoolTeamsController@confirmAddPlayers')->name('frontend.pool.confirmAddPlayers');

Route::get('pool/{pool_id}/trade/{trade_id}', 'PoolTeamsController@trade')->name('frontend.pool_teams.trade');
Route::get('pool/{pool_id}/trade-details/{trade_id}', 'PoolTeamsController@tradeDetails')->name('frontend.pool_teams.tradeDetails');
Route::post('pool/{pool_id}/changeTradeStatus/{trade_id}', 'PoolTeamsController@changeTradeStatus')->name('frontend.pool_teams.changeTradeStatus');
Route::get('pool/{pool_id}/trade', 'PoolsController@trade')->name('frontend.pool.trade');
Route::post('pool/createTrade', 'PoolTeamsController@createTrade')->name('frontend.pool.createTrade');

Route::get('pool/{pool_id}/transaction', 'PoolsController@transaction')->name('frontend.pool.transaction');
Route::get('pool/{pool_id}/getTransactionsData', 'PoolsController@getTransactionsData')->name('frontend.pool.getTransactionsData');

Route::get('pool/{pool_id}/payment/{payment_item_id}', 'PaymentsController@index')->name('frontend.pool.payment');

Route::get('pool/{pool_id}/getPoolTeamSeasonStat', 'PoolsController@getPoolTeamSeasonStat')->name('frontend.pool.getPoolTeamSeasonStat');
Route::get('pool/{pool_id}/getPoolTeamSeasonStat/team/{team_id}', 'PoolsController@getPoolTeamSeasonStat')->name('frontend.pool.getPoolTeamSeasonStatWithTeam');

Route::get('pool/{pool_id}/message-board/{message_board_id?}', 'MessageBoardsController@index')->name('frontend.pool.message_board.index');

Route::get('pool/{pool_id}/box-draft', 'PoolsController@boxDraft')->name('frontend.pool.boxDraft');
Route::get('pool/{pool_id}/getDataForBoxDraft', 'PoolsController@getDataForBoxDraft')->name('frontend.pool.getDataForBoxDraft');
Route::post('pool/{pool_id}/saveBoxPoolTeamPlayers', 'PoolsController@saveBoxPoolTeamPlayers')->name('frontend.pool.saveBoxPoolTeamPlayers');


Route::get('pool/{pool_id}/team/edit', 'PoolTeamsController@edit')->name('frontend.pool.team.edit');
Route::post('pool/{pool_id}/team/{team_id}/update', 'PoolTeamsController@update')->name('frontend.pool.team.update');

Route::get('pool/{pool_id}/getAllTrades', 'PoolTeamsController@getAllTrades')->name('frontend.pool.getAllTrades');

Route::get('pool/{pool_id}/getmatchupHeaderData', 'PoolsController@getmatchupHeaderData')->name('frontend.pool.getmatchupHeaderData');

Route::get('pool/{pool_id}/live-draft', 'LiveDraftsController@index')->name('frontend.pool.liveDraft');
Route::get('pool/{pool_id}/pre-draft', 'LiveDraftsController@preDraft')->name('frontend.pool.preDraft');

Route::post('pool/addUniqueRoundPlayers', 'PoolsController@addUniqueRoundPlayers')->name('frontend.pool.addUniqueRoundPlayers');

Route::resource('pool', 'PoolsController');
