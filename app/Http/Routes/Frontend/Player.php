<?php

Route::get('player/search', 'PlayersController@search')->name('frontend.player.search');
Route::get('player/stat/{id}', 'PlayersController@stat')->name('frontend.player.stat');
Route::get('player/getDataForSidebarInjuryReport/{pool_id?}', 'PlayersController@getDataForSidebarInjuryReport')->name('frontend.player.getDataForSidebarInjuryReport');
