<?php

Route::get('/draft-kit', 'PaymentsController@draftKit')->name('frontend.payment.draft_kit');
Route::post('payment/webhook', 'PaymentsController@webhook')->name('frontend.payment.webhook');
Route::post('payment/draft-kit-webhook', 'PaymentsController@draftKitWebhook')->name('frontend.payment.draft_kit_webhook');

