<?php

Route::group(['middleware' => 'user'], function () {
    Route::get('/notification/getUserNotifications', 'NotificationsController@getUserNotifications')->name('frontend.notifications.getUserNotifications');
    Route::get('/notification/markRead/{id}', 'NotificationsController@markRead')->name('frontend.notifications.markRead');
});

