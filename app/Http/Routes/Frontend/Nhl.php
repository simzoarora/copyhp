<?php

Route::get('/schedule/{schedule_start_date?}', 'NhlController@schedule')->name('frontend.nhl.schedule');
Route::get('/{team_nick_name}/schedule/{schedule_start_date?}', 'NhlController@schedule')->name('frontend.nhl.schedule_team');
Route::get('/schedule2/{schedule_start_date?}', 'NhlController@schedule2')->name('frontend.nhl.schedule2'); //delete this after development
Route::get('/{team_nick_name}/schedule2/{schedule_start_date?}', 'NhlController@schedule2')->name('frontend.nhl.schedule_team2'); //delete this after development
Route::get('/getScheduleData/{schedule_start_date?}', 'NhlController@getScheduleData')->name('frontend.nhl.getScheduleData');
Route::get('/{team_nick_name}/getScheduleData/{schedule_start_date?}', 'NhlController@getScheduleData')->name('frontend.nhl.getScheduleData_team');
Route::get('/standings/{season_name?}', 'NhlController@standings')->name('frontend.nhl.standings');
Route::get('/getStandingData/{season_name?}', 'NhlController@getStandingData')->name('frontend.nhl.getStandingData');
Route::get('/injury-report', 'NhlController@injuryReport')->name('frontend.nhl.injuryReport');
Route::get('/injuryReport2', 'NhlController@injuryReport2')->name('frontend.nhl.injuryReport2'); //delete this after development
Route::get('/getDataForInjuryReport', 'NhlController@getDataForInjuryReport')->name('frontend.nhl.getDataForInjuryReport');
Route::get('/player-stats', 'NhlController@playerStats')->name('frontend.nhl.playerStats');
Route::get('/getDataForPlayerStats', 'NhlController@getDataForPlayerStats')->name('frontend.nhl.getDataForPlayerStats');

