<?php

Route::get('message-board/getAllMessageBoards/{pool_id}', 'MessageBoardsController@getAllMessageBoards')->name('frontend.message_board.getAllMessageBoards');
Route::post('message-board/store', 'MessageBoardsController@store')->name('frontend.message_board.store');
Route::post('message-board/reply', 'MessageBoardsController@reply')->name('frontend.message_board.reply');
Route::get('message-board/saveMessageReaction', 'MessageBoardsController@saveMessageReaction')->name('frontend.message_board.saveMessageReaction');
Route::get('message-board/getDataForMessageBoard/{id}', 'MessageBoardsController@getDataForMessageBoard')->name('frontend.message_board.getDataForMessageBoard');
Route::post('message-board/delete', 'MessageBoardsController@delete')->name('frontend.message_board.delete');

