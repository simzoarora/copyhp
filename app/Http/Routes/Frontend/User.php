<?php

Route::group(['namespace' => 'User', 'middleware' => 'user'], function () {
    Route::post('user/updateEmailNotificationSetting', 'ProfileController@updateEmailNotificationSetting')->name('frontend.user.updateEmailNotificationSetting');
});

