<?php

/** 
 * Cron Controllers Routes   
 */
Route::get('/scoreboarddata', 'AtomfeedController@index')->name('cron.atomfeed.index');
Route::get('/firstscoreboarddata', 'AtomfeedController@firstScoreboardData')->name('cron.atomfeed.firstScoreboardData');
Route::get('/twitterfeed', 'TwitterController@index')->name('cron.twitter.index');
Route::get('/firsttwitterfeed', 'TwitterController@firstTwitterFeed')->name('cron.twitter.firstTwitterFeed');
Route::get('/live-draft-queue', 'LiveDraftsController@processPoolForLiveDraft');
 