<?php

Route::get('/preranking','PrerankingController@index')->name('admin.preranking.index');
Route::get('/preranking/exportRankings/{season_id}','PrerankingController@exportRankings')->name('admin.preranking.exportRankings');
Route::post('/preranking/importRankings','PrerankingController@importRankings')->name('admin.preranking.importRankings');