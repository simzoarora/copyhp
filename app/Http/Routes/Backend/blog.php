<?php

Route::get('/blog','BlogController@index')->name('admin.blog');
Route::get('/blog/create', 'BlogController@create')->name('admin.view_blog_add');
Route::post('/blog/create', 'BlogController@store')->name('admin.save_blog_add');
Route::get('/blog/{id}/edit', 'BlogController@edit')->name('admin.view_edit');
Route::post('/blog/{id}/edit', 'BlogController@update')->name('admin.update_edit');
Route::post('/blog/delete', 'BlogController@delete')->name('admin.delete_blog');
Route::post('/blog/checkslug', 'BlogController@checkSlugAvailability')->name('admin.check_slug');