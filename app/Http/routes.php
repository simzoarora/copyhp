<?php
Route::group(['middleware' => 'web'], function() {
    /**
     * Switch between the included languages
     */
    Route::group(['namespace' => 'Language'], function () {
        require (__DIR__ . '/Routes/Language/Language.php');
    });

    /**
     * Frontend Routes
     * Namespaces indicate folder structure
     */
    Route::group(['namespace' => 'Frontend'], function () {
        require (__DIR__ . '/Routes/Frontend/Frontend.php');
        require (__DIR__ . '/Routes/Frontend/Access.php');
        require (__DIR__ . '/Routes/Frontend/Player.php');
        require (__DIR__ . '/Routes/Frontend/Nhl.php');
        require (__DIR__ . '/Routes/Frontend/Payment.php');
        require (__DIR__ . '/Routes/Frontend/Notification.php');
        require (__DIR__ . '/Routes/Frontend/User.php');
    });

    Route::group(['namespace' => 'Cron'], function () {
        require (__DIR__ . '/Routes/Cron/index.php');
    });
});

/**
 * Backend Routes
 * Namespaces indicate folder structure
 * Admin middleware groups web, auth, and routeNeedsPermission
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'middleware' => 'admin'], function () {
    /**
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     */
    require (__DIR__ . '/Routes/Backend/Dashboard.php');
    require (__DIR__ . '/Routes/Backend/Access.php');
    require (__DIR__ . '/Routes/Backend/LogViewer.php');
    require (__DIR__ . '/Routes/Backend/blog.php');
    require (__DIR__ . '/Routes/Backend/Preranking.php');
    require (__DIR__ . '/Routes/Backend/Message.php');
});
// Below route only for development purpose.
if (\Config('app.env') == 'local') {
    Route::group(['namespace' => 'Cron'], function () {
        Route::get('pool/createSeasonWeeks', 'PoolsController@createSeasonWeeks');
        Route::get('pool/addPoolScore', 'PoolsController@addPoolScore');
        Route::get('pool/addPoolScoreForPreviousSeasons', 'PoolsController@addPoolScoreForPreviousSeasons');
        Route::get('pool/addPoolScoreForCurrentSeason', 'PoolsController@addPoolScoreForCurrentSeason');
        Route::get('pool/addPlayerRankForPreviousSeason', 'PoolsController@addPlayerRankForPreviousSeason');
        Route::get('pool/addPlayerRankForCurrentSeason', 'PoolsController@addPlayerRankForCurrentSeason');
        Route::get('pool/checkGoalieGamesAndRemoveStats', 'PoolsController@checkGoalieGamesAndRemoveStats');
        Route::get('pool/createPlayOffMatches', 'PoolsController@createPlayOffMatches');
    });
}
Route::group(['namespace' => 'Frontend', 'middleware' => 'web'], function () {
    Route::get('pool/invite/{invite_key}', 'PoolsController@invite')->name('frontend.pool.invite');
});
Route::group(['namespace' => 'Frontend', 'middleware' => 'user'], function () {
    require (__DIR__ . '/Routes/Frontend/Pool.php');
    require (__DIR__ . '/Routes/Frontend/MessageBoard.php');
    require (__DIR__ . '/Routes/Frontend/LiveDraft.php');
});
Route::group(['namespace' => 'Frontend', 'middleware' => 'web'], function () {
    require (__DIR__ . '/Routes/Frontend/Blog.php');
});

// Below route only for development purpose.
if (\Config('app.env') == 'local') {
    Route::group(['namespace' => 'Cron'], function () {
        Route::get('fetchSeasonTeams', 'IndexController@fetchSeasonTeams');
        Route::get('fetchSeasonTeamPlayers', 'IndexController@fetchSeasonTeamPlayers');
        Route::get('fetchBoxScore', 'IndexController@fetchBoxScore');
        Route::get('fetchInjuredPlayer', 'IndexController@fetchInjuredPlayer');
        Route::get('fetchDraftPlayers', 'IndexController@fetchDraftPlayers');
        Route::get('fetchSeasonSchedule', 'ScheduleController@fetchSeasonSchedule');
        Route::get('saveCompetitionTypes', 'ScheduleController@saveCompetitionTypes');
        Route::get('fetchStandings', 'StandingsController@fetchStandings');
        Route::get('getHistoricalPlayerStats', 'PlayersController@getHistoricalPlayerStats');
        Route::get('processWaiver', 'PoolTeamsController@processWaiver');
        Route::get('processTrades', 'PoolTeamsController@processTrades');
        Route::get('notification/poolteams', 'NotificationsController@poolTeams');
    });
    Route::group(['namespace' => 'Frontend', 'middleware' => 'web'], function () {
        Route::get('/transactions', 'LandingController@transactions');
        Route::get('/trades', 'LandingController@trades');
        Route::get('/poolInfo', 'LandingController@poolInfo');
        Route::get('/standings', 'LandingController@standings');
        Route::get('/messages1', 'LandingController@messages1');
        Route::get('/messages2', 'LandingController@messages2');
        Route::get('/poolStanding', 'LandingController@poolStanding');
//    Route::get('/playerStats', 'LandingController@playerStats');
        Route::get('/playerTransaction', 'LandingController@playerTransaction');
        Route::get('/history', 'LandingController@history');
        Route::get('/addSetup', 'LandingController@addSetup');
        Route::get('/addConfirmation', 'LandingController@addConfirmation');
        Route::get('/dropSetup', 'LandingController@dropSetup');
        Route::get('/dropConfirmation', 'LandingController@dropConfirmation');
        Route::get('/proposedTrade', 'LandingController@proposedTrade');
        Route::get('/payment', 'LandingController@payment');
    });
}
//Pages Controller routes
Route::group(['namespace' => 'Frontend', 'middleware' => 'web'], function () {
    Route::get('/advertise', 'PagesController@advertise')->name('frontend.pages.advertise');
    Route::post('/advertiseSubmit', 'PagesController@advertiseSubmit')->name('frontend.pages.advertiseSubmit');
    Route::get('/contact', 'PagesController@support')->name('frontend.pages.contact');
    Route::post('/supportSubmit', 'PagesController@supportSubmit')->name('frontend.pages.supportSubmit');
    Route::get('/faq', 'PagesController@faq')->name('frontend.pages.faq');
    Route::get('/pool-of-types', 'PagesController@poolTypes')->name('frontend.pages.poolTypes');
    Route::get('/how-it-works', 'PagesController@howItWorks')->name('frontend.pages.howItWorks');
    Route::get('/privacy-policy', 'PagesController@privacyPolicy')->name('frontend.pages.privacyPolicy');
    Route::get('/terms-of-use', 'PagesController@termsOfUse')->name('frontend.pages.termsOfUse');
});
