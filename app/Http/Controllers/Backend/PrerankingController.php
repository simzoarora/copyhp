<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Season;
use App\Models\PlayerPreRanking;
use App\Models\Player;
use Excel;
use Input;

/**
 * Description of PrerankingController
 *
 * @author Anik
 */
class PrerankingController extends Controller {
    /*
     * Index page:choose a season, export/import a CSV file
     */

    public function index() {
        return view('backend.preranking.index', [
            'seasons' => Season::orderBy('sort_order')->pluck('name', 'id')->toArray(),
        ]);
    }

    /*
     * Download a CSV file containing all player's current pre-season rankings
     */

    public function exportRankings(Request $request, $season_id) {
        $season = Season::where('id', $season_id);
        //validate season_id
        if (!empty($season) && $season->count()) {

            $players_by_season_id = Player::whereHas('seasons', function($query) use($season_id) {
                        $query->where('seasons.id', $season_id);
                    })
                    ->with([
                        'playerSeasons' => function($query)use($season_id) {
                            $query->where('season_id', $season_id);
                        },
                        'playerSeasons.team',
                        'playerPreRankings' => function($query) use($season_id) {
                            $query->where('player_pre_rankings.season_id', $season_id);
                        }
                    ])
                    ->get();

            if ($players_by_season_id->isEmpty()) {
                return redirect()->back()->withFlashDanger(trans('prerankings.messages.danger.no_players'));
            } else {
                $old_pre_rankings = Player::whereHas('playerPreRankings', function($query) use($season_id) {
                            $query->where('player_pre_rankings.season_id', $season_id);
                        })->with([
                            'playerSeasons' => function($query)use($season_id) {
                                $query->where('season_id', $season_id);
                            },
                            'playerSeasons.team',
                            'playerPreRankings' => function($query) use($season_id) {
                                $query->where('player_pre_rankings.season_id', $season_id);
                            }
                        ])->get();
                if ($old_pre_rankings->isEmpty()) { //no prior data
                    //first export - data with blank rankings
                    foreach ($players_by_season_id as $key => $player) {
                        $player_seasons = $player->playerSeasons;
                        if (!$player_seasons->isEmpty()) {
                            //                        dump($player_seasons);
                            $data_for_export[$key] = $this->_createExportArray($player);
                        }
                    }
                } else { 
                    //repeat export - data with DB data
                    foreach ($old_pre_rankings as $key => $ranking) {
                        $player_seasons = $ranking->playerSeasons;
                        if (!$player_seasons->isEmpty()) {
                            $data_for_export[$key] = $this->_createExportArray($ranking);
                        }
                    }
                }
                
                $pre_rankings = [
                    'content' => $data_for_export,
                    'season' => $season->first()->name
                ];
                //create excel with $data_for_export & download
                Excel::create('Pre_Rankings-' . $season->first()->name, function($excel) use($pre_rankings) {
                    $excel->sheet($pre_rankings['season'], function($sheet) use($pre_rankings) {
                        $sheet->fromArray($pre_rankings['content']);
                    });
                })->export('xls');
                return redirect()->back()->withFlashSuccess(trans('prerankings.messages.success.file_downloaded'));
            }
        } else {
            return redirect()->back()->withFlashDanger(trans('prerankings.messages.danger.invalid_season'));
        }
    }

    private function _createExportArray($player) {
        $data_for_export = [
            'player_id' => $player->id,
            'first_name' => $player->first_name,
            'last_name' => $player->last_name,
            'team_name' => $player->playerSeasons->get(0)->team->display_name,
            'rank' => (!$player->playerPreRankings->isEmpty()) ? $player->playerPreRankings->get(0)->rank : ''
        ];
        return $data_for_export;
    }

    /*
     * Uploadload a CSV file containing all player's current pre-season rankings
     * & updating the DB according to the file
     */

    public function importRankings(Request $request) {
        $season_id = $request->season_id;

        $players_by_season_id_query = Player::whereHas('seasons', function($query) use($season_id) {
                        $query->where('seasons.id', $season_id);
                    })
                    ->with([
                        'playerSeasons' => function($query)use($season_id) {
                            $query->where('season_id', $season_id);
                        },
                        'playerSeasons.team'
                    ]);
        $players_by_season_id = $players_by_season_id_query->get();        

        $database_players = $players_by_season_id_query->pluck('id')->toArray();

        if ($request->hasFile('import_file')) {
            $file_extension = $request->file('import_file')->getClientOriginalExtension();

            if (!in_array($file_extension, ['xls', 'csv', 'xlsx'])) {
                return redirect()->back()->withFlashDanger(trans('prerankings.messages.danger.unsupported_file_format'));
            }

            $path = $request->file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
                    })->get();
            $file_players = $data->pluck('player_id')->transform(function($item, $key) {
                        return (int) ($item);
                    })->toArray();

            /* validations */
            if ($players_by_season_id->count() != $data->count()) {
                return redirect()->back()->withFlashDanger(trans('prerankings.messages.danger.wrong_player_total'));
            }

            if (!empty(array_diff($database_players, $file_players))) {
                return redirect()->back()->withFlashDanger(trans('prerankings.messages.danger.wrong_player_ids'));
            }

            foreach ($data as $key => $filerow) {
                $player_pre_rankings[] = [
                    'season_id' => $season_id,
                    'player_id' => (int) $filerow->player_id,
                    'rank' => ((int) $filerow->rank)
                ];
            }

            $existing_prerankings_by_season = PlayerPreRanking::where('season_id', $season_id)->get();
            if ($existing_prerankings_by_season->isEmpty()) {
                PlayerPreRanking::insert($player_pre_rankings);
                return redirect()->back()->withFlashSuccess(trans('prerankings.messages.success.data_inserted'));
            } else {
                foreach ($player_pre_rankings as $player) {
                    PlayerPreRanking::where('player_id', $player['player_id'])
                            ->where('season_id', $player['season_id'])
                            ->update(['rank' => $player['rank']]);
                }
                return redirect()->back()->withFlashSuccess(trans('prerankings.messages.success.data_updated'));
            }
        } else {
            return redirect()->back()->withFlashDanger(trans('prerankings.messages.danger.file_not_present'));
        }
    }

}
