<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\BlogRequest;

class BlogController extends Controller {

    /**
     * Show all blog posts
     * @return type
     */
    public function index() {
        $blog_posts = Blog::getAllBlogPosts();
        return view('backend.blog.index', ['blog_posts' => $blog_posts]);
    }

    /**
     * Form : create new blog post
     * @return type
     */
    public function create() {
        return view('backend.blog.add_blog');
    }

    /**
     * Save a blog post
     */
    public function store(BlogRequest $request) {
        $data = $request->all();
        
        if (empty($data['slug'])) {
            $data['slug'] = $this->_createSlug($data['title']);
        }else{
            $data['slug'] = $this->_checkSlugDuplicacy($data['slug']);
        }

        if ($data['thumbnail_image']['name'] != "") {
            $image_name = \Uuid::generate() . $data['thumbnail_image']['name'];
            $img = explode(',', $data['thumbnail_image']['data'])[1];
            $this->_uploadBase64Image(public_path() . config('backend.blog.img_path'), $image_name, base64_decode($img));
            $data['thumbnail_image'] = $image_name;
        } else {
            unset($data['thumbnail_image']);
        }

        if ($data['featured_image']['name'] != "") {
            $image_name = \Uuid::generate() . $data['featured_image']['name'];
            $img = explode(',', $data['featured_image']['data'])[1];
            $this->_uploadBase64Image(public_path() . config('backend.blog.img_path'), $image_name, base64_decode($img));
            $data['featured_image'] = $image_name;
        } else {
            unset($data['featured_image']);
        }
        
        $save_new_blog = Blog::saveNewBlogPost($data);
        if ($save_new_blog) {
            return redirect()->route('admin.blog')
                            ->withFlashSuccess(trans('alerts.backend.blog.blog_created'));
        }
    }

    /**
     * Form : edit blog post
     */
    public function edit($blog_id) {
        $blog = Blog::findOrFail($blog_id);
        return view('backend.blog.edit_blog', ['blog' => $blog]);
    }

    /**
     * Update blog post
     */
    public function update(BlogRequest $request) {
        $data = $request->all();
        
        if (!empty($data['thumbnail_image']) && $data['thumbnail_image']['name'] != "") {
            $image_name = \Uuid::generate() . $data['thumbnail_image']['name'];
            $img = explode(',', $data['thumbnail_image']['data'])[1];
            $this->_uploadBase64Image(public_path() . config('backend.blog.img_path'), $image_name, base64_decode($img));
            $data['thumbnail_image'] = $image_name;
        } else {
            $data['thumbnail_image'] = $data['thumbnail_image_old'];
        }
        
        if (!empty($data['featured_image']) && $data['featured_image']['name'] != "") {
            $image_name = \Uuid::generate() . $data['featured_image']['name'];
            $img = explode(',', $data['featured_image']['data'])[1];
            $this->_uploadBase64Image(public_path() . config('backend.blog.img_path'), $image_name, base64_decode($img));
            $data['featured_image'] = $image_name;
        } else {
            $data['featured_image'] = $data['featured_image_old'];
        }

        $data['status'] = (!isset($data['status'])) ? \Config::get('constant.blog.status.draft') : \Config::get('constant.blog.status.published');
        $blog = Blog::findOrFail($request->id);
        $blog_update = $blog->update($data);
        if ($blog_update) {
            return redirect()->route('admin.blog')
                            ->withFlashSuccess(trans('alerts.backend.blog.blog_updated'));
        }
    }

    /**
     * Delete a blog post
     * @return type
     */
    public function delete(Request $request) {
        $blog_delete = Blog::findOrFail($request->id)->delete();
        if ($blog_delete) {
            return redirect()->route('admin.blog')
                            ->withFlashSuccess(trans('alerts.backend.blog.blog_deleted'));
        }
    }

    /**
     * Checks whether the slug entered by user is available or not
     */
    public function checkSlugAvailability(Request $request) {
        $slug_present = Blog::checkSlug($request['slug']);
        $message = ($slug_present) ? 'Sorry. This slug is taken.' : 'This slug is available.';
        $status = ($slug_present) ? 0 : 1;
        return response()->json(array('message' => $message, 'status' => $status), 200);
    }

    private function _uploadBase64Image($path, $file, $imgdata) {
        $handle = fopen($path . DS . $file, 'w');
        fwrite($handle, $imgdata);
        fclose($handle);
        return TRUE;
    }

    private function _createSlug($title) {
        //Lower case everything
        $title = strtolower($title);
        //Make alphanumeric (removes all other characters)
        $title = preg_replace("/[^a-z0-9_\s-]/", "", $title);
        //Clean up multiple dashes or whitespaces
        $title = preg_replace("/[\s-]+/", " ", $title);
        //Convert whitespaces and underscore to dash
        $title = preg_replace("/[\s_]/", "-", $title);
        
        $title = $this->_checkSlugDuplicacy($title);
        return $title;
    }
    
    private function _checkSlugDuplicacy($slug){
        $count = Blog::where('slug',$slug)->count();
        if($count){
            return $slug.'-'.($count+1);
        }else{
            return $slug;
        }
    }

}
