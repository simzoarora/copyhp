<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\NotificationUser;

/**
 * Description of NotificationController
 *
 * @author Anik
 */
class NotificationsController extends Controller {

    public function getUserNotifications() {
        return access()->user()->unReadNotifications()
                        ->orderBy('created_at', 'desc')
                        ->with([
                            'pool' => function($query) {
                                $query->select('id', 'logo');
                            }
                        ])
                        ->get();
    }

    public function markRead($id) {
        $notification_user = NotificationUser::where('user_id', \Auth::id())->where('notification_id', $id)->firstOrFail();
        $notification_user->read_at = date('Y-m-d H:i:s');
        if ($notification_user->save()) {
            return response()->json(array('message' => trans('messages.success.data_saved')), 200);
        } else {
            return response()->json(array('message' => trans('messages.error.cant_save')), 500);
        }
    }

}
