<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Competition;

/**
 * Description of ScoresController
 *
 * @author Anik
 */
class ScoresController extends Controller {

    public function index() {
        $competition_dates = Competition::where('match_start_date', '<=', date('Y-m-d H:i:s'))->orderBy('match_start_date', 'desc')->limit(1)->lists('match_start_date');
        return view('frontend.scores.index', ['competition_dates' => $competition_dates]);
    }

    public function gameDetails($competition_id) {
     $competition = Competition::where('id',$competition_id)->with(['homeTeam','awayTeam'])->first();
     $title = "Hockey Pool - {$competition->awayTeam->display_name} @ {$competition->homeTeam->display_name} - {$competition->formatted_match_date}";
        return view('frontend.scores.game_details')->withCompetitionId($competition_id)->withTitle($title);
    }

    /**
     * TODO : add security features for csrf and only ajax
     * @param type $competition_id
     * @return type
     */
    public function getCompetitionScore($competition_id) {
        return Competition::getCompetitionFullStats($competition_id);
    }

    /**
     * TODO : add security features for csrf and only ajax
     * @param type $date
     * @return type
     */
    public function getDayScores($date) {
        return Competition::getCompetitionDayScore($date);
    }

}
