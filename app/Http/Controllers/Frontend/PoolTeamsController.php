<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\Models\PlayerSeason;
use App\Models\PoolTeamPlayer;
use App\Models\PoolTeam;
use App\Models\PoolTrade;
use App\Models\Pool;
use App\Models\PoolTradeVoting;
use App\Models\Notification;
use Illuminate\Http\Request;
use Carbon\Carbon;

/**
 * Description of PoolsController
 *
 * @author Anik
 */
class PoolTeamsController extends Controller {

    public function confirmDropPlayers(Request $request)
    {
//        DB::enableQueryLog();
        $data = $request->all();
        PoolTeam::where('id', $data['pool_team_id'])
                ->where('user_id', Auth::id())
                ->whereHas('pool', function($query) {
                    $query->where('pool_type', '<>', config('pool.inverse_type.box'));
                })
                ->firstOrFail(); // used just to check if box pool's player is not getting dropped
        DB::beginTransaction();
        list($status, $message) = PoolTrade::processDropPlayer($data['pool_team_id'], $data['pool_team_players_id']);
        if ($status) {
            DB::commit();
            return response()->json(array('message' => $message), 200);
        } else {
            DB::rollBack();
            return response()->json(array('message' => $message), 500);
        }
//        dd(DB::getQueryLog());
    }

    public function confirmAddPlayers(Request $request)
    {
        $data = $request->all();
//        $data = [
//            'pool_team_id' => 191,
//            'add_player_season_id' => [
//                18379,
//            ],
//            'drop_pool_team_players_id' => [
//                288
//            ],
//        ];
        DB::beginTransaction();

        $pool_team = PoolTeam::where('id', $data['pool_team_id'])
                        ->where('user_id', Auth::id())
                        ->whereHas('pool', function($query) {
                            $query->where('pool_type', '<>', config('pool.inverse_type.box')); // box pool cant add or drop players
                        })
                        ->with([
                            'totalAcquisition',
                            'totalAcquisitionCurrentWeek',
                            'pool',
                            'pool.totalPoolRoster',
                            'pool.season',
                            'pool.poolSetting.poolsetting',
                        ])->firstOrFail();


        list($status, $message) = $pool_team->checkAcquisitionAvailable($pool_team->pool);
        if (!$status) {
            return response()->json(array('message' => $message), 500);
        }

        $waiver_players = PlayerSeason::whereIn('id', $data['add_player_season_id'])
                ->whereHas('poolWaiver', function($query) use($pool_team) {
                    $query->where('is_completed', '=', 0)
                    ->where('pool_id', $pool_team->pool->id)
                    ->withFutureData();
                })
                ->with([
                    'poolWaiver' => function($query) use($pool_team) {
                        $query->where('is_completed', '=', 0)
                        ->where('pool_id', $pool_team->pool->id)
                        ->withFutureData();
                    }
                ])
                ->get();

        $waiver_player_count = $waiver_players->count();
        $deleted_at = NULL;
        if ($waiver_player_count == 1) {
            if (count($data['add_player_season_id']) > 1) {
                return response()->json(array('message' => trans('messages.pool.no_free_agent_with_waiver')), 500);
            }
            $deleted_at = $waiver_players->first()->poolWaiver->waiver_till;
            if (count($data['drop_pool_team_players_id']) > 0) {
                foreach ($data['drop_pool_team_players_id'] as $drop_player_id) {
                    $waiver_players->first()->poolWaiver->poolWaiverDeletes()->firstOrCreate([
                        'pool_team_player_id' => $drop_player_id
                    ]);
                }
            }
        } elseif ($waiver_player_count > 1) {
            return response()->json(array('message' => trans('messages.pool.only_one_waiver_player_in_transaction')), 500);
        }
        list($status, $message) = PoolTrade::processDropPlayer($data['pool_team_id'], $data['drop_pool_team_players_id'], $deleted_at);
        if ($status) {
            list($status, $message) = PoolTrade::processAddPlayer($data['pool_team_id'], $data['add_player_season_id'], $pool_team);
            if ($status) {
                DB::commit();
                return response()->json(array('message' => $message), 200);
            } else {
                DB::rollBack();
                return response()->json(array('message' => $message), 500);
            }
        } else {
            DB::rollBack();
            return response()->json(array('message' => $message), 500);
        }
    }

    public function createTrade(Request $request)
    {
        $data = $request->all();
//        $data = [
//            'parent_pool_team_id' => 190,
//            'trading_pool_team_id' => 191,
//            'parent_pool_team_players_id' => [
//                295
//            ],
//            'trading_pool_team_players_id' => [
//                306
//            ],
////            'drop_pool_team_players_id' => [
////                287
////            ],
//        ];
//        
        // remove empty values
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $value = array_filter($value);
                if (empty($value)) {
                    unset($data[$key]);
                } else {
                    $data[$key] = $value;
                }
            }
        }

        // checking if user is trying to drop same player which it is trading
        if (isset($data['drop_pool_team_players_id']) && array_intersect($data['drop_pool_team_players_id'], $data['parent_pool_team_players_id'])) {
            return response()->json(array('message' => trans('messages.pool.cant_drop_trade_same_player')), 500);
        }

        DB::beginTransaction();
        $parent_team = PoolTeam::where('id', $data['parent_pool_team_id'])
                        ->where('user_id', Auth::id())
                        ->whereHas('pool', function($query) {
                            $query->where('pool_type', '<>', config('pool.inverse_type.box'));
                        })
                        ->with('pool.poolSetting.poolsetting')->firstOrFail();
        $trading_team = PoolTeam::where('id', $data['trading_pool_team_id'])->where('pool_id', $parent_team->pool->id)->firstOrFail();

        list($status, $message) = $parent_team->checkTradeAvailable($parent_team->pool->poolSetting->poolsetting->max_trades_season);
        if (!$status) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $parent_team->pool->id])->withFlashDanger($message);
        }

        if ($parent_team->pool->poolSetting->poolsetting->trade_end_date->lte(Carbon::now())) {
            // Trade End Date setting check
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $parent_team->pool->id])->withFlashDanger(trans('messages.pool.trade_ended', ['date' => $parent_team->pool->poolSetting->poolsetting->trade_end_date->toDateString()]));
        }

        // checking both team roster condition after trade
        list($roster_status, $roster_message) = $this->_checkTradingRoster($data, $parent_team, $trading_team);
        if (!$roster_status) {
            return response()->json(array('message' => $roster_message), 500);
        }

        $pool_trade = PoolTrade::create([
                    'parent_pool_team_id' => $parent_team->id,
                    'trading_pool_team_id' => $trading_team->id,
                    'deadline' => date('Y-m-d H:i:s', strtotime('+' . $parent_team->pool->poolSetting->poolsetting->trade_reject_time . ' days')),
        ]);
        list($status, $message) = $this->_savePoolTradePlayers($pool_trade, $data['parent_pool_team_players_id'], $parent_team->id, 0);
        if ($status) {
            list($status, $message) = $this->_savePoolTradePlayers($pool_trade, $data['trading_pool_team_players_id'], $trading_team->id, 0);
            if ($status) {
                if (isset($data['drop_pool_team_players_id'])) {
                    list($status, $message) = $this->_savePoolTradePlayers($pool_trade, $data['drop_pool_team_players_id'], $parent_team->id, 1);
                    if ($status) {
                        DB::commit();
//                        Notification::createAndBroadcast($parent_team->pool->name, trans('notifications.trade_created', ['Team' => $parent_team->name, 'Expire' => $pool_trade->deadline]), route('frontend.pool_teams.tradeDetails', [$parent_team->pool->id, $pool_trade->id]), [$trading_team->user_id], $parent_team->pool->id);
                        Notification::createAndBroadcast($parent_team->pool->name, trans('notifications.trade_created', ['Team' => $parent_team->name, 'Expire' => $pool_trade->deadline]), route('frontend.pool.transaction', [$parent_team->pool->id]), [$trading_team->user_id], $parent_team->pool->id);
                        return response()->json(array('message' => $message, 'data' => ['id' => $pool_trade->id]), 200);
                    } else {
                        DB::rollBack();
                        return response()->json(array('message' => $message), 500);
                    }
                } else {
                    DB::commit();
                    Notification::createAndBroadcast($parent_team->pool->name, trans('notifications.trade_created', ['Team' => $parent_team->name, 'Expire' => $pool_trade->deadline]), route('frontend.pool.transaction', [$parent_team->pool->id]), [$trading_team->user_id], $parent_team->pool->id);
                    return response()->json(array('message' => $message, 'data' => ['id' => $pool_trade->id]), 200);
                }
            } else {
                DB::rollBack();
                return response()->json(array('message' => $message), 500);
            }
        } else {
            DB::rollBack();
            return response()->json(array('message' => $message), 500);
        }
    }

    private function _checkTradingRoster($data, $parent_team, $trading_team)
    {
        // Check total rosters under limit for parent team
        $parent_player_count = PoolTeamPlayer::where('pool_team_id', $data['parent_pool_team_id'])
                ->withFutureData()
                ->withFutureDelete()
                ->count();
        $parent_total_future_players = ($parent_player_count + count($data['trading_pool_team_players_id']) - count($data['parent_pool_team_players_id']) - ((isset($data['drop_pool_team_players_id']) ? count($data['drop_pool_team_players_id']) : 0)) );
        if ($parent_total_future_players > $parent_team->pool->totalPoolRoster->total_roster) {
            return [FALSE, trans('messages.pool.lineup_more_than_roster')];
        }

        // Check total rosters under limit for trading team
        $trading_player_count = PoolTeamPlayer::where('pool_team_id', $data['trading_pool_team_id'])
                ->withFutureData()
                ->withFutureDelete()
                ->count();
        $trading_total_future_players = ($trading_player_count + count($data['parent_pool_team_players_id']) - count($data['trading_pool_team_players_id']) );
        if ($trading_total_future_players > $trading_team->pool->totalPoolRoster->total_roster) {
            return [FALSE, trans('messages.pool.lineup_more_than_roster')];
        }
        return [TRUE, 'DONE'];
    }

    private function _savePoolTradePlayers($pool_trade, $pool_team_players_id, $pool_team_id, $type)
    {
        $parent_team_players = PoolTeamPlayer::whereIn('id', $pool_team_players_id)
                ->whereHas('poolTeam', function($query) use($pool_team_id) {
                    $query->where('id', $pool_team_id);
                })
                ->with([
                    'poolTradePlayer' => function($query) {
                        $query->whereHas('poolTrade', function($query) {
                                    $query->where('status', config('pool.pool_trades.inverse_status.no_action'));
                                });
                    },
                    'playerSeason.player'
                ])
                ->get();
        foreach ($parent_team_players as $parent_team_player) {
            if ($parent_team_player->poolTradePlayer != NULL) {
                // player already involved in a trade
                return [FALSE, trans('messages.pool.player_involved_in_trade', ['Player' => $parent_team_player->playerSeason->player->full_name])];
            }
            $pool_trade->poolTradePlayers()->firstOrCreate([
                'pool_team_player_id' => $parent_team_player->id,
                'type' => $type
            ]);
        }
        return [TRUE, trans('messages.pool.trade_created')];
    }

    public function trade($pool_id, $trade_id)
    {
        $pool_details = Pool::getPoolDataForAddDrop($pool_id);
        return view('frontend.pool_teams.trade', ['pool_id' => $pool_id, 'trade_id' => $trade_id, 'pool_details' => $pool_details, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id')]);
    }

    public function tradeDetails($pool_id, $trade_id)
    {
//        DB::enableQueryLog();
        $pool_trade = PoolTrade::where('id', $trade_id)
                ->where(function($query) use($pool_id) {
                    $query->whereHas('parentTeam', function($query) use($pool_id) {
                        $query->where('user_id', Auth::id())
                        ->where('pool_id', $pool_id);
                    })
                    ->orWhereHas('tradingTeam', function($query) use($pool_id) {
                        $query->where('user_id', Auth::id())
                        ->where('pool_id', $pool_id);
                    });
                })
                ->where('deadline', '>', date('Y-m-d H:i:s'))
                ->where('status', config('pool.pool_trades.inverse_status.no_action'))
                ->with([
                    'parentTeam',
                    'parentTeam.poolTradePlayers'=>function($query) use ($trade_id){
                    $query->where('pool_trade_id',$trade_id);
                    },
                    'parentTeam.poolTradePlayers.poolTeamPlayer',
                    'parentTeam.poolTradePlayers.poolTeamPlayer.poolTeamLineup' => function($query) {
                        $query->current();
                    },
                    'parentTeam.poolTradePlayers.poolTeamPlayer.poolTeamLineup.playerPosition',
                    'tradingTeam',
                    'tradingTeam.poolTradePlayers'=>function($query) use($trade_id){
                    $query->where('pool_trade_id',$trade_id);
                    },
                    'tradingTeam.poolTradePlayers.poolTeamPlayer',
                    'tradingTeam.poolTradePlayers.poolTeamPlayer.poolTeamLineup' => function($query) {
                        $query->current();
                    },
                    'tradingTeam.poolTradePlayers.poolTeamPlayer.poolTeamLineup.playerPosition',
                ])
                ->firstOrFail();

        $parent_players_season_id = $pool_trade->parentTeam->poolTradePlayers->pluck('poolTeamPlayer.player_season_id')->toArray();
        $trading_players_season_id = $pool_trade->tradingTeam->poolTradePlayers->pluck('poolTeamPlayer.player_season_id')->toArray();

        $stat_data = PlayerSeason::getPoolStatForPlayers($pool_id, array_merge($parent_players_season_id, $trading_players_season_id))->keyBy('id');
        return [
            'pool_trade' => $pool_trade,
            'stat_data' => $stat_data
        ];
//        dump(DB::getQueryLog());
//        dump($stat_data->toArray());
//        dd($pool_trade->toArray());
    }

    public function changeTradeStatus(Request $request, $pool_id, $trade_id)
    {
        try {
            DB::beginTransaction();
            $pool = Pool::active()->where('id', $pool_id)->with(['poolSetting.poolsetting', 'poolTeam'])->first();

            if (!$pool) {
                abort(403);
            }
            $pool_setting = $pool->poolSetting->poolsetting->trade_management;
            
            if (($request->status == config('pool.pool_trades.inverse_status.accepted') || $request->status == config('pool.pool_trades.inverse_status.rejected')) && ($pool_setting == config('pool.pool_trade_managemnet_settings_inverse.league_votes')) && ($request->type == config('pool.pool_trade_managemnet_settings_inverse.league_votes') )) {
                if ($this->_tradeVoting($trade_id, $pool_id, $request)) {
                    DB::commit();
                    return response()->json(array('message' => trans('messages.pool.trade_vote_status_changed')), 200);
                } else {
                    return response()->json(array('message' => trans('messages.pool.already_voted')), 500);
                }
            } elseif (($request->status == config('pool.pool_trades.inverse_status.cancelled_by_commissioner')) && ($pool_setting == config('pool.pool_trade_managemnet_settings_inverse.commissioner_decides')) && ($pool->user_id == Auth::id())) {
                $pool_trade = $this->_cancelledByCommissioner($trade_id, $request);
            } else {
                $pool_trade = $this->_poolTrade($trade_id, $pool_id, $request);
                
                if (($pool_setting == config('pool.pool_trade_managemnet_settings_inverse.commissioner_decides') || $pool_setting == config('pool.pool_trade_managemnet_settings_inverse.league_votes')) && ($pool_trade->pool_trade_setting_based_status == config('pool.pool_trades.pool_trade_setting_based_status.no_action'))) {
                    $pool_trade = $this->_poolTradeStatusUpdate($request, $pool, $pool_trade, $pool_setting);
                } else {
                    $pool_trade->update([
                        'status' => $request->status,
                        'is_processed' => 1,
                        'status_change_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }

            if (($request->status == config('pool.pool_trades.inverse_status.accepted')) && ($pool_setting == config('pool.pool_trade_managemnet_settings_inverse.none') || ($pool_setting == config('pool.pool_trade_managemnet_settings_inverse.commissioner_decides') && ($pool_trade->is_processed == 1)))) {
                $pool_trade_accept = PoolTrade::poolTradeAccept($pool_trade);
                if (isset($pool_trade_accept['message'])) {
                    DB::rollBack();
                    return response()->json(array('message' => $pool_trade_accept['message']), 500);
                }
                $this->_sendTradeAcceptedNotification($pool_trade);
            } elseif ($request->status == config('pool.pool_trades.inverse_status.rejected')) {
                $this->_sendTradeRejectedNotification($pool_trade);
            } elseif ($request->status == config('pool.pool_trades.inverse_status.cancelled_by_parent')) {
                $this->_sendTradeCancelledNotification($pool_trade, 'notifications.trade_cancelled_by_parent');
            } elseif ($request->status == config('pool.pool_trades.inverse_status.cancelled_by_commissioner')) {
                $this->_sendTradeCancelledNotification($pool_trade, 'notifications.trade_cancelled_by_commissioner');
            }

            DB::commit();
            return response()->json(array('message' => trans('messages.pool.trade_status_changed')), 200);
        } catch (Illuminate\Database\Eloquent\ModelNotFoundException $ex) {
            return response()->json(array('message' => trans('messages.pool.trade_not_found')), 403);
        }
    }

    private function _poolTrade($trade_id, $pool_id, $request)
    {
        return PoolTrade::where('id', $trade_id)
                        ->where(function($query) use($pool_id, $request) {

                            if ($request->status == config('pool.pool_trades.inverse_status.accepted') || $request->status == config('pool.pool_trades.inverse_status.rejected')) {
                                $query->whereHas('tradingTeam', function($query) use($pool_id) {
                                    $query->where('user_id', Auth::id())
                                    ->where('pool_id', $pool_id);
                                });
                            } elseif ($request->status == config('pool.pool_trades.inverse_status.cancelled_by_parent')) {
                                $query->whereHas('parentTeam', function($query) use($pool_id) {
                                    $query->where('user_id', Auth::id())
                                    ->where('pool_id', $pool_id);
                                });
                            } else {
                                abort(403);
                            }
                        })
                        ->where('deadline', '>', date('Y-m-d H:i:s'))
                        ->where('status', config('pool.pool_trades.inverse_status.no_action'))
                        ->with([
                            'poolTradePlayers.poolTeamPlayer.poolTeam',
                            'parentPoolTeam',
                            'parentPoolTeam.pool',
                            'tradingPoolTeam',
                        ])
                        ->firstOrFail();
    }

    private function _tradeVoting($trade_id, $pool_id, $request)
    {
        $pool_trade = PoolTeam::queryTradeStatus($trade_id);
        if ($pool_trade) {
            $pool_team_player = PoolTeam::where('pool_id', $pool_id)
                    ->where('user_id', '!=', $pool_trade->parentPoolTeam->user_id)
                    ->where('user_id', '!=', $pool_trade->tradingPoolTeam->user_id)
                    ->where('user_id', Auth::id())
                    ->exists();
            //check if logged in user is part of that pool and not part of trading teams and yet to vote
            if (!($pool_team_player) || (PoolTradeVoting::where('user_id', Auth::id())->where('pool_trade_id', $trade_id)->exists())) {
                return FALSE;
            }
            $voting = new PoolTradeVoting;
            $voting->user_id = Auth::id();
            $voting->pool_trade_id = $trade_id;
            $voting->status = $request->status;
            $voting->save();

            return TRUE;
        }
        abort(403);
    }

    private function _cancelledByCommissioner($trade_id, $request)
    {
        $pool_trade = PoolTeam::queryTradeStatus($trade_id);
        if ($pool_trade) {
            $pool_trade->update([
                'status' => $request->status,
                'is_processed' => 1,
                'status_change_at' => date('Y-m-d H:i:s')
            ]);
            return $pool_trade;
        }
        abort(403);
    }

    private function _poolTradeStatusUpdate($request, $pool, $pool_trade, $pool_setting)
    {
        if ($request->status == config('pool.pool_trades.pool_trade_setting_based_status.accepted')) {
            if ($pool_setting == config('pool.pool_trade_managemnet_settings_inverse.commissioner_decides') && ($pool->user_id == $pool_trade->parentPoolTeam->user_id || $pool->user_id == $pool_trade->tradingPoolTeam->user_id)) {
                $pool_trade->update([
                    'status' => $request->status,
                    'is_processed' => 1,
                    'status_change_at' => date('Y-m-d H:i:s')
                ]);
            } else {
                $pool_trade->update([
                    'pool_trade_setting_based_status' => $request->status,
                ]);
            }
        } elseif ($request->status == config('pool.pool_trades.pool_trade_setting_based_status.rejected') || $request->status == config('pool.pool_trades.inverse_status.cancelled_by_parent')) {
            $pool_trade->update([
                'status' => $request->status,
                'is_processed' => 1,
                'status_change_at' => date('Y-m-d H:i:s')
            ]);
        } else {
            abort(403);
        }
        return $pool_trade;
    }

    private function _sendTradeCancelledNotification($pool_trade, $msg)
    {
        $trading_notification_title = $pool_trade->parentPoolTeam->pool->name . ' - ' . $pool_trade->tradingPoolTeam->name;
        $trading_notification_msg = trans($msg, ['Team' => $pool_trade->parentPoolTeam->name]);
        $trading_notification_link = route('frontend.pool_teams.tradeDetails', ['pool_id' => $pool_trade->parentPoolTeam->pool->id, 'trade_id' => $pool_trade->id]);
        Notification::createAndBroadcast(
                $trading_notification_title, $trading_notification_msg, $trading_notification_link, [$pool_trade->tradingPoolTeam->user_id], $pool_trade->parentPoolTeam->pool->id
        );
    }

    private function _sendTradeRejectedNotification($pool_trade)
    {
        $parent_notification_title = $pool_trade->parentPoolTeam->pool->name . ' - ' . $pool_trade->parentPoolTeam->name;
        $parent_notification_msg = trans('notifications.trade_rejected', ['Team' => $pool_trade->tradingPoolTeam->name]);
        $parent_notification_link = route('frontend.pool_teams.tradeDetails', ['pool_id' => $pool_trade->parentPoolTeam->pool->id, 'trade_id' => $pool_trade->id]);
        Notification::createAndBroadcast(
                $parent_notification_title, $parent_notification_msg, $parent_notification_link, [$pool_trade->parentPoolTeam->user_id], $pool_trade->parentPoolTeam->pool->id
        );
    }

    private function _sendTradeAcceptedNotification($pool_trade)
    {
        $parent_notification_title = $pool_trade->parentPoolTeam->pool->name . ' - ' . $pool_trade->parentPoolTeam->name;
        $parent_notification_msg = trans('notifications.trade_accepted', ['Team' => $pool_trade->tradingPoolTeam->name, 'time' => config('pool.addition_deletion_time')->toDateTimeString()]);
        $parent_notification_link = route('frontend.pool_teams.tradeDetails', ['pool_id' => $pool_trade->parentPoolTeam->pool->id, 'trade_id' => $pool_trade->id]);
        Notification::createAndBroadcast(
                $parent_notification_title, $parent_notification_msg, $parent_notification_link, [$pool_trade->parentPoolTeam->user_id], $pool_trade->parentPoolTeam->pool->id
        );

        $trading_notification_title = $pool_trade->parentPoolTeam->pool->name . ' - ' . $pool_trade->tradingPoolTeam->name;
        $trading_notification_msg = trans('notifications.trade_accepted', ['Team' => $pool_trade->parentPoolTeam->name, 'time' => config('pool.addition_deletion_time')->toDateTimeString()]);
        $trading_notification_link = route('frontend.pool_teams.tradeDetails', ['pool_id' => $pool_trade->parentPoolTeam->pool->id, 'trade_id' => $pool_trade->id]);
        Notification::createAndBroadcast(
                $trading_notification_title, $trading_notification_msg, $trading_notification_link, [$pool_trade->tradingPoolTeam->user_id], $pool_trade->parentPoolTeam->pool->id
        );
    }

    public function getAllTrades($pool_id)
    {

        $current_user_pool_team = PoolTeam::where('pool_id', $pool_id)->currentUserTeam()->firstOrFail();
        $pool = Pool::active()->where('id', $pool_id)->with('poolSetting.poolsetting')->first();
//        dd($pool);
//        DB::enableQueryLog();
        $pool_trades = PoolTrade::whereHas('parentPoolTeam', function($query) use($pool_id) {
                    $query->whereHas('pool', function($query) use($pool_id) {
                        $query->where('id', $pool_id);
                    });
                })
                ->with([
                    'poolTradePlayers.poolTeamPlayer' => function($query) {
                        $query->select('id', 'pool_team_id', 'player_season_id');
                    },
                    'poolTradePlayers.poolTeamPlayer.playerSeason' => function($query) {
                        $query->select('id', 'player_id', 'team_id', 'player_position_id');
                    },
                    'poolTradePlayers.poolTeamPlayer.playerSeason.player' => function($query) {
                        $query->select('id', 'first_name', 'last_name');
                    },
                    'poolTradePlayers.poolTeamPlayer.playerSeason.team' => function($query) {
                        $query->select('id', 'short_name');
                    },
                    'poolTradePlayers.poolTeamPlayer.playerSeason.playerPosition' => function($query) {
                        $query->select('id', 'short_name');
                    },
                    'tradeVoteFirst' => function($query) {
                        $query->select('id', 'user_id', 'pool_trade_id', 'status')
                        ->where('user_id', Auth::id())->first();
                    },
                    'parentPoolTeam' => function($query) {
                        $query->select('id', 'name', 'user_id');
                    },
                    'tradingPoolTeam' => function($query) {
                        $query->select('id', 'name', 'user_id');
                    },
                ])
//                ->whereNotExists(function($subquery) {
//                    $subquery->select(\DB::raw(1))
//                    ->from('pool_trade_votings')
//                    ->whereRaw('pool_trade_votings.pool_trade_id=pool_trades.id')
//                    ->where('user_id', Auth::id());
//                })
                ->where('is_processed', 0)
                ->where('deadline', '>', date('Y-m-d H:i:s'))
                ->get();
//        dump(DB::getQueryLog());
        $prepared_response = [
            'pending_trades' => [],
            'my_trades' => [],
            'offered_trades' => [],
        ];

        $trade_management = $pool->poolSetting->poolsetting->trade_management;
        $commissioner_decides = config('pool.pool_trade_managemnet_settings_inverse.commissioner_decides');

        foreach ($pool_trades as $key => $trade) {
            $trade->deadline = Carbon::parse($trade->deadline)->tz(config('app.timezone'))->setTimezone(config('app.timezone_for_calculation'))->toDateTimeString();
//
            if ($trade->parent_pool_team_id == $current_user_pool_team->id) {
                $prepared_response['my_trades'][$key] = $trade;
            } elseif (($trade->trading_pool_team_id == $current_user_pool_team->id) && ($trade->pool_trade_setting_based_status == config('pool.pool_trades.pool_trade_setting_based_status.no_action'))) {
                $prepared_response['offered_trades'][$key] = $trade;
                $prepared_response['offered_trades'][$key]['trade_managemnet_status'] = $trade_management;
                $prepared_response['offered_trades'][$key]['owner_id'] = $pool->user_id;
            } elseif ($trade->pool_trade_setting_based_status == config('pool.pool_trades.pool_trade_setting_based_status.accepted')) {

                if (($trade_management == $commissioner_decides && ($pool->user_id == Auth::id() || $trade->tradingPoolTeam->user_id == Auth::id())) || $trade_management == config('pool.pool_trade_managemnet_settings_inverse.league_votes')) {
                    $prepared_response['pending_trades'][$key] = $trade;
                    $prepared_response['pending_trades'][$key]['trade_managemnet_status'] = $trade_management;
                    $prepared_response['pending_trades'][$key]['owner_id'] = $pool->user_id;
                    $vote_status = null;
                    if ($trade->tradeVoteFirst) {
                        $vote_status = $trade->tradeVoteFirst->status;
                    }
                    $prepared_response['pending_trades'][$key]['vote_status'] = $vote_status;
                }
            }
        }
        return $prepared_response;
    }

    public function edit($pool_id)
    {
        $pool_team = PoolTeam::where('user_id', \Auth::id())->where('pool_id', $pool_id)->where('user_id', Auth::id())->firstOrFail();
        return view('frontend.pool_teams.edit', ['pool_team' => $pool_team, 'pool_id' => $pool_id, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id'),'pool_details'=>Pool::findOrFail($pool_id)]);
    }

    public function update(Request $request, $pool_id, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $data = $request->all();
        $pool_team = PoolTeam::where('id', $id)->where('pool_id', $pool_id)->where('user_id', Auth::id())->firstOrFail();
        if (isset($data['logo']['data']) && $data['logo']['name'] != "") {
            $logo_name = \Uuid::generate() . $data['logo']['name'];
            $img = $data['logo']['data'];
            $this->_uploadBase64Image(public_path() . config('pool.paths.team_logo'), $logo_name, base64_decode($img));
            $pool_team->logo = $logo_name;
        }
        $pool_team->name = $data['name'];
        if ($pool_team->save()) {
            return response()->json(array('message' => trans('messages.success.data_saved')), 200);
        } else {
            return response()->json(array('message' => trans('messages.error.cant_save')), 500);
        }
    }

    private function _uploadBase64Image($path, $file, $imgdata)
    {
        $handle = fopen($path . DS . $file, 'w');
        fwrite($handle, $imgdata);
        fclose($handle);
        return TRUE;
    }

}
