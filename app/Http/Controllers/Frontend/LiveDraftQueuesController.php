<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Pool;
use App\Models\LiveDraftQueue;
use Auth;
use DB;

class LiveDraftQueuesController extends Controller
{

    /**
     * Send data of all the queue player of a logged in user's pool team in the
     * pool whose ID is send as GET variable in "pool_id"
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return LiveDraftQueue::getDataForLiveDraft($request->pool_id);
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Save a new player to pool team queue.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            $pool_id = $request->pool_id;
            $pool = Pool::active()
                    ->where('id', $pool_id)
                    ->has('poolTeam')
                    ->firstOrFail();
            $queue = $pool->poolTeam->liveDraftQueues()->firstOrNew([
                'player_season_id' => $request->player_season_id
            ]);
            $queue->sort_order = $request->sort_order;
            if ($queue->save()) {
                return response()->json(array('message' => trans('messages.success.data_saved'), 'data' => ['id' => $queue->id]), 200);
            } else {
                return response()->json(array('message' => trans('messages.error.cant_save')), 500);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified queue player after checking condition if that queue
     * player belongs to the team which is trying to remove.
     *
     * @param  int  $id Queue ID
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $queue_member = LiveDraftQueue::where('id', $id)
                            ->whereHas('poolTeam', function($query) {
                                $query->where('user_id', Auth::id());
                            })->firstOrFail();
            if ($queue_member->delete()) {
                return response()->json(array('message' => trans('messages.success.data_deleted')), 200);
            } else {
                return response()->json(array('message' => trans('messages.error.cant_delete')), 500);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Function to change sort order of the queue user is managing.
     * @param Request $request
     * Sample Data -
     * "sort_array" => [
     *      $queue_id => $sort_order
     * ] 
     * @return type
     */
    public function changeSortOrder(Request $request)
    {
        if ($request->ajax() && !empty($request->sort_array)) {
            $commit = true;
            DB::beginTransaction();
            foreach ($request->sort_array as $queue_id => $sort_order) {
                try {
                    $queue_member = LiveDraftQueue::where('id', $queue_id)
                                    ->whereHas('poolTeam', function($query) {
                                        $query->where('user_id', Auth::id());
                                    })->firstOrFail();
                    $queue_member->sort_order = $sort_order;
                    if (!$queue_member->save()) {
                        $commit = FALSE;
                        break;
                    }
                } catch (Exception $ex) {
                    $this->_logWithQueueAndUser(json_encode($request->sort_array), $ex);
                    DB::rollback();
                    return response()->json(array('message' => $ex->getMessage()), 500);
                } catch (ModelNotFoundException $ex) {
                    $this->_logWithQueueAndUser(json_encode($request->sort_array), $ex);
                    DB::rollback();
                    return response()->json(array('message' => trans('messages.error.cant_save')), 500);
                }
            }
            if ($commit) {
                DB::commit();
                return response()->json(array('message' => trans('messages.success.data_saved')), 200);
            } else {
                DB::rollBack();
                return response()->json(array('message' => trans('messages.error.cant_save')), 500);
            }
        } else {
            abort(403);
        }
    }

    private function _logWithQueueAndUser($queue_data, $ex)
    {
        \Log::alert("LiveDraftQueue:changeSortOrder, queue:$queue_data, user:" . Auth::id() . "  : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
    }
}