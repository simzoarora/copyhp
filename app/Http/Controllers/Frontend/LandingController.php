<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LiveDraftMessage;
use LRedis;
use Auth;
use \Firebase\JWT\JWT;

class LandingController extends Controller
{

    public function index()
    {
        return view('frontend.landing.index');
    }

    public function mailchimpSubscribe()
    {
        if (Request::ajax()) {
            $email_data = array(
                'email' => Request::input('email')
            );

            $mailchimp_settings = array(
                'apikey' => '8776d8a8fe587c826e75e526d1f1b3d3-us2',
                'list_id' => '4735159d33',
                'account_email' => 'staff@hockeypool.com'
            );

            $mailchimp_result = $this->_ibMailchimpSubscribe($email_data, $mailchimp_settings);

            $mailchimp_result = json_decode($mailchimp_result);
            if ($mailchimp_result->status == 'subscribed') {
                return response()->json(array('message' => 'GREAT!! Thanks for subscribing.'), 201);
            } else {
                if ($mailchimp_result->title == 'Member Exists') {
                    return response()->json(array('message' => 'OOPS!! You have already subscribed.'), 500);
                } else {
                    return response()->json(array('message' => 'OOPS!! ' . $mailchimp_result->title), 500);
                }
            }
            die();
        }
    }

    public function _ibMailchimpSubscribe($data = false, $MC_VARS)
    {
        $key = explode('-', $MC_VARS['apikey']);

        $api = array
            (
            'login' => $MC_VARS['account_email'],
            'key' => $MC_VARS['apikey'],
            'url' => 'https://' . $key[1] . '.api.mailchimp.com/3.0/'
        );
        $type = 'PUT';
        $target = 'lists/' . $MC_VARS['list_id'] . '/members/' . md5($data['email']);

        $ch = curl_init($api['url'] . $target);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array
            (
            'Content-Type: application/json',
            'Authorization: ' . $api['login'] . ' ' . $api['key'],
        ));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/3.0');


        $postData = array(
            "email_address" => $data['email'],
            "status" => 'subscribed',
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}