<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use DB;
use Mail;
use Auth;
use App\Models\Pool;
use App\Models\PoolH2hSetting;
use App\Models\PoolStandardSetting;
use App\Models\PoolBoxSetting;
use App\Models\PlayerPosition;
use App\Models\PoolScoringField;
use App\Models\Season;
use App\Models\PoolTeam;
use App\Models\PoolInvitation;
use App\Models\Player;
use App\Models\Access\User\User;
use App\Models\SeasonCompetitionType;
use App\Models\Competition;
use App\Models\PlayerSeason;
use App\Models\PoolMatchup;
use App\Models\LiveDraftSetting;
use App\Models\SeasonCompetitionTypeWeek;
use App\Models\PoolTeamLineup;
use App\Models\LiveDraftPlayer;
use App\Models\PoolTeamPlayer;
use App\Http\Requests\Frontend\PoolRequest;
use App\Http\Requests\Frontend\PoolTeamRequest;
use App\Http\Requests\Frontend\PoolScoringRequest;
use App\Http\Requests\Frontend\PoolRosterRequest;
use App\Http\Requests\Frontend\PoolDraftRequest;
use App\Http\Requests\Frontend\PoolBoxRequest;
use App\Http\Requests\Frontend\PoolRoundRequest;
use App\Services\LiveDraftService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Description of PoolsController
 *
 * @author Anik
 */
class PoolsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dump(time());
        dd(microtime());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $a = Pool::first()->poolSetting()->first()->poolsetting;
        $player_positions = PlayerPosition::orderBy('sort_order')->get();
        $pool_scoring_fields = PoolScoringField::getFieldsForPoolCreation();
        return view('frontend.pool.create', ['player_positions' => $player_positions, 'pool_scoring_fields' => $pool_scoring_fields, 'season_weeks' => SeasonCompetitionTypeWeek::getSeasonTypeWeeks(), 'current_season_end_date' => Season::where('current', 1)->first(['end_date'])]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PoolRequest $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            $current_season = Season::where('current', 1)->firstOrFail();
            $data['season_id'] = $current_season->id;
            $this->validate($request, [
                'name' => 'unique:pools,name,' . ((isset($data['id'])) ? ($data['id']) : 'NULL') . ',id,season_id,' . $current_season->id,
            ]);
            return DB::transaction(function () use($data) {
                        $user = access()->user();
                        if ($data['logo']['name'] != "") {
                            $logo_name = \Uuid::generate() . $data['logo']['name'];
                            $img = explode(',', $data['logo']['data'])[1];
                            $this->_uploadBase64Image(public_path() . config('pool.paths.logo'), $logo_name, base64_decode($img));
                            $data['logo'] = $logo_name;
                        } else {
                            unset($data['logo']);
                        }
                        $pool = $this->_createDataToStorePool($data, $user);
                        $all_pool_types = \Config('pool.inverse_type');
                        if (isset($data['id'])) {
                            $final_saved = $pool->poolSetting->poolsetting->update($data);
                        } else {
                            if (isset($data['season_type']) && $data['season_type'] == config('pool.inverse_season.playoff')) {
                                $playoff_competition_type = SeasonCompetitionTypeWeek::whereHas('seasonCompetitionType', function($query) {
                                            $query->where('competition_type', config('competition.inverse_competition_type.Playoff'));
                                        })->first();
                                if ($playoff_competition_type == NULL) {
                                    $playoff_competition_type = SeasonCompetitionTypeWeek::whereHas('seasonCompetitionType', function($query) {
                                                $query->where('competition_type', config('competition.inverse_competition_type.Regular Season'));
                                            })->orderBy('start_date', 'desc')->first();
                                }
                                $data['league_start_week'] = $playoff_competition_type->id;
                            }
                            switch ($data['pool_type']):
                                case $all_pool_types['h2h']:
                                    $pool_ind_setting = PoolH2hSetting::create($data);
                                    break;
                                case $all_pool_types['box']:
                                    $pool_ind_setting = PoolBoxSetting::create($data);
                                    break;
                                case $all_pool_types['standard']:
                                    $pool_ind_setting = PoolStandardSetting::create($data);
                                    break;
                                default :
                                    return response()->json(array('message' => trans('messages.pool.no_pool_type')), 500);
                            endswitch;
                            $pool_setting = $pool->poolSetting()->firstOrNew(['pool_id' => $pool->id]);
                            $final_saved = $pool_ind_setting->poolSettings()->save($pool_setting);
                        }
                        if ($final_saved) {
                            return response()->json(array('message' => trans('messages.success.data_saved'), 'data' => ['id' => $pool->id]), 200);
                        } else {
                            return response()->json(array('message' => trans('messages.error.cant_save')), 500);
                        }
                    });
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    private function _createDataToStorePool($data, $user)
    {
        if (!isset($data['id'])) {
            $pool = $user->pools()->create($data);
            if ((isset($data['live_draft']) && $data['live_draft'] == 1)) {
                $this->_saveLiveDraftSetting($pool, $data);
            }
        } else {
            $pool = Pool::where('id', $data['id'])
                    ->where('user_id', Auth::id())
                    ->with([
                        'liveDraftSetting',
                        'liveDraftPlayer' => function($query) {
                            $query->whereNull('player_season_id');
                        }
                    ])
                    ->firstOrFail();
            if ($pool->is_completed == 0) {
                $pool->pool_type = $data['pool_type'];
                if (isset($data['live_draft']) && $data['live_draft'] == 1) {
                    $this->_saveLiveDraftSetting($pool, $data);
                } elseif ($pool->liveDraftSetting != null) {
                    $pool->liveDraftSetting->delete();
                }
            } else {
                if ($pool->liveDraftSetting != null) {
                    $live_draft_date = LiveDraftService::convertStartTimeToSystemTime($pool->liveDraftSetting);
                    $back_15_minutes_date = new \DateTime("now +15 minutes");
                }
                if (
                        (isset($live_draft_date) && $live_draft_date <= $back_15_minutes_date) || // dont allow if 15 mins left for live draft
                        ($pool->liveDraftSetting == null && isset($data['live_draft']) && $data['live_draft'] == 1) || // dont allow if pool completed and it was not on earlier
                        ($pool->liveDraftSetting != null && $pool->liveDraftPlayer == null) // dont allow if live draft already completed
                ) {
                    return response()->json(array('message' => trans('messages.live_draft.cant_enable')), 500);
                } elseif (isset($data['live_draft']) && $data['live_draft'] == 1) {
                    $this->_saveLiveDraftSetting($pool, $data, TRUE);
                }
            }
            $pool->name = $data['name'];
            if (isset($data['logo'])) {
                $pool->logo = $data['logo'];
            }
            $pool->save();
        }
        return $pool;
    }

    private function _saveLiveDraftSetting($pool, $data, $change_round_timing = FALSE)
    {
        $live_draft_setting = $pool->liveDraftSetting()->firstOrNew([]);
        $live_draft_setting->date = $data['live_draft_date'];
        $live_draft_setting->time = $data['live_draft_time'];
        $live_draft_setting->time_zone = $data['live_draft_time_zone'];
        $live_draft_setting->save();
        if ($change_round_timing === TRUE) {
            $this->_generateLiveDraftPlayers($pool, $pool->poolTeams()->orderBy('sort_order')->get());
        }
    }

    private function _uploadBase64Image($path, $file, $imgdata)
    {
        $handle = fopen($path . DS . $file, 'w');
        fwrite($handle, $imgdata);
        fclose($handle);
        return TRUE;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pool = Pool::active()
                ->where('id', $id)
                ->has('currentSeason')
                ->has('poolTeam')
                ->with([
                    'poolSetting.poolsetting',
                    'season',
                    'poolRosters.playerPosition',
                    'poolScoreSettings.poolScoringField'
                ])
                ->firstOrFail();

        $season_competition_type = SeasonCompetitionType::where('season_id', $pool->season_id)->where('competition_type', $pool->season_type)->with([
                    'seasonCompetitionTypeWeek' => function($query)use($pool) {
                        $query->where('id', $pool->poolSetting->poolsetting->league_start_week);
                    }
                ])->firstOrFail();
        return view('frontend.pool.show', ['pool' => $pool, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id'), 'pool_id' => $id, 'season_competition_type' => $season_competition_type, 'pool_details' => Pool::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pool = Pool::where('id', $id)
                ->where('user_id', \Auth::id())
                ->with([
                    'poolSetting.poolsetting',
//                    'poolTeams.poolTeamPlayers.poolRound',
                    'poolTeams.user',
//                    'poolInvitations.poolTeamPlayers.poolRound',
                    'poolRosters',
                    'poolScoreSettings',
                    'poolRounds.poolTeamPlayers.playerSeason.playerPosition' => function($query) {
                        $query->select('id', 'short_name');
                    },
                    'poolRounds.poolTeamPlayers.playerSeason.team' => function($query) {
                        $query->select('id', 'short_name');
                    },
                    'poolRounds.poolTeamPlayers.playerSeason.player',
                    'poolRounds.poolTeamPlayers.poolInvitation',
                    'poolRounds.poolTeamPlayers.poolTeam',
                    'poolBoxes.poolBoxPlayers.playerSeason.player',
                    'liveDraftSetting'
                ])
                ->firstOrFail();
        $season_competition_type = SeasonCompetitionType::where('season_id', $pool->season_id)->where('competition_type', $pool->season_type)->with([
                    'seasonCompetitionTypeWeek' => function($query)use($pool) {
                        $query->where('id', $pool->poolSetting->poolsetting->league_start_week);
                    }
                ])->first();
        if ($season_competition_type != NULL && $season_competition_type->seasonCompetitionTypeWeek == NULL) {
            $season_competition_type->load(['seasonCompetitionTypeWeek']);
        }

        $first_competition = NULL;
        if ($season_competition_type != NULL) {
            $first_competition = Competition::where(\DB::raw('DATE(match_start_date)'), $season_competition_type->seasonCompetitionTypeWeek->start_date)->firstOrFail();
        }
        $field_check_array = [
            'pool_setting' => TRUE
        ];
        if ($first_competition != NULL && $first_competition->match_start_date->lte(Carbon::now())) {
            $field_check_array['first_game'] = TRUE;
        } else {
            $field_check_array['first_game'] = FALSE;
        }

        if ($pool->poolScoreSettings->isEmpty()) {
            $field_check_array['scoring'] = FALSE;
        } else {
            $field_check_array['scoring'] = TRUE;
        }
        if (!$pool->poolTeams->where('sort_order', '<>', NULL)->isEmpty() || !$pool->poolInvitations->where('sort_order', '<>', NULL)->isEmpty()) {
            $field_check_array['draft_order'] = FALSE;
        } else {
            $field_check_array['draft_order'] = TRUE;
        }
// TRUE in $field_check_array means that step is completed
        $player_positions = PlayerPosition::orderBy('sort_order')->get();
        $pool_scoring_fields = PoolScoringField::getFieldsForPoolCreation();
        return view('frontend.pool.create', [
            'player_positions' => $player_positions,
            'pool_scoring_fields' => $pool_scoring_fields,
            'pool' => $pool,
            'title'=>'Hockey Pool -'.$pool->name,
            'field_check_array' => $field_check_array,
            'season_weeks' => SeasonCompetitionTypeWeek::getSeasonTypeWeeks(TRUE),
            'current_season_end_date' => Season::where('current', 1)->first(['end_date'])
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $pool = Pool::where('id', $id)
                    ->where('user_id', Auth::id())
                    ->where('is_completed', 0)
                    ->firstOrFail();
            if ($pool->delete()) {
                return response()->json(array('message' => trans('messages.pool.pool_deleted')), 200);
            } else {
                return response()->json(array('message' => trans('messages.pool.unable_delete_pool')), 500);
            }
        } else {
            abort(404);
        }
    }

    public function addDraft(PoolDraftRequest $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            return DB::transaction(function () use($data) {
                        $pool = Pool::where('id', $data['pool_id'])->with('poolTeams', 'liveDraftSetting', 'poolRounds')->firstOrFail();
                        if ($pool->poolTeams->where('sort_order', '<>', NULL)->isEmpty()) {
                            try {
                                foreach ($data['pool_draft'] as $draft) {
                                    if (isset($draft['pool_team_id'])) {
                                        $pool_team = PoolTeam::where('pool_id', $data['pool_id'])->where('id', $draft['pool_team_id'])->firstOrFail();
                                        $pool_team->sort_order = $draft['sort_order'];
                                        $pool_team->save();
                                    } else {
                                        abort(403, trans('pool.unauthorized_action'));
                                    }
                                }

                                $pool_teams = $pool->poolTeams()->orderBy('sort_order', 'desc')->get();
                                $waiver_priority = 1;
                                foreach ($pool_teams as $pool_team) {
                                    $pool_team->poolWaiverPriority()->create([
                                        'pool_id' => $pool->id,
                                        'priority' => $waiver_priority++
                                    ]);
                                }
                                if ($pool->liveDraftSetting != NULL) {
                                    $this->_generateLiveDraftPlayers($pool, $pool_teams->sortBy('sort_order')->values());
                                    $pool->finalizePool();
                                }
                            } catch (Exception $ex) {
                                return response()->json(array('message' => $ex->getMessage()), 500);
                            } catch (ModelNotFoundException $ex) {
                                return response()->json(array('message' => trans('messages.error.cant_save')), 500);
                            }
                            return response()->json(array('message' => trans('messages.success.data_saved')), 200);
                        } else {
                            return response()->json(array('message' => trans('messages.error.data_locked')), 403);
                        }
                    });
        } else {
            abort(403, trans('pool.unauthorized_action'));
        }
    }

    /**
     * Function to generate data for live_draft_players table.
     * It keeps track of the time at which team can draft player
     * @param type $pool
     */
    private function _generateLiveDraftPlayers($pool, $pool_teams)
    {
        $date = LiveDraftService::convertStartTimeToSystemTime($pool->liveDraftSetting()->first());
        $start_time = $date->format('Y-m-d H:i:s');

        $built_data = [];
        $next_pick = [];
        $overall_pick = [];
        $total_teams = $pool->poolTeams->count();
        foreach ($pool_teams as $key => $pool_team) {
            foreach ($pool->poolRounds as $pool_round) {
                LiveDraftPlayer::buildNextAndOverallPick($pool_team->id, $total_teams, $key, $next_pick, $overall_pick);
                $built_data[] = [
                    'round' => $pool_round->id,
                    'overall' => intval($overall_pick[$pool_team->id]),
                    'pool_team_id' => $pool_team->id,
                ];
            }
        }

        // Sorting $built_data with overall key
        $overall = array();
        foreach ($built_data as $key => $row) {
            $overall[$key] = $row['overall'];
        }
        array_multisort($overall, SORT_ASC, $built_data);

        foreach ($built_data as $live_draft_player_data) {
            $live_draft_player = LiveDraftPlayer::firstOrNew([
                        'pool_id' => $pool->id,
                        'pool_round_id' => $live_draft_player_data['round'],
                        'pool_team_id' => $live_draft_player_data['pool_team_id'],
            ]);
            $live_draft_player->start_time = $start_time;
            $live_draft_player->end_time = $start_time = date('Y-m-d H:i:s', strtotime($start_time . ' +1 minute 30 seconds'));
            $live_draft_player->save();
        }
    }

    public function addPoolScoring(PoolScoringRequest $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            return DB::transaction(function () use($data) {
                        $pool = Pool::where('id', $data['pool_id'])->with(['poolScoreSettings'])->firstOrFail();

                        /**
                         *  We have to lock saving this if first game has already started
                         */
                        $season_competition_type = SeasonCompetitionType::where('season_id', $pool->season_id)->where('competition_type', $pool->season_type)->with([
                                    'seasonCompetitionTypeWeeks' => function($query)use($pool) {
                                        $query->where('id', $pool->poolSetting->poolsetting->league_start_week);
                                    }
                                ])->first();
                        if ($season_competition_type != NULL) {
                            $first_competition = Competition::where(\DB::raw('DATE(match_start_date)'), '>=', $season_competition_type->seasonCompetitionTypeWeeks[0]->start_date)->orderBy('match_start_date')->firstOrFail();
                        }
                        if (!$pool->poolScoreSettings->isEmpty() && isset($first_competition) && $first_competition->match_start_date->lte(Carbon::now())) {
                            return response()->json(array('message' => trans('messages.error.data_locked_first_game')), 403);
                        } else {
                            try {
                                foreach ($data['score_settings'] as $score_setting) {
                                    if (isset($score_setting['check']) && $score_setting['check'] == 1) {
                                        $pool_score_setting = $pool->poolScoreSettings()->firstOrNew(['pool_scoring_field_id' => $score_setting['pool_scoring_field_id'], 'type' => $score_setting['type']]);
                                        $pool_score_setting->value = $score_setting['value'];
                                        $pool_score_setting->save();
                                    } else {
// delete setting which are unticked by user
                                        $pool->poolScoreSettings()->where('pool_scoring_field_id', $score_setting['pool_scoring_field_id'])->where('type', $score_setting['type'])->delete();
                                    }
                                }
                            } catch (Exception $ex) {
                                return response()->json(array('message' => $ex->getMessage()), 500);
                            } catch (ModelNotFoundException $ex) {
                                return response()->json(array('message' => trans('messages.error.cant_save')), 500);
                            }
                            return response()->json(array('message' => trans('messages.success.data_saved')), 200);
                        }
                    });
        } else {
            abort(403, trans('pool.unauthorized_action'));
        }
    }

    public function addRosters(PoolRosterRequest $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            return DB::transaction(function () use($data) {
                        $pool = Pool::where('id', $data['pool_id'])->with('poolTeams', 'poolInvitations')->firstOrFail();
                        if ($pool->poolTeams->where('sort_order', '<>', NULL)->isEmpty() && $pool->poolInvitations->where('sort_order', '<>', NULL)->isEmpty()) {
// proceed only if draft order is not yet set
                            try {
                                $total_roster = 0;
                                $j = 1;
                                foreach ($data['roster'] as $roster) {
                                    if(empty($roster['value'])) $roster['value']=0;
                                    $pool_roster = $pool->poolRosters()->firstOrNew([
                                        'player_position_id' => $roster['player_position_id']
                                    ]);
                                    $pool_roster->value = $roster['value'];
                                    $pool_roster->save();
                                    if ($pool->pool_type == config('pool.inverse_type.h2h') || $pool->pool_type == config('pool.inverse_type.standard')) {
                                        for ($i = 1; $i <= $roster['value']; $i++) {
                                            $pool->poolRounds()->firstOrCreate([
                                                'name' => 'Round ' . $j,
                                                'player_position_id' => $roster['player_position_id']
                                            ]);
                                            $j++;
                                        }
                                    }
                                    $total_roster += $roster['value'];
                                }
                            } catch (Exception $ex) {
                                return response()->json(array('message' => $ex->getMessage()), 500);
                            } catch (ModelNotFoundException $ex) {
                                return response()->json(array('message' => trans('messages.error.cant_save')), 500);
                            }
                            return response()->json(array('message' => trans('messages.success.data_saved'), 'rounds' => $pool->poolRounds), 200);
                        } else {
                            return response()->json(array('message' => trans('messages.error.data_locked')), 403);
                        }
                    });
        } else {
            abort(403, trans('pool.unauthorized_action'));
        }
    }

    public function addTeams(PoolTeamRequest $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            return DB::transaction(function () use($data) {
                        $pool = Pool::where('id', $data['pool_id'])->with('poolTeams', 'poolInvitations')->firstOrFail();
                        if ($pool->poolTeams->where('sort_order', '<>', NULL)->isEmpty() && $pool->poolInvitations->where('sort_order', '<>', NULL)->isEmpty()) {
// proceed only if draft order is not yet set
                            $errors = [];
                            $pool_invitation_ids = [];
                            $pool_team_ids = [];
                            if (isset($data['teams'])) {
                                foreach ($data['teams'] as $team) {
                                    $user = User::where('email', $team['email'])->first();
                                    if ($user) {
                                        $pool_team = $pool->poolTeams()->firstOrNew(['user_id' => $user->id]);
                                        $pool_team->name = $team['name'];
                                        if (!$pool_team->save()) {
                                            $errors[$team['email']] = $team['name'];
                                        } else {
                                            $pool_team_ids[] = $pool_team->id;
                                        }
                                    } else {
                                        if ($team['email'] != "" && $team['name'] != "") {
                                            $pool_team = $pool->poolTeams()->firstOrNew(['email' => $team['email']]);
                                            $pool_team->name = $team['name'];
                                            if (!$pool_team->save()) {
                                                $errors[$team['email']] = $team['name'];
                                            } else {
                                                $pool_team_ids[] = $pool_team->id;
                                            }
                                        }
                                    }
                                }
                            }
                            if (empty($errors)) {
                                return response()->json(array(
                                            'message' => trans('messages.success.data_saved'),
                                            'data' => [
                                                'pool_invitations' => $pool->poolInvitations()->whereIn('id', $pool_invitation_ids)->get(),
                                                'pool_teams' => $pool->poolTeams()->whereIn('id', $pool_team_ids)->with(['user' => function($query) {
                                                        $query->select('users.email', 'users.id');
                                                    }])->get()
                                            ]), 200);
                            } else {
                                return response()->json(array('message' => trans('messages.error.cant_save'), 'data' => $errors), 500);
                            }
                        } else {
                            return response()->json(array('message' => trans('messages.error.data_locked')), 403);
                        }
                    });
        } else {
            abort(403, trans('pool.unauthorized_action'));
        }
    }

    public function addBoxPlayers(PoolBoxRequest $request)
    {
        $data = $request->all();
        if ($request->ajax()) {
            DB::beginTransaction();
            $pool = Pool::where('id', $data['pool_id'])->with('poolBoxPlayers')->firstOrFail();
            if ($pool->poolBoxPlayers->isEmpty()) {
                try {
                    foreach ($data['box'] as $box) {
                        $pool_box = $pool->poolBoxes()->firstOrCreate(['name' => $box['name']]);
                        foreach ($box['players'] as $player_id) {
                            $player = Player::where('id', $player_id)->with('currentPlayerSeason')->firstOrFail();
                            $pool_box->poolBoxPlayers()->firstOrCreate(['player_season_id' => $player->currentPlayerSeason->id]);
                        }
                    }
                    $pool->invite_key = \Uuid::generate();
                    $pool->active_till = date('Y-m-d H:i:s', strtotime('+7 days')); // giving 7 days trial period
                    $pool->is_completed = 1;
                    $pool->save();

                    $pool->poolTeam()->firstOrCreate([
                        'user_id' => Auth::id(),
                        'name' => $pool->name . '_' . Auth::id()
                    ]);
                } catch (Exception $ex) {
                    DB::rollBack();
                    return response()->json(array('message' => $ex->getMessage()), 500);
                } catch (ModelNotFoundException $ex) {
                    DB::rollBack();
                    return response()->json(array('message' => trans('messages.error.cant_save')), 500);
                }
                DB::commit();
                return response()->json(array('message' => trans('messages.success.data_saved'), 'data' => ['invite_link' => route('frontend.pool.invite', [$pool->invite_key])]), 200);
            } else {
                return response()->json(array('message' => trans('messages.error.data_locked')), 403);
            }
        } else {
            abort(403, trans('pool.unauthorized_action'));
        }
    }

    private function _prepareSyncData($players_data, &$added_players)
    {
        dump('_________________________________-');
        dump($players_data);
        dump($added_players);
        $sync_data = [];
        foreach ($players_data as $player) {
            $player_season = PlayerSeason::where('player_id', $player['player_id'])->whereHas('season', function($query) {
                        $query->where('current', 1);
                    })->where('is_active', 1)->firstOrFail();
            dump('player seaon');
            dump($player_season);
            if (!in_array($player_season->id, $added_players)) {
                $sync_data[$player_season->id] = [
                    'pool_round_id' => $player['round_id']
                ];
                $added_players[] = $player_season->id;
            }
        }
        return $sync_data;
    }

    public function addRoundPlayers(PoolRoundRequest $request)
    {

        if ($request->ajax()) {

            $data = $request->all();
            DB::beginTransaction();

            $pool = Pool::where('id', $data['pool_id'])->with([
                        'poolTeamPlayers',
                        'poolTeams.user',
                        'poolSetting.poolsetting',
                        'season',
                        'poolRosters',
                        'liveDraftSetting'
                    ])->firstOrFail();

            try {
                $pool_teams = PoolTeam::where('pool_id', $data['pool_id'])->with('poolTeamPlayers.playerSeason')->get();

                foreach ($pool_teams as $pool_team) {
                    $team_roster_track = $this->_createTeamRosterTrack($pool->poolRosters);
                    foreach ($pool_team->poolTeamPlayers as $pool_team_player) {
                        PoolTeamLineup::savePlayerLineup($pool_team_player, $pool->season->end_date, $team_roster_track);
                    }
                }

                $pool->finalizePool();
            } catch (Exception $ex) {
                DB::rollBack();
                return response()->json(array('message' => $ex->getMessage()), 500);
            } catch (ModelNotFoundException $ex) {
                DB::rollBack();
                return response()->json(array('message' => trans('messages.error.cant_save')), 500);
            }
            DB::commit();
            return response()->json(array('message' => trans('messages.success.data_saved')), 200);
        } else {
            abort(403, trans('pool.unauthorized_action'));
        }
    }

    public function addUniqueRoundPlayers(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            DB::beginTransaction();

            $pool = Pool::where('id', $data['pool_id'])->with([
                        'poolTeamPlayers',
                        'liveDraftSetting'
                    ])->firstOrFail();

            if ($pool->liveDraftSetting != NULL) {
                return response()->json(array('message' => trans('messages.pool.cant_draft_live_draft_player')), 500);
            }

            try {
                $player_season = PlayerSeason::where('player_id', $data['player_id'])->whereHas('season', function($query) {
                            $query->where('current', 1);
                        })->where('is_active', 1)->firstOrFail();

                PoolTeam::where('pool_id', $data['pool_id'])->where('id', $data['pool_team_id'])->firstOrFail();

                $poolTeamPlayerExists = PoolTeamPlayer::where('pool_round_id', $data['round_id'])
                        ->where('pool_team_id', $data['pool_team_id'])
                        ->first();


                if ($poolTeamPlayerExists) {
                    $poolTeamPlayerExists->delete();
                }

                $poolTeamPlayer = new PoolTeamPlayer();
                $poolTeamPlayer->pool_team_id = $data['pool_team_id'];
                $poolTeamPlayer->pool_round_id = $data['round_id'];
                $poolTeamPlayer->player_season_id = $player_season->id;

                if ($poolTeamPlayer->save()) {

                    DB::commit();
                    return response()->json(array('message' => trans('messages.success.draft_auto_saved')), 200);
                } else {
                    DB::rollBack();
                    return response()->json(array('message' => trans('messages.error.cant_save')), 500);
                }
            } catch (Exception $ex) {
                DB::rollBack();
                return response()->json(array('message' => $ex->getMessage()), 500);
            } catch (ModelNotFoundException $ex) {
                DB::rollBack();
                return response()->json(array('message' => trans('messages.error.cant_save')), 500);
            }
        } else {
            abort(403, trans('pool.unauthorized_action'));
        }
    }

    private function _createTeamRosterTrack($pool_rosters)
    {
        $team_roster_track = [];
        foreach ($pool_rosters as $pool_roster) {
            if ($pool_roster->value > 0) {
                $team_roster_track[$pool_roster->player_position_id] = [
                    'total' => $pool_roster->value,
                    'used' => 0,
                ];
            }
        }
        return $team_roster_track;
    }

    public function invite($invite_key)
    {
        $pool = Pool::active()->where('invite_key', $invite_key)->where('pool_type', config('pool.inverse_type.box'))->firstOrFail();
        if (Auth::check()) {
            $pool_team = $pool->poolTeams()->firstOrNew(['user_id' => Auth::id()]);
            $pool_team->name = $pool->name . "_" . Auth::id();

            if ($pool_team->save()) {
                return redirect()->route('frontend.home.index')->withFlashSuccess(trans('messages.pool.added_as_team'));
            } else {
                return redirect()->route('frontend.home.index')->withFlashDanger(trans('messages.pool.unable_to_add_to_team'));
            }
        } else {
            session(['pool_team_invite' => $invite_key]);
            return redirect()->route('auth.login')->withFlashInfo(trans('messages.pool.register_login_for_team'));
        }
    }

    private function _checkPoolActive($pool_id)
    {
        return Pool::where('id', $pool_id)->active()->exists();
    }

    public function dashboard($id)
    {
        $pool = Pool::findOrFail($id);
        return view('frontend.pool.pool_overview', ['title'=>'Hockey Pool -'.$pool->name, 'pool_id' => $id, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id'), 'pool_details' => $pool]);
    }

    public function getDataForDashboard(Request $request, $id)
    {
        $pool = Pool::getPoolDetailsForDashboard($id, $request->season_competition_week_id);
        $pool->poolTeams->transform(function ($item, $key) use($pool) {
            return $item->addTotalScore($pool, $item);
        });
        $pool_teams = $pool->poolTeams->sortByDesc('total_score');
        $pool_array = $pool->toArray();
        $pool_array['pool_teams'] = array_values($pool_teams->toArray());
        return $pool_array;
    }

    public function inActionTonight($id)
    {
        if (!$this->_checkPoolActive($id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $id]);
        }
        $pool = Pool::findOrFail($id);
        return view('frontend.pool.in_action_tonight', ['title'=>'Hockey Pool -'.$pool->name, 'pool_id' => $id, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id'), 'pool_details' => $pool]);
    }

    public function getDataForInActionTonight($id)
    {
        $pool = Pool::getDataForInAction($id);
        $pool_teams = array_values($pool->poolTeams->sortByDesc(function($query) {
                    return $query->poolTeamPlayers->count();
                })->transform(function ($item, $key)use($pool) {
                    $item->total_player = $item->poolTeamPlayers->count();
                    return $item;
                })->toArray());
        $pool_array = $pool->toArray();
        $pool_array['pool_teams'] = $pool_teams;
        return $pool_array;
    }

    public function liveStats($id)
    {
        if (!$this->_checkPoolActive($id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $id]);
        }
        return view('frontend.pool.live_stats', ['pool_id' => $id, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id'), 'pool_details' => Pool::findOrFail($id)]);
    }

    public function getDataForLiveStats($id)
    {
        return Pool::getDataForLiveStats($id);
    }

    public function myTeam($id, $pool_team_id = NULL)
    {
        if (!$this->_checkPoolActive($id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $id]);
        }
        $pool_details = Pool::active()->with('poolSetting.poolsetting', 'poolTeam')->has('poolTeam')->where('id', $id)->firstOrFail();
        return view('frontend.pool.my_team', [
            'pool_id' => $id,
            'pool_team_id' => $pool_team_id,
            'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id'),
            'season' => Season::current()->first(),
            'pool_details' => $pool_details
        ]);
    }

    public function getDataForMyTeam(Request $request, $id, $pool_team_id = NULL)
    {
        if (!isset($request->start_date) || !isset($request->end_date) || !isset($request->lineup_date)) {
            abort(500, 'Development Send dates');
        }
        return Pool::getDataForMyTeam($id, $request->start_date, $request->end_date, $request->lineup_date, $pool_team_id);
    }

    public function standings($id)
    {
        if (!$this->_checkPoolActive($id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $id]);
        }
        return view('frontend.pool.standings', ['pool_id' => $id, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id'), 'pool_details' => Pool::findOrFail($id)]);
    }

    public function getDataForStandings($id)
    {
        $pool = Pool::active()->where('id', $id)
                ->has('poolTeam')
                ->with(['poolSetting.poolsetting'])
                ->firstOrFail();

        if ($pool->pool_type == config('pool.inverse_type.h2h')) {
            $pool->load([
                'poolTeams.poolMatchupResult'
            ]);
            $pool_teams = $pool->poolTeams->sortByDesc('poolMatchupResult.total_score')->values()->toArray();
        } else {
            $pool->load([
                'poolTeams.standardScore'
            ]);
            $pool_teams = $pool->poolTeams->sortByDesc('standardScore.total_score')->values()->toArray();
        }

        $final_data = $pool->toArray();
        $final_data['pool_teams'] = $pool_teams;
        return $final_data;
    }

    /**
     * Function to get sort function name of collection for league leader data.
     * This is used to sort data based on user selection if user selected
     * or a default function.
     * @param type $request
     * @return string
     */
    private function getSortFunctionNameForLeagueLeader($request)
    {
        if (!isset($request->order_type)) {
            $sort_function_name = 'sortByDesc';
        } else {
            if ($request->order_type == 'asc') {
                $sort_function_name = 'sortBy';
            } else {
                $sort_function_name = 'sortByDesc';
            }
        }
        return $sort_function_name;
    }

    public function leagueLeader($id)
    {
        if (!$this->_checkPoolActive($id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $id]);
        }
        $pool = Pool::getPoolDataForLeagueLeaders($id, 0); //0 in second param so that we get all the fields
        $positions = PlayerPosition::lists('short_name', 'id');
        return view('frontend.pool.league_leader', [
            'pool_id' => $id,
            'pool' => $pool,
            'positions' => $positions->toArray(),
            'seasons' => Season::orderBy('name', 'desc')->lists('name', 'id')->toArray(),
            'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id'),
            'pool_details' => $pool
        ]);
    }

    public function getDataForLeagueLeader(Request $request, $id)
    {
        if (!isset($request->page)) {
            $request->page = 0;
        }
        $pool_scoring_field_id = $request->pool_scoring_field_id;
        $field_name = NULL;
        if ($pool_scoring_field_id > 0) {
            // 0 is used for total points. if its not 0 then we will map field for finding data from table
            $field_name = config('pool.pool_scoring_field.fields_map_to_stat_table')[$pool_scoring_field_id];
        }
        $pool = Pool::getPoolDataForLeagueLeaders($id, $pool_scoring_field_id);
        $pool_team_ids = $pool->poolTeams->transform(function($item, $key) {
                    return $item['id'];
                })->toArray();
        $season_id = $pool->season_id;
        if (isset($request->season_id) && $request->season_id != "") {
            $season_id = $request->season_id;
        }
        $field_scoring = [];
        $fields_to_find = [
            'goalie' => [],
            'skater' => [],
            'extra' => [],
        ];
        $all_pool_scoring_fields = config('pool.pool_scoring_field.inverse_fields');
        $skater_fields = \Schema::getColumnListing('competition_skater_stats');
        $goalie_fields = \Schema::getColumnListing('competition_goalie_stats');
        foreach ($pool->poolScoreSettings as $score_setting) {
            switch ($score_setting['pool_scoring_field_id']) {
                case $all_pool_scoring_fields['save_percentage']:
                    $fields_to_find['goalie'][] = '(sum(shots_against) - sum(goals_against)) / sum(shots_against) as save_percentage';
                    break;
                case $all_pool_scoring_fields['goals_against_avg']:
                    $fields_to_find['extra'][] = '(sum(goals_against) * 3600) / sum(time_on_ice_secs) as goals_against_average';
                    break;
                default:
                    $field_name_local = config('pool.pool_scoring_field.fields_map_to_stat_table')[$score_setting['pool_scoring_field_id']];
                    if (in_array($field_name_local, $skater_fields) === TRUE) {
                        $field_type = 'skater';
                    } else {
                        $field_type = 'goalie';
                    }
                    $fields_to_find[$field_type][] = 'sum(' . $field_name_local . ') as ' . $field_name_local;
                    break;
            }
            $field_scoring[config('pool.pool_scoring_field.fields_map_to_stat_table')[$score_setting['pool_scoring_field_id']]] = $score_setting->toArray();
        }
        $player_season_query_builder = PlayerSeason::getQueryBuilderForLeagueLeaders($pool, $request, $fields_to_find, $pool_team_ids, $season_id, $field_scoring);

        $player_season = $player_season_query_builder->paginate(config('pool.paginations.league_leaders.per_page_limit'))->toArray();
        /**
         *  Here we are making similar data structure in both the cases 
         * 1. When user searched for season
         * 2. When user seatched for a date
         * 
         * Here Case 2 is handled
         */
        if (!isset($request->data_retrieval) || $request->data_retrieval == '') {
            foreach ($player_season['data'] as $key => $player_data) {
                foreach ($field_scoring as $field => $score_setting) {
                    if ($score_setting['pool_scoring_field_id'] == $all_pool_scoring_fields['goals_against_avg']) {
                        if ($player_data['competition_skater_stat']['time_on_ice_secs'] != 0) {
                            $local_value = ($player_data['competition_goalie_stat']['goals_against'] * 3600) / $player_data['competition_skater_stat']['time_on_ice_secs'];
                        } else {
                            $local_value = NULL;
                        }
                    } elseif ($player_data['competition_skater_stat'] !== NULL) {
                        $local_value = (array_key_exists($field, $player_data['competition_skater_stat']) === TRUE) ? $player_data['competition_skater_stat'][$field] : $player_data['competition_goalie_stat'][$field];
                    } else {
                        $local_value = NULL;
                    }
                    $player_season['data'][$key]['player']['player_stat'][$score_setting['pool_scoring_field_id']] = $local_value;
                }
                $player_season['data'][$key]['total_points'] = $player_data['player']['calculated_pool_point']['total_points'];
            }
        } else {
            foreach ($player_season['data'] as $key => $player_data) {
                foreach ($field_scoring as $field => $score_setting) {
                    $player_season['data'][$key]['player']['player_stat'][$score_setting['pool_scoring_field_id']] = $player_data['player']['player_stat'][$field];
                }
                $player_season['data'][$key]['total_points'] = $player_data['player']['season_pool_point']['total_points'];
            }
        }
        $player_season_data = collect($player_season['data']);
        $player_season['data'] = $player_season_data->toArray();
        $player_season['pool'] = $pool;
        return $player_season;
    }

    private function _addMissingAttributeForPlayerStats(&$request)
    {
        if (!isset($request->start_date)) {
            $request->start_date = date('Y-m-d');
        }
        if (!isset($request->end_date)) {
            $request->end_date = date('Y-m-d');
        }
        if (!isset($request->order_type)) {
            $request->order_type = 'asc';
        }
    }

    public function playerStats($id)
    {
        if (!$this->_checkPoolActive($id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $id]);
        }
        $pool_details = Pool::active()->with('poolTeams', 'poolScoreSettings.poolScoringField', 'poolTeam.poolMatchupResult')->has('poolTeam')->where('id', $id)->firstOrFail();
        $pool_teams = [];
        foreach ($pool_details->poolTeams as $pool_team) {
            $pool_teams[$pool_team->id] = $pool_team->name;
        }
        return view('frontend.pool.player_stats', [
            'pool_id' => $id,
            'positions' => PlayerPosition::getDataForSelectBox(),
            'pool_details' => $pool_details,
            'pool_teams' => $pool_teams,
            'seasons' => Season::orderBy('name', 'desc')->lists('name', 'id')->toArray(),
            'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id')
        ]);
    }

    public function getDataForPlayerStats(Request $request, $id)
    {
        $pool = Pool::active()->where('id', $id)->has('poolTeam')->with([ 'poolScoreSettings', 'poolTeams', 'poolSetting.poolsetting'])->firstOrFail();
        $total_pool = Pool::active()->where('season_id', $pool->season_id)->count();
        $fields_to_find = [
            'goalie' => [],
            'skater' => [],
            'extra' => [],
        ];
        $field_scoring = [];
        $all_pool_scoring_fields = config('pool.pool_scoring_field.inverse_fields');
        $skater_fields = \Schema::getColumnListing('competition_skater_stats');
        $goalie_fields = \Schema::getColumnListing('competition_goalie_stats');
        foreach ($pool->poolScoreSettings as $score_setting) {
            switch ($score_setting['pool_scoring_field_id']) {
                case $all_pool_scoring_fields['save_percentage']:
                    $fields_to_find['goalie'][] = '(sum(shots_against) - sum(goals_against)) / sum(shots_against) as save_percentage';
                    break;
                case $all_pool_scoring_fields['goals_against_avg']:
                    $fields_to_find['extra'][] = '(sum(goals_against) * 3600) / sum(time_on_ice_secs) as goals_against_average';
                    break;
                default:
                    $field_name_local = config('pool.pool_scoring_field.fields_map_to_stat_table')[$score_setting['pool_scoring_field_id']];
                    if (in_array($field_name_local, $skater_fields) === TRUE) {
                        $field_type = 'skater';
                    } else {
                        $field_type = 'goalie';
                    }
                    $fields_to_find[$field_type][] = 'sum(' . $field_name_local . ') as ' . $field_name_local;
                    break;
            }
            $field_scoring[config('pool.pool_scoring_field.fields_map_to_stat_table')[$score_setting['pool_scoring_field_id']]] = $score_setting->toArray();
        }
        $pool_team_ids = $pool->poolTeams->transform(function($item, $key) {
                    return $item['id'];
                })->toArray();
        $season_id = $pool->season_id;
        if (isset($request->season_id) && $request->season_id != "") {
            $season_id = $request->season_id;
        }
        $this->_addMissingAttributeForPlayerStats($request);

        $player_season_query_builder = PlayerSeason::getQueryBuilderForPlayerStats($season_id, $request, $pool_team_ids, $fields_to_find, $field_scoring, $pool);

        $player_season = $player_season_query_builder->paginate((isset($request->paginate) && $request->paginate != '') ? $request->paginate : config('pool.paginations.player_stats.per_page_limit'))->toArray();

        /**
         *  Here we are making similar data structure in both the cases 
         * 1. When user searched for season
         * 2. When user seatched for a date
         * 
         * Here Case 2 is handled
         */
        if (!isset($request->data_retrieval) || $request->data_retrieval == '') {
            foreach ($player_season['data'] as $key => $player_data) {
                foreach ($field_scoring as $field => $score_setting) {
                    if ($score_setting['pool_scoring_field_id'] == $all_pool_scoring_fields['goals_against_avg']) {
                        if ($player_data['competition_skater_stat']['time_on_ice_secs'] != 0) {
                            $local_value = ($player_data['competition_goalie_stat']['goals_against'] * 3600) / $player_data['competition_skater_stat']['time_on_ice_secs'];
                        } else {
                            $local_value = NULL;
                        }
                    } elseif (isset($player_data['competition_skater_stat']) && $player_data['competition_skater_stat'] !== NULL) {
                        $local_value = (array_key_exists($field, $player_data['competition_skater_stat']) === TRUE) ? $player_data['competition_skater_stat'][$field] : $player_data['competition_goalie_stat'][$field];
                    } else {
                        $local_value = NULL;
                    }
                    $player_season['data'][$key]['player']['player_stat'][$score_setting['pool_scoring_field_id']] = $local_value;
                }
                $player_season['data'][$key]['total_points'] = (isset($player_data['player']['calculated_pool_point']['total_points']))?$player_data['player']['calculated_pool_point']['total_points']:0;
            }
        } else {
            foreach ($player_season['data'] as $key => $player_data) {
                foreach ($field_scoring as $field => $score_setting) {
                    $player_season['data'][$key]['player']['player_stat'][$score_setting['pool_scoring_field_id']] = (isset($player_data['player']['player_stat'][$field])) ? $player_data['player']['player_stat'][$field] : NULL;
                }
                $player_season['data'][$key]['total_points'] = $player_data['player']['season_pool_point']['total_points'];
            }
        }
        $player_season_data = collect($player_season['data']);
        if (!isset($request->live_draft)) {
            $player_season_data->transform(function($item, $key)use($total_pool) {
                $item['free_agent'] = ($item['pool_team_player'] == null) ? true : false;

                if ($item['pool_waiver'] != NULL) {
                    $item['waiver'] = 'Waiver (' . date('M d', strtotime($item['pool_waiver']['waiver_till'])) . ')';
                }

                $item['own'] = round((((isset($item['total_pools_added_in']['count'])) ? $item['total_pools_added_in']['count'] : 0) / $total_pool) * 100, 2);
                return $item;
            });
        }
        $player_season['data'] = $player_season_data->toArray();
        $player_season['pool'] = $pool;

        return $player_season;
    }

    public function draftResult($id)
    {
        if (!$this->_checkPoolActive($id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $id]);
        }
        $pool_details = Pool::active()->where('id', $id)->has('poolTeam')->with(['poolScoreSettings.poolScoringField', 'poolTeam'])->firstOrFail();
        return view('frontend.pool.draft_result', ['pool_id' => $id, 'pool_details' => $pool_details, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id')]);
    }

    public function getDataForDraftResult($id)
    {
        $pool = Pool::active()->where('id', $id)
                ->has('poolTeam')
                ->with([
                    'poolScoreSettings.poolScoringField',
                    'poolSetting.poolsetting'
                ])
                ->firstOrFail();
        $all_competition_types = config('competition.inverse_competition_type');
        $competition_type = ($pool->season_type == config('pool.inverse_season.regular_season')) ? $all_competition_types['Regular Season'] : $all_competition_types['Playoff'];
        if ($pool->pool_type == config('pool.inverse_type.h2h') || $pool->pool_type == config('pool.inverse_type.standard')) {
            $pool->load([
                'poolRounds.poolTeamPlayers' => function($query) {
                    $query->withTrashed()->whereNotNull('pool_round_id');
                },
                'poolRounds.poolTeamPlayers.poolTeam',
                'poolRounds.poolTeamPlayers.playerSeason.player',
                'poolRounds.poolTeamPlayers.playerSeason.competitionSkaterStats' => function($query) use($competition_type) {
                    $query->whereHas('competition', function($query) use($competition_type) {
                                $query->where('competition_type', '=', $competition_type);
                            });
                },
                'poolRounds.poolTeamPlayers.playerSeason.competitionGoalieStats' => function($query) use($competition_type) {
                    $query->whereHas('competition', function($query) use($competition_type) {
                                $query->where('competition_type', '=', $competition_type);
                            });
                },
            ]);

            $round_or_box = 'pool_rounds';
            $team_or_box_player = 'pool_team_players';
        } else {
            $pool->
                    load([
                        'poolTeamPlayers' => function($query) {
                            $query->lists('player_season_id');
                        },
                        'poolBoxes.poolBoxPlayers',
                        'poolBoxes.poolBoxPlayers.playerSeason.player',
                        'poolBoxes.poolBoxPlayers.playerSeason.competitionSkaterStats' => function($query) use($competition_type) {
                            $query->whereHas('competition', function($query) use($competition_type) {
                                        $query->where('competition_type', '=', $competition_type);
                                    });
                        },
                        'poolBoxes.poolBoxPlayers.playerSeason.competitionGoalieStats' => function($query) use($competition_type) {
                            $query->whereHas('competition', function($query) use($competition_type) {
                                        $query->where('competition_type', '=', $competition_type);
                                    });
                        },
            ]);
            $round_or_box = 'pool_boxes';
            $team_or_box_player = 'pool_box_players';
            $pool->poolTeamPlayers->transform(function($item, $key) {
                return $item['player_season_id'];
            });
        }

        $field_scoring = [];
        foreach ($pool->poolScoreSettings as $score_setting) {
            $field_scoring[config('pool.pool_scoring_field.fields_map_to_stat_table')[$score_setting['pool_scoring_field_id']]] = $score_setting->toArray();
        }
        $arrayed_pool = $pool->toArray();
//
        $this->calculateStatAndScore($field_scoring, $round_or_box, $team_or_box_player, 'competition_skater_stats', $arrayed_pool);
        $this->calculateStatAndScore($field_scoring, $round_or_box, $team_or_box_player, 'competition_goalie_stats', $arrayed_pool);
        return $arrayed_pool;
    }

    private function calculateStatAndScore($field_scoring, $round_or_box, $team_or_box_player, $skater_key, &$arrayed_pool)
    {
        foreach ($arrayed_pool[$round_or_box] as $round_key => $pool_round) {
            foreach ($pool_round[$team_or_box_player] as $player_key => $pool_team_player) {
                foreach ($pool_team_player['player_season'][$skater_key] as $stat_key => $skater_stat) {
                    foreach ($field_scoring as $field => $score_setting) {
                        if (isset($skater_stat[$field])) {
                            if (!isset($arrayed_pool[$round_or_box][$round_key][$team_or_box_player][$player_key]['scores'][$score_setting['pool_scoring_field_id']])) {
                                $arrayed_pool[$round_or_box][$round_key][$team_or_box_player][$player_key]['scores'][$score_setting['pool_scoring_field_id']] = [
                                    'count' => 0,
                                    'value' => 0,
                                ];
                            }
                            if (!isset($arrayed_pool[$round_or_box][$round_key][$team_or_box_player][$player_key]['pool_points'])) {
                                $arrayed_pool[$round_or_box][$round_key][$team_or_box_player][$player_key]['pool_points'] = 0;
                            }
                            $arrayed_pool[$round_or_box][$round_key][$team_or_box_player][$player_key]['scores'][$score_setting['pool_scoring_field_id']]['count'] += $skater_stat[$field];
                            $arrayed_pool[$round_or_box][$round_key][$team_or_box_player][$player_key]['scores'][$score_setting['pool_scoring_field_id']]['value'] += $skater_stat[$field] * $score_setting['value'];
                            $arrayed_pool[$round_or_box][$round_key][$team_or_box_player][$player_key]['pool_points'] += $skater_stat[$field] * $score_setting['value'];
                        }
                    }
                }
            }
            $team_player_colection = collect($arrayed_pool[$round_or_box][$round_key][$team_or_box_player]);
            $sorted_team_player = $team_player_colection->sortByDesc('pool_points')->toArray();
            $rank = 1;
            foreach ($sorted_team_player as $key => $team_player) {
                $sorted_team_player[$key]['rank'] = $rank++;
            }
            ksort($sorted_team_player);
            $arrayed_pool[$round_or_box][$round_key][$team_or_box_player] = $sorted_team_player;
        }
    }

    public function getDataForSidebarPoolTeams($pool_id)
    {
        $pool = Pool::active()->has('poolTeam')->findOrFail($pool_id);
        if ($pool->pool_type == config('pool.inverse_type.h2h')) {
            $pool->load([
                'poolTeams.poolMatchupResult',
                'poolTeams.h2hScore',
            ]);
            $pool_teams = $pool->poolTeams->sortByDesc('h2hScore.total_score')->values();
        } else {
            $pool->load([
                'poolTeams.standardScore'
            ]);
            $pool_teams = $pool->poolTeams->sortByDesc('standardScore.total_score')->values();
        }
        return $pool_teams;
    }

    public function matchups($id)
    {
        if (!$this->_checkPoolActive($id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $id]);
        }
        $pool_details = Pool::active()
                        ->where('id', $id)
                        ->where('pool_type', config('pool.inverse_type.h2h'))
                        ->has('poolTeam')
                        ->with([
                            'poolScoreSettings.poolScoringField',
                            'poolTeam',
                            'season.seasonCompetitionType' => function($query) {
                                $query->where('competition_type', '=', config('competition.inverse_competition_type.Regular Season'));
                            },
                            'season.seasonCompetitionType.seasonCompetitionTypeWeeks' => function ($query) {
                                $query->select('name', 'id', 'season_competition_type_id');
                            }
                        ])->firstOrFail();
//                        dd($pool_details->season->seasonCompetitionType->seasonCompetitionTypeWeeks->keyBy('id'));
        $season_weeks = $pool_details->season->seasonCompetitionType->seasonCompetitionTypeWeeks->pluck('name', 'id');
        return view('frontend.pool.matchup_overview', ['pool_id' => $id, 'pool_details' => $pool_details, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id'), 'season_weeks' => $season_weeks->toArray()]);
    }

    public function matchupIndividual($matchup_id)
    {
        $pool_matchup = PoolMatchup::where('id', $matchup_id)->with(['pool.poolScoreSettings.poolScoringField', 'pool.poolSetting.poolsetting'])->firstOrFail();
        return view('frontend.pool.matchup_individual', ['matchup_id' => $matchup_id, 'pool_id' => $pool_matchup->pool->id, 'matchup_details' => $pool_matchup, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id'), 'pool_details' => Pool::findOrFail($pool_matchup->pool->id)]);
    }

    public function getDataForMatchupIndividual(Request $request, $matchup_id)
    {
        return PoolMatchup::getDataForMatchupIndividual($matchup_id, $request->type); // type could be 0 or 1
    }

    public function changeTeamLineup(Request $request, $pool_id)
    {
        $data = $request->all();
        $pool = Pool::active()->where('id', $pool_id)->has('poolTeam')->with([
                    'poolSetting.poolsetting',
                    'poolTeam.poolTeamLineups' => function($query) use($data) {
                        $query->where('start_time', '<=', $data['lineup_date'])->where('end_time', '>=', $data['lineup_date']);
                    },
                ])->firstOrFail();

        $pool_roster = $pool->poolRosters()->lists('value', 'player_position_id');

        if ($pool->pool_type == config('pool.inverse_type.box')) {
            return response()->json(['message' => trans('messages.pool.box_pool_no_lineup')], 405);
        }
        if ($pool->poolSetting->poolsetting->pool_format == config('pool.inverse_format.h2h_weekly')) {
            if (strtotime($data['lineup_date']) > strtotime('sunday last week') && strtotime($data['lineup_date']) <= strtotime('sunday this week')) {
                // Lineup date selected is of current week
                if (date('N', strtotime($data['lineup_date'])) == 1) { // 1 is for monday, weekly win can change lineup only on monday
                    return $this->_addTeamLineup($data, $pool, $pool_roster, date('Y-m-d', strtotime('monday this week')), date('Y-m-d', strtotime('sunday this week')));
                } else {
                    return response()->json(['message' => trans('messages.pool.lineup_locked_weekly_win')], 405);
                }
            } else {
                // Lineup date is of some other week
                $lineup_date = Carbon::createFromFormat('Y-m-d', $data['lineup_date']);
                return $this->_addTeamLineup($data, $pool, $pool_roster, $lineup_date->startOfWeek()->toDateString(), $lineup_date->endOfWeek()->toDateString());
            }
        } else {
            return $this->_addTeamLineup($data, $pool, $pool_roster, $data['lineup_date']);
        }
    }

    private function _addTeamLineup($data, $pool, $pool_roster, $lineup_date, $end_date = NULL)
    {
        \DB::beginTransaction();
        if (count($data['players']) == 2) {
            foreach ($data['players'] as $player) {
                $this->_processTeamLineupChange($pool, $player, $lineup_date, $end_date);
            }
        } elseif (count($data['players']) == 1) {
            $player = $data['players'][0];
            $this->_processTeamLineupChange($pool, $player, $lineup_date, $end_date);
        } else {
            
        }
        $team_current_lineups_check = $pool->poolTeam->poolTeamLineups()
                        ->where('start_time', '<=', $lineup_date)->where('end_time', '>=', ($end_date)? : $lineup_date)
                        ->get()
                        ->groupBy('player_position_id')->toArray();
        $commit = TRUE;
        foreach ($pool_roster as $player_position_id => $value) {
            if ($player_position_id != config('pool.inverse_player_position.bn') && isset($team_current_lineups_check[$player_position_id])) {
                $total_occupied_positions = count($team_current_lineups_check[$player_position_id]);
                if ($total_occupied_positions > $value) {
                    \DB::rollBack();
                    $commit = FALSE;
                    break;
                }
            }
        }
        if ($commit) {
            \DB::commit();
            return response()->json(['message' => trans('messages.pool.lineup_changed')], 200);
        } else {
            return response()->json(['message' => trans('messages.pool.lineup_more_than_roster')], 405);
        }
    }

    private function _processTeamLineupChange($pool, $player, $lineup_date, $end_date)
    {
        $player_position_id = $player['position'];
        $pool_team_player_id = $player['pool_team_player_id'];




        $pool_team_player = $pool->poolTeam->poolTeamPlayers()
                ->where('id', $pool_team_player_id)
                ->whereHas('playerSeason', function($query) use($player_position_id) {
                    $player_position_grouping = config('pool.player_position_grouping');
                    if (isset($player_position_grouping[$player_position_id])) {
                        $query->whereIn('player_position_id', $player_position_grouping[$player_position_id]);
                    } else {
                        $query->where('player_position_id', $player_position_id);
                    }
                })
                ->with([
                    'playerSeason.team',
                    'playerSeason.team.homeCompetitions' => function($query) {
                        $query->where('match_start_date', '<=', date('Y-m-d H:i:s', strtotime('+5 minutes')))
                        ->where(\DB::raw('DATE(match_start_date)'), date('Y-m-d'));
                    },
                    'playerSeason.team.awayCompetitions' => function($query) {
                        $query->where('match_start_date', '<=', date('Y-m-d H:i:s', strtotime('+5 minutes')))
                        ->where(\DB::raw('DATE(match_start_date)'), date('Y-m-d'));
                    },
                    'poolTeamLineup' => function($query) use($lineup_date, $end_date) {
                        $query->where(\DB::raw('DATE(start_time)'), '<=', $lineup_date)->where(\DB::raw('DATE(end_time)'), '>=', ($end_date)? : $lineup_date);
                    }
                ])
                ->firstOrFail();
        if ($pool_team_player->playerSeason->team->homeCompetitions->isEmpty() && $pool_team_player->playerSeason->team->awayCompetitions->isEmpty()) {
            $lineup_db = \App\Models\PoolTeamLineup::firstOrNew([
                        'start_time' => $lineup_date,
                        'end_time' => ($end_date)? : $pool_team_player->poolTeamLineup->end_time,
                        'pool_team_player_id' => $pool_team_player_id,
            ]);
            $lineup_db->player_position_id = $player_position_id;
            $lineup_db->save();
            if ($end_date && $end_date != $pool_team_player->poolTeamLineup->end_time) {
                // This is used only in case of h2h weekly win
                $lineup_db_weekly = \App\Models\PoolTeamLineup::firstOrNew([
                            'start_time' => date('Y-m-d', strtotime($end_date . ' +1 day')),
                            'end_time' => $pool_team_player->poolTeamLineup->end_time,
                            'pool_team_player_id' => $pool_team_player_id,
                ]);
                $lineup_db_weekly->player_position_id = $pool_team_player->poolTeamLineup->player_position_id;
                $lineup_db_weekly->save();
                $pool_team_player->poolTeamLineup->delete();
            }

            if ($lineup_date != $pool_team_player->poolTeamLineup->start_time) {
                $pool_team_player->poolTeamLineup->end_time = date('Y-m-d', strtotime($lineup_date . ' -1 days'));
                $pool_team_player->poolTeamLineup->save();
            }
        } else {
            // cant change its lineup now, game about to start/started
            abort(405, trans('messages.pool.lineup_locked_match_started'));
        }
    }

    public function getPoolHeader($pool_id, $pool_team_id = NULL)
    {
        $pool = Pool::getDataForHeader($pool_id, $pool_team_id);
        return $pool;
    }

    public function getUserPools(Request $request)
    {
        $past_pools = FALSE;
        if (isset($request->past_pools)) {
            $past_pools = TRUE;
        }
        if (isset($request->only_completed_pools)) {
            $pools = Pool::getDataForUserPools($request->page, TRUE, $past_pools);
        } else {
            $pools = Pool::getDataForUserPools($request->page, FALSE, $past_pools);
        }
        return $pools;
    }

    public function getPoolStatForPlayers(Request $request, $pool_id)
    {
        $player_data = PlayerSeason::getPoolStatForPlayers($pool_id, $request->player_season_ids);
        return $player_data;
    }

    public function getPoolTeamSeasonStat(Request $request, $id, $pool_team_id = NULL)
    {
        $pool = Pool::where('id', $id)
                ->has('poolTeam')
                ->with([
                    'poolTeamFirst' => function($query) use($pool_team_id) {
                        if ($pool_team_id != NULL) {
                            $query->where('id', $pool_team_id);
                        } else {
                            $query->where('user_id', Auth::id());
                        }
                    },
                    'poolTeamFirst.poolTeamPlayers',
                    'poolTeamFirst.poolTeamPlayers.poolTeamLineup' => function($query) {
                        $query->current();
                    },
                    'poolTeamFirst.poolTeamPlayers.poolTeamLineup.playerPosition',
                    'poolTeamFirst.poolTeamPlayers.poolTradePlayer' => function($query) {
                        $query->whereHas('poolTrade', function($query) {
                                    $query->where('is_processed', 0);
                                });
                    },
                    'poolSetting.poolsetting'
                ])
                ->firstOrFail();
        $player_season_ids = $pool->poolTeamFirst->poolTeamPlayers->map(function ($item) {
            return $item['player_season_id'];
        });
        $player_data = PlayerSeason::getPoolStatForPlayers($id, $player_season_ids->toArray())->keyBy('id');

        return [
            'pool' => $pool,
            'stat_data' => $player_data
        ];
    }

    public function dropSetup($pool_id)
    {
        if (!$this->_checkPoolActive($pool_id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id]);
        }
        $pool_details = Pool::getPoolDataForAddDrop($pool_id);
        list($status, $message) = $pool_details->poolTeam->checkAcquisitionAvailable($pool_details);
        if (!$status) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id])->withFlashDanger($message);
        }
        return view('frontend.pool.drop_setup', ['pool_id' => $pool_id, 'pool_details' => $pool_details, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id')]);
    }

    public function addSetup($pool_id)
    {
        if (!$this->_checkPoolActive($pool_id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id]);
        }
        $pool_details = Pool::getPoolDataForAddDrop($pool_id);
        list($status, $message) = $pool_details->poolTeam->checkAcquisitionAvailable($pool_details);
        if (!$status) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id])->withFlashDanger($message);
        }
        return view('frontend.pool.add_setup', ['pool_id' => $pool_id, 'pool_details' => $pool_details, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id')]);
    }

    public function trade($pool_id)
    {
        if (!$this->_checkPoolActive($pool_id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id]);
        }
        $pool_details = Pool::getPoolDataForAddDrop($pool_id);

        if ($pool_details->poolSetting->poolsetting->trade_end_date->lte(Carbon::now())) {
            // Trade End Date setting check
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id])->withFlashDanger(trans('messages.pool.trade_ended', ['date' => $pool_details->poolSetting->poolsetting->trade_end_date->toDateString()]));
        } else {
            // checking if user exceeded no of trades allowed in a season
            list($status, $message) = $pool_details->poolTeam->checkTradeAvailable($pool_details->poolSetting->poolsetting->max_trades_season);
            if (!$status) {
                return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id])->withFlashDanger($message);
            }
            return view('frontend.pool.trades', [
                'pool_id' => $pool_id,
                'pool_details' => $pool_details,
                'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id'),
                'pool_teams' => PoolTeam::where('pool_id', $pool_id)->where('id', '<>', $pool_details->poolTeam->id)->lists('name', 'id')->toArray()
            ]);
        }
    }

    public function transaction($id)
    {
        if (!$this->_checkPoolActive($id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $id]);
        }
        $pool_details = Pool::active()->where('id', $id)->where('pool_type', '<>', config('pool.inverse_type.box'))->has('poolTeam')->with(['poolScoreSettings.poolScoringField', 'poolTeam'])->firstOrFail();
        return view('frontend.pool.transactions', ['title'=>'Hockey Pool -'.$pool_details->name, 'pool_id' => $id, 'pool_details' => $pool_details, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id')]);
    }

    public function getTransactionsData($pool_id)
    {
        $pool = Pool::active()->where('id', $pool_id)
                ->has('poolTeam')
                ->with([
                    'deletedPoolTeamPlayers',
                    'deletedPoolTeamPlayers.playerSeason',
                    'deletedPoolTeamPlayers.playerSeason.player',
                    'deletedPoolTeamPlayers.playerSeason.team',
                    'deletedPoolTeamPlayers.playerSeason.playerPosition',
                    'deletedPoolTeamPlayers.poolTeam',
                    'poolTeamPlayers' => function($query) {
                        $query->whereNull('pool_round_id')
                        ->whereNotExists(function($subquery) {
                                    /**
                                     *  We are checking if this player was added
                                     * through trade.
                                     */
                                    $subquery->select(\DB::raw(1))
                                    ->from('pool_trade_players')
                                    ->whereRaw('pool_trade_players.pool_team_player_id = pool_team_players.id');
                                })
                        ->withTrashed();
                    },
                    'poolTeamPlayers.playerSeason',
                    'poolTeamPlayers.playerSeason.playerPosition',
                    'poolTeamPlayers.playerSeason.player',
                    'poolTeamPlayers.playerSeason.team',
                    'poolTeamPlayers.poolTeam',
                    'completedPoolTrades',
                    'completedPoolTrades.poolTradePlayers',
                    'completedPoolTrades.poolTradePlayers.poolTeamPlayer' => function($query) {
                        $query->withTrashed();
                    },
                    'completedPoolTrades.parentPoolTeam',
                    'completedPoolTrades.poolTradePlayers.poolTeamPlayer.playerSeason',
                    'completedPoolTrades.poolTradePlayers.poolTeamPlayer.playerSeason.player',
                    'completedPoolTrades.poolTradePlayers.poolTeamPlayer.playerSeason.team',
                    'completedPoolTrades.poolTradePlayers.poolTeamPlayer.playerSeason.playerPosition',
                    'completedPoolTrades.poolTradePlayers.poolTeamPlayer.poolTeam',
                ])
                ->firstOrFail();


        $merged_collection = Pool::createTransactionData($pool);

        return $merged_collection->sortByDesc('sort_date')->values();
    }

    public function boxDraft($id)
    {
        if (!$this->_checkPoolActive($id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $id]);
        }
        $pool_details = Pool::active()
                ->where('id', $id)
                ->where('pool_type', config('pool.inverse_type.box'))
                ->has('poolTeam')
                ->with(['poolTeam.poolTeamPlayers'])
                ->firstOrFail();
        if (!$pool_details->poolTeam->poolTeamPlayers->isEmpty()) {
            return redirect()->route('frontend.pool.dashboard', $id)->withFlashDanger(trans('messages.pool.draft_selection_already_done'));
        }
        return view('frontend.pool.box_draft', ['pool_id' => $id, 'pool_details' => $pool_details, 'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id')]);
    }

    public function getDataForBoxDraft($pool_id)
    {
        return Pool::active()
                        ->where('id', $pool_id)
                        ->has('poolTeam')
                        ->where('pool_type', config('pool.inverse_type.box'))
                        ->with([
                            'poolBoxes.poolBoxPlayers.playerSeason.player',
                            'poolBoxes.poolBoxPlayers.playerSeason.team',
                        ])
                        ->firstOrFail();
    }

    public function saveBoxPoolTeamPlayers(Request $request, $pool_id)
    {
        $pool_details = Pool::active()
                ->where('id', $pool_id)
                ->where('pool_type', config('pool.inverse_type.box'))
                ->has('poolTeam')
                ->with([
                    'poolBoxPlayers' => function($query) use($request) {
                        $query->whereIn('pool_box_players.id', $request->box_player_ids);
                    },
                    'poolTeam.poolTeamPlayers',
                    'poolRosters',
                    'season'
                ])
                ->firstOrFail();

        if ($pool_details->poolTeam->poolTeamPlayers->isEmpty()) {
            $sync_data = [];
            foreach ($pool_details->poolBoxPlayers as $box_player) {
                $sync_data[] = $box_player->player_season_id;
            }

            if ($pool_details->poolTeam->playerSeason()->sync($sync_data)) {
                $pool_team_players = $pool_details->poolTeam->poolTeamPlayers()->with('playerSeason')->get();
                $team_roster_track = $this->_createTeamRosterTrack($pool_details->poolRosters);
                foreach ($pool_team_players as $pool_team_player) {
                    PoolTeamLineup::savePlayerLineup($pool_team_player, $pool_details->season->end_date, $team_roster_track);
                }
                return response()->json(array('message' => trans('messages.success.data_saved')), 200);
            } else {
                return response()->json(array('message' => trans('messages.error.cant_save')), 500);
            }
        } else {
            abort(403, trans('pool.development_error'));
        }
    }

    public function getmatchupHeaderData($pool_id)
    {
        $pool = Pool::active()
                ->has('poolTeam')
                ->where('id', $pool_id)
                ->with([
                    'poolMatchups' => function($query) {
                        $query->select('id', 'pool_id', 'team1', 'team2', 'season_competition_type_week_id');
                    },
                    'poolMatchups.poolTeam1' => function($query) {
                        $query->select('id', 'name');
                    },
                    'poolMatchups.totalPoolMatchupResults',
                    'poolMatchups.poolTeam2' => function($query) {
                        $query->select('id', 'name');
                    },
                    'poolMatchups.seasonCompetitionTypeWeek' => function($query) {
                        $query->select('id', 'name');
                    }
                ])
                ->select('id')
                ->firstOrFail();
        $pool->poolMatchups->transform(function($item) {
            $item->pool_matchup_results = $item->totalPoolMatchupResults->keyBy('pool_team_id');
            unset($item->totalPoolMatchupResults);
            return $item;
        });
        return $pool;
    }
}
