<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Request;
use Auth;
use Carbon\Carbon;
use App\Models\Blog;
use App\Models\BlogComment;

class BlogController extends Controller {

    /**
     * Lists all the published blogs
     */
    public function index() {
        $blogs = Blog::getAllPublishedBlogs();
        return view('frontend.blog.index', ['blogs' => $blogs]);
    }

    /**
     * Get the recent blogs on the homepage
     */
    public static function blogsWidget() {
        $blogs = Blog::getBlogsPostsForHomepage();
        return view('frontend.blog.widget', ['blogs' => $blogs]);
    }

    /**
     * Single blog post page
     */
    public static function blogPost($slug) {
        $blog = Blog::getPublishedBlogInfo($slug);

        if (strlen($blog->body) > 320) {
            $pos = strpos($blog->body, ' ', 320);
            $short_description = substr($blog->body, 0, $pos) . '...';
        } else {
            $short_description = $blog->body;
        }

        $share_data = array(
            'title' => $blog->title,
            'short_description' => strip_tags($short_description),
            'image' => $blog->featured_image
        );

        return view('frontend.blog.single_post', ['blog' => $blog, 'share_data' => $share_data]);
    }

    public function blogCommentsSubmit(BlogComment $request) {
        if (Request::ajax()) {
            $blog_check = Blog::where('id', Request::input('blog_id'))->get()->first();

            if (!empty($blog_check)) {
                $comment_data = array(
                    'user_id' => Auth::user()->id,
                    'blog_id' => $blog_check->id,
                    'comment' => Request::input('comment')
                );
                BlogComment::create($comment_data);
                return response()->json(['created' => Carbon::createFromTimeStamp(strtotime('now'))->diffForHumans(), 'name' => Auth::user()->name], 201);
                die();
            } else {
                return response()->json(['error' => trans('blog.blogCommentsSubmit.invalid_blog_error')], 500);
                die();
            }
        }
    }

}
