<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\MessageBoard;
use App\Models\MessageBoardReply;
use App\Models\MessageBoardStatus;
use App\Models\Pool;
use App\Models\PoolTeam;
use App\Http\Requests\Frontend\MessageBoardRequest;
use App\Http\Requests\Frontend\MessageBoardReplyRequest;
use Illuminate\Http\Request;
use App\Models\Notification;
use Auth;
use DB;

/**
 * Description of MessageBoardsController
 *
 * @author Anik
 */
class MessageBoardsController extends Controller {

    public function index($pool_id, $message_board_id = NULL)
    {
        $pool_details = Pool::active()->where('id', $pool_id)->has('poolTeam')->with('poolTeam')->firstOrFail();
        if ($message_board_id !== NULL) {
            MessageBoard::where('pool_id', $pool_id)->findOrFail($message_board_id);
        }

        $pool_team_users = PoolTeam::where('pool_id', $pool_id)->with('user')->get();
        $user_data = [];

        foreach ($pool_team_users as $value) {
            if ($value->user && ($value->user->id != Auth::id()) && !empty($value->user->name)) {
                $user_data[$value->user->id] = $value->user->name;
            } elseif ($value->user && ($value->user->id != Auth::id())) {
                $user_data[$value->user->id] = $value->user->email;
            }
        }

        return view('frontend.pool.message_boards', [
            'pool_id' => $pool_id,
            'message_board_id' => $message_board_id,
            'user_pools' => Pool::active()->has('poolTeam')->lists('name', 'id'),
            'user_data' => $user_data,
            'pool_details'=>$pool_details
        ]);
    }

    public function getAllMessageBoards(Request $request, $pool_id)
    {
        DB::enableQueryLog();

        if (isset($request->message_type) && ($request->message_type == config('message_board.message_type.private') )) {
            $type = $request->message_type;
            $with = ['user', 'latestReply.user', 'toUser', 'totalReplies'];
        } else {
            $type = config('message_board.message_type.public');
            $with = ['user', 'likes', 'dislikes', 'totalReplies', 'latestReply.user'];
        }

        $message_boards_query = MessageBoard::
                        whereHas('pool', function($query) use($pool_id) {
                            $query->where('id', $pool_id)->has('poolTeam');
                        })
                        ->leftJoin(DB::raw("( SELECT  r.message_board_id, MAX(created_at) reply_max_created_at
                            FROM    message_board_replies r
                            WHERE r.parent_id IS NULL
                            GROUP BY r.message_board_id 
                            ) as r"), function($join) {
                            $join->on("message_boards.id", "=", "r.message_board_id");
                        })
                        ->with($with)->where('type', $type);


        if (isset($request->self) && $request->self == 1) {
            $message_boards_query->where('message_boards.user_id', Auth::id());
        }

        if (isset($request->sort_by)) {
            if ($request->sort_by == 'latest_reply') {
                $message_boards_query->orderBy('reply_max_created_at', $request->sort_by_type)->orderBy('message_boards.created_at', $request->sort_by_type);
            }
        } else {
            $message_boards_query->orderBy(DB::raw("IFNULL(reply_max_created_at, message_boards.created_at)"), 'desc');
        }


        $message_boards = $message_boards_query->paginate(config('message_board.paginations.boards.per_page_limit'))->toArray();

        $message_boards['data_unread'] = MessageBoardStatus::where('is_read', 0)
                        ->where('user_id', Auth::id())
                        ->where('pool_id', $pool_id)
                        ->pluck('message_board_id')->toArray();


        return $message_boards;
    }

    public function store(MessageBoardRequest $request)
    {
        if ($request->ajax()) {
            $pool = Pool::active()
                    ->where('id', $request->pool_id)
                    ->has('poolTeam')
                    ->firstOrFail();
            $pool_team_users = PoolTeam::where('pool_id', $request->pool_id)->where('user_id', '!=', NULL)->pluck('user_id');

            $message_users = [];
            $message_board = new MessageBoard;
            $message_board->user_id = Auth::id();
            $message_board->title = $request->title;
            $message_board->description = $request->description;
            $message_board->pool_id = $request->pool_id;

            if (isset($request->to) && $pool_team_users->contains($request->to)) {

                $message_board->to_user_id = $request->to;
                $message_board->type = config('message_board.message_type.private');
                $message_users[0] = $request->to;
            } elseif (isset($request->to) && !$pool_team_users->contains($request->to)) {
                return response()->json(array('message' => trans('messages.error.cant_save')), 500);
            } else {
                $message_users = $pool_team_users->toArray();
            }
            if ($message_board->save()) {
                if (isset($request->to)) {
                    $message_board_status = new MessageBoardStatus();
                    $message_board_status->user_id = $request->to;
                    $message_board_status->message_board_id = $message_board->id;
                    $message_board_status->pool_id = $request->pool_id;
                    $message_board_status->save();
                }
                Notification::createAndBroadcast($pool->name, trans('notifications.new_message_board', ['Pool' => $pool->name]), route('frontend.pool.message_board.index', [$request->pool_id, $message_board->id]), $message_users, $pool->id);
                return response()->json(array('message' => trans('messages.success.data_saved'), 'data' => ['id' => $message_board->id]), 200);
            } else {
                return response()->json(array('message' => trans('messages.error.cant_save')), 500);
            }
        }
    }

    public function getDataForMessageBoard(Request $request, $id)
    {
        $message_board_status = MessageBoardStatus::where('message_board_id', $id)
                ->where('user_id', Auth::id())
                ->first();
        if ($message_board_status) {
            $message_board_status->is_read = 1;
            $message_board_status->save();
        }

        $with = [
            'user',
            'user.poolTeam' => function($query) use($request) {
                $query->where('pool_id', $request->pool_id)->select('user_id', 'name', 'logo');
            },
            'likes',
            'dislikes',
            'currentUserReaction',
            'totalDirectReplies',
            'directReplies' => function($query) use($request) {
                $query->paginate(config('message_board.paginations.replies.per_page_limit'));
                if (isset($request->replies_sort_by)) {
                    $query->orderBy($request->replies_sort_by, $request->replies_sort_by_type);
                } else {
                    $query->orderBy('created_at', 'asc');
                }
            },
            'directReplies.user',
            'directReplies.user.poolTeam' => function($query) use($request) {
                $query->where('pool_id', $request->pool_id)->select('user_id', 'name', 'logo');
            },
            'directReplies.likes',
            'directReplies.dislikes',
            'directReplies.currentUserReaction',
            'directReplies.descendants',
            'directReplies.descendants.user.poolTeam' => function($query) use($request) {
                $query->where('pool_id', $request->pool_id)->select('user_id', 'name', 'logo');
            },
            'directReplies.descendants.likes',
            'directReplies.descendants.dislikes',
            'directReplies.descendants.currentUserReaction',
        ];

        if (isset($request->message_type) && ($request->message_type == config('message_board.message_type.private') )) {
            $with += ['toUser'];
        }
        $message_board = MessageBoard::where('id', $id)
                ->whereHas('pool', function($query) {
                    $query->has('poolTeam');
                })
                ->with($with)
                ->firstOrFail();
        $message_board->directReplies->transform(function($item) {
            $item->children = $item->descendants->toTree()->toArray();
            unset($item->descendants);
            return $item;
        });
        return $message_board;
    }

    public function reply(MessageBoardReplyRequest $request)
    {
        if ($request->ajax()) {

            $message_board = MessageBoard::whereHas('pool', function($query) {
                        $query->has('poolTeam');
                    })
                    ->with(['pool'])
                    ->findOrFail($request->message_board_id);
            $notification_user_id = $message_board->user_id;
            $create_array = [
                'user_id' => Auth::id(),
                'reply' => $request->reply
            ];
            if (isset($request->reply_id) && $request->reply_id != "") {
                $create_array['parent_id'] = $request->reply_id;
                $notification_user_id = $message_board->messageBoardReply()->where('id', $request->reply_id)->firstOrFail()->user_id;
            }
            $reply_save = $message_board->messageBoardReplies()->create($create_array);
            if ($reply_save) {

                If (!empty($message_board->to_user_id)) {

                    if ($message_board->to_user_id == Auth::id()) {
                        $message_board_user = $message_board->user_id;
                    } else {
                        $message_board_user = $message_board->to_user_id;
                    }

                    $message_board_status = MessageBoardStatus::where('message_board_id', $request->message_board_id)
                            ->where('user_id', $message_board_user)
                            ->first();
                    if ($message_board_status) {
                        $message_board_status->is_read = 0;
                        $message_board_status->pool_id = $message_board->pool_id;
                        $message_board_status->save();
                    } else {
                        $message_board_status = new MessageBoardStatus();
                        $message_board_status->user_id = $message_board_user;
                        $message_board_status->message_board_id = $message_board->id;
                        $message_board_status->pool_id = $message_board->pool_id;
                        $message_board_status->save();
                    }
                }
                Notification::createAndBroadcast($message_board->pool->name, trans('notifications.reply_to_post'), route('frontend.pool.message_board.index', [$message_board->pool->id, $message_board->id]), [$notification_user_id], $message_board->pool->id);
                return response()->json(array('message' => trans('messages.message_board.reply_added'), 'data' => ['id' => $reply_save->id]), 200);
            } else {
                return response()->json(array('message' => trans('messages.error.cant_save')), 500);
            }
        }
    }

    public function saveMessageReaction(Request $request)
    {
//    public function saveMessageReaction() {
        if ($request->ajax()) {
//        $request = new \stdClass();
//        $request->message_board_reply_id = 3;
//        $request->type = 1;
            if (isset($request->message_board_id)) {
                $parent = MessageBoard::whereHas('pool', function($query) {
                            $query->has('poolTeam');
                        })
                        ->findOrFail($request->message_board_id);
            } elseif (isset($request->message_board_reply_id)) {
                $parent = MessageBoardReply::whereHas('messageBoard', function($query) {
                            $query->whereHas('pool', function($query) {
                                $query->has('poolTeam');
                            });
                        })
                        ->findOrFail($request->message_board_reply_id);
            } else {
                return response()->json(array('message' => trans('messages.message_board.no_parent_for_reaction')), 500);
            }
            $reaction = $parent->reactions()->firstOrNew([
                'user_id' => Auth::id(),
            ]);
            if ($reaction->type == NULL) {
                $reaction->type = $request->type;
                if ($reaction->save()) {
                    return response()->json(array('message' => trans('messages.success.data_saved')), 200);
                } else {
                    return response()->json(array('message' => trans('messages.error.cant_save')), 500);
                }
            } else {
                return response()->json(array('message' => trans('messages.message_board.already_reacted')), 500);
            }
        }
    }

    /**
     * Delete Private Messages
     * @param Request $request
     * @return type json
     */
    public function delete(Request $request)
    {
        if (isset($request->message_reply_id)) {
            $message_delete = MessageBoardReply::where('id', $request->message_reply_id)->where('user_id', Auth::id())->delete();
        } else {
            $message_delete = MessageBoard::where('id', $request->message_board_id)->where('user_id', Auth::id())->delete();
        }
        if ($message_delete) {
            if (isset($request->message_type) && ($request->message_type == config('message_board.message_type.private') )) {
                MessageBoardStatus::where('message_board_id', $request->message_board_id)->delete();
            }
            return response()->json(array('message' => trans('messages.message_board.deleteSuccess')), 200);
        } else {
            return response()->json(array('message' => trans('messages.message_board.deleteFail')), 500);
        }
    }

}
