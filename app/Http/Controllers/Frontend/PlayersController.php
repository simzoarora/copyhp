<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Player;
use App\Models\InjuredPlayer;
use App\Models\Pool;
use Illuminate\Http\Request;

/**
 * Description of PlayersController
 *
 * @author Anik
 */
class PlayersController extends Controller {

    public function search(Request $request) {
        $player_query_builder = Player::with([
                    'currentPlayerSeason.team' => function($query) {
                        $query->select('teams.id', 'teams.short_name');
                    },
                    'currentPlayerSeason.playerPosition'
                ])->select('players.id', 'players.first_name', 'players.last_name');
        if (isset($request->name)) {
            $player_query_builder = $player_query_builder->where(function($query) use($request) {
                $exploded_name = explode(' ', $request->name);
                if (count($exploded_name) > 1) {
                    $query->where('players.first_name', 'LIKE', "%" . $exploded_name[0] . "%")
                            ->where('players.last_name', 'LIKE', "%" . $exploded_name[1] . "%");
                } else {
                    $query->where('players.first_name', 'LIKE', "%" . $request->name . "%")
                            ->orWhere('players.last_name', 'LIKE', "%" . $request->name . "%");
                }
            });
        }
        if (isset($request->player_position_id)) {
            $player_query_builder = $player_query_builder->whereHas('currentPlayerSeason', function($query) use($request) {
                $player_position_grouping = config('pool.player_position_grouping');
                if (isset($player_position_grouping[$request->player_position_id])) {
                    $query->whereIn('player_position_id', $player_position_grouping[$request->player_position_id]);
                } elseif ($request->player_position_id == config('pool.inverse_player_position.ir')) {
                    // for searching Injured player
                    $query->has('injuredPlayer');
                } else {
                    $query->where('player_position_id', $request->player_position_id);
                }
            });
        } else {
            $player_query_builder = $player_query_builder->has('currentPlayerSeason');
        }

        if (isset($request->ignore_player_id)) {
            $player_query_builder = $player_query_builder->whereNotIn('id', $request->ignore_player_id);
        }
        
        $players = $player_query_builder->get();
        return $players;
    }

    public function getDataForSidebarInjuryReport($pool_id = NULL) {
        $injured_players_query_builder = InjuredPlayer::with([]);
        if ($pool_id != NULL) {
            $pool = Pool::find($pool_id);
            if($pool) {

                $injured_players_query_builder = $injured_players_query_builder->whereHas('playerSeason', function($query) use($pool){
                    $query->where('season_id',$pool->season_id);
                })->with([
                    'playerSeason.player',
                    'playerSeason.team',
                    'playerSeason.poolTeamPlayer' => function($query) use($pool_id) {
                        $query->whereHas('poolTeam', function($query) use($pool_id) {
                                    $query->where('pool_id', $pool_id);
                                })->select('id', 'player_season_id');
                    }
                ]);
            }
        }
        $injured_players = $injured_players_query_builder->get();
        return $injured_players;
    }

    public function stat(Request $request, $id) {
        \DB::enableQueryLog();
        $player = Player::where('id', $id)
                        ->with([
                            'currentPlayerSeason.team.homeCompetitions',
                            'currentPlayerSeason.team.awayCompetitions',
                        ])->firstOrFail()
        ;
        dump(\DB::getQueryLog());
        dd($player->toArray());
    }

}
