<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Repositories\Frontend\Access\User\UserRepositoryContract;
use Illuminate\Http\Request;
use App\Models\EmailNotificationSetting;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Frontend
 */
class ProfileController extends Controller {

    /**
     * @return mixed
     */
    public function edit() {
        return view('frontend.user.profile.edit')
                        ->withUser(access()->user());
    }

    /**
     * @param  UserRepositoryContract         $user
     * @param  UpdateProfileRequest $request
     * @return mixed
     */
    public function update(UserRepositoryContract $user, UpdateProfileRequest $request) {
        $user->updateProfile(access()->id(), $request->all());
        return redirect()->route('frontend.user.dashboard')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));
    }

    public function updateEmailNotificationSetting(Request $request) {
        if ($request->ajax()) {
            foreach ($request->data as $data) {
                if (!isset($data['value'])) {
                    $data['value'] = 0;
                }
                $email_notification_setting = EmailNotificationSetting::firstOrNew([
                            'user_id' => access()->id(),
                            'type' => $data['type']
                ]);
                $email_notification_setting->value = $data['value'];
                $email_notification_setting->save();
            }
            return response()->json(array('message' => trans('auth.editAccount.messages.success')), 200);
        }
    }

}
