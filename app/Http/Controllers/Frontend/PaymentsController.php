<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Pool;
use App\Models\PoolPayment;
use App\Models\PoolPaymentItem;
use App\Models\StoredValue;
use App\Models\DraftKitPayment;
use Log;
use Mail;

/**
 * Description of PaymentsController
 *
 * @author Anik
 */
class PaymentsController extends Controller {

    public function index($pool_id, $payment_item_id) {
        $pool_payment_item = PoolPaymentItem::where('is_active', 1)->findOrFail($payment_item_id);
        $link = config('pool.payment_link') . $pool_payment_item->cart . '?pool_id=' . $pool_id;

        $ad_link = NULL;
        if ($pool_payment_item->is_premium == 1) {
            $ad_supported = PoolPaymentItem::where('is_active', 1)->where('is_premium', 0)->firstOrFail();
            $ad_link = route('frontend.pool.payment', [$pool_id, $ad_supported->id]);
        }
        return view('frontend.payment.index', ['pool_id' => $pool_id, 'link' => $link, 'pool_payment_item' => $pool_payment_item, 'ad_link' => $ad_link]);
    }

    public function draftkit() {
        $sku = StoredValue::where('key', 'draft_kit_sku')->firstOrFail();
        $price = StoredValue::where('key', 'draft_kit_price')->firstOrFail();
        return view('frontend.payment.draft_kit', ['sku' => $sku, 'price' => $price, 'link' => config('pool.payment_link') . $sku->value]);
    }

    public function draftKitWebhook() {
        if ($this->_verifyAuthToken()) {
            $data = json_decode(json_encode(new \SimpleXMLElement(file_get_contents("php://input"))), 1);
            if (env('PAYMENT_MODE') == 'live' && isset($data['void'])) {
                Log::alert('PAYMENT ERROR : TEST CARD USED : ' . json_encode($data));
                die('OK');
            }

            $payment = new DraftKitPayment;
            $payment->order_id = $data['@attributes']['id'];
            $payment->order_date = date('Y-m-d H:i:s', $data['@attributes']['date']);
            $payment->event_id = $data['@attributes']['event_id'];
            $payment->event_ref = $data['@attributes']['ref'];
            $payment->event_time = $data['event']['@attributes']['date'];
            $payment->event_status_code = $data['event']['@attributes']['status_code'];
            $payment->sale_amount_usd = $data['event']['sale']['@attributes']['amount_usd'];
            $payment->sale_amount = $data['event']['sale']['@attributes']['amount'];
            $payment->sale_method = $data['event']['sale']['@attributes']['method'];
            $payment->sale_currency = $data['event']['sale']['@attributes']['currency'];
            $payment->customer_name = $data['customer']['name'];
            $payment->customer_email = $data['customer']['email'];
            $payment->customer_ip = $data['customer']['ip'];
            if ($payment->save()) {
                $season = \App\Models\Season::current()->first()->name;
                Mail::send('emails.drat_kit', [], function ($mail) use ($payment, $season) {
                    $mail->attach(app_path() . '/Storage/draftkits/' . $season . '_hockeypool_draft_kit.pdf');
                    $mail->from(\Config('mail.from.address'), \Config('mail.from.name'));
                    $mail->to($payment->customer_email)->subject(trans('emails.subjects.draft_kit'));
                });
            } else {
                Log::alert('PAYMEN ERROR : Not able to save draft kit payment : ' . json_encode($data));
            }
        }
    }

    private function _verifyAuthToken() {
        // Your unique AuthToken
        $authToken = env('REVENUEWIRE_AUTHTOKEN');

        $message = file_get_contents("php://input");

        $signature = base64_encode(hash_hmac('sha256', $message, $authToken));
        if (isset($_SERVER['HTTP_X_REVENUEWIRE_SIGNATURE']) && $_SERVER['HTTP_X_REVENUEWIRE_SIGNATURE'] === $signature) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function webhook() {
        // Your unique AuthToken
        $authToken = env('REVENUEWIRE_AUTHTOKEN');

        $message = file_get_contents("php://input");

        $signature = base64_encode(hash_hmac('sha256', $message, $authToken));

        $data = json_decode(json_encode(new \SimpleXMLElement($message)), 1);
        if (isset($_SERVER['HTTP_X_REVENUEWIRE_SIGNATURE']) && $_SERVER['HTTP_X_REVENUEWIRE_SIGNATURE'] === $signature) {
            Log::info('PAYMENT RECEIVED : ' . json_encode($data));
            if (env('PAYMENT_MODE') == 'live' && isset($data['void'])) {
                Log::alert('PAYMENT ERROR : TEST CARD USED : ' . json_encode($data));
                die('OK');
            }


            preg_match("/pool_id=(\\d*)/", urldecode($data['extra']['request']), $matches);
            if (empty($matches) || !isset($matches[1])) {
                Log::emergency('PAYMENT ERROR : POOL NOT FOUND : ' . json_encode($data));
                die('OK');
            } else {
                $pool_id = $matches[1];
            }

            $pool = Pool::where('id', $pool_id)->with('season')->firstOrFail();
            $pool_payment = new PoolPayment;
            $pool_payment->pool_id = $pool->id;
            $pool_payment->order_id = $data['@attributes']['id'];
            $pool_payment->order_date = date('Y-m-d H:i:s', $data['@attributes']['date']);
            $pool_payment->event_id = $data['@attributes']['event_id'];
            $pool_payment->event_ref = $data['@attributes']['ref'];
            $pool_payment->event_time = $data['event']['@attributes']['date'];
            $pool_payment->event_status_code = $data['event']['@attributes']['status_code'];
            $pool_payment->sale_amount_usd = $data['event']['sale']['@attributes']['amount_usd'];
            $pool_payment->sale_amount = $data['event']['sale']['@attributes']['amount'];
            $pool_payment->sale_method = $data['event']['sale']['@attributes']['method'];
            $pool_payment->sale_currency = $data['event']['sale']['@attributes']['currency'];
            $pool_payment->customer_name = $data['customer']['name'];
            $pool_payment->customer_email = $data['customer']['email'];
            $pool_payment->customer_ip = $data['customer']['ip'];
            $pool_payment->pool_payment_item_id = PoolPaymentItem::where('sku', '=', $data['products']['item']['@attributes']['sku'])->first()->id;
            if ($data['products']['item']['@attributes']['next_expected_date'] != "") {
                $pool_payment->next_expected_date = $data['products']['item']['@attributes']['next_expected_date'];
            }
            $pool_payment->save();
            $pool->is_active = 1;
            $pool->active_till = NULL;
            $pool->save();
        } else {
            Log::alert('PAYMENT ERROR : WEBHOOK NOT FROM SOURCE : ' . json_encode($data));
            die('OK');
        }
        die('OK');
    }

}
