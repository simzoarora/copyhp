<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\SupportRequest;
use App\Http\Requests\Frontend\AdvertiseRequest;
use Illuminate\Http\Response;
use Request;
use Mail;

class PagesController extends Controller {

    public function advertise() {
        return view('frontend.pages.advertise');
    }

    public function advertiseSubmit(AdvertiseRequest $request) {
        if ($request->ajax()) {
            $data_to_send = array();
            $data_to_send['name'] = Request::input('name');
            $data_to_send['email'] = Request::input('email');
            $data_to_send['information'] = Request::input('information');

            if (Mail::send('emails.advertise', ['data' => $data_to_send], function($message)use($data_to_send) {
                        $message->to(config('mail.admin_email.email'), config('mail.admin_email.name'))->subject(trans('pages.advertise.mail.subject'));
                    })) {
                return response()->json(['message' => trans('pages.advertise.messages.success')], 201);
                die();
            }
            return response()->json(['message' => trans('pages.advertise.messages.failure')], 500);
            die();
        }
    }

    public function support() {
        return view('frontend.pages.support');
    }

    public function supportSubmit(SupportRequest $request) {
        if ($request->ajax()) {
            $support = array();
            $support['name'] = Request::input('name');
            $support['email'] = Request::input('email');
            $support['question'] = Request::input('question');
            $support['type'] = Request::input('type');
            $support['page_type'] = ucfirst(Request::input('page_type'));

            if (Mail::send('emails.support', ['data' => $support], function($message)use($support) {
                        $message->to(config('mail.admin_email.email'), config('mail.admin_email.name'))->subject("Hockeypool: " . $support['page_type'] . trans('pages.contact.mail.subject'));
                    })) {
                return response()->json(['message' => trans('pages.contact.messages.success')], 201);
                die();
            }
            return response()->json(['message' => trans('pages.contact.messages.failure')], 500);
            die();
        }
    }

    public function faq() {
        return view('frontend.pages.faq');
    }

    public function poolTypes() {
        return view('frontend.pages.pool_types');
    }

    public function howItWorks() {
        return view('frontend.pages.how_it_works');
    }

    public function privacyPolicy() {
        return view('frontend.pages.privacy_policy');
    }

    public function termsOfUse() {
        return view('frontend.pages.terms_of_use');
    }
    
    public function draftkit() {
        return view('frontend.pages.draft_kit');
    }

}
