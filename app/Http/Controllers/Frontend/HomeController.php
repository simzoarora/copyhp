<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Auth;
use App\Models\TweetComment;
use App\Models\TwitterFeed;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Request;
use App\Models\Access\User\User;

class HomeController extends Controller {

    public function index() {
        // $user = User::find(1);
        // Auth::login($user);
        return view('frontend.home.index');
    }

    public function poolCreator() {
        return view('frontend.home.pool_creator');
    }

    public function twitterCommentsSubmit() {
        if (Request::ajax()) {
            $user = Auth::user();
            $twitter_result = TwitterFeed::where('tweet_id', Request::input('tweet_id'))->get()->first();
            if (!empty($twitter_result)) {
                $comment_data = array(
                    'user_id' => $user->id,
                    'tweet_id' => $twitter_result->id,
                    'comment' => Request::input('comment')
                );
                TweetComment::create($comment_data);
                return response()->json(['created' => Carbon::createFromTimeStamp(strtotime('now'))->diffForHumans(), 'name' => $user->name], 201);
                die();
            } else {
                return response()->json(['error' => trans('home.twitterCommentsSubmit.invalid_tweet_error')], 500);
                die();
            }
        }
    }

}
