<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Services\Access\Traits\ConfirmUsers;
use App\Services\Access\Traits\UseSocialite;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Services\Access\Traits\AuthenticatesAndRegistersUsers;
use App\Repositories\Frontend\Access\User\UserRepositoryContract;
use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Http\Requests\Frontend\Auth\LoginRequest;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Http\Requests\Frontend\User\ChangePasswordRequest;
use App\Models\Access\User\User;
use App\Models\PoolInvitation;
use App\Models\PoolTeam;
use App\Models\Pool;
use App\Models\EmailNotificationSetting;
use Socialite;
use Auth;
use Hash;
use Request;
use Redirect;

/**
 * Class AuthController
 * @package App\Http\Controllers\Frontend\Auth
 */
class AuthController extends Controller {

    use AuthenticatesAndRegistersUsers,
        ConfirmUsers,
        ThrottlesLogins,
        UseSocialite;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
//    protected $redirectTo = '/dashboard';
    protected $redirectTo = '/';

    /**
     * Where to redirect users after they logout
     *
     * @var string
     */
    protected $redirectAfterLogout = '/';

    /**
     * @param UserRepositoryContract $user
     */
    public function __construct(UserRepositoryContract $user) {
        $this->user = $user;
    }

    public function register(RegisterRequest $request) {
        $data = $request->all();
        if (empty($data['subscribe'])) {
            $data['subscribe'] = 0;
        }
        $new_user = $this->user->create($data);

        //Add user email to mailchimp list
        if ($data['subscribe'] == 1) {
            $email_data = array(
                'email' => Request::input('email')
            );
            $mailchimp_settings = array(
                'apikey' => env('MAILCHIMP_APIKEY'),
                'list_id' => env('MAILCHIMP_REGISTER_LIST_ID'),
                'account_email' => env('MAILCHIMP_ACCOUNT_EMAIL')
            );
            $this->_ibMailchimpSubscribe($email_data, $mailchimp_settings);
        }
        //END
        //Adding email notifications
        $notification_type = Config('constant.notification_settings');
        foreach ($notification_type as $type => $input) {
            $email_setting = new EmailNotificationSetting;
            $email_setting->user_id = $new_user->id;
            $email_setting->type = $type;
            $email_setting->value = 1;
            $email_setting->save();
        }
        //END
        if (!PoolTeam::addUserToPool($new_user)) {
            $request->session()->flash('flash_danger', trans('messages.pool.unable_to_add_to_team'));
        }
        if ($request->session()->has('pool_team_invite') && !Pool::addUserToPoolTeamThroughInvite($request->session()->pull('pool_team_invite'), $new_user)) {
            $request->session()->flash('flash_danger', trans('messages.pool.unable_to_add_to_team'));
        }
        return response()->json(['message' => trans('exceptions.frontend.auth.confirmation.created_confirm')], 201);
    }

    public function login(LoginRequest $request) {
        if ($request->ajax()) {
            $user['email'] = $request['email'];
            $user['password'] = $request['password'];

            $redirect_url = route('frontend.home.index');
            if ($request->session()->has('url.intended')) {
                $redirect_url = $request->session()->pull('url.intended');
            }

            if (isset($request['remember'])) {
                $remember = true;

                if (Auth::viaRemember()) {
                    return response()->json(['url' => $redirect_url], 201);
                }
            } else {
                $remember = false;
            }

            $checkEmail = User::where('email', $user['email'])->first();
            if (!$checkEmail) {
                return response()->json(['email' => trans('auth.login.unknown_email')], 500);
            }
            $checkEmailConfirmed = User::where('email', $user['email'])->where('confirmed',1)->first();
            if (!$checkEmailConfirmed) {
                return response()->json(['email' => trans('auth.login.unconfirmed_email')], 500);
            }

            if (Auth::attempt([ 'email' => $user['email'], 'password' => $user['password']], $remember)) {
                if ($request->session()->has('pool_team_invite') && !Pool::addUserToPoolTeamThroughInvite($request->session()->pull('pool_team_invite'), $checkEmail)) {
                    $request->session()->flash('flash_danger', trans('messages.pool.unable_to_add_to_team'));
                }

                return response()->json(['url' => $redirect_url], 201);
            } else {
                return response()->json(['password' => trans('auth.login.incorrect_password')], 500);
            }
        }
    }

    public function logout() {
        Auth::logout();
        return Redirect::to('/');
    }

    public function editAccount() {
        return view('frontend.auth.edit_account', ['email_notifications' => \App\Models\EmailNotificationSetting::where('user_id', Auth::id())->get()]);
    }

    public function myAccount() {
        return view('frontend.auth.my_account');
    }

    public function editAccountSubmit(UpdateProfileRequest $request) {
        if ($request->ajax()) {
            if (Hash::check(Request::input('password'), Auth::user()->password)) {
                $user = Auth::user();
                $user->name = Request::input('name');
                $user->email = Request::input('email');
                $user->password = bcrypt(Request::input('password'));
                $user->save();

                return response()->json(['message' => trans('auth.editAccount.messages.success')], 201);
                die();
            } else {
                return response()->json(['password' => trans('auth.editAccount.messages.failure')], 500);
                die();
            }
        }
    }

    public function changePasswordSubmit(ChangePasswordRequest $request) {
        if ($request->ajax()) {
            if (Hash::check(Request::input('old_password'), Auth::user()->password)) {
                $user = Auth::user();
                $user->password = bcrypt(Request::input('password'));
                $user->save();

                return response()->json(['message' => trans('auth.changePasswordSubmit.update_successful')], 201);
                die();
            } else {
                return response()->json(['password' => trans('auth.changePasswordSubmit.wrong_password')], 500);
                die();
            }
        }
    }

    public function _ibMailchimpSubscribe($data = false, $MC_VARS) {
        $key = explode('-', $MC_VARS['apikey']);

        $api = array
            (
            'login' => $MC_VARS['account_email'],
            'key' => $MC_VARS['apikey'],
            'url' => 'https://' . $key[1] . '.api.mailchimp.com/3.0/'
        );
        $type = 'PUT';
        $target = 'lists/' . $MC_VARS['list_id'] . '/members/' . md5($data['email']);

        $ch = curl_init($api['url'] . $target);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array
            (
            'Content-Type: application/json',
            'Authorization: ' . $api['login'] . ' ' . $api['key'],
        ));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/3.0');


        $postData = array(
            "email_address" => $data['email'],
            "status" => 'subscribed',
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

}
