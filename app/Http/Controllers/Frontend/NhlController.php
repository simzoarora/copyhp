<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Competition;
use App\Models\Season;
use App\Models\Conference;
use App\Models\InjuredPlayer;
use App\Models\Team;
use App\Models\Player;
use Illuminate\Http\Request;

/**
 * Description of NhlController
 *
 * @author Anik
 */
class NhlController extends Controller {

    public function schedule($nick_name = NULL, $start_date = NULL) {
        return view('frontend.nhl.schedule', ['teams' => Team::orderBy('display_name')->lists('display_name', 'nick_name')]);
    }

    public function getScheduleData(Request $request, $nick_name = NULL, $start_date = NULL) {
//        dd($request->competition_type);
        if (\DateTime::createFromFormat('Y-m-d', $nick_name) !== FALSE) {
            /**
             *  this is done for route configuration. When we want to get a overall
             * schedule searched from date, we have to inverse the variable because 
             * date is coming in nick_name
             */
            $start_date = $nick_name;
            $nick_name = NULL;
        }
//        \DB::enableQueryLog();
        $competition_type = NULL;
        
        if ($request->competition_type >= 0) {
            $competition_type = $request->competition_type;
        }

        if ($nick_name && empty($start_date)) {
            $final_data = Competition::has('currentSeason')
                            ->where(function($query) use($nick_name) {
                                $query->whereHas('homeTeam', function($query) use($nick_name) {
                                    $query->where('nick_name', $nick_name);
                                })->orWhereHas('awayTeam', function($query) use($nick_name) {
                                    $query->where('nick_name', $nick_name);
                                });
                            })->where(function($query) use($competition_type) {
                                if ($competition_type !== NULL) {
                                    $query->where('competition_type', $competition_type);
                                }
                            })->with([
                                'homeTeam' => function($query) {
                                    $query->select('id', 'display_name', 'short_name', 'logo');
                                },
                                'awayTeam' => function($query) {
                                    $query->select('id', 'display_name', 'short_name', 'logo');
                                }
                            ])->orderBy('match_start_date')->get()->toArray();
        } else {
            $final_data = $this->_subGetSchedule($start_date,$nick_name,$competition_type);
            
        }
        return $final_data;
    }
    
    private function _subGetSchedule($start_date = NULL, $nick_name = NULL, $competition_type = NUll){
        
        $first_competition = Competition::where('venue_id', '!=', null)->where(\DB::raw('DATE(match_start_date)'), '>=', ($start_date !== NULL) ? $start_date : date('Y-m-d'))
                    ->select('match_start_date')
                    ->orderBy('match_start_date')
                    ->firstOrFail();

            $query_builder = Competition::where('venue_id', '!=', null)->whereBetween(\DB::raw('DATE(match_start_date)'), [$first_competition->match_start_date->toDateString(), $first_competition->match_start_date->addMonth()->toDateString()])
                    ->where(function($query) use($competition_type) {
                                if ($competition_type !== NULL) {
                                    $query->where('competition_type', $competition_type);
                                }
                            })
                    ->with([
                        'homeTeam' => function($query) {
                            $query->select('id', 'display_name', 'short_name', 'logo');
                        },
                        'awayTeam' => function($query) {
                            $query->select('id', 'display_name', 'short_name', 'logo');
                        }
                    ])
                    ->orderBy('match_start_date');

            return $this->_getWithDateFinalData($nick_name, $query_builder, $start_date);
    }
    
    private function _getWithDateFinalData($nick_name=NULL, $query_builder=NULL,$start_date = NULL){
        if ($nick_name !== NULL) {
                $query_builder = $query_builder->where(function($query) use($nick_name) {
                    $query->whereHas('homeTeam', function($query) use($nick_name) {
                        $query->where('nick_name', $nick_name);
                    })->orWhereHas('awayTeam', function($query) use($nick_name) {
                        $query->where('nick_name', $nick_name);
                    });
                });
            }

            $competitions = $query_builder->get();
            $final_data = [];

            if ($nick_name && $start_date) {
                $final_data = $competitions->toArray();
            } else {

                foreach ($competitions as $competition) {

                    $competition_date = $competition->match_start_date->format('l, M d');
                    if (!isset($final_data[$competition_date])) {
                        $final_data[$competition_date] = [];
                    }
                    $final_data[$competition_date][] = $competition->toArray();
                }
            }
            return $final_data;
    }

    public function standings($season_name = NULL) {
        if ($season_name != NULL) {
            $selected_season = Season::where('name', $season_name)->firstOrFail();
        } else {
            $selected_season = NULL;
        }
        return view('frontend.nhl.standings', ['seasons' => Season::orderBy('sort_order', 'asc')->lists('name', 'id'), 'selected_season' => $selected_season]);
    }

    public function getStandingData($season_name = NULL) {
//        \DB::enableQueryLog();
        if ($season_name == NULL) {
            $season_query_builder = Season::where('current', 1);
        } else {
            $season_query_builder = Season::where('name', $season_name);
        }
        $data = $season_query_builder->with([
                    'leagueStandings' => function($query) {
                        $query->orderBy('rank');
                    },
                    'leagueStandings.team',
                    'divisionStandings' => function($query) {
                        $query->orderBy('division_ranking');
                    },
                    'divisionStandings.division.conference',
                    'conferenceStandings' => function($query) {
                        $query->orderBy('conference_ranking');
                    },
                    'conferenceStandings.conference',
                    'extraLeagueStandings'
                ])->firstOrFail();
        $division_standings = [];
        foreach ($data->divisionStandings as $division_standing) {
            if (!isset($division_standings[$division_standing->division->conference->short_name][$division_standing->division->name])) {
                $division_standings[$division_standing->division->conference->short_name][$division_standing->division->name] = [];
            }
            $this_data_array = $division_standing->toArray();
            unset($this_data_array['division']);
            $division_standings[$division_standing->division->conference->short_name][$division_standing->division->name][] = $this_data_array;
        }
        $conference_standings = $data->conferenceStandings->groupBy('conference.short_name')->toArray();
        $extra_league_standings = [];
        foreach ($data->extraLeagueStandings as $standing) {
            $extra_league_standings[$standing->team_id][config('constant.sportsdirectconstant.extra_league_standings_types')[$standing->type]] = $standing->games_won . '-' . $standing->games_lost . '-' . $standing->games_lost_overtime . '-' . $standing->games_lost_shootout;
        }
        $league_standings = [];
        foreach ($data->leagueStandings as $standing) {
            $league_standings[$standing['team_id']] = $standing->toArray();
        }
        $data_array = $data->toArray();
        $data_array['league_standings'] = $league_standings;
        $data_array['division_standings'] = $division_standings;
        $data_array['conference_standings'] = $conference_standings;
        $data_array['extra_league_standings'] = $extra_league_standings;
        return $data_array;
//        dump(\DB::getQueryLog());
//        dd($data_array);
    }

    public function injuryReport() {
        return view('frontend.nhl.injury_report');
    }

    public function injuryReport2() {
        return view('frontend.nhl.injury_report2');
    }

    public function getDataForInjuryReport() {
        return InjuredPlayer::orderBy('last_updated', 'desc')->with('playerSeason.team', 'playerSeason.player')->get();
    }

    public function playerStats() {
        return view('frontend.nhl.player_stats', [
            'seasons' => Season::orderBy('name', 'desc')->lists('name', 'id')->toArray(),
            'player_positions' => \App\Models\PlayerPosition::orderBy('sort_order')->lists('name', 'id')->toArray(),
            'teams' => Team::orderBy('display_name')->lists('display_name', 'id')->toArray(),
            'stat_fields' => \App\Models\PoolScoringField::orderBy('sort_order')->lists('stat', 'id')
        ]);
    }

    public function getDataForPlayerStats(Request $request) {
//        \DB::enableQueryLog();
        $query_builder = Player::getQueryBuilderForNhlPlayerStat($request);
        $data = $query_builder->paginate(config('constant.pagination.nhl_player_stats.per_page_limit'));
        return $data;
//        dump(\DB::getQueryLog());
//        dd($data->toArray());
    }

}
