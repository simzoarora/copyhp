<?php

namespace App\Http\Controllers\Frontend;

use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use LRedis;
use App\Models\Season;
use App\Models\PlayerPosition;
use App\Models\Team;
use App\Models\Pool;
use App\Models\LiveDraftMessage;
use App\Models\PoolRoster;
use App\Models\PoolTeamPlayer;
use App\Models\LiveDraftPlayer;
use App\Models\LiveDraftTeamSetting;
use App\Services\LiveDraftService;

class LiveDraftsController extends Controller
{

    public function index($pool_id)
    {
        if (!Pool::checkPoolActive($pool_id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id]);
        }

        $pool_details = Pool::where('id', $pool_id)
                ->completed()
                ->active()
                ->has('poolTeam')
                ->with([
                    'poolScoreSettings.poolScoringField',
                    'poolTeam.liveDraftTeamSetting',
                    'poolSetting.poolsetting',
                    'liveDraftSetting'
                ])
                ->firstOrFail();
        if ($pool_details->liveDraftSetting == null) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id])->withFlashDanger(trans('messages.live_draft.not_enabled'));
        } else {
            $date = LiveDraftService::convertStartTimeToSystemTime($pool_details->liveDraftSetting);
            $back_15_minutes_date = new \DateTime("now +15 minutes");
            if ($date > $back_15_minutes_date) {
                return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id])->withFlashDanger(trans('messages.live_draft.only_15_minutes_before'));
            }
        }

        if (!LiveDraftPlayer::where('pool_id', $pool_id)->whereNull('player_season_id')->exists()) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id])->withFlashDanger(trans('messages.live_draft.finished'));
        }
        return view('frontend.live_draft.index', [
            'current_season' => Season::current()->firstOrFail(),
            'seasons' => Season::orderBy('sort_order')->get(),
            'positions' => PlayerPosition::getDataForSelectBox(),
            'teams' => Team::getDataForSelectBox(),
            'pool_details' => $pool_details,
            'encryptedPoolId' => "live-draft-chat-" . JWT::encode((int) $pool_id, env('JWT_KEY')),
            'socket_channel' => "live-draft-" . JWT::encode((int) $pool_id, env('JWT_KEY')),
            'socket_channel_autoselect' => "live-draft-autoselection-" . JWT::encode((int) $pool_id, env('JWT_KEY')),
        ]);
    }

    public function getTeamsTabData($pool_id)
    {
        return LiveDraftService::buildDataForTeamTab(Pool::getDataOfRoundsForLiveDraft($pool_id));
    }

    public function getDraftResultsTabData($pool_id)
    {
        return LiveDraftService::buildDataForDraftResultsTab(Pool::getDataOfRoundsForLiveDraft($pool_id));
    }

    public function getMyTeam($pool_id)
    {
        $pool_rosters = PoolRoster::where('pool_id', $pool_id)
                ->leftJoin('player_positions', 'pool_rosters.player_position_id', '=', 'player_positions.id')
                ->orderBy('player_positions.sort_order')
                ->select('pool_rosters.*')
                ->with('playerPosition')
                ->get();
        $pool_team_players = PoolTeamPlayer::getDataForMyTeamInLiveDraft($pool_id);
        $built_array = [];
        foreach ($pool_rosters as $pool_roster) {
            $position_players = $pool_team_players->where('poolTeamLineup.player_position_id', $pool_roster->player_position_id);
            for ($i = 0; $i < $pool_roster->value; $i++) {
                $built_array[] = [
                    'position' => $pool_roster->playerPosition->short_name,
                    'pool_team_player' => $position_players->shift()
                ];
            }
        }
        return $built_array;
    }

    /**
     * @param Request $request
     */
    public function draftPlayer(Request $request, $pool_id)
    {
        $data = $request->all();
        DB::beginTransaction();
        try {
            if (PoolTeamPlayer::checkIfPlayerAlreadyExistInPool($pool_id, $data['player_season_id'])) {
                return response()->json(array('message' => trans('messages.live_draft.player_already_drafted')), 500);
            }
            $live_draft_player = LiveDraftPlayer::getPlayerDataForDrafting($pool_id);
            $pool = Pool::getDataForLiveDrafting($pool_id);

            LiveDraftService::draftPlayer($pool, $live_draft_player, $data['player_season_id']);

            DB::commit();
            return response()->json(array('message' => trans('messages.live_draft.player_drafted')), 200);
        } catch (Exception $ex) {
            $this->_logWithPoolAndUser($pool_id, $ex);
            DB::rollback();
            return response()->json(array('message' => $ex->getMessage()), 500);
        } catch (ModelNotFoundException $ex) {
            $this->_logWithPoolAndUser($pool_id, $ex);
            DB::rollback();
            return response()->json(array('message' => trans('messages.error.cant_save')), 500);
        }
        DB::rollback();
        return response()->json(array('message' => trans('messages.error.cant_save')), 500);
    }

    private function _logWithPoolAndUser($pool_id, $ex)
    {
        \Log::alert("LiveDraft:draftPlayer, pool:$pool_id, user:" . Auth::id() . "  : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
    }

    public function getDraftTiming($pool_id)
    {
        return LiveDraftPlayer::getDataForDraftTimings($pool_id);
    }

    public function changeAutomaticSelection(Request $request, $pool_id)
    {
        $pool = Pool::active()->where('id', $pool_id)->has('poolTeam')->with('poolTeam')->firstOrFail();

        $live_draft_team_setting = LiveDraftTeamSetting::firstOrNew([
                    'pool_team_id' => $pool->poolTeam->id
        ]);
        $live_draft_team_setting->automatic_selection = $request->automatic_selection;
        if ($live_draft_team_setting->save()) {
            return response()->json(array('message' => trans('messages.success.data_saved')), 200);
        } else {
            return response()->json(array('message' => trans('messages.error.cant_save')), 500);
        }
    }

    public function preDraft($pool_id)
    {
        if (!Pool::checkPoolActive($pool_id)) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id]);
        }

        $pool_details = Pool::where('id', $pool_id)
                ->completed()
                ->active()
                ->has('poolTeam')
                ->with([
                    'poolScoreSettings.poolScoringField',
                    'poolTeam.liveDraftTeamSetting',
                    'poolSetting.poolsetting',
                    'liveDraftSetting'
                ])
                ->firstOrFail();
        if ($pool_details->liveDraftSetting == null) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id])->withFlashDanger(trans('messages.live_draft.not_enabled'));
        }

        if (!LiveDraftPlayer::where('pool_id', $pool_id)->whereNull('player_season_id')->exists()) {
            return redirect()->route('frontend.pool.dashboard', ['pool_id' => $pool_id])->withFlashDanger(trans('messages.live_draft.finished'));
        }
        return view('frontend.live_draft.pre_draft', [
            'current_season' => Season::current()->firstOrFail(),
            'seasons' => Season::orderBy('sort_order')->get(),
            'positions' => PlayerPosition::getDataForSelectBox(),
            'teams' => Team::getDataForSelectBox(),
            'pool_details' => $pool_details,
            'user_pools' => Pool::getUserPoolsList(),
        ]);
    }

    public function getLiveDraftMessages(Request $request)
    {
        return LiveDraftMessage::where('pool_id', $request->pool_id)
                        ->orderBy('created_at', 'desc')
                        ->with([
                            'user.poolTeam' => function($query) use($request) {
                                $query->where('pool_id', $request->pool_id);
                            }
                        ])
                        ->paginate(10);
    }

    public function saveLiveDraftMessages(Request $request)
    {
        $data = $request->all();
        $message = new LiveDraftMessage();
        $message->sender_user_id = Auth::id();
        $message->pool_id = $data['pool_id'];
        $message->message = $data['message_text'];
        if ($message->save()) {
            //publishing messages via Redis
            $redis = LRedis::connection();
            $broadCastingMessage['user'] = \App\Models\PoolTeam::where('user_id', Auth::id())->where('pool_id', $data['pool_id'])->first(['name']);
            $broadCastingMessage['message'] = $message->message;
            $redis->publish("live-draft-chat-" . JWT::encode((int) $message->pool_id, env('JWT_KEY')), json_encode($broadCastingMessage));
            //End publishing messages via Redis
            return response()->json(['message' => 'Saved'], 200);
        } else {
            return response()->json(['message' => 'Error'], 500);
        }
    }
}