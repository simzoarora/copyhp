<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use App\Models\Season;
use App\Models\Team;
use App\Models\City;
use App\Models\Player;
use App\Models\PlayerPosition;
use App\Models\Competition;
use App\Models\StoredValue;
use App\Models\PlayerSeason;
use App\Models\InjuredPlayer;
use App\Models\Conference;
use App\Models\Division;
use SportsDirect\NHL;
use DB;
use LRedis;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Description of IndexController
 *
 * @author Anik
 */
class IndexController extends Controller
{

    public function fetchInjuredPlayer()
    {
        try {
            $stored_value = StoredValue::where('key', 'injury_feed_update_time')->firstOrFail();
            $nhl = new NHL(\Config('sportsdirectconfig.api_key'));
            $feed = $nhl->getInjuryFeed(date('Y-m-d\TH:i:s.uP', strtotime($stored_value->value)));
            if ($feed) {
                list($injury_feed, $atom_feed) = $feed;
                $season_api_id = str_replace(config('constant.sportsdirectconstant.season_id_prefix'), '', $injury_feed['team-sport-content']['league-content']['season-content']['season']['id']);
                $season = Season::where('api_id', $season_api_id)
                        ->with([
                            'players' => function($query) {
                                $query->whereHas('currentPlayerSeason', function($query) {
                                            $query->has('injuredPlayer');
                                        });
                            },
                            'players.currentPlayerSeason.injuredPlayer'
                        ])
                        ->firstOrFail();
                $saved_injured_players = [];
                if (!$season->players->isEmpty()) {
                    foreach ($season->players as $player) {
                        if ($player->currentPlayerSeason->injuredPlayer != null) {
                            $saved_injured_players[] = $player->currentPlayerSeason->injuredPlayer->api_id;
                        }
                    }
                }
                $injury_id_from_api = [];
                foreach ($injury_feed['team-sport-content']['league-content']['season-content']['team-content'] as $team) {
                    if (isset($team['player-content'])) {
                        try {
                            if (isset($team['player-content'][0])) {
                                foreach ($team['player-content'] as $player_content) {
                                    $injured_player = InjuredPlayer::saveFromApi($season, $team, $player_content);
                                    if ($injured_player) {
                                        $injury_id_from_api[] = $injured_player->api_id;
                                    }
                                }
                            } else {
                                $injured_player = InjuredPlayer::saveFromApi($season, $team, $team['player-content']);
                                if ($injured_player) {
                                    $injury_id_from_api[] = $injured_player->api_id;
                                }
                            }
                        } catch (Exception $e) {
                            continue;
                        }
                    }
                }
                $fit_players = array_diff($saved_injured_players, $injury_id_from_api);
                if (!empty($fit_players)) {
                    InjuredPlayer::whereIn('api_id', $fit_players)->delete();
                }
                $stored_value->value = date('Y-m-d H:i:s', strtotime($atom_feed['updated'] . ' +1 second'));
                $stored_value->save();
            }
        } catch (\Exception $ex) {
            \Log::alert("fetchInjuredPlayer : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }

    private function _processAndSaveTeam($entry)
    {
        $client = new \GuzzleHttp\Client();
        $season_data = json_decode(json_encode(new \SimpleXMLElement($client->request('GET', $entry['id'])->getBody())), 1);
        $current_season = Season::sportsDirectSaveOrGetSeason($season_data['team-sport-content']['league-content']['season-content']['season']);
        foreach ($season_data['team-sport-content']['league-content']['season-content']['conference-content'] as $conference_content) {
            $conference = Conference::sportsDirectSaveOrGetConference($conference_content['conference']);
            foreach ($conference_content['division-content'] as $division_content) {
                $division = Division::sportsDirectSaveOrGetConference($division_content['division'], $conference->id);
                foreach ($division_content['team-content'] as $team_content) {
                    $team_id = str_replace(\Config('constant.sportsdirectconstant.team_id_prefix'), '', $team_content['team']['id']);
                    $already_exist = Team::where('api_id', $team_id)->first();
                    if ($already_exist) {
                        if (!$current_season->teams->contains($already_exist->id)) {
                            $current_season->teams()->attach($already_exist->id, ['conference_id' => $conference->id, 'division_id' => $division->id]);
                        } else {
                            $current_season->teams()->updateExistingPivot($already_exist->id, ['conference_id' => $conference->id, 'division_id' => $division->id]);
                        }
                    } else {
                        $city = City::findOrCreate($team_content['team']['location']['city'], $team_content['team']['location']['state'], $team_content['team']['location']['country']);
                        if ($city) {
                            $sdata = array(
                                'api_id' => $team_id,
                                'display_name' => $team_content['team']['name'][0],
                                'first_name' => $team_content['team']['name'][1],
                                'nick_name' => $team_content['team']['name'][2],
                                'short_name' => $team_content['team']['name'][3],
                                'city_id' => $city->id,
                            );
                            $current_season->teams()->create($sdata);
                        }
                    }
                }
            }
        }
    }

    public function fetchSeasonTeams()
    {
        try {
            $stored_value = StoredValue::where('key', 'team_feed_update_time')->firstOrFail();
            $nhl = new NHL(\Config('sportsdirectconfig.api_key'));
            $atom_feed = $nhl->getTeamsAtomFeed(date('Y-m-d\TH:i:s.uP', strtotime($stored_value->value)));

            if (isset($atom_feed['entry']) && !empty($atom_feed['entry'])) {
                if (!isset($atom_feed['entry'][0])) {
                    $this->_processAndSaveTeam($atom_feed['entry']);
                } else {
                    foreach ($atom_feed['entry'] as $key => $entry) {
                        $this->_processAndSaveTeam($entry);
                    }
                }
            }
            $stored_value->value = date('Y-m-d H:i:s', strtotime($atom_feed['updated'] . ' +1 second'));
            $stored_value->save();
        } catch (\Exception $ex) {
            \Log::alert("fetchSeasonTeams : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }

    private function _processAndSavePlayers($entry)
    {
        $client = new \GuzzleHttp\Client();
        $team_player_data = json_decode(json_encode(new \SimpleXMLElement($client->request('GET', $entry['id'])->getBody())), 1);
        if (isset($team_player_data['team-sport-content']['league-content']['season-content']['team-content']['meta']['property']) && $team_player_data['team-sport-content']['league-content']['season-content']['team-content']['meta']['property'] == 'all-star') {
            return;
        }
        $current_season = Season::sportsDirectSaveOrGetSeason($team_player_data['team-sport-content']['league-content']['season-content']['season']);
        if (!isset($team_player_data['team-sport-content']['league-content']['season-content']['team-content']['team']['id'])) {
            \Log::alert('fetchSeasonTeamPlayers : Team Not Found : ' . json_encode($team_player_data));
            return;
        }
        $team = Team::getTeamFromApiIdString($team_player_data['team-sport-content']['league-content']['season-content']['team-content']['team']['id']);
        $team->load(['players']);
        foreach ($team_player_data['team-sport-content']['league-content']['season-content']['team-content']['player-content'] as $player) {
            $player_id = str_replace(\Config('constant.sportsdirectconstant.player_id_prefix'), '', $player['player']['id']);
            $player_position = PlayerPosition::where('short_name', $player['player']['season-details']['position']['name'][1])->first();
            $db_player = Player::firstOrNew(['api_id' => $player_id]);
            if (isset($player['player']['location'])) {
                $city = City::findOrCreate($player['player']['location']['city'], (isset($player['player']['location']['state']) ? $player['player']['location']['state'] : NULL), (isset($player['player']['location']['country']) ? $player['player']['location']['country'] : NULL));
            } else {
                $city = NULL;
            }
            $db_player->api_id = $player_id;
            $db_player->first_name = $player['player']['name'][0];
            $db_player->last_name = $player['player']['name'][1];
            $db_player->height = (isset($player['player']['height'])) ? $player['player']['height'] : NULL;
            $db_player->weight = (isset($player['player']['weight'])) ? $player['player']['weight'] : NULL;
            $db_player->dob = (isset($player['player']['birthdate'])) ? $player['player']['birthdate'] : NULL;
            $db_player->city_id = ($city) ? $city->id : NULL;
            $db_player->save();
            $this->_attachPlayerToSeason($current_season, $db_player->id, $team, $player, $player_position);
        }
        return;
    }

    public function fetchSeasonTeamPlayers()
    {
        try {
            ini_set('max_execution_time', 0);
            ini_set('memory_limit', 268435456); // 256 MB because first time on this script will demand lot of memory
            $stored_value = StoredValue::where('key', 'player_feed_update_time')->firstOrFail();
            $nhl = new NHL(\Config('sportsdirectconfig.api_key'));
            $atom_feed = $nhl->getPlayersAtomFeed(date('Y-m-d\TH:i:s.uP', strtotime($stored_value->value)));


            if (isset($atom_feed['entry']) && !empty($atom_feed['entry'])) {
                if (!isset($atom_feed['entry'][0])) {
                    $this->_processAndSavePlayers($atom_feed['entry']);
                } else {
                    foreach ($atom_feed['entry'] as $key => $entry) {
                        $this->_processAndSavePlayers($entry);
                    }
                }
            }
            $stored_value->value = date('Y-m-d H:i:s', strtotime($atom_feed['updated'] . ' +1 second'));
            $stored_value->save();
        } catch (\Exception $ex) {
            \Log::alert("fetchSeasonTeamPlayers : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }

    private function _processAndSaveDraftPlayers($entry)
    {
        $client = new \GuzzleHttp\Client();
        $team_player_data = json_decode(json_encode(new \SimpleXMLElement($client->request('GET', $entry['id'])->getBody())), 1);
        $current_season = Season::sportsDirectSaveOrGetSeason($team_player_data['team-sport-content']['league-content']['season-content']['season']);
        foreach ($team_player_data['team-sport-content']['league-content']['season-content']['draft']['round'] as $round) {
            if (!isset($round['selection'])) {
                continue;
            }
            foreach ($round['selection'] as $player) {
                if (isset($player['team']) && isset($player['position'])) {
                    $team = Team::getTeamFromApiIdString($player['team']['id']);
                    $team->load(['players']);

                    $player_id = str_replace(\Config('constant.sportsdirectconstant.player_id_prefix'), '', $player['player']['id']);
                    $player_position = PlayerPosition::where('short_name', $player['position']['name'][1])->first();
                    $db_player = Player::firstOrNew(['api_id' => $player_id]);
                    if (isset($player['player']['location'])) {
                        $city = City::findOrCreate($player['player']['location']['city'], (isset($player['player']['location']['state']) ? $player['player']['location']['state'] : NULL), (isset($player['player']['location']['country']) ? $player['player']['location']['country'] : NULL));
                    } else {
                        $city = NULL;
                    }
                    $db_player->api_id = $player_id;
                    $db_player->first_name = $player['player']['name'][0];
                    $db_player->last_name = $player['player']['name'][1];
                    $db_player->city_id = ($city) ? $city->id : NULL;
                    $db_player->save();
                    if (!$current_season->players->contains($db_player->id) || !$team->players->contains($db_player->id)) {
                        $this->_attachPlayerToSeason($current_season, $db_player->id, $team, $player, $player_position, TRUE);
                    }
                } else {
                    \Log::alert('FETCH DRAFT PLAYER : full data not there' . json_encode($player));
                }
            }
        }
        return;
    }

    public function fetchDraftPlayers()
    {
        try {
            ini_set('max_execution_time', 0);
            ini_set('memory_limit', 268435456); // 256 MB because first time on this script will demand lot of memory
            $stored_value = StoredValue::where('key', 'player_draft_feed_update_time')->firstOrFail();
            $nhl = new NHL(\Config('sportsdirectconfig.api_key'));
            $atom_feed = $nhl->getDrafterPlayerAtomFeed(date('Y-m-d\TH:i:s.uP', strtotime($stored_value->value)));


            if (isset($atom_feed['entry']) && !empty($atom_feed['entry'])) {
                if (!isset($atom_feed['entry'][0])) {
                    $this->_processAndSaveDraftPlayers($atom_feed['entry']);
                } else {
                    foreach ($atom_feed['entry'] as $key => $entry) {
                        $this->_processAndSaveDraftPlayers($entry);
                    }
                }
            }
            $stored_value->value = date('Y-m-d H:i:s', strtotime($atom_feed['updated'] . ' +1 second'));
            $stored_value->save();
        } catch (\Exception $ex) {
            \Log::alert("fetchDraftPlayers : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }

    /**
     * Original attach function of laravel cant be used in this as we need to update the extra data every time data is fetched
     * @param type $current_season
     * @param type $attach_id
     * @param type $teams
     * @param type $player
     * @param type $player_position
     */
    private function _attachPlayerToSeason($current_season, $attach_id, $teams, $player, $player_position, $draft = FALSE)
    {
        if ($draft || $player['player']['season-details']['active'] == 'true') {
            // first delete entry of player for different team of current season
            PlayerSeason::where('player_id', $attach_id)->where('season_id', $current_season->id)->where('team_id', '<>', $teams->id)->where('is_active', 1)->delete();
        }
        $player_season = PlayerSeason::firstOrNew(['player_id' => $attach_id, 'season_id' => $current_season->id, 'team_id' => $teams->id]);
        $player_season->player_position_id = $player_position->id;
        if ($draft) {
            $player_season->is_active = 1;
            $player_season->is_draft = 1;
        } else {
            $player_season->number = (isset($player['player']['season-details']['number'])) ? $player['player']['season-details']['number'] : NULL;
            $player_season->is_active = ($player['player']['season-details']['active'] == 'true') ? 1 : 0;
            $player_season->salary = (isset($player['player']['season-details']['salary']) ? $player['player']['season-details']['salary'] : NULL);
        }
        $player_season->save();
    }

    public function fetchBoxScore()
    {
        try {
            $nhl = new NHL(\Config('sportsdirectconfig.api_key'));
            $stored_value = StoredValue::where('key', 'box_score_update_time')->firstOrFail();
            $box_score = $nhl->getBoxScoreAtomFeed(date('Y-m-d\TH:i:s.uP', strtotime($stored_value->value)));
            if ($box_score && isset($box_score['entry'])) {
                if (isset($box_score['entry'][0])) {
                    foreach ($box_score['entry'] as $entry) {
                        $this->_processBoxScore($entry);
                    }
                } else {
                    $this->_processBoxScore($box_score['entry']);
                }
                $stored_value->value = date('Y-m-d H:i:s', strtotime($box_score['updated'] . ' +1 second'));
                $stored_value->save();
            }
        } catch (\Exception $ex) {
            \Log::alert("fetchBoxScore : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }

    private function _processBoxScore($entry)
    {
        $box_score_link = $entry['link']['@attributes']['href'];
        preg_match("/boxscore_NHL_(\\d*).xml/", $box_score_link, $matches);
        $competition_id = $matches[1];
        $competition = Competition::where('api_id', $competition_id)->with('homeTeam', 'awayTeam')->first();
        if ($competition) {
            $competition_box_score = json_decode(json_encode(new \SimpleXMLElement($box_score_link, 0, true)), 1);
            foreach (['home', 'away'] as $key_for_team) {
                $api_team_id = $competition->{$key_for_team . 'Team'}->api_id;
                $this->_saveTeamStats($competition_box_score, $competition, $key_for_team . "-team-content", $competition->{$key_for_team . '_team_id'});
                $this->_savePlayerContent($competition_box_score, $competition, $key_for_team . "-team-content", $api_team_id);
                $this->_prepareCompetitionGoals($competition_box_score, $competition, $key_for_team . "-team-content", $api_team_id);
                $this->_prepareCompetitionPenalty($competition_box_score, $competition, $key_for_team . "-team-content", $api_team_id);
                if ($key_for_team == 'home') {
                    $this->_prepareCompetitionShootout(
                            $competition_box_score, $competition, $key_for_team . "-team-content", $api_team_id, $competition->awayTeam->api_id
                    );
                } else {
                    $this->_prepareCompetitionShootout(
                            $competition_box_score, $competition, $key_for_team . "-team-content", $api_team_id, $competition->homeTeam->api_id
                    );
                }
            }
            if ($competition->type == \Config('competition.inverse_type.current')) {
                $redis = LRedis::connection();
                $redis->publish('BoxScore', json_encode(Competition::getCompetitionFullStats($competition->id)));
            }
        }
    }

    private function _prepareCompetitionShootout($competition_box_score, &$competition, $key_for_team, $api_team_id, $goalie_api_team_id)
    {
        if (isset($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['events']['shootout-result'])) {
            foreach ($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['events']['shootout-result'] as $shootout) {
                $api_id = str_replace(\Config('constant.sportsdirectconstant.shootout_id_prefix'), "", $shootout['id']);
                $competition_shootout = $competition->competitionShootouts()->firstOrNew(array('competition_id' => $competition->id, 'api_id' => $api_id));
                $competition_shootout->api_id = $api_id;
                $competition_shootout->order = $shootout['order'];
                $competition_shootout->scored = ($shootout["scored"] == 'false') ? 0 : 1;
                foreach ($shootout['player-role-grouping'] as $player) {
                    if ($player['player-role']['id'] == \Config('constant.sportsdirectconstant.player_role_id_prefix') . \Config('constant.sportsdirectconstant.inverse_player_role_ids.goalie')) {
                        $db_player = Player::getplayerForBoxScore($player['player']['id'], $goalie_api_team_id);
                        if ($db_player->onlyCurrentPlayerSeason == null) {
                            $this->_logShootOutError($shootout, $player, $competition);
                            continue;
                        }
                        $competition_shootout->goalie_id = $db_player->onlyCurrentPlayerSeason->id;
                    } else {
                        $db_player = Player::getplayerForBoxScore($player['player']['id'], $api_team_id);
                        if ($db_player->onlyCurrentPlayerSeason == null) {
                            $this->_logShootOutError($shootout, $player, $competition);
                            continue;
                        }
                        $competition_shootout->skater_id = $db_player->onlyCurrentPlayerSeason->id;
                    }
                }
                $competition_shootout->save();
            }
        }
    }

    private function _logShootOutError($shootout, $player, $competition)
    {
        \Log::alert('fetchBoxScore : Player Not Found In Current Season while saving shootout : ' . json_encode($shootout) . ' : with player :' . json_encode($player) . ' : with competition :' . json_encode($competition));
    }

    private function _prepareCompetitionPenalty($competition_box_score, &$competition, $key_for_team, $api_team_id)
    {
        if (!isset($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['events']['penalty'])) {
            return;
        }
        if (isset($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['events']['penalty'][0])) {
            foreach ($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['events']['penalty'] as $penalty) {
                $this->_saveCompetitionPenalty($penalty, $competition, $api_team_id);
            }
        } else {
            $this->_saveCompetitionPenalty($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['events']['penalty'], $competition, $api_team_id);
        }
    }

    private function _saveCompetitionPenalty($penalty, &$competition, $api_team_id)
    {
        if ($penalty['team-penalty'] == 'false') { // not a boolean check as string is coming
            $api_id = str_replace(\Config('constant.sportsdirectconstant.penalty_id_prefix'), "", $penalty['id']);
            $player = Player::getplayerForBoxScore($penalty['player-role-grouping']['player']['id'], $api_team_id);
            if ($player->onlyCurrentPlayerSeason == null) {
                \Log::alert('fetchBoxScore : Player Not Found In Current Season while saving penalty : ' . json_encode($penalty) . ' : with competition :' . json_encode($competition));
                return;
            }
            $competition_penalty = $competition->competitionPenalties()->firstOrNew(array('competition_id' => $competition->id, 'api_id' => $api_id));
            $competition_penalty->api_id = $api_id;
            $competition_penalty->player_season_id = $player->onlyCurrentPlayerSeason->id;
            $competition_penalty->period = $penalty["competition-event-time"]["period-number"];
            $competition_penalty->penalty_duration = $penalty["minutes"];
            $competition_penalty->penalty_time = $penalty["competition-event-time"]["time"];
            $competition_penalty->penalty_type = $penalty["penalty-type"]["name"];
            $competition_penalty->team_penalty = ($penalty["team-penalty"] == 'false') ? 0 : 1;
            $competition_penalty->save();
        }
    }

    private function _prepareCompetitionGoals($competition_box_score, &$competition, $key_for_team, $api_team_id)
    {
        if (!isset($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['events']['goal'])) {
            return;
        }
        if (isset($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['events']['goal'][0])) {
            foreach ($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['events']['goal'] as $goal) {
                $this->_saveCompetitionGoals($goal, $competition, $api_team_id);
            }
        } elseif (isset($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['events']['goal'])) {
            $this->_saveCompetitionGoals($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['events']['goal'], $competition, $api_team_id);
        }
    }

    private function _saveCompetitionGoals($goal, &$competition, $api_team_id)
    {
        $api_id = str_replace(\Config('constant.sportsdirectconstant.goal_id_prefix'), "", $goal['id']);
        $competition_goal = $competition->competitionGoals()->firstOrNew(array('competition_id' => $competition->id, 'api_id' => $api_id));
        $competition_goal->api_id = $api_id;
        $competition_goal->period = $goal["competition-event-time"]["period-number"];
        $competition_goal->goal_time = $goal["competition-event-time"]["time"];
        $competition_goal->scoring_type = $goal["scoring-type"]["name"];
        $competition_goal->goal_timestamp = date('Y-m-d H:i:s', strtotime($goal['timestamp']));
        $first_assist = false;
        $second_assist = false;
        if (isset($goal["player-role-grouping"][0])) {
            foreach ($goal["player-role-grouping"] as $player_role) {
                $this->_addGoalRole($player_role, $competition_goal, $first_assist, $second_assist, $api_team_id);
            }
        } else {
            $player_role = $goal["player-role-grouping"];
            $this->_addGoalRole($player_role, $competition_goal, $first_assist, $second_assist, $api_team_id);
        }
        if (!$second_assist) {
            $competition_goal->second_assisted_by = NULL;
        }
        if (!$first_assist) {
            $competition_goal->first_assisted_by = NULL;
        }
        $competition_goal->save();
    }

    private function _addGoalRole($player_role, &$competition_goal, &$first_assist, &$second_assist, $api_team_id)
    {
        $player = Player::getplayerForBoxScore($player_role['player']['id'], $api_team_id);
        if ($player->onlyCurrentPlayerSeason == null) {
            \Log::alert('fetchBoxScore : Player Not Found In Current Season while saving goal : ' . json_encode($player_role) . ' : with competition goal :' . json_encode($competition_goal));
            return;
        }
//Scorrer
        if ($player_role["player-role"]["id"] == "/sport/hockey/player-role:1") {
            $competition_goal->scorer_id = $player->onlyCurrentPlayerSeason->id;
        } elseif (!$first_assist) {
            $competition_goal->first_assisted_by = $player->onlyCurrentPlayerSeason->id;
            $first_assist = TRUE;
        } else {
            $competition_goal->second_assisted_by = $player->onlyCurrentPlayerSeason->id;
            $second_assist = TRUE;
        }
    }

    private function _saveTeamStats($competition_box_score, &$competition, $key_for_team, $team_id)
    {
        if (!isset($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['stat-group'])) {
            return;
        }
        $competition_team_stats = $competition->competitionTeamStats()->firstOrNew(array('competition_id' => $competition->id, 'team_id' => $team_id));
        $team_content = $competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['stat-group'];
        if (isset($team_content[0])) {
            foreach ($team_content as $stats_group) {
                $this->_saveTeamStatWithPreparation($stats_group, $competition_team_stats, $team_id);
            }
        } else {
            $this->_saveTeamStatWithPreparation($team_content, $competition_team_stats, $team_id);
        }
    }

    private function _saveTeamStatWithPreparation($stats_group, &$competition_team_stats, $team_id)
    {
        if (isset($stats_group['key']) && $stats_group['key'] == "game-stats") {
            $competition_team_stats->team_id = $team_id;
            foreach ($stats_group['stat'] as $stat) {
                if (!isset($stat['@attributes'])) {
                    continue;
                }
                $competition_team_stats->{$stat['@attributes']['type']} = $stat['@attributes']['num'];
            }
            $competition_team_stats->save();
        }
    }

    private function _savePlayerContent($competition_box_score, &$competition, $key_for_team, $api_team_id)
    {
        if (!isset($competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team]['player-content'])) {
            return;
        }
        $team_data = $competition_box_score['team-sport-content']['league-content']['season-content']['competition'][$key_for_team];
        foreach ($team_data['player-content'] as $player_content) {
            $db_player = Player::getplayerForBoxScore($player_content['player']['id'], $api_team_id);
            if ($db_player == null) {
                \Log::alert('fetchBoxScore : Player Not Found In Current Season : ' . json_encode($player_content) . ' : with competition :' . json_encode($competition));
                continue;
            }

            $competition_goalie_stats_fields = DB::getSchemaBuilder()->getColumnListing('competition_goalie_stats');
            if ($db_player->onlyCurrentPlayerSeason == null) {
                \Log::alert('fetchBoxScore : Player Not Found In Current Season : ' . json_encode($player_content) . ' : with competition :' . json_encode($competition));
                continue;
            }
            $player_skater_stats = $competition->competitionSkaterStats()->firstOrNew(array('competition_id' => $competition->id, 'player_season_id' => $db_player->onlyCurrentPlayerSeason->id));
            $player_golie_stats = $competition->competitionGoalieStats()->firstOrNew(array('competition_id' => $competition->id, 'player_season_id' => $db_player->onlyCurrentPlayerSeason->id));
            $save_goalie = false;
            if (isset($player_content['stat-group']['stat'])) {
                if (isset($player_content['stat-group']['stat'][0])) {
                    foreach ($player_content['stat-group']['stat'] as $stat) {
                        $this->_prepareStat($stat, $competition_goalie_stats_fields, $player_golie_stats, $player_skater_stats, $save_goalie);
                    }
                } else {
                    $stat = $player_content['stat-group']['stat'];
                    $this->_prepareStat($stat, $competition_goalie_stats_fields, $player_golie_stats, $player_skater_stats, $save_goalie);
                }

                $player_skater_stats->save();
                if ($save_goalie) {
                    $player_golie_stats->save();
                }
            } else {
                $player_skater_stats->save();
                if ($db_player->onlyCurrentPlayerSeason->player_position_id == config('pool.inverse_player_position.g')) {
                    $player_golie_stats->save();
                }
            }
        }
    }

    private function _prepareStat($stat, $competition_goalie_stats_fields, &$player_golie_stats, &$player_skater_stats, &$save_goalie)
    {
        if (in_array($stat['@attributes']['type'], $competition_goalie_stats_fields)) {
            $player_golie_stats->{$stat['@attributes']['type']} = $stat['@attributes']['num'];
            $save_goalie = true;
        } else {
            $player_skater_stats->{$stat['@attributes']['type']} = $stat['@attributes']['num'];
        }
    }
}