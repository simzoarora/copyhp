<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use DB;
use App\Models\PoolWaiver;
use App\Models\PoolTrade;
use App\Models\PoolTeamPlayer;
use App\Models\Notification;
use App\Models\PoolTradeVoting;
use App\Models\Pool;

/**
 * Description of PoolTeamsController
 *
 * @author Anik
 */
class PoolTeamsController extends Controller {

    public function processTrades()
    {
        try {
            $pool_trades = PoolTrade::where('status', '=', config('pool.pool_trades.inverse_status.no_action'))
                    ->where('is_processed', '=', 0)
                    ->with([
                        'parentPoolTeam.pool',
                        'tradingPoolTeam',
                    ])
                    ->where('deadline', '<', date('Y-m-d H:i:s'))
                    ->get();
            $pool_trade_accepted = config('pool.pool_trades.inverse_status.accepted');
            $pool_trade_rejected = config('pool.pool_trades.inverse_status.rejected');
            foreach ($pool_trades as $pool_trade) {

                $pool_trade->status = config('pool.pool_trades.inverse_status.auto_rejected');
                $pool = Pool::active()->where('id', $pool_trade->parentPoolTeam->pool_id)->with('poolSetting.poolsetting')->first();

                if ($pool->poolSetting->poolsetting->trade_management == config('pool.pool_trade_managemnet_settings_inverse.league_votes') && $pool_trade->pool_trade_setting_based_status == config('pool.pool_trades.pool_trade_setting_based_status.accepted')) {
                    $trade_voting = PoolTradeVoting::where('pool_trade_id', $pool_trade->id)->select('status', DB::raw('count(*) as total'))->groupBy('status')->lists('total', 'status');
                    if ((isset($trade_voting[$pool_trade_accepted]) && isset($trade_voting[$pool_trade_rejected]) && ($trade_voting[$pool_trade_rejected] > $trade_voting[$pool_trade_accepted])) || (isset($trade_voting[$pool_trade_rejected]) && !isset($trade_voting[$pool_trade_accepted]))) {
                        $pool_trade->status = $pool_trade_rejected;
                    } else {
                        $pool_trade->status = $pool_trade_accepted;
                    }
                } elseif ($pool->poolSetting->poolsetting->trade_management == config('pool.pool_trade_managemnet_settings_inverse.commissioner_decides') && $pool_trade->pool_trade_setting_based_status == config('pool.pool_trades.pool_trade_setting_based_status.accepted')) {
                    $pool_trade->status = $pool_trade_accepted;
                }

                $pool_trade->is_processed = 1;
                $pool_trade->status_change_at = date('Y-m-d H:i:s');

                DB::beginTransaction();
                if ($pool_trade->save()) {
                    $trading_notification_title = $pool_trade->parentPoolTeam->pool->name . ' - ' . $pool_trade->parentPoolTeam->name;
                    $trading_notification_link = route('frontend.pool.myTeam', ['pool_id' => $pool_trade->parentPoolTeam->pool->id]);
                    $error = FALSE;
                    if ($pool_trade->status == $pool_trade_accepted) {
                        $pool_trade_accept = PoolTrade::poolTradeAccept($pool_trade);
                        if (isset($pool_trade_accept['message'])) {
                            DB::rollBack();
                            $error = TRUE;
                            Log::error('POOL TRADE ERROR :' . $pool_trade_accept['message'] . ' : ' . $pool_trade->id);
                        } else {
                            $trading_notification_msg = trans('notifications.trade_accepted', ['Team' => $pool_trade->tradingPoolTeam->name]);
                        }
                    } else {
                        $trading_notification_msg = trans('notifications.trade_expired', ['Team' => $pool_trade->tradingPoolTeam->name]);
                    }
                    if (!$error) {
                        DB::commit();
                        Notification::createAndBroadcast(
                                $trading_notification_title, $trading_notification_msg, $trading_notification_link, [$pool_trade->parentPoolTeam->user_id], $pool_trade->parentPoolTeam->pool->id
                        );
                    }
                } else {
                    Log::alert('POOL TRADE ERROR : UNABLE TO MAKE TRADE EXPIRE : ' . $pool_trade->id);
                }
            }
        } catch (\Exception $ex) {
            \Log::alert("processTrades : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }

    public function processWaiver()
    {
        try {
            $pool_waivers = PoolWaiver::where('is_completed', '=', 0)
                    ->where('waiver_till', '<', date('Y-m-d H:i:s'))
                    ->with([
                        'PoolWaiverClaims',
                        'pool.totalPoolRoster',
                        'pool.season',
                    ])
                    ->get();
            foreach ($pool_waivers as $pool_waiver) {
                if (!$pool_waiver->PoolWaiverClaims->isEmpty()) {
                    $pool_waiver->PoolWaiverClaims->load([
                        'poolTeam.poolWaiverPriority' // loading priorities here because single team could have claimed 2 waiver players, so priority will be changed after 1st claim
                    ]);
                    $prioritized_claim = $pool_waiver->PoolWaiverClaims->sortByDesc('poolTeam.poolWaiverPriority.priority'); // sorting by reverse order to pop the item from collection
                    $this->_processWinClaim($prioritized_claim, $pool_waiver);
                }
                $pool_waiver->is_completed = 1;
                $pool_waiver->save();
            }
        } catch (\Exception $ex) {
            \Log::alert("processWaiver : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }

    private function _processWinClaim($prioritized_claim, $pool_waiver)
    {
        DB::beginTransaction();
        $won_item = $prioritized_claim->pop();
        if ($won_item != NULL) {
            $pool_team_player = $won_item->poolTeam->poolTeamPlayers()->create([
                'player_season_id' => $pool_waiver->player_season_id
            ]);

            // add new players on bench in lineup
            $lineup_item = $pool_team_player->poolTeamLineups()->firstOrNew([
                'start_time' => date('Y-m-d'),
                'end_time' => $pool_waiver->pool->season->end_date->toDateString()
            ]);
            $lineup_item->player_position_id = config('pool.inverse_player_position.bn');
            $lineup_item->save();


            $player_count = PoolTeamPlayer::getTotalPlayerCount($won_item->poolTeam->id);

            if ($player_count > $pool_waiver->pool->totalPoolRoster->total_roster) {
                DB::rollBack();
                $this->_processUndeleteLostPlayers($won_item);
                $this->_processWinClaim($prioritized_claim, $pool_waiver);
            } else {
                //save new priorities
                $last_priority = \App\Models\PoolWaiverPriority::where('pool_id', $pool_waiver->pool_id)->orderBy('priority', 'desc')->first();
                $won_item->poolTeam->poolWaiverPriority->priority = $last_priority->priority + 1;
                $won_item->poolTeam->poolWaiverPriority->save();

                // undelete all those players of team which didnt won waiver claim
                $this->_processUndeleteLostPlayers($prioritized_claim);
                DB::commit();
            }
        }
    }

    private function _processUndeleteLostPlayers($lost_data)
    {
        if (is_a($lost_data, 'Illuminate\Database\Eloquent\Collection')) {
            foreach ($lost_data as $data) {
                $this->_undeleteLostPlayers($data);
            }
        } else {
            $this->_undeleteLostPlayers($lost_data);
        }
    }

    private function _undeleteLostPlayers($data)
    {
        $data->load([
            'poolTeam.poolTeamPlayers' => function($query) use($data) {
                $query->whereHas('poolWaiverDelete', function($query) use($data) {
                            $query->where('pool_waiver_id', $data->pool_waiver_id);
                        })->withTrashed();
            },
        ]);

        foreach ($data->poolTeam->poolTeamPlayers as $pool_team_player) {
            //undeleting pool team players
            $pool_team_player->deleted_at = NULL;
            $pool_team_player->save();

            // removing all pool waivers which were added because of delete of lost players
            PoolWaiver::where('is_completed', 0)
                    ->where('player_season_id', $pool_team_player->player_season_id)
                    ->delete();

            // restoring all pool team lineups
            $pool_team_player->poolTeamLineups()->withTrashed()->update(['deleted_at' => NULL]);
            $current_lineup = $pool_team_player->poolTeamLineups()->whereNotNull('prev_end_time')->first();
            $current_lineup->end_time = $current_lineup->prev_end_time;
            $current_lineup->prev_end_time = NULL;
            $current_lineup->save();
        }
    }

}
