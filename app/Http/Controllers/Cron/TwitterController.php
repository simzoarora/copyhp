<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\TwitterFeed;
use App\Models\TweetComment;
use LRedis;
use Abraham\TwitterOAuth\TwitterOAuth;
use DB;

/**
 * 
 * Contain functions for twitter feeds
 * 
 */
class TwitterController extends Controller {

    /**
     * 
     * Gets the twitter favourite tweets using TwitterOAuth class
     * checks if the are not alreay present in database.. if not add them
     * add the comments on those tweets if present in the resulting array
     * and send the result tweets data to atom_feed.js node file using redis
     * 
     * @return array which gets publish through redis 
     */
    public function index() {
        $feed_empty_error = false;
        $connection = new TwitterOAuth(env('TWITTER_CUSTOMER_KEY'), env('TWITTER_CUSTOMER_SECRET'), env('TWITTER_ACCESS_TOKEN'), env('TWITTER_ACCESS_TOKEN_SECRET'));
//        $twitter_feed = $connection->get("favorites/list", ["count" => env('TWITTER_COUNT')]);
        //new member fetch 
        $twitter_feed = $connection->get("lists/statuses", ["count" => env('TWITTER_COUNT'), "slug" => env('TWITTER_SLUG'), "owner_screen_name" => env('TWITTER_OWNER_SCREEN_NAME'), "include_rts" => false]);
//        dd($twitter_feed); 
        if (!empty($twitter_feed)) {
            //checking if rate exceed error came
            foreach ($twitter_feed as $feed) {
                if (empty($feed->id_str) && !empty($feed[0]->message)) {
                    $feed_empty_error = true;
                }
            }
            if ($feed_empty_error) {
                return;
            }

            foreach ($twitter_feed as $key1 => $feed) {
                $twitter_feed[$key1] = array(
                    'tweet_id' => $feed->id_str,
                    'image' => $feed->user->profile_image_url,
                    'name' => $feed->user->name,
                    'tweet' => $feed->text,
                    'tweet_created' => date('Y-m-d H:i:s', strtotime($feed->created_at))
                );
                $result = TwitterFeed::where('tweet_id', $feed->id_str)->first();
                $image_url = $feed->user->profile_image_url;
                if (strpos($image_url, 'http:') !== false) {
                    $image_url = str_replace('http:', '', $image_url);
                } else if (strpos($image_url, 'https:') !== false) {
                    $image_url = str_replace('https:', '', $image_url);
                }
                if (empty($result)) {
                    $feed_data = array(
                        'tweet_id' => $feed->id_str,
                        'image' => $image_url,
                        'name' => $feed->user->name,
                        'tweet' => $feed->text,
                        'tweet_created' => date('Y-m-d H:i:s', strtotime($feed->created_at))
                    );
                    TwitterFeed::create($feed_data);
                } else {
                    //checking if image is not valid
                    $image_http_url = 'http:' . $result->image;
                    if (!@getimagesize($image_http_url)) {
                        TwitterFeed::where('id', $result->id)
                                ->update(['image' => $image_url]);
                    }

                    //check for comments 
                    $all_comments = TweetComment::where('tweet_id', $result->id)->get()->toArray();
                    if (!empty($all_comments)) {
                        foreach ($all_comments as $key => $comment) {
                            $all_comments[$key]['created_at'] = date('F j, Y', strtotime($comment['created_at']));
                            //also need to add username in it
                            $user_details = DB::table('users')->where('id', '=', $comment['user_id'])->select('name')->first();
                            $all_comments[$key]['name'] = $user_details->name;
                        }
                    }
                    $twitter_feed[$key1]['comments'] = $all_comments;
                }
                $twitter_feed[$key1]['tweet_created'] = date('F j, Y', strtotime($feed->created_at));
                $twitter_feed[$key1]['image'] = $image_url;
            }
            $redis = LRedis::connection();
            $redis->publish('TwitterFeed', json_encode($twitter_feed));
            echo 'Twitter Feed Send';
        }
    }

    /**
     * 
     * Gets the twitter favourite tweets from database for user who just open site 
     * 
     * @return array which gets publish through redis 
     */
    public function firstTwitterFeed() {
        $imageNotValid = false;
        $twitter_feed = TwitterFeed::orderBy('tweet_created', 'desc')->take(env('TWITTER_COUNT'))->get()->toArray();
        if (!empty($twitter_feed)) {
            foreach ($twitter_feed as $key1 => $feed) {
                if ($imageNotValid == true) {
                    return;
                }
                $image_http_url = 'http:' . $feed['image'];
                //checking if image is not valid
                if (!@getimagesize($image_http_url)) {
                    $this->index();
                    $imageNotValid = true;
                    return;
                }

                //check for comments 
                $all_comments = TweetComment::where('tweet_id', $feed['id'])->orderBy('created_at', 'desc')->get()->toArray();
                if (!empty($all_comments)) {
                    foreach ($all_comments as $key2 => $comment) {
                        $all_comments[$key2]['created_at'] = Carbon::createFromTimeStamp(strtotime($comment['created_at']))->diffForHumans();
                        //also need to add username in it
                        $user_details = DB::table('users')->where('id', '=', $comment['user_id'])->select('name')->first();
                        $all_comments[$key2]['name'] = $user_details->name;
                    }
                }
                $twitter_feed[$key1]['tweet_created'] = date('F j, Y', strtotime($feed['tweet_created']));
                $twitter_feed[$key1]['comments'] = $all_comments;
            }
        }
        if ($imageNotValid == false) {
            $redis = LRedis::connection();
            $redis->publish('TwitterFeed', json_encode($twitter_feed));
            echo 'Twitter Feed First Send';
        }
    }

}
