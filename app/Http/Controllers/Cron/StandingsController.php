<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use SportsDirect\NHL;
use App\Models\Conference;
use App\Models\Division;
use App\Models\Team;
use App\Models\Season;
use App\Models\ConferenceStanding;
use App\Models\DivisionStanding;
use App\Models\LeagueStanding;
use App\Models\ExtraLeagueStanding;
use App\Models\StoredValue;

/**
 * Description of StandingsController
 *
 * @author Anik
 */
class StandingsController extends Controller
{

    public function fetchStandings()
    {
        try {
            $stored_value = StoredValue::where('key', 'standings_feed_update_time')->firstOrFail();
            $nhl = new NHL(\Config('sportsdirectconfig.api_key'));
            $atom_feed = $nhl->getStandingAtomFeed(date('Y-m-d\TH:i:s.uP', strtotime($stored_value->value)));
            if (isset($atom_feed['entry']) && !empty($atom_feed['entry'])) {
                if (!isset($atom_feed['entry'][0])) {
                    $this->_processAndSaveStanding($atom_feed['entry']);
                } else {
                    foreach ($atom_feed['entry'] as $key => $entry) {
                        $this->_processAndSaveStanding($entry);
                    }
                }
            }
            $stored_value->value = date('Y-m-d H:i:s', strtotime($atom_feed['updated'] . ' +1 second'));
            $stored_value->save();

            if (!LeagueStanding::whereHas('season', function($query) {
                        $query->where('current', '1');
                    })->exists()) {
                $this->_addDefaultValuesForStanding();
            }
        } catch (\Exception $ex) {
            \Log::alert("fetchStandings : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }

    private function _addDefaultValuesForStanding()
    {
        $season = Season::where('current', 1)->with('teams')->first();
        $league_rank = 1;
        $division_ranks = [];
        $conference_ranks = [];
        foreach ($season->teams as $team) {
            LeagueStanding::create([
                'team_id' => $team->id,
                'season_id' => $season->id,
                'rank' => $league_rank++,
            ]);
            if (!isset($conference_ranks[$team->pivot->conference_id])) {
                $conference_ranks[$team->pivot->conference_id] = 1;
            }
            ConferenceStanding::create([
                'team_id' => $team->id,
                'season_id' => $season->id,
                'conference_id' => $team->pivot->conference_id,
                'conference_ranking' => $conference_ranks[$team->pivot->conference_id]++
            ]);
            if (!isset($division_ranks[$team->pivot->division_id])) {
                $division_ranks[$team->pivot->division_id] = 1;
            }
            DivisionStanding::create([
                'team_id' => $team->id,
                'season_id' => $season->id,
                'division_id' => $team->pivot->division_id,
                'division_ranking' => $division_ranks[$team->pivot->division_id]++
            ]);
            foreach (config('constant.sportsdirectconstant.inverse_extra_league_standings_types') as $type) {
                ExtraLeagueStanding::create([
                    'team_id' => $team->id,
                    'season_id' => $season->id,
                    'type' => $type,
                ]);
            }
        }
    }

    private function _processAndSaveStanding($entry)
    {
        $client = new \GuzzleHttp\Client();
        $standing_data = json_decode(json_encode(new \SimpleXMLElement($client->request('GET', $entry['id'])->getBody())), 1);
        if ($standing_data['team-sport-content']['league-content']['season-content']['season']['name'] == '2015-2016') {
//            dd($standing_data['team-sport-content']['league-content']['season-content']['conference-content'][0]['division-content'][0]['team-content'][0]['stat-group']);
        }
        $season = Season::sportsDirectSaveOrGetSeason($standing_data['team-sport-content']['league-content']['season-content']['season']);
        foreach ($standing_data['team-sport-content']['league-content']['season-content']['conference-content'] as $conference_content) {
            $conference = Conference::sportsDirectSaveOrGetConference($conference_content['conference']);
            foreach ($conference_content['division-content'] as $division_content) {
                $division = Division::sportsDirectSaveOrGetConference($division_content['division'], $conference->id);
                foreach ($division_content['team-content'] as $team_content) {
                    $team = Team::getTeamFromApiIdString($team_content['team']['id']);
                    if (!$team) {
                        continue;
                    }
                    foreach ($team_content['stat-group'] as $stat_group) {
                        switch ($stat_group['key']) {
                            case 'conference-standings':
                                ConferenceStanding::processAndSaveDataFromSportsDirect($stat_group, $team, $season->id, $conference->id);
                                break;
                            case 'division-standings':
                                DivisionStanding::processAndSaveDataFromSportsDirect($stat_group, $team, $season->id, $division->id);
                                break;
                            case 'league-standings':
                                LeagueStanding::processAndSaveDataFromSportsDirect($stat_group, $team, $season->id);
                                break;
                            case 'last-10-league-standings':
                                ExtraLeagueStanding::processAndSaveDataFromSportsDirect($stat_group, $team, $season->id, config('constant.sportsdirectconstant.inverse_extra_league_standings_types.last10'));
                                break;
                            case 'home-league-standings':
                                ExtraLeagueStanding::processAndSaveDataFromSportsDirect($stat_group, $team, $season->id, config('constant.sportsdirectconstant.inverse_extra_league_standings_types.home'));
//                                die('here');
                                break;
                            case 'away-league-standings':
                                ExtraLeagueStanding::processAndSaveDataFromSportsDirect($stat_group, $team, $season->id, config('constant.sportsdirectconstant.inverse_extra_league_standings_types.away'));
//                                die('here2');
                                break;
                            default :
                                break;
                        }
                    }
                }
            }
        }
        LeagueStanding::calculateLeagueRanks($season);
    }
}