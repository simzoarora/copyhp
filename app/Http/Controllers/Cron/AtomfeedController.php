<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Team;
use App\Models\Season;
use App\Models\Competition;
use App\Models\Scorefeedupdates;
use LRedis;
use SportsDirect\NHL;

/**
 * 
 * Contain functions for scoreboard feeds
 * 
 */
class AtomfeedController extends Controller
{
    public $all_matches = array();

    /**
     * 
     * Scoreboard data fetching from http://xml.sportsdirectinc.com
     * And send scoreboard data to browser
     * And also save scoreboard data to database
     * This function is used as a cron job to fetch data in a interval of 1 min
     * This function uses the xml links from constant file
     *
     * @return array which gets publish through redis
     */
    public function index()
    {
        $live_feed = $this->_liveFeed();
        $tomorrow_feed = $this->_tomorrowFeed();

        $total_feed['yesterday_feed'] = $live_feed['yesterday_feed'];
        $total_feed['today_live_feed'] = $live_feed['today_live_feed'];
        $total_feed['previous_feed'] = $live_feed['previous_feed'];
        $total_feed['tomorrow_feed'] = $tomorrow_feed;

        $current_season = Season::where('current', 1)->firstOrFail();
        if (!empty($total_feed['yesterday_feed'])) {
            foreach ($total_feed['yesterday_feed'] as $key => $row) {
                $home_team_details = Team::where('api_id', $row['home_team_id'])->get()->first();
                $away_team_details = Team::where('api_id', $row['away_team_id'])->get()->first();

                $total_feed['yesterday_feed'][$key]['home_abbr'] = $home_team_details['short_name'];
                $total_feed['yesterday_feed'][$key]['home_image'] = '/img/nhl_logos/' . $home_team_details['logo'];
                $total_feed['yesterday_feed'][$key]['home_team_id'] = $home_team_details['id'];

                $total_feed['yesterday_feed'][$key]['away_abbr'] = $away_team_details['short_name'];
                $total_feed['yesterday_feed'][$key]['away_image'] = '/img/nhl_logos/' . $away_team_details['logo'];
                $total_feed['yesterday_feed'][$key]['away_team_id'] = $away_team_details['id'];

                $total_feed['yesterday_feed'][$key]['match_status'] = 'Final';
                $total_feed['yesterday_feed'][$key]['match_complition_type'] = $this->_matchEnded($row['match_complition_type']);

                //Saving to DB
                $feed = $total_feed['yesterday_feed'][$key];
                $feed['match_start_date'] = date('Y-m-d H:i:s', strtotime($feed['match_start_date']));
                $feed['type'] = 4;
                $feed_check = $current_season->competitions()->where('api_id', $feed['api_id'])->first();
                if (empty($feed_check)) {
                    $feed_check = $current_season->competitions()->create($feed);
                } else {
                    $feed_check->update($feed);
                }
                $total_feed['yesterday_feed'][$key]['id'] = $feed_check->id;
            }
        }

        if (!empty($total_feed['today_live_feed'])) {
            foreach ($total_feed['today_live_feed'] as $key => $row) {
                $home_team_details = Team::where('api_id', $row['home_team_id'])->get()->first();
                $away_team_details = Team::where('api_id', $row['away_team_id'])->get()->first();

                $total_feed['today_live_feed'][$key]['home_abbr'] = $home_team_details['short_name'];
                $total_feed['today_live_feed'][$key]['home_image'] = '/img/nhl_logos/' . $home_team_details['logo'];
                $total_feed['today_live_feed'][$key]['home_team_id'] = $home_team_details['id'];

                $total_feed['today_live_feed'][$key]['away_abbr'] = $away_team_details['short_name'];
                $total_feed['today_live_feed'][$key]['away_image'] = '/img/nhl_logos/' . $away_team_details['logo'];
                $total_feed['today_live_feed'][$key]['away_team_id'] = $away_team_details['id'];

                if ($row['match_status'] == 'complete') {
                    $total_feed['today_live_feed'][$key]['match_status'] = 'Final';
                    $total_feed['today_live_feed'][$key]['match_complition_type'] = $this->_matchEnded($row['match_complition_type_string']);
                } else {
                    if ($row['clock'] != 'END') {
                        $total_feed['today_live_feed'][$key]['match_status'] = date('i:s', strtotime($row['clock']));
                    } else {
                        $total_feed['today_live_feed'][$key]['match_status'] = $row['clock'];
                    }
                    $total_feed['today_live_feed'][$key]['match_complition_type'] = $this->_numberSuffix($row['match_complition_type']);
                }

                //Saving to DB
                $feed = $total_feed['today_live_feed'][$key];
                $feed['match_start_date'] = date('Y-m-d H:i:s', strtotime($feed['match_start_date']));
                $feed['type'] = 2;
                $feed_check = $current_season->competitions()->where('api_id', $feed['api_id'])->first();
                if (empty($feed_check)) {
                    $feed_check = $current_season->competitions()->create($feed);
                } else {
                    $feed_check->update($feed);
                }
                $total_feed['today_live_feed'][$key]['id'] = $feed_check->id;
            }
        }
        if (!empty($total_feed['previous_feed'])) {
            foreach ($total_feed['previous_feed'] as $key => $row) {
                $home_team_details = Team::where('api_id', $row['home_team_id'])->get()->first();
                $away_team_details = Team::where('api_id', $row['away_team_id'])->get()->first();

                $total_feed['previous_feed'][$key]['home_abbr'] = $home_team_details['short_name'];
                $total_feed['previous_feed'][$key]['home_image'] = '/img/nhl_logos/' . $home_team_details['logo'];
                $total_feed['previous_feed'][$key]['home_team_id'] = $home_team_details['id'];

                $total_feed['previous_feed'][$key]['away_abbr'] = $away_team_details['short_name'];
                $total_feed['previous_feed'][$key]['away_image'] = '/img/nhl_logos/' . $away_team_details['logo'];
                $total_feed['previous_feed'][$key]['away_team_id'] = $away_team_details['id'];

                $total_feed['previous_feed'][$key]['match_status'] = 'Final';
                $total_feed['previous_feed'][$key]['match_complition_type'] = $this->_matchEnded($row['match_complition_type']);
                $total_feed['previous_feed'][$key]['match_start_date'] = date('M jS', strtotime($row['match_start_date']));
                $total_feed['previous_feed'][$key]['match_start_date_real'] = $row['match_start_date'];

                //Saving to DB
                $feed = $total_feed['previous_feed'][$key];
                $feed['match_start_date'] = date('Y-m-d H:i:s', strtotime($feed['match_start_date_real']));
                $feed['type'] = 1;
                $feed_check = $current_season->competitions()->where('api_id', $feed['api_id'])->first();
                if (empty($feed_check)) {
                    $feed_check = $current_season->competitions()->create($feed);
                } else {
                    $feed_check->update($feed);
                }
                $total_feed['previous_feed'][$key]['id'] = $feed_check->id;
            }
        }
        if (!empty($total_feed['tomorrow_feed'])) {
            foreach ($total_feed['tomorrow_feed'] as $key => $row) {
                $home_team_details = Team::where('api_id', $row['home_team_id'])->get()->first();
                $away_team_details = Team::where('api_id', $row['away_team_id'])->get()->first();

                $total_feed['tomorrow_feed'][$key]['home_abbr'] = $home_team_details['short_name'];
                $total_feed['tomorrow_feed'][$key]['home_image'] = '/img/nhl_logos/' . $home_team_details['logo'];
                $total_feed['tomorrow_feed'][$key]['home_team_id'] = $home_team_details['id'];

                $total_feed['tomorrow_feed'][$key]['away_abbr'] = $away_team_details['short_name'];
                $total_feed['tomorrow_feed'][$key]['away_image'] = '/img/nhl_logos/' . $away_team_details['logo'];
                $total_feed['tomorrow_feed'][$key]['away_team_id'] = $away_team_details['id'];

                $total_feed['tomorrow_feed'][$key]['match_start_date'] = date('g:i A', strtotime($row['match_start_date']));
                $total_feed['tomorrow_feed'][$key]['match_start_date2'] = date('M jS', strtotime($row['match_start_date']));
                $total_feed['tomorrow_feed'][$key]['match_start_date_real'] = $row['match_start_date'];

                $tomorrow_date = Carbon::tomorrow(config('app.timezone_for_calculation'))->format('Y-m-d');
                $today_date = Carbon::now(config('app.timezone_for_calculation'))->format('Y-m-d');
                if (date('Y-m-d', strtotime($row['match_start_date'])) == $tomorrow_date) {
                    $what_day = 'tomorrow';
                } else if (date('Y-m-d', strtotime($row['match_start_date'])) == $today_date) {
                    $what_day = 'today';
                } else {
                    $what_day = 'day after';
                }
                $total_feed['tomorrow_feed'][$key]['what_day'] = $what_day;

                //Saving to DB
                $feed = $total_feed['tomorrow_feed'][$key];
                $feed['match_start_date'] = date('Y-m-d H:i:s', strtotime($feed['match_start_date_real']));
                $feed['type'] = 3;
                $feed_check = $current_season->competitions()->where('api_id', $feed['api_id'])->first();
                if (empty($feed_check)) {
                    $feed_check = $current_season->competitions()->create($feed);
                } else {
                    $feed_check->update($feed);
                }
                $total_feed['tomorrow_feed'][$key]['id'] = $feed_check->id;
            }
        }
        $redis = LRedis::connection();
        $redis->publish('ScoreboardFeed', json_encode($total_feed));
        echo 'Score feed cron done.';
    }

    /**
     * 
     * Determine the tomorrow and future feed form scorebord data 
     * This function goes back 7 days and try to find data
     * If there is no data then it goes back to 30 days
     * 
     * @return array 
     */
    private function _tomorrowFeed()
    {
        $nhl = new NHL(\Config('sportsdirectconfig.api_key'));
        $tomorrow_feed = array();
        $date_to_check = date('Y-m-d');
        $found_days = 0;

        //checking for next 7 days
        $season_data = $nhl->getGameSchedule(7);
        if (!empty($season_data['team-sport-content']['league-content']['season-content']['competition'])) {
            $tomorrow_feed = $this->_tomorrowFeedArray($date_to_check, $season_data['team-sport-content']['league-content']['season-content']['competition'], $tomorrow_feed, $found_days);
        }

        //checking for next 30 days
        if (empty($tomorrow_feed)) {
            $season_data = $nhl->getGameSchedule(30);
            if (!empty($season_data['team-sport-content']['league-content']['season-content']['competition'])) {
                $tomorrow_feed = $this->_tomorrowFeedArray($date_to_check, $season_data['team-sport-content']['league-content']['season-content']['competition'], $tomorrow_feed, $found_days);
            }
        }

        //checking for next all days
        if (empty($tomorrow_feed)) {
            $season_data = $nhl->getGameSchedule();
            if (!empty($season_data['team-sport-content']['league-content']['season-content']['competition'])) {
                $tomorrow_feed = $this->_tomorrowFeedArray($date_to_check, $season_data['team-sport-content']['league-content']['season-content']['competition'], $tomorrow_feed, $found_days);
            }
        }
        sort($tomorrow_feed);
        return $tomorrow_feed;
    }

    /**
     * 
     * Determine the tomorrow and future array 
     * 
     * @return array 
     */
    private function _tomorrowFeedArray($date_to_check, $data, $tomorrow_feed, $found_days)
    {
        //checking if value is single
        if (empty($data[0])) {
            $contain_one_array = 1;
            if ((explode('T', $data['start-date'])[0]) == $date_to_check) {
                $match_check = in_array($data['id'], $this->all_matches);
                if ($match_check == false) {
                    $this->all_matches[] = $data['id'];
                    $result[] = array(
                        'api_id' => str_replace('/sport/hockey/competition:', '', $data['id']),
                        'match_start_date' => $data['start-date'],
                        'home_team_id' => str_replace('/sport/hockey/team:', '', $data['home-team-content']['team']['id']),
                        'away_team_id' => str_replace('/sport/hockey/team:', '', $data['away-team-content']['team']['id']),
                        'competition_type' => $data['details']['competition-type']
                    );
                }
            }
        } else {
            $contain_one_array = 0;
            foreach ($data as $key1 => $value1) {
                if ((explode('T', $value1['start-date'])[0]) == $date_to_check) {
                    $match_check = in_array($value1['id'], $this->all_matches);
                    if ($match_check == false) {
                        $this->all_matches[] = $value1['id'];
                        $result[] = array(
                            'api_id' => str_replace('/sport/hockey/competition:', '', $value1['id']),
                            'match_start_date' => $value1['start-date'],
                            'home_team_id' => str_replace('/sport/hockey/team:', '', $value1['home-team-content']['team']['id']),
                            'away_team_id' => str_replace('/sport/hockey/team:', '', $value1['away-team-content']['team']['id']),
                            'competition_type' => $value1['details']['competition-type']
                        );
                    }
                }
            }
        }

        if (!empty($result)) {
            $tomorrow_feed = array_merge($tomorrow_feed, $result);
            $found_days = $found_days + 1;

            if ($contain_one_array == 0) {
                if ($found_days < 2) {
                    $date_to_check = date('Y-m-d', strtotime('+1 day', strtotime($date_to_check)));
                    return $this->_tomorrowFeedArray($date_to_check, $data, $tomorrow_feed, $found_days);
                } else {
                    return $tomorrow_feed;
                }
            } else {
                return $tomorrow_feed;
            }
        } else {
            $date_to_check = date('Y-m-d', strtotime('+1 day', strtotime($date_to_check)));
            return $this->_tomorrowFeedArray($date_to_check, $data, $tomorrow_feed, $found_days);
        }
    }

    /**
     * 
     * Determine the live, yesterday and past feed form scorebord data 
     * 
     * @return array 
     */
    private function _liveFeed()
    {
        $yesterday_feed = array();
        $today_live_feed = array();
        $previous_feed = array();

        $scorefeedupdates = Scorefeedupdates::get()->first();
        $database_date = strtotime($scorefeedupdates['livescores']);

        $feed = $this->_curlNew(\Config::get('constant.new_feed_link'));
        //checking if feed entry is empty or not
        if (!empty($feed['entry'])) {
            //converting into server timezone
            $updated_date = strtotime($feed['updated'] . ' ' . Config('app.timezone'));
//            if ($updated_date > $database_date) {
            $scorefeedupdates->update(array('livescores' => date('Y-m-d H:i:s', $updated_date)));
            if (empty($feed['entry'][0])) {
                //not need to do foreach
                $key = 0;
                $feed1 = $this->_curlNew($feed['entry']['id']);
                list($yesterday_feed, $today_live_feed) = $this->_liveFeedArrayFetch($key, $feed1, $yesterday_feed, $today_live_feed);
            } else {
                foreach ($feed['entry'] as $key => $value) {
                    $feed1 = $this->_curlNew($value['id']);
                    list($yesterday_feed, $today_live_feed) = $this->_liveFeedArrayFetch($key, $feed1, $yesterday_feed, $today_live_feed);
                }
            }
//            }
        }

        //no of $old_days means how many days for which we need past data -- 2 means get data of yesterday and day before yesterday
        $old_days = 0;
        if (empty($today_live_feed)) {
            $old_days = $old_days + 1;
        }
        if (empty($yesterday_feed)) {
            $old_days = $old_days + 1;
        }

        // livescores NOT present - fetch results link
        if ($old_days > 0) {
            $current_season = Season::where('current', 1)->firstOrFail();
            $nhl = new NHL(\Config('sportsdirectconfig.api_key'));
            $season_data = $nhl->getSeasonResults($current_season->name);
            if (!empty($season_data['team-sport-content']['league-content']['season-content']['competition'])) {
                //$old_days_found to check how many days we have already found
                $old_days_found = 0;
                $rotate = 0;
                if ($old_days == 1) {
                    $date_to_check = date('Y-m-d', strtotime('-1 day'));
                } else if ($old_days == 2) {
                    $date_to_check = date('Y-m-d', strtotime('-2 day'));
                }
                $previous_feed = $this->_prevFeedArrayFetch($date_to_check, $season_data['team-sport-content']['league-content']['season-content']['competition'], $old_days, $old_days_found, $previous_feed, $rotate);
            }
        }

        sort($yesterday_feed);
        sort($today_live_feed);
        sort($previous_feed);
        return [
            'yesterday_feed' => $yesterday_feed,
            'today_live_feed' => $today_live_feed,
            'previous_feed' => $previous_feed
        ];
    }

    /**
     * 
     * Determine the past feed array 
     * 
     * @return array 
     */
    private function _prevFeedArrayFetch($date_to_check, $data, $old_days, $old_days_found, $previous_feed, $rotate)
    {
        foreach ($data as $value4) {
            if (explode('T', $value4['start-date'])[0] == $date_to_check) {
                $match_check = in_array($value4['id'], $this->all_matches);
                if ($match_check == false) {
                    $this->all_matches[] = $value4['id'];
                    $result[] = array(
                        'api_id' => str_replace('/sport/hockey/competition:', '', $value4['id']),
                        'match_start_date' => $value4['start-date'],
                        'match_status' => $value4['result-scope']['competition-status'],
                        'match_update_date' => $value4['result-scope']['completed-date'],
                        'match_complition_type' => $value4['result-scope']['scope']['@attributes']['type'],
                        'home_team_id' => str_replace('/sport/hockey/team:', '', $value4['home-team-content']['team']['id']),
                        'home_team_score' => (!empty($value4['home-team-content']['stat-group']['stat'][1]) ? $value4['home-team-content']['stat-group']['stat'][1]['@attributes']['num'] : 0),
                        'away_team_id' => str_replace('/sport/hockey/team:', '', $value4['away-team-content']['team']['id']),
                        'away_team_score' => (!empty($value4['away-team-content']['stat-group']['stat'][1]) ? $value4['away-team-content']['stat-group']['stat'][1]['@attributes']['num'] : 0)
                    );
                }
            }
        }
        $rotate = $rotate + 1;
        //going only one month back
        if ($rotate <= 30) {
            if (!empty($result)) {
                $previous_feed = array_merge($previous_feed, $result);
                $old_days_found = $old_days_found + 1;

                if ($old_days_found != $old_days) {
                    $date_to_check = date('Y-m-d', strtotime('-1 day', strtotime($date_to_check)));
                    return $this->_prevFeedArrayFetch($date_to_check, $data, $old_days, $old_days_found, $previous_feed, $rotate);
                } else {
                    return $previous_feed;
                }
            } else {
                $date_to_check = date('Y-m-d', strtotime('-1 day', strtotime($date_to_check)));
                return $this->_prevFeedArrayFetch($date_to_check, $data, $old_days, $old_days_found, $previous_feed, $rotate);
            }
        } else {
            return [];
        }
    }

    /**
     * 
     * Determine the live and yesterday feed array 
     * 
     * @return array 
     */
    private function _liveFeedArrayFetch($key, $feed1, $yesterday_feed, $today_live_feed)
    {
        $yesterday_date = Carbon::yesterday(config('app.timezone_for_calculation'))->format('Y-m-d');
        $today_date = Carbon::now(config('app.timezone_for_calculation'))->format('Y-m-d');
        if (!empty($feed1['team-sport-content'])) {
            foreach ($feed1['team-sport-content'] as $value1) {
//                dump($value1);
                if (isset($value1['competition']['id'])) {
                    if (explode('T', $value1['competition']['start-date'])[0] == $yesterday_date) {
                        $match_check = in_array($value1['competition']['id'], $this->all_matches);
                        if ($match_check == false) {
                            $this->all_matches[] = $value1['competition']['id'];
                            $yesterday_feed[$key]['api_id'] = str_replace('/sport/hockey/competition:', '', $value1['competition']['id']);
                            $yesterday_feed[$key]['match_start_date'] = $value1['competition']['start-date'];
                            $yesterday_feed[$key]['match_status'] = $value1['competition']['result-scope']['competition-status'];
                            $yesterday_feed[$key]['match_update_date'] = $value1['competition']['result-scope']['update-date'];
                            $yesterday_feed[$key]['match_complition_type'] = $value1['competition']['result-scope']['scope']['@attributes']['type'];
                            $yesterday_feed[$key]['home_team_id'] = str_replace('/sport/hockey/team:', '', $value1['competition']['home-team-content']['team']['id']);
                            $yesterday_feed[$key]['home_team_score'] = $value1['competition']['home-team-content']['stat-group'][0]['stat'] ['@attributes']['num'];
                            $yesterday_feed[$key]['away_team_id'] = str_replace('/sport/hockey/team:', '', $value1['competition']['away-team-content']['team']['id']);
                            $yesterday_feed[$key]['away_team_score'] = $value1['competition']['away-team-content']['stat-group'][0]['stat'] ['@attributes']['num'];
                        }
                    } elseif (explode('T', $value1['competition']['start-date'])[0] == $today_date) {
                        //just checking if the feed data is not of a game which is just going to start. As this game will come in scheduler and livescore.
                        //And we only want to show it in scheduler
                        if (!empty($value1['competition']['home-team-content']['stat-group'])) {
                            $match_check = in_array($value1['competition']['id'], $this->all_matches);
                            if ($match_check == false) {
                                $this->all_matches[] = $value1['competition']['id'];
                                $today_live_feed[$key]['api_id'] = str_replace('/sport/hockey/competition:', '', $value1['competition']['id']);
                                $today_live_feed[$key]['match_start_date'] = $value1['competition']['start-date'];
                                $today_live_feed[$key]['match_status'] = $value1['competition']['result-scope']['competition-status'];
                                if (!empty($value1['competition']['result-scope']['clock'])) {
                                    $today_live_feed[$key]['clock'] = $value1['competition']['result-scope']['clock'];
                                } else {
                                    //when game is in progress and there is no clock
                                    $today_live_feed[$key]['clock'] = 'END';
                                }
                                $today_live_feed[$key]['match_update_date'] = $value1['competition']['result-scope']['update-date'];
                                $today_live_feed[$key]['match_complition_type'] = $value1['competition']['result-scope']['scope']['@attributes']['num'];
                                $today_live_feed[$key]['match_complition_type_string'] = $value1['competition']['result-scope']['scope']['@attributes']['type'];
                                $today_live_feed[$key]['home_team_id'] = str_replace('/sport/hockey/team:', '', $value1['competition']['home-team-content']['team']['id']);
                                $today_live_feed[$key]['home_team_score'] = $value1['competition']['home-team-content']['stat-group'][0]['stat'] ['@attributes']['num'];
                                $today_live_feed[$key]['away_team_id'] = str_replace('/sport/hockey/team:', '', $value1['competition']['away-team-content']['team']['id']);
                                $today_live_feed[$key]['away_team_score'] = $value1['competition']['away-team-content']['stat-group'][0]['stat'] ['@attributes']['num'];
                            }
                        }
                    }
                }
            }
        }
        return array($yesterday_feed, $today_live_feed);
    }

    /**
     * 
     * Perform the curl function and convert the result data into json array
     * 
     * @param string $url link to make curl request
     * 
     * @return array 
     */
    private function _curlNew($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $content = curl_exec($curl);
        curl_close($curl);
        if (!empty($content)) {
            $result = @simplexml_load_string($content);
            if ($result) {
                return json_decode(json_encode($result), true);
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    /**
     * 
     * Add a suffix after a number 
     * 
     * @param int $number number to add suffix
     *
     * @return integer 
     */
    private function _numberSuffix($number)
    {
        $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
        if ((($number % 100) >= 11) && (($number % 100) <= 13))
            return $number . 'th';
        else
            return $number . $ends[$number % 10];
    }

    /**
     * 
     * Determine the match state 
     * 
     * @param string $match_complition_type string that determines
     *
     * @return string 
     */
    private function _matchEnded($match_complition_type)
    {
        if ($match_complition_type == 'period') {
            return $match_ended = 'F';
        } else if ($match_complition_type == 'overtime') {
            return $match_ended = 'F/OT';
        } else if ($match_complition_type == 'shootout') {
            return $match_ended = 'F/SO';
        }
    }

    /**
     * 
     * Scoreboard data fetching from database when user open site 
     *
     * @return array which gets publish through redis
     */
    public function firstScoreboardData()
    {
//        return $this->index();
//        ini_set('memory_limit', 268435456);
//        ini_set('max_execution_time', 0);
        //adding +3 hours to convert timezone from new york to vancover 
        $yesterday_date = Carbon::yesterday(config('app.timezone_for_calculation'))->format('Y-m-d');
        $today_date = Carbon::now(config('app.timezone_for_calculation'))->format('Y-m-d');
//        $today_date = date('Y-m-d', strtotime('+3 hours'));
//        $yesterday_date = date('Y-m-d', strtotime('-1 day +3 hours'));
        $today_live_feed = Competition::where('match_start_date', 'LIKE', '%' . $today_date . '%')->where('type', 2)->get()->toArray();

        $yesterday_feed = Competition::where('match_start_date', 'LIKE', '%' . $yesterday_date . '%')->where('type', 4)->get()->toArray();

        //Getting previous feed
        if (!empty($yesterday_feed)) {
            $prev_get_count = 1;
        } else {
            $prev_get_count = 2;
        }
        //getting maximum count
        $previous_feed_count = Competition::where('type', 1)->count();

        $previous_feed = array();
//        $date_to_check = date('Y-m-d', strtotime('-2 day +3 hours'));
        $date_to_check = Carbon::now(config('app.timezone_for_calculation'))->subDays(2)->format('Y-m-d');
        $found = 0;
        $rotate = 0;
        $previous_feed = $this->_getPreviousFeed($previous_feed, $date_to_check, $found, $previous_feed_count, $rotate, $prev_get_count);
        //END
        //getting maximum count
        $tomorrow_feed_count = Competition::where('type', 3)->count();
        if ($tomorrow_feed_count != 0) {
            $tomorrow_feed2 = array();
            $date_to_check = $today_date;
            $found = 0;
            $rotate = 0;
            $tomorrow_feed = $this->_getTomorrowFeed($tomorrow_feed2, $date_to_check, $found, $tomorrow_feed_count, $rotate);
        }

        if (!empty($today_live_feed)) {
            foreach ($today_live_feed as $key => $feed) {
                $home_team_details = Team::where('id', $feed['home_team_id'])->get()->first();
                $away_team_details = Team::where('id', $feed['away_team_id'])->get()->first();

                $today_live_feed[$key]['home_abbr'] = $home_team_details['short_name'];
                $today_live_feed[$key]['home_image'] = '/img/nhl_logos/' . $home_team_details['logo'];


                $today_live_feed[$key]['away_abbr'] = $away_team_details['short_name'];
                $today_live_feed[$key]['away_image'] = '/img/nhl_logos/' . $away_team_details['logo'];
            }
            $total_feed['today_live_feed'] = $today_live_feed;
        }

        if (!empty($yesterday_feed)) {
            foreach ($yesterday_feed as $key => $feed) {
                $home_team_details = Team::where('id', $feed['home_team_id'])->get()->first();
                $away_team_details = Team::where('id', $feed['away_team_id'])->get()->first();

                $yesterday_feed[$key]['home_abbr'] = $home_team_details['short_name'];
                $yesterday_feed[$key]['home_image'] = '/img/nhl_logos/' . $home_team_details['logo'];

                $yesterday_feed[$key]['away_abbr'] = $away_team_details['short_name'];
                $yesterday_feed[$key]['away_image'] = '/img/nhl_logos/' . $away_team_details['logo'];
            }
            $total_feed['yesterday_feed'] = $yesterday_feed;
        }

        if (!empty($previous_feed)) {
            foreach ($previous_feed as $key => $feed) {
                $home_team_details = Team::where('id', $feed['home_team_id'])->get()->first();
                $away_team_details = Team::where('id', $feed['away_team_id'])->get()->first();

                $previous_feed[$key]['home_abbr'] = $home_team_details['short_name'];
                $previous_feed[$key]['home_image'] = '/img/nhl_logos/' . $home_team_details['logo'];

                $previous_feed[$key]['away_abbr'] = $away_team_details['short_name'];
                $previous_feed[$key]['away_image'] = '/img/nhl_logos/' . $away_team_details['logo'];

                $previous_feed[$key]['match_start_date'] = date('M jS', strtotime($feed['match_start_date']));
            }
            $total_feed['previous_feed'] = $previous_feed;
        }

        if (!empty($tomorrow_feed)) {
            foreach ($tomorrow_feed as $key => $feed) {
                $home_team_details = Team::where('id', $feed['home_team_id'])->get()->first();
                $away_team_details = Team::where('id', $feed['away_team_id'])->get()->first();

                $tomorrow_feed[$key]['home_abbr'] = $home_team_details['short_name'];
                $tomorrow_feed[$key]['home_image'] = '/img/nhl_logos/' . $home_team_details['logo'];

                $tomorrow_feed[$key]['away_abbr'] = $away_team_details['short_name'];
                $tomorrow_feed[$key]['away_image'] = '/img/nhl_logos/' . $away_team_details['logo'];

                $tomorrow_feed[$key]['match_start_date'] = date('g:i A', strtotime($feed['match_start_date']));
                $tomorrow_feed[$key]['match_start_date2'] = date('M jS', strtotime($feed['match_start_date']));

                $tomorrow_date = Carbon::tomorrow(config('app.timezone_for_calculation'))->format('Y-m-d');
                $today_date = Carbon::now(config('app.timezone_for_calculation'))->format('Y-m-d');
                if (date('Y-m-d', strtotime($feed['match_start_date'])) == $tomorrow_date) {
                    $what_day = 'tomorrow';
                } else if (date('Y-m-d', strtotime($feed['match_start_date'])) == $today_date) {
                    $what_day = 'today';
                } else {
                    $what_day = 'day after';
                }
                $tomorrow_feed[$key]['what_day'] = $what_day;
            }
            $total_feed['tomorrow_feed'] = $tomorrow_feed;
        }
        $redis = LRedis::connection();

        if (isset($total_feed)) {
            $redis->publish('ScoreboardFeed', json_encode($total_feed));
        }
        echo 'Scoreboard First Send';
    }

    /**
     * 
     * Determine the previous feed array 
     * 
     * @return array 
     */
    private function _getPreviousFeed($previous_feed, $date_to_check, $found, $previous_feed_count, $rotate, $prev_get_count)
    {
        $last_competition = Competition::where(\DB::raw('date(match_start_date)'), '<', $date_to_check)->orderBy('match_start_date', 'DESC')->first();
        if ($last_competition == NULL) {
            return [];
        } elseif ($rotate == 0) {
            $date_to_check = $last_competition->match_start_date->toDateString();
        }

        $result = Competition::where('match_start_date', 'LIKE', '%' . $date_to_check . '%')->where('type', 1)->get();
        if (!$result->isEmpty()) {
            $result = $result->toArray();
            $previous_feed = array_merge($previous_feed, $result);
            $found = $found + 1;
            $rotate = $rotate + 1;
            if ($found != $prev_get_count && $found < $previous_feed_count) {
                $date_to_check = date('Y-m-d', strtotime('-1 day', strtotime($date_to_check)));
                return $this->_getPreviousFeed($previous_feed, $date_to_check, $found, $previous_feed_count, $rotate, $prev_get_count);
            } else {
                return $previous_feed;
            }
        } else {
            $rotate = $rotate + 1;
            $date_to_check = date('Y-m-d', strtotime('-1 day', strtotime($date_to_check)));
            return $this->_getPreviousFeed($previous_feed, $date_to_check, $found, $previous_feed_count, $rotate, $prev_get_count);
        }
    }

    /**
     * 
     * Determine the tomorrow feed array 
     * 
     * @return array 
     */
    private function _getTomorrowFeed($tomorrow_feed2, $date_to_check, $found, $tomorrow_feed_count, $rotate)
    {
        $result = Competition::where('match_start_date', 'LIKE', '%' . $date_to_check . '%')->where('type', 3)->get();
        if (!$result->isEmpty()) {
            $result = $result->toArray();
            $tomorrow_feed2 = array_merge($tomorrow_feed2, $result);
            $found = $found + 1;
            $rotate = $rotate + 1;
            if ($found != 2 && $rotate < $tomorrow_feed_count) {
                $date_to_check = date('Y-m-d', strtotime('+1 day', strtotime($date_to_check)));
                return $this->_getTomorrowFeed($tomorrow_feed2, $date_to_check, $found, $tomorrow_feed_count, $rotate);
            } else {
                return $tomorrow_feed2;
            }
        } else {
            $rotate = $rotate + 1;
            $date_to_check = date('Y-m-d', strtotime('+1 day', strtotime($date_to_check)));
            return $this->_getTomorrowFeed($tomorrow_feed2, $date_to_check, $found, $tomorrow_feed_count, $rotate);
        }
    }
}