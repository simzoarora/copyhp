<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use App\Models\Pool;
use App\Jobs\ProcessLiveDraft;
use DateTime;
use DateTimeZone;

/**
 * Description of LiveDraftsController
 *
 * @author Anik
 */
class LiveDraftsController extends Controller
{

    public function processPoolForLiveDraft()
    {
        $pools = Pool::active()->whereHas('liveDraftSetting', function($query) {
                    $query->where('in_queue', 0);
                })
                ->with('liveDraftSetting')
                ->get();
        foreach ($pools as $pool) {
            $livedraft_date = new DateTime($pool->liveDraftSetting->date . " " . $pool->liveDraftSetting->time, new DateTimeZone($pool->liveDraftSetting->time_zone));
            $livedraft_date->setTimezone(new DateTimeZone(config('app.timezone')));
            $now = new DateTime("now");
            if ($livedraft_date <= $now) {
                $job = (new ProcessLiveDraft($pool))->onQueue('live_draft');
                $this->dispatch($job);
                $pool->liveDraftSetting->in_queue = 1;
                $pool->liveDraftSetting->save();
            }
        }
    }
}