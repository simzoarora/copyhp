<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use SportsDirect\NHL;
use App\Models\Season;
use App\Models\Team;
use App\Models\Venue;
use App\Models\City;
use App\Models\SeasonCompetitionType;
use App\Models\Competition;

/**
 * Description of Schedule
 *
 * @author Anik
 */
class ScheduleController extends Controller
{

    public function fetchSeasonSchedule()
    {
        try {
            $nhl = new NHL(\Config('sportsdirectconfig.api_key'));
            $season_data = $nhl->getGameSchedule();
//        $season_data = json_decode(json_encode(new \SimpleXMLElement('http://hockeypool.anikgoel.com/schedule_NHL.xml', 0, true)), 1);
            if ($season_data) {
                $season_content = $season_data['team-sport-content']['league-content']['season-content'];
                $season = Season::firstOrNew(['name' => $season_content['season']['name']]);
                $season->start_date = $season_content['season']['details']['start-date'];
                $season->end_date = $season_content['season']['details']['end-date'];
                $season->save();

                foreach ($season_content['competition'] as $competition) {
                    $home_team = Team::getTeamFromApiIdString($competition['home-team-content']['team']['id']);
                    $away_team = Team::getTeamFromApiIdString($competition['away-team-content']['team']['id']);

                    if (!$home_team || !$away_team) {
                        continue;
                    }
                    $location = $competition['details']['venue']['location'];
                    $venue_city = City::findOrCreate($location['city'], (isset($location['state'])) ? $location['state'] : NULL, (isset($location['country'])) ? $location['country'] : NULL);
                    $venue = $venue_city->venues()->firstOrNew([
                        'api_id' => str_replace(config('constant.sportsdirectconstant.venue_id_prefix'), '', $competition['details']['venue']['id'])
                    ]);

                    if (is_array($competition['details']['venue']['name'])) {
                        $venue->name = $competition['details']['venue']['name'][0];
                        $venue->short_name = $competition['details']['venue']['name'][1];
                    } else {
                        $venue->name = $competition['details']['venue']['name'];
                    }

                    $venue->timezone = $competition['details']['venue']['location']['timezone'];
                    $venue->save();


                    $db_competition = $season->competitions()->firstOrNew(['api_id' => str_replace(config('constant.sportsdirectconstant.competition_id_prefix'), "", $competition['id'])]);
                    $db_competition->match_start_date = date('Y-m-d H:i:s', strtotime($competition['start-date']));
                    $db_competition->match_status = $competition['result-scope']['competition-status'];
                    $db_competition->home_team_id = $home_team->id;
                    $db_competition->away_team_id = $away_team->id;
                    $db_competition->venue_id = $venue->id;

                    if (isset(config('competition.inverse_type')[$competition['result-scope']['competition-status']])) {
                        $db_competition->type = config('competition.inverse_type')[$competition['result-scope']['competition-status']];
                    }
                    if (isset(config('competition.inverse_competition_type')[$competition['details']['competition-type']])) {
                        $db_competition->competition_type = config('competition.inverse_competition_type')[$competition['details']['competition-type']];
                    }
                    $db_competition->save();
                }
            } else {
                echo 'DATA NOT FETCHED';
            }
        } catch (\Exception $ex) {
            \Log::alert("fetchSeasonSchedule : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }

    public function saveCompetitionTypes()
    {
        try {
            $current_season = Season::where('current', 1)->firstOrFail();
            foreach (config('competition.inverse_competition_type') as $competition_type) {
//            dd($competition_type);
                $season_competition_type = $current_season->seasonCompetitionTypes()->firstOrNew(['competition_type' => $competition_type]);
                $competition_start = $current_season->competitions()->where('competition_type', $competition_type)->orderBy('match_start_date')->first();
                $competition_end = $current_season->competitions()->where('competition_type', $competition_type)->orderBy('match_start_date', 'desc')->first();
                if ($competition_start && $competition_end) {
                    $season_competition_type->start_date = $competition_start->match_start_date->format('Y-m-d');
                    $season_competition_type->end_date = $competition_end->match_start_date->format('Y-m-d');
                    $season_competition_type->save();
                }
            }
        } catch (\Exception $ex) {
            \Log::alert("saveCompetitionTypes : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }
}