<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use App\Models\Pool;
use App\Models\SeasonCompetitionType;
use App\Models\Season;
use App\Models\PoolTeamPlayer;
use App\Models\PoolStandardScore;
use App\Models\PoolStandardRottoResult;
use App\Models\PoolScoreSetting;
use App\Models\StoredValue;
use App\Models\Competition;
use App\Models\SeasonCompetitionTypeWeek;
use App\Models\PoolMatchup;
use App\Models\PoolMatchupResult;
use App\Models\PlayerStat;
use App\Models\CompetitionGoalieStat;
use App\Models\CompetitionSkaterStat;
use Carbon\Carbon;

/**
 * Description of PoolController
 *
 * @author Anik
 */
class PoolsController extends Controller
{

    private function getSeasonCompetitionTypeForMatchup($current_season,$competition_type_number)
    {
        return $current_season->seasonCompetitionTypes()->with('seasonCompetitionTypeWeeks')->where('competition_type', $competition_type_number)->first();
    }

    /**
     * This function create weeks for season.
     */
    public function createSeasonWeeks()
    {
        try {
            $current_season = Season::where('current', 1)->firstOrFail();
            foreach (config('competition.inverse_competition_type') as $competition_type_number) {
                $competition_type = $this->getSeasonCompetitionTypeForMatchup($current_season,$competition_type_number);
                if($competition_type == null){
                    continue;
                }
                $start_date = $competition_type->start_date->toDateString();
                $end_date = $competition_type->start_date->next(0)->toDateString(); // 0 is for sunday. carbon constant not working
                $this->_saveCompetitionWeek($competition_type, $start_date, $end_date, 'Week 1');
                $i = 1;
                $competition_end_date = $competition_type->end_date->toDateString();
                while ($end_date < $competition_end_date) {
                    $start_date = date('Y-m-d', strtotime($end_date . ' +1 day'));
                    $end_date = date('Y-m-d', strtotime($start_date . ' +6 days'));
                    $this->_saveCompetitionWeek($competition_type, $start_date, $end_date, 'Week ' . ++$i);
                }
            }
        } catch (\Exception $ex) {
            \Log::alert("createSeasonWeeks : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'done';
    }

    private function _saveCompetitionWeek($competition_type, $start_date, $end_date, $name)
    {
        $season_competition_type_week = $competition_type->seasonCompetitionTypeWeeks()->firstOrNew([
            'name' => $name,
        ]);
        $season_competition_type_week->start_date = $start_date;
        $season_competition_type_week->end_date = $end_date;
        $season_competition_type_week->save();
    }

    public function createPlayOffMatches()
    {
        try {
            $week = SeasonCompetitionTypeWeek::where('start_date', '<=', date('Y-m-d'))->where('start_date', '>=', date('Y-m-d'))->first();
            if ($week != NULL) {
                $pools = Pool::active()
                        ->has('currentSeason')
                        ->where('pool_type', config('pool.inverse_type.h2h'))
                        ->with([
                            'poolSetting.poolsetting'
                        ])
                        ->get();
                foreach ($pools as $pool) {
                    if ($pool->poolSetting->poolsetting->playoff_format == config('pool.inverse_playoff_format.playoff')) {
                        $pool_matchup = PoolMatchup::whereHas('poolTeam1', function($query) use($pool) {
                                    $query->where('pool_id', $pool->id);
                                })
                                ->with('seasonCompetitionTypeWeek')
                                ->orderBy('id', 'desc')
                                ->first();

                        if ($pool_matchup->seasonCompetitionTypeWeek->end_date->gt(Carbon::now())) {
                            // If last match end date is in future, that means regular games have not yet completed
                            // so we will not create playoff for them
                            continue;
                        }

                        if ($pool_matchup->type == config('pool.pool_matchups.inverse_type.regular_matches')) {
                            $pool->load([
                                'poolTeams.poolMatchupResult',
                            ]);
                            $sorted_pool_teams = $pool->poolTeams->sortByDesc('poolMatchupResult.total_score');
                            $sorted_pool_team_tie_breaker = $this->_performTieBreakerForRegularMatches($sorted_pool_teams);

                            $eliminated_teams = $sorted_pool_team_tie_breaker->splice($pool->poolSetting->poolsetting->playoff_teams_number);

                            $playoff_teams = $sorted_pool_team_tie_breaker->toArray();
                            $chunked_pool_team = array_chunk($playoff_teams, (count($playoff_teams) / 2)); // divide teams in 2 parts to create
                            $inverse_second_array = array_reverse($chunked_pool_team[1]);
                            foreach ($chunked_pool_team[0] as $pool_team_key => $pool_team) {
                                $pool->poolMatchups()->firstOrCreate([
                                    'team1' => $pool_team['id'],
                                    'team2' => $inverse_second_array[$pool_team_key]['id'],
                                    'season_competition_type_week_id' => $week->id,
                                    'type' => config('pool.pool_matchups.inverse_type.playoff_matches'),
                                ]);
                            }
                        } else {
                            $all_previous_playoffs = PoolMatchup::whereHas('poolTeam1', function($query) use($pool) {
                                        $query->where('pool_id', $pool->id);
                                    })
                                    ->where('season_competition_type_week_id', $pool_matchup->season_competition_type_week_id)
                                    ->with('poolMatchupResults.poolTeam')
                                    ->get();

                            $next_playoff_team_array = $this->_performTieBreakerForPlayoffMatches($all_previous_playoffs);
                            if (count($next_playoff_team_array) > 1) {
                                $chunked_pool_team = array_chunk($next_playoff_team_array, (count($next_playoff_team_array) / 2)); // divide teams in 2 parts to create
                                $inverse_second_array = array_reverse($next_playoff_team_array[1]);

                                foreach ($chunked_pool_team[0] as $pool_team_key => $pool_team_id) {
                                    $pool->poolMatchups()->firstOrCreate([
                                        'team1' => $pool_team_id,
                                        'team2' => $inverse_second_array[$pool_team_key],
                                        'season_competition_type_week_id' => $week->id,
                                        'type' => config('pool.pool_matchups.inverse_type.playoff_matches'),
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            \Log::alert("createPlayOffMatches : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'PLAYOFF CRON DONE';
        return;
    }

    private function _performTieBreakerForPlayoffMatches($all_previous_playoffs)
    {
        $next_playoff_team_array = [];
        foreach ($all_previous_playoffs as $playoff) {
            if ($playoff->poolMatchupResults->count() != 2) {
                continue;
            }
            $team1_result = $playoff->poolMatchupResults->get(0);
            $team2_result = $playoff->poolMatchupResults->get(1);
            $pool_team = $team1_result->poolTeam;
            $previous_team = $team2_result->poolTeam;
            if ($team1_result->score == $team2_result->score) {
                //FURTHER TIE BREAKER
                $matchup_results = PoolMatchupResult::whereHas('poolMatchup', function($query) use($pool_team, $previous_team) {
                            $query->where(function($query) use($pool_team, $previous_team) {
                                $query->where(function($query) use($pool_team, $previous_team) {
                                    $query->where('team1', $pool_team->id)
                                    ->where('team2', $previous_team->id);
                                })
                                ->orWhere(function($query) use($pool_team, $previous_team) {
                                    $query->where('team2', $pool_team->id)
                                    ->where('team1', $previous_team->id);
                                });
                            });
                        })
                        ->selectRaw("sum(score) as total_score, pool_team_id, pool_matchup_id")
                        ->groupBy('pool_team_id')
                        ->get();
                if ($matchup_results->count() == 2) {
                    // result of both team should be there
                    $pool_team_score = $matchup_results->where('pool_team_id', $pool_team->id)->first()->total_score;
                    $previous_team_score = $matchup_results->where('pool_team_id', $previous_team->id)->first()->total_score;
                    if ($pool_team_score == $previous_team_score) {
                        // Team still tied, team which drafted later will win this tie breaker
                        if ($pool_team->sort_order > $previous_team->sort_order) {
                            $next_playoff_team_array[] = $pool_team->id;
                        } else {
                            $next_playoff_team_array[] = $previous_team->id;
                        }
                    } elseif ($pool_team_score > $previous_team_score) {
                        $next_playoff_team_array[] = $pool_team->id;
                    } else {
                        $next_playoff_team_array[] = $previous_team->id;
                    }
                } else {
                    // if there is not match of both team (which would be less chances)
                    // team which drafted later will win this tie breaker
                    if ($pool_team->sort_order > $previous_team->sort_order) {
                        $next_playoff_team_array[] = $pool_team->id;
                    } else {
                        $next_playoff_team_array[] = $previous_team->id;
                    }
                }
            } elseif ($team1_result->score > $team2_result->score) {
                $next_playoff_team_array[] = $team1_result->pool_team_id;
            } else {
                $next_playoff_team_array[] = $team2_result->pool_team_id;
            }
        }
        return $next_playoff_team_array;
    }

    private function _performTieBreakerForRegularMatches($sorted_pool_teams)
    {
        $previous_team = NULL;
        $sorted_pool_team_tie_breaker = collect([]);
        $total_teams = $sorted_pool_teams->count();
        $iterator = 0;
        foreach ($sorted_pool_teams as $key => $pool_team) {
            $change_prev_team = TRUE;
            $iterator++;
            if ($previous_team != NULL) {
                if ($pool_team->poolMatchupResult != NULL && $previous_team->poolMatchupResult != NULL && $pool_team->poolMatchupResult->total_score == $previous_team->poolMatchupResult->total_score) {
                    // Team tied, Tie breaker based on total wins of team
                    if ($pool_team->poolMatchupResult->total_win == $previous_team->poolMatchupResult->total_win) {
                        // Team still tied, Tie breaker based on the score of both team when they were matching up with each other
                        $matchup_results = PoolMatchupResult::whereHas('poolMatchup', function($query) use($pool_team, $previous_team) {
                                    $query->where(function($query) use($pool_team, $previous_team) {
                                        $query->where(function($query) use($pool_team, $previous_team) {
                                            $query->where('team1', $pool_team->id)
                                            ->where('team2', $previous_team->id);
                                        })
                                        ->orWhere(function($query) use($pool_team, $previous_team) {
                                            $query->where('team2', $pool_team->id)
                                            ->where('team1', $previous_team->id);
                                        });
                                    });
                                })
                                ->selectRaw("sum(score) as total_score, pool_team_id, pool_matchup_id")
                                ->groupBy('pool_team_id')
                                ->get();

                        if ($matchup_results->count() == 2) {
                            // result of both team should be there
                            $pool_team_score = $matchup_results->where('pool_team_id', $pool_team->id)->first()->total_score;
                            $previous_team_score = $matchup_results->where('pool_team_id', $previous_team->id)->first()->total_score;
                            if ($pool_team_score == $previous_team_score) {
                                // Team still tied, team which drafted later will win this tie breaker
                                if ($pool_team->sort_order > $previous_team->sort_order) {
                                    $sorted_pool_team_tie_breaker->push($pool_team);
                                    $change_prev_team = FALSE;
                                } else {
                                    $sorted_pool_team_tie_breaker->push($previous_team);
                                }
                            } elseif ($pool_team_score > $previous_team_score) {
                                $sorted_pool_team_tie_breaker->push($pool_team);
                                $change_prev_team = FALSE;
                            } else {
                                $sorted_pool_team_tie_breaker->push($previous_team);
                            }
                        } else {
                            // if there is not match of both team (which would be less chances)
                            // team which drafted later will win this tie breaker
                            if ($pool_team->sort_order > $previous_team->sort_order) {
                                $sorted_pool_team_tie_breaker->push($pool_team);
                                $change_prev_team = FALSE;
                            } else {
                                $sorted_pool_team_tie_breaker->push($previous_team);
                            }
                        }
                    } elseif ($pool_team->poolMatchupResult->total_win > $previous_team->poolMatchupResult->total_win) {
                        $sorted_pool_team_tie_breaker->push($pool_team);
                        $change_prev_team = FALSE;
                    } else {
                        $sorted_pool_team_tie_breaker->push($previous_team);
                    }
                } else {
                    if ($pool_team->poolMatchupResult == NULL && $previous_team->poolMatchupResult != NULL) {
                        $sorted_pool_team_tie_breaker->push($previous_team);
                    } else {
                        if ($pool_team->sort_order > $previous_team->sort_order) {
                            $sorted_pool_team_tie_breaker->push($pool_team);
                            $change_prev_team = FALSE;
                        } else {
                            $sorted_pool_team_tie_breaker->push($previous_team);
                        }
                    }
                }
            }
            if ($change_prev_team) {
                $previous_team = $pool_team;
            }
            if ($iterator == $total_teams) {
                // checking for last team in loop, it needs to be added here only
                if ($change_prev_team) {
                    $sorted_pool_team_tie_breaker->push($pool_team);
                } else {
                    $sorted_pool_team_tie_breaker->push($previous_team);
                }
            }
        }
        return $sorted_pool_team_tie_breaker;
    }

    public function addPlayerRankForPreviousSeason()
    {
        try {
//        \DB::enableQueryLog();
            ini_set('max_execution_time', 0);
            ini_set('memory_limit', 268435456);
            $seasons = Season::where('current', 0)->get();
            $seasons->each(function($season) {
                $pool_item = Pool::active()->whereHas('seasonPoolPoints', function($query) use($season) {
                            $query->where('season_id', $season->id);
                        })
                        ->has('currentSeason')
                        ->with([
                            'seasonPoolPoints' => function($query) use($season) {
                                $query->where('season_id', $season->id)->orderBy('total_points', 'desc');
                            },
                        ])
                        ->whereNotExists(function($subquery)use($season) {
                            /**
                             *  We are checking if this pool data for previous season
                             *  has already been added or not. We will get only those pool
                             *  whose previous season data is not there
                             */
                            $subquery->select(\DB::raw(1))
                            ->from('pool_season_ranks')
                            ->whereRaw('pool_season_ranks.pool_id=pools.id')
                            ->where('season_id', $season->id);
                        })
                        ->orderBy('id')
                        ->first();
                if ($pool_item != NULL) {
//            $pools->each(function($pool_item) use($season) {
                    $pool_item->seasonPoolPoints->each(function($point_data, $key) use($pool_item, $season) {
                        $pool_season_rank = $pool_item->poolSeasonRanks()->firstOrNew([
                            'season_id' => $season->id,
                            'player_id' => $point_data->player_id
                        ]);
                        $pool_season_rank->rank = $key + 1;
                        $pool_season_rank->save();
                    });
//            });
                }
            });
        } catch (\Exception $ex) {
            \Log::alert("addPlayerRankForPreviousSeason : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'done';
//        dd(\DB::getQueryLog());
    }

    public function addPlayerRankForCurrentSeason()
    {
        try {
//        \DB::enableQueryLog();
            ini_set('max_execution_time', 0);
            ini_set('memory_limit', 512435456);
            $season = Season::where('current', 1)->firstOrFail();
            $pools = Pool::active()->whereHas('seasonPoolPoints', function($query) use($season) {
                        $query->where('season_id', $season->id);
                    })
                    ->has('currentSeason')
                    ->with([
                        'seasonPoolPoints' => function($query) use($season) {
                            $query->where('season_id', $season->id)->orderBy('total_points', 'desc');
                        },
                    ])
                    ->orderBy('id')
                    ->get();
            if ($pools != NULL) {
                $pools->each(function($pool_item) use($season) {
                    $pool_item->seasonPoolPoints->each(function($point_data, $key) use($pool_item, $season) {
                        $pool_season_rank = $pool_item->poolSeasonRanks()->firstOrNew([
                            'season_id' => $season->id,
                            'player_id' => $point_data->player_id
                        ]);
                        $pool_season_rank->rank = $key + 1;
                        $pool_season_rank->save();
                    });
                });
            }
        } catch (\Exception $ex) {
            \Log::alert("addPlayerRankForCurrentSeason : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
//        dd(\DB::getQueryLog());
    }

    public function addPoolScoreForCurrentSeason()
    {
        try {
//        \DB::enableQueryLog();
            ini_set('max_execution_time', 0);
            $stored_value = StoredValue::where('key', 'current_season_pool_score_last_pool')->firstOrFail();

            $pool_item = Pool::active()->has('currentSeason')
                    ->with([
                        'poolScoreSettings.poolScoringField',
                    ])
                    ->where('id', '>', $stored_value->value)
                    ->orderBy('id')
                    ->first();
            if ($pool_item == null) {
                //resetting pool id so that it start again next time we run cron
                $stored_value->value = 0;
                $stored_value->save();
                return;
            }

            $season = Season::where('current', 1)
                    ->firstOrFail();
            $season->load([
                'players.playerSeason',
                'players.playerStats' => function($query) use($season, $pool_item) {
                    $query->where('stat_key', PlayerStat::getStatKeyBySeasonType($pool_item->season_type))
                            ->where('season_id', '=', $season->id);
                }
            ]);

            $this->_savePoolScoringForSeason($season, $pool_item);
            $stored_value->value = $pool_item->id;
            $stored_value->save();
//        dump(\DB::getQueryLog());
//        dd($season->toArray());
        } catch (\Exception $ex) {
            \Log::alert("addPoolScoreForCurrentSeason : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }

    /**
     * This function is only used for previous season records.
     * We have to run this function multiple times in a day for fetching it for every
     * pool. It takes around 2 minutes for a single pool.
     * @return type
     */
    public function addPoolScoreForPreviousSeasons()
    {
        try {
            ini_set('max_execution_time', 0);
//        \DB::enableQueryLog();
            $season = Season::where('current', 1)
                    ->firstOrFail();

            $pool_item = Pool::active()
                    ->where('is_completed', 1)
                    ->has('currentSeason')
                    ->with([
                        'poolScoreSettings.poolScoringField',
                    ])
                    ->whereNotExists(function($subquery)use($season) {
                        /**
                         *  We are checking if this pool data for previous season
                         *  has already been added or not. We will get only those pool
                         *  whose previous season data is not there
                         */
                        $subquery->select(\DB::raw(1))
                        ->from('season_pool_points')
                        ->whereRaw('season_pool_points.pool_id=pools.id')
                        ->where('season_id', '<>', $season->id);
                    })
                    ->orderBy('id')
                    ->first();
            if ($pool_item == null) {
                return;
            }

            $season->load([
                'players.playerSeason',
                'players.playerStats' => function($query) use($season, $pool_item) {
                    $query->where('stat_key', PlayerStat::getStatKeyBySeasonType($pool_item->season_type))
                            ->where('season_id', '<>', $season->id);
                }
            ]);

            $this->_savePoolScoringForSeason($season, $pool_item);
//        dump(\DB::getQueryLog());
        } catch (\Exception $ex) {
            \Log::alert("addPoolScoreForPreviousSeasons : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }

    private function _savePoolScoringForSeason($season, $pool_item)
    {
        $season->players->each(function($player_item) use($pool_item, $season) {
            if ($player_item->playerStats->isEmpty()) {
                $season_pool_points = $pool_item->seasonPoolPoints()->firstOrNew([
                    'season_id' => $season->id,
                    'player_id' => $player_item->id,
                ]);
                $season_pool_points->total_points = 0;
                $season_pool_points->save();
            } else {
                $player_item->playerStats->each(function($stat_item) use($pool_item, $player_item) {
                    $score = 0;
                    foreach ($pool_item->poolScoreSettings as $score_setting_item) {
                        if ($score_setting_item->type == config('pool.pool_scoring_field.goalie') && $player_item->playerSeason->player_position_id != config('pool.inverse_player_position.g')) {
                            continue;
                        }
                        $field_name = config('pool.pool_scoring_field.fields_map_to_stat_table')[$score_setting_item->pool_scoring_field_id];
                        if ($field_name != '' && isset($stat_item->$field_name)) {
                            $stat = $stat_item->$field_name;
                            $score += $stat * $score_setting_item->value;
                        }
                    }
                    $season_pool_points = $pool_item->seasonPoolPoints()->firstOrNew([
                        'season_id' => $stat_item->season_id,
                        'player_id' => $player_item->id,
                    ]);
                    $season_pool_points->total_points = number_format($score, 3);
                    $season_pool_points->save();
                });
            }
        });
    }

    public function checkGoalieGamesAndRemoveStats()
    {
        try {
            $pools = Pool::active()
                    ->has('currentSeason')
                    ->where('pool_type', config('pool.inverse_type.h2h'))
                    ->with([
                        'poolTeams.lastWeekPoolMatchupResult.poolMatchup',
                        'poolSetting.poolsetting'
                    ])
                    ->get();
            foreach ($pools as $pool) {
                foreach ($pool->poolTeams as $pool_team) {
                    $last_week = Carbon::now()->subWeek();
                    $compare_array = [
                        $last_week->startOfWeek()->toDateString(),
                        $last_week->endOfWeek()->toDateString(),
                    ];
                    $goalie_competitions = Competition::whereBetween(\DB::raw('DATE(match_start_date)'), $compare_array)
                                    ->whereHas('competitionGoalieStats.playerSeason.poolTeamPlayers', function($query) use($pool_team) {
                                        $query->where('pool_team_id', $pool_team->id)
                                        ->withTrashed()
                                        ->whereHas('poolTeamLineup', function($query) {
                                            $query->whereRaw('pool_team_lineups.start_time <= DATE(competitions.match_start_date)')
                                            ->whereRaw('pool_team_lineups.end_time >= DATE(competitions.match_start_date)')
                                            ->where('pool_team_lineups.player_position_id', '<>', config('pool.inverse_player_position.bn'))
                                            ->withTrashed();
                                        });
                                    })->get();
                    if ($goalie_competitions->count() < $pool->poolSetting->poolsetting->min_goalie_appearance) {
                        foreach ($goalie_competitions as $competition) {
                            $pool_team->poolH2hScores()
                                    ->whereHas('poolTeamPlayer', function($query) {
                                        $query->whereHas('playerSeason', function($query) {
                                            $query->where('player_position_id', config('pool.inverse_player_position.g'));
                                        })->withTrashed();
                                    })
                                    ->where('competition_id', $competition->id)
                                    ->delete();
                        }
                        if ($pool_team->lastWeekPoolMatchupResult != NULL) {
                            $pool_matchup = $pool_team->lastWeekPoolMatchupResult->poolMatchup;
                            if ($pool->poolSetting->poolsetting->pool_format == config('pool.inverse_format.h2h_cat')) {
                                $this->saveMatchupH2HCatResult($pool_matchup);
                            } else {
                                $this->saveMatchupH2HWeeklyResult($pool_matchup);
                            }
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            \Log::alert("checkGoalieGamesAndRemoveStats : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'done';
    }

    public function addPoolScore()
    {
        try {
            $current_season = Season::where('current', 1)->with('pools.poolScoreSettings.poolScoringField')->firstOrFail();
            $pool_score_last_pool = StoredValue::where('key', 'pool_score_last_pool')->first();
            $competition = $current_season->competitions()
//                    ->where('match_status', 'LIKE', '%Final%')
                    ->with('competitionSkaterStats.playerSeason')
                    ->with('competitionGoalieStats.playerSeason')
                    ->where(\DB::raw('DATE(match_start_date)'), '>=', date('Y-m-d', strtotime('- 1 day')))
                    ->where('id', '>', $pool_score_last_pool->value)
                    ->has('competitionSkaterStats')
                    ->first();
            if ($competition == NULL) {
                $pool_score_last_pool->value = 0;
                $pool_score_last_pool->save();
                return;
            }
            // Merging skater and goalie stats
            $competition->competitionSkaterStats->transform(function($stat_item) use($competition) {
                $goalie_data = $competition->competitionGoalieStats->where('player_season_id', $stat_item->player_season_id);
                if (!$goalie_data->isEmpty()) {
                    $goalie_data->each(function($goalie_item, $key)use($stat_item) {
                        $actual_data = $goalie_item->toArray();
                        foreach ($actual_data as $key_to_add => $value_to_add) {
                            $stat_item->$key_to_add = $value_to_add;
                        }
                    });
                    $stat_item->save_percentage = ($stat_item->shots_against != 0) ? (($stat_item->shots_against - $stat_item->goals_against) / $stat_item->shots_against) : NULL;
                }
                return $stat_item;
            });

            foreach ($competition->competitionSkaterStats as $player_stat) {
                $this->prepareForSavingScore($player_stat, $competition, $current_season->pools);
            }
            $pool_score_last_pool->value = $competition->id;
            $pool_score_last_pool->save();
        } catch (\Exception $ex) {
            \Log::alert("addPoolScore : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'done';
    }

    private function prepareForSavingScore($player_stat, $competition, $pools)
    {
        $this->prepareForH2hScore($player_stat, $competition);
        $this->prepareForStandardScore($player_stat, $competition);
        $this->saveScoreForEachPool($player_stat, $competition, $pools);
    }

    private function saveScoreForEachPool($player_stat, $competition, $pools)
    {
        $pools->each(function($pool_item, $key)use($player_stat, $competition) {
            $total_score = 0;
            foreach ($pool_item->poolScoreSettings as $setting_item) {
                if ($setting_item->type == config('pool.pool_scoring_field.inverse_type.goalie') && $player_stat->playerSeason->player_position_id != config('pool.inverse_player_position.g')) {
                    continue;
                }
                $field_name = config('pool.pool_scoring_field.fields_map_to_stat_table')[$setting_item->pool_scoring_field_id];
                if ($field_name != '' && isset($player_stat->$field_name)) {
                    $stat = $player_stat->$field_name;
                    $total_score += $stat * $setting_item->value;
                }
            }
            $calculated_score = $pool_item->calculatedPoolPoints()->firstOrNew([
                'season_id' => $competition->season_id,
                'player_id' => $player_stat->playerSeason->player_id,
                'points_date' => $competition->match_start_date->format('Y-m-d')
            ]);
            $calculated_score->competition_id = $competition->id;
            $calculated_score->total_points = $total_score;
            $calculated_score->save();
        });
    }

    private function prepareForH2hScore($player_stat, $competition)
    {
        $pool_team_players = PoolTeamPlayer::getPlayerForScoreCalculation($player_stat->player_season_id, $competition->match_start_date, config('pool.inverse_type.h2h'), NULL, $competition->competition_type);
        foreach ($pool_team_players as $pool_team_player) {
            foreach ($pool_team_player->poolTeam->pool->poolScoreSettings as $pool_score_setting) {
                if ($pool_score_setting->type == config('pool.pool_scoring_field.inverse_type.goalie') && $pool_team_player->playerSeason->player_position_id != config('pool.inverse_player_position.g')) {
                    continue;
                }
                if (isset($player_stat->{config('pool.pool_scoring_field.fields_map_to_stat_table')[$pool_score_setting->pool_scoring_field_id]})) {
                    $score = $this->getPlayerStatScore($player_stat, $pool_score_setting);
                    if ($pool_team_player->poolTeam->pool->pool_type == config('pool.inverse_type.h2h')) {
                        if (!$pool_team_player->poolTeam->poolMatchupTeam1->isEmpty()) {
                            $this->savePoolScore($pool_team_player, $pool_score_setting, 1, $score, $competition->id);
                        } elseif (!$pool_team_player->poolTeam->poolMatchupTeam2->isEmpty()) {
                            $this->savePoolScore($pool_team_player, $pool_score_setting, 2, $score, $competition->id);
                        }
                    }
                }
            }
        }
    }

    private function prepareForStandardScore($player_stat, $competition)
    {
        $pool_team_players = PoolTeamPlayer::getPlayerForScoreCalculation($player_stat->player_season_id, $competition->match_start_date, config('pool.inverse_type.standard'), config('pool.inverse_type.box'), $competition->competition_type);
        foreach ($pool_team_players as $pool_team_player) {
            foreach ($pool_team_player->poolTeam->pool->poolScoreSettings as $pool_score_setting) {
                if ($pool_score_setting->type == config('pool.pool_scoring_field.inverse_type.goalie') && $pool_team_player->playerSeason->player_position_id != config('pool.inverse_player_position.g')) {
                    continue;
                }
                if (isset($player_stat->{config('pool.pool_scoring_field.fields_map_to_stat_table')[$pool_score_setting->pool_scoring_field_id]})) {
                    $score = $this->getPlayerStatScore($player_stat, $pool_score_setting);
                    if ($pool_team_player->poolTeam->pool->pool_type == config('pool.inverse_type.standard') ||
                            $pool_team_player->poolTeam->pool->pool_type == config('pool.inverse_type.box')) {
                        $pool_standard_score = $pool_team_player->poolStandardScores()->firstOrNew([
                            'pool_team_id' => $pool_team_player->poolTeam->id,
                            'pool_score_setting_id' => $pool_score_setting->id,
                            'competition_id' => $competition->id
                        ]);
                        $stat_value = ($pool_score_setting->value != NULL) ? $pool_score_setting->value : 1; //adding lifetime stat to value by keeping value to 1
                        $pool_standard_score->value = $score * $stat_value;
                        $pool_standard_score->original_stat = $score;
                        $pool_standard_score->save();
                        $pool_setting = $pool_team_player->poolTeam->pool->poolSetting->poolsetting;
                        if (isset($pool_setting->pool_format) && $pool_setting->pool_format == config('pool.inverse_format.standard_rotisserie')) {
                            $this->saveStandardRottoResult($pool_standard_score);
                        }
                    }
                }
            }
        }
    }

    private function saveStandardRottoResult($pool_standard_score)
    {
        $pool_teams = array_map('intval', $pool_standard_score->poolTeam->pool->poolTeams()->lists('id')->all());
        $total_pool_teams = count($pool_teams);
        $pool_scores = PoolStandardScore::whereIn('pool_team_id', $pool_teams)
                ->groupBy('pool_team_id')
                ->groupBy('pool_score_setting_id')
                ->selectRaw('sum(value) as score,pool_team_id,pool_score_setting_id')
                ->get();
        $pre_result = [];
        foreach ($pool_scores as $pool_score) {
            if (!isset($pre_result[$pool_score->pool_score_setting_id])) {
                $pre_result[$pool_score->pool_score_setting_id] = [];
            }
            $pre_result[$pool_score->pool_score_setting_id][$pool_score->pool_team_id] = $pool_score->score;
        }

        /**
         * adding those score setting which are not yet being scored by any player
         */
        $pool_score_settings = $pool_standard_score->poolTeam->pool->poolScoreSettings()->lists('id')->all();
        foreach ($pool_score_settings as $pool_score_setting_id) {
            if (!isset($pre_result[$pool_score_setting_id])) {
                $pre_result[$pool_score_setting_id] = [];
            }
        }
        /**
         * ends
         */
        $this->addStandardRottoResult($pre_result, $total_pool_teams, $pool_teams);
    }

    private function addStandardRottoResult($pre_result, $total_pool_teams, $pool_teams)
    {
        foreach ($pre_result as $pool_score_setting_id => $data) {
            $score_starting_point = $total_pool_teams;
            $pool_score_setting_data = PoolScoreSetting::find($pool_score_setting_id);
            arsort($data);
            $teams_added = array_keys($data);
            foreach ($teams_added as $pool_team_id) {
                $pool_result = PoolStandardRottoResult::firstOrNew([
                            'pool_team_id' => $pool_team_id,
                            'pool_scoring_field_id' => $pool_score_setting_data->pool_scoring_field_id,
                ]);
                $pool_result->value = $score_starting_point;
                $pool_result->save();
                $score_starting_point--;
            }
            if (count($data) != $total_pool_teams) {
                $remaining_teams = array_diff($pool_teams, $teams_added);
                $positions = range(1, $total_pool_teams - count($teams_added));
                $average = array_sum($positions) / count($positions);
                foreach ($remaining_teams as $pool_team_id) {
                    $pool_result = PoolStandardRottoResult::firstOrNew([
                                'pool_team_id' => $pool_team_id,
                                'pool_scoring_field_id' => $pool_score_setting_data->pool_scoring_field_id,
                    ]);
                    $pool_result->value = $average;
                    $pool_result->save();
                }
            }
        }
    }

    private function getPlayerStatScore($player_stat, $pool_score_setting)
    {
        return $player_stat->{config('pool.pool_scoring_field.fields_map_to_stat_table')[$pool_score_setting->pool_scoring_field_id]};
    }

    private function savePoolScore($pool_team_player, $pool_score_setting, $key, $score, $competition_id)
    {
        foreach ($pool_team_player->poolTeam->{'poolMatchupTeam' . $key} as $pool_matchup) {
            $pool_score = $pool_matchup->poolH2hScores()->firstOrNew([
                'pool_score_setting_id' => $pool_score_setting->id,
                'competition_id' => $competition_id,
                'pool_team_player_id' => $pool_team_player->id,
                'pool_team_id' => $pool_matchup->{'poolTeam' . $key}->id
            ]);
            if ($pool_score_setting->value == NULL) {
                $stat_value = 1; //adding lifetime stat to value
            } else {
                $stat_value = $pool_score_setting->value;
            }
            $pool_score->value = $score * $stat_value;
            $pool_score->original_stat = $score;
            $pool_score->save();
            if ($pool_team_player->poolTeam->pool->poolSetting->poolsetting->pool_format == config('pool.inverse_format.h2h_cat')) {
                $this->saveMatchupH2HCatResult($pool_matchup);
            } elseif ($pool_team_player->poolTeam->pool->poolSetting->poolsetting->pool_format == config('pool.inverse_format.h2h_weekly') ||
                    $pool_team_player->poolTeam->pool->poolSetting->poolsetting->pool_format == config('pool.inverse_format.h2h_weekly_daily')) {
                $this->saveMatchupH2HWeeklyResult($pool_matchup);
            }
        }
    }

    private function saveMatchupH2HWeeklyResult($pool_matchup)
    {
        $pool_matchup_scores = $pool_matchup->poolH2hScores()->groupBy('pool_team_id')
                        ->selectRaw('sum(value) as score, pool_team_id')
                        ->lists('score', 'pool_team_id')->all();
        if (count($pool_matchup_scores) < 2) {
            return; // only go ahead if data of both team available
        }
        $first = array_slice($pool_matchup_scores, 0, 1, true);
        $second = array_slice($pool_matchup_scores, 1, 1, true);
        $team_first = key($first);
        $team_second = key($second);
        $final_result = [
            $team_first => [
                'win' => 0,
                'loss' => 0,
                'tie' => 0,
                'score' => 0,
            ],
            $team_second => [
                'win' => 0,
                'loss' => 0,
                'tie' => 0,
                'score' => 0,
            ],
        ];
        if (current($first) == current($second)) {
            $final_result[$team_first]['tie'] = 1;
            $final_result[$team_second]['tie'] = 1;
            $final_result[$team_second]['score'] += config('pool.scoring.tie');
            $final_result[$team_first]['score'] += config('pool.scoring.tie');
        } elseif (current($first) > current($second)) {
            $final_result[$team_first]['win'] = 1;
            $final_result[$team_second]['loss'] = 1;
            $final_result[$team_first]['score'] += config('pool.scoring.win');
        } else {
            $final_result[$team_second]['win'] = 1;
            $final_result[$team_first]['loss'] = 1;
            $final_result[$team_second]['score'] += config('pool.scoring.win');
        }
        foreach ($final_result as $team_id => $data) {
            $pool_matchup_result = $pool_matchup->poolMatchupResults()->firstOrNew([
                'pool_team_id' => $team_id
            ]);
            $pool_matchup_result->win = $data['win'];
            $pool_matchup_result->loss = $data['loss'];
            $pool_matchup_result->tie = $data['tie'];
            $pool_matchup_result->score = $data['score'];
            $pool_matchup_result->save();
        }
    }

    private function getGoalieAndSkaterStat($pool_matchup, $team_player_season_ids, $team_id)
    {
        $goalie_stat = CompetitionGoalieStat::whereHas('competition', function($query) use($pool_matchup) {
                    $query->where(\DB::raw('DATE(match_start_date)'), '>=', $pool_matchup->seasonCompetitionTypeWeek->start_date->toDateString())
                    ->where(\DB::raw('DATE(match_start_date)'), '<=', $pool_matchup->seasonCompetitionTypeWeek->end_date->toDateString());
                })
                ->selectRaw('sum(shots_against) as shots_against, sum(goals_against) as goals_against')
                ->whereIn('player_season_id', $team_player_season_ids[$team_id])
                ->first();

        $skater_stat = CompetitionSkaterStat::whereHas('competition', function($query) use($pool_matchup) {
                    $query->where(\DB::raw('DATE(match_start_date)'), '>=', $pool_matchup->seasonCompetitionTypeWeek->start_date->toDateString())
                    ->where(\DB::raw('DATE(match_start_date)'), '<=', $pool_matchup->seasonCompetitionTypeWeek->end_date->toDateString());
                })
                ->selectRaw('sum(time_on_ice_secs) as time_on_ice_secs')
                ->whereIn('player_season_id', $team_player_season_ids[$team_id])
                ->first();
        return [$goalie_stat, $skater_stat];
    }

    private function saveMatchupH2HCatResult($pool_matchup)
    {
        $pool_matchup_scores = $pool_matchup->poolH2hScores()
                ->with([
                    'poolScoreSetting',
                    'poolTeamPlayer.playerSeason'
                ])
                ->get();
        $matchup_pool = $pool_matchup->pool()->with([
                    'poolScoreSettings'
                ])->first();
        $result = [];
        foreach ($pool_matchup_scores as $matchup_score) {
            if (!isset($result[$matchup_score->pool_team_id])) {
                $result[$matchup_score->pool_team_id] = [];
            }
            if (!isset($result[$matchup_score->pool_team_id][$matchup_score->poolScoreSetting->pool_scoring_field_id])) {
                $result[$matchup_score->pool_team_id][$matchup_score->poolScoreSetting->pool_scoring_field_id] = 0;
            }
            $result[$matchup_score->pool_team_id][$matchup_score->poolScoreSetting->pool_scoring_field_id] += $matchup_score->value;

            if ($matchup_score->poolTeamPlayer->playerSeason->player_position_id == config('pool.inverse_player_position.g')) {
                $team_player_season_ids[$matchup_score->pool_team_id][] = $matchup_score->poolTeamPlayer->playerSeason->id;
            }
        }
        if (count($result) < 2) {
            return; // only go ahead if data of both team available
        }

        foreach ($result as $team_id => $data) {
            foreach ($data as $field_id => $value) {
                if ($field_id == config('pool.pool_scoring_field.inverse_fields.save_percentage')) {
                    list($goalie_stat, $skater_stat) = $this->getGoalieAndSkaterStat($pool_matchup, $team_player_season_ids, $team_id);
                    $total_stat = ($goalie_stat->shots_against != 0) ? (($goalie_stat->shots_against - $goalie_stat->goals_against) / $goalie_stat->shots_against) : NULL;
                    $result[$team_id][$field_id] = $total_stat * $matchup_pool->poolScoreSettings->where('pool_scoring_field_id', config('pool.pool_scoring_field.inverse_fields.save_percentage'))->first()->value;
                } elseif ($field_id == config('pool.pool_scoring_field.inverse_fields.goals_against_avg')) {
                    list($goalie_stat, $skater_stat) = $this->getGoalieAndSkaterStat($pool_matchup, $team_player_season_ids, $team_id);
                    $total_stat = ($skater_stat->time_on_ice_secs != 0) ? (($goalie_stat->goals_against * 3600) / $skater_stat->time_on_ice_secs) : NULL;
                    $result[$team_id][$field_id] = $total_stat * $matchup_pool->poolScoreSettings->where('pool_scoring_field_id', config('pool.pool_scoring_field.inverse_fields.goals_against_avg'))->first()->value;
                }
            }
        }


        $this->addMissingValuesToResult($result, $pool_matchup->team1, $pool_matchup->team2);
        $this->addMissingValuesToResult($result, $pool_matchup->team2, $pool_matchup->team1);

        $final_result = [
            $pool_matchup->team1 => [
                'win' => 0,
                'loss' => 0,
                'tie' => 0,
                'score' => 0,
            ],
            $pool_matchup->team2 => [
                'win' => 0,
                'loss' => 0,
                'tie' => 0,
                'score' => 0,
            ],
        ];
        foreach ($result[$pool_matchup->team1] as $key => $value) {
            if ($value == $result[$pool_matchup->team2][$key]) {
                $final_result[$pool_matchup->team1]['tie'] += 1;
                $final_result[$pool_matchup->team2]['tie'] += 1;
                $final_result[$pool_matchup->team1]['score'] += config('pool.scoring.tie');
                $final_result[$pool_matchup->team2]['score'] += config('pool.scoring.tie');
            } elseif ($value > $result[$pool_matchup->team2][$key]) {
                $final_result[$pool_matchup->team1]['win'] += 1;
                $final_result[$pool_matchup->team2]['loss'] += 1;
                $final_result[$pool_matchup->team1]['score'] += config('pool.scoring.win');
            } else {
                $final_result[$pool_matchup->team2]['win'] += 1;
                $final_result[$pool_matchup->team1]['loss'] += 1;
                $final_result[$pool_matchup->team2]['score'] += config('pool.scoring.win');
            }
        }

        foreach ($final_result as $team_id => $data) {
            $pool_matchup_result = $pool_matchup->poolMatchupResults()->firstOrNew([
                'pool_team_id' => $team_id
            ]);
            $pool_matchup_result->win = $data['win'];
            $pool_matchup_result->loss = $data['loss'];
            $pool_matchup_result->tie = $data['tie'];
            $pool_matchup_result->score = $data['score'];
            $pool_matchup_result->save();
        }
    }

    private function addMissingValuesToResult(&$result, $team1_id, $team2_id)
    {
        $keys = array_keys(array_diff_key($result[$team1_id], $result[$team2_id]));
        foreach ($keys as $key) {
            $result[$team2_id][$key] = 0;
        }
    }
}