<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\NotificationUser;
use App\Models\Pool;

/**
 * Description of NotificationController
 *
 * @author Anik
 */
class NotificationsController extends Controller {

    public function poolTeams() {
        Pool::active()
                ->where('id', 63)
                ->with([
                    'poolTeams',
                    'totalPoolRoster',
                    'poolTeams.totalCurrentPoolTeamLineup',
                    'poolTeams.poolTeamPlayers' => function($query) {
                        $query->has('totalPoolTeamLineup');
                    },
                    'poolTeams.poolTeamPlayers.playerSeason.player',
                    'poolTeams.poolTeamPlayers.playerSeason.injuredPlayer',
                ])
                ->chunk(50, function($pools) {
                    foreach ($pools as $pool) {
                        if ($pool->totalPoolRoster != NULL) {
                            foreach ($pool->poolTeams as $pool_team) {
                                // Checking Missing Roster Spot
                                if ($pool_team->totalCurrentPoolTeamLineup->isEmpty()) {
                                    Notification::createAndBroadcast($pool->name . ' - ' . $pool_team->name, trans('notifications.missing_roster'), route('frontend.pool.myTeam', ['pool_id' => $pool->id]), [$pool_team->user_id], $pool->id);
                                } else {
                                    if ($pool_team->totalCurrentPoolTeamLineup->get(0)->total < $pool->totalPoolRoster->total_roster) {
                                        Notification::createAndBroadcast($pool->name . ' - ' . $pool_team->name, trans('notifications.missing_roster'), route('frontend.pool.myTeam', ['pool_id' => $pool->id]), [$pool_team->user_id], $pool->id);
                                    }
                                }

                                // Checking Injury Report
                                foreach ($pool_team->poolTeamPlayers as $pool_team_player) {
                                    if ($pool_team_player->playerSeason->injuredPlayer != NULL && !$pool_team_player->playerSeason->injuredPlayer->isEmpty()) {
                                        Notification::createAndBroadcast($pool->name . ' - ' . $pool_team->name, trans('notifications.injured_player', ['Player' => $pool_team_player->playerSeason->player->full_name]), route('frontend.pool.myTeam', ['pool_id' => $pool->id]), [$pool_team->user_id], $pool->id);
                                    }
                                }
                            }
                        }
                    }
                });
    }

}
