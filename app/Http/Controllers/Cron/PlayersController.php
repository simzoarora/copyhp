<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use SportsDirect\NHL;
use App\Models\Season;
use App\Models\Team;
use App\Models\Player;
use App\Models\StoredValue;

/**
 * Description of PlayersController
 *
 * @author Anik
 */
class PlayersController extends Controller
{

    public function getHistoricalPlayerStats()
    {
        try {
            ini_set('max_execution_time', 0);
            ini_set('memory_limit', 268435456); // 256 MB because first time on this script will demand lot of memory
            $stored_value = StoredValue::where('key', 'player_stats_feed_update_time')->firstOrFail();
            $nhl = new NHL(\Config('sportsdirectconfig.api_key'));
            $atom_feed = $nhl->getPlayerStatsAtomFeed(date('Y-m-d\TH:i:s.uP', strtotime($stored_value->value)));
            if (isset($atom_feed['entry']) && !empty($atom_feed['entry'])) {
                if (!isset($atom_feed['entry'][0])) {
                    $this->_processAndSaveStat($atom_feed['entry']);
                } else {
                    foreach ($atom_feed['entry'] as $key => $entry) {
                        $this->_processAndSaveStat($entry);
                    }
                }
            }
            $stored_value->value = date('Y-m-d H:i:s', strtotime($atom_feed['updated'] . ' +1 second'));
            $stored_value->save();
        } catch (\Exception $ex) {
            \Log::alert("getHistoricalPlayerStats : " . $ex->getMessage() . " Line : " . $ex->getLine() . ". Trace : " . $ex->getTraceAsString());
        }
        echo 'DONE';
    }

    private function _processAndSaveStat($entry)
    {
        $client = new \GuzzleHttp\Client();
        $stat_data = json_decode(json_encode(new \SimpleXMLElement($client->request('GET', $entry['id'])->getBody())), 1);
        $season = Season::sportsDirectSaveOrGetSeason($stat_data['team-sport-content']['league-content']['season-content']['season']);
        if (!isset($stat_data['team-sport-content']['league-content']['season-content']['team-content']['team']['id'])) {
            return;
        }
        $team = Team::getTeamFromApiIdString($stat_data['team-sport-content']['league-content']['season-content']['team-content']['team']['id']);
        foreach ($stat_data['team-sport-content']['league-content']['season-content']['team-content']['player-content'] as $player_content) {
            $player = Player::getPlayerFromApiIdString($player_content['player']['id']);
            if (isset($player_content['stat-group'])) {
                if ($player == null) {
                    \Log::alert("getHistoricalPlayerStats : player not found with API ID : {$player_content['player']['id']}");
                    continue;
                }
                if (isset($player_content['stat-group'][0])) {
                    foreach ($player_content['stat-group'] as $stat_group) {
                        $this->_saveStatGroup($player, $season, $team, $stat_group);
                    }
                } else {
                    $stat_group = $player_content['stat-group'];
                    $this->_saveStatGroup($player, $season, $team, $stat_group);
                }
            }
        }
    }

    private function _saveStatGroup($player, $season, $team, $stat_group)
    {
        $player_stat = $player->playerStats()->firstOrNew([
            'season_id' => $season->id,
            'team_id' => $team->id,
            'stat_key' => $stat_group['key'],
        ]);
        foreach ($stat_group['scope'] as $scope) {
            $player_stat->{$scope['@attributes']['type']} = $scope['@attributes']['str'];
        }
        foreach ($stat_group['stat'] as $stat) {
            if (isset($stat['@attributes'])) {
                $player_stat->{$stat['@attributes']['type']} = $stat['@attributes']['num'];
            } else {
                $player_stat->{$stat['type']} = $stat['num'];
            }
        }
        $player_stat->save();
    }
}