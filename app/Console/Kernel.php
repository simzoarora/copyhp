<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\FetchSeasonTeams::class,
        Commands\FetchBoxScore::class,
        Commands\FetchDraftPlayer::class,
        Commands\FetchInjuredPlayer::class,
        Commands\FetchSeasonSchedule::class,
        Commands\FetchSeasonTeamPlayers::class,
        Commands\FetchStandings::class,
        Commands\GetHistoricalPlayerStats::class,
        Commands\ProcessTrades::class,
        Commands\ProcessWaiver::class,
        Commands\SaveCompetitionTypes::class,
        Commands\CreateSeasonWeeks::class,
        Commands\UpdateTeamLogo::class,
        Commands\SortPlayerPosition::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        /**
         * Laravel Backup Commands
         */
        // $schedule->command('backup:clean')->daily()->at('01:00');
        // $schedule->command('backup:run')->daily()->at('02:00');
        $schedule->call('App\Http\Controllers\Cron\IndexController@fetchSeasonTeams')->dailyAt('03:30');
        $schedule->call('App\Http\Controllers\Cron\IndexController@fetchSeasonTeamPlayers')->everyFiveMinutes()->name('season-team-players')->withoutOverlapping();
        $schedule->call('App\Http\Controllers\Cron\IndexController@fetchDraftPlayers')->dailyAt('03:50');
        $schedule->call('App\Http\Controllers\Cron\IndexController@fetchInjuredPlayer')->dailyAt('04:00');
        $schedule->call('App\Http\Controllers\Cron\IndexController@fetchBoxScore')->everyMinute()->name('box-score')->withoutOverlapping();

        $schedule->call('App\Http\Controllers\Cron\StandingsController@fetchStandings')->dailyAt('04:20');

        $schedule->call('App\Http\Controllers\Cron\ScheduleController@fetchSeasonSchedule')->dailyAt('04:30');

        $schedule->call('App\Http\Controllers\Cron\PlayersController@getHistoricalPlayerStats')->dailyAt('04:40');

        $schedule->call('App\Http\Controllers\Cron\PoolsController@createSeasonWeeks')->weekly();

//        $schedule->call('App\Http\Controllers\Cron\PoolsController@addPoolScore')->everyMinute()->name('pool-score')->withoutOverlapping();
        $schedule->call('App\Http\Controllers\Cron\PoolsController@addPoolScore')->everyMinute()->when(function () {
            return date('H:i:s') >= "17:00:00" || date('H:i:s') <= "7:00:00";
        })->name('pool-score')->withoutOverlapping();

        $schedule->call('App\Http\Controllers\Cron\PoolsController@addPoolScoreForPreviousSeasons')->everyMinute()->when(function () {
            return date('H') >= 4 && date('H') <= 6;
        })->name('pool-score-for-previous-season')->withoutOverlapping();

        $schedule->call('App\Http\Controllers\Cron\PoolsController@addPoolScoreForCurrentSeason')->everyMinute()->when(function () {
            return date('H') >= 4 && date('H') <= 6;
        })->name('pool-score-for-current-season')->withoutOverlapping();

        $schedule->call('App\Http\Controllers\Cron\PoolsController@addPlayerRankForPreviousSeason')->everyMinute()->when(function () {
            return date('H') >= 6 && date('H') <= 8;
        })->name('player-rank-for-previous-season')->withoutOverlapping();

        $schedule->call('App\Http\Controllers\Cron\PoolsController@addPlayerRankForCurrentSeason')->everyMinute()->when(function () {
            return date('H') >= 6 && date('H') <= 8;
        })->name('player-rank-for-current-season')->withoutOverlapping();

        $schedule->call('App\Http\Controllers\Cron\PoolTeamsController@processTrades')->dailyAt('08:00');
        $schedule->call('App\Http\Controllers\Cron\PoolTeamsController@processWaiver')->dailyAt('08:00');

        $schedule->call('App\Http\Controllers\Cron\NotificationsController@poolTeams')->dailyAt('11:00');

        $schedule->call('App\Http\Controllers\Cron\PoolsController@checkGoalieGamesAndRemoveStats')->weekly()->mondays()->at('07:00');

        $schedule->call('App\Http\Controllers\Cron\PoolsController@createPlayOffMatches')->weekly()->mondays()->at('08:00');

        $schedule->call('App\Http\Controllers\Cron\AtomfeedController@index')->everyFiveMinutes();

        $schedule->call('App\Http\Controllers\Cron\TwitterController@index')->everyFiveMinutes();

        $schedule->call('App\Http\Controllers\Cron\LiveDraftsController@processPoolForLiveDraft')->name('live-draft-processing')->withoutOverlapping();
    }
}