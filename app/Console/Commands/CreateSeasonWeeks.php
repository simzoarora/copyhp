<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateSeasonWeeks extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:CreateSeasonWeeks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Season Weeks';
    protected $controller;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(\App\Http\Controllers\Cron\PoolsController $controller) {
        parent::__construct();
        $this->controller = $controller;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->controller->createSeasonWeeks();
    }

}
