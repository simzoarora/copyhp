<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Team;

class UpdateTeamLogo extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:UpdateTeamLogo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Team Logo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $logo_array = [
            1 => 'colorado_avalanche_logo.svg',
            2 => 'detroit_red_wings_logo.svg',
            3 => 'boston_bruins_logo.svg',
            4 => 'new_york_islanders_logo.svg',
            5 => 'carolina_hurricanes_logo.svg',
            6 => 'new_york_rangers_logo.svg',
            7 => 'pittsburgh_penguins_logo.svg',
            8 => 'toronto_maple_leafs_logo.svg',
            9 => 'ottawa_senetors_logo.svg',
            10 => 'buffalo_sabres_logo.svg',
            11 => 'montreal_canadiens_logo.svg',
            12 => 'philadelphia_flyers_logo.svg',
            13 => 'new_jersey_devils_logo.svg',
            14 => 'florida_panthers.svg',
            15 => 'washington_capitals_logo.svg',
            16 => 'st_louis_blues_logo.svg',
            17 => 'tampa_bay_lightning_logo.svg',
            18 => 'calgary_flames_logo.svg',
            19 => 'arizona_coyotes_logo.svg',
            20 => 'dallas_stars_logo.svg',
            21 => 'san_jose_sharks_logo.svg',
            22 => 'chicago_blackhawks_logo.svg',
            23 => 'los_angeles_kings_logo.svg',
            24 => 'edmonton_oilders_logo.svg',
            25 => 'anaheim_ducks_logo.svg',
            26 => 'vancouver_canucks_logo.svg',
            27 => 'nashville_predators_logo.svg',
            28 => 'winnipeg_jets_logo.svg',
            29 => 'minnesota_wild_logo.svg',
            30 => 'columbus_blueJackets_logo.svg',
        ];

        foreach ($logo_array as $api_id => $logo) {
            Team::where('api_id', $api_id)->update(['logo' => $logo]);
        }
        die('DONE');
    }

}
