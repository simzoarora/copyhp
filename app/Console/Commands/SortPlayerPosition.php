<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\PlayerPosition;

class SortPlayerPosition extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:SortPlayerPosition';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sort Player Position';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $position_array = [
            'C' => '3',
            'RW' => '5',
            'D' => '6',
            'G' => '8',
            'LW' => '4',
            'S' => '1',
            'F' => '2',
            'BN' => '7',
        ];

        foreach ($position_array as $short_name => $sort_order) {
            PlayerPosition::where('short_name', $short_name)->update(['sort_order' => $sort_order]);
        }
        die('DONE');
    }

}
