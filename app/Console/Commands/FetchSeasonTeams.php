<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class FetchSeasonTeams extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:FetchSeasonTeams';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch NHL teams';
    protected $controller;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(\App\Http\Controllers\Cron\IndexController $controller) {
        parent::__construct();
        $this->controller = $controller;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->controller->fetchSeasonTeams();
    }

}
