<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;

class PoolInvitation extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'email',
        'name',
        'accepted',
    ];

    public function pool() {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    public function poolTeamPlayers() {
        return $this->hasMany('App\Models\PoolTeamPlayer');
    }

    public function playerSeason() {
        return $this->belongsToMany('App\Models\PlayerSeason', 'pool_team_players')->withTimestamps();
    }

    public static function addUserToPool($user) {
        $pool_invitations = self::where('email', $user->email)->where('accepted', 0)->with('poolTeamPlayers')->get();
        if (!empty($pool_invitations)) {
            foreach ($pool_invitations as $invitation) {
                $pool_team = $user->poolTeams()->firstOrNew([
                    'pool_id' => $invitation->pool_id
                ]);
                $pool_team->pool_id = $invitation->pool_id;
                $pool_team->name = $invitation->name;
                $pool_team->sort_order = $invitation->sort_order;
                if ($pool_team->save()) {
                    $invitation->accepted = 1;
                    $invitation->save();
                    foreach ($invitation->poolTeamPlayers as $team_player) {
                        $team_player->update(['pool_team_id' => $pool_team->id, 'pool_invitation_id' => NULL]);
                    }
                } else {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

}
