<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use App\Services\Traits\FutureData;
use App\Services\Traits\SoftDeletes;
use Auth;

class PoolTeamPlayer extends Model
{

    use SoftDeletes,
        FutureData;
    protected $guarded = [
        'id',
        'updated_at',
        'created_at',
    ];

    public function poolTeam()
    {
        return $this->belongsTo('App\Models\PoolTeam');
    }

    public function poolInvitation()
    {
        return $this->belongsTo('App\Models\PoolInvitation');
    }

    public function poolRound()
    {
        return $this->belongsTo('App\Models\PoolRound');
    }

    public function playerSeason()
    {
        return $this->belongsTo('App\Models\PlayerSeason');
    }

    public function poolTeamLineups()
    {
        return $this->hasMany('App\Models\PoolTeamLineup');
    }

    /**
     * Used just to get first result
     * @return type
     */
    public function poolTeamLineup()
    {
        return $this->hasOne('App\Models\PoolTeamLineup');
    }

    public function totalPoolTeamLineup()
    {
        return $this->hasOne('App\Models\PoolTeamLineup')
                        ->current()
                        ->whereNotIn('player_position_id', [config('pool.inverse_player_position.bn'), config('pool.inverse_player_position.ir')])
                        ->selectRaw('pool_team_player_id, count(*) as count')
                        ->groupBy('pool_team_player_id');
    }

    public function poolStandardScores()
    {
        return $this->hasMany('App\Models\PoolStandardScore');
    }

    public function poolH2hScores()
    {
        return $this->hasMany('App\Models\PoolH2hScore');
    }

    public function totalPoolH2hScore()
    {
        return $this->hasOne('App\Models\PoolH2hScore')
                        ->selectRaw('pool_team_player_id, count(*) as count')
                        ->groupBy('pool_team_player_id');
    }

    public function totalPoolStandardScore()
    {
        return $this->hasOne('App\Models\PoolStandardScore')
                        ->selectRaw('pool_team_player_id, count(*) as count')
                        ->groupBy('pool_team_player_id');
    }

    public function poolTradePlayer()
    {
        return $this->hasOne('App\Models\PoolTradePlayer');
    }

    public function poolWaiverDelete()
    {
        return $this->hasOne('App\Models\PoolWaiverDelete');
    }

    /**
     * 
     * @param type $player_season_id
     * @param type $match_start_date
     * @param type $pool_type
     * 4th param can be passed to add or condition in pool type
     * @return type
     */
    public static function getPlayerForScoreCalculation($player_season_id, $match_start_date, $pool_type, $additional_pool_type = NULL, $competition_type)
    {
        $query_builder = self::where('player_season_id', $player_season_id)
                ->withTrashed() // including dropped players as well with deleted at condition to add score of players which were there yesterday
                ->where(function($query) use($match_start_date) {
                    $query->where('deleted_at', '>=', $match_start_date->toDateTimeString())
                    ->orWhereNull('deleted_at');
                })
                ->where('created_at', '<', $match_start_date->toDateTimeString()) // including only those players which were added before starting of match
                ->whereHas('poolTeam.pool', function($query)use($pool_type, $additional_pool_type, $competition_type) {
                    $query->where(function($query) use($pool_type, $additional_pool_type) {
                        $query->where('pool_type', $pool_type)->has('currentSeason');
                        if ($additional_pool_type !== NULL) { // checking 4th param which can be another pool type. if there we will add it on or condition
                            $query->orWhere('pool_type', $additional_pool_type);
                        }
                    })
                    ->where('season_type', $competition_type);
                })
                ->whereHas('poolTeamLineups', function($query) use($match_start_date) {
                    $query->where('start_time', '<=', $match_start_date->toDateString())
                    ->where('end_time', '>=', $match_start_date->toDateString())
                    ->where('player_position_id', '<>', config('pool.inverse_player_position.bn')); // not including bench position player
                })
                ->with([
            'poolTeam.pool.poolScoreSettings',
            'poolTeam.pool.poolSetting.poolsetting',
            'playerSeason'
        ]);

        if ($pool_type == config('pool.inverse_type.h2h')) {
            $query_builder = $query_builder->where(function($query)use($match_start_date) {
                        $query->whereHas('poolTeam.poolMatchupTeam1.seasonCompetitionTypeWeek', function($query) use($match_start_date) {
                            $query->where('start_date', '<=', $match_start_date->toDateString())
                            ->where('end_date', '>=', $match_start_date->toDateString());
                        })
                        ->orWhereHas('poolTeam.poolMatchupTeam2.seasonCompetitionTypeWeek', function($query)use($match_start_date) {
                            $query->where('start_date', '<=', $match_start_date->toDateString())
                            ->where('end_date', '>=', $match_start_date->toDateString());
                        });
                    })
                    ->with([
                'poolTeam.poolMatchupTeam1' => function($query)use($match_start_date) {
                    $query->whereHas('seasonCompetitionTypeWeek', function($query)use($match_start_date) {
                                $query->where('start_date', '<=', $match_start_date->toDateString())
                                ->where('end_date', '>=', $match_start_date->toDateString());
                            })
                    ->with('poolTeam1');
                },
                'poolTeam.poolMatchupTeam2' => function($query)use($match_start_date) {
                    $query->whereHas('seasonCompetitionTypeWeek', function($query) use($match_start_date) {
                                $query->where('start_date', '<=', $match_start_date->toDateString())
                                ->where('end_date', '>=', $match_start_date->toDateString());
                            })
                    ->with('poolTeam2');
                },
            ]);
        }

        $pool_team_players = $query_builder->get()->filter(function ($value) {
            // Filtering to remove those items whose pool's league start date is in future
            return SeasonCompetitionTypeWeek::where('id', $value->poolTeam->pool->poolSetting->poolsetting->league_start_week)
                            ->where('start_date', '<=', date('Y-m-d'))
                            ->exists();
        });
        return $pool_team_players;
    }

    public static function getTotalPlayerCount($pool_team_id)
    {
        return self::where('pool_team_id', $pool_team_id)
                        ->withFutureData()
                        ->withFutureDelete()
                        ->whereNotExists(function($subquery) {
                            /**
                             * We will not count future waiver deletes as actual delete
                             */
                            $subquery->select(\DB::raw(1))
                            ->from('pool_waiver_deletes')
                            ->whereRaw('pool_waiver_deletes.pool_team_player_id = pool_team_players.id')
                            ->where(function($query) {
                                $query->whereNull('deleted_at');
                            });
                        })
                        ->count();
    }

    public static function getDataForMyTeamInLiveDraft($pool_id)
    {
        return self::whereHas('poolTeam', function($query) use($pool_id) {
                            $query->where('pool_id', $pool_id)
                            ->where('user_id', Auth::id());
                        })
                        ->with([
                            'poolTeamLineup' => function($query) {
                                $query->where('start_time', '<=', date('Y-m-d'))
                                ->where('end_time', '>=', date('Y-m-d'))
                                ->select('id', 'pool_team_player_id', 'player_position_id');
                            },
                            'playerSeason' => function($query) {
                                $query->select('id', 'player_id', 'team_id', 'player_position_id');
                            },
                            'playerSeason.player' => function($query) {
                                $query->select('id', 'first_name', 'last_name');
                            },
                            'playerSeason.team' => function($query) {
                                $query->select('id', 'short_name');
                            },
                            'playerSeason.playerPosition' => function($query) {
                                $query->select('id', 'short_name');
                            },
                        ])
                        ->select('id', 'player_season_id')
                        ->get();
    }

    public static function checkIfPlayerAlreadyExistInPool($pool_id, $player_season_id)
    {
        return self::whereHas('poolTeam', function($query) use($pool_id){
                            $query->where('pool_id', $pool_id);
                        })
                        ->where('player_season_id', $player_season_id)
                        ->exists();
    }
}