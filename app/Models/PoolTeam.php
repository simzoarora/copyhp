<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;
use App\Models\PoolTrade;
use Carbon\Carbon;

class PoolTeam extends Model
{

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'name',
        'email',
    ];

    public function pool()
    {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User');
    }

    public function poolTeamPlayers()
    {
        return $this->hasMany('App\Models\PoolTeamPlayer');
    }

    public function totalAcquisition()
    {
        return $this->hasOne('App\Models\PoolTeamPlayer')
                        ->whereNull('pool_round_id')
                        ->selectRaw('pool_team_id, count(*) as count')
                        ->groupBy('pool_team_id');
    }

    public function totalAcquisitionCurrentWeek()
    {
        $carbon_date = Carbon::now();
        $start_end_array = [
            $carbon_date->startOfWeek()->toDateString(),
            $carbon_date->endOfWeek()->toDateString(),
        ];
        return $this->hasOne('App\Models\PoolTeamPlayer')
                        ->whereNull('pool_round_id')
                        ->whereIn(\DB::raw('created_at'), $start_end_array)
                        ->selectRaw('pool_team_id, count(*) as count')
                        ->groupBy('pool_team_id');
    }

    public function poolMatchupTeam1()
    {
        return $this->hasMany('App\Models\PoolMatchup', 'team1');
    }

    public function poolMatchupTeam2()
    {
        return $this->hasMany('App\Models\PoolMatchup', 'team2');
    }

    public function poolH2hScores()
    {
        return $this->hasMany('App\Models\PoolH2hScore');
    }

    public function poolStandardScores()
    {
        return $this->hasMany('App\Models\PoolStandardScore');
    }

    public function poolStandardRottoResults()
    {
        return $this->hasMany('App\Models\PoolStandardRottoResult');
    }

    public function poolMatchupResults()
    {
        return $this->hasMany('App\Models\PoolMatchupResult');
    }

    /**
     * For finding total result of a single team
     * @return type
     */
    public function poolMatchupResult()
    {
        return $this->hasOne('App\Models\PoolMatchupResult')
                        ->selectRaw("sum(win) as total_win,sum(loss) as total_loss,sum(tie) as total_tie,sum(score) as total_score, pool_team_id")
                        ->groupBy('pool_team_id');
    }

    /**
     * For finding total result of a single team
     * @return type
     */
    public function lastWeekPoolMatchupResult()
    {
        return $this->hasOne('App\Models\PoolMatchupResult')
                        ->whereHas('poolMatchup', function($query) {
                            $query->whereHas('seasonCompetitionTypeWeek', function($query) {
                                $query->where('start_date', '<=', date('Y-m-d', strtotime('-7 days')))->where('end_date', '>=', date('Y-m-d', strtotime('-7 days')));
                            });
                        });
    }

    public function poolTeamLineups()
    {
        return $this->hasManyThrough('App\Models\PoolTeamLineup', 'App\Models\PoolTeamPlayer');
    }

    public function totalCurrentPoolTeamLineup()
    {
        return $this->hasManyThrough('App\Models\PoolTeamLineup', 'App\Models\PoolTeamPlayer')->current()->selectRaw('count(*) as total, pool_team_lineups.id, pool_team_players.id, pool_team_player_id')->groupBy('pool_team_players.pool_team_id');
    }

    public function h2hScore()
    {
        return $this->hasOne('App\Models\PoolMatchupResult')->selectRaw('sum(score) as total_score, pool_team_id')->groupBy('pool_team_id');
    }

    public function standardScore()
    {
        return $this->hasOne('App\Models\PoolStandardScore')->selectRaw('sum(value) as total_score, pool_team_id')->groupBy('pool_team_id');
    }

    public function standardRottoScore()
    {
        return $this->hasOne('App\Models\PoolStandardRottoResult')->selectRaw('sum(value) as total_score, pool_team_id')->groupBy('pool_team_id');
    }

    public function scopeCurrentUserTeam($query)
    {
        return $query->where('user_id', \Auth::id());
    }

    public function playerSeason()
    {
        return $this->belongsToMany('App\Models\PlayerSeason', 'pool_team_players')->withTimestamps();
    }

    public function poolWaiverPriority()
    {
        return $this->hasOne('App\Models\PoolWaiverPriority');
    }

    public function poolTradePlayers()
    {
        return $this->hasManyThrough('App\Models\PoolTradePlayer', 'App\Models\PoolTeamPlayer');
    }

    public function liveDraftQueues()
    {
        return $this->hasMany('App\Models\LiveDraftQueue');
    }

    public function liveDraftPlayers()
    {
        return $this->hasMany('App\Models\LiveDraftPlayer');
    }

    /**
     * Use to get first result only
     * @return type
     */
    public function liveDraftPlayer()
    {
        return $this->hasOne('App\Models\LiveDraftPlayer');
    }

    public function liveDraftTeamSetting()
    {
        return $this->hasOne('App\Models\LiveDraftTeamSetting');
    }

    public function addTotalScore($pool)
    {
        if ($pool->pool_type == config('pool.inverse_type.h2h')) {
            $this->total_score = ($this->poolMatchupResult != null) ? $this->poolMatchupResult->total_score : 0;
        } else if ($pool->pool_type == config('pool.inverse_type.standard')) {
            if ($pool->poolSetting->poolsetting->pool_format == config('pool.inverse_format.standard_standard')) {
                $this->total_score = $this->poolStandardScores->sum('score');
            } elseif ($pool->poolSetting->poolsetting->pool_format == config('pool.inverse_format.standard_rotisserie')) {
                $this->total_score = $this->poolStandardRottoResults->sum('value');
            }
        } else if ($pool->pool_type == config('pool.inverse_type.box')) {
            $this->total_score = $this->poolStandardScores->sum('score');
        }
        return $this;
    }

    public static function addUserToPool($user)
    {
        $pool_invitations = self::where('email', $user->email)->with('poolTeamPlayers')->get();
        if (!empty($pool_invitations)) {
            foreach ($pool_invitations as $invitation) {
                $invitation->user_id = $user->id;
                $invitation->email = NULL;
                $invitation->save();
            }
        }
        return TRUE;
    }

    public function checkAcquisitionAvailable($pool)
    {
        if ($pool->poolSetting->poolsetting->max_acquisition_team > config('pool.inverse_pool_settings.max_acquisition_team.no_add')) {
            if ($this->totalAcquisition != NULL) {
                if ($this->totalAcquisition->count >= $pool->poolSetting->poolsetting->max_acquisition_team) {
                    return [FALSE, trans('messages.pool.season_acquisition_exceeded')];
                }
            }
        } elseif ($pool->poolSetting->poolsetting->max_acquisition_team == config('pool.inverse_pool_settings.max_acquisition_team.no_add')) {
            return [FALSE, trans('messages.pool.player_acquisition_not_allowed')];
        }

        if ($pool->poolSetting->poolsetting->max_acquisition_week > config('pool.inverse_pool_settings.max_acquisition_team.no_add')) {
            if ($this->totalAcquisitionCurrentWeek != NULL) {
                if ($this->totalAcquisitionCurrentWeek->count >= $pool->poolSetting->poolsetting->max_acquisition_week) {
                    return [FALSE, trans('messages.pool.week_acquisition_exceeded')];
                }
            }
        } elseif ($pool->poolSetting->poolsetting->max_acquisition_week == config('pool.inverse_pool_settings.max_acquisition_team.no_add')) {
            return [FALSE, trans('messages.pool.player_acquisition_not_allowed')];
        }
        return [TRUE, ''];
    }

    public function checkTradeAvailable($max_trade_season)
    {
        // checking if user exceeded no of trades allowed in a season
        $total_trades = \App\Models\PoolTrade::where(function($query) {
                    $query->where('parent_pool_team_id', '=', $this->id)
                            ->orWhere('trading_pool_team_id', '=', $this->id);
                })->where(function($query) {
                    $query->where('status', '=', config('pool.pool_trades.inverse_status.no_action'))
                            ->orWhere('status', '=', config('pool.pool_trades.inverse_status.accepted'));
                })->count();
        if ($max_trade_season > config('pool.inverse_pool_settings.max_trades_season.no_add')) {
            if ($total_trades >= $max_trade_season) {
                return [FALSE, trans('messages.pool.trade_exceeded')];
            }
        } elseif ($max_trade_season == config('pool.inverse_pool_settings.max_trades_season.no_add')) {
            return [FALSE, trans('messages.pool.trade_not_allowed')];
        }
        return [TRUE, ''];
    }

    public static function queryTradeStatus($trade_id)
    {
        return PoolTrade::where('id', $trade_id)->where('deadline', '>', date('Y-m-d H:i:s'))
                        ->where('status', config('pool.pool_trades.inverse_status.no_action'))
                        ->where('pool_trade_setting_based_status', config('pool.pool_trades.pool_trade_setting_based_status.accepted'))
                        ->with(['parentPoolTeam', 'tradingPoolTeam'])->first();
    }

    public static function getUserIdOfPool($pool_id)
    {
        return self::where('pool_id', $pool_id)->pluck('user_id');
    }
}