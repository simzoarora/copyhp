<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Division extends Model {

    protected $fillable = ['api_id', 'name', 'alias', 'is_active', 'conference_id'];

    public function conference() {
        return $this->belongsTo('App\Models\Conference');
    }

    public static function sportsDirectSaveOrGetConference($division_data, $conference_id) {
        $division = Division::firstOrNew([
                    'api_id' => str_replace(config('constant.sportsdirectconstant.division_id_prefix'), '', $division_data['id'])
        ]);
        $division->conference_id = $conference_id;
        $division->name = $division_data['name'];
        if (isset($division_data['alias'])) {
            $division->alias = $division_data['alias'];
        }
        $division->is_active = ($division_data['active'] == 'true') ? 1 : 0;
        $division->save();
        return $division;
    }

}
