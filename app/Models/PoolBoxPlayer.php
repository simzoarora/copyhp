<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolBoxPlayer extends Model {

    public $timestamps = false;
     protected $fillable = [
        'player_season_id',
    ];

    public function poolBox() {
        return $this->belongsTo('App\Models\PoolBox');
    }
    
    public function playerSeason(){
        return $this->belongsTo('App\Models\PlayerSeason');
    }

}
