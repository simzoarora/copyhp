<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of CalculatedPoolPoint
 *
 * @author Anik
 */
class SeasonPoolPoint extends Model {

    protected $fillable = [
        'player_id',
        'season_id',
        'pool_id',
    ];

    public function pool() {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    public function season() {
        return $this->belongsTo('App\Models\Season');
    }

    public function player() {
        return $this->belongsTo('App\Models\Player');
    }

}
