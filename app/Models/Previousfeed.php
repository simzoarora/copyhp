<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Previousfeed extends Model {

    protected $fillable = [
        'match_id',
        'match_start_date',
        'match_status',
        'home_team_id',
        'home_team_score',
        'away_team_id',
        'away_team_score',
        'match_complition_type'
    ];

}
