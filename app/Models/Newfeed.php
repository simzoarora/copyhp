<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Newfeed extends Model
{
    protected $fillable=[
        'match_id',
        'match_start_date',
        'match_status',
        'match_update_date',
        'match_complition_type',
        'home_team_id',
        'home_team_name',
        'home_team_code',
        'home_team_score',
        'away_team_id',
        'away_team_name',
        'away_team_code',
        'away_team_score'
    ];
}
