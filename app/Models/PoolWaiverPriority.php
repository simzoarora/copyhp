<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolWaiverPriority extends Model {

    protected $fillable = [
        'pool_id',
        'pool_team_id',
        'priority',
    ];

    public function pool() {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    public function poolTeam() {
        return $this->belongsTo('App\Models\PoolTeam');
    }

}
