<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;

/**
 * Description of InjuredPlayer
 *
 * @author Anik
 */
class InjuredPlayer extends Model
{

    use SoftDeletes;
    protected $dates = ['deleted_at', 'last_updated', 'injury_start_date'];
    protected $fillable = [
        'api_id',
        'injury_location',
        'injury_start_date',
        'injury_status',
        'injury_display_status',
        'injury_note',
        'last_updated',
    ];

    public function playerSeason()
    {
        return $this->belongsTo('App\Models\PlayerSeason');
    }

    public static function saveFromApi($season, $team, $player_content)
    {
        $db_team = $season->teams()->where('api_id', str_replace(config('constant.sportsdirectconstant.team_id_prefix'), '', $team['team']['id']))->firstOrFail();
        $db_player = $db_team->players()
                ->where('api_id', str_replace(config('constant.sportsdirectconstant.player_id_prefix'), '', $player_content['player']['id']))
                ->with(['currentPlayerSeason' => function($query)use($season) {
                        $query->where('season_id', $season->id);
                    }])
                ->firstOrFail();
        if ($db_player->currentPlayerSeason == null) {
            \Log::alert("fetchInjuredPlayer : currentPlayerSeason not found with data : " . json_encode($player_content));
            return FALSE;
        }
        $injured_player = $db_player->currentPlayerSeason->injuredPlayer()->firstOrNew([
            'api_id' => str_replace(config('constant.sportsdirectconstant.injury_id_prefix'), '', $player_content['injury']['id']),
        ]);
        $injured_player->injury_location = $player_content['injury']['location']['name'];
        $injured_player->injury_start_date = $player_content['injury']['start-date'];
        $injured_player->injury_status = $player_content['injury']['injury-status']['status'];
        $injured_player->injury_display_status = $player_content['injury']['injury-status']['display-status'];
        $injured_player->injury_note = $player_content['injury']['injury-status']['note'];
        $injured_player->last_updated = date('Y-m-d H:i:s', strtotime($player_content['injury']['last-updated']));
        $injured_player->save();
        return $injured_player;
    }
}