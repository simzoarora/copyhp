<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;
use Auth;

class MessageBoard extends Model {

    use SoftDeletes;

    protected $fillable = [
        'title',
        'description',
    ];
    protected $dates = [
        'deleted_at',
    ];

    public function user() {
        return $this->belongsTo('App\Models\Access\User\User');
    }
    
    public function toUser() {
        return $this->belongsTo('App\Models\Access\User\User','to_user_id');
    }

    public function pool() {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    public function messageBoardReplies() {
        return $this->hasMany('App\Models\MessageBoardReply');
    }

    /**
     * To get only first result
     * @return type
     */
    public function messageBoardReply() {
        return $this->hasOne('App\Models\MessageBoardReply');
    }

    public function directReplies() {
        return $this->hasMany('App\Models\MessageBoardReply')->whereNull('parent_id');
    }

    public function totalDirectReplies() {
        return $this->hasOne('App\Models\MessageBoardReply')->whereNull('parent_id')->selectRaw("count(*) as total, message_board_id");
    }

    public function latestReply() {
        return $this->hasOne('App\Models\MessageBoardReply')->whereNull('parent_id')->orderBy('created_at', 'desc');
    }

    public function totalReplies() {
        return $this->hasOne('App\Models\MessageBoardReply')
                        ->whereNull('parent_id')
                        ->groupBy('message_board_id')
                        ->selectRaw("count(*) as total, id,message_board_id");
    }

    public function reactions() {
        return $this->morphMany('App\Models\MessageReaction', 'reactionfor');
    }

    public function likes() {
        return $this->morphOne('App\Models\MessageReaction', 'reactionfor')
                        ->where('type', config('message_board.inverse_reaction.like'))
                        ->groupBy('reactionfor_id')
                        ->selectRaw("count(*) as total, id,reactionfor_id,reactionfor_type");
    }

    public function dislikes() {
        return $this->morphOne('App\Models\MessageReaction', 'reactionfor')
                        ->where('type', config('message_board.inverse_reaction.dislike'))
                        ->groupBy('reactionfor_id')
                        ->selectRaw("count(*) as total, id,reactionfor_id,reactionfor_type");
    }

    public function currentUserReaction() {
        return $this->morphOne('App\Models\MessageReaction', 'reactionfor')
                        ->where('user_id', Auth::id())
                        ->select('id', 'reactionfor_id', 'reactionfor_type');
    }

}
