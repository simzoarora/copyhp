<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeasonCompetitionType extends Model
{
    protected $dates = ['start_date', 'end_date'];
    protected $fillable = [
        'competition_type',
        'start_date',
        'end_date',
    ];

    public function season()
    {
        return $this->belongsTo('App\Models\Season');
    }

    public function seasonCompetitionTypeWeeks()
    {
        return $this->hasMany('App\Models\SeasonCompetitionTypeWeek');
    }

    /**
     * Used for first result
     * @return type
     */
    public function seasonCompetitionTypeWeek()
    {
        return $this->hasOne('App\Models\SeasonCompetitionTypeWeek');
    }

    public function currentSeason()
    {
        return $this->belongsTo('App\Models\Season', 'season_id')->current();
    }
}