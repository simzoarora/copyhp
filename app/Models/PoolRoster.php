<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolRoster extends Model
{
    protected $fillable = [
        'pool_id',
        'player_position_id',
        'value',
    ];

    /**
     * Function used to add any default where condition to query in model
     * @param type $excludeDeleted
     * @return type
     */
    public function newQuery($excludeDeleted = true)
    {
        return parent::newQuery($excludeDeleted)
                        ->where('value', '>', 0);
    }

    public function pool()
    {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    public function playerPosition()
    {
        return $this->belongsTo('App\Models\PlayerPosition');
    }
}