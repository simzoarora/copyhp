<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

class PoolSetting extends Model {

//    use SoftDeletes;
//    protected $dates = ['deleted_at'];
    protected $fillable = [
        'pool_id',
    ];
    public $timestamps = false;

    public function pool() {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    public function poolsetting() {
        return $this->morphTo();
    }

}
