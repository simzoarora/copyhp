<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerPreRanking extends Model {

    public $timestamps = true;
    public $fillable = ['season_id','player_id','rank'];
    
    public function players(){
        return $this->belongsTo('App\Models\Player');
    }
    public function season(){
        return $this->belongsTo('App\Models\Season');
    }
}