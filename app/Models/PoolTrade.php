<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PoolTeamPlayer;
use App\Models\PoolTeam;
use App\Models\PlayerSeason;
use Auth;

class PoolTrade extends Model {

    protected $guarded = [
        'id'
    ];

    public function parentTeam()
    {
        return $this->belongsTo('App\Models\PoolTeam', 'parent_pool_team_id');
    }

    public function tradingTeam()
    {
        return $this->belongsTo('App\Models\PoolTeam', 'trading_pool_team_id');
    }

    public function poolTradePlayers()
    {
        return $this->hasMany('App\Models\PoolTradePlayer');
    }

    public function parentPoolTeam()
    {
        return $this->belongsTo('App\Models\PoolTeam', 'parent_pool_team_id');
    }

    public function tradingPoolTeam()
    {
        return $this->belongsTo('App\Models\PoolTeam', 'trading_pool_team_id');
    }

    public function tradeVotings()
    {
        return $this->hasMany('App\Models\PoolTradeVoting');
    }
    
    public function tradeVoteFirst()
    {
        return $this->hasOne('App\Models\PoolTradeVoting');
    }

    public static function poolTradeAccept($pool_trade)
    {
        $to_be_processed_data = self::_prepareTradeDataToBeExecuted($pool_trade);

        list($status, $message) = self::processDropPlayer($pool_trade->parent_pool_team_id, $to_be_processed_data[$pool_trade->parent_pool_team_id]['drop_players'], NULL, FALSE);
        if ($status) {
            list($status, $message) = self::processDropPlayer($pool_trade->trading_pool_team_id, $to_be_processed_data[$pool_trade->trading_pool_team_id]['drop_players'], NULL, FALSE);
            if (!$status) {
                return ['message' => $message];
            }
        } else {
            return ['message' => $message];
        }
        foreach ($to_be_processed_data as $pool_team_id => $data) {
            $pool_team = PoolTeam::where('id', $pool_team_id)
                    ->with([
                        'pool',
                        'pool.totalPoolRoster',
                        'pool.season',
                    ])
                    ->firstOrFail();
            list($status, $message) = self::processAddPlayer($pool_team_id, $data['add_players'], $pool_team, FALSE);
            if (!$status) {
                return ['message' => $message];
            }
        }
        return TRUE;
    }

    private static function _prepareTradeDataToBeExecuted($pool_trade)
    {
        $to_be_processed_data = [
            $pool_trade->parent_pool_team_id => [
                'add_players' => [],
                'drop_players' => []
            ],
            $pool_trade->trading_pool_team_id => [
                'add_players' => [],
                'drop_players' => []
            ],
        ];
        $team_trade_player_grouped = $pool_trade->poolTradePlayers->groupBy('poolTeamPlayer.poolTeam.id');
        foreach ($team_trade_player_grouped as $pool_team_id => $trade_players) {
            $type_trade_player_grouped = $trade_players->groupBy('type');
            $drop_players = $type_trade_player_grouped->get(config('pool.pool_trades.inverse_trade_player_type.drop_players'));
            if ($drop_players != NULL) {
                $dropped_players_id = $drop_players->map(function ($item) {
                            return $item->poolTeamPlayer->id;
                        })->toArray();
                $to_be_processed_data[$pool_team_id]['drop_players'] = $dropped_players_id;
            }


            $trade_players = $type_trade_player_grouped->get(config('pool.pool_trades.inverse_trade_player_type.trade_players'));
            if ($trade_players != NULL) {
                $trade_players_season_id = $trade_players->map(function ($item) {
                            return $item->poolTeamPlayer->player_season_id;
                        })->toArray();
                $trade_players_id = $trade_players->map(function ($item) {
                            return $item->poolTeamPlayer->id;
                        })->toArray();

                $to_be_processed_data[$pool_team_id]['drop_players'] +=$trade_players_id;

                if ($pool_team_id == $pool_trade->parent_pool_team_id) {
                    $to_be_processed_data[$pool_trade->trading_pool_team_id]['add_players'] = $trade_players_season_id;
                } else {
                    $to_be_processed_data[$pool_trade->parent_pool_team_id]['add_players'] = $trade_players_season_id;
                }
            }
        }
        return $to_be_processed_data;
    }

    public static function processDropPlayer($pool_team_id, $player_ids, $deleted_at = NULL, $current_user_check = TRUE)
    {
        $player_ids = array_filter($player_ids);
        if ($deleted_at == NULL) {
            $deleted_at = config('pool.addition_deletion_time');
        }
        $today_date = date('Y-m-d', strtotime($deleted_at . ' -1 days')); // this has to be done because a player can be deleted if waiver is awarded to that player
        if (!empty($player_ids)) {
            $pool_team_players = PoolTeamPlayer::whereIn('id', $player_ids)
                    ->whereHas('poolTeam', function($query) use($pool_team_id, $current_user_check) {
                        $query->where('id', $pool_team_id);
                        if ($current_user_check) {
                            $query->where('user_id', Auth::id());
                        }
                    })
                    ->withTrashed() // this and below is used to check if this player is already not deleted in future
                    ->whereNull('deleted_at')
                    ->with([
                        'poolTeam.pool.poolSetting.poolsetting',
                        'poolTradePlayer' => function($query) {
                            $query->whereHas('poolTrade', function($query) {
                                        $query->where('status', config('pool.pool_trades.inverse_status.no_action'));
                                    });
                        },
                        'playerSeason.player'
                    ])
                    ->get();
            if ($pool_team_players->count() != count($player_ids)) {
                return [FALSE, trans('messages.pool.transaction_cannot_processed')];
            }
            foreach ($pool_team_players as $pool_team_player) {
                if ($pool_team_player->poolTradePlayer != NULL) {
                    // player already involved in a trade
                    return [FALSE, trans('messages.pool.player_involved_in_trade', ['Player' => $pool_team_player->playerSeason->player->full_name])];
                }
                if (isset($pool_team_player->poolTeam->pool->poolSetting->poolsetting->waiver_mode) &&
                        $pool_team_player->poolTeam->pool->poolSetting->poolsetting->waiver_mode == 1) {
                    $waiver = $pool_team_player->poolTeam->pool->poolWaivers()->firstOrCreate([
                        'player_season_id' => $pool_team_player->player_season_id,
                        'waiver_till' => date('Y-m-d H:i:s', strtotime($deleted_at->toDateTimeString() . ' +' . $pool_team_player->poolTeam->pool->poolSetting->poolsetting->waiver_time . ' days'))
                    ]);
                    $waiver->created_at = $deleted_at->toDateTimeString();
                    $waiver->updated_at = $deleted_at->toDateTimeString();
                    $waiver->save();
                }
                $pool_team_player->delete($deleted_at->toDateTimeString());
                //remove player from future lineup
                $current_lineup = $pool_team_player->poolTeamLineup()->where('start_time', '<=', $today_date)->where('end_time', '>=', $today_date)->first();
                if ($current_lineup != NULL) {
                    $pool_team_player->poolTeamLineup()->where('start_time', '>', $current_lineup->end_time)->delete($deleted_at->toDateTimeString());
                    $current_lineup->prev_end_time = $current_lineup->end_time;
                    $current_lineup->end_time = $today_date;
                    $current_lineup->save();
                } else {
                    $pool_team_player->poolTeamLineup()->where('start_time', '>', date('Y-m-d'))->delete($deleted_at->toDateTimeString());
                }
            }
        }
        return [TRUE, trans('messages.pool.player_deleted')];
    }

    public static function processAddPlayer($pool_team_id, $player_season_ids, $pool_team, $check_other_team_player = TRUE)
    {
        $players = PlayerSeason::whereIn('id', $player_season_ids)
                ->with([
                    'poolTeamPlayer' => function($query) use($pool_team) {
                        $query->whereHas('poolTeam', function($query) use($pool_team) {
                                    $query->whereHas('pool', function($query) use($pool_team) {
                                                $query->where('id', $pool_team->pool->id);
                                            });
                                })->withFutureData();
                    },
                    'poolWaiver' => function($query) use($pool_team) {
                        $query->where('is_completed', '=', 0)
                        ->where('pool_id', $pool_team->pool->id);
                    }
                ])
                ->get();
        // Check total rosters under limit
        $player_count = PoolTeamPlayer::getTotalPlayerCount($pool_team_id);
        if (($player_count + $players->count()) > $pool_team->pool->totalPoolRoster->total_roster) {
            return [FALSE, trans('messages.pool.lineup_more_than_roster')];
        }


        foreach ($players as $player) {
            if ($check_other_team_player && $player->poolTeamPlayer != NULL) {
                return [FALSE, trans('messages.pool.player_on_other_team')];
            } else {
                // player is not added in other teams in same pool
                if ($player->poolWaiver == NULL) {
                    // player is not active on waivers
                    $pool_team_player = $player->poolTeamPlayer()->create([
                        'pool_team_id' => $pool_team_id
                    ]);

                    $pool_team_player->created_at = config('pool.addition_deletion_time')->toDateTimeString();
                    $pool_team_player->updated_at = config('pool.addition_deletion_time')->toDateTimeString();
                    $pool_team_player->save();
                    // add new players on bench in lineup
                    $lineup_item = $pool_team_player->poolTeamLineups()->firstOrNew([
                        'start_time' => date('Y-m-d', strtotime('+1 days')), // new player's lineup will start from next day
                        'end_time' => $pool_team->pool->season->end_date
                    ]);
                    $lineup_item->player_position_id = config('pool.inverse_player_position.bn');
                    $lineup_item->created_at = config('pool.addition_deletion_time')->toDateTimeString();
                    $lineup_item->updated_at = config('pool.addition_deletion_time')->toDateTimeString();
                    $lineup_item->save();
                } else {
                    // Add player in waiver list priority
                    if ($player->poolWaiver->waiver_till->gte(Carbon::now())) {
                        $player->poolWaiver->poolWaiverClaims()->firstOrCreate([
                            'pool_team_id' => $pool_team_id
                        ]);
                    } else {
                        return [FALSE, trans('messages.pool.player_waiver_closed')];
                    }
                }
            }
        }

        return [TRUE, trans('messages.pool.player_added')];
    }

}
