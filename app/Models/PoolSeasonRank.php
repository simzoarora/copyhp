<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of CalculatedPoolPoint
 *
 * @author Anik
 */
class PoolSeasonRank extends Model {

    protected $fillable = [
        'player_id',
        'pool_id',
        'season_id',
        'rank',
    ];

    public function pool() {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    public function player() {
        return $this->belongsTo('App\Models\Player');
    }

    public function season() {
        return $this->belongsTo('App\Models\Season');
    }

}
