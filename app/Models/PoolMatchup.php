<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Season;

class PoolMatchup extends Model
{
    protected $fillable = [
        'team1',
        'team2',
        'team1_score',
        'team2_score',
        'season_competition_type_week_id',
        'type',
    ];

    public function pool()
    {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    public function poolTeam1()
    {
        return $this->belongsTo('App\Models\PoolTeam', 'team1');
    }

    public function poolTeam2()
    {
        return $this->belongsTo('App\Models\PoolTeam', 'team2');
    }

    public function seasonCompetitionTypeWeek()
    {
        return $this->belongsTo('App\Models\SeasonCompetitionTypeWeek');
    }

    public function poolH2hScores()
    {
        return $this->hasMany('App\Models\PoolH2hScore');
    }

    public function poolMatchupResults()
    {
        return $this->hasMany('App\Models\PoolMatchupResult');
    }

    public function totalPoolMatchupResults()
    {
        return $this->hasMany('App\Models\PoolMatchupResult')->selectRaw("win as total_win,loss as total_loss,tie as total_tie,score as total_score, pool_team_id, pool_matchup_id");
    }

    public static function getDataForMatchupIndividual($matchup_id, $today = true)
    {
//        \DB::enableQueryLog();
        $search_date = date('Y-m-d');
//        $search_date = '2016-10-12';
        $pool_matchup = PoolMatchup::where('id', $matchup_id)->whereHas('pool', function($query) {
                    $query->has('poolTeam');
                })->with([
                    'seasonCompetitionTypeWeek',
                ])->firstOrFail();


        $pool_matchup->load([
            'poolTeam1',
            'poolTeam1.user',
            'poolTeam1.poolMatchupResult' => function($query) {
                Pool::_addDayConditionToMatchupResult($query);
            },
            'poolTeam1.poolTeamPlayers' => function($query) use($pool_matchup) {
                $query->withTrashed()
                        ->where(\DB::raw('DATE(created_at)'), '<=', $pool_matchup->seasonCompetitionTypeWeek->end_date->toDateString())
                        ->where(function($query) use($pool_matchup) {
                                    $query->where(\DB::raw('DATE(deleted_at)'), '>=', $pool_matchup->seasonCompetitionTypeWeek->end_date->toDateString())
                                    ->orWhereNull('deleted_at');
                                });
            },
            'poolTeam1.poolTeamPlayers.poolH2hScores' => function ($query) use($today, $search_date, $pool_matchup) {
                if ($today) {
                    self::_addTodayClauseToMatchup($search_date, $query);
                }
                $query->where('pool_matchup_id', $pool_matchup->id)
                        ->groupBy('pool_team_player_id', 'pool_score_setting_id')
                        ->selectRaw("sum(value) as total_value, sum(original_stat) as total_stat,pool_team_id,pool_score_setting_id,pool_matchup_id,pool_team_player_id");
            },
            'poolTeam1.poolTeamPlayers.playerSeason.playerPosition',
            'poolTeam1.poolTeamPlayers.playerSeason.player',
            'poolTeam1.poolTeamPlayers.poolH2hScores.poolScoreSetting',
            'poolH2hScores' => function ($query) use($today, $search_date) {
//                if ($today) {
//                    self::_addTodayClauseToMatchup($search_date, $query);
//                }
                $query->groupBy('pool_team_id', 'pool_score_setting_id')
                        ->selectRaw("sum(value) as total_value,sum(original_stat) as total_stat,pool_team_id,pool_score_setting_id,pool_matchup_id,pool_team_player_id");
            },
            'poolH2hScores.poolScoreSetting',
            'poolTeam2',
            'poolTeam2.user',
            'poolTeam2.poolMatchupResult' => function($query) {
                Pool::_addDayConditionToMatchupResult($query);
            },
            'poolTeam2.poolTeamPlayers' => function($query) use($pool_matchup) {
                $query->withTrashed()
                        ->where(\DB::raw('DATE(created_at)'), '<=', $pool_matchup->seasonCompetitionTypeWeek->end_date->toDateString())
                        ->where(function($query) use($pool_matchup) {
                                    $query->where(\DB::raw('DATE(deleted_at)'), '>=', $pool_matchup->seasonCompetitionTypeWeek->end_date->toDateString())
                                    ->orWhereNull('deleted_at');
                                });
            },
            'poolTeam2.poolTeamPlayers.poolH2hScores' => function ($query) use($today, $search_date, $pool_matchup) {
                if ($today) {
                    self::_addTodayClauseToMatchup($search_date, $query);
                }
                $query->where('pool_matchup_id', $pool_matchup->id)
                        ->groupBy('pool_team_player_id', 'pool_score_setting_id')
                        ->selectRaw("sum(value) as total_value,sum(original_stat) as total_stat,pool_team_id,pool_score_setting_id,pool_matchup_id,pool_team_player_id");
            },
            'poolTeam2.poolTeamPlayers.playerSeason.playerPosition',
            'poolTeam2.poolTeamPlayers.playerSeason.player',
            'poolTeam2.poolTeamPlayers.poolH2hScores.poolScoreSetting',
            'pool.poolScoreSettings',
            'poolMatchupResults'
        ]);

        // processing for GAA & SV%
        if (
                !$pool_matchup->pool->poolScoreSettings
                        ->whereIn('pool_scoring_field_id', [config('pool.pool_scoring_field.inverse_fields.save_percentage'), config('pool.pool_scoring_field.inverse_fields.goals_against_avg')])
                        ->isEmpty()
        ) {

            for ($i = 1; $i <= 2; $i++) {
                $pool_team_var = "poolTeam$i";
                $team_player_season_ids = [];
                $pool_matchup->$pool_team_var->poolTeamPlayers->transform(function($item) use($pool_matchup, &$team_player_season_ids) {
                    if ($item->playerSeason->player_position_id == config('pool.inverse_player_position.g')) {
                        $team_player_season_ids[] = $item->playerSeason->id;
                        self::_processAdditionalDataForDisplay($pool_matchup, $item->playerSeason->id, $item);
                    }
                    return $item;
                });

                self::_processAdditionalDataForDisplay($pool_matchup, $team_player_season_ids, $pool_matchup, $pool_matchup->$pool_team_var->id);
            }
        }

        $pool_h2h_scores = NULL;
        if (!$pool_matchup->poolH2hScores->isEmpty()) {
            $pool_matchup = Pool::getCategoryWinning($pool_matchup);
            $pool_h2h_scores = $pool_matchup->poolH2hScores->groupBy('pool_team_id');
        }

        $pool_matchup_array = $pool_matchup->toArray();
        $pool_matchup_array['pool_h2h_scores'] = $pool_h2h_scores;
        return $pool_matchup_array;
//        dump(\DB::getQueryLog());
//        dd($pool_matchup_array);
    }

    private static function _processAdditionalDataForDisplay($pool_matchup, $player_season_id, &$item, $team_id = FALSE)
    {
        $goalie_stat_builder = CompetitionGoalieStat::whereHas('competition', function($query) use($pool_matchup) {
                    $query->where(\DB::raw('DATE(match_start_date)'), '>=', $pool_matchup->seasonCompetitionTypeWeek->start_date->toDateString())
                    ->where(\DB::raw('DATE(match_start_date)'), '<=', $pool_matchup->seasonCompetitionTypeWeek->end_date->toDateString());
                })
                ->selectRaw('sum(shots_against) as shots_against, sum(goals_against) as goals_against');

        $skater_stat_builder = CompetitionSkaterStat::whereHas('competition', function($query) use($pool_matchup) {
                    $query->where(\DB::raw('DATE(match_start_date)'), '>=', $pool_matchup->seasonCompetitionTypeWeek->start_date->toDateString())
                    ->where(\DB::raw('DATE(match_start_date)'), '<=', $pool_matchup->seasonCompetitionTypeWeek->end_date->toDateString());
                })
                ->selectRaw('sum(time_on_ice_secs) as time_on_ice_secs');
        if (is_array($player_season_id)) {
            $goalie_stat_builder->whereIn('player_season_id', $player_season_id);
            $skater_stat_builder->whereIn('player_season_id', $player_season_id);
        } else {
            $goalie_stat_builder->where('player_season_id', $player_season_id);
            $skater_stat_builder->where('player_season_id', $player_season_id);
        }
        $goalie_stat = $goalie_stat_builder->first();
        $skater_stat = $skater_stat_builder->first();
        $save_percentage_check = $item->poolH2hScores->search(function($search_item) use($team_id) {
            return $search_item->poolScoreSetting->pool_scoring_field_id == config('pool.pool_scoring_field.inverse_fields.save_percentage') && (($team_id) ? ($search_item->pool_team_id == $team_id) : TRUE);
        });
        if ($save_percentage_check !== FALSE) {
            $save_percentage_data_builder = $item->poolH2hScores->where('poolScoreSetting.pool_scoring_field_id', config('pool.pool_scoring_field.inverse_fields.save_percentage'));
            if ($team_id) {
                $save_percentage_data_builder = $save_percentage_data_builder->where('pool_team_id', $team_id);
            }
            $save_percentage_data = $save_percentage_data_builder->first();
            $save_percentage_data->total_stat = ($goalie_stat->shots_against != 0) ? (($goalie_stat->shots_against - $goalie_stat->goals_against) / $goalie_stat->shots_against) : NULL;
            $save_percentage_data->total_value = $save_percentage_data->total_stat * $pool_matchup->pool->poolScoreSettings->where('pool_scoring_field_id', config('pool.pool_scoring_field.inverse_fields.save_percentage'))->first()->value;
            $item->poolH2hScores->put($save_percentage_check, $save_percentage_data);
        }

        $gaa_check = $item->poolH2hScores->search(function($search_item) use($team_id) {
            return $search_item->poolScoreSetting->pool_scoring_field_id == config('pool.pool_scoring_field.inverse_fields.goals_against_avg') && (($team_id) ? ($search_item->pool_team_id == $team_id) : TRUE);
        });
        if ($gaa_check !== FALSE) {
            $gaa_data_builder = $item->poolH2hScores->where('poolScoreSetting.pool_scoring_field_id', config('pool.pool_scoring_field.inverse_fields.goals_against_avg'));
            if ($team_id) {
                $gaa_data_builder = $gaa_data_builder->where('pool_team_id', $team_id);
            }
            $gaa_data = $gaa_data_builder->first();
            $gaa_data->total_stat = ($skater_stat->time_on_ice_secs != 0) ? (($goalie_stat->goals_against * 3600) / $skater_stat->time_on_ice_secs) : NULL;
            $gaa_data->total_value = $gaa_data->total_stat * $pool_matchup->pool->poolScoreSettings->where('pool_scoring_field_id', config('pool.pool_scoring_field.inverse_fields.goals_against_avg'))->first()->value;
            $item->poolH2hScores->put($gaa_check, $gaa_data);
        }
    }

    private static function _addTodayClauseToMatchup($search_date, &$query)
    {
        $query->whereHas('competition', function($query) use($search_date) {
            $query->where(\DB::raw('DATE(match_start_date)'), $search_date);
        });
    }

    /**
     * This function create matchups for h2h pool types. it handles if user selected
     * regular pool or playoff pool.
     */
    public static function createMatchups($pool_id)
    {
        $current_season = Season::where('current', 1)->firstOrFail();
        $pool = $current_season->pools()->where('id', $pool_id)
                ->where('pool_type', config('pool.inverse_type.h2h'))
                ->has('poolTeams', '>=', 2)
                ->with('poolTeams')
                ->with('poolSetting.poolsetting')
                ->first();
        if ($pool == NULL) {
            return;
        }
        $competition_type = self::getSeasonCompetitionTypeForMatchup($current_season);
        $playoff_formats = config('pool.inverse_playoff_format');
        $pool_teams = $pool->poolTeams->all();
        $total_teams = count($pool_teams);
        switch ($pool->poolSetting->poolsetting->playoff_format) {
            case $playoff_formats['none']:
                self::saveMatchup($competition_type, $pool, $pool_teams, $total_teams, count($competition_type->seasonCompetitionTypeWeeks));
                break;
            case $playoff_formats['playoff']:
                $playoff_week_setting = $pool->poolSetting->poolsetting->playoff_schedule;
                $playoff_teams = $pool->poolSetting->poolsetting->playoff_teams_number;
                $total_weeks = count($competition_type->seasonCompetitionTypeWeeks);
                if ($playoff_teams == 4) {
                    $end_week = $total_weeks - ($playoff_week_setting + 1);
                } else {
                    $end_week = $total_weeks - ($playoff_week_setting + 2);
                }
                self::saveMatchup($competition_type, $pool, $pool_teams, $total_teams, $end_week);
                break;
        }
    }

    private static function getSeasonCompetitionTypeForMatchup($current_season)
    {
        return $current_season->seasonCompetitionTypes()->with('seasonCompetitionTypeWeeks')->where('competition_type', config('competition.inverse_competition_type.Regular Season'))->first();
    }

    private static function saveMatchup($competition_type, $pool, $pool_teams, $total_teams, $end_week)
    {
        $chunked_pool_team = array_chunk($pool_teams, ($total_teams / 2)); // divide teams in 2 parts to create
        foreach ($competition_type->seasonCompetitionTypeWeeks as $key => $week) {
            if ($pool->poolSetting->poolsetting->league_start_week > $week->id || $key + 1 > $end_week) {
                continue;
            }
            foreach ($chunked_pool_team[0] as $pool_team_key => $pool_team) {
                $pool->poolMatchups()->firstOrCreate([
                    'team1' => $pool_team->id,
                    'team2' => $chunked_pool_team[1][$pool_team_key]->id,
                    'season_competition_type_week_id' => $week->id,
                ]);
            }

            $shifted_value = array_shift($chunked_pool_team[0]);
            array_push($chunked_pool_team[0], $shifted_value); // pool team rotated sequentially
        }
    }
}