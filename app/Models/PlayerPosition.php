<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerPosition extends Model
{
    protected $fillable = [];
    public $timestamps = false;

    public function player_season()
    {
        return $this->hasMany('App\Models\PlayerSeason');
    }

    public function poolTeamLineups()
    {
        return $this->hasMany('App\Models\PoolTeamLineup');
    }

    public function poolTeamLineupCount()
    {
        return $this->hasOne('App\Models\PoolTeamLineup')->selectRaw('id, player_position_id, count(*) as total')->groupBy('player_position_id');
    }

    public static function getDataForSelectBox()
    {
        return self::where('id', '<>', config('pool.inverse_player_position.bn'))->orderBy('sort_order')->lists('name', 'id')->toArray();
    }
}