<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolWaiverClaim extends Model {

    protected $fillable = [
        'pool_waiver_id',
        'pool_team_id',
    ];

    public function poolWaiver() {
        return $this->belongsTo('App\Models\PoolWaiver');
    }

    public function poolTeam() {
        return $this->belongsTo('App\Models\PoolTeam');
    }

}
