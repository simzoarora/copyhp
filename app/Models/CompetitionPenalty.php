<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetitionPenalty extends Model {

    protected $guarded = [
        'id',
        'competition_id',
        'updated_at',
        'created_at'
    ];

    public function competition() {
        return $this->belongsTo('App\Models\Competition');
    }
    public function playerSeason() {
        return $this->belongsTo('App\Models\PlayerSeason');
    }

}
