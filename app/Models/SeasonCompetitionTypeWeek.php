<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeasonCompetitionTypeWeek extends Model
{
    protected $dates = ['start_date', 'end_date'];
    protected $fillable = [
        'name',
        'start_date',
        'end_date',
    ];
    protected $appends = ['name_with_date'];

    /**
     * accessor for full name
     */
    public function getNameWithDateAttribute()
    {
        if ($this->start_date != NULL && $this->end_date != NULL) {
            return $this->name . ' - ' . $this->start_date->format('M d') . ' to ' . $this->end_date->format('M d');
        } else {
            return $this->name;
        }
    }

    public function seasonCompetitionType()
    {
        return $this->belongsTo('App\Models\SeasonCompetitionType');
    }

    public function poolMatchups()
    {
        return $this->hasMany('App\Models\PoolMatchup');
    }

    public static function getSeasonTypeWeeks($all_weeks = FALSE)
    {
        $season_competition_type_weeks_builder = self::whereHas('seasonCompetitionType', function($query) {
                    $query->where('competition_type', config('competition.inverse_competition_type.Regular Season'));
                });
        if (!$all_weeks) {
            $season_competition_type_weeks_builder->where('end_date', '>=', date('Y-m-d'));
        }
        $season_competition_type_weeks = $season_competition_type_weeks_builder->get();
        $season_weeks = [];
        foreach ($season_competition_type_weeks as $week) {
            $season_weeks[$week->id] = $week->name . ' (' . $week->start_date->toFormattedDateString() . ')';
        }
        return $season_weeks;
    }
}