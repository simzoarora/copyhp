<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoredValue extends Model {

    protected $fillable = ['key', 'value'];
    public $timestamps = false;

}
