<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;

class LiveDraftQueue extends Model
{

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['player_season_id'];

    public function poolTeam()
    {
        return $this->belongsTo('App\Models\PoolTeam');
    }

    public function playerSeason()
    {
        return $this->belongsTo('App\Models\PlayerSeason');
    }

    public static function getDataForLiveDraft($pool_id)
    {
        return self::whereHas('poolTeam', function($query) {
                            $query->currentUserTeam();
                        })
                        ->whereHas('poolTeam.pool', function($query) use($pool_id){
                            $query->where('id', $pool_id)->active();
                        })
                        ->with([
                            'playerSeason' => function($query) {
                                $query->select('id', 'player_id', 'team_id', 'player_position_id');
                            },
                            'playerSeason.player' => function($query) {
                                $query->select('id', 'first_name', 'last_name');
                            },
                            'playerSeason.playerPosition' => function($query) {
                                $query->select('id', 'short_name');
                            },
                            'playerSeason.team' => function($query) {
                                $query->select('id', 'short_name');
                            },
                        ])
                        ->select('id', 'pool_team_id', 'player_season_id')
                        ->where('is_used', 0)
                        ->orderBy('sort_order')
                        ->get();
    }
}