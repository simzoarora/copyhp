<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;

class PoolH2hScore extends Model {

    use SoftDeletes;

    protected $fillable = [
        'pool_score_setting_id',
        'pool_team_id',
        'competition_id',
        'pool_team_player_id',
        'value',
    ];

    public function poolScoreSetting() {
        return $this->belongsTo('App\Models\PoolScoreSetting');
    }

    public function poolMatchup() {
        return $this->belongsTo('App\Models\PoolMatchup');
    }

    public function poolTeam() {
        return $this->belongsTo('App\Models\PoolTeam');
    }

    public function competition() {
        return $this->belongsTo('App\Models\Competition');
    }

    public function poolTeamPlayer() {
        return $this->belongsTo('App\Models\PoolTeamPlayer');
    }

}
