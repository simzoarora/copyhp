<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;
use Carbon\Carbon;
use Auth;

class PlayerSeason extends Model
{

    use SoftDeletes;
    public $timestamps = true;
    public $table = "player_season";
    protected $guarded = ['id', 'updated_at', 'created_at'];

    public function season()
    {
        return $this->belongsTo('App\Models\Season');
    }

    public function player()
    {
        return $this->belongsTo('App\Models\Player');
    }

    public function playerPosition()
    {
        return $this->belongsTo('App\Models\PlayerPosition');
    }

    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }

    public function injuredPlayer()
    {
        return $this->hasOne('App\Models\InjuredPlayer');
    }

    public function competitionSkaterStats()
    {
        return $this->hasMany('App\Models\CompetitionSkaterStat');
    }

    public function competitionGoalieStats()
    {
        return $this->hasMany('App\Models\CompetitionGoalieStat');
    }

    public function poolTeamPlayers()
    {
        return $this->hasMany('App\Models\PoolTeamPlayer');
    }

    public function poolWaivers()
    {
        return $this->hasMany('App\Models\PoolWaiver');
    }

    /**
     * Specifically used to get 1st result
     * @return type
     */
    public function poolWaiver()
    {
        return $this->hasOne('App\Models\PoolWaiver');
    }

    /**
     * This function is used only where we desperately need first result
     * @return type
     */
    public function poolTeamPlayer()
    {
        return $this->hasOne('App\Models\PoolTeamPlayer');
    }

    /**
     * This function is used only where we desperately need first result
     * @return type
     */
    public function competitionSkaterStat()
    {
        return $this->hasOne('App\Models\CompetitionSkaterStat');
    }

    /**
     * This function is used only where we desperately need first result
     * @return type
     */
    public function competitionGoalieStat()
    {
        return $this->hasOne('App\Models\CompetitionGoalieStat');
    }

    public function totalPoolsAddedIn()
    {
        return $this->hasOne('App\Models\PoolTeamPlayer')
                        ->whereHas('poolTeam', function($query) {
                            $query->whereHas('pool', function($query) {
                                $query->active();
                            });
                        })
                        ->selectRaw('player_season_id, count(*) as count')
                        ->groupBy('player_season_id');
    }

    /**
     * This function is used to bypass pool team player table to get the data
     * about how many pools added particular player to its lineup
     * @return type
     */
    public function totalPoolTeamLineup()
    {
        return $this->hasManyThrough('App\Models\PoolTeamLineup', 'App\Models\PoolTeamPlayer')
                        ->where(\DB::raw('DATE(start_time)'), '<=', date('Y-m-d'))
                        ->where(\DB::raw('DATE(end_time)'), '>=', date('Y-m-d'))
                        ->whereNotIn('player_position_id', [config('pool.inverse_player_position.bn'), config('pool.inverse_player_position.ir')])
                        ->selectRaw('pool_team_player_id, count(*) as count')
                        ->groupBy('pool_team_player_id');
    }

    public function currentWeekAppearance()
    {
        $carbon_date = Carbon::now();
        return $this->_getAppearanceAssociation($carbon_date);
    }

    public function lastWeekAppearance()
    {
        $carbon_date = Carbon::now()->subWeek();
        return $this->_getAppearanceAssociation($carbon_date);
    }

    private function _getAppearanceAssociation($carbon_date)
    {
        $start_end_array = [
            $carbon_date->startOfWeek()->toDateString(),
            $carbon_date->endOfWeek()->toDateString(),
        ];
        return $this->hasOne('App\Models\competitionSkaterStat')
                        ->whereHas('competition', function($query) use($start_end_array) {
                            $query->whereIn(\DB::raw('match_start_date'), $start_end_array);
                        })
                        ->where('time_on_ice_secs', '>', 0)
                        ->selectRaw('id, player_season_id, count(*) as count')
                        ->groupBy('player_season_id');
    }

    /**
     * Adding scope to model for retriving only active data
     * @param type $query
     * @return type
     */
    public function scopeCurrent($query)
    {
        return $query->whereHas('season', function($query) {
                    $query->where('current', 1);
                });
    }

    /**
     * Adding scope to model for retriving only active data
     * @param type $query
     * @return type
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', '=', 1);
    }

    /**
     * Adding scope to model for retriving only active data
     * @param type $query
     * @return type
     */
    public function scopeActiveCurrent($query)
    {
        return $query->whereHas('season', function($query) {
                    $query->where('current', 1);
                })->where('is_active', 1);
    }

    public static function transformPlayerDataToAddPoints($field_scoring, &$player_data)
    {
        $player_data->transform(function ($item, $key)use($field_scoring) {
            $item->total_points = 0;
            $skater_stat = $item->player->playerStat;
            foreach ($field_scoring as $field => $score_setting) {
                if (isset($skater_stat[$field])) {
                    $item->total_points += $skater_stat[$field] * $score_setting['value'];
                }
            }
            return $item;
        });
    }

    /**
     * This is a temporary function
     * @param type $field_scoring
     * @param type $player_data
     */
    public static function transformPlayerDataToAddPoints1($field_scoring, &$player_data)
    {
        $player_data->transform(function ($item, $key)use($field_scoring) {
            $item['total_points'] = 0;
            $skater_stat = $item['player']['player_stat'];
//            dump($skater_stat);
            foreach ($field_scoring as $field => $score_setting) {
//            dump($score_setting);
                if (isset($skater_stat[$score_setting['pool_scoring_field_id']])) {
//                    die('here');
                    $item['total_points'] += $skater_stat[$score_setting['pool_scoring_field_id']] * $score_setting['value'];
                }
            }
            return $item;
        });
    }

    public static function sortAndSliceLeagueLeaderData(&$player_data, $sort_function_name, $order_key, $per_page_limit, $page, $pool, $field_name)
    {
        return $player_data->$sort_function_name($order_key)->slice($per_page_limit * $page, $per_page_limit)->transform(function ($item, $key)use($pool, $field_name) {
                    if ($pool->pool_type != config('pool.inverse_type.box')) {
                        if ($item->poolTeamPlayer == NULL) {
                            $item->free_agent = TRUE;
                        } else {
                            $item->free_agent = FALSE;
                        }
                    }
                    if ($field_name != NULL) {
                        // bringing the requested stat field in main array
                        $item->$field_name = (isset($item->player->playerStat->$field_name)) ? $item->player->playerStat->$field_name : NULL;
                    }
                    unset($item->player->playerStat);
                    return $item;
                });
    }

    public static function getQueryBuilderForLeagueLeaders($pool, $request, $fields_to_find, $pool_team_ids, $season_id, $field_scoring)
    {
        $pool_id = $pool->id;
        $player_season_query_builder = PlayerSeason::where('player_season.season_id', $pool->season_id)
                        ->join('players', function($join) {
                            $join->on('player_season.player_id', '=', 'players.id');
                        })
                        ->join('teams', 'teams.id', '=', 'player_season.team_id')
                        ->join('player_positions', 'player_season.player_position_id', '=', 'player_positions.id')
                        ->select('player_season.*')
                        ->with([
                            'player',
                            'team',
                            'playerPosition',
                            'poolTeamPlayer' => function($query) use($pool_team_ids) {
                                $query->whereIn('pool_team_id', $pool_team_ids);
                            },
                            'poolTeamPlayer.poolTeam',
                        ])->groupBy('player_season.id');

        self::_checkAndAddBoxPoolConditionForFindingPlayers($pool, $pool_team_ids, $player_season_query_builder);

        if (isset($request->data_retrieval) && $request->data_retrieval == 'season') {
            self::_buildQueryForSeasonStat($request, $field_scoring, $season_id, $pool, $player_season_query_builder);
        } else {
            self::_getCompetitionsAndBuildQueryForDailyData($request, $pool, $fields_to_find, $season_id, $player_season_query_builder);
        }

        if (isset($request->player_position_id) && $request->player_position_id != "") {
            // if user selected a position filter on frontend
            self::_filterPlayerPosition($request->player_position_id, $player_season_query_builder);
        }

        return $player_season_query_builder;
    }

    /**
     * This function check if we are finding data for any pool except box pool type.
     * If so, it joins with pool_team_players and ultimately with pool_teams as we
     * need pool team of that player to be displayed on frontend. For BOX pool, we dont 
     * need any pool team because box pool player can be added in multiple
     * pool team.
     * @param type $pool
     * @param type $pool_team_ids
     * @param type $player_season_query_builder
     */
    private static function _checkAndAddBoxPoolConditionForFindingPlayers($pool, $pool_team_ids, &$player_season_query_builder)
    {
        if ($pool->pool_type != config('pool.inverse_type.box')) {
            /**
             * we need pool team if pool is of type h2h or standard as box can have multiple team
             */
            $player_season_query_builder = $player_season_query_builder->leftJoin('pool_team_players', function($join) use($pool_team_ids) {
                        $join->on('player_season.id', '=', 'pool_team_players.player_season_id')
                        ->whereIn('pool_team_players.pool_team_id', $pool_team_ids);
                    })
                    ->leftJoin('pool_teams', 'pool_teams.id', '=', 'pool_team_players.pool_team_id');
        }
    }

    private static function _getCompetitionsAndBuildQueryForDailyData($request, $pool, $fields_to_find, $season_id, &$player_season_query_builder)
    {
        // first finding competition id of date range user selected and use these competition id to get data
        $competitions = Competition::whereBetween(\DB::raw('DATE(match_start_date)'), [$request->start_date, $request->end_date])
                ->where('competition_type', $pool->season_type)
                ->lists('id');
        if (!$competitions->isEmpty()) {
            $select_string = implode(',', array_merge($fields_to_find['skater'], $fields_to_find['goalie'], $fields_to_find['extra']));
            $player_season_query_builder = self::_buildQueryForDailyData($player_season_query_builder, $competitions, $season_id, $pool->id, $request, $fields_to_find, $select_string);

            self::addOrderByForPlayerStats($request, $player_season_query_builder, TRUE, FALSE);
        } else {
            self::addOrderByForPlayerStats($request, $player_season_query_builder, FALSE, FALSE);
        }
    }

    private static function _buildQueryForDailyData($player_season_query_builder, $competitions, $season_id, $pool_id, $request, $fields_to_find, $select_string)
    {
        return $player_season_query_builder
                        ->leftJoin('competition_skater_stats', function($join)use($competitions) {
                            $join->on('competition_skater_stats.player_season_id', '=', 'player_season.id')
                            ->whereIn('competition_skater_stats.competition_id', $competitions->toArray());
                        })
                        ->leftJoin('competition_goalie_stats', function($join)use($competitions) {
                            $join->on('competition_goalie_stats.player_season_id', '=', 'player_season.id')
                            ->whereIn('competition_goalie_stats.competition_id', $competitions->toArray());
                        })
                        ->leftJoin('calculated_pool_points', function($join)use($season_id, $pool_id, $request) {
                            $join->on('calculated_pool_points.player_id', '=', 'players.id')
                            ->on('calculated_pool_points.competition_id', '=', 'competition_skater_stats.competition_id')
                            ->where('calculated_pool_points.season_id', '=', $season_id)
                            ->where('calculated_pool_points.pool_id', '=', $pool_id)
                            ->where(\DB::raw('DATE(points_date)'), '>=', $request->start_date)
                            ->where(\DB::raw('DATE(points_date)'), '<=', $request->end_date);
                        })
                        ->with([
                            'competitionSkaterStat' => function($query) use($competitions, $fields_to_find) {
                                $query->whereIn('competition_id', $competitions->toArray())
                                ->selectRaw('id,player_season_id,sum(time_on_ice_secs) as time_on_ice_secs' . (!empty($fields_to_find['skater']) ? ',' . implode(', ', $fields_to_find['skater']) : ''))
                                ->groupBy('player_season_id');
                            },
                            'competitionGoalieStat' => function($query) use($competitions, $fields_to_find) {
                                $query->whereIn('competition_id', $competitions->toArray())
                                ->selectRaw('id,player_season_id,sum(goals_against) as goals_against' . (!empty($fields_to_find['goalie']) ? ',' . implode(', ', $fields_to_find['goalie']) : ''))
                                ->groupBy('player_season_id');
                            },
                            'player.calculatedPoolPoint' => function($query) use($season_id, $pool_id, $request, $competitions) {
                                $query->where('calculated_pool_points.season_id', '=', $season_id)
                                ->where('calculated_pool_points.pool_id', '=', $pool_id)
                                ->where(\DB::raw('DATE(points_date)'), '>=', $request->start_date)
                                ->where(\DB::raw('DATE(points_date)'), '<=', $request->end_date)
                                ->whereIn('competition_id', $competitions->toArray())
                                ->selectRaw('id, player_id, sum(calculated_pool_points.total_points) as total_points')
                                ->groupBy('player_id');
                            }
                        ])
                        ->selectRaw($select_string . ',sum(calculated_pool_points.total_points) as total_points');
//                                        ->groupBy('player_season.id','calculated_pool_points.pool_id','calculated_pool_points.season_id','calculated_pool_points.player_id');
    }

    public static function _buildQueryForSeasonStat($request, $field_scoring, $season_id, $pool, &$player_season_query_builder)
    {// only if user want to see full season data
        // as save percentage is not actual field in sports direct, this needs to be used
        if (isset($field_scoring['save_percentage'])) {
            $field_scoring['(sum(shots_against) - sum(goals_against)) / sum(shots_against) as save_percentage'] = '';
            unset($field_scoring['save_percentage']);
        }

        // games_played is added for LIVE DRAFT page. So, currently done for season stats only
        $select_array = array_merge(['id', 'season_id', 'player_id', 'games_played'], array_keys($field_scoring));

        $player_season_query_builder = $player_season_query_builder
                ->leftJoin('player_stats', function($join)use($season_id, $pool) {
                    $join->on('player_stats.player_id', '=', 'players.id')
                    ->where('player_stats.season_id', '=', $season_id)
                    ->where('player_stats.stat_key', '=', PlayerStat::getStatKeyBySeasonType($pool->season_type));
                })
                ->leftJoin('season_pool_points', function($join)use($season_id, $pool) {
                    $join->on('season_pool_points.player_id', '=', 'players.id')
                    ->where('season_pool_points.season_id', '=', $season_id)
                    ->where('season_pool_points.pool_id', '=', $pool->pool_id);
                })
                ->with([
            'player.playerStat' => function($query)use($season_id, $select_array, $pool) {
                $query->where('season_id', $season_id)
                ->where('stat_key', PlayerStat::getStatKeyBySeasonType($pool->season_type))
                ->groupBy('id')
                ->selectRaw(implode(',', $select_array));
            },
            'player.seasonPoolPoint' => function($query) use($season_id, $pool) {
                $query->where('season_id', $season_id)
                ->where('pool_id', $pool->pool_id);
            },
        ]);
        self::addOrderByForPlayerStats($request, $player_season_query_builder);
    }

    public static function _applyAdditionalConditionForLiveDraftSearch($request, $pool_id, &$player_season_query_builder)
    {
        if (isset($request->live_draft_search_player)) {
            $player_season_query_builder->whereNotExists(function($subquery)use($pool_id) {
                        $subquery->select(\DB::raw(1))
                        ->from('pool_team_players')
                        ->whereRaw('pool_team_players.player_season_id = player_season.id')
                        ->join('pool_teams', 'pool_teams.id', '=', 'pool_team_players.pool_team_id')
                        ->where('pool_teams.pool_id', '=', $pool_id);
                    })
                    ->whereNotExists(function($subquery)use($pool_id) {
                        $subquery->select(\DB::raw(1))
                        ->from('live_draft_queues')
                        ->whereRaw('live_draft_queues.player_season_id = player_season.id')
                        ->join('pool_teams', 'pool_teams.id', '=', 'live_draft_queues.pool_team_id')
                        ->where('pool_teams.pool_id', '=', $pool_id)
                        ->where('pool_teams.user_id', '=', Auth::id());
                    });
        }
    }

    public static function getQueryBuilderForPlayerStats($season_id, $request, $pool_team_ids, $fields_to_find, $field_scoring, $pool)
    {
        $player_season_query_builder = self::_buildBaseQueryForPlayerStatApi($pool, $request, $season_id, $pool_team_ids);
        self::_checkAndAddBoxPoolConditionForFindingPlayers($pool, $pool_team_ids, $player_season_query_builder);
        self::_applyFiltersToPlayerStats($request, $pool_team_ids, $player_season_query_builder);
        self::_applyAdditionalConditionForLiveDraftSearch($request, $pool->id, $player_season_query_builder);

        if (isset($request->data_retrieval) && $request->data_retrieval == 'season') {
            self::_buildQueryForSeasonStat($request, $field_scoring, $season_id, $pool, $player_season_query_builder);
        } else {
            self::_getCompetitionsAndBuildQueryForDailyData($request, $pool, $fields_to_find, $season_id, $player_season_query_builder);
        }

        return $player_season_query_builder;
    }

    /**
     * Function to build base query for player stats API.
     * Joins are used below to get the data sorted overall.
     * Eloquent relationship method cannot do sorting in multiple tables as 
     * it add a new query for every relationship.
     * Joins are just used to sort the data. For getting actual data eloquent
     * relationships are used.
     * @param type $pool
     * @param type $request
     * @param type $season_id
     * @param type $pool_team_ids
     * @return type
     */
    private static function _buildBaseQueryForPlayerStatApi($pool, $request, $season_id, $pool_team_ids)
    {
        $pool_id = $pool->id;

        $with = [
            'player',
            'playerPosition',
            'team' => function($query) {
                $query->select('id', 'logo', 'short_name');
            }
        ];

        if (isset($request->live_draft) && $request->live_draft == 1) {
            $with = array_merge($with, [
                'player.playerPreRanking' => function($query) use($season_id) {
                    $query->where('season_id', $season_id)->select('id', 'player_id', 'rank');
                }
            ]);
        } else {
            $with = array_merge($with, [
                'poolTeamPlayer' => function($query) use($pool_team_ids) {
                    $query->whereIn('pool_team_id', $pool_team_ids)->withFutureData();
                },
                'poolTeamPlayer.poolTeam',
                'poolTeamPlayer.poolTradePlayer' => function($query) {
                    $query->whereHas('poolTrade', function($query) {
                                $query->where('is_processed', 0);
                            });
                },
                'totalPoolsAddedIn',
                'player.poolSeasonRank' => function($query) use($season_id, $pool_id) {
                    $query->where('season_id', $season_id)
                            ->where('pool_id', $pool_id);
                },
                'poolWaiver' => function($query) use($pool_id) {
                    $query->notCompleted()->where('pool_id', $pool_id);
                },
            ]);
        }
        return self::where('player_season.season_id', $pool->season_id)
                        ->where('is_active', 1)
                        ->join('players', function($join)use($request) {
                            $join->on('player_season.player_id', '=', 'players.id');
                            // If user queried using player name
                            if (isset($request->player_name) && $request->player_name != "") {
                                $join->where(function($query)use($request) {
                                    $exploded_name = explode(' ', $request->player_name);
                                    if (count($exploded_name) > 1) {
                                        $query->where('players.first_name', 'LIKE', "%" . $exploded_name[0] . "%")
                                        ->where('players.last_name', 'LIKE', "%" . $exploded_name[1] . "%");
                                    } else {
                                        $query->where('players.first_name', 'LIKE', "%" . $request->player_name . "%")
                                        ->orWhere('players.last_name', 'LIKE', "%" . $request->player_name . "%");
                                    }
                                });
                            }
                        })
                        ->join('player_positions', 'player_season.player_position_id', '=', 'player_positions.id')
                        ->leftJoin('pool_season_ranks', function($join) use($season_id, $pool_id) {
                            $join->on('pool_season_ranks.player_id', '=', 'players.id')
                            ->where('pool_season_ranks.season_id', '=', $season_id)
                            ->where('pool_season_ranks.pool_id', '=', $pool_id);
                        })
                        ->select('player_season.*')
                        ->with($with)
                        ->groupBy('player_season.id');
    }

    /**
     * Function to apply all filters to player stats API
     * @param type $request
     * @param type $pool_team_ids
     * @param type $player_season_query_builder 
     */
    private static function _applyFiltersToPlayerStats($request, $pool_team_ids, &$player_season_query_builder)
    {
        // This is for filtering pool team
        if (isset($request->player_season_id) && $request->player_season_id != "") {
            $player_season_query_builder = $player_season_query_builder->where('player_season.id', $request->player_season_id);
        }

        // This is for filtering pool team
        if (isset($request->team) && $request->team != "") {
            self::_filterPoolTeam($request->team, $pool_team_ids, $player_season_query_builder);
        }

        // This is for filtering NHL team
        if (isset($request->nhl_team) && $request->nhl_team != "") {
            self::_filterTeam($request->nhl_team, $player_season_query_builder);
        }

        if (isset($request->player_position_id) && $request->player_position_id != "") {
            // if user selected a position filter on frontend
            self::_filterPlayerPosition($request->player_position_id, $player_season_query_builder);
        }
    }

    /**
     * Function to apply player position filter on API. NHL position is filtered
     * through this.
     * @param type $player_position_id
     * @param type $player_season_query_builder
     */
    private static function _filterPlayerPosition($player_position_id, &$player_season_query_builder)
    {
        $player_position_grouping = config('pool.player_position_grouping');
        if (isset($player_position_grouping[$player_position_id])) {
            $player_season_query_builder->whereIn('player_position_id', $player_position_grouping[$player_position_id]);
        } else {
            $player_season_query_builder->where('player_position_id', $player_position_id);
        }
    }

    /**
     * Function to filter players based on pool team selected inside a pool.
     * It also handles waiver players
     * @param type $team
     * @param type $pool_team_ids
     * @param type $player_season_query_builder
     */
    private static function _filterPoolTeam($team, $pool_team_ids, &$player_season_query_builder)
    {
        // If user selected anything from team dropdown
        if ($team > 0) {
            // If user actually selected a team but NOT waivers or Free Agent
            $player_season_query_builder = $player_season_query_builder->whereHas('poolTeamPlayers', function($query) use($team) {
                $query->where('pool_team_id', $team);
            });
        } else {
            //TODO - Get waiver players
            $player_season_query_builder = $player_season_query_builder->whereNotExists(function($subquery)use($pool_team_ids) {
                $subquery->select(\DB::raw(1))
                        ->from('pool_team_players')
                        ->whereRaw('pool_team_players.player_season_id = player_season.id')
                        ->whereIn('pool_team_id', $pool_team_ids);
            });
        }
    }

    /**
     * Function to filter players based on NHL team selected. This is used in 
     * LIVE DRAFT page.
     * @param type $nhl_team
     * @param type $player_season_query_builder
     */
    private static function _filterTeam($nhl_team, &$player_season_query_builder)
    {
        $player_season_query_builder = $player_season_query_builder->whereHas('team', function($query) use($nhl_team) {
            $query->where('nick_name', $nhl_team);
        });
    }

    private static function addOrderByForPlayerStats($request, &$player_season_query_builder, $stat_order = TRUE, $season = TRUE)
    {
        if (isset($request->order)) {
            switch ($request->order) {
                case 'team':
                    $player_season_query_builder = $player_season_query_builder->orderBy('teams.display_name', $request->order_type);
                    break;
                case 'pool_team':
                    $player_season_query_builder = $player_season_query_builder->orderBy('pool_teams.name', $request->order_type);
                    break;
                case 'position':
                    $player_season_query_builder = $player_season_query_builder->orderBy('player_positions.short_name', $request->order_type);
                    break;
                case 'player':
                    $player_season_query_builder = $player_season_query_builder->orderBy('players.first_name', $request->order_type)->orderBy('players.last_name', $request->order_type);
                    break;
                case 'nba_team':
                    $player_season_query_builder = $player_season_query_builder->leftJoin('teams', 'player_season.team_id', '=', 'teams.id')->orderBy('teams.display_name', $request->order_type);
                    break;
                case 'stat':
                    if ($stat_order) {
                        switch ($request->stat_order_key) {
                            case 'current':
                                $player_season_query_builder = $player_season_query_builder->orderBy('pool_season_ranks.rank', $request->order_type);
                                break;
                            case 'own':
                                break;
                            case 'points':
                                if ($season) {
                                    $player_season_query_builder = $player_season_query_builder->orderBy('season_pool_points.total_points', $request->order_type);
                                } else {
                                    $player_season_query_builder = $player_season_query_builder->orderBy('total_points', $request->order_type);
                                }
                                break;
                            case 'games_played':
                                // This order is just used in LIVE DRAFT page. So, currently done for season stats only
                                $player_season_query_builder = $player_season_query_builder->orderBy('player_stats.games_played', $request->order_type);
                                break;
                            case 'pre_ranking':
                                self::_sortByPreRanking($player_season_query_builder, $request);
                                break;
                            default :
                                $sort_key = config('pool.pool_scoring_field.fields_map_to_stat_table')[$request->stat_order_key];
                                $player_season_query_builder = $player_season_query_builder->orderBy($sort_key, $request->order_type);
                                break;
                        }
                    }
                    break;
            }
        } elseif (isset($request->live_draft)) {
            self::_sortByPreRanking($player_season_query_builder, $request);
        }
    }

    private static function _sortByPreRanking(&$player_season_query_builder, $request)
    {
        $player_season_query_builder->leftJoin('player_pre_rankings', function($join) use($request) {
                    $join->on('player_pre_rankings.player_id', '=', 'players.id')
                    ->where('player_pre_rankings.season_id', '=', $request->season_id);
                })
                ->orderBy('player_pre_rankings.rank', $request->order_type);
    }

    public static function getPoolStatForPlayers($pool_id, $player_season_ids)
    {
        if (!is_array($player_season_ids)) {
            $player_season_ids = explode(',', $player_season_ids);
        }
        $pool = Pool::where('id', $pool_id)
                ->has('poolTeam')
                ->with('poolScoreSettings')
                ->firstOrFail();
        $field_scoring = [];
        $all_pool_scoring_fields = config('pool.pool_scoring_field.inverse_fields');
        foreach ($pool->poolScoreSettings as $score_setting) {
            switch ($score_setting['pool_scoring_field_id']) {
                case $all_pool_scoring_fields['save_percentage']:
                    $field_scoring['(sum(shots_against) - sum(goals_against)) / sum(shots_against) as save_percentage'] = $score_setting->toArray();
                    break;
                default:
                    $field_scoring[config('pool.pool_scoring_field.fields_map_to_stat_table')[$score_setting['pool_scoring_field_id']]] = $score_setting->toArray();
                    break;
            }
        }
        $select_array = array_merge(['id', 'season_id', 'player_id'], array_keys($field_scoring));
        $player_data = PlayerSeason::whereIn('id', $player_season_ids)
                        ->with([
                            'player',
                            'player.seasonPoolPoint' => function($query) use($pool_id) {
                                $query->whereHas('season', function($query) {
                                            $query->current();
                                        })
                                ->where('pool_id', $pool_id);
                            },
                            'player.poolSeasonRank' => function($query) use($pool_id) {
                                $query->whereHas('season', function($query) {
                                            $query->current();
                                        })
                                ->where('pool_id', $pool_id);
                            },
                            'player.playerPreRanking' => function($query) {
                                $query->whereHas('season', function($query) {
                                            $query->current();
                                        });
                            },
                            'player.playerStat' => function($query) use($select_array) {
                                $query->whereHas('season', function($query) {
                                            $query->current();
                                        })
                                ->where('stat_key', config('pool.stat_keys.regular'))
                                ->groupBy('id')
                                ->selectRaw(implode(',', $select_array));
                                ;
                            },
                            'team.homeCompetition' => function($query) { // for getting today's match of player
                                $query->where(\DB::raw('DATE(match_start_date)'), date('Y-m-d'));
                            },
                            'team.homeCompetition.awayTeam',
                            'team.awayCompetition' => function($query) { // for getting today's match of player
                                $query->where(\DB::raw('DATE(match_start_date)'), date('Y-m-d'));
                            },
                            'team.awayCompetition.homeTeam',
                            'playerPosition',
                            'totalPoolsAddedIn', // for calculating percentage of pools which have got this player
                            'totalPoolTeamLineup', // for calculating percentage of pools which have this player in starting lineup
                        ])->get();

        $total_pool = Pool::active()->where('season_id', $pool->season_id)->count();
        $player_data->transform(function($item)use($total_pool, $pool) {
            $item->own = round(( (isset($item->totalPoolsAddedIn->count)) ? ($item->totalPoolsAddedIn->count / $total_pool * 100) : 0), 2);
            $totalPoolTeamLineup = NULL;
            if (!$item->totalPoolTeamLineup->isEmpty()) {
                $totalPoolTeamLineup = $item->totalPoolTeamLineup->first();
            }
            $item->start = round((isset($totalPoolTeamLineup->count)) ? (($totalPoolTeamLineup->count / $total_pool) * 100) : 0, 2);
            $player_stat = [];
            if ($item->player->playerStat != NULL) {
                foreach ($pool->poolScoreSettings as $score_setting) {
                    $player_stat[$score_setting['pool_scoring_field_id']] = $item->player->playerStat->{config('pool.pool_scoring_field.fields_map_to_stat_table')[$score_setting['pool_scoring_field_id']]};
                }
            }
            $item->player_stat = $player_stat;
            return $item;
        });
        return $player_data;
    }
}