<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;

class PoolTeamLineup extends Model {

    use SoftDeletes;

    protected $fillable = [
        'start_time',
        'end_time',
        'pool_team_player_id',
        'player_position_id',
    ];

    public function poolTeam() {
        return $this->belongsTo('App\Models\PoolTeam');
    }

    public function poolTeamPlayer() {
        return $this->belongsTo('App\Models\PoolTeamPlayer');
    }

    public function playerPosition() {
        return $this->belongsTo('App\Models\PlayerPosition');
    }

    public function scopeCurrent($query) {
        return $query->where('start_time', '<=', date('Y-m-d'))->where('end_time', '>=', date('Y-m-d'));
    }
    
    
    public static function savePlayerLineup($pool_team_player, $season_end_date, &$team_roster_track)
    {
        $player_position_id = $pool_team_player->playerSeason->player_position_id;
        $lineup_item = $pool_team_player->poolTeamLineups()->firstOrNew([
            'start_time' => date('Y-m-d'),
            'end_time' => $season_end_date
        ]);

        $position_array = config('pool.inverse_player_position');

        switch ($player_position_id) {
            case $position_array['g']:
                if (isset($team_roster_track[$player_position_id]) && $team_roster_track[$player_position_id]['used'] < $team_roster_track[$player_position_id]['total']) {
                    $lineup_item->player_position_id = $player_position_id;
                    $team_roster_track[$player_position_id]['used'] ++;
                } else {
                    $lineup_item->player_position_id = config('pool.inverse_player_position.bn');
                }
                break;
            case $position_array['c']:
                self::_setPlayerPositionBasedOnEmptyLineup($player_position_id, $position_array, $team_roster_track, $lineup_item);
                break;
            case $position_array['lw']:
                self::_setPlayerPositionBasedOnEmptyLineup($player_position_id, $position_array, $team_roster_track, $lineup_item);
                break;
            case $position_array['rw']:
                self::_setPlayerPositionBasedOnEmptyLineup($player_position_id, $position_array, $team_roster_track, $lineup_item);
                break;
            case $position_array['d']:
                if (isset($team_roster_track[$player_position_id]) && $team_roster_track[$player_position_id]['used'] < $team_roster_track[$player_position_id]['total']) {
                    $lineup_item->player_position_id = $player_position_id;
                    $team_roster_track[$player_position_id]['used'] ++;
                } elseif (isset($team_roster_track[$position_array['s']]) && $team_roster_track[$position_array['s']]['used'] < $team_roster_track[$position_array['s']]['total']) {
                    $lineup_item->player_position_id = $position_array['s'];
                    $team_roster_track[$position_array['s']]['used'] ++;
                } else {
                    $lineup_item->player_position_id = config('pool.inverse_player_position.bn');
                }
                break;
        }
        $lineup_item->save();
    }

    private static function _setPlayerPositionBasedOnEmptyLineup($player_position_id, $position_array, &$team_roster_track, &$lineup_item)
    {
        if (isset($team_roster_track[$player_position_id]) && $team_roster_track[$player_position_id]['used'] < $team_roster_track[$player_position_id]['total']) {
            $lineup_item->player_position_id = $player_position_id;
            $team_roster_track[$player_position_id]['used'] ++;
        } elseif (isset($team_roster_track[$position_array['f']]) && $team_roster_track[$position_array['f']]['used'] < $team_roster_track[$position_array['f']]['total']) {
            $lineup_item->player_position_id = $position_array['f'];
            $team_roster_track[$position_array['f']]['used'] ++;
        } elseif (isset($team_roster_track[$position_array['s']]) && $team_roster_track[$position_array['s']]['used'] < $team_roster_track[$position_array['s']]['total']) {
            $lineup_item->player_position_id = $position_array['s'];
            $team_roster_track[$position_array['s']]['used'] ++;
        } else {
            $lineup_item->player_position_id = config('pool.inverse_player_position.bn');
        }
    }

}
