<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

class PoolBoxSetting extends Model {

//    use SoftDeletes;
//    protected $dates = ['deleted_at'];
    protected $fillable = [
        'league_start_week',
        'options_per_box',
    ];
    public $timestamps = false;

    public function poolSettings() {
        return $this->morphMany('App\Models\PoolSetting', 'poolsetting');
    }

}
