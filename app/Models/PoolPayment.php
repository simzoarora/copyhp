<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolPayment extends Model {

    protected $guarded = [
        'id',
    ];

    public function pool() {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    public function poolPaymentItem() {
        return $this->belongsTo('App\Models\PoolPaymentItem');
    }

}
