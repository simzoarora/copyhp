<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConferenceStanding extends Model {

    protected $fillable = ['season_id', 'team_id', 'conference_id', 'conference_ranking'];

    public function season() {
        return $this->belongsTo('App\Models\Season');
    }

    public function team() {
        return $this->belongsTo('App\Models\Team');
    }

    public function conference() {
        return $this->belongsTo('App\Models\Conference');
    }

    public static function processAndSaveDataFromSportsDirect($stat_group, $team, $season_id, $conference_id) {
        $conference_standing = $team->conferenceStandings()->firstOrNew([
            'season_id' => $season_id,
            'conference_id' => $conference_id
        ]);
        if (isset($stat_group['stat'][0])) {
            foreach ($stat_group['stat'] as $stat) {
                if ($stat['@attributes']['type'] == 'conference_ranking') {
                    $conference_standing->conference_ranking = $stat['@attributes']['num'];
                }
            }
        } else {
            $stat = $stat_group['stat'];
            if ($stat['@attributes']['type'] == 'conference_ranking') {
                $conference_standing->conference_ranking = $stat['@attributes']['num'];
            }
        }
        $conference_standing->save();
    }

}
