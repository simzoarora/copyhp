<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;

class Team extends Model
{

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'api_id',
        'display_name',
        'first_name',
        'nick_name',
        'short_name',
        'city_id',
    ];

    public function seasons()
    {
        return $this->belongsToMany('App\Models\Season');
    }

    public function players()
    {
        return $this->belongsToMany('App\Models\Player', 'player_season');
    }

    public function playerSeasons()
    {
        return $this->hasMany('App\Models\PlayerSeason');
    }

    public function homeCompetitions()
    {
        return $this->hasMany('App\Models\Competition', 'home_team_id');
    }

    public function awayCompetitions()
    {
        return $this->hasMany('App\Models\Competition', 'away_team_id');
    }

    /**
     * used to get first data
     * @return type
     */
    public function homeCompetition()
    {
        return $this->hasOne('App\Models\Competition', 'home_team_id');
    }

    /**
     * used to get first data
     * @return type
     */
    public function awayCompetition()
    {
        return $this->hasOne('App\Models\Competition', 'away_team_id');
    }

    public function conferenceStandings()
    {
        return $this->hasMany('App\Models\ConferenceStanding');
    }

    public function divisionStandings()
    {
        return $this->hasMany('App\Models\DivisionStanding');
    }

    public function leagueStandings()
    {
        return $this->hasMany('App\Models\LeagueStanding');
    }

    public function extraLeagueStandings()
    {
        return $this->hasMany('App\Models\ExtraLeagueStanding');
    }

    public function playerStats()
    {
        return $this->hasMany('App\Models\PlayerStat');
    }

    /**
     * Team has many league standings, but just for returning 1 data, we are 
     * using this.
     * @return type
     */
    public function leagueStanding()
    {
        return $this->hasOne('App\Models\LeagueStanding');
    }

    /**
     * Function to get team id saved in our database from the string of id 
     * sportsdirect API gives
     * 
     * @param string $api_id_string
     * @return object
     * @author Anik Goel <anikgoel19@gmail.com>
     */
    public static function getTeamFromApiIdString($api_id_string)
    {
        return self::where('api_id', str_replace(config('constant.sportsdirectconstant.team_id_prefix'), "", $api_id_string))->first();
    }

    /**
     * Adding scope to model for retriving only current season data
     * @param type $query
     * @return type
     */
    public function scopeCurrent($query)
    {
        return $query->whereHas('seasons', function($query) {
                    $query->current();
                });
    }

    public static function getDataForSelectBox()
    {
        return self::orderBy('display_name')->lists('display_name', 'nick_name')->toArray();
    }
}