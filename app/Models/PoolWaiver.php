<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\FutureData;
use App\Services\Traits\SoftDeletes;

class PoolWaiver extends Model {

    use FutureData,
        SoftDeletes;

    protected $dates = ['waiver_till'];
    protected $fillable = [
        'pool_id',
        'player_season_id',
        'waiver_till'
    ];

    public function pool() {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    public function playerSeason() {
        return $this->belongsTo('App\Models\PlayerSeason');
    }

    public function poolWaiverClaims() {
        return $this->hasMany('App\Models\PoolWaiverClaim');
    }

    public function poolWaiverDeletes() {
        return $this->hasMany('App\Models\PoolWaiverDelete');
    }

    public function scopeNotCompleted($query) {
        $query->where('is_completed', 0);
    }

}
