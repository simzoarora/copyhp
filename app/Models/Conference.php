<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conference extends Model {

    protected $fillable = ['api_id', 'name', 'short_name'];

    public function divisions() {
        return $this->hasMany('App\Models\Division');
    }

    public static function sportsDirectSaveOrGetConference($conference_data) {
        $conference = self::firstOrNew([
                    'api_id' => str_replace(config('constant.sportsdirectconstant.conference_id_prefix'), '', $conference_data['id'])
        ]);
        $conference->name = $conference_data['name'][0];
        $conference->short_name = $conference_data['name'][1];
        $conference->save();
        return $conference;
    }

}
