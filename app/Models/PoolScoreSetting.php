<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

class PoolScoreSetting extends Model {

//    use SoftDeletes;
//    protected $dates = ['deleted_at'];
    protected $fillable = [
        'pool_scoring_field_id',
        'value',
        'type'
    ];

//    public $timestamps = false;

    public function pool() {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    public function poolScoringField() {
        return $this->belongsTo('App\Models\PoolScoringField');
    }

}
