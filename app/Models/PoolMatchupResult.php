<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolMatchupResult extends Model {

    protected $fillable = [
        'pool_team_id',
        'win',
        'loss',
        'tie',
        'score',
    ];

    public function poolMatchup() {
        return $this->belongsTo('App\Models\PoolMatchup');
    }

    public function poolTeam() {
        return $this->belongsTo('App\Models\PoolTeam');
    }

}
