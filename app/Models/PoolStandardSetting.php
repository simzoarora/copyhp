<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

class PoolStandardSetting extends Model {

//    use SoftDeletes;
//    protected $dates = ['deleted_at'];
    protected $fillable = [
        'max_teams',
        'max_acquisition_team',
        'max_acquisition_week',
        'max_trades_season',
        'trade_end_date',
        'trading_draft_pick',
        'trade_reject_time',
        'waiver_mode',
        'waiver_time',
        'league_start_week',
        'min_goalie_appearance',
        'pool_format',
        'trade_management',
    ];
    public $timestamps = false;
    protected $dates = ['trade_end_date'];

    public function setTradeEndDateAttribute($value) {
        $this->attributes['trade_end_date'] = date('Y-m-d',  strtotime($value));
    }

    public function poolSettings() {
        return $this->morphMany('App\Models\PoolSetting', 'poolsetting');
    }

}
