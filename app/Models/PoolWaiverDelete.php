<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;

class PoolWaiverDelete extends Model {

    use SoftDeletes;

    protected $fillable = [
        'pool_waiver_id',
        'pool_team_player_id'
    ];

    public function poolWaiver() {
        return $this->belongsTo('App\Models\PoolWaiver');
    }

    public function poolTeamPlayer() {
        return $this->hasMany('App\Models\PoolTeamPlayer');
    }

}
