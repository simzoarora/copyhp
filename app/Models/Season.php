<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Season extends Model {

    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'api_id',
        'current',
    ];
    public $timestamps = false;
    protected $dates = [
        'start_date',
        'end_date',
    ];

    public function teams() {
        return $this->belongsToMany('App\Models\Team')->withPivot('conference_id','division_id');
    }

    public function players() {
        return $this->belongsToMany('App\Models\Player')->withTimestamps()->withPivot('id','team_id');
    }

    public function competitions() {
        return $this->hasMany('App\Models\Competition');
    }

    public function pools() {
        return $this->hasMany('App\Models\Pool')->active();
    }

    public function seasonCompetitionTypes() {
        return $this->hasMany('App\Models\SeasonCompetitionType');
    }

    /**
     * Only for 1st result
     * @return type
     */
    public function seasonCompetitionType() {
        return $this->hasOne('App\Models\SeasonCompetitionType');
    }

    public function leagueStandings() {
        return $this->hasMany('App\Models\LeagueStanding');
    }

    public function divisionStandings() {
        return $this->hasMany('App\Models\DivisionStanding');
    }

    public function conferenceStandings() {
        return $this->hasMany('App\Models\ConferenceStanding');
    }

    public function extraLeagueStandings() {
        return $this->hasMany('App\Models\ExtraLeagueStanding');
    }

    public function playerStats() {
        return $this->hasMany('App\Models\PlayerStat');
    }

    public function seasonPoolPoints() {
        return $this->hasMany('App\Models\SeasonPoolPoint');
    }

    public function poolSeasonRanks() {
        return $this->hasMany('App\Models\PoolSeasonRank');
    }

    /**
     * Adding scope to model for retriving only current season data
     * @param type $query
     * @return type
     */
    public function scopeCurrent($query) {
        return $query->where('current', 1);
    }

    public static function sportsDirectSaveOrGetSeason($season_data) {
        $season = self::firstOrNew([
                    'api_id' => str_replace(config('constant.sportsdirectconstant.season_id_prefix'), '', $season_data['id'])
        ]);
        $season->name = $season_data['name'];
        $season->start_date = $season_data['details']['start-date'];
        $season->end_date = $season_data['details']['end-date'];
        $season->save();
        return $season;
    }

}
