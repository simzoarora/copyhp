<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Venue
 *
 * @author Anik
 */
class EmailNotificationSetting extends Model {

    protected $fillable = [
        'user_id',
        'type',
    ];

}
