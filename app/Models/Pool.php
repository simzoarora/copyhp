<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;
use Auth;
use Mail;
use Illuminate\Pagination\LengthAwarePaginator;
use Carbon\Carbon;
use App\Models\PoolMatchup;

class Pool extends Model
{

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'season_id',
        'season_type',
        'pool_type',
        'name',
        'logo'
    ];

    /**
     * Function used to add any default where condition to query in model
     * @param type $excludeDeleted
     * @return type
     */
//    public function newQuery($excludeDeleted = true) {
//        return parent::newQuery($excludeDeleted)
//                        ->where(function($query) {
//                            $query->where('is_active', '=', 1)
//                            ->orWhere('active_till', '>=', date('Y-m-d H:i:s'))
//                            ->orWhere('is_completed', '=', 0);
//                        });
//    }

    public function scopeActive($query)
    {
        return $query->where(function($query) {
                    $query->where('is_active', '=', 1)
                            ->orWhere('active_till', '>=', date('Y-m-d H:i:s'));
                });
    }

    public function scopeCompleted($query)
    {
        return $query->where('is_completed', 1);
    }

    public function season()
    {
        return $this->belongsTo('App\Models\Season');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User');
    }

    public function currentSeason()
    {
        return $this->belongsTo('App\Models\Season', 'season_id')->current();
    }

    public function poolSetting()
    {
        return $this->hasOne('App\Models\PoolSetting');
    }

    public function poolTeams()
    {
        return $this->hasMany('App\Models\PoolTeam');
    }

    /**
     * Used to get first result
     * @return type
     */
    public function poolTeamFirst()
    {
        return $this->hasOne('App\Models\PoolTeam');
    }

    /**
     * current user scope added from PoolTeam model
     * @return type
     */
    public function poolTeam()
    {
        return $this->hasOne('App\Models\PoolTeam')->currentUserTeam();
    }

    public function poolInvitations()
    {
        return $this->hasMany('App\Models\PoolInvitation');
    }

    public function poolRosters()
    {
        return $this->hasMany('App\Models\PoolRoster');
    }

    public function totalPoolRoster()
    {
        return $this->hasOne('App\Models\PoolRoster')->selectRaw('pool_id, sum(value) as total_roster')->groupBy('pool_id');
    }

    public function poolBoxes()
    {
        return $this->hasMany('App\Models\PoolBox');
    }

    public function poolRounds()
    {
        return $this->hasMany('App\Models\PoolRound');
    }

    public function poolScoreSettings()
    {
        return $this->hasMany('App\Models\PoolScoreSetting');
    }

    public function poolMatchups()
    {
        return $this->hasMany('App\Models\PoolMatchup');
    }

    /**
     * Used to get first result
     * @return type
     */
    public function poolMatchup()
    {
        return $this->hasOne('App\Models\PoolMatchup');
    }

    public function calculatedPoolPoints()
    {
        return $this->hasMany('App\Models\CalculatedPoolPoint');
    }

    public function seasonPoolPoints()
    {
        return $this->hasMany('App\Models\SeasonPoolPoint');
    }

    public function poolSeasonRanks()
    {
        return $this->hasMany('App\Models\PoolSeasonRank');
    }

    public function poolWaivers()
    {
        return $this->hasMany('App\Models\PoolWaiver');
    }

    public function poolWaiverPriorities()
    {
        return $this->hasMany('App\Models\PoolWaiverPriority');
    }

    public function poolTeamPlayers()
    {
        return $this->hasManyThrough('App\Models\PoolTeamPlayer', 'App\Models\PoolTeam');
    }

    public function deletedPoolTeamPlayers()
    {
        return $this->hasManyThrough('App\Models\PoolTeamPlayer', 'App\Models\PoolTeam')->onlyTrashed();
    }

    public function completedPoolTrades()
    {
        return $this->hasManyThrough('App\Models\PoolTrade', 'App\Models\PoolTeam', 'pool_id', 'parent_pool_team_id')
                        ->where('status', config('pool.pool_trades.inverse_status.accepted'))
                        ->where('is_processed', 1);
    }

    public function poolBoxPlayers()
    {
        return $this->hasManyThrough('App\Models\PoolBoxPlayer', 'App\Models\PoolBox');
    }

    public function liveDraftSetting()
    {
        return $this->hasOne('App\Models\LiveDraftSetting');
    }

    public function liveDraftPlayers()
    {
        return $this->hasMany('App\Models\LiveDraftPlayer');
    }

    /**
     * Use to get first player
     * @return type
     */
    public function liveDraftPlayer()
    {
        return $this->hasOne('App\Models\LiveDraftPlayer');
    }

    public static function getPoolDataForAddDrop($pool_id)
    {
        return Pool::active()
                        ->where('id', $pool_id)
                        ->where('pool_type', '<>', config('pool.inverse_type.box'))
                        ->has('poolTeam')
                        ->with([
                            'poolScoreSettings.poolScoringField',
                            'poolRosters.playerPosition',
                            'poolRosters.playerPosition.poolTeamLineupCount' => function($query) use($pool_id) {
                                $query->whereHas('poolTeamPlayer', function($query) use($pool_id) {
                                            $query->whereHas('poolTeam', function($query) use($pool_id) {
                                                        $query->where('pool_id', $pool_id)->where('user_id', \Auth::id());
                                                    });
                                        });
//                        ->where('start_time', '<=', date('Y-m-d'))
//                        ->where('end_time', '>=', date('Y-m-d'));
                            },
                            'poolTeam',
                            'season',
                            'poolSetting.poolsetting'
                        ])
                        ->firstOrFail();
    }

    public static function addUserToPoolTeamThroughInvite($invite_key, $user)
    {
        try {
            $pool = self::where('invite_key', $invite_key)->firstOrFail();
            $pool_team = $pool->poolTeams()->firstOrNew([
                'user_id' => $user->id
            ]);
            $pool_team->name = $pool->name . "_" . $user->id;
            if ($pool_team->save()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (ModelNotFoundException $ex) {
            return FALSE;
        }
        return TRUE;
    }

    public static function getPoolDetailsForDashboard($id, $season_competition_week_id = NULL)
    {
//        \DB::enableQueryLog();
        $pool = self::where('id', $id)
                ->has('poolTeam')
                ->with('poolSetting.poolsetting')
                ->firstOrFail();
        $query_builder = self::where('id', $id)
                ->has('poolTeam')
                ->with('poolSetting.poolsetting', 'poolScoreSettings.poolScoringField', 'poolTeam', 'poolTeam.poolTeamPlayers');

        if ($pool->pool_type == config('pool.inverse_type.h2h')) {
            $query_builder = $query_builder->with([
                'poolMatchups' => function($query) use($season_competition_week_id) {
                    if ($season_competition_week_id != NULL) {
                        $query->where('season_competition_type_week_id', $season_competition_week_id);
                    } else {
                        $query->whereHas('seasonCompetitionTypeWeek', function($query) {
                                    $query->where('start_date', '<=', date('Y-m-d'))
                                            ->where('end_date', '>=', date('Y-m-d'));
                                })->orderBy('id', 'asc');
                    }
                },
                'poolMatchups.poolTeam1',
                'poolMatchups.poolTeam1.poolMatchupResult' => function($query) {
                    self::_addDayConditionToMatchupResult($query);
                },
                'poolMatchups.poolTeam2',
                'poolMatchups.poolTeam2.poolMatchupResult' => function($query) {
                    self::_addDayConditionToMatchupResult($query);
                },
                'poolMatchups.seasonCompetitionTypeWeek',
                'poolTeams',
                'poolTeams.poolMatchupResult', // this is used to get overall data
                'poolTeams.lastWeekPoolMatchupResult',
                'poolMatchups.poolMatchupResults',
                'poolMatchups.poolH2hScores' => function($query) {
                    $query->groupBy('pool_team_id', 'pool_score_setting_id')
                            ->selectRaw("sum(original_stat) as total_stat,pool_team_id,pool_score_setting_id,pool_matchup_id");
                },
                'poolTeam.poolMatchupResult',
            ]);
        } else if ($pool->pool_type == config('pool.inverse_type.standard')) {
            if ($pool->poolSetting->poolsetting->pool_format == config('pool.inverse_format.standard_standard')) {
                $query_builder = $query_builder->with([
                    'poolTeams.poolStandardScores' => function($query) {
                        $query
                                ->groupBy('pool_team_id')
                                ->groupBy('pool_score_setting_id')
                                ->with('poolScoreSetting.poolScoringField')
                                ->selectRaw('sum(value) as score, sum(original_stat) as original_stat, pool_team_id, pool_score_setting_id');
                    }
                ]);
            } elseif ($pool->poolSetting->poolsetting->pool_format == config('pool.inverse_format.standard_rotisserie')) {
                $query_builder = $query_builder->with([
                    'poolTeams.poolStandardRottoResults' => function($query) {
                        $query->with('poolScoringField');
                    }
                ]);
            }
        } else if ($pool->pool_type == config('pool.inverse_type.box')) {
            $query_builder = $query_builder->with([
                'poolTeams.poolStandardScores' => function($query) {
                    $query
                            ->groupBy('pool_team_id')
                            ->groupBy('pool_score_setting_id')
                            ->with('poolScoreSetting.poolScoringField')
                            ->selectRaw('sum(value) as score,pool_team_id,pool_score_setting_id');
                },
            ]);
        } else {
            abort(404);
        }
        $return_data = $query_builder->firstOrFail();

        /**
         * Checking if pool matchups are there as per current date, if not we will try to load
         * either next matchups or previous matchups based on the current date
         */
        if ($pool->pool_type == config('pool.inverse_type.h2h') && $return_data->poolMatchups->isEmpty() && $season_competition_week_id == NULL) {
            $pool->load([
                'poolMatchup',
                'poolMatchup.seasonCompetitionTypeWeek',
            ]);
            if ($pool->poolMatchup != NULL) {
                if ($pool->poolMatchup->seasonCompetitionTypeWeek->start_date < date('Y-m-d')) {
                    $pool->load([
                        'poolMatchup' => function($query) {
                            $query->orderBy('id', 'desc');
                        },
                        'poolMatchup.seasonCompetitionTypeWeek',
                    ]);
                }

                $return_data->load([
                    'poolMatchups' => function($query) use($pool) {
                        $query->whereHas('seasonCompetitionTypeWeek', function($query) use($pool) {
                                    $query->where('id', '=', $pool->poolMatchup->seasonCompetitionTypeWeek->id);
                                });
                    },
                    'poolMatchups.poolTeam1',
                    'poolMatchups.poolTeam2',
                    'poolMatchups.poolMatchupResults',
                    'poolMatchups.seasonCompetitionTypeWeek',
                    'poolMatchups.poolH2hScores' => function($query) {
                        $query->groupBy('pool_team_id', 'pool_score_setting_id')
                                ->selectRaw("sum(original_stat) as total_stat,pool_team_id,pool_score_setting_id,pool_matchup_id");
                    },
                ]);
            }
        }
//        dd($return_data->toArray());
        if ($pool->poolSetting->poolsetting->pool_format == config('pool.inverse_format.h2h_cat')) {
            $return_data->poolMatchups->transform(function($matchup_item) {
                return self::getCategoryWinning($matchup_item);
            });
        }

        if ($return_data->active_till > date('Y-m-d H:i:s')) {
            $return_data->is_trial = 1;
            $return_data->trial_days = (int) floor((strtotime($return_data->active_till) - time()) / 86400);
        } else {
            $return_data->is_trial = 0;
        }



        return $return_data;
//        dump($return_data->toArray());
//        dd(\DB::getQueryLog());
    }

    /**
     * Function to calculate win, tie, or loose of a category in a matchup
     * @param type $matchup_item
     * @return type
     */
    public static function getCategoryWinning($matchup_item)
    {
        if (isset($matchup_item->poolH2hScores) && $matchup_item->poolH2hScores != null) {
            $cat_result = $matchup_item->poolH2hScores->groupBy('pool_score_setting_id');
            $cat_result->transform(function($item, $key) {
                $temp_item = new \stdClass();
                if ($item->count() == 2) {
                    if ($item[0]->total_stat > $item[1]->total_stat) {
                        $temp_item->win = $item[0]->pool_team_id;
                    } elseif ($item[0]->total_stat == $item[1]->total_stat) {
                        $temp_item->tie = true;
                    } else {
                        $temp_item->win = $item[1]->pool_team_id;
                    }
                } elseif ($item->count() == 1) {
                    $temp_item->win = $item[0]->pool_team_id;
                } else {
                    $temp_item->tie = true;
                }
                return $temp_item;
            });
            $matchup_item->cat_result = $cat_result;
        }
        return $matchup_item;
    }

    /**
     * NOTE - Total playing players would be count of all the players from here
     * @param type $id
     * @return type
     */
    public static function getDataForInAction($id)
    {
        $date_to_search_for = date('Y-m-d');
//        $date_to_search_for = '2016-10-13';
        $pool = self::active()->where('id', $id)
                ->has('poolTeam')
                ->with([
                    'poolTeams.poolTeamPlayers' => function($query)use($date_to_search_for) {
                        $query->whereHas('playerSeason', function($query)use($date_to_search_for) {
                                    $query->whereHas('team', function($query)use($date_to_search_for) {
                                                $query->whereHas('homeCompetitions', function($query)use($date_to_search_for) {
                                                            $query->where(\DB::raw('DATE(match_start_date)'), $date_to_search_for);
                                                        });
                                                $query->orWhereHas('awayCompetitions', function($query)use($date_to_search_for) {
                                                            $query->where(\DB::raw('DATE(match_start_date)'), $date_to_search_for);
                                                        });
                                            });
                                });
                    },
                    'poolTeams.poolTeamPlayers.playerSeason.team.homeCompetitions' => function($query)use($date_to_search_for) {
                        $query->where(\DB::raw('DATE(match_start_date)'), $date_to_search_for)
                        ->with('awayTeam');
                    },
                    'poolTeams.poolTeamPlayers.playerSeason.team.awayCompetitions' => function($query)use($date_to_search_for) {
                        $query->where(\DB::raw('DATE(match_start_date)'), $date_to_search_for)
                        ->with('homeTeam');
                    },
                    'poolTeams.poolTeamPlayers.playerSeason.player',
                    'poolTeams.poolTeamPlayers.playerSeason.playerPosition',
                    'poolTeams.poolTeamPlayers.totalPoolStandardScore' => function($query)use($date_to_search_for) {
                        $query->whereHas('competition', function($query)use($date_to_search_for) {
                                    $query->where(\DB::raw('DATE(match_start_date)'), $date_to_search_for);
                                });
                    },
                    'poolSetting.poolsetting'
                ])
                ->firstOrFail();
        if ($pool->pool_type = config('pool.inverse_type.h2h')) {
            $pool->load([
                'poolTeam.poolMatchupResult'
            ]);
        } else {
            $pool->load([
                'poolTeam'
            ]);
        }
        return $pool;
    }

    public static function getDataForLiveStats($id)
    {
        date_default_timezone_set(config('app.timezone_for_calculation'));
//        \DB::enableQueryLog();
        $date_to_search_for = date('Y-m-d');
        date_default_timezone_set(config('app.timezone')); // change to original timezone so that created_at can work properly
//        $date_to_search_for = '2016-10-13';
        $pool = self::active()->where('id', $id)
                ->has('poolTeam')
                ->with([
                    'poolScoreSettings.poolScoringField',
                    'poolTeams.poolTeamPlayers.playerSeason.player',
                    'poolTeams.poolTeamPlayers.playerSeason.playerPosition',
                    'poolTeams.poolTeamPlayers.poolTeamLineup' => function($query) use($date_to_search_for) {
                        $query->where(\DB::raw('DATE(start_time)'), '<=', $date_to_search_for)->where(\DB::raw('DATE(end_time)'), '>=', $date_to_search_for);
                    },
                    'poolTeams.poolTeamPlayers.poolTeamLineup.playerPosition',
//                    'poolTeams.poolTeamPlayers.playerSeason.player.calculatedPoolPoint' => function($query) use($id, $date_to_search_for) {
//                        $query->where('pool_id', $id)->where('points_date', $date_to_search_for);
//                    },
                    "poolTeams.poolTeamPlayers.playerSeason.competitionSkaterStat" => function($query)use($date_to_search_for) {
                        $query->whereHas('competition', function($query)use($date_to_search_for) {
                                    $query->where(\DB::raw('DATE(match_start_date)'), $date_to_search_for);
                                })
                        ;
                    },
                    "poolTeams.poolTeamPlayers.playerSeason.competitionGoalieStat" => function($query)use($date_to_search_for) {
                        $query->whereHas('competition', function($query)use($date_to_search_for) {
                                    $query->where(\DB::raw('DATE(match_start_date)'), $date_to_search_for);
                                })
                        ;
                    },
                    'poolSetting.poolsetting'
                ])
                ->firstOrFail();
        $pool->poolTeams->transform(function($team_item) use($pool) {
            return self::tranformPoolItem($pool, $team_item);
        });
        return $pool;
//        dump(\DB::getQueryLog());
//        dd($pool->toArray());
    }

    public static function getDataForMyTeam($id, $start_date = "2016-01-01", $end_date = "2016-12-31", $lineup_date, $pool_team_id)
    {
        $current_date = date('Y-m-d');
//        $current_date = "2016-10-12";
//        \DB::enableQueryLog();
        $pool = self::active()->where('id', $id)
                ->has('poolTeam')
                ->with([
                    'poolScoreSettings.poolScoringField',
                    'poolRosters' => function($query) {
                        // This code is done to send rosters in sorter order
                        $query->leftJoin('player_positions', function($join) {
                                    $join->on('player_positions.id', '=', 'pool_rosters.player_position_id');
                                })
                        ->orderBy('player_positions.sort_order')
                        ->select('pool_rosters.*');
                    },
                    'poolRosters.playerPosition',
                    'poolSetting.poolsetting'
                ])
                ->firstOrFail();
        if ($pool->pool_type == config('pool.inverse_type.h2h')) {
            $model = 'App\Models\PoolH2hScore';
            $relation = 'poolH2hScores';
        } else {
            $model = 'App\Models\PoolStandardScore';
            $relation = 'poolStandardScores';
        }
        $score_table = with(new $model)->getTable();
//        $pool_team_player_table = with(new App\Models\PoolTeamPlayer)->getTable();


        if ($pool_team_id != NULL) {
            $load_array = [
                'poolTeamFirst' => function($query) use($pool_team_id) {
                    $query->where('id', $pool_team_id);
                }
            ];
            $pool_team_relation_name = 'poolTeamFirst';
        } else {
            $load_array = [
                'poolTeam'
            ];
            $pool_team_relation_name = 'poolTeam';
        }

        $load_array += [
            "$pool_team_relation_name.$relation" => function($query)use($start_date, $end_date, $score_table) {
                $query
                        ->whereHas('competition', function($query)use($start_date, $end_date) {
                                    $query->where(\DB::raw('DATE(match_start_date)'), '>=', $start_date);
                                    $query->where(\DB::raw('DATE(match_start_date)'), '<=', $end_date);
                                })
                        ->groupBy('pool_team_id')
                        ->groupBy('pool_score_setting_id')
                        ->selectRaw("sum(original_stat) as total_stat,pool_team_id,pool_score_setting_id,value");
            },
            "$pool_team_relation_name.poolTeamPlayers" => function($query) use($lineup_date) {
                $query->leftJoin('player_season', function($join) {
                                    $join->on('pool_team_players.player_season_id', '=', 'player_season.id');
                                })
                        ->leftJoin('pool_team_lineups', function($join) use($lineup_date) {
                                    $join->on('pool_team_players.id', '=', 'pool_team_lineups.pool_team_player_id')
                                    ->where(\DB::raw('DATE(start_time)'), '<=', $lineup_date)->where(\DB::raw('DATE(end_time)'), '>=', $lineup_date)
                                    ->whereNull('pool_team_lineups.deleted_at');
                                })
                        ->leftJoin('player_positions', function($join) {
                                    $join->on('player_positions.id', '=', 'pool_team_lineups.player_position_id');
                                })
                        /**
                         * Below conditions are added to get only those players which
                         * were there on lineup as per lineup date
                         */
                        ->withTrashed() // including deleted players with below condition
                        ->where(function($query) use($lineup_date) {
                                    $query->where(\DB::raw('DATE(pool_team_players.deleted_at)'), '>', $lineup_date)
                                    ->orWhereNull('pool_team_players.deleted_at');
                                })
                        ->withFutureData()
                        ->where(\DB::raw('DATE(pool_team_players.created_at)'), '<=', $lineup_date)
                        /**
                         * Condition ends
                         */
                        ->orderBy('player_positions.sort_order')
                        ->orderBy('player_season.player_position_id')
                        ->select('pool_team_players.*');
            },
            "$pool_team_relation_name.poolTeamPlayers.$relation" => function($query)use($score_table, $start_date, $end_date) {
                $query
                        ->whereHas('competition', function($query)use($start_date, $end_date) {
                                    $query->where(\DB::raw('DATE(match_start_date)'), '>=', $start_date);
                                    $query->where(\DB::raw('DATE(match_start_date)'), '<=', $end_date);
                                })
                        ->groupBy('pool_team_player_id')
                        ->groupBy('pool_score_setting_id')
                        ->selectRaw("$score_table.*, sum(original_stat) as total_stat,sum(value) as total_value, pool_team_player_id,pool_score_setting_id")
                ;
            },
            "$pool_team_relation_name.poolTeamPlayers.total" . str_singular(studly_case($relation)), // generating totalPoolH2HScore OR totalPoolStandardScore
            "$pool_team_relation_name.poolTeamPlayers.$relation.poolScoreSetting.poolScoringField",
            "$pool_team_relation_name.poolTeamPlayers.playerSeason.player",
            "$pool_team_relation_name.poolTeamPlayers.playerSeason.playerPosition",
            "$pool_team_relation_name.poolTeamPlayers.poolTeamLineup" => function($query) use($lineup_date) {
                $query->where(\DB::raw('DATE(start_time)'), '<=', $lineup_date)->where(\DB::raw('DATE(end_time)'), '>=', $lineup_date);
            },
            "$pool_team_relation_name.poolTeamPlayers.poolTeamLineup.playerPosition",
            "$pool_team_relation_name.$relation.poolScoreSetting",
            "$pool_team_relation_name.poolTeamPlayers.playerSeason.team.homeCompetitions" => function($query)use ($current_date) { // for getting today's match of player
                $query->where(\DB::raw('DATE(match_start_date)'), $current_date)
                        ->with('awayTeam');
            },
            "$pool_team_relation_name.poolTeamPlayers.playerSeason.team.awayCompetitions" => function($query)use ($current_date) { // for getting today's match of player
                $query->where(\DB::raw('DATE(match_start_date)'), $current_date)
                        ->with('homeTeam');
            },
            "$pool_team_relation_name.poolTeamPlayers.playerSeason.totalPoolsAddedIn", // for calculating percentage of pools which have got this player
            "$pool_team_relation_name.poolTeamPlayers.totalPoolTeamLineup", // for calculating percentage of pools which have this player in starting lineup
            "$pool_team_relation_name.poolTeamPlayers.playerSeason.player.poolSeasonRank" => function($query) use($pool) {
                $query->where('pool_id', $pool->id)->where('season_id', $pool->season_id);
            },
            "$pool_team_relation_name.poolTeamPlayers.playerSeason.player.playerPreRanking" => function($query) use($pool) {
                $query->where('season_id', $pool->season_id);
            },
        ];

        if ($pool->pool_type == config('pool.inverse_type.h2h')) {
            $load_array = array_merge($load_array, [
                "$pool_team_relation_name.poolMatchupTeam1" => function($query)use($current_date) {
                    $query->whereHas('seasonCompetitionTypeWeek', function($query)use($current_date) {
                                $query->where('start_date', '<=', $current_date)
                                        ->where('end_date', '>=', $current_date);
                            });
                },
                "$pool_team_relation_name.poolMatchupTeam2" => function($query)use($current_date) {
                    $query->whereHas('seasonCompetitionTypeWeek', function($query)use($current_date) {
                                $query->where('start_date', '<=', $current_date)
                                        ->where('end_date', '>=', $current_date);
                            });
                },
                "$pool_team_relation_name.poolMatchupTeam1.poolTeam1",
                "$pool_team_relation_name.poolMatchupTeam1.poolTeam2",
                "$pool_team_relation_name.poolMatchupTeam1.poolMatchupResults",
                "$pool_team_relation_name.poolMatchupTeam2.poolTeam1",
                "$pool_team_relation_name.poolMatchupTeam2.poolTeam2",
                "$pool_team_relation_name.poolMatchupTeam2.poolMatchupResults",
            ]);
        }
        $pool->load($load_array);
        $total_pool = Pool::active()->where('season_id', $pool->season_id)->count();
        $rank_array = [];
        $extra_data_check = $pool->pool_type == config('pool.inverse_type.h2h') && !$pool->poolScoreSettings
                        ->whereIn('pool_scoring_field_id', [config('pool.pool_scoring_field.inverse_fields.save_percentage'), config('pool.pool_scoring_field.inverse_fields.goals_against_avg')])
                        ->isEmpty();
        $pool->$pool_team_relation_name->poolTeamPlayers->transform(function($item)use($total_pool, &$rank_array, $start_date, $end_date, $pool, $extra_data_check) {
            $item->own = round(( (isset($item->playerSeason->totalPoolsAddedIn->count)) ? ($item->playerSeason->totalPoolsAddedIn->count / $total_pool * 100) : 0), 2);
            $item->start = round((isset($item->totalPoolTeamLineup->count)) ? (($item->totalPoolTeamLineup->count / $total_pool) * 100) : 0, 2);
            if (isset($item->totalPoolH2hScore->count)) {
                $rank_array[$item->id] = $item->totalPoolH2hScore->count;
            } else {
                $rank_array[$item->id] = NULL;
            }

            // processing for GAA & SV%
            if ($extra_data_check && $item->playerSeason->player_position_id == config('pool.inverse_player_position.g')) {
                self::_processAdditionalDataForMyTeam($pool, $start_date, $end_date, $item);
            }

            return $item;
        });

        $final_data = $pool->toArray();

//        foreach ($final_data['pool_team']['pool_team_players'] as $key => $pool_team_player) {
//            dump( $pool_team_player['total_pool_h2h_score']['count']);
//            $final_data['pool_team']['pool_team_players'][$key]['own'] = round(($pool_team_player['player_season']['total_pools_added_in']['count'] / $total_pool) * 100, 2);
//            $final_data['pool_team']['pool_team_players'][$key]['start'] = round(($pool_team_player['total_pool_team_lineup']['count'] / $total_pool) * 100, 2);
//            $rank_array[$pool_team_player['id']] = $pool_team_player['total_pool_h2h_score']['count'];
//        }
        arsort($rank_array);
        $final_data['rank_array'] = array_keys($rank_array);
        return $final_data;
//        dump(\DB::getQueryLog());
//        dd($final_data);
    }

    private static function _processAdditionalDataForMyTeam($pool, $start_date, $end_date, &$item)
    {
        $player_season_id = $item->playerSeason->id;
        $goalie_stat = CompetitionGoalieStat::whereHas('competition', function($query) use($start_date, $end_date) {
                    $query->where(\DB::raw('DATE(match_start_date)'), '>=', $start_date)
                    ->where(\DB::raw('DATE(match_start_date)'), '<=', $end_date);
                })
                ->where('player_season_id', $player_season_id)
                ->selectRaw('sum(shots_against) as shots_against, sum(goals_against) as goals_against')
                ->first();

        $skater_stat = CompetitionSkaterStat::whereHas('competition', function($query) use($start_date, $end_date) {
                    $query->where(\DB::raw('DATE(match_start_date)'), '>=', $start_date)
                    ->where(\DB::raw('DATE(match_start_date)'), '<=', $end_date);
                })
                ->where('player_season_id', $player_season_id)
                ->selectRaw('sum(time_on_ice_secs) as time_on_ice_secs')
                ->first();
        $save_percentage_check = $item->poolH2hScores->search(function($search_item) {
            return $search_item->poolScoreSetting->pool_scoring_field_id == config('pool.pool_scoring_field.inverse_fields.save_percentage');
        });
        if ($save_percentage_check !== FALSE) {
            $save_percentage_data = $item->poolH2hScores->where('poolScoreSetting.pool_scoring_field_id', config('pool.pool_scoring_field.inverse_fields.save_percentage'))->first();
            $save_percentage_data->total_stat = ($goalie_stat->shots_against != 0) ? (($goalie_stat->shots_against - $goalie_stat->goals_against) / $goalie_stat->shots_against) : NULL;
            $save_percentage_data->total_value = $save_percentage_data->total_stat * $pool->poolScoreSettings->where('pool_scoring_field_id', config('pool.pool_scoring_field.inverse_fields.save_percentage'))->first()->value;
            $item->poolH2hScores->put($save_percentage_check, $save_percentage_data);
        }

        $gaa_check = $item->poolH2hScores->search(function($search_item) {
            return $search_item->poolScoreSetting->pool_scoring_field_id == config('pool.pool_scoring_field.inverse_fields.goals_against_avg');
        });
        if ($gaa_check !== FALSE) {
            $gaa_data = $item->poolH2hScores->where('poolScoreSetting.pool_scoring_field_id', config('pool.pool_scoring_field.inverse_fields.goals_against_avg'))->first();
            $gaa_data->total_stat = ($skater_stat->time_on_ice_secs != 0) ? (($goalie_stat->goals_against * 3600) / $skater_stat->time_on_ice_secs) : NULL;
            $gaa_data->total_value = $gaa_data->total_stat * $pool->poolScoreSettings->where('pool_scoring_field_id', config('pool.pool_scoring_field.inverse_fields.goals_against_avg'))->first()->value;
            $item->poolH2hScores->put($gaa_check, $gaa_data);
        }
    }

    private static function tranformPoolItem($pool, $team_item)
    {
        $team_item->team_stat = new \stdClass();
        $team_item->off_starting_team_stat = new \stdClass();
        $team_item->goalie_starting_team_stat = new \stdClass();
        $team_item->team_stat->total_points = 0;
        $team_item->off_starting_team_stat->total_points = 0;
        $team_item->goalie_starting_team_stat->total_points = 0;
        $team_item->poolTeamPlayers->transform(function($team_player_item) use($pool, &$team_item) {
            $total_points = 0;
            $lineup_check = $team_player_item->poolTeamLineup != null && $team_player_item->poolTeamLineup->player_position_id != config('pool.inverse_player_position.bn') && $team_player_item->poolTeamLineup->player_position_id != config('pool.inverse_player_position.ir');
            $team_player_item->player_stat = new \stdClass();
            $team_player_item->playerSeason->player->calculated_pool_point = new \stdClass();
            foreach ($pool->poolScoreSettings as $score_setting) {
                if (!isset($team_item->team_stat->{$score_setting['pool_scoring_field_id']})) {
                    $team_item->team_stat->{$score_setting['pool_scoring_field_id']} = 0;
                }
                if (!isset($team_player_item->player_stat->{$score_setting['pool_scoring_field_id']})) {
                    $team_player_item->player_stat->{$score_setting['pool_scoring_field_id']} = null;
                }
                if (!isset($team_item->off_starting_team_stat->{$score_setting['pool_scoring_field_id']})) {
                    $team_item->off_starting_team_stat->{$score_setting['pool_scoring_field_id']} = 0;
                    $team_item->goalie_starting_team_stat->{$score_setting['pool_scoring_field_id']} = 0;
                }
                $field_name = config('pool.pool_scoring_field.fields_map_to_stat_table')[$score_setting['pool_scoring_field_id']];
                if (isset($team_player_item->playerSeason->competitionSkaterStat->$field_name)) {
                    $value = $team_player_item->playerSeason->competitionSkaterStat->$field_name;
                } elseif (isset($team_player_item->playerSeason->competitionGoalieStat->$field_name)) {
                    $value = $team_player_item->playerSeason->competitionGoalieStat->$field_name;
                } else {
                    $value = 0;
                }
                $team_player_item->player_stat->{$score_setting['pool_scoring_field_id']} = $value;
                $team_item->team_stat->{$score_setting['pool_scoring_field_id']} += $value;
                if ($lineup_check) {
                    $total_points += $value * $score_setting->value;
                    if ($team_player_item->playerSeason->player_position_id == config('pool.inverse_player_position.g')) {
                        $team_item->goalie_starting_team_stat->{$score_setting['pool_scoring_field_id']} += $value;
                    } else {
                        $team_item->off_starting_team_stat->{$score_setting['pool_scoring_field_id']} += $value;
                    }
                }
            }
            if ($lineup_check) {
                if ($team_player_item->playerSeason->player_position_id == config('pool.inverse_player_position.g')) {
                    $team_item->goalie_starting_team_stat->total_points += $total_points;
                } else {
                    $team_item->off_starting_team_stat->total_points += $total_points;
                }
                $team_item->team_stat->total_points += $total_points;
                $team_player_item->playerSeason->player->calculated_pool_point->total_points = $total_points;
            } else {
                $team_player_item->playerSeason->player->calculated_pool_point->total_points = 0;
            }
            return $team_player_item;
        });
        return $team_item;
    }

    public static function getPoolDataForLeagueLeaders($id, $pool_scoring_field_id)
    {
        return self::active()->where('id', $id)
                        ->has('poolTeam')
                        ->with([
                            'poolScoreSettings' => function($query)use($pool_scoring_field_id) {
//                                if ($pool_scoring_field_id > 0) {
//                                    $query->where('pool_scoring_field_id', $pool_scoring_field_id);
//                                }
                            },
                            'poolScoreSettings.poolScoringField',
                            'poolTeams',
                            'poolTeam.poolMatchupResult',
                            'poolSetting.poolsetting'
                        ])
                        ->firstOrFail();
    }

    /**
     * This function is used to get data of pool header as well as for getting 
     * specific pool data of user pools
     * @param type $pool_id
     * @param type $pool_team_id
     * @return type
     */
    public static function getDataForHeader($pool_id, $pool_team_id = NULL)
    {
        $today_date = date('Y-m-d');
//        $today_date = '2017-10-13';

        $pool = Pool::has('poolTeam')->with('poolScoreSettings.poolScoringField', 'poolSetting.poolsetting')->where('id', $pool_id)->firstOrFail();


        $today = Carbon::now();
        $compare_array = [
            $today->startOfWeek()->toDateString(),
            $today->endOfWeek()->toDateString(),
        ];
        $goalie_competitions = Competition::whereBetween(\DB::raw('DATE(match_start_date)'), $compare_array)
                        ->where('competition_type', $pool->season_type)
                        ->whereHas('competitionGoalieStats.playerSeason.poolTeamPlayers', function($query) use($pool_id) {
                            $query->whereHas('poolTeam', function($query) use($pool_id) {
                                $query->where('pool_id', $pool_id)->where('user_id', \Auth::id());
                            })
                            ->withTrashed()
                            ->whereHas('poolTeamLineup', function($query) {
                                $query->whereRaw('pool_team_lineups.start_time <= DATE(competitions.match_start_date)')
                                ->whereRaw('pool_team_lineups.end_time >= DATE(competitions.match_start_date)')
                                ->where('pool_team_lineups.player_position_id', '<>', config('pool.inverse_player_position.bn'))
                                ->withTrashed();
                            });
                        })->count();

        if ($pool_team_id == NULL) {
            $pool->load([
                'poolTeam',
                'poolTeam.poolTeamPlayers' => function($query) { // mainly used in box pool to check if player already drafted players
                    $query->select('id', 'pool_team_id')->limit(1);
                }
            ]);
            $pool_team_id = $pool->poolTeam->id;
        }

        self::_loadAdditionalDataForPool($pool, $pool_team_id, $today_date);
        $pool->goalie_games = $goalie_competitions;
        return $pool;
    }

    private static function _loadMatchup(&$pool, $pool_team_id, $today_date = NULL, $order = 'asc')
    {
        $pool->load([
            'poolMatchup' => function($query) use($pool_team_id, $today_date, $order) {
                $query->where(function($query) use($pool_team_id, $order) {
                            $query->where('team1', '=', $pool_team_id)
                                    ->orWhere('team2', '=', $pool_team_id);
                        })->orderBy('id', $order);
                if ($today_date != NULL) {
                    $query->whereHas('seasonCompetitionTypeWeek', function($query) use($today_date) {
                                $query->where('start_date', '<=', $today_date)->where('end_date', '>=', $today_date);
                            });
                }
            },
            'poolMatchup.seasonCompetitionTypeWeek',
            'poolMatchup.poolH2hScores' => function($query) {
                $query->groupBy('pool_team_id', 'pool_score_setting_id')
                        ->selectRaw("sum(original_stat) as total_stat,sum(value) as total_value,pool_team_id,pool_score_setting_id,pool_matchup_id");
            },
            'poolMatchup.poolH2hScores.poolScoreSetting',
            'poolTeams',
        ]);
        if ($pool->poolMatchup == NULL) {
            $pool->load([
                'poolMatchup' => function($query) use($pool_team_id) {
                    $query->where(function($query) use($pool_team_id) {
                                $query->where('team1', '=', $pool_team_id)
                                        ->orWhere('team2', '=', $pool_team_id);
                            });
                },
                'poolMatchup.seasonCompetitionTypeWeek',
            ]);
            if ($pool->poolMatchup != NULL) {
                if ($pool->poolMatchup->seasonCompetitionTypeWeek->start_date < $today_date) {
                    self::_loadMatchup($pool, $pool_team_id, NULL, 'desc');
                } else {
                    self::_loadMatchup($pool, $pool_team_id, NULL, 'asc');
                }
            }
        } else {
            $pool->poolMatchup->load([
                'poolTeam1.poolMatchupResult' => function($query) {
                    self::_addDayConditionToMatchupResult($query);
                },
                'poolTeam2.poolMatchupResult' => function($query) {
                    self::_addDayConditionToMatchupResult($query);
                },
                'poolMatchupResults'
            ]);
        }
    }

    public static function _addDayConditionToMatchupResult(&$query)
    {
        $query->whereHas('poolMatchup', function($query) {
            $query->whereHas('seasonCompetitionTypeWeek', function($query) {
                $query->where('end_date', '<', date('Y-m-d'));
            });
        });
    }

    private static function _loadAdditionalDataForPool(&$pool, $pool_team_id, $today_date)
    {
        if ($pool->pool_type == config('pool.inverse_type.h2h')) {
            self::_loadMatchup($pool, $pool_team_id, $today_date);
            $pool->pool_matchup = Pool::getCategoryWinning($pool->poolMatchup);
        } elseif ($pool->pool_type == config('pool.inverse_type.standard')) {
            if ($pool->poolSetting->poolsetting->pool_format == config('pool.inverse_format.standard_rotisserie')) {
                $pool->load([
                    'poolTeamFirst' => function($query) use($pool_team_id) {
                        $query->where('id', $pool_team_id);
                    },
                    'poolTeamFirst.poolStandardRottoResults',
                    'poolTeamFirst.standardRottoScore',
                    'poolTeams.standardRottoScore',
                ]);
                $rank_key = $pool->poolTeams->sortByDesc('standardRottoScore.total_score')->values()->pluck('id')->search($pool_team_id);
                $pool->poolTeamFirst->rank = ($rank_key ? : 0) + 1;
            } else {
                self::_loadDataForBoxAndStandard($pool, $pool_team_id);
            }
        } elseif ($pool->pool_type == config('pool.inverse_type.box')) {
            self::_loadDataForBoxAndStandard($pool, $pool_team_id);
        }
    }

    public static function _loadDataForBoxAndStandard(&$pool, $pool_team_id)
    {
        $pool->load([
            'poolTeamFirst' => function($query) use($pool_team_id) {
                $query->where('id', $pool_team_id);
            },
            'poolTeamFirst.poolStandardScores' => function($query) {
                $query->selectRaw('sum(value) as total_score, pool_team_id, pool_score_setting_id')->groupBy('pool_score_setting_id');
            },
            'poolTeamFirst.poolStandardScores.poolScoreSetting',
            'poolTeamFirst.standardScore',
            'poolTeams.standardScore',
        ]);
        $rank_key = $pool->poolTeams->sortByDesc('standardScore.total_score')->values()->pluck('id')->search($pool_team_id);
        $pool->poolTeamFirst->rank = ($rank_key ? : 0) + 1;
    }

    public static function getDataForUserPools($page = NULL, $only_completed_pools = FALSE, $past_pools = FALSE)
    {
        if ($page == NULL) {
            $page = 1;
        }
        $today_date = date('Y-m-d');
//        $today_date = '2016-10-13';
        $pools_query_builder = Pool::
                where(function($query) {
                    $query->has('poolTeam')
                    ->orWhere('user_id', \Auth::id());
                })
                ->with([
                    'poolTeam',
                    'user' => function($query) {
                        $query->where('id', \Auth::id())->select('id');
                    },
                    'poolScoreSettings.poolScoringField',
                    'poolSetting.poolsetting',
                    'liveDraftSetting'
//                ])->paginate(config('pool.paginations.user_pools.per_page_limit'));
                ])
                ->orderBy('id', 'desc');
        if ($past_pools === FALSE) {
            $pools_query_builder->has('currentSeason');
        } else {
            $pools_query_builder->whereNotExists(function($subquery) {
                /**
                 *  getting pool which is of past seasons
                 */
                $subquery->select(\DB::raw(1))
                        ->from('seasons')
                        ->whereRaw('pools.season_id=seasons.id')
                        ->where('current', 1);
            });
        }

        if ($only_completed_pools) {
            $pools_query_builder->where('is_completed', 1);
        }

        $pools = $pools_query_builder->get();
        $final_array = [];
        foreach ($pools as $pool_item) {
            if ($pool_item->user != NULL) {
                $pool_item->load([
                    'poolBoxPlayers',
                    'poolTeamPlayers'
                ]);
            } elseif ($pool_item->pool_type == config('pool.inverse_type.box')) {
                $pool_item->load([
                    'poolTeamPlayers'
                ]);
            }
            if ($pool_item->user != NULL || ($pool_item->user == NULL && $pool_item->is_completed == 1)) {
                if ($pool_item->poolTeam != NULL) {
                    self::_loadAdditionalDataForPool($pool_item, $pool_item->poolTeam->id, $today_date);
                }
                $pool_item->load([
                    'liveDraftPlayer' => function($query) {
                        $query->whereNull('player_season_id');
                    }
                ]);
                $final_array[] = $pool_item;
            }
        }
        $per_page = config('pool.paginations.user_pools.per_page_limit');
        $data = new LengthAwarePaginator(array_slice($final_array, $per_page * ($page - 1), $per_page), count($final_array), config('pool.paginations.user_pools.per_page_limit'));
        return $data;
    }

    public static function createTransactionData($pool)
    {
        $merged_collection = collect([]);

        foreach ($pool->deletedPoolTeamPlayers as $item) {
            $item->sort_date = $item->deleted_at;
            $item->type = config('pool.transactions.inverse_type.deleted');
            $item->detail_string = self::_getBasicStringForTransaction($item);
            $merged_collection->push($item);
        }
        foreach ($pool->poolTeamPlayers as $item) {
            $item->sort_date = $item->created_at->toDateTimeString();
            $item->type = config('pool.transactions.inverse_type.added');
            $item->detail_string = self::_getBasicStringForTransaction($item);
            $merged_collection->push($item);
        }
        foreach ($pool->completedPoolTrades as $item) {
            $item->sort_date = $item->status_change_at;
            $item->type = config('pool.transactions.inverse_type.traded');

            $detail_string_array = [];
            foreach ($item->poolTradePlayers as $trade_player) {
                $detail_string_array[] = self::_getBasicStringForTransaction($trade_player->poolTeamPlayer) . (($trade_player->poolTeamPlayer->poolTeam->id != $item->parentPoolTeam->id) ? ' to ' . $trade_player->poolTeamPlayer->poolTeam->name : '');
            }
            $item->detail_string = implode('; ', $detail_string_array);
            $merged_collection->push($item);
        }
        return $merged_collection;
    }

    private static function _getBasicStringForTransaction($item)
    {
        return $item->playerSeason->player->full_name . ' - ' . $item->playerSeason->team->short_name . ' (' . $item->playerSeason->playerPosition->short_name . ')';
    }

    public static function checkPoolActive($pool_id)
    {
        return self::where('id', $pool_id)->active()->exists();
    }

    public static function getDataOfRoundsForLiveDraft($pool_id, $current_user_check = TRUE)
    {
        $query_builder = self::active()
                ->where('id', $pool_id)
                ->with([
            'poolTeams' => function($query) {
                $query->select('id', 'pool_id', 'name', 'sort_order')->orderBy('sort_order');
            },
            'poolTeams.poolTeamPlayers' => function($query) {
                $query->select('id', 'pool_team_id', 'pool_round_id', 'player_season_id');
            },
            'poolTeams.poolTeamPlayers.playerSeason' => function($query) {
                $query->select('id', 'player_id', 'team_id', 'player_position_id');
            },
            'poolTeams.poolTeamPlayers.playerSeason.player' => function($query) {
                $query->select('id', 'first_name', 'last_name');
            },
            'poolTeams.poolTeamPlayers.playerSeason.team' => function($query) {
                $query->select('id', 'short_name');
            },
            'poolTeams.poolTeamPlayers.playerSeason.playerPosition' => function($query) {
                $query->select('id', 'short_name');
            },
            'poolRounds' => function($query) {
                $query->select('id', 'pool_id');
            }
        ]);
        if ($current_user_check) {
            $query_builder->has('poolTeam');
        }
        return $query_builder->firstOrFail();
    }

    public static function getDataForLiveDrafting($pool_id)
    {
        return self::where('id', $pool_id)
                        ->active()
                        ->completed()
                        ->with([
                            'season',
                            'poolRosters',
                            'poolTeam.poolTeamLineups' => function($query) {
                                $query->where('start_time', '<=', date('Y-m-d'))
                                ->where('end_time', '>=', date('Y-m-d'))
                                ->selectRaw('pool_team_player_id, player_position_id, count(*) as total_filled')
                                ->groupBy('player_position_id');
                            }
                        ])->firstOrFail();
    }

    public function finalizePool()
    {
        $this->poolTeams->each(function($pool_team) {
            if ($pool_team->email != NULL) {
                Mail::send('emails.pool_invitation', ['pool_name' => $this->name], function ($mail) use ($pool_team) {
                    $mail->from(\Config('mail.from.address'), \Config('mail.from.name'));
                    $mail->to($pool_team->email)->subject(trans('emails.subjects.pool_team_invite'));
                });
            } else {
                Mail::send('emails.pool_added', ['pool_name' => $this->name], function ($mail) use ($pool_team) {
                    $mail->from(\Config('mail.from.address'), \Config('mail.from.name'));
                    $mail->to($pool_team->user->email)->subject(trans('emails.subjects.pool_team_add'));
                });
            }
        });
//        $this->active_till = date('Y-m-d H:i:s', strtotime('+7 days')); // giving 7 days trial period. commented temporarily to make pools automatically active
        $this->active_till = null; // added temporarily to make pools automatically active
        $this->is_active = 1; // added temporarily to make pools automatically active
        $this->is_completed = 1; // marking the pool creation to be completed
        $this->save();
        PoolMatchup::createMatchups($this->id);
    }

    public static function getUserPoolsList()
    {
        return self::active()->has('poolTeam')->lists('name', 'id');
    }
}