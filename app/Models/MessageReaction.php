<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;

class MessageReaction extends Model {

    use SoftDeletes;

    protected $fillable = [
        'title',
        'description',
        'user_id'
    ];
    protected $dates = [
        'deleted_at',
    ];
    
    protected $hidden = ['reactionfor_type', 'reactionfor_id'];

    public function user() {
        return $this->belongsTo('App\Models\Access\User\User');
    }

    /**
     * Get all of the owning likeable models.
     */
    public function reactionfor() {
        return $this->morphTo();
    }

}
