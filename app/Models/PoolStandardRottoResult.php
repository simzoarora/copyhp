<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolStandardRottoResult extends Model {

    protected $fillable = [
        'pool_team_id',
        'pool_scoring_field_id',
        'value',
    ];

    public function poolScoringField() {
        return $this->belongsTo('App\Models\PoolScoringField');
    }

    public function poolTeam() {
        return $this->belongsTo('App\Models\PoolTeam');
    }

}
