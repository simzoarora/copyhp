<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\MessageNotification;

class Notification extends Model {

    protected $fillable = ['title', 'message', 'link', 'pool_id'];

    public function users() {
        return $this->belongsToMany('App\Models\Access\User\User');
    }

    public function pool() {
        return $this->belongsTo('App\Models\Pool');
    }

    public static function createAndBroadcast($title, $message, $link, array $users, $pool_id = NULL) {
        //skip notification if all team has not registered yet
        if(in_array(null, $users)) {
            return;
        }
        $notification = self::create([
                    'title' => $title,
                    'message' => $message,
                    'link' => $link,
                    'pool_id' => $pool_id,
        ]);
        $notification->load(['pool']);
        $notification->syncUsers($users);
    }

    public function syncUsers($ids, $detaching = true) {
        $result = $this->users()->sync($ids, $detaching);
        $attached = $result['attached'];
        if ($attached) {
            foreach ($attached as $user_id) {
                event(new MessageNotification($this, $user_id));
            }
        }
    }

}
