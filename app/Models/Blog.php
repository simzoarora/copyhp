<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model {

    public $timestamps = true;
    protected $fillable = [
        'excerpt',
        'title',
        'slug',
        'body',
        'featured_image',
        'thumbnail_image',
        'status'
    ];

    public function blogComments() {
        return $this->hasMany('App\Models\BlogComment')->orderBy('id', 'desc');
    }

    /**
     * Fetch all blog posts in descending order
     */
    public static function getAllBlogPosts() {
        return Blog::orderBy('id', 'desc')
                        ->get();
    }

    /**
     * Saves new blog post
     */
    public static function saveNewBlogPost($request) {
        $new_blog_post = new Blog;
        $new_blog_post->title = $request['title'];
        $new_blog_post->slug = $request['slug'];
        $new_blog_post->body = $request['body'];
        $new_blog_post->thumbnail_image = $request['thumbnail_image'];
        $new_blog_post->featured_image = $request['featured_image'];
        $new_blog_post->status = (isset($request['status'])) ? \Config::get('constant.blog.status.published') : \Config::get('constant.blog.status.draft');
        return $new_blog_post->save();
    }

    /**
     * Gets top 3 published blog posts : FRONTEND
     */
    public static function getBlogsPostsForHomepage() {
        return Blog::where('status', \Config::get('constant.blog.status.published'))
                        ->orderBy('id', 'desc')
                        ->limit(\Config::get('constant.blog.homepage_blog_limit'))
                        ->get();
    }

    /**
     * Get all published blogs : FRONTEND
     */
    public static function getAllPublishedBlogs() {
        return Blog::where('status', \Config::get('constant.blog.status.published'))
                        ->orderBy('id', 'desc')
                        ->get();
    }

    /**
     * Get all info for a blog : FRONTEND
     */
    public static function getPublishedBlogInfo($slug) {
        return self::where('slug', $slug)
                ->where('status', \Config::get('constant.blog.status.published'))
                ->with('blogComments')
//                ->select('blogComments.user_id,blogComments.comment,blogComments.created_at,blogComments.blog_id') //will desc content later
                ->with('blogComments.user')
                ->first();
    }

    /**
     * Checks if the slug entered by user is available
     */
    public static function checkSlug($slug) {
        return Blog::where('slug', $slug)
                        ->count();
    }

}
