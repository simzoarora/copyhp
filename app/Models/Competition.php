<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

class Competition extends Model
{
//    use SoftDeletes;
    protected $dates = ['match_start_date'];
    protected $appends = ['formatted_match_date'];
    protected $fillable = [
        'api_id',
        'match_start_date',
        'match_status',
        'match_complition_type',
        'home_team_id',
        'home_team_score',
        'away_team_id',
        'away_team_score',
        'type',
    ];

    public function homeTeam()
    {
        return $this->belongsTo('App\Models\Team', 'home_team_id');
    }

    public function awayTeam()
    {
        return $this->belongsTo('App\Models\Team', 'away_team_id');
    }

    public function competitionTeamStats()
    {
        return $this->hasMany('App\Models\CompetitionTeamStat');
    }

    public function competitionGoalieStats()
    {
        return $this->hasMany('App\Models\CompetitionGoalieStat');
    }

    public function competitionSkaterStats()
    {
        return $this->hasMany('App\Models\CompetitionSkaterStat');
    }

    public function competitionGoals()
    {
        return $this->hasMany('App\Models\CompetitionGoal');
    }

    public function competitionPenalties()
    {
        return $this->hasMany('App\Models\CompetitionPenalty');
    }

    public function competitionShootouts()
    {
        return $this->hasMany('App\Models\CompetitionShootout')->orderBy('order');
    }

    public function season()
    {
        return $this->belongsTo('App\Models\Season');
    }

    public function currentSeason()
    {
        return $this->belongsTo('App\Models\Season', 'season_id')->current();
    }

    /**
     * accessor for match start date
     */
    public function getFormattedMatchDateAttribute()
    {
        return date("l M d, Y - g:i A", strtotime($this->match_start_date));
    }

    public static function getCompetitionFullStats($id)
    {
        $competition = self::where('id', $id)->with(array_merge(self::_getCompetitionGoalsDataArray(), self::_getCompetitionTeamStatsDataArray(), self::_getCompetitionSkaterStatsDataArray(), self::_getCompetitionGoalieStatsDataArray(), self::_getCompetitionPenaltyStatsDataArray(), self::_getCompetitionShootoutStatsDataArray()))->firstOrFail();
        $competition->load(array_merge(self::_getCompetitionHomeTeamDataArray($competition->season_id), self::_getCompetitionAwayTeamDataArray($competition->season_id)));
        $competition_goals = $competition->competitionGoals->sortBy(function ($goal) {
           return date('H:i:s',  strtotime(str_replace(['PT','M','S'], ['',':',''], $goal->goal_time))); 
        })->values();
        $return_data = $competition->toArray();
        $return_data['competition_goals'] = $competition_goals;
        return $return_data;
    }

    public static function getCompetitionDayScore($date)
    {
        $competitions = Competition::where(\DB::raw('DATE(match_start_date)'), $date)->with(array_merge(Competition::_getCompetitionGoalsDataArray(), Competition::_getCompetitionShootoutStatsDataArray()))->get();
        foreach ($competitions as $competition) {
            $competition->load(array_merge(self::_getCompetitionHomeTeamDataArray($competition->season_id), self::_getCompetitionAwayTeamDataArray($competition->season_id)));
        }
        return $competitions;
    }

    public static function _getCompetitionGoalsDataArray()
    {
        return [
            'competitionGoals.scorer' => function($query) {
                $query->select('id', 'player_id', 'team_id', 'number');
            },
            'competitionGoals.scorer.player' => function($query) {
                $query->select('id', 'first_name', 'last_name');
            },
            'competitionGoals.scorer.player.playerStat' => function($query) {
                $query->whereHas('season', function($query) {
                                    $query->current();
                                })
                        ->where('stat_key', '!=', 'pre-season-stats')
                        ->groupBy('player_id')
                        ->selectRaw('id, player_id, sum(goals) as goals');
            },
            'competitionGoals.firstAssistance' => function($query) {
                $query->select('id', 'player_id', 'team_id', 'number');
            },
            'competitionGoals.firstAssistance.player' => function($query) {
                $query->select('id', 'first_name', 'last_name');
            },
            'competitionGoals.firstAssistance.player.playerStat' => function($query) {
                $query->whereHas('season', function($query) {
                                    $query->current();
                                })
                        ->where('stat_key', '!=', 'pre-season-stats')
                        ->groupBy('player_id')
                        ->selectRaw('id, player_id, sum(assists) as assists');
            },
            'competitionGoals.secondAssistance' => function($query) {
                $query->select('id', 'player_id', 'team_id', 'number');
            },
            'competitionGoals.secondAssistance.player' => function($query) {
                $query->select('id', 'first_name', 'last_name');
            },
            'competitionGoals.secondAssistance.player.playerStat' => function($query) {
                $query->whereHas('season', function($query) {
                                    $query->current();
                                })
                        ->where('stat_key', '!=', 'pre-season-stats')
                        ->groupBy('player_id')
                        ->selectRaw('id, player_id, sum(assists) as assists');
            },
        ];
    }

    public static function _getCompetitionTeamStatsDataArray()
    {
        return [
            'competitionTeamStats' => function($query) {
                $query->select('id', 'competition_id', 'team_id', 'shots', 'faceoff_total_losses', 'faceoff_total_wins', 'power_plays', 'goals_power_play', 'penalty_minutes', 'player_hits', 'player_blocked_shots');
            }
        ];
    }

    public static function _getCompetitionSkaterStatsDataArray()
    {
        return [
            'competitionSkaterStats' => function($query) {
                $query->select('id', 'competition_id', 'player_season_id', 'goals', 'assists', 'shots', 'hits', 'blocked_shots', 'faceoffs_won', 'faceoffs_lost', 'time_on_ice_secs', 'time_on_ice_power_play_secs', 'time_on_ice_short_handed_secs', 'plus_minus', 'penalty_minutes');
            },
            'competitionSkaterStats.playerSeason' => function($query) {
                $query->select('id', 'player_id', 'team_id', 'number')->activeCurrent();
            },
            'competitionSkaterStats.playerSeason.player' => function($query) {
                $query->select('id', 'first_name', 'last_name');
            },
        ];
    }

    public static function _getCompetitionGoalieStatsDataArray()
    {
        return [
            'competitionGoalieStats' => function($query) {
                $query->select('id', 'competition_id', 'player_season_id', 'goals_against', 'shots_against', 'wins', 'goals_against_average');
            },
            'competitionGoalieStats.playerSeason' => function($query) {
                $query->select('id', 'player_id', 'team_id', 'number')->activeCurrent();
            },
            'competitionGoalieStats.playerSeason.player' => function($query) {
                $query->select('id', 'first_name', 'last_name');
            },
        ];
    }

    public static function _getCompetitionPenaltyStatsDataArray()
    {
        return [
            'competitionPenalties.playerSeason' => function($query) {
                $query->select('id', 'player_id', 'team_id', 'number')->activeCurrent();
            },
            'competitionPenalties.playerSeason.player' => function($query) {
                $query->select('id', 'first_name', 'last_name');
            }
        ];
    }

    public static function _getCompetitionShootoutStatsDataArray()
    {
        return [
            'competitionShootouts.skaterSeason' => function($query) {
                $query->select('id', 'player_id', 'team_id', 'number');
            },
            'competitionShootouts.skaterSeason.player' => function($query) {
                $query->select('id', 'first_name', 'last_name');
            },
            'competitionShootouts.goalieSeason' => function($query) {
                $query->select('id', 'player_id', 'team_id', 'number')->activeCurrent();
            },
            'competitionShootouts.goalieSeason.player' => function($query) {
                $query->select('id', 'first_name', 'last_name');
            }
        ];
    }

    public static function _getCompetitionHomeTeamDataArray($season_id = NULL)
    {
        return [
            'homeTeam',
            'homeTeam.leagueStanding' => function($query)use($season_id ) {
                if ($season_id != NULL) {
                    $query->where('season_id', $season_id);
                }
            },
        ];
    }

    public static function _getCompetitionAwayTeamDataArray($season_id = NULL)
    {
        return [
            'awayTeam',
            'awayTeam.leagueStanding' => function($query)use($season_id ) {
                if ($season_id != NULL) {
                    $query->where('season_id', $season_id);
                }
            },
        ];
    }
}