<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;

class Player extends Model
{

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $appends = ['full_name'];
    protected $fillable = [
        'api_id',
        'first_name',
        'last_name',
        'height',
        'weight',
        'dob',
        'city_id',
    ];

    /**
     * accessor for full name
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function playerPreRankings()
    {
        return $this->hasMany('App\Models\PlayerPreRanking');
    }

    /**
     * Used to get first result
     * @return type
     */
    public function playerPreRanking()
    {
        return $this->hasOne('App\Models\PlayerPreRanking');
    }

    public function seasons()
    {
        return $this->belongsToMany('App\Models\Season')->withPivot('team_id');
    }

    public function playerSeasons()
    {
        return $this->hasMany('App\Models\PlayerSeason');
    }

    public function playerStats()
    {
        return $this->hasMany('App\Models\PlayerStat');
    }

    public function calculatedPoolPoints()
    {
        return $this->hasMany('App\Models\CalculatedPoolPoint');
    }

    public function seasonPoolPoints()
    {
        return $this->hasMany('App\Models\SeasonPoolPoint');
    }

    public function poolSeasonRanks()
    {
        return $this->hasMany('App\Models\PoolSeasonRank');
    }

    /**
     * Used deliberately for finding first result
     * @return type
     */
    public function seasonPoolPoint()
    {
        return $this->hasOne('App\Models\SeasonPoolPoint');
    }

    /**
     * Used deliberately for finding first result
     * @return type
     */
    public function calculatedPoolPoint()
    {
        return $this->hasOne('App\Models\CalculatedPoolPoint');
    }

    /**
     * Used deliberately for finding first result
     * @return type
     */
    public function poolSeasonRank()
    {
        return $this->hasOne('App\Models\PoolSeasonRank');
    }

    /**
     * This is used deliberately only where we want first result
     * @return type
     */
    public function playerStat()
    {
        return $this->hasOne('App\Models\PlayerStat');
    }

    /**
     * This function is used only for the season and team
     * @return type
     */
    public function playerSeason()
    {
        return $this->hasOne('App\Models\PlayerSeason');
    }

    /**
     * This function is made after we realized there is not much use of is_active
     * @return type
     */
    public function onlyCurrentPlayerSeason()
    {
        return $this->hasOne('App\Models\PlayerSeason')->current();
    }

    /**
     * This function is used only for the season and team in which player is active
     * and season is current
     * @return type
     */
    public function currentPlayerSeason()
    {
        return $this->hasOne('App\Models\PlayerSeason')->activeCurrent();
    }

    public function teams()
    {
        return $this->belongsToMany('App\Models\Team', 'player_season');
    }

    public function currentTeam()
    {
        return $this->belongsToMany('App\Models\Team', 'player_season')->current();
    }

    /**
     * Function to get player id saved in our database from the string of id 
     * sportsdirect API gives
     * 
     * @param string $api_id_string
     * @return object
     * @author Anik Goel <anikgoel19@gmail.com>
     */
    public static function getPlayerFromApiIdString($api_id_string)
    {
        return self::where('api_id', str_replace(config('constant.sportsdirectconstant.player_id_prefix'), "", $api_id_string))->first();
    }

    public static function getQueryBuilderForNhlPlayerStat($request)
    {
        $query_builder = Player::with([
                    'playerStat' => function($query) use($request) {
                        $query->where('season_id', $request->season_id)->where('stat_key', config('pool.stat_keys.' . $request->game_type));
                    },
                    'playerSeason' => function($query) use($request) {
                        $query->where('player_season.season_id', '=', $request->season_id);
                    },
                    'playerSeason.team',
                    'playerSeason.playerPosition',
                ])
                ->join('player_stats', function($join) use($request) {
                    $join->on('players.id', '=', 'player_stats.player_id')
                    ->where('season_id', '=', $request->season_id)->where('stat_key', '=', config('pool.stat_keys.' . $request->game_type));
                })
                ->join('player_season', function($join) use($request) {
                    $join->on('players.id', '=', 'player_season.player_id')
                    ->where('player_season.season_id', '=', $request->season_id)
                    ->whereNull('player_season.deleted_at');
                })
                ->groupBy('players.id')
                ->select('players.*', \DB::raw('(IFNULL(`player_stats`.`goals`,0) + IFNULL(`player_stats`.`assists`,0)) as points'));

        $player_position_grouping = config('pool.player_position_grouping');
        if (isset($player_position_grouping[$request->position])) {
            $query_builder->whereIn('player_season.player_position_id', $player_position_grouping[$request->position]);
        } elseif ($request->position == config('pool.inverse_player_position.ir')) {
            // for searching Injured player
            $query_builder->has('injuredPlayer');
        } else {
            $query_builder->where('player_season.player_position_id', $request->position);
        }


        if (isset($request->team)) {
            $query_builder = $query_builder->where('player_season.team_id', '=', $request->team);
        }
        self::addOrderByForPlayerStats($request, $query_builder);
        return $query_builder;
    }

    private static function addOrderByForPlayerStats($request, &$query_builder, $stat_order = TRUE)
    {
        if (isset($request->order)) {
            switch ($request->order) {
                case 'team':
                    $query_builder = $query_builder->join('teams', 'player_season.team_id', '=', 'teams.id')
                            ->orderBy('teams.display_name', $request->order_type);
                    break;
                case 'position':
                    $query_builder = $query_builder->join('player_positions', 'player_season.player_position_id', '=', 'player_positions.id')
                            ->orderBy('player_positions.short_name', $request->order_type);
                    break;
                case 'player':
                    $query_builder = $query_builder->orderBy('players.first_name', $request->order_type)->orderBy('players.last_name', $request->order_type);
                    break;
                case 'stat':
                    if ($stat_order) {
                        switch ($request->stat_order_key) {
                            case 'points':
                                $query_builder = $query_builder->orderBy('points', $request->order_type)->orderBy('player_stats.goals', $request->order_type)->orderBy('player_stats.assists', $request->order_type);
                                break;
                            default :
                                $query_builder = $query_builder->orderBy('player_stats.' . $request->stat_order_key, $request->order_type);
                                break;
                        }
                    }
                    break;
            }
        } else {
            if ($request->position == config('pool.inverse_player_position.g')) {
                $query_builder = $query_builder->orderBy('player_stats.wins', $request->order_type);
            } else {
                $query_builder = $query_builder->orderBy('points', $request->order_type)->orderBy('player_stats.goals', $request->order_type)->orderBy('player_stats.assists', $request->order_type);
            }
        }
    }

    public static function getplayerForBoxScore($api_player_id, $api_team_id)
    {
        return self::where('api_id', str_replace(config('constant.sportsdirectconstant.player_id_prefix'), "", $api_player_id))
                        ->with([
                            'onlyCurrentPlayerSeason' => function($query) use($api_team_id) {
                                $query->whereHas('team', function($query) use($api_team_id) {
                                            $query->where('api_id', $api_team_id);
                                        });
                            }
                        ])
                        ->firstOrFail();
    }
}