<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolBox extends Model {

    protected $fillable = [
        'name',
    ];

    public function pool() {
        return $this->belongsTo('App\Models\Pool')->active();
    }
    
    public function poolBoxPlayers(){
        return $this->hasMany('App\Models\PoolBoxPlayer');
    }

}
