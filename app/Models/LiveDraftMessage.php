<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LiveDraftMessage extends Model
{
    public function user()
    {
        return $this->hasOne('App\Models\Access\User\User','id','sender_user_id');
    }
    public function pool()
    {
        return $this->hasOne('App\Models\Pool','id','pool_id');
    }
}