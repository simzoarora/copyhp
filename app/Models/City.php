<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\State;
use App\Models\Country;
use Log;

class City extends Model {

    protected $fillable = ['name', 'state_id'];
    public $timestamps = false;

    public function state() {
        return $this->belongsTo('App\Models\State');
    }

    public function venues() {
        return $this->hasMany('App\Models\Venue');
    }

    public static function findOrCreate($city_name, $state_name = NULL, $country_name = NULL) {
        $city = self::where('name', $city_name)->first();
        if (!$city) {
            if ($state_name == NULL && $country_name != NULL) {
                $country = Country::where('alpha3', $country_name)->first();
                if ($country) {
                    $state = State::firstOrCreate(array('name' => 'Dummy', 'country_id' => $country->id));
                } else {
                    Log::error("Country NOT FOUND in database name $country_name");
                }
            } else if ($state_name != NULL && $country_name != NULL) {
                $country = Country::where('alpha3', $country_name)->first();
                $state = State::firstOrCreate(array('name' => $state_name, 'country_id' => $country->id));
            }
            if (!empty($state)) {
                $city = self::create(array(
                            'name' => $city_name,
                            'state_id' => $state->id,
                ));
            } else {
                Log::error("State NOT FOUND in database name $state_name");
            }
        }
        return $city;
    }

}
