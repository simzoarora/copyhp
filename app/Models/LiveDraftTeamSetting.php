<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LiveDraftTeamSetting extends Model
{
    protected $fillable = [
        'pool_team_id'
    ];

    public function poolTeam()
    {
        return $this->belongsTo('App\Models\PoolTeam');
    }
}