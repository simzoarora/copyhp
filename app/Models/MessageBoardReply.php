<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;
use Auth;

class MessageBoardReply extends Model {

    use SoftDeletes,
        NodeTrait;

    protected $fillable = [
        'reply',
        'user_id',
        'parent_id',
    ];
    protected $dates = [
        'deleted_at',
    ];
    protected $hidden = ['_lft', '_rgt', 'parent_id'];

    public function messageBoard() {
        return $this->belongsTo('App\Models\MessageBoard');
    }

    public function user() {
        return $this->belongsTo('App\Models\Access\User\User');
    }

    /**
     * Get all of the post's likes.
     */
    public function reactions() {
        return $this->morphMany('App\Models\MessageReaction', 'reactionfor');
    }

    public function likes() {
        return $this->morphOne('App\Models\MessageReaction', 'reactionfor')
                        ->where('type', config('message_board.inverse_reaction.like'))
                        ->groupBy('reactionfor_id')
                        ->selectRaw("count(*) as total, id,reactionfor_id,reactionfor_type");
    }

    public function dislikes() {
        return $this->morphOne('App\Models\MessageReaction', 'reactionfor')
                        ->where('type', config('message_board.inverse_reaction.dislike'))
                        ->groupBy('reactionfor_id')
                        ->selectRaw("count(*) as total, id,reactionfor_id,reactionfor_type");
    }
    
    public function currentUserReaction() {
        return $this->morphOne('App\Models\MessageReaction', 'reactionfor')
                        ->where('user_id', Auth::id())
                        ->select('id', 'reactionfor_id', 'reactionfor_type');
    }

}
