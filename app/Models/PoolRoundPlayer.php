<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolRoundPlayer extends Model {

    public $timestamps = false;
    protected $fillable = [
        'player_season_id',
    ];

    public function poolRound() {
        return $this->belongsTo('App\Models\PoolRound');
    }
    
    public function playerSeason(){
        return $this->hasMany('App\Models\PlayerSeason');
    }

}
