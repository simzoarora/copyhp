<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Venue
 *
 * @author Anik
 */
class Venue extends Model {

    protected $fillable = [
        'api_id',
        'name',
        'short_name',
        'city_id',
        'timezone',
    ];

}
