<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Traits\SoftDeletes;
use Auth;

class LiveDraftPlayer extends Model
{

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'player_season_id',
        'pool_id',
        'pool_team_id',
        'pool_round_id',
    ];

    public function poolTeam()
    {
        return $this->belongsTo('App\Models\PoolTeam');
    }

    public function playerSeason()
    {
        return $this->belongsTo('App\Models\PlayerSeason');
    }

    public function poolRound()
    {
        return $this->belongsTo('App\Models\PoolRound');
    }

    public function pool()
    {
        return $this->belongsTo('App\Models\Pool');
    }

    public static function getPlayerDataForDrafting($pool_id)
    {
        return self::where('pool_id', $pool_id)
                        ->whereHas('poolTeam', function($query) {
                            $query->where('user_id', Auth::id());
                        })
                        ->where('start_time', '<=', date('Y-m-d H:i:s'))
                        ->where('end_time', '>=', date('Y-m-d H:i:s'))
                        ->whereNull('player_season_id')
                        ->firstOrFail();
    }

    public static function buildNextAndOverallPick($pool_team_id, $total_teams, $key, &$next_pick, &$overall_pick)
    {
        $next_pick[$pool_team_id] = (!isset($next_pick[$pool_team_id])) ? ($key + 1) : ($total_teams - ($next_pick[$pool_team_id] - 1));
        $overall_pick[$pool_team_id] = (!isset($overall_pick[$pool_team_id])) ? ($key + 1) : ($total_teams * ceil($overall_pick[$pool_team_id] / $total_teams) + $next_pick[$pool_team_id]);
    }

    public static function changeFutureDraftPlayerTiming($pool_id, $current_draft_player_id)
    {
        $future_draft_players = self::where('pool_id', $pool_id)
                ->where('id', '>', $current_draft_player_id)
                ->get();
        $start_time = date('Y-m-d H:i:s');
        foreach ($future_draft_players as $draft_player) {
            $draft_player->start_time = $start_time;
            $start_time = $draft_player->end_time = date('Y-m-d H:i:s', strtotime($start_time . ' +1 minute 30 seconds'));
            $draft_player->save();
        }
    }

    public static function getDataForDraftTimings($pool_id, $current_user_check = TRUE)
    {
        $query_builder = self::where('pool_id', $pool_id)->with('poolTeam');

        if ($current_user_check) {
            $query_builder->whereHas('pool', function($query) {
                $query->whereHas('poolTeams', function($query) {
                    $query->where('user_id', Auth::id());
                });
            });
        }
        return $query_builder->get();
    }
}