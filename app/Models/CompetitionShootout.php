<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetitionShootout extends Model {

    protected $guarded = [
        'id',
        'competition_id',
        'updated_at',
        'created_at'
    ];

    public function competition() {
        return $this->belongsTo('App\Models\Competition');
    }

    public function skaterSeason() {
        return $this->belongsTo('App\Models\PlayerSeason', 'skater_id');
    }

    public function goalieSeason() {
        return $this->belongsTo('App\Models\PlayerSeason', 'goalie_id');
    }

}
