<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtraLeagueStanding extends Model
{
    protected $guarded = ['id', 'updated_at', 'created_at'];

    public function season()
    {
        return $this->belongsTo('App\Models\Season');
    }

    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }

    public static function processAndSaveDataFromSportsDirect($stat_group, $team, $season_id, $type)
    {
        $league_standing = $team->extraLeagueStandings()->firstOrNew([
            'season_id' => $season_id,
            'type' => $type
        ]);
        if (isset($stat_group['stat'][0])) {
            foreach ($stat_group['stat'] as $stat) {
                $league_standing->{$stat['@attributes']['type']} = $stat['@attributes']['num'];
            }
        } else {
                $league_standing->{$stat_group['stat']['@attributes']['type']} = $stat_group['stat']['@attributes']['num'];
        }
        $league_standing->type = $type;
        $league_standing->save();
    }
}