<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetitionGoal extends Model {

    protected $guarded = [
        'id',
        'competition_id',
        'updated_at',
        'created_at'
    ];

    public function competition() {
        return $this->belongsTo('App\Models\Competition');
    }
    
    public function scorer() {
        return $this->belongsTo('App\Models\PlayerSeason','scorer_id');
    }
    public function firstAssistance() {
        return $this->belongsTo('App\Models\PlayerSeason','first_assisted_by');
    }
    public function secondAssistance() {
        return $this->belongsTo('App\Models\PlayerSeason','second_assisted_by');
    }

}
