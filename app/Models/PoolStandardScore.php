<?php
/**
 * This is used for standard as well as for box pool types
 */


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolStandardScore extends Model {

    protected $fillable = [
        'pool_score_setting_id',
        'pool_team_id',
        'competition_id',
        'pool_team_player_id',
        'value',
    ];

    public function poolScoreSetting() {
        return $this->belongsTo('App\Models\PoolScoreSetting');
    }

    public function poolTeam() {
        return $this->belongsTo('App\Models\PoolTeam');
    }

    public function competition() {
        return $this->belongsTo('App\Models\Competition');
    }

    public function poolTeamPlayer() {
        return $this->belongsTo('App\Models\PoolTeamPlayer');
    }

}
