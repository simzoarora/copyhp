<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TwitterFeed extends Model {

    protected $fillable = [
        'tweet_id',
        'image',
        'name',
        'tweet',
        'tweet_created'
    ];

}
