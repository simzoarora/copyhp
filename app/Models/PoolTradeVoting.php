<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolTradeVoting extends Model {

    protected $guarded = [
        'id'
    ];

    public function poolTrade() {
        return $this->belongsTo('App\Models\PoolTrade');
    }

    public function user() {
        return $this->belongsTo('App\Models\Access\User\User');
    }

}
