<?php

namespace App\Models\Access\User;

use App\Models\Access\User\Traits\UserAccess;
use App\Services\Traits\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\User\Traits\Attribute\UserAttribute;
use App\Models\Access\User\Traits\Relationship\UserRelationship;

/**
 * Class User
 * @package App\Models\Access\User
 */
class User extends Authenticatable {

    use SoftDeletes,
        UserAccess,
        UserAttribute,
        UserRelationship;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'confirmation_code'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at','last_login'];

//    protected $fillable = ['email', 'password', 'name', 'subscribe'];

    /**
     * Pools which user has created
     * @return type
     */
    public function pools() {
        return $this->hasMany('App\Models\Pool')->active();
    }

    /**
     * Pool which user is part of.
     * @return type
     */
    public function poolTeams() {
        return $this->hasMany('App\Models\PoolTeam');
    }

    /**
     * Use to get first data
     * @return type
     */
    public function poolTeam() {
        return $this->hasOne('App\Models\PoolTeam');
    }

    public function notifications() {
        return $this->belongsToMany('App\Models\Notification');
    }

    public function unReadNotifications() {
        return $this->belongsToMany('App\Models\Notification')->where(\DB::raw('DATE(created_at)'), '>=', date('Y-m-d', strtotime('-6 days')))->wherePivot('read_at', NULL);
    }

    public function tradeVotings() {

        return $this->hasMany('App\Models\PoolTradeVoting');
    }

}
