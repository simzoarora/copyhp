<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationUser extends Model {

    public $timestamps = false;
    public $table = "notification_user";

}
