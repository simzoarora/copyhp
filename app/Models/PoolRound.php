<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolRound extends Model {

    protected $fillable = [
        'name',
        'player_position_id'
    ];

    public function pool() {
        return $this->belongsTo('App\Models\Pool')->active();
    }

    /**
     * 
     * @return type
     * @deprecated since version 1
     */
    public function poolRoundPlayers() {
        return $this->hasMany('App\Models\PoolRoundPlayer');
    }

    public function poolTeamPlayers() {
        return $this->hasMany('App\Models\PoolTeamPlayer');
    }

}
