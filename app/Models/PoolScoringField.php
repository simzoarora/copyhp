<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolScoringField extends Model
{

    public function poolScoreSettings()
    {
        return $this->hasMany('App\Models\PoolScoreSetting');
    }

    public function totalUsed()
    {
        return $this->hasMany('App\Models\PoolScoreSetting')
                        ->selectRaw('count(*) as total, pool_scoring_field_id, type')
                        ->groupBy('pool_scoring_field_id', 'type')
                        ->whereHas('pool', function($query) {
                            $query->has('currentSeason')->completed();
                        });
    }

    public static function getFieldsForPoolCreation()
    {
        $pool_scoring_fields = self::with('totalUsed')->orderBy('sort_order')->get();
        $total_pools = Pool::active()->completed()->has('currentSeason')->count();
        $pool_scoring_fields->transform(function($item) use($total_pools) {
            if (!$item->totalUsed->isEmpty()) {
                $item->used = new \stdClass;
                $skater = $item->totalUsed->where('type', config('pool.pool_scoring_field.inverse_type.player'))->first();
                if ($skater != NULL) {
                    if ($total_pools != 0) {
                        $item->used->skater_percent = round(($skater->total / $total_pools) * 100, 2);
                    } else {
                        $item->used->skater_percent = 0;
                    }
                }
                $goalie = $item->totalUsed->where('type', config('pool.pool_scoring_field.inverse_type.goalie'))->first();
                if ($goalie != NULL) {
                    if ($total_pools != 0) {
                        $item->used->goalie_percent = round(($goalie->total / $total_pools) * 100, 2);
                    } else {
                        $item->used->goalie_percent = 0;
                    }
                }
            }
            return $item;
        });
        return $pool_scoring_fields;
//        return $pool_scoring_fields->sortByDesc('used.skater_percent');
    }
}