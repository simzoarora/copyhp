<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageBoardStatus extends Model {


    protected $fillable = [
        'status',
    ];
    
    public function user() {
        return $this->belongsTo('App\Models\Access\User\User');
    }
    
    public function messageBoard() {
        return $this->belongsTo('App\Models\MessageBoard');
    }
}