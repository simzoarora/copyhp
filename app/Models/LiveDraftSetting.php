<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LiveDraftSetting extends Model {

    protected $guarded = [
        'id'
    ];

    public function pool()
    {
        return $this->belongsTo('App\Models\Pool');
    }

}
