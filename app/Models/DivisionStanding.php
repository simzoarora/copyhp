<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DivisionStanding extends Model {

    protected $fillable = ['season_id', 'team_id', 'division_id', 'division_ranking'];

    public function season() {
        return $this->belongsTo('App\Models\Season');
    }

    public function team() {
        return $this->belongsTo('App\Models\Season');
    }

    public function division() {
        return $this->belongsTo('App\Models\Division');
    }

    public static function processAndSaveDataFromSportsDirect($stat_group, $team, $season_id, $division_id) {
        $division_standing = $team->divisionStandings()->firstOrNew([
            'season_id' => $season_id,
            'division_id' => $division_id
        ]);
        if (isset($stat_group['stat'][0])) {
            foreach ($stat_group['stat'] as $stat) {
                if ($stat['@attributes']['type'] == 'division_ranking') {
                    $division_standing->division_ranking = $stat['@attributes']['num'];
                }
            }
        } else {
            $stat = $stat_group['stat'];
            if ($stat['@attributes']['type'] == 'division_ranking') {
                $division_standing->division_ranking = $stat['@attributes']['num'];
            }
        }
        $division_standing->save();
    }

}
