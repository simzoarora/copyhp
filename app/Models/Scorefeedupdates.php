<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scorefeedupdates extends Model {

    protected $fillable = [
        'livescores',
        'schedules',
        'results'
    ];

}
