<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerStat extends Model {

    protected $guarded = [
        'id',
        'updated_at',
        'created_at',
    ];

    public function season() {
        return $this->belongsTo('App\Models\Season');
    }

    public function team() {
        return $this->belongsTo('App\Models\Team');
    }

    public function player() {
        return $this->belongsTo('App\Models\Player');
    }
    
    public static function getStatKeyBySeasonType($season_type){
        return ($season_type == config('competition.inverse_competition_type')['Regular Season']) ? config('pool.stat_keys.regular') : config('pool.stat_keys.post');
    }

}
