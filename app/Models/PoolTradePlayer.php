<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoolTradePlayer extends Model {

    protected $guarded = [
        'id'
    ];

    public function poolTrade() {
        return $this->belongsTo('App\Models\PoolTrade');
    }

    public function poolTeamPlayer() {
        return $this->belongsTo('App\Models\PoolTeamPlayer');
    }

}
