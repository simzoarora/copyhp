<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetitionTeamStat extends Model {

    protected $guarded = [
        'id',
        'competition_id',
        'updated_at',
        'created_at'
    ];

    public function competition() {
        return $this->belongsTo('App\Models\Competition');
    }

}
