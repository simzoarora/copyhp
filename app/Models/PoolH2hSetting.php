<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

class PoolH2hSetting extends Model {

//    use SoftDeletes;
    protected $dates = ['trade_end_date'];
    protected $fillable = [
        'pool_format',
        'playoff_format',
        'lock_eliminated_teams',
        'max_teams',
        'league_start_week',
        'min_goalie_appearance',
        'draft_order',
        'max_acquisition_team',
        'max_acquisition_week',
        'max_trades_season',
        'trade_end_date',
        'trading_draft_pick',
        'trade_reject_time',
        'waiver_time',
        'waiver_mode',
        'playoff_schedule',
        'playoff_teams_number',
        'trade_management',
    ];
    public $timestamps = false;

    public function poolSettings() {
        return $this->morphMany('App\Models\PoolSetting', 'poolsetting');
    }

    public function setTradeEndDateAttribute($value) {
        $this->attributes['trade_end_date'] = date('Y-m-d', strtotime($value));
    }

}
