<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeagueStanding extends Model
{
    protected $guarded = ['id', 'updated_at', 'created_at'];
    protected $appends = ['games_played', 'diff', 'manipulated_team_name'];

    /**
     * accessor for games_played
     */
    public function getGamesPlayedAttribute()
    {
        return $this->games_won + $this->games_lost + $this->games_lost_overtime + $this->games_lost_shootout;
    }

    /**
     * accessor for diff
     */
    public function getDiffAttribute()
    {
        return $this->goals_for - $this->goals_against;
    }

    /**
     * accessor for manipulated_team_name
     */
    public function getManipulatedTeamNameAttribute()
    {
        $team_name = $this->team->display_name;
        if ($this->clinched_best_league_record != NULL) {
            $team_name = 'p-' . $team_name;
        } elseif ($this->clinched_wildcard != NULL) {
            $team_name = 'w-' . $team_name;
        } elseif ($this->clinched_conference != NULL) {
            $team_name = 'z-' . $team_name;
        } elseif ($this->clinched_division_top_3 != NULL) {
            $team_name = 'x-' . $team_name;
        }
        return $team_name;
    }

    public function season()
    {
        return $this->belongsTo('App\Models\Season');
    }

    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }

    public static function processAndSaveDataFromSportsDirect($stat_group, $team, $season_id)
    {
        $league_standing = $team->leagueStandings()->firstOrNew([
            'season_id' => $season_id,
        ]);
        foreach ($stat_group['stat'] as $stat) {
            if ($stat['@attributes']['type'] === 'loss_streak') {
                $league_standing->win_streak = null;
            } elseif ($stat['@attributes']['type'] === 'win_streak') {
                $league_standing->loss_streak = null;
            }
            $league_standing->{$stat['@attributes']['type']} = $stat['@attributes']['num'];
        }
        $league_standing->calculated_points = ($league_standing->games_won * 2) + $league_standing->games_lost_overtime + $league_standing->games_lost_shootout;
        $league_standing->save();
    }

    public static function calculateLeagueRanks($season)
    {
        $season_league_standings = $season->leagueStandings()->orderBy('calculated_points', 'desc')->with('team')->get();
        $grouped_data = [];
        if (!$season_league_standings->isEmpty()) {
            // Making group based on calculated points
            foreach ($season_league_standings as $league_standing) {
                if (!isset($grouped_data[$league_standing->calculated_points])) {
                    $grouped_data[$league_standing->calculated_points] = [];
                }
                $grouped_data[$league_standing->calculated_points][] = $league_standing;
            }
            $rank = 1;
            foreach ($grouped_data as $data) {
                if (count($data) == 1) {
                    $data[0]->rank = $rank++;
                    $data[0]->save();
                } else {
                    $data_collection = collect($data);

                    /**
                     * Tie Breaker using games won
                     */
                    $sorted_games_won = $data_collection->sortByDesc('games_won')->values();
                    $previous_games_won = 0;
                    $previous_data_ref = NULL;
                    $temp_data = [];
                    $temp_data_added = FALSE;
                    $ranks_left = [];
                    foreach ($sorted_games_won as $internal_key => $sorted_data) {
                        if ($previous_games_won != 0) {
                            if ($sorted_data->games_won == $previous_games_won) {
                                // if the first and second element match, we have to add first element for next processing
                                $temp_data[$previous_data_ref->id] = $previous_data_ref;
                                $temp_data[$sorted_data->id] = $sorted_data;
                                $temp_data_added = TRUE;
                                $ranks_left[] = $rank + ($internal_key - 1);
                                $ranks_left[] = $rank + $internal_key;
                            } else {
                                // if the first and second element doesnt match we have to update 1st element too
                                if (!$temp_data_added) {
                                    $previous_data_ref->rank = $rank + ($internal_key - 1);
                                    $previous_data_ref->save();
                                }
                                //save rank of current element
                                if ($internal_key + 1 == count($sorted_games_won)) {
                                    $sorted_data->rank = $rank + $internal_key;
                                    $sorted_data->save();
                                }
                                $temp_data_added = FALSE;
                            }
                        }
                        $previous_data_ref = $sorted_data;
                        $previous_games_won = $sorted_data->games_won;
                    }
                    /**
                     * Tie Breaker using less games played
                     */
                    if (!empty($temp_data)) {
                        $ranks_left = array_unique($ranks_left);
                        $temp_data_collection = collect(array_values($temp_data));
                        $temp_data = self::_tieBreakerUsingGamesPlayed($temp_data_collection, $ranks_left);
                    }
                    /**
                     * Tie breaker using games_won - games_lost_shootout
                     */
                    if (!empty($temp_data)) {
                        $temp_data_collection = collect(array_values($temp_data));
                        $temp_data = self::_tieBreakerThird($temp_data_collection, $ranks_left);
                    }
                    /**
                     * Tie breaker using games_won - games_lost_shootout - games_lost_overtime
                     */
                    if (!empty($temp_data)) {
                        $temp_data_collection = collect(array_values($temp_data));
                        $temp_data = self::_tieBreakerFourth($temp_data_collection, $ranks_left);
                    }
                    /**
                     * Tie breaker using goals_for - goals_against
                     */
                    if (!empty($temp_data)) {
                        $temp_data_collection = collect(array_values($temp_data));
                        $temp_data = self::_tieBreakerFifth($temp_data_collection, $ranks_left);
                    }

                    /**
                     * If still teams are ties, they will be assigned rank based on team name
                     */
                    if (!empty($temp_data)) {
                        $temp_data_collection = collect(array_values($temp_data));
                        $sorted_games_played = $temp_data_collection->sortBy('team.display_name')->values();
                        foreach ($sorted_games_played as $sort_by_games_played) {
                            $sort_by_games_played->rank = array_shift($ranks_left);
                            $sort_by_games_played->save();
                        }
                        $temp_data = [];
                    }

                    $rank += count($data);
                }
            }
        }
    }

    private static function _tieBreakerFifth($temp_data_collection, &$ranks_left)
    {
        $previous_total_games = false;
        $previous_data_ref = NULL;
        $temp_data = [];
        $temp_data_added = FALSE;
        $sorted_games_played = $temp_data_collection->sortByDesc(function ($data) {
                    return $data->goals_for - $data->goals_against;
                })->values();
        foreach ($sorted_games_played as $internal_key => $sort_by_games_played) {
            $total_games = $sort_by_games_played->goals_for - $sort_by_games_played->goals_against;
            self::_assignRanksBasedOnConditions($sorted_games_played, $total_games, $internal_key, $sort_by_games_played, $previous_total_games, $previous_data_ref, $temp_data, $temp_data_added, $ranks_left);
        }
        return $temp_data;
    }

    private static function _tieBreakerFourth($temp_data_collection, &$ranks_left)
    {
        $previous_total_games = false;
        $previous_data_ref = NULL;
        $temp_data = [];
        $temp_data_added = FALSE;
        $sorted_games_played = $temp_data_collection->sortByDesc(function ($data) {
                    return $data->games_won - $data->games_lost_shootout - $data->games_lost_overtime;
                })->values();
        foreach ($sorted_games_played as $internal_key => $sort_by_games_played) {
            $total_games = $sort_by_games_played->games_won - $sort_by_games_played->games_lost_shootout - $sort_by_games_played->games_lost_overtime;
            self::_assignRanksBasedOnConditions($sorted_games_played, $total_games, $internal_key, $sort_by_games_played, $previous_total_games, $previous_data_ref, $temp_data, $temp_data_added, $ranks_left);
        }
        return $temp_data;
    }

    private static function _tieBreakerUsingGamesPlayed($temp_data_collection, &$ranks_left)
    {
        $previous_total_games = false;
        $previous_data_ref = NULL;
        $temp_data_added = FALSE;
        $temp_data = [];
        $sorted_games_played = $temp_data_collection->sortBy(function ($data) {
                    return $data->games_won + $data->games_lost + $data->games_lost_overtime + $data->games_lost_shootout;
                })->values();

        foreach ($sorted_games_played as $internal_key => $sort_by_games_played) {
            $total_games = $sort_by_games_played->games_won + $sort_by_games_played->games_lost + $sort_by_games_played->games_lost_overtime + $sort_by_games_played->games_lost_shootout;
            self::_assignRanksBasedOnConditions($sorted_games_played, $total_games, $internal_key, $sort_by_games_played, $previous_total_games, $previous_data_ref, $temp_data, $temp_data_added, $ranks_left);
        }
        return $temp_data;
    }

    private static function _tieBreakerThird($temp_data_collection, &$ranks_left)
    {
        $previous_total_games = false;
        $previous_data_ref = NULL;
        $temp_data = [];
        $temp_data_added = FALSE;
        $sorted_games_played = $temp_data_collection->sortByDesc(function ($data) {
                    return $data->games_won - $data->games_lost_shootout;
                })->values();
        foreach ($sorted_games_played as $internal_key => $sort_by_games_played) {
            $total_games = $sort_by_games_played->games_won - $sort_by_games_played->games_lost_shootout;
            self::_assignRanksBasedOnConditions($sorted_games_played, $total_games, $internal_key, $sort_by_games_played, $previous_total_games, $previous_data_ref, $temp_data, $temp_data_added, $ranks_left);
        }
        return $temp_data;
    }

    private static function _assignRanksBasedOnConditions($sorted_games_played, $total_games, $internal_key, &$sort_by_games_played, &$previous_total_games, &$previous_data_ref, &$temp_data, &$temp_data_added, &$ranks_left)
    {
        if ($previous_total_games !== false) {
            if ($total_games == $previous_total_games) {
                // if the first and second element match, we have to add first element for next processing
                $temp_data[$previous_data_ref->id] = $previous_data_ref;
                $temp_data[$sort_by_games_played->id] = $sort_by_games_played;
                $temp_data_added = TRUE;
            } else {
                // if the first and second element doesnt match we have to update 1st element too
                if (!$temp_data_added) {
                    $previous_data_ref->rank = array_shift($ranks_left);
                    $previous_data_ref->save();
                }
                //save rank of current element
                if ($internal_key + 1 == count($sorted_games_played)) {
                    $sort_by_games_played->rank = array_pop($ranks_left);
                    $sort_by_games_played->save();
                }
                $temp_data_added = FALSE;
            }
        }
        $previous_data_ref = $sort_by_games_played;
        $previous_total_games = $total_games;
    }
}