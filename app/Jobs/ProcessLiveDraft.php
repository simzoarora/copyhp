<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Models\Pool;
use App\Models\PlayerPreRanking;
use App\Models\PlayerSeason;
use App\Models\LiveDraftQueue;
use App\Models\LiveDraftTeamSetting;
use App\Models\LiveDraftPlayer;
use App\Services\LiveDraftService;
use LRedis;
use Firebase\JWT\JWT;

class ProcessLiveDraft extends Job implements ShouldQueue, SelfHandling
{

    use InteractsWithQueue,
        SerializesModels;
    protected $pool;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Pool $pool)
    {
        $this->pool = $pool;
    }

    /**
     * Execute the job.
     *
     * @return void
     * 
     * TODO : 
     */
    public function handle()
    {
        $this->pool->load([
            'poolRosters'
        ]);
        $break_count = 0;
        while (TRUE) {
            $change_auto_selection = true;

            $next_entry = $this->pool->liveDraftPlayers()
                    ->where('player_season_id', NULL)
                    ->orderBy('start_time')
                    ->with([
                        'poolTeam.poolTeamLineups' => function($query) {
                            $query->where('start_time', '<=', date('Y-m-d'))
                            ->where('end_time', '>=', date('Y-m-d'))
                            ->selectRaw('pool_team_player_id, player_position_id, count(*) as total_filled')
                            ->groupBy('player_position_id');
                        },
                        'poolTeam.liveDraftTeamSetting',
                    ])
                    ->first();
            $timed_out_entry = null;
            if (
                    $next_entry != null &&
                    $next_entry->poolTeam->liveDraftTeamSetting != NULL &&
                    $next_entry->poolTeam->liveDraftTeamSetting->automatic_selection == 1 &&
                    strtotime($next_entry->start_time . " +10 seconds") <= time()) {
                $timed_out_entry = $next_entry;
                $change_auto_selection = false;
            } elseif ($next_entry != null && strtotime($next_entry->end_time) <= time()) {
                $timed_out_entry = $next_entry;
            }
            if ($timed_out_entry != NULL) {
                $pool_team_lineups = $timed_out_entry->poolTeam->poolTeamLineups()
                                ->where('start_time', '<=', date('Y-m-d'))
                                ->where('end_time', '>=', date('Y-m-d'))
                                ->selectRaw('pool_team_player_id, player_position_id, count(*) as total_filled')
                                ->groupBy('player_position_id')
                                ->pluck('total_filled', 'player_position_id')->toArray();
                $live_draft_queue = LiveDraftQueue::where('pool_team_id', $timed_out_entry->pool_team_id)
                        ->orderBy('sort_order')
                        ->first();
                if ($live_draft_queue != NULL) {
                    LiveDraftService::draftPlayer(
                            $this->pool, $timed_out_entry, $live_draft_queue->player_season_id, $pool_team_lineups, config('live-draft.draft_type.inverse.automatic')
                    );
                } else {
                    $player_data = PlayerSeason::where('player_season.season_id', $this->pool->season_id)
                            ->join('player_pre_rankings', function($join) {
                                $join->on('player_season.player_id', '=', 'player_pre_rankings.player_id')
                                ->where('player_pre_rankings.season_id', '=', $this->pool->season_id);
                            })
                            ->whereNotExists(function($subquery) {
                                $subquery->select(\DB::raw(1))
                                ->from('pool_team_players')
                                ->whereRaw('pool_team_players.player_season_id = player_season.id')
                                ->join('pool_teams', 'pool_teams.id', '=', 'pool_team_players.pool_team_id')
                                ->where('pool_teams.pool_id', '=', $this->pool->id);
                            })
                            ->where('is_active', '=', 1)
                            ->orderBy('player_pre_rankings.rank')
                            ->select('player_season.*')
                            ->first();
                    if ($player_data == null) {
                        \Log::alert("AUTOMATIC LIVE DRAFT : pool id " . $this->pool->id . " : Not able to find sufficient data to auto select player based on ranking");
                        break;
                    }
                    LiveDraftService::draftPlayer(
                            $this->pool, $timed_out_entry, $player_data->id, $pool_team_lineups, config('live-draft.draft_type.inverse.automatic')
                    );
                }
                if ($change_auto_selection) {
                    $live_draft_team_setting = LiveDraftTeamSetting::firstOrNew([
                                'pool_team_id' => $timed_out_entry->pool_team_id
                    ]);
                    $live_draft_team_setting->automatic_selection = 1;
                    $live_draft_team_setting->save();
                    $redis = LRedis::connection();
                    $redis->publish("live-draft-autoselection-" . JWT::encode((int) $this->pool->id, env('JWT_KEY')), json_encode(['data' => ['user_id' => $timed_out_entry->poolTeam->user_id]]));
                }
            }
            $break_count++;
            if ($break_count > 5 && !LiveDraftPlayer::where('pool_id', $this->pool->id)->whereNull('player_season_id')->exists()) {
                break;
            }

            if ($break_count > 5) {
                $break_count = 0;
            }
        }
    }
}