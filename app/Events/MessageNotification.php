<?php

namespace App\Events;

use App\Models\Notification;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Description of MessageNotification
 *
 * @author Anik
 */
class MessageNotification extends Event implements ShouldBroadcast {

    use SerializesModels;

    public $notification;
    public $user_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Notification $notification, $user_id) {
        $this->notification = $notification;
        $this->user_id = $user_id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn() {
        return ['notification'];
    }

    public function broadcastWith() {
        return [
            'notification' => $this->notification->toArray(),
            'user_id' => $this->user_id
        ];
    }

    /**
     * Get the broadcast event name.
     *
     * @return string
     */
//    public function broadcastAs() {
//        return 'notification';
//    }

}
