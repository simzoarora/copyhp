<?php

namespace App\Listeners;

use App\Events\MessageNotification;

/**
 * Description of MessageNotification
 *
 * @author Anik
 */
class MessageNotificationListener {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PodcastWasPurchased  $event
     * @return void
     */
    public function handle(MessageNotification $event) {
//        dd($event);
    }

}
