<?php

namespace App\Services;

use LRedis;
use Firebase\JWT\JWT;
use App\Models\PoolTeamPlayer;
use App\Models\PoolTeamLineup;
use App\Models\LiveDraftQueue;
use App\Models\LiveDraftPlayer;
use App\Models\Pool;
use App\Models\PoolTeam;

/**
 * Description of LiveDraftService
 *
 * @author Anik
 */
class LiveDraftService
{

    public static function draftPlayer($pool, $live_draft_player, $player_season_id, $pool_team_lineups = NULL, $type = NULL)
    {
        $live_draft_player->player_season_id = $player_season_id;
        $live_draft_player->end_time = date('Y-m-d H:i:s');
        $live_draft_player->drafted_at = date('Y-m-d H:i:s');
        if ($type !== NULL) {
            $live_draft_player->type = $type;
        }
        $live_draft_player->save();
        $pool_team_player = PoolTeamPlayer::firstOrCreate([
                    'pool_team_id' => $live_draft_player->pool_team_id,
                    'pool_round_id' => $live_draft_player->pool_round_id,
                    'player_season_id' => $player_season_id,
        ]);
        if ($pool_team_lineups === NULL) {
            $pool->poolTeam->poolTeamLineups->pluck('total_filled', 'player_position_id')->toArray();
        }
        $team_roster_track = self::_createTeamRosterTrack($pool->poolRosters, $pool_team_lineups);

        PoolTeamLineup::savePlayerLineup($pool_team_player, $pool->season->end_date, $team_roster_track);

        // Remove the added player from all the queues of this pool as this player cant be drafted again
        LiveDraftQueue::whereHas('poolTeam', function($query) use($pool) {
            $query->where('pool_id', $pool->id);
        })->where('player_season_id', $player_season_id)->delete();

        LiveDraftPlayer::changeFutureDraftPlayerTiming($pool->id, $live_draft_player->id);

        usleep(600000); // delyaing for 0.6 seconds as javascript calculating stuff in microseconds
        $redis = LRedis::connection();
        $redis->publish("live-draft-" . JWT::encode((int) $pool->id, env('JWT_KEY')), json_encode(self::_prepareBroadcastData($pool->id)));
    }

    private static function _createTeamRosterTrack($pool_rosters, $filled_positions)
    {
        $team_roster_track = [];
        foreach ($pool_rosters as $pool_roster) {
            if ($pool_roster->value > 0) {
                $team_roster_track[$pool_roster->player_position_id] = [
                    'total' => $pool_roster->value,
                    'used' => (isset($filled_positions[$pool_roster->player_position_id])) ? $filled_positions[$pool_roster->player_position_id] : 0,
                ];
            }
        }
        return $team_roster_track;
    }

    private static function _prepareBroadcastData($pool_id)
    {
        $pool_data = Pool::getDataOfRoundsForLiveDraft($pool_id, FALSE);
        return [
            'data' => [
                'teams_tab' => self::buildDataForTeamTab($pool_data),
                'draft_results_tab' => self::buildDataForDraftResultsTab($pool_data)->toArray(),
                'user_id' => PoolTeam::getUserIdOfPool($pool_id)->toArray(),
                'draft_timing' => LiveDraftPlayer::getDataForDraftTimings($pool_id, FALSE)->toArray()
            ]
        ];
    }

    public static function buildDataForTeamTab($pool)
    {
        $next_pick = [];
        $overall_pick = [];
        $built_data = [];
        $total_teams = $pool->poolTeams->count();
        foreach ($pool->poolTeams as $key => $pool_team) {
            $built_data[$pool_team->id] = [
                'team' => $pool_team->toArray(),
                'rounds' => []
            ];
            foreach ($pool->poolRounds as $pool_round) {
                LiveDraftPlayer::buildNextAndOverallPick($pool_team->id, $total_teams, $key, $next_pick, $overall_pick);
                $player = $pool_team->poolTeamPlayers->where('pool_round_id', $pool_round->id)->first();
                $built_data[$pool_team->id]['rounds'][] = [
                    'pick' => $next_pick[$pool_team->id],
                    'overall' => intval($overall_pick[$pool_team->id]),
                    'player' => ($player != NULL) ? $player->toArray() : NULL,
                ];
            }
            unset($built_data[$pool_team->id]['team']->poolTeamPlayers);
        }
        return $built_data;
    }

    public static function buildDataForDraftResultsTab($pool)
    {
        $built_data = [];
        $next_pick = [];
        $overall_pick = [];
        $total_teams = $pool->poolTeams->count();
        foreach ($pool->poolTeams as $key => $pool_team) {
            foreach ($pool->poolRounds as $round_key => $pool_round) {
                LiveDraftPlayer::buildNextAndOverallPick($pool_team->id, $total_teams, $key, $next_pick, $overall_pick);
                $player = $pool_team->poolTeamPlayers->where('pool_round_id', $pool_round->id)->first();
                $built_data[] = [
                    'round' => $round_key + 1,
                    'round_id' => $pool_round->id,
                    'pick' => $next_pick[$pool_team->id],
                    'overall' => intval($overall_pick[$pool_team->id]),
                    'team' => $pool_team->name,
                    'pool_team_id' => $pool_team->id,
                    'player' => ($player != NULL) ? $player->toArray() : NULL,
                ];
            }
        }
        return collect($built_data)->sortBy(function($post) {
                    return intval($post['round'] . $post['pick']);
                })->values();
    }

    public static function convertStartTimeToSystemTime($live_draft_setting)
    {
        $date = new \DateTime($live_draft_setting->date . " " . $live_draft_setting->time, new \DateTimeZone($live_draft_setting->time_zone));
        $date->setTimezone(new \DateTimeZone(config('app.timezone')));
        return $date;
    }
}