<?php

namespace App\Services\Traits;

use App\Services\Scopes\FutureDataScope;

/**
 * This trait is used to put a data to be used in future using created_at field.  
 *
 * @author Anik
 */
trait FutureData {

    /**
     * Boot the future data trait for a model.
     *
     * @return void
     */
    public static function bootFutureData() {
        static::addGlobalScope(new FutureDataScope);
    }

    /**
     * Get the fully qualified "created at" column.
     *
     * @return string
     */
    public function getQualifiedCreatedAtColumn() {
        return $this->getTable() . '.' . $this->getCreatedAtColumn();
    }

    /**
     * Get the name of the "created at" column.
     *
     * @return string
     */
    public function getCreatedAtColumn() {
        return defined('static::CREATED_AT') ? static::CREATED_AT : 'created_at';
    }

}
