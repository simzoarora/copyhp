<?php

namespace App\Services\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Description of FutureDataScope
 *
 * @author Anik
 */
class FutureDataScope implements Scope {

    /**
     * All of the extensions to be added to the builder.
     *
     * @var array
     */
    protected $extensions = ['WithFutureData', 'OnlyFutureData'];

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model) {
        $builder->where($model->getQualifiedCreatedAtColumn(), '<=', date('Y-m-d H:i:s'));
    }

    /**
     * Extend the query builder with the needed functions.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    public function extend(Builder $builder) {
        foreach ($this->extensions as $extension) {
            $this->{"add{$extension}"}($builder);
        }
    }

    /**
     * Add the with-future-data extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addWithFutureData(Builder $builder) {
        $builder->macro('withFutureData', function (Builder $builder) {
            return $builder->withoutGlobalScope($this);
        });
    }

    /**
     * Add the only future data extension to the builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @return void
     */
    protected function addOnlyFutureData(Builder $builder) {
        $builder->macro('onlyFutureData', function (Builder $builder) {
            $model = $builder->getModel();
            $builder->withoutGlobalScope($this)
                    ->where($model->getQualifiedCreatedAtColumn(), '>', date('Y-m-d H:i:s'));
            return $builder;
        });
    }

}
