<?php

/*
 * Constant file for having all constant related to competition
 */

return [
    'type' => [
        1 => 'Past',
        2 => 'Current',
        3 => 'Scheduled',
        4 => 'Yesterday',
    ],
    'inverse_type' => [
        'past' => 1,
        'current' => 2,
        'scheduled' => 3,
        'yesterday' => 4,
    ],
    'competition_type' => [
        1 => 'Regular Season',
        2 => 'Playoffs',
        3 => 'Special',
    ],
    'inverse_competition_type' => [
        "Pre-Season" => 0,
        "Regular Season" => 1,
        "Playoff" => 2,
        'Conference Quarterfinals' => 2,
        'Conference Semifinals' => 2,
        'Conference Finals' => 2,
        'Stanley Cup Finals' => 2,
        'All Star' => 3,
    ],
    'schedule_competition_type' => [
        -1 => 'Select Game Type', 
        0 => 'Pre-Season',
        1 => 'Regular Season',
        2 => 'Playoffs',
        3 => 'Special',
    ],
    'inverse_schedule_competition_type' => [
        'Select Game Type' => -1,
        'Pre-Season' => 0,
        'Regular Season' => 1,
        'Playoffs' => 2,
        'Conference Quarterfinals' => 2,
        'Conference Semifinals' => 2,
        'Conference Finals' => 2,
        'Stanley Cup Finals' => 2,
        'All Star' => 3,
    ],
];

