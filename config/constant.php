<?php

return [
    'new_feed_link' => 'http://xml.sportsdirectinc.com/Atom?feed=/nhl/livescores/hockey&apiKey=0995E727-5411-4E8F-B0E6-F6E8A8E524E6',
    'other_feed_link' => 'http://xml.sportsdirectinc.com/Atom?feed=/nhl/schedules/hockey&apiKey=0995E727-5411-4E8F-B0E6-F6E8A8E524E6',
    'current_session_link' => 'http://xml.sportsdirectinc.com/Atom?feed=/nhl/results/hockey&apiKey=0995E727-5411-4E8F-B0E6-F6E8A8E524E6',
    'results_link' => 'http://xml.sportsdirectinc.com/Atom?feed=/results/hockey&apiKey=0995E727-5411-4E8F-B0E6-F6E8A8E524E6',
    'sportsdirectconstant' => [
        'team_id_prefix' => '/sport/hockey/team:',
        'player_id_prefix' => '/sport/hockey/player:',
        'player_position_id_prefix' => '/sport/hockey/player-position:',
        'player_role_id_prefix' => '/sport/hockey/player-role:',
        'goal_id_prefix' => '/sport/hockey/competition-event/goal:',
        'penalty_id_prefix' => '/sport/hockey/competition-event/penalty:',
        'shootout_id_prefix' => '/sport/hockey/competition-event/shootout-result:',
        'competition_id_prefix' => '/sport/hockey/competition:',
        'season_id_prefix' => '/sport/hockey/season:',
        'injury_id_prefix' => '/sport/hockey/injury:',
        'venue_id_prefix' => '/sport/hockey/venue:',
        'conference_id_prefix' => '/sport/hockey/conference:',
        'division_id_prefix' => '/sport/hockey/division:',
        'inverse_player_role_ids' => [
            'goalie' => 4,
            'skater' => 5,
        ],
        'extra_league_standings_types' => [
            1 => 'last_10_league_standings',
            2 => 'home_league_standings',
            3 => 'away_league_standings',
        ],
        'inverse_extra_league_standings_types' => [
            'last10' => 1,
            'home' => 2,
            'away' => 3,
        ],
    ],
    'timezone_for_schedule' => [
        'NT' => 'Newfoundland Time',
        'AT' => 'Atlantic Time',
        'ET' => 'Eastern Time',
        'CT' => 'Central Time',
        'MT' => 'Mountain Time',
        'PT' => 'Pacific Time',
    ],
    'inverse_timezone_key_to_timezone' => [
        'NT' => 'Canada/Newfoundland',
        'AT' => 'Canada/Atlantic',
        'ET' => 'Canada/Eastern',
        'CT' => 'Canada/Central',
        'MT' => 'Canada/Mountain',
        'PT' => 'Canada/Pacific',
    ],
    'blog' => [
        'status' => [
            'published' => 1,
            'draft' => 0
        ],
        'homepage_blog_limit' => 3,
    ],
    'pagination' => [
        'nhl_player_stats' => [
            'per_page_limit' => 25
        ]
    ],
    'nhl_play_stat' => [
        'game_type' => [
            'regular' => 'Regular Season',
            'post' => 'Playoffs',
        ]
    ],
    'total_faqs' => 15,
    'notification_settings' => [
        'weekly_standing_updates' => 'auth.editAccount.form_fields.checkbox_1',
        'trade_offers' => 'auth.editAccount.form_fields.checkbox_2',
        'roster_notification' => 'auth.editAccount.form_fields.checkbox_3',
        'newsletter' => 'auth.editAccount.form_fields.checkbox_4',
    ]
];
