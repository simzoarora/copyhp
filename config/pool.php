<?php
$league_start_week = Array();
for ($i = 1; $i <= 25; $i++) {
    $league_start_week[$i] = date('jS', strtotime(date('Y') . '-' . date('m') . '-' . $i)) . ' week';
}

$min_goalie_appearance = Array();
$min_goalie_appearance[0] = 'None';
for ($i = 1; $i <= 10; $i++) {
    $min_goalie_appearance[$i] = $i;
}

$max_acquisition_team = Array();
$max_acquisition_team[-1] = 'No maximum';
$max_acquisition_team[0] = 'No player acquisitions allowed';
for ($i = 1; $i <= 100; $i++) {
    $max_acquisition_team[$i] = $i;
}

$max_acquisition_week = Array();
$max_acquisition_week[-1] = 'No maximum';
$max_acquisition_week[0] = 'No player acquisitions allowed';
for ($i = 1; $i <= 30; $i++) {
    $max_acquisition_week[$i] = $i;
}

$max_trades_season = Array();
$max_trades_season[-1] = 'No maximum';
$max_trades_season[0] = 'No trades allowed';
for ($i = 1; $i <= 40; $i++) {
    $max_trades_season[$i] = $i;
}

$trade_reject_time = Array();
$trade_reject_time[0] = 'No protests allowed';
$trade_reject_time[1] = '1 day';
for ($i = 1; $i <= 6; $i++) {
    $trade_reject_time[$i + 1] = ($i + 1) . ' days';
}

$waiver_time = Array();
$waiver_time[1] = '1 day';
for ($i = 1; $i <= 3; $i++) {
    $waiver_time[$i + 1] = ($i + 1) . ' days';
}

$max_teams_standard = array();
for ($i = 1; $i <= 20; $i++) {
    $max_teams_standard[$i] = $i;
}

$addition_deletion_time = \Carbon\Carbon::tomorrow()->addHours(5);

return [
    'type' => [
        1 => 'Head to Head',
        2 => 'Box',
        3 => 'Standard',
    ],
    'inverse_type' => [
        'h2h' => 1,
        'box' => 2,
        'standard' => 3,
    ],
    'format' => [
        'h2h' => [
            1 => 'Head-to-Head - Categories',
            2 => 'Head-to-Head - Weekly Win',
            3 => 'Head-to-Head - Weekly Win (Daily)',
        ],
        'standard' => [
            4 => 'Standard - Standard',
            5 => 'Standard - Rotisserie'
        ]
    ],
    'inverse_format' => [
        'h2h_cat' => 1,
        'h2h_weekly' => 2,
        'h2h_weekly_daily' => 3,
        'standard_standard' => 4,
        'standard_rotisserie' => 5,
    ],
    'playoff_format' => [
        0 => 'None',
        1 => 'PlayOff',
    ],
    'inverse_playoff_format' => [
        'none' => 0,
        'playoff' => 1,
    ],
    'max_teams' => [
        4 => 4,
        6 => 6,
        8 => 8,
        10 => 10,
        12 => 12,
        14 => 14,
        16 => 16,
        18 => 18,
        20 => 20,
        22 => 22,
        24 => 24,
        26 => 26,
        28 => 28,
        30 => 30
    ],
    'max_teams_standard' => $max_teams_standard,
    'season' => [
        1 => 'Regular Season',
        2 => 'Playoffs'
    ],
    'inverse_season' => [
        'regular_season' => 1,
        'playoff' => 2,
    ],
    'league_start_week' => $league_start_week,
    'min_goalie_appearance' => $min_goalie_appearance,
    'max_acquisition_team' => $max_acquisition_team,
    'max_acquisition_week' => $max_acquisition_week,
    'max_trades_season' => $max_trades_season,
    'trade_reject_time' => $trade_reject_time,
    'waiver_mode' => [
        0 => 'None',
        1 => 'Standard'
    ],
    'waiver_time' => $waiver_time,
    'trade_deadline' =>  env('TRADE_DEADLINE', '02/24/2020'),
    'pool_scoring_field' => [
        'type' => [
            1 => 'Player',
            2 => 'Goalie',
            3 => 'Both',
        ],
        'inverse_type' => [
            'player' => 1,
            'goalie' => 2,
            'both' => 3,
        ],
        'inverse_fields' => [
            'goals' => 1,
            'assists' => 2,
            'plus_minus' => 3,
            'powerplay_point' => 4,
            'powerplay_goal' => 5,
            'powerplay_assist' => 6,
            'hits' => 7,
            'shots' => 8,
            'penalty_minutes' => 9,
            'hat_tricks' => 10,
            'game_winning_goals' => 11,
            'defense_goal' => 12,
            'goals_overtime' => 13,
            'goals_shorthanded' => 14,
            'blocked_shots' => 15,
            'games_played' => 16,
            'shifts' => 17,
            'faceoff_lost' => 18,
            'faceoff_won' => 19,
            'shootout_attempts' => 20,
            'shootout_scored' => 21,
            'games_started' => 22,
            'goals_against' => 23,
            'goals_against_avg' => 24,
            'wins' => 25,
            'shutouts' => 26,
            'ties' => 27,
            'looses' => 28,
            'overtime_losses' => 29,
            'shootout_losses' => 30,
            'shots_against' => 31,
            'short_handed_points' => 32,
            'short_handed_goals' => 33,
            'short_handed_assists' => 34,
            'save_percentage' => 35,
        ],
        'fields_map_to_stat_table' => [
            1 => 'goals',
            2 => 'assists',
            3 => 'plus_minus',
            4 => 'points_power_play',
            5 => 'goals_power_play',
            6 => 'assists_power_play',
            7 => 'hits',
            8 => 'shots',
            9 => 'penalty_minutes',
            10 => '',
            11 => 'game_winning_goals',
            12 => '',
            13 => 'goals_overtime',
            14 => 'goals_short_handed',
            15 => 'blocked_shots',
            16 => 'games_played',
            17 => 'shifts',
            18 => 'faceoffs_lost',
            19 => 'faceoffs_won',
            20 => 'shootouts_attempted',
            21 => 'shootouts_scored',
            22 => 'games_started',
            23 => 'goals_against',
            24 => 'goals_against_average',
            25 => 'wins',
            26 => 'shutouts',
            27 => 'ties',
            28 => 'losses',
            29 => 'overtime_losses',
            30 => 'shootout_losses',
            31 => 'shots_against',
            32 => 'points_short_handed',
            33 => 'goals_short_handed',
            34 => 'assists_short_handed',
            35 => 'save_percentage',
            'total_points' => 'total_points',
        ]
    ],
    'scoring' => [
        'win' => 2,
        'loss' => 0,
        'tie' => 1,
    ],
    'player_position_grouping' => [
        6 => [
            1,
            5,
            2,
            3,
        ],
        7 => [
            1,
            2,
            5
        ],
        8 => [
            1,
            5,
            2,
            3,
            4
        ]
    ],
    'inverse_player_position' => [
        'c' => 1,
        'rw' => 2,
        'd' => 3,
        'g' => 4,
        'lw' => 5,
        's' => 6,
        'f' => 7,
        'bn' => 8,
        'ir' => 9,
    ],
    'playoff_schedule' => [
        1 => 'Last week',
        2 => '2nd Last week'
    ],
    'playoff_teams_number' => [
        4 => 4,
//        6 => 6,
        8 => 8,
//        10 => 10,
//        12 => 12,
//        14 => 14,
        16 => 16,
//        18 => 18
    ],
    'pool_editable_settings' => [
        'pool_setting' => [
            'fields' => [
                'pool_format',
                'season_type',
                'playoff_format',
            ]
        ],
        'scoring' => [
        ],
        'draft_order' => [
            'fields' => [
                'max_teams',
            ],
            'pages' => [
                'teams',
                'roster',
                'draft_order'
            ]
        ],
        'draft' => [
            'pages' => [
                'draft_rounds'
            ]
        ],
        'pool_creation' => [
            'fields' => [
                'league_start_week'
            ]
        ],
        'first_game' => [
            'fields' => [
                'name',
                'logo',
                'lock_eliminated_teams',
                'min_goalie_appearance',
                'max_acquisition_team',
                'max_acquisition_week',
                'max_trades_season',
                'trade_end_date',
                'trade_reject_time',
                'waiver_time',
                'waiver_mode',
                'trading_draft_pick'
            ],
            'pages' => [
                'scoring'
            ]
        ],
    ],
    'paginations' => [
        'league_leaders' => [
            'max_results' => 100,
            'per_page_limit' => 25
        ],
        'player_stats' => [
            'per_page_limit' => 25
        ],
        'user_pools' => [
            'per_page_limit' => 8
        ],
    ],
    'stat_keys' => [
        'pre' => 'pre-season-stats',
        'regular' => 'regular-season-stats',
        'post' => 'post-season-stats',
    ],
    'addition_deletion_time' => $addition_deletion_time,
    'pool_trades' => [
        'status' => [
            0 => 'No Action',
            1 => "Accepted",
            2 => "Rejected",
            3 => "Auto Rejected",
            4 => "Cancelled By Parent",
            5 => "Cancelled By Commissioner",
        ],
        'inverse_status' => [
            'no_action' => 0,
            'accepted' => 1,
            'rejected' => 2,
            'auto_rejected' => 3,
            'cancelled_by_parent' => 4,
            'cancelled_by_commissioner' => 5
        ],
        'trade_player_type' => [
            0 => 'Trade Players',
            1 => 'Drop Players'
        ],
        'inverse_trade_player_type' => [
            'trade_players' => 0,
            'drop_players' => 1
        ],
        'pool_trade_setting_based_status' => [
            'no_action' => 0,
            'accepted' => 1,
            'rejected' => 2
        ]
    ],
    'payment_link' => 'https://safecart.com/hockeypool/',
    'inverse_payment_items' => [
        'premium' => 1,
        'ad_supported' => 3,
    ],
    'transactions' => [
        'type' => [
            1 => 'Deleted',
            2 => 'Added',
            3 => 'Traded',
        ],
        'inverse_type' => [
            'deleted' => 1,
            'added' => 2,
            'traded' => 3,
        ],
    ],
    'paths' => [
        'logo' => '/img/pool_logos/',
        'team_logo' => '/img/team_logos/'
    ],
    'inverse_pool_settings' => [
        'max_acquisition_team' => [
            'unlimited' => -1,
            'no_add' => 0,
        ],
        'max_acquisition_week' => [
            'unlimited' => -1,
            'no_add' => 0,
        ],
        'max_trades_season' => [
            'unlimited' => -1,
            'no_add' => 0,
        ],
    ],
    'pool_matchups' => [
        'type' => [
            0 => 'Regular Matches',
            1 => 'Playoff Matches'
        ],
        'inverse_type' => [
            'regular_matches' => 0,
            'playoff_matches' => 1,
        ]
    ],
    'player_position_team' => [
        1 => [
            1,
            6,
            7,
            8
        ],
        2 => [
            2,
            6,
            7,
            8
        ],
        3 => [
            3,
            6,
            8
        ],
        4 => [
            4,
            8
        ],
        5 => [
            5,
            6,
            7,
            8
        ],
        6 => [
            1,
            5,
            2,
            3,
            6,
            8
        ],
        7 => [
            1,
            2,
            5,
            7,
            8
        ]
    ],
    'pool_creator_scoring_stats_values' => [
        0 => [
            'stat' => 'W',
            'value' => 2
        ],
    ],
    'pool_trade_managemnet_settings' => [
        0 => 'None',
        1 => 'Commissioner Decides',
        2 => 'League Votes'
    ],
    'pool_trade_managemnet_settings_inverse' => [
        'none' => 0,
        'commissioner_decides' => 1,
        'league_votes' => 2
    ],
    'pool_trade_vote_status' => [
        'Accept' => 1,
        'Reject' => 2
    ],
];

