<?php
return [
    'draft_type' => [
        'regular' => [
            0 => 'Manual',
            1 => 'Automatic'
        ],
        'inverse' => [
            'manual' => 0,
            'automatic' => 1
        ]
    ]
];

