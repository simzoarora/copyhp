<?php

return [
    'paginations' => [
        'replies' => [
            'per_page_limit' => 5
        ],
        'boards' => [
            'per_page_limit' => 5
        ]
    ],
    'inverse_reaction' => [
        'like' => 1,
        'dislike' => 2
    ],
    'message_type' => [
        'public' => 0,
        'private' => 1
    ]
];

