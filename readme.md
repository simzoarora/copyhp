# Hockey Pool setup instructions:
### (Assuming all basic setup on server and node, gulp  installed). If you are doing it on SERVER(Not on homestead)  

1. Pull the git setup (only get development branch)
1. sudo composer install
1. npm install node-gyp marked -g
1. npm install
1. Create a database with name hockeypool
1. Create .env file with your own settings(.env.example included)
1. php artisan key:generate
1. php artisan migrate
1. php artisan db:seed
1. gulp
1. chmod 0777 -R storage
1. Go to project directory then "cd nodejs" and then "node atom_feed.js" (this will start bringing the scoreboard data on top of page and twitter feeds on home sidebar).
1. You also need to add screen for node atom_feed.js file to work continously. For installing screen "sudo apt-get install screen" (or some related technology)
1. Also need to add a cron job for url:siteurl/atomfeed for 1 minute. (this will start bringing the scoreboard latest data after 1 minute)
1. Add your ip in master.blade.php file in front of socket_url variable for sockets to connect.
1. sudo php artisan queue:listen in a screen
1. php /path/to/artisan schedule:run >> /dev/null 2>&1

##############################
### If done with HOMESTEAD. (If you don't have vagrant and virtual box installed)  

1. Go to https://www.virtualbox.org/ for virtual box and just go with simple download(it is pretty simple)
1. Go to https://www.vagrantup.com/ for vagrant and just go with simple download(it is pretty simple)
1. To check both of these are installed properly. Just type "vagrant" in your terminal. If it shows common commands, then you are good to go.
1. Now, just follow the above mentioned steps from 1 to 9.
1. Make changes in homestead.yaml file with your own settings(Homestead.yaml.example included)
1. No need to make any changes in vagrant file. (You don't have to install homestead, As homestead is already installed as per the Hockeypool project)

### For looking at database. (A adminer file has already been added to public folder)
### Commands to run first time in sequence.
1. php artisan cron:FetchSeasonTeams
1. php artisan cron:FetchSeasonTeamPlayers
1. php artisan cron:FetchDraftPlayer
1. php artisan cron:FetchInjuredPlayer
1. php artisan cron:FetchStandings
1. php artisan cron:GetHistoricalPlayerStats
1. php artisan cron:FetchSeasonSchedule
1. php artisan cron:SaveCompetitionTypes
1. php artisan cron:CreateSeasonWeeks
1. php artisan cron:FetchBoxScore
1. php artisan cron:UpdateTeamLogo
1. php artisan cron:SortPlayerPosition