var fs = require('fs');
require('dotenv').config({path: __dirname + '/../.env'});

var jwt = require('jsonwebtoken');
var app = require('express')();
if (process.env.APP_ENV == 'production' || process.env.APP_ENV == 'development') {
    var options = {
        key: fs.readFileSync('/home/forge/server.key'),
        cert: fs.readFileSync('/home/forge/server.crt'),
        requestCert: false,
        rejectUnauthorized: false
    };
    var server = require('https').createServer(options, app);
} else {
    var server = require('http').createServer(app);
}

var io = require('socket.io').listen(server),
        redis = require('redis');

server.listen(process.env.SOCKET_SERVER_PORT, '0.0.0.0', function () {
    console.log('Listening at: http://localhost:' + process.env.SOCKET_SERVER_PORT);
});

users = {}; //tracking usernames private messaging

var redisClient = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);
io.on('connection', function (socket) {
    //adding new users 
    socket.on('new_user', function (data) {
        jwt.verify(data, process.env.JWT_KEY, function (err, decoded) {
            socket.nickname = decoded;
            users[socket.nickname] = socket;
        });
    });

    //removing name from list if user disconnect from chat. server end 
    socket.on('disconnect', function () {
        if (!socket.nickname)
            return; //if  user disconnect without choosing a user name
        delete users[socket.nickname]; //deleting socket used by user
        console.log('connection close');
    });

});

redisClient.psubscribe('*', function (err, count) {
    //
});

function emitToUser(user_id, channel, message) {
    if (users[user_id]) {
        users[user_id].emit(channel, message);
    } else {
        console.log('user not set : ' + user_id);
    }
}

redisClient.on('pmessage', function (subscribed, channel, message) {
    message = JSON.parse(message);
    if (message.data && message.data.user_id) {
        if (message.data.user_id.constructor === Array) {
            for (var i = 0; i < message.data.user_id.length; i++) {
                emitToUser(message.data.user_id[i], channel, message);
            }
        } else {
            emitToUser(message.data.user_id, channel, message);

        }
    } else {
        io.emit(channel, message);
    }
});