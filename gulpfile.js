var elixir = require('laravel-elixir');
// require('./elixir-extensions');

elixir(function (mix) {
    mix
//            .phpUnit()
//            .compressHtml()
//            .copy(
//                    'node_modules/font-awesome/fonts',
//                    'public/build/fonts' 
//                    )
            //home public page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/home/home_public.scss'
            ], 'resources/assets/css/frontend/home_public.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/home_public.css'
            ], 'public/css/home_public.css')
            .scripts([
                'plugin/underscore-min.js',
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/home/home_public.js'
            ], 'public/js/home_public.js')
            //home logged page 
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/home/home_loggedin.scss'
            ], 'resources/assets/css/frontend/home_loggedin.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/home_loggedin.css'
            ], 'public/css/home_loggedin.css')
            .scripts([
                'plugin/underscore-min.js',
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/moment.min.js',
                'plugin/moment-timezone-with-data.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/home/home_loggedin.js'
            ], 'public/js/home_loggedin.js')
            //landing page
//            .sass([
//                'plugin/sweetalert/sweetalert.scss',
//                'frontend/landing/style.scss'
//            ], 'resources/assets/css/frontend/landing.css')
//            .styles([
//                'frontend/landing.css'
//            ], 'public/css/landing.css')
//            .scripts([
//                'plugin/sweetalert/sweetalert.min.js',
//                'frontend/landing/jquery.form.js', 
//                'frontend/landing/jquery.validate.min.js',
//                'frontend/landing/respond.min.js',
//                'frontend/landing/contact.js', 
//                'frontend/landing/scripts.js'
//            ], 'public/js/landing.js')

            //pool_creator page 
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'backend/plugin/toastr/toastr.scss',
                'frontend/pool/pool_creator.scss'
            ], 'resources/assets/css/frontend/pool_creator.css')
            .styles([
                '../sass/plugin/bootstrap-datetimepicker.min.css',
                '../sass/plugin/selectBoxIt.css',
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/squaredThree.css',
                '../sass/plugin/html5imageupload.css',
                'frontend/pool_creator.css'
            ], 'public/css/pool_creator.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/moment.min.js',
                'plugin/moment-timezone-with-data.min.js',
                'plugin/bootstrap-datetimepicker.min.js',
                'plugin/html5imageupload.min.js',
                'backend/plugin/toastr/toastr.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/pool/pool_creator.js'
            ], 'public/js/pool_creator.js')
            //scores game details page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/scores/game_details.scss'
            ], 'resources/assets/css/frontend/game_details.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/game_details.css'
            ], 'public/css/game_details.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/scores/game_details.js'
            ], 'public/js/game_details.js')
//            //scores index page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/scores/scores_index.scss'
            ], 'resources/assets/css/frontend/scores_index.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                '../sass/plugin/bootstrap-datetimepicker.min.css',
                'frontend/scores_index.css'
            ], 'public/css/scores_index.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/moment.min.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/bootstrap-datetimepicker.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/scores/scores_index.js'
            ], 'public/js/scores_index.js')
            //pool overview page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/pool_overview.scss'
            ], 'resources/assets/css/frontend/pool_overview.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/pool_overview.css'
            ], 'public/css/pool_overview.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/jquery.dataTables.min.js',
                'plugin/moment.min.js',
                'plugin/moment-timezone-with-data.min.js',
                'plugin/bootstrap.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/includes/team_records.js',
                'frontend/includes/player_news.js',
                'frontend/pool/pool_overview.js'
            ], 'public/js/pool_overview.js')
            //Transactions page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/transactions.scss'
            ], 'resources/assets/css/frontend/transactions.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/transactions.css'
            ], 'public/css/transactions.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/includes/team_records.js',
                'frontend/includes/player_news.js',
                'frontend/pool/transactions.js'
            ], 'public/js/transactions.js')
            //In action tonight page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/in_action_tonight.scss'
            ], 'resources/assets/css/frontend/in_action_tonight.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/in_action_tonight.css'
            ], 'public/css/in_action_tonight.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/includes/team_records.js',
                'frontend/includes/player_news.js',
                'frontend/pool/in_action_tonight.js'
            ], 'public/js/in_action_tonight.js')
            //Live stats page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/live_stats.scss'
            ], 'resources/assets/css/frontend/live_stats.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/live_stats.css'
            ], 'public/css/live_stats.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/jquery.dataTables.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool/live_stats.js'
            ], 'public/js/live_stats.js')
            //Trades page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/trades.scss'
            ], 'resources/assets/css/frontend/trades.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/trades.css'
            ], 'public/css/trades.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool/trades.js'
            ], 'public/js/trades.js')
            //Standings page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/nhl/standings.scss'
            ], 'resources/assets/css/frontend/standings.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/standings.css'
            ], 'public/css/standings.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/nhl/standings.js'
            ], 'public/js/standings.js')
            //Pool info page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/new/pool_info.scss'
            ], 'resources/assets/css/frontend/pool_info.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/pool_info.css'
            ], 'public/css/pool_info.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/new/pool_info.js'
            ], 'public/js/pool_info.js')
            //Schedule page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/nhl/schedule.scss'
            ], 'resources/assets/css/frontend/schedule.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                '../sass/plugin/bootstrap-datetimepicker.min.css',
                'frontend/schedule.css'
            ], 'public/css/schedule.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/moment.min.js',
                'plugin/bootstrap-datetimepicker.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/nhl/schedule.js'
            ], 'public/js/schedule.js')
            //Messages page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/new/messages.scss'
            ], 'resources/assets/css/frontend/messages.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                '../sass/plugin/custom-bootstrap.css',
                '../sass/plugin/bootstrap-datetimepicker.min.css',
                'frontend/messages.css'
            ], 'public/css/messages.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'plugin/jqueryUI.js',
                'plugin/bootstrap.min.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/new/messages.js'
            ], 'public/js/messages.js')
            //my_teams page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/my_team.scss'
            ], 'resources/assets/css/frontend/my_team.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                '../sass/plugin/bootstrap-datetimepicker.min.css',
                'frontend/my_team.css'
            ], 'public/css/my_team.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'plugin/moment-timezone-with-data.min.js',
                'plugin/bootstrap-datetimepicker.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool/my_team.js'
            ], 'public/js/my_team.js')
            //basic_user pages
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/basic_user.scss'
            ], 'resources/assets/css/frontend/basic_user.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/basic_user.css'
            ], 'public/css/basic_user.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/basic_user.js'
            ], 'public/js/basic_user.js')
            //login pages
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/user/login.scss'
            ], 'resources/assets/css/frontend/login.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/login.css'
            ], 'public/css/login.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/user/login.js'
            ], 'public/js/login.js')
            //register pages
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/user/register.scss'
            ], 'resources/assets/css/frontend/register.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/register.css'
            ], 'public/css/register.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/user/register.js'
            ], 'public/js/register.js')
            //blog page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/blog/blog.scss'
            ], 'resources/assets/css/frontend/blog.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/blog.css'
            ], 'public/css/blog.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jquery.sharrre.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/blog/blog.js'
            ], 'public/js/blog.js')
            //pool_standing page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/pool_standing.scss'
            ], 'resources/assets/css/frontend/pool_standing.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/pool_standing.css'
            ], 'public/css/pool_standing.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/includes/team_records.js',
                'frontend/includes/player_news.js',
                'frontend/includes/pool_slider.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool/pool_standing.js'
            ], 'public/js/pool_standing.js')
            //player Stats page 
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/player_stats.scss'
            ], 'resources/assets/css/frontend/player_stats.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/player_stats.css'
            ], 'public/css/player_stats.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool/player_stats.js',
                'frontend/pool/player_stats&league_leaders.js'
            ], 'public/js/player_stats.js')
            //League leaders page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/league_leaders.scss'
            ], 'resources/assets/css/frontend/league_leaders.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/league_leaders.css'
            ], 'public/css/league_leaders.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool/league_leaders.js',
                'frontend/pool/player_stats&league_leaders.js'
            ], 'public/js/league_leaders.js')
            //Player transaction page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                '../sass/plugin/bootstrap-datetimepicker.min.css',
                'frontend/new/player_transaction.scss'
            ], 'resources/assets/css/frontend/player_transaction.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/player_transaction.css'
            ], 'public/css/player_transaction.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'plugin/bootstrap-datetimepicker.min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/new/player_transaction.js'
            ], 'public/js/player_transaction.js')
            //Injury report page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/nhl/injury_report.scss'
            ], 'resources/assets/css/frontend/injury_report.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/injury_report.css'
            ], 'public/css/injury_report.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/nhl/injury_report.js'
            ], 'public/js/injury_report.js')
            //Draft results page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/draft_results.scss'
            ], 'resources/assets/css/frontend/draft_results.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/draft_results.css'
            ], 'public/css/draft_results.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool/draft_results.js'
            ], 'public/js/draft_results.js')
            //box_draft
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/box_draft.scss'
            ], 'resources/assets/css/frontend/box_draft.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/box_draft.css'
            ], 'public/css/box_draft.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool/box_draft.js'
            ], 'public/js/box_draft.js')
            //matchup individual page
            .sass([
                '../sass/plugin/simple-switch.css',
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/matchup_individual.scss'
            ], 'resources/assets/css/frontend/matchup_individual.css')
            .styles([
                '../sass/plugin/selectBoxIt.css',
                'frontend/matchup_individual.css'
            ], 'public/css/matchup_individual.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/includes/pool_slider.js',
                'frontend/pool/matchup_individual.js'
            ], 'public/js/matchup_individual.js')
            //My account page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/auth/my_account.scss'
            ], 'resources/assets/css/frontend/my_account.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/my_account.css'
            ], 'public/css/my_account.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/auth/my_account.js'
            ], 'public/js/my_account.js')
            //Add setup page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/add_setup.scss'
            ], 'resources/assets/css/frontend/add_setup.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/add_setup.css'
            ], 'public/css/add_setup.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool/add_setup.js'
            ], 'public/js/add_setup.js')
            //Drop setup page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/add_setup.scss'
            ], 'resources/assets/css/frontend/drop_setup.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/drop_setup.css'
            ], 'public/css/drop_setup.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool/add_setup.js'
            ], 'public/js/drop_setup.js')
            //Proposed trade page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool_teams/trade.scss'
            ], 'resources/assets/css/frontend/trade.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/trade.css'
            ], 'public/css/trade.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool_teams/trade.js'
            ], 'public/js/trade.js')
            //Advertise page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pages/advertise.scss'
            ], 'resources/assets/css/frontend/advertise.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/advertise.css'
            ], 'public/css/advertise.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/pages/advertise.js'
            ], 'public/js/advertise.js')
            //Support page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pages/support.scss'
            ], 'resources/assets/css/frontend/support.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/support.css'
            ], 'public/css/support.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/pages/support.js'
            ], 'public/js/support.js')
            //  Faq page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pages/faq.scss'
            ], 'resources/assets/css/frontend/faq.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/faq.css'
            ], 'public/css/faq.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/bootstrap.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/pages/faq.js'
            ], 'public/js/faq.js')
            //Pool types page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pages/pool_types.scss'
            ], 'resources/assets/css/frontend/pool_types.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/pool_types.css'
            ], 'public/css/pool_types.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/pages/pool_types.js'
            ], 'public/js/pool_types.js')
            //How it works page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pages/how_it_works.scss'
            ], 'resources/assets/css/frontend/how_it_works.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/how_it_works.css'
            ], 'public/css/how_it_works.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/pages/how_it_works.js'
            ], 'public/js/how_it_works.js')
            //Privacy policy page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pages/privacy_policy.scss'
            ], 'resources/assets/css/frontend/privacy_policy.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/privacy_policy.css'
            ], 'public/css/privacy_policy.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/pages/privacy_policy.js'
            ], 'public/js/privacy_policy.js')
            //Terms of use page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pages/terms_of_use.scss'
            ], 'resources/assets/css/frontend/terms_of_use.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/terms_of_use.css'
            ], 'public/css/terms_of_use.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/pages/terms_of_use.js'
            ], 'public/js/terms_of_use.js')
            // NHL Player Stats page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/nhl/nhl_player_stats.scss'
            ], 'resources/assets/css/frontend/nhl_player_stats.css')
            .styles([
                '../sass/plugin/selectBoxIt.css',
                '../sass/plugin/simple-switch.css',
                'frontend/nhl_player_stats.css'
            ], 'public/css/nhl_player_stats.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/nhl/nhl_player_stats.js'
            ], 'public/js/nhl_player_stats.js')
            //Edit account page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/auth/edit_account.scss'
            ], 'resources/assets/css/frontend/edit_account.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/edit_account.css'
            ], 'public/css/edit_account.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/auth/edit_account.js'
            ], 'public/js/edit_account.js')
            //Pool Show Page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/pool_show.scss'
            ], 'resources/assets/css/frontend/pool_show.css')
            .styles([
                '../sass/plugin/selectBoxIt.css',
                'frontend/pool_show.css'
            ], 'public/css/pool_show.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/moment.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool/pool_show.js'
            ], 'public/js/pool_show.js')
            //Team edit page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/edit_team.scss'
            ], 'resources/assets/css/frontend/edit_team.css')
            .styles([
                '../sass/plugin/selectBoxIt.css',
                '../sass/plugin/html5imageupload.css',
                'frontend/edit_team.css'
            ], 'public/css/edit_team.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/moment.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/html5imageupload.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/twitter.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/pool/edit_team.js'
            ], 'public/js/edit_team.js')
            //Payment page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/payment.scss'
            ], 'resources/assets/css/frontend/payment.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                'frontend/payment.css'
            ], 'public/css/payment.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/pool/payment.js'
            ], 'public/js/payment.js')
            //matchup overview page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/pool/matchup_overview.scss'
            ], 'resources/assets/css/frontend/matchup_overview.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/matchup_overview.css'
            ], 'public/css/matchup_overview.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/moment.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/includes/pool_slider.js',
                'frontend/pool/matchup_overview.js'
            ], 'public/js/matchup_overview.js')
            //live-draft page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/live_draft/live_draft.scss'
            ], 'resources/assets/css/frontend/live_draft.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/live_draft.css'
            ], 'public/css/live_draft.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/bootstrap.min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/jquery.dataTables.min.js',
                'plugin/moment.min.js',
                'plugin/moment-timezone-with-data.min.js',
                'plugin/jquery.nicescroll.min.js',
                'plugin/jquery.countdownTimer.min.js',
                'frontend/common.js',
                'frontend/live_draft/livedraft_players.js',
                'frontend/live_draft/livedraft_center_header.js',
                'frontend/live_draft/livedraft_teams.js',
                'frontend/live_draft/livedraft_draft_results.js',
                'frontend/live_draft/livedraft_my_team.js',
                'frontend/live_draft/livedraft_draft_overview.js',
                'frontend/live_draft/livedraft_players_loop.js',
                'frontend/live_draft/livedraft_trash_talk.js',
                'frontend/live_draft/live_draft.js'
            ], 'public/js/live_draft.js')
//            //pre draft page
            .sass([
                'plugin/fontawesome/scss/font-awesome.scss',
                'plugin/sweetalert/sweetalert.scss',
                'frontend/live_draft/pre_draft.scss'
            ], 'resources/assets/css/frontend/pre_draft.css')
            .styles([
                '../sass/plugin/simple-switch.css',
                '../sass/plugin/selectBoxIt.css',
                'frontend/pre_draft.css'
            ], 'public/css/pre_draft.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/underscore-min.js',
                'plugin/bootstrap.min.js',
                'plugin/jqueryUI.js',
                'plugin/jquery.selectBoxIt.min.js',
                'plugin/jquery.dataTables.min.js',
                'plugin/moment.min.js',
                'plugin/moment-timezone-with-data.min.js',
                'plugin/jquery.nicescroll.min.js',
                'frontend/common.js',
                'frontend/scorefeed.js',
                'frontend/includes/pool_nav.js',
                'frontend/includes/pool_name_header.js',
                'frontend/live_draft/pre_draft.js'
            ], 'public/js/pre_draft.js')

            /**
             * Process backend SCSS stylesheets
             */
            .sass([
                'backend/app.scss',
                'plugin/sweetalert/sweetalert.scss',
                'backend/plugin/toastr/toastr.scss'
            ], 'resources/assets/css/backend/app.css')
            .styles([
                '../sass/plugin/html5imageupload.css',
                '../sass/plugin/jquery.dataTables.min.css',
                'backend/app.css'
            ], 'public/css/backend.css')
            .scripts([
                'plugin/sweetalert/sweetalert.min.js',
                'backend/plugin/toastr/toastr.min.js',
                'plugin/html5imageupload.min.js',
                'plugin/jquery.dataTables.min.js',
                'plugins.js',
                'backend/app.js',
                'backend/custom.js'
            ], 'public/js/backend.js')

            /**
             * Apply version control
             */
            .version([
                "public/css/scores_index.css", "public/js/scores_index.js",
                "public/css/game_details.css", "public/js/game_details.js",
                "public/css/pool_creator.css", "public/js/pool_creator.js",
                "public/css/pool_overview.css", "public/js/pool_overview.js",
                "public/css/transactions.css", "public/js/transactions.js",
                "public/css/in_action_tonight.css", "public/js/in_action_tonight.js",
                "public/css/trades.css", "public/js/trades.js",
                "public/css/live_stats.css", "public/js/live_stats.js",
                "public/css/pool_info.css", "public/js/pool_info.js",
                "public/css/standings.css", "public/js/standings.js",
                "public/css/schedule.css", "public/js/schedule.js",
                "public/css/messages.css", "public/js/messages.js",
                "public/css/my_team.css", "public/js/my_team.js",
                "public/css/home_public.css", "public/js/home_public.js",
                "public/css/home_loggedin.css", "public/js/home_loggedin.js",
                "public/css/landing.css", "public/js/landing.js",
                "public/css/basic_user.css", "public/js/basic_user.js",
                "public/css/login.css", "public/js/login.js",
                "public/css/register.css", "public/js/register.js",
                "public/css/blog.css", "public/js/blog.js",
                "public/css/pool_standing.css", "public/js/pool_standing.js",
                "public/css/player_stats.css", "public/js/player_stats.js",
                "public/css/nhl_player_stats.css", "public/js/nhl_player_stats.js",
                "public/css/league_leaders.css", "public/js/league_leaders.js",
                "public/css/player_transaction.css", "public/js/player_transaction.js",
                "public/css/injury_report.css", "public/js/injury_report.js",
                "public/css/draft_results.css", "public/js/draft_results.js",
                "public/css/box_draft.css", "public/js/box_draft.js",
                "public/css/matchup_individual.css", "public/js/matchup_individual.js",
                "public/css/my_account.css", "public/js/my_account.js",
                "public/css/add_setup.css", "public/js/add_setup.js",
                "public/css/add_confirmation.css", "public/js/add_confirmation.js",
                "public/css/drop_setup.css", "public/js/drop_setup.js",
                "public/css/drop_confirmation.css", "public/js/drop_confirmation.js",
                "public/css/trade.css", "public/js/trade.js",
                "public/css/advertise.css", "public/js/advertise.js",
                "public/css/support.css", "public/js/support.js",
                "public/css/faq.css", "public/js/faq.js",
                "public/css/pool_types.css", "public/js/pool_types.js",
                "public/css/how_it_works.css", "public/js/how_it_works.js",
                "public/css/privacy_policy.css", "public/js/privacy_policy.js",
                "public/css/terms_of_use.css", "public/js/terms_of_use.js",
                "public/css/player.css", "public/js/player.js",
                "public/css/edit_account.css", "public/js/edit_account.js",
                "public/css/pool_show.css", "public/js/pool_show.js",
                "public/css/edit_team.css", "public/js/edit_team.js",
                "public/css/matchup_overview.css", "public/js/matchup_overview.js",
                "public/css/live_draft.css", "public/js/live_draft.js",
                "public/css/pre_draft.css", "public/js/pre_draft.js",
                "public/css/payment.css", "public/js/payment.js",
                "public/css/backend.css", "public/js/backend.js"
            ]);
}); 