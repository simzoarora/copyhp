$(document).ready(function () {
    //END Arranging height of left and sidebar 
    if ($(window).width() > 768) {
        hpApp.arrangeLeftHeight($('.account-content'));

        //loader
        var checkHeight = setInterval(function () {
            if ($('.upcoming-tweets').height() >= '180') {
                //Arranging height of left and sidebar 
                setTimeout(function () {
                    hpApp.arrangeLeftHeight($('.account-content'));
                }, 50);
                clearInterval(checkHeight);
            }
        }, 200);
        //END loader
    }
    //Right sidebar width calc from right 
    hpApp.arrangeRightSidebar();
    $(window).resize(function () {
        hpApp.arrangeRightSidebar();

        if ($(window).width() > 768) {
            hpApp.arrangeLeftHeight($('.account-content'));
        }
    });
    //END Right sidebar width calc from right 
    //Edit account submit form
    $('#account-info-form').on('submit', function (e) {
        var $this = $(this);
        //disabling submit button
        $this.find('.submit-btn').attr('disabled', 'disabled').addClass('active').find('span').text(buttons_general.submitting);
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (response) {
                //removing all error classes
                $this.find('.has-error').removeClass('has-error');
                $this.find('.error-msg').remove();

                $this.find('.submit-btn').removeAttr('disabled').removeClass('active').find('span').text(buttons_general.submit);
                $this.find('#alert-box').addClass('success').text(response.message);

                setTimeout(function () {
                    $this.find('#alert-box').removeClass('success').text('');
                }, 3000);
            },
            error: function (error) {
                var formSel = $('#account-info-form');
                formSel.find('.submit-btn').removeAttr('disabled').removeClass('active').find('span').text(buttons_general.submit);
                hpApp.ajaxInputError(error, formSel);
            }
        });
        e.preventDefault();
    });
    //END Edit account submit form
    //Change password submit form
    $('#change-password-form').on('submit', function (e) {
        var $this = $(this);
        //disabling submit button
        $this.find('.submit-btn').attr('disabled', 'disabled').addClass('active').find('span').text(buttons_general.submitting);
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (response) {
                //removing all error classes
                $this.find('.has-error').removeClass('has-error');
                $this.find('.error-msg').remove();

                $this.find('.submit-btn').removeAttr('disabled').removeClass('active').find('span').text(buttons_general.submit);
                $this.find('[type="text"], [type="email"], [type="password"]').val('');
                $this.find('#alert-box').addClass('success').text(response.message);

                setTimeout(function () {
                    $this.find('#alert-box').removeClass('success').text('');
                }, 3000);
            },
            error: function (error) {
                var formSel = $('#change-password-form');
                formSel.find('.submit-btn').removeAttr('disabled').removeClass('active').find('span').text(buttons_general.submit);
                hpApp.ajaxInputError(error, formSel);
            }
        });
        e.preventDefault();
    });
    //END Change password submit form
    //Email Notification submit form
    $('#email-settings-form').on('submit', function (e) {
        var $this = $(this);
        //disabling submit button
        $this.find('.submit-btn').attr('disabled', 'disabled').addClass('active').find('span').text(buttons_general.submitting);
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (response) {
                //removing all error classes
                $this.find('.has-error').removeClass('has-error');
                $this.find('.error-msg').remove();

                $this.find('.submit-btn').removeAttr('disabled').removeClass('active').find('span').text(buttons_general.submit);
                $this.find('#alert-box').addClass('success').text(response.message);

                setTimeout(function () {
                    $this.find('#alert-box').removeClass('success').text('');
                }, 3000);
            },
            error: function (error) {
                var formSel = $('#change-password-form');
                formSel.find('.submit-btn').removeAttr('disabled').removeClass('active').find('span').text(buttons_general.submit);
                hpApp.ajaxInputError(error, formSel);
            }
        });
        e.preventDefault();
    });
    //END Email Notification submit form

});