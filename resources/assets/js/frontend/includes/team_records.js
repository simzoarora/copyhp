$(document).ready(function () {
    $.ajax({
        url: $('#team-records').data('url'),
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            if (!$.isEmptyObject(response)) {
                var teamRecordsTemplate = $('#team-records-template').html();
                teamRecordsTemplate = _.template(teamRecordsTemplate);
                var team_records_data = {
                    records: response
                };
                $('#team-records-all').append(teamRecordsTemplate(team_records_data));
            } else {
                $('#team-records-all').append('<tr class="no-result-present"><td colspan="2">'+team_records.messages.no_team_records+'</td></tr>');
            } 
        },
        error: function (error) {
            $('#team-records-all').append('<tr class="no-result-present"><td colspan="2">'+team_records.messages.no_team_records+'</td></tr>');
        }
    });
})


