//slider
function animateSlider() {
    var slideWidth = $('.matchup-individual-slider .matchup-teams').outerWidth(),
            slideHeight = $('.matchup-individual-slider .matchup-teams').outerHeight(),
            totlSlides = $('.matchup-individual-slider .matchup-teams').length,
            totlWidth = slideWidth * totlSlides,
            sliderWidth = $('.slider').width(totlWidth),
            noAnimate = totlWidth - ($('.matchup-individual-slider').outerWidth()) + 46,
            moveSlide = 0;
    $('.slider--left-arrow .graphics').on('click', function () {
        //check if animated
        if ($('.matchup-individual-slider .slider').is(':animated')) {
            //do nothing
            $(this).stop();
        }
//check if it is first-slide
        else if ($(".slider").css("marginLeft") === '50px') {
//do nothing
        } else {
            $('.matchup-individual-slider .slider').animate({
                marginLeft: moveSlide + slideWidth + 50
            });
            moveSlide = moveSlide + slideWidth;
        }
    });
    $('.slider--right-arrow .graphics').on('click', function () {
//check if animated
        if ($('.matchup-individual-slider .slider').is(':animated')) {
            $(this).stop();
        }
//check if it is last-slide
        else if ($(".slider").css("marginLeft") === (-noAnimate + 'px')) {
//do nothing
        } else {
            $('.matchup-individual-slider .slider').animate({
                marginLeft: moveSlide - slideWidth + 50
            });
            moveSlide = moveSlide - slideWidth;
        }
    });
    $('#slider-all .team-name').each(function () {
        var $this = $(this);
        if ($this.text().length > 12) {
            var res = $this.text().substring(0, 12) + '..';
            $this.text(res);
        }
    });
}

function getSliderData(url) {
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (resp) {
            if (resp.pool_matchups.length > 0) {
                var slider_template = $('#slider-template').html();
                slider_template = _.template(slider_template);
                $('#slider-all').append(slider_template({pool_matchups: resp.pool_matchups}));
                animateSlider();

                //adding width to week name divs
                $.each(weekCountArr, function (i, v) {
                    var weekName = i.replace(' ', '-'),
                            weekWidth = (parseInt(v) * parseInt(179)) - parseInt(1);
                    $('.matchup-weeks.' + weekName).width(weekWidth);
                });
            } else {
                $('#slider-all').hide(); 
            }
        },
        error: function (err) {
            $('#slider-all').hide();
        }
    });
}
getSliderData(slider_url);
//end slider

