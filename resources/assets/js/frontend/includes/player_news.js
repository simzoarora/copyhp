$(document).ready(function () {
    $('#player_news_tabs li').on('click', function () {
        var $this = $(this);
        $this.addClass('active').siblings().removeClass('active');
        $('#' + $this.data('tab')).show().siblings().hide();
//        readmore();
    });
    $.ajax({
        url: $('#player_news_tabs').data('url'),
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            if (!$.isEmptyObject(response)) {
                var playerNewsTemplate = $('#player-news-template').html();
                playerNewsTemplate = _.template(playerNewsTemplate);
                $.each(response, function (index, injury) {
                    var player_news_data = {
                        injury: injury
                    };
                    $('#league').append(playerNewsTemplate(player_news_data));
                    if(injury.player_season != null) { 
                        if (injury.player_season.pool_team_player != null) {
                            $('#myteam').append(playerNewsTemplate(player_news_data));
                        }
                    }
                });
                readmore();
                //checking if team div or league div is empty
                if (hpApp.isElementEmpty($('#myteam'))) {
                    $('#myteam').append('<p class="no-result-present">'+player_news.messages.no_team_news+'</p>');
                }
                if (hpApp.isElementEmpty($('#league'))) {
                    $('#league').append('<p class="no-result-present">'+player_news.messages.no_league_news+'</p>');
                }
            } else {
                $('#league').append('<p class="no-result-present">'+player_news.messages.no_league_news+'</p>');
                $('#myteam').append('<p class="no-result-present">'+player_news.messages.no_team_news+'</p>');
            }
        },
        error: function (err) {
            $('#league').append('<p class="no-result-present">'+player_news.messages.no_league_news+'</p>');
            $('#myteam').append('<p class="no-result-present">'+player_news.messages.no_team_news+'</p>');
        }
    });
    function readmore() {
        $('.news-main').each(function () {
            var $this = $(this).find('p');
            var words = $this.text().length;
            if (words.length > 16) {
                $this.find('.init-shown').text($this.text().split(/\s+/).slice(0, 16).join(" ") + "... ");
                        $this.append('<a class="read-more">'+player_news.read_more+'</a>');
                $this.addClass('readmore');
//                $(this).parent().find('a').text(player_news.read_more);
            } else {
//                $(this).parent().find('a').text('');
            }
        });
    }
    $('.player-news').on('click', '.read-more', function (e) {
        e.preventDefault();
        var $this = $(this);
        $this.closest('.news-single').siblings().find('p').addClass('readmore');
        $this.closest('p').toggleClass('readmore');
        $this.closest('.news-single').siblings().find('a').text(player_news.read_more);
        if ($this.text() == player_news.read_more) {
            $this.text(player_news.less);
        } else {
            $this.text(player_news.read_more);
        }
        $this.parent().prev().find('p').toggleClass('readmore');
    });

});
