$(document).ready(function () {
    //getting data
    $.ajax({
        url: poolHeaderUrl,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            //Creating pool list
            if (!$.isEmptyObject(response.pool_teams)) {
                var poolTeams = [];
                $.each(response.pool_teams, function (i, v) {
                    poolTeams.push({
                        'id': v.id,
                        'value': v.name
                    });
                });
            }
            //END Creating pool list

            //Calculating current team
            if (!$.isEmptyObject(response.pool_team)) {
                var currentTeamId = response.pool_team.id;
            }
            //END Calculating current team

            //checking which pool type/cat is this
            if (response.pool_type == poolTypes.h2h) {
                if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                    var poolToShow = 11;
                } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly) {
                    var poolToShow = 12;
                } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly_daily) {
                    var poolToShow = 13;
                }
            } else if (response.pool_type == poolTypes.box) {
                var poolToShow = 2;
            } else if (response.pool_type == poolTypes.standard) {
                if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_standard) {
                    var poolToShow = 31;
                } else if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                    var poolToShow = 32;
                }
            }
            //END checking which pool type/cat is this

            //Append left header data
            if (!$.isEmptyObject(response.pool_matchup)) {
                if (currentTeamId == response.pool_matchup.pool_team1.id) {
                    var currentTeam = response.pool_matchup.pool_team1;
                } else {
                    var currentTeam = response.pool_matchup.pool_team2;
                }
            } else {
                if (!$.isEmptyObject(response.pool_team)) {
                    var currentTeam = response.pool_team;
                } else {
                    //opening other team
                    var currentTeam = response.pool_team_first;
                }
            }
            $('#pool-left-header-outer').empty();
            if (!$.isEmptyObject(response)) {
                var poolLeftHeaderOuter = $('#pool-left-header-outer'),
                        poolLeftHeaderTemplate = $('#pool-left-header-template').html();
                poolLeftHeaderTemplate = _.template(poolLeftHeaderTemplate);
                var poolLeftHeaderData = {
                    siteLink: api_url,
                    poolTeams: poolTeams,
                    currentTeam: currentTeam,
                    response: response,
                    poolToShow: poolToShow
                };
                poolLeftHeaderOuter.append(poolLeftHeaderTemplate(poolLeftHeaderData));
                $('.pool-name-header .pool-name .details select').val(currentTeam.id).selectBoxIt();
            }
            //END Append left header data

            if (poolToShow == 11) {
                var poolRightHeaderOuter = $('#pool-right-header-outer'),
                        poolRightHeaderTemplate = $('#pool-right-header-template').html();
                poolRightHeaderTemplate = _.template(poolRightHeaderTemplate);
                var poolRightHeaderData = {
                    poolMatchup: response.pool_matchup,
                    poolScoreSettings: response.pool_score_settings,
                    siteLink: api_url
                };
                poolRightHeaderOuter.append(poolRightHeaderTemplate(poolRightHeaderData));
                //bottom data width
                hpApp.h2hStatsSetting();
                //bottom data colors 
                if (response.pool_matchup.team1 == currentTeamId || response.pool_matchup.team2 == currentTeamId) {
                    $.each(response.pool_matchup.cat_result, function (i, v) {
                        if (v.tie == true) {
                            $('#score-setting-' + response.pool_matchup.id + '-' + i).addClass('blue');//means grey color
                        } else {
                            if (v.win == currentTeamId) {
                                $('#score-setting-' + response.pool_matchup.id + '-' + i).addClass('green');
                            } else {
                                $('#score-setting-' + response.pool_matchup.id + '-' + i).addClass('red');
                            }
                        }
                    });
                }
            } else if (poolToShow == 2 || poolToShow == 31) {
                var poolRightHeaderOuter = $('#pool-right-header-outer'),
                        poolRightHeaderTemplate = $('#pool-right-header-box-template').html();
                poolRightHeaderTemplate = _.template(poolRightHeaderTemplate);
                var poolRightHeaderData = {
                    poolTeamFirst: response.pool_team_first,
                    poolScoreSettings: response.pool_score_settings,
                    siteLink: api_url
                };
                poolRightHeaderOuter.append(poolRightHeaderTemplate(poolRightHeaderData));

                $.each(response.pool_team_first.pool_standard_scores, function (i, v) {
                    $('#score-setting-' + response.pool_team_first.id + '-' + v.pool_score_setting.pool_scoring_field_id + '-' + v.pool_score_setting.type).text(hpApp.twoDecimal(v.total_score));
                });
                hpApp.boxStandardStatsSetting();

                if (!$.isEmptyObject(response.pool_team)) {
                    if (poolToShow == 2 && $.isEmptyObject(response.pool_team.pool_team_players)) {
                        var draftTeamAlertTemplate = $('#draft-team-alert-template').html();
                        draftTeamAlertTemplate = _.template(draftTeamAlertTemplate);

                        var boxDraftLink = $('#pool-boxdraft-link').attr('href'),
                                newBoxDraftLink = boxDraftLink.replace('draftLink', response.id);

                        var draftTeamAlertData = {
                            boxDraftLink: newBoxDraftLink
                        };
                        $('#draft-team-alert-outer').append(draftTeamAlertTemplate(draftTeamAlertData));
                    }
                }
            } else if (poolToShow == 32) {
                var poolRightHeaderOuter = $('#pool-right-header-outer'),
                        poolRightHeaderTemplate = $('#pool-right-header-box-template').html();
                poolRightHeaderTemplate = _.template(poolRightHeaderTemplate);
                var poolRightHeaderData = {
                    poolTeamFirst: response.pool_team_first,
                    poolScoreSettings: response.pool_score_settings,
                    siteLink: api_url
                };
                poolRightHeaderOuter.append(poolRightHeaderTemplate(poolRightHeaderData));

                $.each(response.pool_team_first.pool_standard_rotto_results, function (i, v) {
                    var text = parseFloat(v.value),
                            text = text.toFixed(2);
                    $('#score-setting-' + response.pool_team_first.id + '-' + v.pool_scoring_field_id).text(text);
                });
                hpApp.boxStandardStatsSetting();
            } else if (poolToShow == 12 || poolToShow == 13) {
                var poolRightHeaderOuter = $('#pool-right-header-outer'),
                        poolRightHeaderTemplate = $('#pool-right-header-template').html();
                poolRightHeaderTemplate = _.template(poolRightHeaderTemplate);
                var poolRightHeaderData = {
                    poolMatchup: response.pool_matchup,
                    siteLink: api_url
                };
                poolRightHeaderOuter.append(poolRightHeaderTemplate(poolRightHeaderData));
            }
            //adding payment warning
            if (response.active_till != null && dashboardPage == true) {
                var specialTo = moment(new Date(response.active_till));
                if (moment().diff(specialTo, 'days') < 0) {
                    var paymentWarningOuter = $('#payment-warning-outer'),
                            paymentWarningTemplate = $('#payment-warning-template').html();
                    paymentWarningTemplate = _.template(paymentWarningTemplate);
                    var paymentWarningData = {
                        activeTill: moment(response.active_till).fromNow('dd'),
                        paymentLink: poolPaymentUrl,
                        poolComplete: response.is_completed,
                        poolToShow: poolToShow,
                        poolTeamPlayers: response.pool_teams
                    };
                    paymentWarningOuter.append(paymentWarningTemplate(paymentWarningData));
                }
            }
            //Appending add/trade/drop buttons
            var poolActionSel = $('#pool-action-template');
            if (poolActionSel.length > 0) {
                var poolActionTemplate = poolActionSel.html();
                poolActionTemplate = _.template(poolActionTemplate);
                var poolActionData = {
                    poolToShow: poolToShow
                };
                $('#pool-action-outer').append(poolActionTemplate(poolActionData));
            }
        },
        error: function (error) {
            hpApp.ajaxSwalError(error);
        }
    });
    //END getting data
    //My team select event handler
    $(document).on('change', '.team-select', function () {
        var $this = $(this),
                teamId = $this.val(),
                newMyTeamUrl = myTeamWithTeamUrl.replace(0, teamId);
        location.href = newMyTeamUrl;
    });
    //END My team select event handler

    $(window).resize(function () {
        //Setting widths of h2h pools
        hpApp.h2hStatsSetting();
        //Setting widths of box and standard pools
        hpApp.boxStandardStatsSetting();
    });
});