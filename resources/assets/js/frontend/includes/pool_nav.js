$(document).ready(function () {
    $('#open-navbar2').on('click', function () {
        var $this = $(this);
        $this.parent().find('.main-ul').toggleClass('show');
        $this.find('.crossbtn').toggleClass('show');
        $('.bg-trans').toggleClass('show');
    });
    $('.main-ul>li').on('click', function () {
        $(this).toggleClass('clicked').siblings().removeClass('clicked');
    });
    $('.select-field').selectBoxIt();

    $('#pool_nav>li').on('click', function () {
        $(this).siblings().removeClass('clicked-active');
        $(this).toggleClass('clicked-active');
    });

    $('.sub-links li').on('click', function () {
        $(this).siblings().find('a').removeClass('active');
        $('a', this).addClass('active');
    });
    //My team select event handler
    $(document).on('change', '.pool-list-select', function () {
        var $this = $(this),
                poolId = $this.val(),
                newPoolDashboardUrl = poolDashboardUrl.replace(0, poolId);
        location.href = newPoolDashboardUrl;
    });
    //END My team select event handler

//on Document click remove class clicked-active
    $(document).mouseup(function (e) {
        var container = $('#pool_nav');
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.find('li').removeClass('clicked-active');
        }
    });
});


 