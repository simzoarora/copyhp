var draftResultDataArr;
$(document).ready(function () {
    //reinitiate scroll on draft result tab click
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if ($(e.target).attr("href") == '#draft-results') {
            $('#draft-results-outer').niceScroll({
                cursorcolor: "#0F6AAE",
                cursoropacitymax: 1,
                cursorwidth: 7,
                cursorborder: 0,
                autohidemode: false
            });
        } else {
            $('#draft-results-outer').getNiceScroll().remove();
        }
        if ($(e.target).attr("href") == '#players') {
            $('#player-table-outer').niceScroll({
                cursorcolor: "#0F6AAE",
                cursoropacitymax: 1,
                cursorwidth: 7,
                cursorborder: 0,
                autohidemode: false
            });
        } else {
            $('#player-table-outer').getNiceScroll().remove();
        }
    });
    //END
    //Get draft result data
    hpApp.appendDraftPlayerData = function (response) {
        $('#draft-results-list-all').empty();
        if (!$.isEmptyObject(response)) {
            var draftResultsListAppend = $('#draft-results-list-all'),
                    draftResultsListTemplate = $('#draft-results-list-template').html();
            draftResultsListTemplate = _.template(draftResultsListTemplate);
            var draftResultsListData = {
                drafts: response
            };
            draftResultsListAppend.append(draftResultsListTemplate(draftResultsListData));
            draftResultsListAppend.find('.table').DataTable({
                "paging": false,
                "info": false,
                "searching": false
            });

            //draft overview append 
            var roundsListAppend = $('#rounds-list-all'),
                    roundsListTemplate = $('#rounds-list-template').html();
            roundsListTemplate = _.template(roundsListTemplate);
            var roundsListData = {
                drafts: response
            };
            roundsListAppend.empty();
            roundsListAppend.append(roundsListTemplate(roundsListData));
            $('#rounds-list-all').getNiceScroll().resize();
            //END
        } else {
            $('#draft-results-list-all').append('<div class="no-drafts">No Draft result Found.</div>');
        }
    };
    $.ajax({
        url: getDraftResultUrl,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            draftResultDataArr = response;
            hpApp.appendDraftPlayerData(response);
            hpApp.loaderRemove();
            //draft timing get data  
            if (!$.isEmptyObject(liveDraftSetting)) {
                var liveDraftMoment = moment.tz(liveDraftSetting.date + ' ' + liveDraftSetting.time,moment.ISO_8601,liveDraftSetting.time_zone),
            currentMomentNew = moment().tz(liveDraftSetting.time_zone);
                if (liveDraftMoment.diff(currentMomentNew) > 0 && liveDraftMoment.diff(currentMomentNew) < 900000) { //check if livedraft is less than 15 mins to start
                    var timerMilliseconds = liveDraftMoment.diff(currentMomentNew),
                    timermins = hpApp.millisToMinutesAndSeconds(timerMilliseconds);
                    $('#round-timer').parent().css({'display': 'table'});
                    $('#round-timer').countdowntimer({
                        minutes: timermins.minutes,
                        seconds: timermins.seconds,
                        size: "lg"
                    });
                    hpApp.countdownTimeUpFifteen(timerMilliseconds);
                    $('#round-details').hide();
                    $("#till-draft-begins").show();
                    $('#rounds-list-all').css({'height': 'calc(100vh - 222px)'});
                } else if (liveDraftMoment.diff(currentMomentNew) < 0) { //checking the live draft is started or not --checking the livedraft moment is bigger than current moment
                    hpApp.getDraftTiming();
                } else {
                    $('#rounds-list-all').css({'height': 'calc(100vh - 133px)'});
                }
            } else {
                $('#rounds-list-all').css({'height': 'calc(100vh - 133px)'});
            }
        },
        error: function (err) {
            hpApp.loaderRemove();
            $('#draft-results-list-all').append('<div class="no-drafts">No Draft Result Found.</div>');
        }
    });
    //END

    //datatable on resizing
    $(window).resize(function () {
        if ($(window).width() < 768) {
            $('#draft-results-list-all table').DataTable().destroy();
            $('#draft-results-list-all table th').width('auto');
        }
//        } else { //giving alert box on resize
//            $('#draft-results-list-all table').DataTable({
//                "paging": false,
//                "info": false,
//                "searching": false
//            });
//        }
    });
    //END
});