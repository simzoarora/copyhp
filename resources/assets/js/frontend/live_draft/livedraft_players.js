$(document).ready(function () {
    hpApp.getPlayerStats = function (getUrl) {
        $.ajax({
            url: getPlayerStatsUrl + getUrl,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                $('#player-stats-all').empty();
                if (!$.isEmptyObject(response.data)) {
                    var appendlocation = $('#player-stats-all'),
                            player_stats_template = $('#player-stats-template').html();
                    player_stats_template = _.template(player_stats_template);
                    var player_data = {
                        players: response.data
                    };
                    appendlocation.append(player_stats_template(player_data));
                    $('#player-table-outer').getNiceScroll().resize();
                    hpApp.playerSearchPaginate(response.current_page, response.last_page);

                    hpApp.loaderRemove();
                } else {
                    $('#player-stats-all').append('<tr class="no-result-present"><td colspan="' + $('#player-table-outer table thead tr').find('th').length + '">' + playerStatsMessages.messages.no_results_found + '</td></tr>');
                }
            },
            error: function (err) {
                $('#player-stats-all').empty().append('<tr class="no-result-present"><td colspan="' + $('#player-table-outer table thead tr').find('th').length + '">' + playerStatsMessages.messages.no_results_found + '</td></tr>');
                hpApp.loaderRemove();
            }
        });
    }
    hpApp.playerSearchPaginate = function (current_page, last_page) {
        var paginateDiv = $('#page-list');
        if (last_page > 0) {
            paginateDiv.empty();
            $('.post-pagination').show();
            var single_anchor_width = 30;
            for (var i = 1; i <= last_page; i++) {
                if (i == current_page) {
                    paginateDiv.append('<a class="active" data-page=' + i + '>' + i + '</a>');
                } else {
                    paginateDiv.append('<a data-page=' + i + '>' + i + '</a>');
                }
            }
            hpApp.paginationAnimate(current_page, false);
        } else {
            $('.post-pagination').hide();
        }
    }
    hpApp.paginationAnimate = function (activePage, status) {
        var pageList = $('#page-list');
        var lastPage = parseInt(pageList.find('a:last-child').data('page'));
        pageList.find('[data-page=' + activePage + ']').addClass('active').siblings().removeClass('active');
        if (activePage >= 3) {
            pageList.animate({marginLeft: -(activePage - 3) * 30}, 300);
        }
        if (activePage == 2) {
            pageList.animate({marginLeft: 0}, 300);
        }
        if (activePage != 1) {
            $('#pagination-left').removeClass('opacity0');
            $('#pagination-first').removeClass('opacity0');
        }
        if (lastPage <= 5) {
            $('.main-pagination').outerWidth(30 * lastPage);
            $('#pagination-right').addClass('opacity0');
            $('#pagination-last').addClass('opacity0');
        } else {
            $('#pagination-right').removeClass('opacity0');
            $('#pagination-last').removeClass('opacity0');
            $('.main-pagination').outerWidth(160);
        }
        if (activePage == lastPage) {
            $('#pagination-right').addClass('opacity0');
            $('#pagination-last').addClass('opacity0');
        }
        if (activePage == 1) {
            pageList.animate({marginLeft: 0}, 300);
            $('#pagination-left').addClass('opacity0');
            $('#pagination-first').addClass('opacity0');
        }
        if (status) {
            var searchInput = $('#search-player-stats,#search-player-stats-mob').val();
            hpApp.getPlayerStatsMakeUrl(searchInput, activePage);
        }
    }
    hpApp.getPlayerStatsMakeUrl = function (searchInput, activePage, statOrderKey, statOrderType) {
        if (searchInput.length > 0) {
            var getUrl = '?data_retrieval=season&season_id=' + currentSeasonId + '&player_name=' + searchInput + '&live_draft=1&paginate=50&live_draft_search_player=1';
        } else {
            var getUrl = '?data_retrieval=season&season_id=' + currentSeasonId + '&live_draft=1&paginate=50&live_draft_search_player=1';
        }

        var teamVal = $('#team').val(),
                positionVal = $('#position').val();

        if (typeof teamVal != 'undefined' && teamVal != "0") {
            getUrl = getUrl + "&nhl_team=" + teamVal;
        }
        if (typeof positionVal != 'undefined' && positionVal != "0") {
            getUrl = getUrl + "&player_position_id=" + positionVal;
        }
        if (typeof activePage != 'undefined' && activePage != '') {
            getUrl = getUrl + "&page=" + activePage;
        }
        if (typeof statOrderKey != 'undefined' && statOrderKey != '') {
            getUrl = getUrl + "&order_type=" + statOrderType + "&order=stat&stat_order_key=" + statOrderKey;
        }
        hpApp.getPlayerStats(getUrl);
    }
    //END
    //Change css of all select boxes
    $('.select-field').selectBoxIt();
    //player search container scroll
    $('#player-table-outer').niceScroll({
        cursorcolor: "#0F6AAE",
        cursoropacitymax: 1,
        cursorwidth: 7,
        cursorborder: 0,
        autohidemode: false
    });
    //END
    //Search players and get stats
    $(document).on('keyup', '#search-player-stats,#search-player-stats-mob', function () {
        var $this = $(this),
                searchInput = $this.val();
        hpApp.getPlayerStatsMakeUrl(searchInput);
    });
    //END 
    //Reset Player search form
    $(document).on('click', '#search-player-reset', function (e) {
        $('#search-player-stats,#search-player-stats-mob').val('');
        //team select set value 0
        var teamSelect = $("#team");
        teamSelect.data("selectBox-selectBoxIt").selectOption("0");
        //position select set value 0
        var positionSelect = $("#position");
        positionSelect.data("selectBox-selectBoxIt").selectOption("0");

        //reset the results
        hpApp.getPlayerStatsMakeUrl('');

        e.preventDefault();
    });
    //END
    //All pagination event handlers
    $('#pagination-right').on('click', function () {
        var activePage = parseInt($('#page-list').find('.active').data('page'));
        hpApp.paginationAnimate(activePage + 1, true);
    });
    $('#pagination-left').on('click', function () {
        var activePage = parseInt($('#page-list').find('.active').data('page'));
        hpApp.paginationAnimate(activePage - 1, true);
    });
    $('#pagination-first').on('click', function () {
        hpApp.paginationAnimate(1, true);
    });
    $('#pagination-last').on('click', function () {
        hpApp.paginationAnimate(parseInt($('#page-list').find('a:last-child').data('page')), true);
    });
    $('.post-pagination').on('click', 'a', function () {
        var activePage = parseInt($(this).data('page'));
        hpApp.paginationAnimate(activePage, true);
    });
    //END
    //When page open search data with no player name
    hpApp.getPlayerStats('?data_retrieval=season&season_id=' + currentSeasonId + '&live_draft=1&paginate=50&live_draft_search_player=1');
    //END
    //Position and team select event handler
    $(document).on('change', '#position,#team', function () {
        hpApp.getPlayerStatsMakeUrl($('#search-player-stats,#search-player-stats-mob').val());
    });
    //END
    //Player stats data sorting
    $(document).on('click', '.player-stats-sort-th', function () {
        var $this = $(this),
                statOrderKey = $this.attr('data-stat_order_key'),
                statOrderType = $this.attr('data-order_type');
        //chnaging to new order type
        if (statOrderType == 'desc') {
            statOrderType = 'asc';
            $this.attr('data-order_type', statOrderType);
        } else {
            statOrderType = 'desc';
            $this.attr('data-order_type', statOrderType);
        }

        if (statOrderKey != '') {
            hpApp.getPlayerStatsMakeUrl($('#search-player-stats,#search-player-stats-mob').val(), '', statOrderKey, statOrderType);
        }
    });
    //END
}); 