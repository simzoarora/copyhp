//Player search
$(document).ready(function () {
    hpApp.loaderRemove = function () {
        currentAjaxCount = currentAjaxCount + 1;
        if (currentAjaxCount == maxAjaxCount) {
            $('.scorer-loader-logo').fadeOut();
        }
    };
    hpApp.getPlayerStats = function (getUrl) {
        $.ajax({
            url: getPlayerStatsUrl + getUrl,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                $('#player-stats-all').empty();
                if (!$.isEmptyObject(response.data)) {
                    var appendlocation = $('#player-stats-all'),
                            player_stats_template = $('#player-stats-template').html();
                    player_stats_template = _.template(player_stats_template);
                    var player_data = {
                        players: response.data
                    };
                    appendlocation.append(player_stats_template(player_data));
                    $('#player-table-outer').getNiceScroll().resize();
                    hpApp.playerSearchPaginate(response.current_page, response.last_page);

                    hpApp.loaderRemove();
                } else {
                    $('#player-stats-all').append('<tr class="no-result-present"><td colspan="' + $('#player-table-outer table thead tr').find('th').length + '">' + playerStatsMessages.messages.no_results_found + '</td></tr>');
                }
            },
            error: function (err) {
                $('#player-stats-all').empty().append('<tr class="no-result-present"><td colspan="' + $('#player-table-outer table thead tr').find('th').length + '">' + playerStatsMessages.messages.no_results_found + '</td></tr>');
                hpApp.loaderRemove();
            }
        });
    }
    hpApp.playerSearchPaginate = function (current_page, last_page) {
        var paginateDiv = $('#page-list');
        if (last_page > 0) {
            paginateDiv.empty();
            $('.post-pagination').show();
            var single_anchor_width = 30;
            for (var i = 1; i <= last_page; i++) {
                if (i == current_page) {
                    paginateDiv.append('<a class="active" data-page=' + i + '>' + i + '</a>');
                } else {
                    paginateDiv.append('<a data-page=' + i + '>' + i + '</a>');
                }
            }
            hpApp.paginationAnimate(current_page, false);
        } else {
            $('.post-pagination').hide();
        }
    }
    hpApp.paginationAnimate = function (activePage, status) {
        var pageList = $('#page-list');
        var lastPage = parseInt(pageList.find('a:last-child').data('page'));
        pageList.find('[data-page=' + activePage + ']').addClass('active').siblings().removeClass('active');
        if (activePage >= 3) {
            pageList.animate({marginLeft: -(activePage - 3) * 30}, 300);
        }
        if (activePage == 2) {
            pageList.animate({marginLeft: 0}, 300);
        }
        if (activePage != 1) {
            $('#pagination-left').removeClass('opacity0');
            $('#pagination-first').removeClass('opacity0');
        }
        if (lastPage <= 5) {
            $('.main-pagination').outerWidth(30 * lastPage);
            $('#pagination-right').addClass('opacity0');
            $('#pagination-last').addClass('opacity0');
        } else {
            $('#pagination-right').removeClass('opacity0');
            $('#pagination-last').removeClass('opacity0');
            $('.main-pagination').outerWidth(160);
        }
        if (activePage == lastPage) {
            $('#pagination-right').addClass('opacity0');
            $('#pagination-last').addClass('opacity0');
        }
        if (activePage == 1) {
            pageList.animate({marginLeft: 0}, 300);
            $('#pagination-left').addClass('opacity0');
            $('#pagination-first').addClass('opacity0');
        }
        if (status) {
            var searchInput = $('#search-player-stats,#search-player-stats-mob').val();
            hpApp.getPlayerStatsMakeUrl(searchInput, activePage);
        }
    }
    hpApp.getPlayerStatsMakeUrl = function (searchInput, activePage, statOrderKey, statOrderType) {
        if (searchInput.length > 0) {
            var getUrl = '?data_retrieval=season&season_id=' + currentSeasonId + '&player_name=' + searchInput + '&live_draft=1&paginate=50&live_draft_search_player=1';
        } else {
            var getUrl = '?data_retrieval=season&season_id=' + currentSeasonId + '&live_draft=1&paginate=50&live_draft_search_player=1';
        }

        var teamVal = $('#team').val(),
                positionVal = $('#position').val();

        if (typeof teamVal != 'undefined' && teamVal != "0") {
            getUrl = getUrl + "&nhl_team=" + teamVal; 
        }
        if (typeof positionVal != 'undefined' && positionVal != "0") {
            getUrl = getUrl + "&player_position_id=" + positionVal;
        }
        if (typeof activePage != 'undefined' && activePage != '') {
            getUrl = getUrl + "&page=" + activePage;
        }
        if (typeof statOrderKey != 'undefined' && statOrderKey != '') {
            getUrl = getUrl + "&order_type=" + statOrderType + "&order=stat&stat_order_key=" + statOrderKey;
        }
        hpApp.getPlayerStats(getUrl);
    }
    //END
    //Change css of all select boxes
    $('.select-field').selectBoxIt();
    //player search container scroll
    $('#player-table-outer').niceScroll({
        cursorcolor: "#0F6AAE",
        cursoropacitymax: 1,
        cursorwidth: 7,
        cursorborder: 0,
        autohidemode: false
    });
    //END
    //Search players and get stats
    $(document).on('keyup', '#search-player-stats,#search-player-stats-mob', function () {
        var $this = $(this),
                searchInput = $this.val();
        hpApp.getPlayerStatsMakeUrl(searchInput);
    });
    //END 
    //Reset Player search form
    $(document).on('click', '#search-player-reset', function (e) {
        $('#search-player-stats,#search-player-stats-mob').val('');
        //team select set value 0
        var teamSelect = $("#team");
        teamSelect.data("selectBox-selectBoxIt").selectOption("0");
        //position select set value 0
        var positionSelect = $("#position");
        positionSelect.data("selectBox-selectBoxIt").selectOption("0");

        //reset the results
        hpApp.getPlayerStatsMakeUrl('');

        e.preventDefault();
    });
    //END
    //All pagination event handlers
    $('#pagination-right').on('click', function () {
        var activePage = parseInt($('#page-list').find('.active').data('page'));
        hpApp.paginationAnimate(activePage + 1, true);
    });
    $('#pagination-left').on('click', function () {
        var activePage = parseInt($('#page-list').find('.active').data('page'));
        hpApp.paginationAnimate(activePage - 1, true);
    });
    $('#pagination-first').on('click', function () {
        hpApp.paginationAnimate(1, true);
    });
    $('#pagination-last').on('click', function () {
        hpApp.paginationAnimate(parseInt($('#page-list').find('a:last-child').data('page')), true);
    });
    $('.post-pagination').on('click', 'a', function () {
        var activePage = parseInt($(this).data('page'));
        hpApp.paginationAnimate(activePage, true);
    });
    //END
    //When page open search data with no player name
    hpApp.getPlayerStats('?data_retrieval=season&season_id=' + currentSeasonId + '&live_draft=1&paginate=50&live_draft_search_player=1');
    //END
    //Position and team select event handler
    $(document).on('change', '#position,#team', function () {
        hpApp.getPlayerStatsMakeUrl($('#search-player-stats,#search-player-stats-mob').val());
    });
    //END
    //Player stats data sorting
    $(document).on('click', '.player-stats-sort-th', function () {
        var $this = $(this),
                statOrderKey = $this.attr('data-stat_order_key'),
                statOrderType = $this.attr('data-order_type');
        //chnaging to new order type
        if (statOrderType == 'desc') {
            statOrderType = 'asc';
            $this.attr('data-order_type', statOrderType);
        } else {
            statOrderType = 'desc';
            $this.attr('data-order_type', statOrderType);
        }

        if (statOrderKey != '') {
            hpApp.getPlayerStatsMakeUrl($('#search-player-stats,#search-player-stats-mob').val(), '', statOrderKey, statOrderType);
        }
    });
    //END
});
//END

//my queue
var playerSearchCurrentSeasonId;
$(document).ready(function () {
    //player search on row click event handler
    $(document).on('click', '#player-stats-all tr', function () {
        var $this = $(this),
                playerSeasonId = $this.attr('data-player_season_id');

        //check if this player is not already appended on center
        if (playerSeasonId != $('#player-add-queue').attr('data-player_season_id')) {
            var centerHeaderSel = $('.center-block .center-header');
            centerHeaderSel.find('h3').html($this.find('td:nth-child(n+2)').html());
            centerHeaderSel.show();
            $('#player-add-queue').show().attr('data-player_season_id', playerSeasonId);
            $('#player-remove-queue').hide();

            //Highlight the row
            $this.addClass('green-row');
            $this.siblings().removeClass('green-row');
            //remove green row from bottom player tr
            $('#player-queue-body tr').removeClass('green-row');

            //check if player is not already present in queue
            var trPlayerQueueSel = $('#player-queue-body tr[data-player_season_id="' + playerSeasonId + '"]');
            if (trPlayerQueueSel.length > 0) {
                $('#player-remove-queue').attr('data-player_queue_id', trPlayerQueueSel.attr('data-player_queue_id')).show();
                $('#player-add-queue').hide();
            }
        }
    });
    //END
    //Add player to queue
    $(document).on('click', '#player-add-queue', function () {
        var $this = $(this),
                playerSeasonId = $this.attr('data-player_season_id');
        //check if player is not already present in queue
        var playerFound = false,
                trPlayerQueueSel = $('#player-queue-body tr[data-player_season_id="' + playerSeasonId + '"]');
        if (trPlayerQueueSel.length > 0) {
            playerFound = true;
        }

        if (playerFound == false) { //Not found 
            var playerH3Selector = $this.parent().find('h3'),
                    playerName = playerH3Selector.text();
            $.ajax({
                url: addPlayerQueueUrl,
                type: 'POST',
                dataType: 'json',
                data: {
                    pool_id: currentPoolId,
                    player_season_id: playerSeasonId,
                    sort_order: 0
                },
                success: function (response) {
                    if (!$.isEmptyObject(response.data)) {
                        //append player in MyQueue 
                        var playerQueueListAppend = $('#player-queue-body'),
                                playerQueueListTemplate = $('#player-queue-list-template').html();
                        playerQueueListTemplate = _.template(playerQueueListTemplate);
                        if (playerQueueListAppend.find('.empty').length > 0) {
                            playerQueueListAppend.empty();
                        }
                        var playerQueueListData = {
                            playerRank: 1,
                            playerName: playerName,
                            playerQueueId: response.data.id,
                            playerSeasonId: playerSeasonId
                        };
                        playerQueueListAppend.append(playerQueueListTemplate(playerQueueListData));
                        //sort order call
                        playerQueueSortOrder($("#player-queue-body"));
                        //show remove from my queue button
                        $('#player-remove-queue').attr('data-player_queue_id', response.data.id).show();
                        $('#player-add-queue').hide();

                        $('.my-queue .all-queues').getNiceScroll().resize();
                    }
                },
                error: function (error) {
                    hpApp.ajaxSwalError(error);
                }
            });
        } else { //Found in my queue
            swal({
                title: 'Oops!!',
                text: 'Player is already present in My Queue.',
                type: 'error',
                customClass: 'sweat-alert-confirm'
            });
        }
    });
    //END
    //Get all queue player 
    hpApp.getMyQueueData = function (appendSpaceEmpty) {
        $.ajax({
            url: getPlayerQueueUrl,
            type: 'GET',
            dataType: 'json',
            data: {
                pool_id: currentPoolId
            },
            success: function (response) {
                if (!$.isEmptyObject(response)) {
                    //append player in MyQueue 
                    var playerQueueListAppend = $('#player-queue-body'),
                            playerQueueListTemplate = $('#player-queue-db-list-template').html();
                    playerQueueListTemplate = _.template(playerQueueListTemplate);
                    if (playerQueueListAppend.find('.empty').length > 0) {
                        playerQueueListAppend.empty();
                    }
                    if (typeof appendSpaceEmpty != 'undefined' && appendSpaceEmpty == true) {
                        playerQueueListAppend.empty();
                    }
                    $.each(response, function (i, v) {
                        var playerQueueListData = {
                            player: v,
                            rank: i + 1
                        };
                        playerQueueListAppend.append(playerQueueListTemplate(playerQueueListData));
                        $('.my-queue .all-queues').getNiceScroll().resize();
                    });
                    hpApp.loaderRemove();
                } else {
                    var emptyHeight = ((parseInt($('.all-queues').height()) - 75) / 2);
                    $('#player-queue-body .empty').css({'padding-top': emptyHeight, 'padding-bottom': emptyHeight});
                    hpApp.loaderRemove();
                    $('.live-draft-queue-row').remove();
                }
            },
            error: function (error) {
                hpApp.loaderRemove();
                hpApp.ajaxSwalError(error);
            }
        });
    };
    hpApp.getMyQueueData();
    //END
    //Making table body sortable
    function playerQueueSortOrder($this) {
        var deletePostArr = {};
        $this.find('tr').each(function (i, v) {
            var playerQueueId = $(this).attr('data-player_queue_id');
            deletePostArr[playerQueueId] = i;
        });
        $.ajax({
            url: playerQueueSortOrderUrl,
            type: 'PATCH',
            dataType: 'json',
            data: {sort_array: deletePostArr},
            success: function (response) {
                //Change ranks
                $this.find('tr').each(function (i, v) {
                    $(this).find('td:nth-child(2)').text(i + 1);
                });
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
    }
    $("#player-queue-body").sortable({
        update: function () {
            playerQueueSortOrder($(this));
        },
        handle: ".player-sort-handle"
    });
    //END
    //Delete player from queue
    function playerRemoveQueueList(removePlayerQueueUrlNew, $this, showAddButton) {
        $.ajax({
            url: removePlayerQueueUrlNew,
            type: 'DELETE',
            dataType: 'json',
            success: function (response) {
                if (!$.isEmptyObject(response)) {
                    $this.remove();
                    //sort order call
                    playerQueueSortOrder($("#player-queue-body"));

                    if (typeof showAddButton != 'undefined' && showAddButton == true) {
                        $('#player-add-queue').show();
                        $('#player-remove-queue').hide();
                    }
                    $('.my-queue .all-queues').getNiceScroll().resize();
                }
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
    }
    $(document).on('click', '.player-queue-remove-list', function () {
        var $this = $(this).parent(),
                playerQueueId = $this.attr('data-player_queue_id'),
                removePlayerQueueUrlNew = removePlayerQueueUrl.replace('player_queue_id', playerQueueId);
        //change remove from my queue if needed
        if ($('#player-remove-queue[data-player_queue_id="' + playerQueueId + '"]').length > 0) {
            playerRemoveQueueList(removePlayerQueueUrlNew, $this, true);
        } else {
            playerRemoveQueueList(removePlayerQueueUrlNew, $this);
        }
    });
    $(document).on('click', '#player-remove-queue', function () {
        var $this = $(this),
                playerQueueId = $this.attr('data-player_queue_id'),
                removePlayerQueueUrlNew = removePlayerQueueUrl.replace('player_queue_id', playerQueueId);
        //check if player is not already present in queue
        var trPlayerQueueSel = $('#player-queue-body tr[data-player_queue_id="' + playerQueueId + '"]');
        if (trPlayerQueueSel.length > 0) {
            playerRemoveQueueList(removePlayerQueueUrlNew, trPlayerQueueSel, true);
        }
    });
    //END
    //Player queue list open player on center
    $(document).on('click', '#player-queue-body .player-name', function () {
        var $this = $(this).parent(),
                playerSeasonId = $this.attr('data-player_season_id');

        //check if this player is not already appended on center
        if (playerSeasonId != $('#player-add-queue').attr('data-player_season_id')) {
            var centerHeaderSel = $('.center-block .center-header');
            centerHeaderSel.find('h3').html($this.find('td:nth-child(3)').html());
            centerHeaderSel.show();
            $('#player-add-queue').attr('data-player_season_id', playerSeasonId);

            //remove green row from bottom player tr
            $('#player-stats-all tr').removeClass('green-row');
            //add green row
            $this.addClass('green-row');
            $this.siblings().removeClass('green-row');

            //Show remove button
            $('#player-remove-queue').attr('data-player_queue_id', $this.attr('data-player_queue_id')).show();
            $('#player-add-queue').hide();
        }
    });
    //END
    $('.my-queue .all-queues').niceScroll({
        cursorcolor: "#0F6AAE",
        cursoropacitymax: 1,
        cursorwidth: 7,
        cursorborder: 0,
        autohidemode: false
    });
    $('.my-queue .all-queues').scroll(function () {
        $('.my-queue .all-queues').getNiceScroll().resize();
    });
});
//END