$(document).ready(function () {
    $('.select-field').selectBoxIt();
    hpApp.loaderRemove = function () {
        currentAjaxCount = currentAjaxCount + 1;
        if (currentAjaxCount == maxAjaxCount) {
            $('.scorer-loader-logo').fadeOut();
        }
    };
    
    // this is temp code
    $('.anik .switch-blue input[type="checkbox"]').on('change', function () {
        var $this = $(this);
        if ($this.is(':checked')) {
            var checkbox_val = '1';
        } else {
            var checkbox_val = '0';
        }
        $this.parent().parent().find('.switch-input').val(checkbox_val);
        $.ajax({
            url: automaticSelectionUrl,
            type: 'POST',
            dataType: 'json',
            data: {
                'automatic_selection': checkbox_val
            },
            success: function (response) {
            },
            error: function (err) {
            }
        });
    });
}); 