var playerSearchCurrentSeasonId;
$(document).ready(function () {
    //player search on row click event handler
    $(document).on('click', '#player-stats-all tr', function () {
        var $this = $(this),
                appendlocation = $('#player-full-desc-all'),
                template = $('#player-full-desc-template').html(),
                playerSeasonId = $this.attr('data-player_season_id');

        //check if this player is not already appended on center
        if (playerSeasonId != $('#player-add-queue').attr('data-player_season_id')) {
            appendlocation.empty();
            template = _.template(template);
            var data = {
                playerDetails: $this.find('td:nth-child(n+2)').html(),
                playerSeasonId: playerSeasonId,
                playerRank: $this.find('td:nth-child(1)').text().trim()
            };
            appendlocation.append(template(data));

            //Highlight the row
            $this.addClass('green-row');
            $this.siblings().removeClass('green-row');
            //remove green row from bottom player tr
            $('#player-queue-body tr').removeClass('green-row');

            //Append latest year data of player
            $('#player-full-desc-body').append('<tr><td>' + allSeasons[0]['name'] + '</td></tr>');
            $('#player-full-desc-body tr').append($('td:nth-child(n+3)', $this).clone());

            //check if player is not already present in queue
            var trPlayerQueueSel = $('#player-queue-body tr[data-player_season_id="' + playerSeasonId + '"]');
            if (trPlayerQueueSel.length > 0) {
                $('#player-remove-queue').attr('data-player_queue_id', trPlayerQueueSel.attr('data-player_queue_id')).show();
                $('#player-add-queue').hide();
            }
            //show draft button if this is current team
            if (typeof currentTeamPick != 'undefined' && currentTeamPick == true) {
                $('#player-final-draft').show();
            }

            //player center area scroll
            $('#player-full-desc-all .center-header').niceScroll({
                cursorcolor: "#0F6AAE",
                cursoropacitymax: 1,
                cursorwidth: 7,
                cursorborder: 0,
                autohidemode: false
            });
            //END

            //Get all other years data
            playerSearchCurrentSeasonId = playerSeasonId; //To solve double click bug(BUG:if user click on one player and just then click again on other player, then first user data get appended on second user place)
            $.each(allSeasons, function (i, v) {
                if (i != 0) {//First year data already appended
                    $.ajax({
                        url: getPlayerStatsUrl + '?data_retrieval=season&season_id=' + v.id + '&player_season_id=' + playerSeasonId + '&live_draft=1',
                        type: 'GET',
                        dataType: 'json',
                        success: function (response) {
                            if (!$.isEmptyObject(response.data)) {
                                if (playerSearchCurrentSeasonId == playerSeasonId) {
                                    var playerFullDescAppend = $('#player-full-desc-body'),
                                            playerSeasonStatTemplate = $('#player-season-stat-template').html();
                                    playerSeasonStatTemplate = _.template(playerSeasonStatTemplate);
                                    var playerFullDescData = {
                                        seasonStats: response.data[0],
                                        seasonName: v.name
                                    };
                                    playerFullDescAppend.append(playerSeasonStatTemplate(playerFullDescData));
                                    $('#player-full-desc-all .center-header').getNiceScroll().resize();
                                }
                            }
                        },
                        error: function (err) {
                        }
                    });
                }
            });
        }
    });
    //END
    //Add player to queue
    $(document).on('click', '#player-add-queue', function () {
        var $this = $(this),
                playerSeasonId = $this.attr('data-player_season_id');
        //check if player is not already present in queue
        var playerFound = false,
                trPlayerQueueSel = $('#player-queue-body tr[data-player_season_id="' + playerSeasonId + '"]');
        if (trPlayerQueueSel.length > 0) {
            playerFound = true;
        }

        if (playerFound == false) { //Not found 
            var playerH3Selector = $this.parent().find('h3'),
                    playerName = playerH3Selector.text(),
                    playerRank = playerH3Selector.attr('data-player-rank');
            $.ajax({
                url: addPlayerQueueUrl,
                type: 'POST',
                dataType: 'json',
                data: {
                    pool_id: currentPoolId,
                    player_season_id: playerSeasonId,
                    sort_order: 0
                },
                success: function (response) {
                    if (!$.isEmptyObject(response.data)) {
                        //append player in MyQueue 
                        var playerQueueListAppend = $('#player-queue-body'),
                                playerQueueListTemplate = $('#player-queue-list-template').html();
                        playerQueueListTemplate = _.template(playerQueueListTemplate);
                        if (playerQueueListAppend.find('.empty').length > 0) {
                            playerQueueListAppend.empty();
                        }
                        var playerQueueListData = {
                            playerRank: playerRank,
                            playerName: playerName,
                            playerQueueId: response.data.id,
                            playerSeasonId: playerSeasonId
                        };
                        playerQueueListAppend.append(playerQueueListTemplate(playerQueueListData));
                        //sort order call
                        playerQueueSortOrder($("#player-queue-body"));
                        //show remove from my queue button
                        $('#player-remove-queue').attr('data-player_queue_id', response.data.id).show();
                        $('#player-add-queue').hide();
                    }
                },
                error: function (error) {
                    hpApp.ajaxSwalError(error);
                }
            });
        } else { //Found in my queue
            swal({
                title: 'Oops!!',
                text: 'Player is already present in My Queue.',
                type: 'error',
                customClass: 'sweat-alert-confirm'
            });
        }
    });
    //END
    //Get all queue player 
    hpApp.getMyQueueData = function (appendSpaceEmpty) {
        $.ajax({
            url: getPlayerQueueUrl,
            type: 'GET',
            dataType: 'json',
            data: {
                pool_id: currentPoolId
            },
            success: function (response) {
                if (!$.isEmptyObject(response)) {
                    //append player in MyQueue 
                    var playerQueueListAppend = $('#player-queue-body'),
                            playerQueueListTemplate = $('#player-queue-db-list-template').html();
                    playerQueueListTemplate = _.template(playerQueueListTemplate);
                    if (playerQueueListAppend.find('.empty').length > 0) {
                        playerQueueListAppend.empty();
                    }
                    if (typeof appendSpaceEmpty != 'undefined' && appendSpaceEmpty == true) {
                        playerQueueListAppend.empty();
                    }
                    $.each(response, function (i, v) {
                        var playerQueueListData = {
                            player: v,
                            rank: i + 1
                        };
                        playerQueueListAppend.append(playerQueueListTemplate(playerQueueListData));
                        $('.my-queue .all-queues').getNiceScroll().resize();
                    });
                    hpApp.loaderRemove();
                } else {
                    var emptyHeight = ((parseInt($('.all-queues').height()) - 75) / 2);
                    $('#player-queue-body .empty').css({'padding-top': emptyHeight, 'padding-bottom': emptyHeight});
                    hpApp.loaderRemove();
                    $('.live-draft-queue-row').remove();
                }
            },
            error: function (error) {
                hpApp.loaderRemove();
                hpApp.ajaxSwalError(error);
            }
        });
    };
    hpApp.getMyQueueData();
    //END
    //Making table body sortable
    function playerQueueSortOrder($this) {
        var deletePostArr = {};
        $this.find('tr').each(function (i, v) {
            var playerQueueId = $(this).attr('data-player_queue_id');
            deletePostArr[playerQueueId] = i;
        });
        $.ajax({
            url: playerQueueSortOrderUrl,
            type: 'PATCH',
            dataType: 'json',
            data: {sort_array: deletePostArr},
            success: function (response) {
                //Change ranks
                $this.find('tr').each(function (i, v) {
                    $(this).find('td:nth-child(2)').text(i + 1);
                });
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
    }
    $("#player-queue-body").sortable({
        update: function () {
            playerQueueSortOrder($(this));
        },
        handle: ".player-sort-handle"
    });
    //END
    //Delete player from queue
    function playerRemoveQueueList(removePlayerQueueUrlNew, $this, showAddButton) {
        $.ajax({
            url: removePlayerQueueUrlNew,
            type: 'DELETE',
            dataType: 'json',
            success: function (response) {
                if (!$.isEmptyObject(response)) {
                    $this.remove();
                    //sort order call
                    playerQueueSortOrder($("#player-queue-body"));

                    if (typeof showAddButton != 'undefined' && showAddButton == true) {
                        $('#player-add-queue').show();
                        $('#player-remove-queue').hide();
                    }
                }
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
    }
    $(document).on('click', '.player-queue-remove-list', function () {
        var $this = $(this).parent(),
                playerQueueId = $this.attr('data-player_queue_id'),
                removePlayerQueueUrlNew = removePlayerQueueUrl.replace('player_queue_id', playerQueueId);
        //change remove from my queue if needed
        if ($('#player-remove-queue[data-player_queue_id="' + playerQueueId + '"]').length > 0) {
            playerRemoveQueueList(removePlayerQueueUrlNew, $this, true);
        } else {
            playerRemoveQueueList(removePlayerQueueUrlNew, $this);
        }
    });
    $(document).on('click', '#player-remove-queue', function () {
        var $this = $(this),
                playerQueueId = $this.attr('data-player_queue_id'),
                removePlayerQueueUrlNew = removePlayerQueueUrl.replace('player_queue_id', playerQueueId);
        //check if player is not already present in queue
        var trPlayerQueueSel = $('#player-queue-body tr[data-player_queue_id="' + playerQueueId + '"]');
        if (trPlayerQueueSel.length > 0) {
            playerRemoveQueueList(removePlayerQueueUrlNew, trPlayerQueueSel, true);
        }
    });
    //END
    //Player queue list open player on center
    $(document).on('click', '#player-queue-body .player-name', function () {
        var $this = $(this).parent(),
                playerSeasonId = $this.attr('data-player_season_id');
        //Append player name 
        var appendlocation = $('#player-full-desc-all'),
                template = $('#player-full-desc-template').html();

        //check if this player is not already appended on center
        if (playerSeasonId != $('#player-add-queue').attr('data-player_season_id')) {
            appendlocation.empty();
            template = _.template(template);
            var data = {
                playerDetails: $this.find('td:nth-child(3)').html(),
                playerSeasonId: playerSeasonId,
                playerRank: $this.find('td:nth-child(2)').text().trim()
            };
            appendlocation.append(template(data));

            //remove green row from bottom player tr
            $('#player-stats-all tr').removeClass('green-row');
            //add green row
            $this.addClass('green-row');
            $this.siblings().removeClass('green-row');

            //Show remove button
            $('#player-remove-queue').attr('data-player_queue_id', $this.attr('data-player_queue_id')).show();
            $('#player-add-queue').hide();

            //show draft button if this is current team
            if (typeof currentTeamPick != 'undefined' && currentTeamPick == true) {
                $('#player-final-draft').show();
            }

            //player center area scroll
            $('#player-full-desc-all .center-header').niceScroll({
                cursorcolor: "#0F6AAE",
                cursoropacitymax: 1,
                cursorwidth: 7,
                cursorborder: 0,
                autohidemode: false
            });
            //END

            //append year data
            $.each(allSeasons, function (i, v) {
                $.ajax({
                    url: getPlayerStatsUrl + '?data_retrieval=season&season_id=' + v.id + '&player_season_id=' + playerSeasonId + '&live_draft=1',
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        if (!$.isEmptyObject(response.data)) {
                            var playerFullDescAppend = $('#player-full-desc-body'),
                                    playerSeasonStatTemplate = $('#player-season-stat-template').html();
                            playerSeasonStatTemplate = _.template(playerSeasonStatTemplate);
                            var playerFullDescData = {
                                seasonStats: response.data[0],
                                seasonName: v.name
                            };
                            playerFullDescAppend.append(playerSeasonStatTemplate(playerFullDescData));
                            $('#player-full-desc-all .center-header').getNiceScroll().resize();
                        }
                    },
                    error: function (err) {
                    }
                });
            });
        }
    });
    //END
    $('.my-queue .all-queues').niceScroll({
        cursorcolor: "#0F6AAE",
        cursoropacitymax: 1,
        cursorwidth: 7,
        cursorborder: 0,
        autohidemode: false
    });
    $('.my-queue .all-queues').scroll(function () {
        $('.my-queue .all-queues').getNiceScroll().resize();
    });
});