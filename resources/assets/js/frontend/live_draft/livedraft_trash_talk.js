$(document).ready(function () {
    $('#liveDraftMessage').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.find('.grey-input-field').val()) {
            $.ajax({
                url: $this.attr('action'),
                type: "POST",
                data: $this.serialize(),
                dataType: "json",
                success: function (response) {
                    $this.find('.grey-input-field').val('');
                },
                error: function (error) {
                    hpApp.ajaxSwalError(error);
                }
            });
        }
    });
    var is_scrolling = true, page = 1;
    $('#trash-talk-messages').scrollTop($(this).height());
    $('#trash-talk-messages').on('scroll', function (e) {
        e.preventDefault();
        if (($(this).scrollTop() < 100) && (is_scrolling == true)) {
            is_scrolling = false;
            page++;
            getLiveDraftMessages(page);
        }
    });

    function getLiveDraftMessages(pageNum) {
        if ($('#no-more-messages').length < 1) {
            $.ajax({
                url: getLiveDraftMessagesRoute,
                data: {page: pageNum, pool_id: currentPoolId},
                type: "GET",
                dataType: 'json',
                success: function (resp) {
                    initialHeight = $('#all-messages-container').height();
                    is_scrolling = true;
                    if (resp.data.length) {
                        $.each(resp.data, function (i, message) {
                            $('#all-messages-container').prepend('\n\
                        <div class="message">\n\
                            <p class="sender">' + message.user.name + '</p>\n\
                            <p class="text">' + message.message + '</p>\n\
                        </div>');
                        });
                        $('#trash-talk-messages').scrollTop($('#all-messages-container').height() - initialHeight);
                        $('.trash-talk .all-messages').getNiceScroll().resize();
                    } else {
                        $('#all-messages-container').prepend('\n\
                        <div id="no-more-messages">\n\
                            <p>No more messages found.</p>\n\
                        </div>');
                    }
                },
                error: function (err) {
                    hpApp.ajaxSwalError(error);
                }
            });
        }
    }
    var connection_string = socket_url + ':' + socket_port,
            socket = io.connect(connection_string, {secure: true});
    socket.on(encrypted_pool_id, function (message) {
        //checking if 'no messages till now' is present
        if ($('#all-messages-container .empty').length > 0) {
            $('#all-messages-container').empty();
        }
        $('#all-messages-container').append('\n\
                        <div class="message">\n\
                            <p class="sender">' + message.user.name + '</p>\n\
                            <p class="text">' + message.message + '</p>\n\
                        </div>');
        $('#trash-talk-messages').animate({scrollTop: $('#all-messages-container').height()});
        $('.trash-talk .all-messages').getNiceScroll().resize();
    });

    $('.trash-talk .all-messages').niceScroll({
        cursorcolor: "#0F6AAE",
        cursoropacitymax: 1,
        cursorwidth: 7,
        cursorborder: 0,
        autohidemode: false
    });

    $.ajax({
        url: getLiveDraftMessagesRoute,
        data: {page: 0, pool_id: currentPoolId},
        type: "GET",
        dataType: 'json',
        success: function (resp) {
            initialHeight = $('#all-messages-container').height();
            is_scrolling = true;
            if (resp.data.length) {
                $.each(resp.data, function (i, message) {
                    $('#all-messages-container').prepend('\n\
                        <div class="message">\n\
                            <p class="sender">' + message.user.pool_team.name + '</p>\n\
                            <p class="text">' + message.message + '</p>\n\
                        </div>');
                });
                $('#trash-talk-messages').scrollTop($('#all-messages-container').height() - initialHeight);
                $('.trash-talk .all-messages').getNiceScroll().resize();
            } else {
                $('#all-messages-container').prepend('\n\
                        <div class="empty">\n\
                            <p>No Messages till now.</p>\n\
                        </div>');
                //giving correct height
                var emptyHeight = ((parseInt($('.all-messages').height()) - 20) / 2);
                $('#all-messages-container .empty').css({'padding-top': emptyHeight, 'padding-bottom': emptyHeight});
            }
        },
        error: function (err) {
            hpApp.ajaxSwalError(error);
        }
    });
});