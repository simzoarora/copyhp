$(document).ready(function () {
    //Get all teams data
    hpApp.appendMyTeamData = function (response) {
        $('#teams-list-all').empty();
        if (!$.isEmptyObject(response)) {
            var teamsListAppend = $('#teams-list-all'),
                    teamsListTemplate = $('#teams-list-template').html();
            teamsListTemplate = _.template(teamsListTemplate);
            var teamsListData = {
                teams: response
            };
            teamsListAppend.append(teamsListTemplate(teamsListData));
        } else {
            $('#teams-list-all').append('<div class="no-teams">No Teams Found.</div>');
        }
    };
    $.ajax({
        url: getTeamsUrl,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            hpApp.appendMyTeamData(response);
        },
        error: function (err) {
            $('#teams-list-all').append('<div class="no-teams">No Teams Found.</div>');
        }
    });
    $('#teams').niceScroll({
        cursorcolor: "#0F6AAE",
        cursoropacitymax: 1,
        cursorwidth: 7,
        cursorborder: 0,
        autohidemode: false
    });
    //END
});