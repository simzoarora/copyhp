$(document).ready(function () {
    //Get all teams data
    hpApp.getMyTeamData = function () {
        $.ajax({
            url: getMyTeamUrl,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                $('#my-team-list-all').empty();
                if (!$.isEmptyObject(response)) {
                    var myTeamListAppend = $('#my-team-list-all'),
                            myTeamListTemplate = $('#my-team-list-template').html();
                    myTeamListTemplate = _.template(myTeamListTemplate);
                    var myTeamListData = {
                        myTeams: response
                    };
                    myTeamListAppend.append(myTeamListTemplate(myTeamListData));
                    $('.my-team .all-teams').getNiceScroll().resize();
                    hpApp.loaderRemove();
                }
            },
            error: function (err) {
                hpApp.loaderRemove();
            }
        });
    };
    hpApp.getMyTeamData();
    $('.my-team .all-teams').niceScroll({
        cursorcolor: "#0F6AAE",
        cursoropacitymax: 1,
        cursorwidth: 7,
        cursorborder: 0,
        autohidemode: false
    });
    //END
});