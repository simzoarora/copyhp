var currentTeamPick = false,
        thirtySecTimeoutClear = false,
        twentySecTimeoutClear = false,
        tenSecTimeoutClear = false,
        oneSecIntervalClear = false,
        countDownInterval = false,
        checkLiveDraftActive = false;
$(document).ready(function () {
    //Get all draft timing
    hpApp.getDraftTiming = function () {
        $.ajax({
            url: getDraftTiming,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (!$.isEmptyObject(response)) {
                    hpApp.processDraftTiming(response);
                }
            },
            error: function (err) {
            }
        });
    };

    hpApp.processDraftTiming = function (response) {
        var liveDraftMoment = moment.tz(liveDraftSetting.date + ' ' + liveDraftSetting.time, moment.ISO_8601, liveDraftSetting.time_zone),
                currentMomentNew = moment().tz(liveDraftSetting.time_zone),
                draft_timing_counter = 0;
        if (liveDraftMoment.diff(currentMomentNew) > 0) {
            return;
        }
        $.each(response, function (i, v) {
            //get current timing at new york timezone
            var currentMoment = moment().tz(newYorkTimezone).format('YYYY-MM-DD HH:mm:ss');
//            if (moment(currentMoment).isBetween(v.start_time, v.end_time, null, '[]')) {
            if (Date.parse(currentMoment) >= Date.parse(v.start_time) && Date.parse(currentMoment) < Date.parse(v.end_time)) {
                checkLiveDraftActive = true;
                if (currentTeamId == v.pool_team_id) {
                    //this teams pick -- change display accordingly
                    currentTeamPick = true;
                    //change text in green box to show which team pick is
                    $('.round-text').show().text(currentPickText);
                    //show draft button
                    $('#player-final-draft').show();
                    //start timer
                    var timerMilliseconds = (moment.tz(v.end_time, newYorkTimezone)).diff(moment().tz(newYorkTimezone));
                    $('#round-timer').parent().css({'display': 'table'});
                    $('#round-timer').css({'color': '#fff'});
                    hpApp.countDownTimer(timerMilliseconds, $('#round-timer'));
                    hpApp.countdownBeforeExpiryFunc(timerMilliseconds);
                } else { //changing display for all other teams
                    //this teams pick -- change display accordingly
                    currentTeamPick = false;
                    //change text in green box to show which team pick is
                    $('.round-text').show().text(v.pool_team.name + ' pick');
                    //hide draft button
                    $('#player-final-draft').hide();
                    //start timer
                    var timerMilliseconds = (moment.tz(v.end_time, newYorkTimezone)).diff(moment().tz(newYorkTimezone));
                    $('#round-timer').parent().css({'display': 'table'});
                    $('#round-timer').css({'color': '#fff'});
                    hpApp.countDownTimer(timerMilliseconds, $('#round-timer'));
                }
                //append blue timer data
                if (!$.isEmptyObject(draftResultDataArr)) {
                    $.each(draftResultDataArr, function (index, value) {
                        if (value.round_id == v.pool_round_id && value.pool_team_id == v.pool_team_id) {
                            $('#round-details').css({'display': 'table-cell'});
                            $('#round-details span:nth-child(1) span').text(value.round);
                            $('#round-details span:nth-child(2) span').text(value.pick);
                            $('#round-details span:nth-child(3) span').text(value.overall);
                        }
                    });
                }
                return false;
            }
            draft_timing_counter++;
        });
        //for loop ended
        if (response.length == draft_timing_counter) {
//            if (checkLiveDraftActive = false) {
            location.href = poolOverviewUrl;
//            }
        }
    }

    //END
    //Draft player event handle
    $(document).on('click', '#player-final-draft', function (e) {
        var $this = $(this),
                playerSeasonId = $this.attr('data-player_season_id');
        $.ajax({
            url: draftplayerUrl,
            type: 'POST',
            dataType: 'json',
            data: {player_season_id: playerSeasonId},
            success: function (response) {
                if (!$.isEmptyObject(response)) {
                    //changing display of center header
                    $('#player-add-queue').remove();
                    $('#player-remove-queue').remove();
                    $('#player-drafted').show();
                    $this.remove();
                    //getting myteam data
                    hpApp.getMyTeamData();
                    //reset the player search results
                    hpApp.getPlayerStatsMakeUrl('');
                    //changing pick value
                    currentTeamPick = false;
                }
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });

        e.preventDefault();
    });
    //END
    //teams, draftresult, draftoverview call data
    var connection_string = socket_url + ':' + socket_port,
            socket = io.connect(connection_string, {secure: true});
    if (encrypted_user_id) {
        socket.emit('new_user', encrypted_user_id);
    }
    socket.on(socketChannel, function (response) {
        hpApp.countdownClear();
        //checking which team has next pick
        if (!$.isEmptyObject(response.data.draft_timing)) {
            hpApp.processDraftTiming(response.data.draft_timing);
        }
        //teams tab data append 
        if (!$.isEmptyObject(response.data.teams_tab)) {
            hpApp.appendMyTeamData(response.data.teams_tab);
        }
        //draftresult data append
        if (!$.isEmptyObject(response.data.draft_results_tab)) {
            draftResultDataArr = response.data.draft_results_tab;
            hpApp.appendDraftPlayerData(response.data.draft_results_tab);
        }
        //getting myqueue data
        hpApp.getMyQueueData(true);
        //reset the player search results
        hpApp.getPlayerStatsMakeUrl('');
        // get my team data of right side
        hpApp.getMyTeamData();
    });
    socket.on(socketChannelAutoselect, function (response) {
        $('.anik .switch-handle').trigger('click');
    });
    //checking if socket is always connected
    setInterval(function () {
        if (socket.connected == false) {
            socket = io.connect(connection_string, {secure: true});
            if (encrypted_user_id) {
                socket.emit('new_user', encrypted_user_id);
            }
        }
    }, 1000);
    //END
    //making timer text color to red when it reached 30 seconds
    hpApp.countdownBeforeExpiryFunc = function (timerMilliseconds) {
        var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");

        if (timerMilliseconds > 30000) {
            var timeoutTime = parseInt(timerMilliseconds) - 30000;
            thirtySecTimeoutClear = setTimeout(function () {
                $('#round-timer').css({'color': '#ff0000'});
                snd.play();
                twentySecTimeoutClear = setTimeout(function () {
                    oneSecIntervalClear = setInterval(function () {
                        snd.play();
                    }, 1000);

                    tenSecTimeoutClear = setTimeout(function () {
                        clearInterval(oneSecIntervalClear);
                    }, 10000);
                }, 20000);
            }, timeoutTime);
        } else if (timerMilliseconds < 30000 && timerMilliseconds > 10000) {
            var timeoutTime = parseInt(timerMilliseconds) - 10000;
            twentySecTimeoutClear = setTimeout(function () {
                oneSecIntervalClear = setInterval(function () {
                    snd.play();
                }, 1000);

                tenSecTimeoutClear = setTimeout(function () {
                    clearInterval(oneSecIntervalClear);
                }, 10000);
            }, timeoutTime);
        } else if (timerMilliseconds < 10000) {
            oneSecIntervalClear = setInterval(function () {
                snd.play();
            }, 1000);

            tenSecTimeoutClear = setTimeout(function () {
                clearInterval(oneSecIntervalClear);
            }, timerMilliseconds);
        }
    };
    hpApp.countdownClear = function () {
        clearTimeout(thirtySecTimeoutClear);
        clearTimeout(twentySecTimeoutClear);
        clearTimeout(tenSecTimeoutClear);
        clearInterval(oneSecIntervalClear);
    };
    //END
    hpApp.countdownTimeUpFifteen = function (timerMilliseconds) { //not working
        setTimeout(function () {
            hpApp.getDraftTiming();
            $("#till-draft-begins").hide();
        }, timerMilliseconds);
    };
});