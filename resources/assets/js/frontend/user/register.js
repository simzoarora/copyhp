$(document).ready(function () {
    $('.register-header a').on('click', function (e) {
        e.preventDefault();
    });
    //Register form
    $('.register-form').on('submit', function (e) {
        var $this = $(this);
        //disabling submit button
        $this.find('button[type="submit"]').attr('disabled', 'disabled').addClass('active');
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (response) {
                //removing all error classes
                $this.find('.has-error').removeClass('has-error');
                $this.find('.error-msg').remove();

                $this.find('button[type="submit"]').removeAttr('disabled').removeClass('active');
                $this.find('[type="password"], [type="email"], [type="text"]').val('');
                $('#alert-box').addClass('success').text(response.message);

                setTimeout(function () {
                    $('#alert-box').removeClass('success').text('');
                }, 8000);
            },
            error: function (error) {
                var formSel = $('.register-form');
                formSel.find('button[type="submit"]').removeAttr('disabled').removeClass('active');
                hpApp.ajaxInputError(error, formSel);
            }
        });
        e.preventDefault(e);
    });
    //END Register form
});