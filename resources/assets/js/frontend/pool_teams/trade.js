$(document).ready(function () {
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    //Getting players data
    function getTeamdata(outerSel, playersTeam, response, type) {
        if (!$.isEmptyObject(playersTeam)) {
            var template = $('#all-players-template').html();
            template = _.template(template);
            var appendData = {
                playersTeam: playersTeam,
                poolTrade: response,
                poolScoringFields: poolScoringFields,
                type: type
            };
            outerSel.append(template(appendData));
        }
        //Checking if players and goalies tables are empty
        if (outerSel.find('#rosters-players tbody').find('tr').length == 0) {
            var colspan = outerSel.find('#rosters-players thead').find('th').length;
            outerSel.find('#rosters-players tbody').empty().append('<tr><td colspan="' + colspan + '" class="no-result-present">No player present.</td><tr>');
        }
        if (outerSel.find('#rosters-goalies tbody').find('tr').length == 0) {
            var colspan = outerSel.find('#rosters-goalies thead').find('th').length;
            outerSel.find('#rosters-goalies tbody').empty().append('<tr><td colspan="' + colspan + '" class="no-result-present">No goalie present.</td><tr>');
        }
    }
    $.ajax({
        url: proposeTradeUrl,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            //your trade data append
            getTeamdata($('#your-traded-outer'), response.pool_trade.parent_team.pool_trade_players, response, dropTradeType.inverse_trade_player_type.trade_players);
            //your dropped data append
            getTeamdata($('#your-dropped-outer'), response.pool_trade.parent_team.pool_trade_players, response, dropTradeType.inverse_trade_player_type.drop_players);
            //team traded data append
            getTeamdata($('#team-traded-outer'), response.pool_trade.trading_team.pool_trade_players, response, dropTradeType.inverse_trade_player_type.trade_players);

            //team name and dates 
            $('.traded-team-name').text(response.pool_trade.trading_team.name);
            var expireDate = moment(response.pool_trade.deadline).format("MMMM D, YYYY"),
                    outerSel = $('#propose__trade'),
                    template = $('#propose__trade-template').html();
            template = _.template(template);
            var appendData = {
                teamName: response.pool_trade.trading_team.name,
                createDate: moment(response.pool_trade.created_at).format("MMMM D, YYYY"),
                expireDate: expireDate,
                parentPoolTeamId: parentPoolTeamId,
                responseParentPoolTeamId: response.pool_trade.parent_pool_team_id
            };
            outerSel.append(template(appendData));

            //accept/reject/cancel
            var outerSel = $('#player__action'),
                    template = $('#player__action-template').html();
            template = _.template(template);
            var appendData = {
                expireDate: moment(response.pool_trade.deadline).fromNow('dd, hd, mm'),
                parentPoolTeamId: parentPoolTeamId,
                responseParentPoolTeamId: response.pool_trade.parent_pool_team_id
            };
            outerSel.append(template(appendData));

            //removing loader
            $('.scorer-loader-logo').hide();
            $('.site-container').css({'background-color': '#fff'});
            $('#pool--content').css({'opacity': 1});
        },
        error: function (error) {
            hpApp.ajaxSwalError(error);
        }
    });
    //END Getting players data
    //Action status event handler
    $(document).on('click', '.action-status', function (e) {
        $.ajax({
            type: "POST",
            url: tradeStatusUrl,
            data: {
                status: $(this).attr('data-status')
            },
            dataType: 'json',
            success: function (response) {
                location.href = playerStatsUrl;
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
        e.preventDefault();
    });
    //END Action status event handler
}); 