$(document).ready(function () {
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    //getting user pool
    function getUserPools(url, siteloader) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function (resp) {
                if (!$.isEmptyObject(resp.data)) {
                    //empty all data
                    $('#pool-right-header-outer').empty();
                    $.each(resp.data, function (index, value) {
                        var response = value;

                        //Calculating current team
                        if (!$.isEmptyObject(response.pool_team)) {
                            var currentTeamId = response.pool_team.id;
                        }
                        //END Calculating current team

                        //checking which pool type/cat is this
                        if (response.pool_type == poolTypes.h2h) {
                            if (!$.isEmptyObject(response.pool_setting)) {
                                if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                                    var poolToShow = 11;
                                } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly) {
                                    var poolToShow = 12;
                                } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly_daily) {
                                    var poolToShow = 13;
                                }
                            }
                        } else if (response.pool_type == poolTypes.box) {
                            var poolToShow = 2;
                        } else if (response.pool_type == poolTypes.standard) {
                            if (!$.isEmptyObject(response.pool_setting)) {
                                if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_standard) {
                                    var poolToShow = 31;
                                } else if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                                    var poolToShow = 32;
                                }
                            }
                        }
                        //END checking which pool type/cat is this

                        if (poolToShow == 11) {
                            var poolLink = $('#pool-overview-link').attr('href'),
                                    newPoolLink = poolLink.replace(0, response.id),
                                    poolEditLink = $('#pool-edit-link').attr('href'),
                                    newPoolEditLink = poolEditLink.replace(0, response.id),
                                    poolDeleteLink = $('#pool-delete-link').attr('href'),
                                    newPoolDeleteLink = poolDeleteLink.replace(0, response.id),
                                    poolLivedraftLink = $('#pool-livedraft-link').attr('href'),
                                    newpoolLivedraftLink = poolLivedraftLink.replace('ld', response.id),
                                    poolPredraftLink = $('#pool-predraft-link').attr('href'),
                                    newpoolPredraftLink = poolPredraftLink.replace('pd', response.id);

                            var poolRightHeaderOuter = $('#pool-right-header-outer'),
                                    poolRightHeaderTemplate = $('#pool-right-header-template').html();
                            poolRightHeaderTemplate = _.template(poolRightHeaderTemplate);
                            var poolRightHeaderData = {
                                poolMatchup: response.pool_matchup,
                                poolScoreSettings: response.pool_score_settings,
                                poolName: response.name,
                                poolId: response.id,
                                poolComplete: response.is_completed,
                                poolLink: newPoolLink,
                                poolEditLink: newPoolEditLink,
                                poolDeleteLink: newPoolDeleteLink,
                                poolUser: response.user,
                                poolLogo: response.logo,
                                poolLiveDraftSetting: response.live_draft_setting,
                                poolLiveDraftActive: response.live_draft_player,
                                poolLivedraftLink: newpoolLivedraftLink,
                                poolPredraftLink: newpoolPredraftLink,
                                siteLink: api_url
                            };
                            poolRightHeaderOuter.append(poolRightHeaderTemplate(poolRightHeaderData));
                            //bottom data width
                            if (!$.isEmptyObject(response.pool_matchup)) {
                                hpApp.h2hStatsSetting();
                                //bottom data colors
                                $.each(response.pool_matchup.cat_result, function (i, v) {
                                    if (v.loss == true) {
                                        $('#score-setting-' + response.pool_matchup.id + '-' + i).addClass('blue');
                                    } else {
                                        if (v.win == currentTeamId) {
                                            $('#score-setting-' + response.pool_matchup.id + '-' + i).addClass('green');
                                        } else {
                                            $('#score-setting-' + response.pool_matchup.id + '-' + i).addClass('red');
                                        }
                                    }
                                });
                            }
                        } else if (poolToShow == 2 || poolToShow == 31) {
                            var poolLink = $('#pool-overview-link').attr('href'),
                                    newPoolLink = poolLink.replace(0, response.id),
                                    poolEditLink = $('#pool-edit-link').attr('href'),
                                    newPoolEditLink = poolEditLink.replace(0, response.id),
                                    poolDeleteLink = $('#pool-delete-link').attr('href'),
                                    newPoolDeleteLink = poolDeleteLink.replace(0, response.id),
                                    boxDraftLink = $('#pool-boxdraft-link').attr('href'),
                                    newBoxDraftLink = boxDraftLink.replace(0, response.id),
                                    poolLivedraftLink = $('#pool-livedraft-link').attr('href'),
                                    newpoolLivedraftLink = poolLivedraftLink.replace('ld', response.id),
                                    poolPredraftLink = $('#pool-predraft-link').attr('href'),
                                    newpoolPredraftLink = poolPredraftLink.replace('pd', response.id);

                            var poolRightHeaderOuter = $('#pool-right-header-outer'),
                                    poolRightHeaderTemplate = $('#pool-right-header-box-template').html();
                            poolRightHeaderTemplate = _.template(poolRightHeaderTemplate);
                            var poolRightHeaderData = {
                                poolTeamFirst: response.pool_team_first,
                                poolScoreSettings: response.pool_score_settings,
                                poolName: response.name,
                                poolId: response.id,
                                poolComplete: response.is_completed,
                                poolTeamPlayers: response.pool_team_players,
                                poolToShow: poolToShow,
                                poolLink: newPoolLink,
                                poolEditLink: newPoolEditLink,
                                poolDeleteLink: newPoolDeleteLink,
                                boxDraftLink: newBoxDraftLink,
                                poolUser: response.user,
                                poolLogo: response.logo,
                                poolLiveDraftSetting: response.live_draft_setting,
                                poolLiveDraftActive: response.live_draft_player,
                                poolLivedraftLink: newpoolLivedraftLink,
                                poolPredraftLink: newpoolPredraftLink,
                                siteLink: api_url
                            };
                            poolRightHeaderOuter.append(poolRightHeaderTemplate(poolRightHeaderData));

                            if (!$.isEmptyObject(response.pool_team_first)) {
                                $.each(response.pool_team_first.pool_standard_scores, function (i, v) {
                                    $('#score-setting-' + response.pool_team_first.id + '-' + v.pool_score_setting.pool_scoring_field_id).text(hpApp.twoDecimal(v.total_score));
                                });
                            }
                            //setting widths
                            hpApp.boxStandardStatsSetting();
                        } else if (poolToShow == 32) {
                            var poolLink = $('#pool-overview-link').attr('href'),
                                    newPoolLink = poolLink.replace(0, response.id),
                                    poolEditLink = $('#pool-edit-link').attr('href'),
                                    newPoolEditLink = poolEditLink.replace(0, response.id),
                                    poolDeleteLink = $('#pool-delete-link').attr('href'),
                                    newPoolDeleteLink = poolDeleteLink.replace(0, response.id),
                                    poolLivedraftLink = $('#pool-livedraft-link').attr('href'),
                                    newpoolLivedraftLink = poolLivedraftLink.replace('ld', response.id),
                                    poolPredraftLink = $('#pool-predraft-link').attr('href'),
                                    newpoolPredraftLink = poolPredraftLink.replace('pd', response.id);

                            var poolRightHeaderOuter = $('#pool-right-header-outer'),
                                    poolRightHeaderTemplate = $('#pool-right-header-box-template').html();
                            poolRightHeaderTemplate = _.template(poolRightHeaderTemplate);
                            var poolRightHeaderData = {
                                poolTeamFirst: response.pool_team_first,
                                poolScoreSettings: response.pool_score_settings,
                                poolName: response.name,
                                poolId: response.id,
                                poolComplete: response.is_completed,
                                poolLink: newPoolLink,
                                poolEditLink: newPoolEditLink,
                                poolDeleteLink: newPoolDeleteLink,
                                poolUser: response.user,
                                poolLogo: response.logo,
                                poolLiveDraftSetting: response.live_draft_setting,
                                poolLiveDraftActive: response.live_draft_player,
                                poolLivedraftLink: newpoolLivedraftLink,
                                poolPredraftLink: newpoolPredraftLink,
                                siteLink: api_url
                            };
                            poolRightHeaderOuter.append(poolRightHeaderTemplate(poolRightHeaderData));

                            $.each(response.pool_team_first.pool_standard_rotto_results, function (i, v) {
                                $('#score-setting-' + response.pool_team_first.id + '-' + v.pool_scoring_field_id).text(hpApp.twoDecimal(v.value));
                            });
                            //setting widths
                            hpApp.boxStandardStatsSetting();
                        } else if (poolToShow == 12 || poolToShow == 13) {
                            var poolLink = $('#pool-overview-link').attr('href'),
                                    newPoolLink = poolLink.replace(0, response.id),
                                    poolEditLink = $('#pool-edit-link').attr('href'),
                                    newPoolEditLink = poolEditLink.replace(0, response.id),
                                    poolDeleteLink = $('#pool-delete-link').attr('href'),
                                    newPoolDeleteLink = poolDeleteLink.replace(0, response.id),
                                    poolLivedraftLink = $('#pool-livedraft-link').attr('href'),
                                    newpoolLivedraftLink = poolLivedraftLink.replace('ld', response.id),
                                    poolPredraftLink = $('#pool-predraft-link').attr('href'),
                                    newpoolPredraftLink = poolPredraftLink.replace('pd', response.id);

                            var poolRightHeaderOuter = $('#pool-right-header-outer'),
                                    poolRightHeaderTemplate = $('#pool-right-header-template').html();
                            poolRightHeaderTemplate = _.template(poolRightHeaderTemplate);
                            var poolRightHeaderData = {
                                poolMatchup: response.pool_matchup,
                                poolName: response.name,
                                poolId: response.id,
                                poolComplete: response.is_completed,
                                poolLink: newPoolLink,
                                poolEditLink: newPoolEditLink,
                                poolDeleteLink: newPoolDeleteLink,
                                poolUser: response.user,
                                poolLogo: response.logo,
                                poolLiveDraftSetting: response.live_draft_setting,
                                poolLiveDraftActive: response.live_draft_player,
                                poolLivedraftLink: newpoolLivedraftLink,
                                poolPredraftLink: newpoolPredraftLink,
                                siteLink: api_url
                            };
                            poolRightHeaderOuter.append(poolRightHeaderTemplate(poolRightHeaderData));
                        }
                    });
                    //Adding pagination
                    var poolPaginationOuter = $('#pool-pagination'),
                            poolPaginationTemplate = $('#pool-pagination-template').html();
                    poolPaginationTemplate = _.template(poolPaginationTemplate);
                    var poolPaginationData = {
                        currentPage: resp.current_page,
                        lastPage: resp.last_page,
                        prevPageUrl: resp.prev_page_url,
                        nextPageUrl: resp.next_page_url
                    };
                    $('#pool-pagination').empty();
                    poolPaginationOuter.append(poolPaginationTemplate(poolPaginationData));
                } else {
                    //empty div
                    $('#pool-right-header-outer').empty().append($('#no-pools-present').show());
                }
                if (siteloader == true) {
                    //arranging height of left and sidebar
                    setTimeout(function () {
                        hpApp.arrangeLeftHeight($('#pool--content'));
                        hpApp.arrangeRightSidebar();

                        $('.scorer-loader-logo').hide();
                        $('.site-container').css({'background-color': '#fff'});
                        $('#pool--content').css({'opacity': 1});
                        $('.adblocks').css({'opacity': 1});
                    }, 500);
                }
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
    }
    getUserPools(poolOverviewUrl, true);
    //END getting user pool

    $(window).resize(function () {
        hpApp.arrangeRightSidebar();

        if ($(window).width() > 768) {
            hpApp.arrangeLeftHeight($('#pool--content'));
        }

        //Setting widths of h2h pools
        hpApp.h2hStatsSetting();
        //Setting widths of box and standard pools
        hpApp.boxStandardStatsSetting();
    });
    //Pagination links event handler
    $(document).on('click', '#page-list a, #get-pool-link', function (e) {
        var poolOverviewUrl = $(this).attr('href');
        getUserPools(poolOverviewUrl, false);
        e.preventDefault();
    });
    //END Pagination links event handler
    //Pool delete
    $(document).on('click', '#pool-destroy', function (e) {
        var $this = $(this),
                url = $this.attr('href');

        swal({title: "Are you sure?",
            text: "You want to delete this pool?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            customClass: 'sweat-alert-confirm'
        },
                function () {
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        dataType: 'json',
                        success: function (response) {
                            $this.parent().parent().parent().remove();
                            swal({
                                title: 'Deleted!',
                                text: 'Pool has been deleted.',
                                type: 'success',
                                customClass: 'sweat-alert-confirm'
                            });
                        },
                        error: function (error) {
                            hpApp.ajaxSwalError(error);
                        }
                    });
                });

        e.preventDefault();
    });
    //END Pool delete
});