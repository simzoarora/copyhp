$(document).ready(function () {
    //Append advertisement in post
    var adBlock = $('.ad-small-block');
    adBlock.insertAfter('.single_post--post .post-body p:nth-child(1)');
    setTimeout(function () {
        adBlock.fadeIn();
    }, 500);
    //END Append advertisement in post
    if ($(window).width() > 768) {
        hpApp.arrangeLeftHeight($('.blog-post'));

        //loader
        var checkHeight = setInterval(function () {
            if ($('.upcoming-tweets').height() >= '180') {
                //Arranging height of left and sidebar  
                setTimeout(function () {
                    hpApp.arrangeLeftHeight($('.blog-post'));
                }, 50);
                clearInterval(checkHeight);
            }
        }, 200);
        //END loader
    }
    //Right sidebar width calc from right 
    hpApp.arrangeRightSidebar();
    $(window).resize(function () {
        hpApp.arrangeRightSidebar();

        if ($(window).width() > 768) {
            hpApp.arrangeLeftHeight($('.blog-post'));
        }
    });
    //END Right sidebar width calc from right 
    //Blog comment submit
    $(document).on('submit', '#post-comment-form', function (e) {
        var $this = $(this),
                comment = $this.find('[name="comment"]').val();
        $this.find('button').attr('disabled', 'disabled');
        if (comment.length > 0) {
            $.ajax({
                url: $this.attr('action'),
                type: 'POST',
                dataType: 'json',
                data: $this.serialize(),
                success: function (response) {
                    $this.find('button').removeAttr('disabled');
                    $this.find('[name="comment"]').val('');

                    $this.parent().find('.all-comments').prepend('<div class="each-comment">' + comment + '<div><span class="name">' + response.name + '</span><span class="created">' + response.created + '</span></div></div>');
                },
                error: function (error) {
                    $('#post-comment-form').find('button').removeAttr('disabled');
                    hpApp.ajaxSwalError(error);
                }
            });
        }
        e.preventDefault();
    });
    //END Blog comment submit

    $('#facebook-share > i > div').sharrre({
        share: {
            facebook: true
        },
        enableHover: false,
        enableTracking: true,
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('facebook');
        }
    });

    $('#twitter-share > i > div').sharrre({
        share: {
            twitter: true
        },
        enableHover: false,
        enableTracking: true,
        buttons: {
            twitter: {
                via: '_anikgoel'
            }
        },
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('twitter');
        }
    });
    
    $('#google-share > i > div').sharrre({
        share: {
            googlePlus: true
        },
        enableHover: false,
        enableTracking: true,
        click: function(api, options){
            api.simulateClick();
            api.openPopup('googlePlus');
        }
    });
});