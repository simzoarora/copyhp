$(document).ready(function () {
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    function getInjuryReport(url) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                $('#injury-report-all').empty();
                if (!$.isEmptyObject(response)) {
                    var appendlocation = $('#injury-report-all'),
                            injury_report_template = $('#injury_report_data').html();
                    injury_report_template = _.template(injury_report_template);
                    var injury_report_data = {
                        injuries: response
                    };
                    appendlocation.append(injury_report_template(injury_report_data));
                } else {
                    $('#injury-report-all').append('<p class="no-result-present">'+injury_report.messages.no_results_found+'</p>')
                }

                //arranging height of left and sidebar
                setTimeout(function () {
                    hpApp.arrangeLeftHeight($('.injury-report'));

                    $('.scorer-loader-logo').hide();
                    $('.site-container').css({'background-color': '#fff'});
                    $('#pool--content').css({'opacity': 1});
                }, 1000);
            },
            error: function (err) {
                $('#injury-report-all').append('<p class="no-result-present">'+injury_report.messages.no_injury_report_present+'</p>');
            }
        });
    }
    getInjuryReport(url);

    if ($(window).width() > 768) {
        hpApp.arrangeLeftHeight($('.injury-report'));
        //loader
        var checkHeight = setInterval(function () {
            if ($('.upcoming-tweets').height() >= '180') {
                //Arranging height of left and sidebar 
                setTimeout(function () {
                    hpApp.arrangeLeftHeight($('.injury-report'));
                }, 50);
                clearInterval(checkHeight);
            }
        }, 200);
        //END loader
    }
    //Right sidebar width calc from right 
    hpApp.arrangeRightSidebar();
    $(window).resize(function () {
        hpApp.arrangeRightSidebar();

        if ($(window).width() > 768) {
            hpApp.arrangeLeftHeight($('.injury-report'));
        }
    });
});