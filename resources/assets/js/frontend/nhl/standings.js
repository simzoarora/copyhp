$(document).ready(function () {
    var firstLoad = true;
    function hideLoader() {
        if (firstLoad) {
            $('body').animate({scrollTop: 0});
            $('.scorer-loader-logo').hide();
            $('.site-container').css({'background-color': '#fff'});
            $('#main-content').css({'opacity': 1});
            firstLoad = false;
        }
    }
    $('.page-sub-nav a').on('click', function () {
        $this = $(this);
        $this.addClass('selected').parent().siblings().find('a').removeClass('selected');
        $("#" + $this.data('show')).show().siblings().hide();
    });

    function empty_all(append_text) {
        $('#divisions-all').empty();
        $('#conference-all').empty();
        $('#overall-all').empty();
        if (append_text) {
            $('#divisions-all').append(append_text);
            $('#conference-all').append(append_text);
            $('#overall-all').append(append_text);
        }
    }
    function getStandings(url) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                console.log(response);
                if (!$.isEmptyObject(response)) {
                    if (!$.isEmptyObject(response.league_standings)) {
                        empty_all();
                        append_data(response, response.division_standings, "divisions-all");
                        append_data(response, response.conference_standings, "conference-all");
                        append_data(response, response.league_standings, "overall-all");
                    } else {
                        empty_all('<p class="no-result-present">' + standings.messages.no_results_found_for_selected_year + '</p>');
                    }
                }
                hideLoader();
            },
            error: function (err) {
                hideLoader();
                empty_all('<p class="no-result-present">' + standings.messages.internal_server_error + '</p>');
            }
        });
    }
    function append_data(response, data, append_location) {
        var appendlocation = $('#' + append_location + ''),
                standingsTemplate = $('.standings-template').html();
        standingsTemplate = _.template(standingsTemplate);
        if (append_location == "divisions-all") {
            $.each(data, function (conference_name, list) {
                appendlocation.append('<h3 class="pool--main-heading">' + conference_name + '</h3>');
                $.each(list, function (division_name, team_list) {
                    var team_data = [];
                    var d_name;
                    $.each(team_list, function (i, team_details) {
                        var team_data_single = {};
                        team_data_single.official = response.league_standings[team_details.team_id];
                        team_data_single.extra = response.extra_league_standings[team_details.team_id];
                        team_data[parseInt(team_details.division_ranking) - 1] = team_data_single;
                    });
                    team_data = team_data.filter(function (item) {
                        return item !== undefined;
                    });
                    d_name = division_name;
                    var teamDetailsData = {
                        team_data: team_data,
                        division_name: d_name,
                    };
                    console.log(team_data);
                    appendlocation.append(standingsTemplate(teamDetailsData));
                });
            });
        } else if (append_location == "conference-all") {
            $.each(data, function (conference_name, list) {
                var team_data = [];
                var d_name;
                $.each(list, function (division_name, team_list) {
                    var team_data_single = {};
                    team_data_single.official = response.league_standings[team_list.team_id];
                    team_data_single.extra = response.extra_league_standings[team_list.team_id];
                    team_data[parseInt(team_list.conference_ranking) - 1] = team_data_single;
                });
                team_data = team_data.filter(function (item) {
                    return item !== undefined;
                });
                d_name = "division_name";
                var teamDetailsData = {
                    team_data: team_data,
                    division_name: conference_name,
                };
                appendlocation.append(standingsTemplate(teamDetailsData));
            });
        } else if (append_location == "overall-all") {
            var team_data = [];
            $.each(data, function (index, team_detail) {
                var team_data_single = {};
                team_data_single.official = team_detail;
                team_data_single.extra = response.extra_league_standings[team_detail.team_id];
                team_data[parseInt(team_detail.rank) - 1] = team_data_single;
            });
            team_data = team_data.filter(function (item) {
                return item !== undefined;
            });
            var teamDetailsData = {
                team_data: team_data,
                division_name: "Overall",
            };
            appendlocation.append(standingsTemplate(teamDetailsData));
        }
    }
    $('#select_year').on('change', function () {
        var currentlocation = $(location).attr('href');
        var selected_year = $('#select_year option:selected').text();
//        var new_location=currentlocation;
        var new_location = currentlocation.split("/");
        var new_ajax_url = getStandingData_url.split("/");
//        if year is present in the old url remove that year
        if (new_location[4]) {
            new_location = currentlocation.replace("/" + new_location[4], "");
        } else {
            new_location = currentlocation;
        }
//        if year is present in the old ajax url       
        if (new_ajax_url[4]) {
            new_ajax_url = getStandingData_url.replace("/" + new_ajax_url[4], "");
        } else {
            new_ajax_url = getStandingData_url;
        }
        window.history.pushState('page2', 'Title', "" + new_location + "/" + selected_year + "");
        getStandings(new_ajax_url + "/" + selected_year);
    });
    function check_url_has_year() {
        var currentlocation = $(location).attr('href');
        var splitted_location = currentlocation.split("/");
        if (splitted_location[4]) {
            $('#select_year').find('option:contains("' + splitted_location[4] + '")').prop("selected", true);
            getStandings(getStandingData_url + "/" + splitted_location[4]);
        } else {
            getStandings(getStandingData_url);
        }
        $('.select-field').selectBoxIt();
    }
    check_url_has_year();
}); 