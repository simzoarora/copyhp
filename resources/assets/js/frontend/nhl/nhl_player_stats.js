var pageLoaded = true;
function getPlayerStats(url) {
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $('#player-stats-all').empty();
            if (!$.isEmptyObject(response.data)) {
                var appendlocation = $('#player-stats-all'),
                        player_stats_template = $('#player-stats-template').html();
                player_stats_template = _.template(player_stats_template);
                var player_data = {
                    players: response.data
                };
                appendlocation.append(player_stats_template(player_data));
                NAMESPACE.PAGINATE(response.current_page, response.last_page);

                if (pageLoaded == true) {
                    pageLoaded = false;
                    $('.scorer-loader-logo').hide();
                    $('.site-container').css({'background-color': '#fff'});
                    $('#pool--content').css({'opacity': 1}).fadeIn('500');
                }
            } else {
                $('.post-pagination').hide();
                $('#player-stats-all').append('<tr class="no-result-present"><td colspan="30">'+player_stats.messages.no_players_found+'</td></tr>');
                if (pageLoaded == true) {
                    pageLoaded = false;
                    $('.scorer-loader-logo').hide();
                    $('.site-container').css({'background-color': '#fff'});
                    $('#pool--content').css({'opacity': 1}).fadeIn('500');
                }
            }
        },
        error: function (err) {
            $('#player-stats-all').empty().append('<tr class="no-result-present"><td colspan="30">'+player_stats.messages.no_players_found+'</td></tr>>');
        }
    });
}
var NAMESPACE = {
    //    display pagination function
    PAGINATE: function paginate(current_page, last_page) {
        $paginateDiv = $('#page-list');
        if (last_page > 0) {
            $paginateDiv.empty();
            $('.post-pagination').show();
            var single_anchor_width = 30;
            for (var i = 1; i <= last_page; i++) {
                if (i == current_page) {
                    $paginateDiv.append('<a class="active" data-page=' + i + '>' + i + '</a>');
                } else {
                    $paginateDiv.append('<a data-page=' + i + '>' + i + '</a>');
                }
            }
            NAMESPACE.PAGINATE_ANIMATE(current_page, false);
        } else {
            $('.post-pagination').hide();
        }
    },
    //    END display pagination function
    PAGINATE_ANIMATE: function pagination_animate(activePage, status) {
        var pageList = $('#page-list');
        var lastPage = parseInt(pageList.find('a:last-child').data('page'));
        pageList.find('[data-page=' + activePage + ']').addClass('active').siblings().removeClass('active');
        if (activePage >= 3) {
            pageList.animate({marginLeft: -(activePage - 3) * 30}, 300);
        }
        if (activePage == 2) {
            pageList.animate({marginLeft: 0}, 300);
        }
        if (activePage != 1) {
            $('#pagination-left').removeClass('opacity0');
            $('#pagination-first').removeClass('opacity0');
        }
        if (lastPage <= 5) {
            $('.main-pagination').outerWidth(30 * lastPage);
            $('#pagination-right').addClass('opacity0');
            $('#pagination-last').addClass('opacity0');
        } else {
            $('#pagination-right').removeClass('opacity0');
            $('#pagination-last').removeClass('opacity0');
            $('.main-pagination').outerWidth(160);
        }
        if (activePage == lastPage) {
            $('#pagination-right').addClass('opacity0');
            $('#pagination-last').addClass('opacity0');
        }
        if (activePage == 1) {
            pageList.animate({marginLeft: 0}, 300);
            $('#pagination-left').addClass('opacity0');
            $('#pagination-first').addClass('opacity0');
        }
        if (status) {
            NAMESPACE.CHANGE_URL("paginate", "&page=" + activePage);
        }
    }, //    main change url function COMMON
    CHANGE_URL:
            function changeUrl(event, add_url) {
                var season_val = $('#season').val(),
                        position_val = $('#position').val(),
                        teams_val = $('#team').val(),
                        game_type_val = $('#game_type').val(),
                        search_input_val = $('#search-input').val(),
                        status = false,
                        new_url = url.split("?");
//        if season is selected
                if (position_val != undefined && season_val != undefined && game_type_val != undefined) {
                    new_url = new_url[0] + "?season_id=" + season_val + '&game_type=' + game_type_val + '&position=' + position_val;
                    status = true;
                }
                if (teams_val != undefined && teams_val != 0) {
                    new_url = new_url + "&team=" + teams_val;
                }
                //  if custom dates are coming
                if (event == 'paginate') {
                    new_url = new_url + add_url;
                } else {
                    $('#page-list').animate({marginLeft: 0}, 300);
                }
//        check whether table has sorting applied or not
                if ($('#main-table').find('th').hasClass('asc')) {
                    $this = $('#main-table').find('.asc');
                    if ($this.data("stat_order_key")) {
                        new_url = new_url + "&order_type=asc&order=" + $this.data("sortname") + "&stat_order_key=" + $this.data("stat_order_key");
                    } else {
                        new_url = new_url + "&order_type=asc&order=" + $this.data("sortname");
                    }
                }
                if ($('#main-table').find('th').hasClass('desc')) {
                    $this = $('#main-table').find('.desc');
                    if ($this.data("stat_order_key")) {
                        new_url = new_url + "&order_type=desc&order=" + $this.data("sortname") + "&stat_order_key=" + $this.data("stat_order_key");
                    } else {
                        new_url = new_url + "&order_type=desc&order=" + $this.data("sortname");
                    }
                }
//        end check whether table has sorting applied or not
                if (status) {
                    getPlayerStats(new_url);
                    var new_browser_url = new_url.replace(get_url_name, normal_url_name);
                    window.history.pushState('title', '', new_browser_url);
                }
            }
};
$(document).ready(function () {
    $('.select-field').selectBoxIt();
    var highlighted_all = "";
    for (var i = 1; i < 30; i++) {
        highlighted_all = highlighted_all + "highlighted-" + i + " ";
    }
//   END main change url function COMMON
//    search on SORTING IN TABLE
    $(document).on('click', 'th', function () {
        var $this = $(this);
        if ($this.data("sortname")) {
            if ($this.hasClass('desc')) {
//                earlier descending. changed it to ascending
                $this.removeClass('desc').addClass('asc');
            } else if ($this.hasClass('asc')) {
//                earlier ascending. changed to descending
                $this.removeClass('asc').addClass('desc');
            }
//                earlier NOTHING. changed it to ascending
            else {
                $this.addClass('asc');
            }
            $('#main-table').removeClass(highlighted_all).addClass('highlighted-' + parseInt($this.index() + 1));
            $this.siblings().removeClass('asc desc');
            NAMESPACE.CHANGE_URL();
        }
    });
//    END search on SORTING IN TABLE
//    search on clicking pagination buttons
//    right pagination arrow on click
    $('#pagination-right').on('click', function () {
        var activePage = parseInt($('#page-list').find('.active').data('page'));
        NAMESPACE.PAGINATE_ANIMATE(activePage + 1, true);
    });
    //    left pagination arrow on click
    $('#pagination-left').on('click', function () {
        var activePage = parseInt($('#page-list').find('.active').data('page'));
        NAMESPACE.PAGINATE_ANIMATE(activePage - 1, true);
    });
    $('#pagination-first').on('click', function () {
        NAMESPACE.PAGINATE_ANIMATE(1, true);
    });
    $('#pagination-last').on('click', function () {
        NAMESPACE.PAGINATE_ANIMATE(parseInt($('#page-list').find('a:last-child').data('page')), true);
    });
    //    pagination NUMBERS arrow on click
    $('.post-pagination').on('click', 'a', function () {
        var activePage = parseInt($(this).data('page'));
        NAMESPACE.PAGINATE_ANIMATE(activePage, true);
    });

//    END search on clicking pagination buttons
//    search on entering text in the input field
    $('#season').on('change', function () {
        NAMESPACE.CHANGE_URL();
    });
    $('#position').on('change', function () {
        NAMESPACE.CHANGE_URL();
        if ($(this).val() == goalie_position_id) {
            $('#main-table').addClass('goalie');
        } else {
            $('#main-table').removeClass('goalie');
        }
    });
    $('#team').on('change', function () {
        NAMESPACE.CHANGE_URL();
    });
    $('#game_type').on('change', function () {
        NAMESPACE.CHANGE_URL();
    });
//    END search on entering text in the input field
    if (season_id && game_type && position) {
        // sorting
        if (order_type) {
            if (order) {
                if (stat_order_key) {
                    $('#main-table').find('th').each(function () {
                        var $this = $(this);
                        if ($this.data('stat_order_key') == stat_order_key) {
                            $this.addClass(order_type);
                            $('#main-table').addClass('highlighted-' + parseInt($this.index() + 1));
                        }
                    });
                } else {
                    $('#main-table').find('th').each(function () {
                        $this = $(this);
                        if ($this.data('sortname') == order) {
                            $this.addClass(order_type);
                            $('#main-table').addClass('highlighted-' + parseInt($this.index() + 1));
                        }
                    });
                }
            }
        }
        // END sorting
        if (season_id) {
            if (page) {
                NAMESPACE.CHANGE_URL("paginate", "&page=" + page);
            } else {
                NAMESPACE.CHANGE_URL();
            }
        }
        if (position == goalie_position_id) {
            $('#main-table').addClass('goalie');
        }
    }
//        nothing is coming
    else {
        NAMESPACE.CHANGE_URL();
    }
});