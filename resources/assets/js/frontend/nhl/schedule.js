$(document).ready(function () {
    $('.select-field').selectBoxIt();

    var firstLoad = true;
    function hideLoader() {
        if (firstLoad) {
            $('body').animate({scrollTop: 0});
            $('.scorer-loader-logo').hide();
            $('.site-container').css({'background-color': '#fff'});
            $('#main-content').css({'opacity': 1});
            firstLoad = false;

            if ($(window).width() > 768) {
                hpApp.arrangeLeftHeight($('.schedule-container'));
                //loader
                var checkHeight = setInterval(function () {
                    if ($('.upcoming-tweets').height() >= '180') {
                        //Arranging height of left and sidebar 
                        setTimeout(function () {
                            hpApp.arrangeLeftHeight($('.schedule-container'));
                        }, 50);
                        clearInterval(checkHeight);
                    }
                }, 200);
            }
        }
    }
    function get_new_schedule() {
        var new_url = url.replace(get_url_path, "");
        if ($('#teams').val() != 0) {
            new_url = new_url + "/" + $('#teams').val();
        }
        new_url = new_url + get_url_path;
        if ($('#datetimepicker').val() != "") {
            new_url = new_url + "/" + $('#datetimepicker').val();
        }
        var new_browser_url = new_url.replace(get_url_path, page_url_path);
        window.history.pushState('page2', 'Title', new_browser_url);
        getSchedule(new_url);
    }
    $('#teams, #competition-type').on('change', function () {
        get_new_schedule();
    });
    $('#datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent: false
    }).on('dp.change', function () {
        get_new_schedule();
    });
    function getSchedule(url) {
        $.ajax({
            url: url,
            type: 'GET',
            data: {
                competition_type: $('#competition-type').val()
            },
            dataType: 'json',
            success: function (response) {
                $('#matchup-all').empty();
                if (!$.isEmptyObject(response)) {
                    var appendlocation = $('#matchup-all'),
                            matchup_data_template = $('#matchup_data').html();
                    matchup_data_template = _.template(matchup_data_template);
                    var schedule_data = {
                        schedules: response
                    };
                    appendlocation.append(matchup_data_template(schedule_data)).promise().done(function(){
                         hpApp.arrangeLeftHeight($('.schedule-container'));
                    });

                    //Making result heading visible on schedule tables
                    $.each(response, function (index, schedule) {
                        if (typeof (index) == 'string') {
                            var scheduleResultClass = hpApp.replaceAll(index, ' ', '-'),
                                    scheduleResultClass = hpApp.replaceAll(scheduleResultClass, ',', '');
                            $.each(schedule, function (i, v) {
                                if (v.home_team_score != null && v.away_team_score != null) {
                                    $('.' + scheduleResultClass).show();
                                }
                            });
                        }
                    });

                    //END
                } else {
                    $('#matchup-all').append('<p class="no-result-present">' + schedule.messages.no_schedule_present + '</p>').promise().done(function(){
                         hpApp.arrangeLeftHeight($('.schedule-container'));
                    });
                }
                hideLoader();
            },
            error: function (err) {
                hideLoader();
                $('#matchup-all').empty().append('<p class="no-result-present">' + schedule.messages.no_schedule_present + '</p>').promise().done(function(){
                         hpApp.arrangeLeftHeight($('.schedule-container'));
                    });
            }
        });
    }
    function isValidDate(str) {
        var d = moment(str, 'YYYY-MM-DD');
        if (d == null || !d.isValid())
            return false;
        return str.indexOf(d.format('YYYY-MM-DD')) >= 0
    }
    function check_url_has_attributes(loc) {
        var currentlocation = $(location).attr('href');
        if (loc) {
            currentlocation = loc;
        }
        var splitted_location = currentlocation.split("/");
        var status = true;
        //only date is coming
        if (splitted_location.length == 5 && splitted_location[3] == page_url_path.replace('/', '')) {
            if (isValidDate(splitted_location[4])) {
                $('#datetimepicker').val(splitted_location[4]);
            } else {
                status = false;
            }
        }
        //only team is coming
        else if (splitted_location.length == 5 && splitted_location[4] == page_url_path.replace('/', '')) {
            var selectBox = $("#teams").data("selectBox-selectBoxIt");
            selectBox.selectOption(splitted_location[3]);
        }
        //date and time both of them are coming
        else if (splitted_location.length == 6) {
            var selectBox = $("#teams").data("selectBox-selectBoxIt");
            selectBox.selectOption(splitted_location[3]);
            if (isValidDate(splitted_location[5])) {
                $('#datetimepicker').val(splitted_location[5]);
            } else {
                status = false;
            }
        }
        if (status) {
            get_new_schedule();
        } else {
            $('#matchup-all').append('<p class="no-result-present">' + schedule.messages.invalid_date_provided + '</p>')
        }
    }
    check_url_has_attributes();
    window.onpopstate = function (e) {
        check_url_has_attributes($(location).attr('href'));
    };

    hpApp.arrangeRightSidebar();
    $(window).resize(function () {
        hpApp.arrangeRightSidebar();

        if ($(window).width() > 768) {
            hpApp.arrangeLeftHeight($('.pool-type--content'));
        }
    });
});