$(document).ready(function () {
    if ($(window).width() > 768) {
        hpApp.arrangeLeftHeight($('.termsofuse--content'));

        //loader 
        var checkHeight = setInterval(function () {
            if ($('.upcoming-tweets').height() >= '180') {
                //Arranging height of left and sidebar 
                setTimeout(function () {
                    hpApp.arrangeLeftHeight($('.termsofuse--content'));
                }, 50);
                clearInterval(checkHeight);
            }
        }, 200);
        //END loader
    }
    //Right sidebar width calc from right 
    hpApp.arrangeRightSidebar();
    $(window).resize(function () {
        hpApp.arrangeRightSidebar();

        if ($(window).width() > 768) {
            hpApp.arrangeLeftHeight($('.termsofuse--content'));
        }
    });
    //END Right sidebar width calc from right 
});