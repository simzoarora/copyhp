$(document).ready(function () {
    if ($(window).width() > 768) {
        hpApp.arrangeLeftHeight($('.pool-type--content'));
        //loader
        var checkHeight = setInterval(function () {
            if ($('.upcoming-tweets').height() >= '180') {
                //Arranging height of left and sidebar 
                setTimeout(function () {
                    hpApp.arrangeLeftHeight($('.pool-type--content'));
                }, 50);
                clearInterval(checkHeight);
            }
        }, 200);
    }
    //END loader
    //Right sidebar width calc from right 
    hpApp.arrangeRightSidebar();
    $(window).resize(function () {
        hpApp.arrangeRightSidebar();

        if ($(window).width() > 768) {
            hpApp.arrangeLeftHeight($('.pool-type--content'));
        }
    });
    //END Right sidebar width calc from right 
    if (typeof location.hash != 'undefined') {
        var sel = $(location.hash);
        if (sel != '') {
            $('body').animate({
                scrollTop: sel.offset().top
            });
        }
    }
});