$(document).ready(function () {
    //accordion color change
    $('.panel-title').on('click', function () {
        $(this).parents('.panel').siblings().find('.panel-title').removeClass('opened-faq');
        $(this).toggleClass('opened-faq');
    });
    //accordion color change-ends 
    //
    //Arranging height of left and sidebar 
    if ($(window).width() > 768) {
        hpApp.arrangeLeftHeight($('.faq--content'));
        //loader
        var checkHeight = setInterval(function () {
            if ($('.upcoming-tweets').height() >= '180') {
                //Arranging height of left and sidebar 
                setTimeout(function () {
                    hpApp.arrangeLeftHeight($('.faq--content'));
                }, 50);
                clearInterval(checkHeight);
            }
        }, 200);
        //END loader
    }
    //Right sidebar width calc from right 
    hpApp.arrangeRightSidebar();
    $(window).resize(function () {
        hpApp.arrangeRightSidebar();

        if ($(window).width() > 768) {
            hpApp.arrangeLeftHeight($('.faq--content'));
        }
    });
    //END Right sidebar width calc from right 
}); 