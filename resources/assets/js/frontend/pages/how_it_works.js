$(document).ready(function () {
    if ($(window).width() > 768) {
        hpApp.arrangeLeftHeight($('.how-it-works--content'));
        //loader
        var checkHeight = setInterval(function () {
            if ($('.upcoming-tweets').height() >= '180') {
                //Arranging height of left and sidebar 
                setTimeout(function () {
                    hpApp.arrangeLeftHeight($('.how-it-works--content'));
                }, 50);
                clearInterval(checkHeight);
            }
        }, 200);
    }
    //END loader
    //Right sidebar width calc from right 
    hpApp.arrangeRightSidebar();
    $(window).resize(function () {
        hpApp.arrangeRightSidebar();

        if ($(window).width() > 768) {
            hpApp.arrangeLeftHeight($('.how-it-works--content'));
        }
    });
    //END Right sidebar width calc from right 
});