$(document).ready(function () {
    //Taking page to top if not
    $('body').animate({scrollTop: 0});

    function getDayScore(selectedDate) {
        $.ajax({
            url: selectedDate,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (!$.isEmptyObject(response)) {
                    //empty div
                    $('#team-details-all').empty();
                    $.each(response, function (i, v) {
                        //top team details data 
                        var response = v,
                                homeTeamId = response.home_team_id,
                                awayTeamId = response.away_team_id,
                                homeTeamPeriod = [];
                        homeTeamPeriod[1] = 0;
                        homeTeamPeriod[2] = 0;
                        homeTeamPeriod[3] = 0;
                        homeTeamPeriod[4] = 0;
                        homeTeamPeriod[5] = 0;
                        var awayTeamPeriod = [];
                        awayTeamPeriod[1] = 0;
                        awayTeamPeriod[2] = 0;
                        awayTeamPeriod[3] = 0;
                        awayTeamPeriod[4] = 0;
                        awayTeamPeriod[5] = 0;
                        if (!$.isEmptyObject(response.competition_goals)) {
                            $.each(response.competition_goals, function (i, v) {
                                if (homeTeamId == v.scorer.team_id) {
                                    if (homeTeamPeriod[v.period] == 0) {
                                        homeTeamPeriod[v.period] = 1;
                                    } else {
                                        homeTeamPeriod[v.period] = homeTeamPeriod[v.period] + 1;
                                    }
                                }
                                if (awayTeamId == v.scorer.team_id) {
                                    if (awayTeamPeriod[v.period] == 0) {
                                        awayTeamPeriod[v.period] = 1;
                                    } else {
                                        awayTeamPeriod[v.period] = awayTeamPeriod[v.period] + 1;
                                    }
                                }
                            });
                        }
                        var homeTeamShootoutSum = 0, awayTeamShootoutSum = 0;
                        if (!$.isEmptyObject(response.competition_shootouts)) {
                            $.each(response.competition_shootouts, function (i, v) {
                                if (homeTeamId == v.skater_season.team_id) {
                                    homeTeamShootoutSum = parseInt(homeTeamShootoutSum) + parseInt(v.scored);
                                }
                                if (awayTeamId == v.skater_season.team_id) {
                                    awayTeamShootoutSum = parseInt(awayTeamShootoutSum) + parseInt(v.scored);
                                }
                            });
                            homeTeamPeriod[5] = homeTeamShootoutSum;
                            awayTeamPeriod[5] = awayTeamShootoutSum;
                        }
                        //creating stats
                        if (!$.isEmptyObject(response.home_team.league_standing)) {
                            var homeOvertime = parseInt(response.home_team.league_standing.games_lost_overtime) + parseInt(response.home_team.league_standing.games_lost_shootout),
                                    homeTeamStats = response.home_team.league_standing.games_won + '-' + response.home_team.league_standing.games_lost + '-' + homeOvertime;
                        } else {
                            var homeTeamStats = '';
                        }
                        if (!$.isEmptyObject(response.away_team.league_standing)) {
                            var awayOvertime = parseInt(response.away_team.league_standing.games_lost_overtime) + parseInt(response.away_team.league_standing.games_lost_shootout),
                                    awayTeamStats = response.away_team.league_standing.games_won + '-' + response.away_team.league_standing.games_lost + '-' + awayOvertime;
                        } else {
                            var awayTeamStats = '';
                        }

                        var teamDetailsOuter = $('#team-details-all'),
                                teamDetailsTemplate = $('.team-details-template').html();
                        teamDetailsTemplate = _.template(teamDetailsTemplate);
                        var teamDetailsData = {
                            competitionId: response.id,
                            formattedMatchDate: response.formatted_match_date,
                            homeTeamFirstName: response.home_team.first_name,
                            homeTeamNickName: response.home_team.nick_name,
                            homeTeamShortName: response.home_team.short_name,
                            homeTeamLogo: api_url + '/img/nhl_logos/' + response.home_team.logo,
                            homeTeamScore: response.home_team_score,
                            homeTeamId: response.home_team_id,
                            homeTeamStats: homeTeamStats,
                            awayTeamFirstName: response.away_team.first_name,
                            awayTeamNickName: response.away_team.nick_name,
                            awayTeamShortName: response.away_team.short_name,
                            awayTeamLogo: api_url + '/img/nhl_logos/' + response.away_team.logo,
                            awayTeamScore: response.away_team_score,
                            awayTeamId: response.away_team_id,
                            awayTeamStats: awayTeamStats,
                            competitionGoals: response.competition_goals,
                            homeTeamGoals: homeTeamPeriod,
                            awayTeamGoals: awayTeamPeriod
                        };
                        teamDetailsOuter.append(teamDetailsTemplate(teamDetailsData));
                    });
                } else {
                    //empty div
                    $('#team-details-all').empty();
                    $('#team-details-all').append('<p class="no-game-p">' + scores.messages.no_games_scheduled + '</p>');
                }

                //checking all matches goal player are present
                $('.team-details .goal-players').each(function (i, v) {
                    var $this = $(this);
                    if (hpApp.isElementEmpty($this)) {
                        $this.parent().remove();
                    }
                });

                //arranging height of left and sidebar
                setTimeout(function () {
                    hpApp.arrangeRightSidebar();
                    hpApp.arrangeLeftHeight($('#scores'));

                    $('.scorer-loader-logo').hide();
                    $('.site-container').css({'background-color': '#fff'});
                    $('#scores').css({'opacity': 1}).fadeIn('500');
                    $('.adblocks').css({'opacity': 1}).fadeIn('500');
                }, 400);
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
    }
    //Get current date data
    getDayScore(dayScoresUrl);
    //END Get current date data
    //On click get data of that date
    //dateTimepicker date select
    $('#datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent: false
    }).on('dp.change', function () {
        var selectedDate = $(this).val(),
                dayScoresUrlNew = dayScoresUrl.replace(CurrentDate, selectedDate);
        getDayScore(dayScoresUrlNew);
    });
    //END On click get data of that date

    //Twitter data append
    var checkHeight = setInterval(function () {
        if ($('.upcoming-tweets').height() >= '180') {
            //Arranging height of left and sidebar 
            setTimeout(function () {
                hpApp.arrangeLeftHeight($('#scores'));
            }, 50);
            clearInterval(checkHeight);
        }
    }, 200);
    //END Twitter data append

    $(window).resize(function () {
        hpApp.arrangeRightSidebar();
        hpApp.arrangeLeftHeight($('#scores'));
    });
});