$(document).ready(function () {
    function fillEmptyTable(sel, message) {
        if (hpApp.isElementEmpty(sel.find('tbody'))) {
            var colspan = sel.find('thead').find('th').length;
            sel.find('tbody').append('<tr><td colspan="' + colspan + '" class="no-result-present">' + message + '</td><tr>');
        }
    }
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    //Ajax for getting data 
    $.ajax({
        url: getCompetitionScoreUrl,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            //top team details data 
            var homeTeamId = response.home_team_id,
                    awayTeamId = response.away_team_id,
                    homeTeamPeriod = [];
            homeTeamPeriod[1] = 0;
            homeTeamPeriod[2] = 0;
            homeTeamPeriod[3] = 0;
            homeTeamPeriod[4] = 0;
            homeTeamPeriod[5] = 0;
            var awayTeamPeriod = [];
            awayTeamPeriod[1] = 0;
            awayTeamPeriod[2] = 0;
            awayTeamPeriod[3] = 0;
            awayTeamPeriod[4] = 0;
            awayTeamPeriod[5] = 0;
            if (!$.isEmptyObject(response.competition_goals)) {
                $.each(response.competition_goals, function (i, v) {
                    if (homeTeamId == v.scorer.team_id) {
                        if (homeTeamPeriod[v.period] == 0) {
                            homeTeamPeriod[v.period] = 1;
                        } else {
                            homeTeamPeriod[v.period] = homeTeamPeriod[v.period] + 1;
                        }
                    }
                    if (awayTeamId == v.scorer.team_id) {
                        if (awayTeamPeriod[v.period] == 0) {
                            awayTeamPeriod[v.period] = 1;
                        } else {
                            awayTeamPeriod[v.period] = awayTeamPeriod[v.period] + 1;
                        }
                    }
                });
            }
            var homeTeamShootoutSum = 0, awayTeamShootoutSum = 0;
            if (!$.isEmptyObject(response.competition_shootouts)) {
                $.each(response.competition_shootouts, function (i, v) {
                    if (homeTeamId == v.skater_season.team_id) {
                        homeTeamShootoutSum = parseInt(homeTeamShootoutSum) + parseInt(v.scored);
                    }
                    if (awayTeamId == v.skater_season.team_id) {
                        awayTeamShootoutSum = parseInt(awayTeamShootoutSum) + parseInt(v.scored);
                    }
                });
                homeTeamPeriod[5] = homeTeamShootoutSum;
                awayTeamPeriod[5] = awayTeamShootoutSum;
            } else {
                homeTeamPeriod[5] = null;
                awayTeamPeriod[5] = null;
            }

            //creating stats
            if (!$.isEmptyObject(response.home_team.league_standing)) {
                var homeOvertime = parseInt(response.home_team.league_standing.games_lost_overtime) + parseInt(response.home_team.league_standing.games_lost_shootout),
                        homeTeamStats = response.home_team.league_standing.games_won + '-' + response.home_team.league_standing.games_lost + '-' + homeOvertime;
            } else {
                var homeTeamStats = '';
            }
            if (!$.isEmptyObject(response.away_team.league_standing)) {
                var awayOvertime = parseInt(response.away_team.league_standing.games_lost_overtime) + parseInt(response.away_team.league_standing.games_lost_shootout),
                        awayTeamStats = response.away_team.league_standing.games_won + '-' + response.away_team.league_standing.games_lost + '-' + awayOvertime;
            } else {
                var awayTeamStats = '';
            }

            var teamDetailsOuter = $('.team-details-outer'),
                    teamDetailsTemplate = $('.team-details-template').html();
            teamDetailsTemplate = _.template(teamDetailsTemplate);
            var teamDetailsData = {
                formattedMatchDate: response.formatted_match_date,
                homeTeamFirstName: response.home_team.first_name,
                homeTeamNickName: response.home_team.nick_name,
                homeTeamShortName: response.home_team.short_name,
                homeTeamLogo: api_url + '/img/nhl_logos/' + response.home_team.logo,
                homeTeamScore: response.home_team_score,
                homeTeamId: response.home_team_id,
                homeTeamStats: homeTeamStats,
                awayTeamFirstName: response.away_team.first_name,
                awayTeamNickName: response.away_team.nick_name,
                awayTeamShortName: response.away_team.short_name,
                awayTeamLogo: api_url + '/img/nhl_logos/' + response.away_team.logo,
                awayTeamScore: response.away_team_score,
                awayTeamId: response.away_team_id,
                awayTeamStats: awayTeamStats,
                competitionGoals: response.competition_goals,
                homeTeamGoals: homeTeamPeriod,
                awayTeamGoals: awayTeamPeriod
            };
            teamDetailsOuter.append(teamDetailsTemplate(teamDetailsData));

            //Scoring Summary data
            var scoringSummaryOuter = $('.scoring-summary-outer'),
                    scoringSummaryTemplate = $('.scoring-summary-template').html();
            scoringSummaryTemplate = _.template(scoringSummaryTemplate);
            var scoringSummaryData = {
                homeTeamId: response.home_team_id,
                homeTeamNickName: response.home_team.nick_name,
                homeTeamShortName: response.home_team.short_name,
                awayTeamId: response.away_team_id,
                awayTeamNickName: response.away_team.nick_name,
                awayTeamShortName: response.away_team.short_name,
                competitionGoals: response.competition_goals,
                homeTeamGoals: homeTeamPeriod,
                awayTeamGoals: awayTeamPeriod
            };
            scoringSummaryOuter.append(scoringSummaryTemplate(scoringSummaryData));

            //Box Scores data
            var boxScoreOuter = $('.box-score-outer'),
                    boxScoreTemplate = $('.box-score-template').html();
            boxScoreTemplate = _.template(boxScoreTemplate);
            var boxScoreData = {
                homeTeamId: response.home_team_id,
                homeTeamNickName: response.home_team.nick_name,
                homeTeamLogo: api_url + '/img/nhl_logos/' + response.home_team.logo,
                awayTeamId: response.away_team_id,
                awayTeamNickName: response.away_team.nick_name,
                awayTeamLogo: api_url + '/img/nhl_logos/' + response.away_team.logo,
                competitionTeamStats: response.competition_team_stats
            };
            boxScoreOuter.append(boxScoreTemplate(boxScoreData));

            //adding goalie data into competition_goalie_stats and removing those goalie array from competition_skater_stats
            var competitionSkaterStatsRemove = [];
            if (!$.isEmptyObject(response.competition_goalie_stats)) {
                $.each(response.competition_goalie_stats, function (i, v) {
                    var goalie_season_id = v.player_season_id,
                            row_id = i;
                    $.each(response.competition_skater_stats, function (index, value) {
                        if (value.player_season_id == goalie_season_id) {
                            response.competition_goalie_stats[row_id]['data'] = value;
                            competitionSkaterStatsRemove.push(index);
                        }
                    });
                });
                $.each(competitionSkaterStatsRemove, function (i, v) {
                    response.competition_skater_stats[v] = [];
                });
            }

            //making a simple array from extra html length
            var teamDetails = [];
            teamDetails.push({
                team_id: response.away_team_id,
                name: response.away_team.first_name + ' ' + response.away_team.nick_name
            });
            teamDetails.push({
                team_id: response.home_team_id,
                name: response.home_team.first_name + ' ' + response.home_team.nick_name
            });
            //adding competition penalities time
            function stringToTime(time) {
                var time = time.replace('PT', '');
                var time = time.replace('S', '');
                var newTime = time.split('M');
                var mins = parseInt(newTime[0]) * 60;
                return parseInt(mins) + parseInt(newTime[1]);
            }
//            if (!$.isEmptyObject(response.competition_penalties)) { //mp -- may be needed in future for penalty_duration variable
//                $.each(response.competition_penalties, function (i, v) {
//                    var player_season_id = v.player_season_id,
//                            penalty_duration = v.penalty_duration;
//                    $.each(response.competition_skater_stats, function (i, v) {
//                        if (v.player_season_id == player_season_id) {
//                            if (response.competition_skater_stats[i]['penalty_duration'] == null) {
//                                response.competition_skater_stats[i]['penalty_duration'] = penalty_duration;
//                            } else {
//                                response.competition_skater_stats[i]['penalty_duration'] = parseInt(response.competition_skater_stats[i]['penalty_duration']) + parseInt(penalty_duration);
//                            }
//                        } else {
//                            response.competition_skater_stats[i]['penalty_duration'] = 0;
//                        }
//                    });
//                });
//            }
            //All Player data
            var teamPlayerDetailsOuter = $('.team-player-details-outer'),
                    teamPlayerDetailsTemplate = $('.team-player-details-template').html();
            teamPlayerDetailsTemplate = _.template(teamPlayerDetailsTemplate);
            var teamPlayerDetailsData = {
                competitionSkaterStats: response.competition_skater_stats,
                competitionGoalieStats: response.competition_goalie_stats,
                homeTeamId: response.home_team_id,
                homeTeamFirstName: response.home_team.first_name,
                homeTeamNickName: response.home_team.nick_name,
                awayTeamId: response.away_team_id,
                awayTeamFirstName: response.away_team.first_name,
                awayTeamNickName: response.away_team.nick_name,
                teamDetails: teamDetails
            };
            teamPlayerDetailsOuter.append(teamPlayerDetailsTemplate(teamPlayerDetailsData));

            //shootout data
            if (response.competition_shootouts.length > 0) {
                var shootoutOuter = $('.shootout-outer'),
                        shootoutTemplate = $('.shootout-template').html();
                shootoutTemplate = _.template(shootoutTemplate);
                var shootoutData = {
                    competitionShootouts: response.competition_shootouts,
                    homeTeamId: response.home_team_id,
                    homeTeamShortName: response.home_team.short_name,
                    awayTeamId: response.away_team_id,
                    awayTeamShortName: response.away_team.short_name
                };
                shootoutOuter.append(shootoutTemplate(shootoutData));
            }
            //filling empty tables 
            fillEmptyTable($('.team-player-details table:nth-child(2)'), game_details.messages.no_player_present);
            fillEmptyTable($('.team-player-details table:nth-child(3)'), game_details.messages.no_goalie_present);
            fillEmptyTable($('.team-player-details table:nth-child(5)'), game_details.messages.no_player_present);
            fillEmptyTable($('.team-player-details table:nth-child(6)'), game_details.messages.no_goalie_present);
            //checking all matches goal player are present
            $('.team-details .goal-players').each(function (i, v) {
                var $this = $(this);
                if (hpApp.isElementEmpty($this)) {
                    $this.parent().remove();
                }
            });
            //removing loader
            hpApp.arrangeRightSidebar();
            hpApp.arrangeLeftHeight($('#game_details'));

            $('.scorer-loader-logo').hide();
            $('.site-container').css({'background-color': '#fff'});
            $('#game_details').css({'opacity': 1}).fadeIn('500');
            $('.adblocks').css({'opacity': 1}).fadeIn('500');
        },
        error: function (error) {
            hpApp.ajaxSwalError(error);
        }
    });

    //Twitter data append
    var checkHeight = setInterval(function () {
        if ($('.upcoming-tweets').height() >= '180') {
            //Arranging height of left and sidebar 
            setTimeout(function () {
                hpApp.arrangeLeftHeight($('#game_details'));
            }, 50);
            clearInterval(checkHeight);
        }
    }, 200);
    //END Twitter data append

    $(window).resize(function () {
        hpApp.arrangeRightSidebar();
        hpApp.arrangeLeftHeight($('#game_details'));
    });
});