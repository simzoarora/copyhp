$(document).ready(function () {
    $('.select-field').selectBoxIt();
//datepicker
    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
//datepicker ends
    //slider
    var slideWidth = $('.player-transaction-slider .transaction').outerWidth();
    var slideHeight = $('.player-transaction-slider .transaction').outerHeight();
    var totlSlides = $('.player-transaction-slider .transaction').length;
    var totlWidth = slideWidth * totlSlides;
    var sliderWidth = $('.slider').width(totlWidth);
    var noAnimate = totlWidth - ($('.player-transaction-slider').outerWidth()) + 51;
    var moveSlide = 0;
    $('.slider--left-arrow .graphics').on('click', function () {
        //check if animated
        if ($('.player-transaction-slider .slider').is(':animated')) {
            //do nothing
            $(this).stop();
        }
        //check if it is first-slide
        else if ($(".slider").css("marginLeft") === '50px') {
            //do nothing
        }
        else {
            $('.player-transaction-slider .slider').animate({
                marginLeft: moveSlide + slideWidth + 50
            });
            moveSlide = moveSlide + slideWidth;
        }
    });
    $('.slider--right-arrow .graphics').on('click', function () {
        //check if animated
        if ($('.player-transaction-slider .slider').is(':animated')) {
            $(this).stop();
        }
        //check if it is last-slide
        else if ($(".slider").css("marginLeft") === (-noAnimate + 'px')) {
            //do nothing
        }
        else {
            $('.player-transaction-slider .slider').animate({
                marginLeft: moveSlide - slideWidth + 50
            });
            moveSlide = moveSlide - slideWidth;
        }
    });
    //slider End

});

