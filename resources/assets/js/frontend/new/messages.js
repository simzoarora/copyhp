$(document).ready(function () {
    $('.select-field').selectBoxIt();
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    //Remove loader
    var checkHeight = setInterval(function () {
        if ($('.pool-name-header').height() >= '50') {
            $('.scorer-loader-logo').hide();
            $('.site-container').css({'background-color': '#fff'});
            $('#pool--content').css({'opacity': 1}).fadeIn('500');

            clearInterval(checkHeight);
        }
    }, 200);
    //END
    $('.message-nav .bootstrap_btn_mobile ul li.active').appendTo(".message-nav .bootstrap_btn_mobile ");
    $('.bootstrap_btn_mobile .close-sign').appendTo('.bootstrap_btn_mobile li.active');
    $('.message-nav .bootstrap_btn_mobile .navbar-toggle').on('click', function () {
        if (!($(this).find('.crossbtn').hasClass('show'))) {
            $('.bootstrap_btn_mobile ul li').show().removeClass('active');
            $('.message-nav .bootstrap_btn_mobile .crossbtn').addClass('show');
        } else {
            $('.bootstrap_btn_mobile ul li').hide();
            $('.message-nav .bootstrap_btn_mobile .crossbtn').removeClass('show');
        }
    });
    var date = moment().format("MMM DD, YYYY"),
            time = moment().format("hh:mm a");

//add-new-topic
    $('#new-topic').on('click', function (e) {
        e.preventDefault();
        $('.message-nav ul').find('[data-count="0"]').first().trigger('click');
        $('#message-box').show();
    });
//add-new-topic-ends  

//display-message-replies
    if (message_board_id) {
        messageBoardData(message_board_id, null, null, null);
    }
    main_ajax(null, null, publicMessageType);
    $('#all-messages-sorting').on('click', function () {
        var $this = $(this);
        if ($this.hasClass('desc')) {
            $this.removeClass('desc');
            main_ajax(null, '?sort_by=' + $this.data('sort_by') + '&sort_by_type=asc');
        } else {
            $this.addClass('desc')
            main_ajax(null, '?sort_by=' + $this.data('sort_by') + '&sort_by_type=desc');
        }
    });
    $(document).on('click', '.sort-posts p', function () {
        var $this = $(this);
        if ($this.hasClass('expand-all')) {
//    expand all functionality
            var originalText = $this.text();
            $this.toggleClass('expanded').text($this.attr('data-text')).attr('data-text', originalText);
            $this.closest('.msg-post').find('.champion-post:not([id="reply-details"])').slideToggle();
//    end expand all functionality
        } else {
            $this.addClass('active').siblings().removeClass('active');
            messageBoardData($this.data('id'), $this.data('replies_sort_by_type'), $this.data('replies_sort_by'));
            expandPosts();
        }
    });
    $(document).on('click', '.message-title', function (e) {
        e.preventDefault();
        messageBoardData($(this).data("msgid"));
        var new_browser_url = messageBoardUrl + '/' + $(this).data("msgid");
        window.history.pushState('title', '', new_browser_url);
        if ($(this).parents('.message-post-details').attr('data-type')=="myposts") {
            $('.message-nav li:last-child').attr('data-type', 'myposts');
        }else{
               $('.message-nav li:last-child').attr('data-type', '');
        }
    });

//End  message-details
//save-new-message-form
    $('#new-message-form').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this),
                form_action = $this.attr('action');
        $this.find('.submit-btn').attr('disabled', 'disabled').addClass('active');
        $.ajax({
            url: form_action,
            type: "POST",
            data: $this.serialize(),
            dataType: "json",
            success: function (response) {
                saveNewPost(response);
                $('#message-box').hide();
                $this.find('[type="text"], [type="email"], textarea').val('');
                $this.find('.submit-post-btn').removeAttr('disabled').removeClass('active');
                $this.find('#alert-box').addClass('success').text(response.message);
                setTimeout(function () {
                    $this.find('#alert-box').removeClass('success').text('');
                }, 3000);
            },
            error: function (error) {
                var formSel = $('#new-message-form');
                formSel.find('.submit-post-btn').removeAttr('disabled').removeClass('active');
                hpApp.ajaxInputError(error, formSel);
            }
        });
    });
    function saveNewPost(resp) {
        var newpost = $('#new-post-template').clone(),
                title = $('input[name="title"]').val();
        newpost.attr('id', '').show();
        newpost.find('.message-title').text(title).attr('data-msgid', resp.data.id);
        newpost.find('p.vertical-line').text(date);
        newpost.find('p:nth-child(2)').text(' ' + time + ' ' + by + ' ' + username);
        $('.message-post-details').prepend(newpost);
    }
//End save-new-message-form

//messages like/dislike/reply
    $(document).on("click", '.post-reponses p', function (e) {
        e.preventDefault();
        var $this = $(this);
        var data,
                type,
                checkReaction = $this.data("reactions");
        if ($this.hasClass('likes')) {
            type = 1;
        } else if ($this.hasClass('dislikes')) {
            type = 2;
        }
        if ($this.hasClass('mainmessage')) {
            data = {
                type: type,
                message_board_id: $(this).data("id")
            };
        } else {
            data = {
                type: type,
                message_board_reply_id: $(this).data("id")
            };
        }

        if (checkReaction == 'noreaction') {
            $.ajax({
                url: reactionsUrl,
                type: "GET",
                data: data,
                dataType: "json",
                success: function (response) {
                    if (isNaN(parseInt($this.text()))) {
                        $this.text(0);
                    }
                    var newlikes = parseInt($this.text()) + 1;
                    $this.html(newlikes + '<span></span>');
                    $this.data('reactions', 'already_reacted');
                    $this.siblings().data('reactions', 'already_reacted');
                },
                error: function (error) {
                    hpApp.ajaxSwalError(error);
                }
            });
        } else if ($this.hasClass('replies')) {
            //reply to message
            if ($this.hasClass('reply-message-open')) {
                $this.parent().find('.reply-form').remove();
            } else {
                var replyClone = $('#reply-form-template').clone();
                replyClone.show().attr('id', '');
                $this.parent().append(replyClone);
                $('#reply-message-form .board-id').val($this.data('messageboardid'));
                if ($this.data('replyid')) {
                    $('#reply-message-form .reply-id').prop("disabled", false).val($this.data('replyid'));
                } else {
                    $('#reply-message-form .reply-id').prop("disabled", true);
                }
            }
            $this.toggleClass('reply-message-open');
        } else if (checkReaction == 'already_reacted') {
            swal({
                title: 'Error',
                text: already_reacted,
                type: 'error',
                customClass: 'sweat-alert-confirm'
            });
        }
    });
//messages like/dislike/reply-ends
    $(document).on('click', '.submit-btn', function (e) {
        e.preventDefault();
//        $(this).parent().find('.replies').removeClass('disabled');
        var $this = $(this).closest('form');
        var formAction = $this.attr('action');
        $.ajax({
            url: formAction,
            type: "POST",
            data: $this.serialize(),
            dataType: "json",
            success: function (response) {
                setTimeout(function () {
                    $this.find('[type="text"], [type="email"], textarea').val('');
                    $this.find('.submit-btn').removeAttr('disabled').removeClass('active');
                    $this.find('#alert-box').removeClass('success').text('');
                }, 1000);
                if ($this.hasClass('private-message')) { //  private-message-form
                    appendPrivateMessage(response.data.id, $this.find('input[name="title"]').val(), $this.find('textarea[name="description"]').val());
                } else if ($this.hasClass('private-reply')) { //  private-reply-form 
                    appendPrivateReply(response.data.id, $this.find('input[name="message_board_id"]').val(), $this.find('textarea[name="reply"]').val())
                } else {
                    appendReply(response.data.id); // public reply-form
                }
            },
            error: function (error) {
                var formSel = $this;
                formSel.find('.submit-btn').removeAttr('disabled').removeClass('active');
                hpApp.ajaxInputError(error, formSel);
            } 
        }); 
        function appendReply(id) {
            $this.parent().hide();
            var replypost = $('.public-new-reply').last().clone();
            reply = $this.find('textarea[name="reply"]').val();
            replypost.find('.champion-name').text(username);
            replypost.find('.champion-pic').attr('src',team_logo);
            replypost.find('.post-date p').text(moment().format("MMM DD, YYYY"));
            replypost.find('.post-date span').text(moment().format("hh:mm a"));
            replypost.find('.description p').text($this.find('textarea').val());
            replypost.find('.likes').attr('data-id', id);
            replypost.find('.dislikes').attr('data-id', id);
            replypost.find('.replies').attr('data-replyid', id);
            replypost.find('.replies').attr('data-messageboardid', $this.find('.board-id').val());
            $this.closest('.champion-details').find('.post-reponses').first().after(replypost.show());
        }
    });
//reply to message-ends
    $(document).on('click', '.show-text', function () {
        var $this = $(this);
        var originalText = $this.text();
        $this.text($this.data('text')).attr('data-text', originalText);
        $this.parent().find('.description').first().toggleClass('readmore');
    });
});
function showMore() {
    $('.description').each(function () {
        var $this = $(this);
        var words = $this.find('p').text().split(' ');
        if (words.length > 30) {
            $this.find('span').text($this.find('p').text().split(/\s+/).slice(0, 30).join(" ") + " ...");
            $this.addClass('readmore');
            $this.parent().find('.show-text').first().show();
        }
    });
}
//message-navbar
$('.message-nav li, .message-nav .bootstrap_btn_mobile li').on('click', function (e) {
    e.preventDefault();
    var $this = $(this);
    if (!($this.is(':last-child')) && !($this.is(':first-child'))) {
        var listCount = $this.data('count');
        var messagetype = $this.data('messagetype');
        $('.msg-post[data-messageDiv="' + listCount + '"]').show().siblings('.msg-post').hide();
        $('.message-nav ul li:last-child, .message-nav ul li:first-child').hide();
        $('.post-pagination').removeClass('message-pagination').show();
        $this.addClass('active').siblings().removeClass('active');
        if (!$this.hasClass('opened')) {
            window.history.pushState('title', '', messageBoardUrl);
            main_ajax(null, null, messagetype);
        }
        if (($this.attr('data-type') == 'myposts') && ($this.attr('data-messagetype') == publicMessageType)) {
            $('#my-message-post').show();
            $('#message-post').hide();
        } else if ($this.attr('data-messagetype') == publicMessageType) {
            $('#my-message-post').hide();
            $('#message-post').show();
        } else if ($this.attr('data-messagetype') == privateMessageType) {
            $('#message-box, .private-message-form, .single-message-thread').hide();
            $('.new-message, .private-message-header, .private-message-box').show();
        }
    }
});
//$('.message-nav .bootstrap_btn_mobile li').on('click', function (e) {
//    e.preventDefault();
//    var listCount = $(this).data('count');
//    $('#message-box,.private-message-form, .single-message-thread').hide();
//    $('.msg-post[data-messageDiv="' + listCount + '"]').show().siblings('.msg-post').hide();
//    $('.message-nav ul li:last-child, .message-nav ul li:first-child').hide();
//    $('.post-pagination').removeClass('message-pagination').show();
//    $this.addClass('active').siblings().removeClass('active');
//    window.history.pushState('title', '', messageBoardUrl); 
//    main_ajax();
//});
$('.close-sign').on('click', function () {
    if ($(this).parents('li').attr('data-messagetype') == privateMessageType) {
        $(this).closest('ul').find('li[data-messagetype="' + privateMessageType + '"]').trigger('click');
    } else if ($(this).parents('li').attr('data-type') == "myposts") {
        $(this).closest('ul').find('li[data-type="myposts"]').trigger('click');
    } else {
        $(this).closest('ul').find('li[data-count="0"]').first().trigger('click');
    }
});
//message-navbar-ends
function messageBoardData(id, sort_type, sort_by, page, messagetype) {
    var sorting = false;
    var message_url = messageBoardDataUrl.replace("0", id);
    var new_message_url = message_url + '?pool_id=' + pool_id;
    if (sort_type && sort_by) {
        new_message_url = new_message_url + '&replies_sort_by_type=' + sort_type + '&replies_sort_by=' + sort_by;
        sorting = true;
    }
    if (page) {
        new_message_url = new_message_url + '&page=' + page;
    }
    $.ajax({
        url: new_message_url,
        type: "GET",
        data: {
            message_type: messagetype
        },
        dataType: "json",
        success: function (response) {
            $('#message-box').hide();
            $('.message-posts').hide();
            $('#detailed-message').show();
            $('.message-nav ul li').removeClass('active');
            $('.post-pagination').addClass('message-pagination');
//            $('#page-list').find('a[data-page=1]').trigger('click');
            var msgData = {data: response},
                    displayData = {data: response.direct_replies};
            if (sorting) {
                $('#detailed-message .champion-post').not(':first').remove();
            } else {
                $('#detailed-message, #all-replies').empty();
            }
            $('.message-nav ul li:last-child').show().attr('data-messagetype', response.type).addClass('active').find('a').text(response.title.length < 25 ? response.title : response.title.substring(0, 25) + '...');
            if (response.type == privateMessageType) {
                var privatemessageDetails = _.template($('#private-message-thread').html());
                $('#private-message').show();
                $('#private-message .private-message-header').hide();
                $('#all-replies').attr('data-messageboardid', response.id).append(privatemessageDetails(msgData));
                var messagePrivate = $('.message-subject[data-msgid="' + id + '"]').parents('.single-message');
                if (messagePrivate.hasClass('grey')) {
                    var notification = $('.message-nav ul li[data-messagetype="' + privateMessageType + '"]').find('.notification').first();
                    if (notification.text() > 1) {
                        notification.text((notification.text()) - 1).show();
                    } else {
                        notification.hide();
                    }
                    messagePrivate.removeClass('grey');
                }
            } else if (response.type == publicMessageType) {
                if (!sorting) {
                    var message_detail = $('#reply-template').html(),
                            message_detail = _.template(message_detail);
                    $('#detailed-message').append(message_detail(msgData));
                }
                printReplies(response.direct_replies);
            }
            var total_replies;
            if (response.total_direct_replies) {
                total_replies = parseInt(response.total_direct_replies.total);
            } else {
                total_replies = 0;
            }
            var msg_limit = parseInt(message_limit),
                    total_pages = parseInt(total_replies / msg_limit);
            if (total_replies % msg_limit != 0) {
                total_pages++;
            }
            var current_page = $('#page-list').find('.active').data('page');
            if (total_replies > msg_limit) {
                if (page) {
                    NAMESPACE.PAGINATE(current_page, total_pages);
                } else {
                    NAMESPACE.PAGINATE(1, total_pages);
                }
            } else {
                $('.post-pagination').hide();
            }
            showMore();
        },
        error: function (error) {
            hpApp.ajaxSwalError(error);
        }
    });
}
var template = $('#msg-replies').html();
template = _.template(template);
function printReplies(arr, parent_id) {
    $.each(arr, function (i, vrr) {
        if (typeof parent_id != 'undefined') {
            $('.cp' + parent_id).append(template({data: vrr}));
            printReplies(vrr.children, vrr.id);
        } else {
            $('#detailed-message').append(template({data: vrr}));
            printReplies(vrr.children, vrr.id);
        }
    });
}
//main-message board
function main_ajax(page, sorting, messagetype) {
    if (page) {
        new_url = url + page;
    } else {
        new_url = url;
    }
    if (sorting) {
        new_url = new_url + sorting;
    }
    if ($('.message-nav .active').data('type') == 'myposts') {
        if (new_url.indexOf('?') > -1) {
            new_url = new_url + '&self=1';
        } else {
            new_url = new_url + '?self=1';
        }
    }
    $.ajax({
        url: new_url,
        type: "GET",
        data: {
            message_type: messagetype
        },
        dataType: "json",
        success: function (response) {
            var message_board = _.template($('#message-board').html()),
                    mypost_message_board = _.template($('#my-post-message-board').html()),
                    private_message_board = _.template($('#all-private-messages').html())
            messages = {msgsdata: response};
            $('.message-nav ul li.active').addClass('opened');
            if (response.data_unread.length >= 1) {
                $('.message-nav ul li[data-messagetype="' + privateMessageType + '"]').find('.notification').show().text(response.data_unread.length);
            } else {
                $('.message-nav ul li[data-messagetype="' + privateMessageType + '"]').find('.notification').hide();
            }
            if (messagetype == privateMessageType) {
                $('#private-message-box').empty();
                $('#private-message-box').append(private_message_board(messages));
                $('#private-message-box').find('.single-message .message-subject').data('data-msgid');
            } else if ($('.message-nav ul li[data-type="myposts"]').hasClass('active')) {
                $('#my-message-post').empty();
                $('#my-message-post').append(mypost_message_board(messages));
            } else {
                $('#message-post').empty();
                $('#message-post').append(message_board(messages));
            }
            NAMESPACE.PAGINATE(response.current_page, response.last_page);
        },
        error: function (error) {
            hpApp.ajaxSwalError(error);
        }
    });
}
function expandPosts() {
    if (!($('.expand-all').hasClass('expanded'))) {
        $('.expand-all').trigger('click');
    }
}
//main-message board-ends
//pagination
var NAMESPACE = {
    PAGINATE: function paginate(current_page, last_page) {
        $paginateDiv = $('#page-list');
        if (last_page > 0) {
            $paginateDiv.empty();
            $('.post-pagination').show();
            var single_anchor_width = 30;
            for (var i = 1; i <= last_page; i++) {
                if (i == current_page) {
                    $paginateDiv.append('<a class="active" data-page=' + i + '>' + i + '</a>');
                } else {
                    $paginateDiv.append('<a data-page=' + i + '>' + i + '</a>');
                }
            }
            NAMESPACE.PAGINATE_ANIMATE(current_page, false);
        } else {
            $('.post-pagination').hide();
        }
    },
    //    END display pagination function
    PAGINATE_ANIMATE: function pagination_animate(activePage, status) {
        var pageList = $('#page-list');
        var lastPage = parseInt(pageList.find('a:last-child').data('page'));
        pageList.find('[data-page=' + activePage + ']').addClass('active').siblings().removeClass('active');
        if (activePage >= 3) {
            pageList.animate({marginLeft: -(activePage - 3) * 30}, 300);
        }
        if (activePage == 2) {
            pageList.animate({marginLeft: 0}, 300);
        }
        if (activePage != 1) {
            $('#pagination-left').removeClass('opacity0');
            $('#pagination-first').removeClass('opacity0');
        }
        if (lastPage <= 5) {
            $('.main-pagination').outerWidth(30 * lastPage);
            $('#pagination-right').addClass('opacity0');
            $('#pagination-last').addClass('opacity0');
        } else {
            $('#pagination-right').removeClass('opacity0');
            $('#pagination-last').removeClass('opacity0');
            $('.main-pagination').outerWidth(160);
        }
        if (activePage == lastPage) {
            $('#pagination-right').addClass('opacity0');
            $('#pagination-last').addClass('opacity0');
        }
        if (activePage == 1) {
            pageList.animate({marginLeft: 0}, 300);
            $('#pagination-left').addClass('opacity0');
            $('#pagination-first').addClass('opacity0');
        }
        if (status) {
            if ($('.post-pagination').hasClass('message-pagination')) {
                if ($('#all-replies').is(':visible')) {
                    NAMESPACE.PRIVATE_MESSAGE_PAGINATION(activePage);
                } else {
                    NAMESPACE.MESSAGE_PAGINATION(activePage);
                }
            } else {
                NAMESPACE.CHANGE_URL("?page=" + activePage);
            }
        }
        expandPosts();
    },
    CHANGE_URL:
            function changeUrl(page) {
                if ($('.message-nav .active').attr('data-messagetype') == privateMessageType) {
                    // private message pagination
                    main_ajax(page, null, privateMessageType);
                } else {
                    var $this = $('#all-messages-sorting');
                    if ($this.hasClass('desc')) {
                        main_ajax(page + '&sort_by=' + $this.data('sort_by') + '&sort_by_type=desc');
                    } else {
                        main_ajax(page + '&sort_by=' + $this.data('sort_by') + '&sort_by_type=asc');
                    }
                }
            },
    MESSAGE_PAGINATION: function message_page(page) {
        var $this = $('.sort-posts').find('.active');
        messageBoardData($this.data('id'), $this.data('replies_sort_by_type'), $this.data('replies_sort_by'), page);
    },
    PRIVATE_MESSAGE_PAGINATION: function private_message_page(page) {
        if ($('#all-replies').is(':visible')) {
            messageBoardData($('#all-replies').attr('data-messageboardid'), null, null, page, privateMessageType);
        }
//        var $this = $('.sort-posts').find('.active');
    }
}
$('#pagination-right').on('click', function () {
    var activePage = parseInt($('#page-list').find('.active').data('page'));
    NAMESPACE.PAGINATE_ANIMATE(activePage + 1, true);
});
//    left pagination arrow on click
$('#pagination-left').on('click', function () {
    var activePage = parseInt($('#page-list').find('.active').data('page'));
    NAMESPACE.PAGINATE_ANIMATE(activePage - 1, true);
});
$('#pagination-first').on('click', function () {
    NAMESPACE.PAGINATE_ANIMATE(1, true);
});
$('#pagination-last').on('click', function () {
    NAMESPACE.PAGINATE_ANIMATE(parseInt($('#page-list').find('a:last-child').data('page')), true);
});
//    pagination NUMBERS arrow on click
$('.post-pagination').on('click', 'a', function () {
    var activePage = parseInt($(this).data('page'));
    NAMESPACE.PAGINATE_ANIMATE(activePage, true);
});

//Private Messages
//New private message
//open-new private message-form
$('.new-message').on('click', function (e) {
    e.preventDefault();
    $('.private-message-form').show();
    $(this).hide();
    $('.message-nav ul li:first-child').show().addClass('active').siblings().removeClass('active');
});
//open single private message thread
$('body').on('click', '.message-subject', function (e) {
    e.preventDefault();
    var $this = $(this);
    var messagetype = $(this).data('messagetype');
    $('.private-message-header, .private-message-box, .new-private-message-reply, .message-nav ul li:first-child').hide();
    $('.single-message-thread').show();
    messageBoardData($this.data("msgid"), null, null, null, messagetype);
    var new_browser_url = messageBoardUrl + '/' + $this.data("msgid");
    window.history.pushState('title', '', new_browser_url);
});
//reply to private message-replies
var messageSbj, username;
$('body').on('click', '.message-actions .reply-icon', function (e) {
    e.preventDefault();
    var $this = $(this);
    messageSbj = $this.parents('.message-actions').siblings('.reply-subject');
    username = $this.parents('.message-actions').siblings('.sender-name').text();
    privateReply(messageSbj, username);
});
//reply to private single-message
$('body').on('click', '.single-message .message-actions .reply-icon', function (e) {
    var $this = $(this);
    e.preventDefault();
    messageSbj = $this.parents('.message').siblings('.message-subject'),
            username = $this.parents('.message-details').data('user');
    $('.private-message-header, .private-message-box, .message-nav ul li:first-child').hide();
    messageBoardData(messageSbj.data("msgid"), null, null, null, messageSbj.data('messagetype'));
    var new_browser_url = messageBoardUrl + '/' + messageSbj.data("msgid");
    window.history.pushState('title', '', new_browser_url);
    $('.single-message-thread').show();
    privateReply(messageSbj, username);
});
//remove-private-message
$('body').on('click', '.message-actions .trash-icon', function () {
    var $this = $(this), data, messagePost,
            msgId = $this.parents('.message-actions').data('msgboardid'),
            replyId = $this.parents('.message-actions').data('replyid');
    if (replyId) {
        data = {
            message_board_id: msgId,
            message_reply_id: replyId,
            message_type: privateMessageType
        };
        messagePost = $this.parents('.panel-default.message-reply');
    } else {
        data = {
            message_board_id: msgId,
            message_type: privateMessageType
        };
        messagePost = $this.parents('.single-message');
    }
    deletePrivateMessage(data, messagePost);
});
function privateReply(messageSbj, username) {
    $('.new-private-message-reply').show();
    $('.new-private-message-reply').find('input.board-id').val(messageSbj.data("msgid"));
    $('.new-private-message-reply').find('select.input-field option:selected').text(username);
    $('.new-private-message-reply').find('input[name="title"]').val(messageSbj.text());
}
function deletePrivateMessage(deletedata, messagePost) {
    $.ajax({
        url: deleteMsgUrl,
        type: "POST",
        data: deletedata,
        dataType: "json",
        success: function (response) {
            messagePost.hide();
        },
        error: function (error) {
            hpApp.ajaxSwalError(error);
        }
    });
}
function appendPrivateMessage(msgid, subject, description) {
    var newMessage = $('.new-private-message').first().clone();
    newMessage.attr('data-boardid', msgid);
    newMessage.find('.message-subject').text(subject).attr('data-msgid', msgid);
    newMessage.find('.message p').text(description);
    newMessage.find('.message-actions').attr('data-msgboardid', msgid);
    newMessage.find('.message-sender .date').text('Date:' + moment().format('YYYY-MM-DD') + ' ' + moment().format('hh:mm:ss'));
    if ($('#private-message').find('.single-message').length > 0) {
        $('#private-message').find('.single-message').first().before(newMessage.show());
    } else {
        $('#private-message').append(newMessage.show());
    }
    $('#private-message-form input[type="text"], #private-message-form textarea, #private-message-form select').val('');
    $('.private-message-form').hide();
    $('.message-nav ul li[data-messagetype=' + privateMessageType + ']').trigger('click');
}
function appendPrivateReply(replyid, msgid, description) {
    var newPrivateReply = $('.message-reply.new-reply').first().clone();
    newPrivateReply.find('.message-actions').attr('data-replyid', replyid);
    newPrivateReply.find('.message-actions').attr('data-msgboardid', msgid);
    newPrivateReply.find('.reply-subject').attr('data-msgid', msgid);
    newPrivateReply.attr('data-boardid', msgid);
    newPrivateReply.find('.panel-title .reply-subject .text').text(description < 60 ? description : description.substring(0, 60) + '...');
    newPrivateReply.find('.panel-body .reply-text').text(description);
    newPrivateReply.find('.panel-title .reply-time').text(moment().format("MMM Do") + ' ' + moment().startOf('day').fromNow());
    newPrivateReply.find('.panel-heading').attr('id', 'replyheading' + msgid);
    newPrivateReply.find('.panel-collapse').attr('aria-labelledby', 'replyheading' + replyid);
    newPrivateReply.find('.panel-heading a').attr('aria-controls', 'replycollapse' + replyid);
    newPrivateReply.find('.panel-heading a').attr('href', '#replycollapse' + replyid);
    newPrivateReply.find('.panel-collapse').attr('id', 'replycollapse' + replyid);
    $('#all-replies').find('.new-private-reply-append').append(newPrivateReply.show());
    $('#private-message-reply-form input[type="text"], #private-message-reply-form textarea').val('');
    $('.new-private-message-reply').hide();
}
