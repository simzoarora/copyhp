$(document).ready(function () {
    //taking page to top if not
    $('body').animate({scrollTop: 0});
    //getting data for InActionTonight
    $.ajax({
        url: inActionTonightUrl,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            if (!$.isEmptyObject(response)) {

                //checking which pool type/cat is this
                if (response.pool_type == poolTypes.h2h) {
                    if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                        var poolToShow = 11;
                    } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly) {
                        var poolToShow = 12;
                    } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly_daily) {
                        var poolToShow = 13;
                    }
                } else if (response.pool_type == poolTypes.box) {
                    var poolToShow = 2;
                } else if (response.pool_type == poolTypes.standard) {
                    if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_standard) {
                        var poolToShow = 31;
                    } else if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                        var poolToShow = 32;
                    }
                }
                //END checking which pool type/cat is this

                //Append In action tonight data
                $('#all-teams-outer').empty();
                if (!$.isEmptyObject(response.pool_teams)) {
                    var allTeamsOuter = $('#all-teams-outer'),
                            allTeamsTemplate = $('#all-teams-template').html();
                    allTeamsTemplate = _.template(allTeamsTemplate);
                    var allTeamsData = {
                        poolTeams: response.pool_teams,
                        siteLink: api_url
                    };
                    allTeamsOuter.append(allTeamsTemplate(allTeamsData));
                } else {
                    $('#all-teams-outer').append('<p class="no-result-present">'+in_action_tonight.messages.no_teams_present+'</p>');
                }
                //END Append In action tonight data
                //Append each team data 
                $('#each-teams-outer').empty();
                if (!$.isEmptyObject(response.pool_teams)) {
                    $.each(response.pool_teams, function (i, v) {
                        if (!$.isEmptyObject(v.pool_team_players)) {
                            var eachTeamsOuter = $('#each-teams-outer'),
                                    eachTeamsTemplate = $('#each-teams-template').html();
                            eachTeamsTemplate = _.template(eachTeamsTemplate);
                            var eachTeamsData = {
                                poolTeam: v,
                                poolTeamsPlayers: v.pool_team_players,
                                siteLink: api_url,
                                poolToShow: poolToShow
                            };
                            eachTeamsOuter.append(eachTeamsTemplate(eachTeamsData));
                        }
                    });
                } else {
                    $('#each-teams-outer').append('<p class="no-result-present">'+in_action_tonight.messages.no_teams_present+'</p>');
                }
                //END Append each team data
            } else {
                //empty div
                $('.pool--content').empty().append('<p class="no-result-present">'+in_action_tonight.messages.invalid_pool+'</p>');
            }

            //Remove Loader
            $('.scorer-loader-logo').hide();
            $('.site-container').css({'background-color': '#fff'});
            $('#pool--content').css({'opacity': 1});
        },
        error: function (error) {
            hpApp.ajaxSwalError(error);
        }
    });
    //END getting data for InActionTonight
});