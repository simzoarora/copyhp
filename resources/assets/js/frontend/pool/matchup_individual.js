$(document).ready(function () {
    function fillEmptyTable(sel, message) {
        if (hpApp.isElementEmpty(sel.find('tbody'))) {
            var colspan = sel.find('thead').find('th').length;
            sel.find('tbody').append('<tr><td colspan="' + colspan + '" class="no-result-present">' + message + '</td><tr>');
        }
    }
    //today/week selection
    $('.matchup--day ul li').on('click', function (e) {
        var $this = $(this),
                newUrl = url + "?type=" + $this.data('type');
        $this.addClass('active').siblings().removeClass('active');
        getMatchup(newUrl);
        e.preventDefault();
    });
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    //today/week selection-ends
    function getMatchup(url) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                $('#pool-h2h-all').empty();
                $('#team-table-all').empty();
                $('#matchup-all').empty();
                var team1_score = 0;
                var team2_score = 0;
                if (response.pool_h2h_scores) {
                    var team1_data = response.pool_h2h_scores[response.team1];
                    var team2_data = response.pool_h2h_scores[response.team2];
                } else {
                    var team1_data = 0;
                    var team2_data = 0;
                }
                var pool_h2h_data = {
                    team1: response.pool_team1,
                    team2: response.pool_team2,
                    team1_data: team1_data,
                    team2_data: team2_data,
                    cat_result: response.cat_result,
                    matchup_results: response.pool_matchup_results
                };
                var pool_h2h_template = $('#pool-h2h-template').html();
                var team_table_template = $('#team-table-template').html();
                var matchup_table_template = $('#matchup-table-template').html();
                if ($(window).width() < 768) {
                    pool_h2h_template = $('#pool-h2h-template-mobile').html();
                    team_table_template = $('#team-table-template-mobile').html();
                    matchup_table_template = $('#matchup-table-template-mobile').html();
                }
                pool_h2h_template = _.template(pool_h2h_template);
                $('#pool-h2h-all').append(pool_h2h_template(pool_h2h_data));
                //upper table
                team_table_template = _.template(team_table_template);
                $('#team-table-all').append(team_table_template(pool_h2h_data));
                // all tables
                matchup_table_template = _.template(matchup_table_template);
                $('#matchup-all').append(matchup_table_template(pool_h2h_data));
                fillEmptyTable($('.matchup-players'), matchup_individual.messages.no_player_present);
                fillEmptyTable($('.matchup-goalies'), matchup_individual.messages.no_goalie_present);
                fillEmptyTable($('.matchup-players2'), matchup_individual.messages.no_player_present);
                fillEmptyTable($('.matchup-goalies2'), matchup_individual.messages.no_goalie_present);

                //Adding width to team table heading
                $('.teamPlayerHeading th:nth-child(2)').attr('colspan', $('.teamPlayerData').length);
                $('.teamPlayerHeading th:nth-child(3)').attr('colspan', $('.teamGoalieData').length);
 
                //Remove loader
                $('.scorer-loader-logo').hide();
                $('.site-container').css({'background-color': '#fff'});
                $('#pool--content').css({'opacity': 1}).fadeIn('500');
                if (!(pool_type == pool_format.h2h_cat)) {
                    $('.total_points').show();
                    $('.light-grey-table,.table-matchup').css({padding: '0px', border: '0'});
                    if ($(window).width() > 768) {
                        $('.matchup-players2 thead tr').prepend($('.matchup-players2 thead tr .total_points'));
                        $('.matchup-players2 tbody tr').each(function () {
                            $(this).prepend($(this).find('.total_points'));
                        });
                        $('.matchup-goalies2 thead tr').prepend($('.matchup-goalies2 thead tr .total_points'));
                        $('.matchup-goalies2 tbody tr').each(function () {
                            $(this).prepend($(this).find('.total_points'));
                        });
                    }
                }
                if ($(window).width() < 768) {
                    checkSliderWidth();
                }
            },
            error: function (err) {
                $('#matchup-all').empty().append('<p class="no-result-present"><td colspan="15">' + matchup_individual.messages.no_results_found + '<p>');
                $('#team-table').empty().append('<tr class="no-result-present"><td colspan="15">' + matchup_individual.messages.no_results_found + '</td></tr>>');
                $('#pool-h2h-all').empty().append('<p class="no-result-present"><td colspan="15">' + matchup_individual.messages.no_results_found + '<p>');
            }
        });
    }
    getMatchup(url + '?type=1');
    function checkSliderWidth() {
        var teamTable = $('#team-table-all');
        if (teamTable.find('.table').outerWidth() <= teamTable.find('.team-score').outerWidth()) {
            teamTable.find('.arrow-right').hide();
            teamTable.find('.team-score').css('width', '85%');
        }
    }
    var i = 0;
    $('#team-table-all').on('click', '.arrow-right', function () {
        var $this = $(this);
        i++;
        var tableWidth = $this.closest('.team-table-outer-mobile').find('.table').outerWidth(),
                teamScoreWidth = $this.closest('.team-table-outer-mobile').find('.team-score').outerWidth(),
                marginLeft = parseInt($this.closest('.team-table-outer-mobile').find('.table').css('margin-left').replace('px', '')),
                margin = $this.closest('.team-table-outer-mobile').find('th:nth-child(' + i + ')').outerWidth();
        var diff = teamScoreWidth - tableWidth;
        if (diff > marginLeft - margin) {
            i = 0;
            $this.closest('.team-table-outer-mobile').find('.table').animate({
                marginLeft: diff + 'px'
            });
        } else {
            $this.closest('.team-table-outer-mobile').find('.table').animate({
                marginLeft: '-=' + margin + 'px'
            });
        }
        if (marginLeft == diff) {
            i = 0;
            $this.closest('.team-table-outer-mobile').find('.table').animate({
                marginLeft: '0px'
            });
        }
    });
    $(window).resize(function () {
        getMatchup(url + '?type=1');
    });
});