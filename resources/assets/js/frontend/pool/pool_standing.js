$(document).ready(function () {
    var firstLoad = true;
    function hideLoader() {
        if (firstLoad) {
            $('body').animate({scrollTop: 0});
            $('.scorer-loader-logo').hide();
            $('.site-container').css({'background-color': '#fff'});
            $('#pool--content').css({'opacity': 1}).fadeIn(500);
            firstLoad = false;
        }
    }
    $('.select-field').selectBoxIt();
    //slider
    var slideWidth = $('.pool-standing-slider .standing-teams').outerWidth();
    var slideHeight = $('.pool-standing-slider .standing-teams').outerHeight();
    var totlSlides = $('.pool-standing-slider .standing-teams').length;
    var totlWidth = slideWidth * totlSlides;
    var sliderWidth = $('.slider').width(totlWidth);
    var noAnimate = totlWidth - ($('.pool-standing-slider').outerWidth()) + 46;
    var moveSlide = 0;
    $('.slider--left-arrow .graphics').on('click', function () {
        //check if animated
        if ($('.pool-standing-slider .slider').is(':animated')) {
            //do nothing
            $(this).stop();
        }
        //check if it is first-slide
        else if ($(".slider").css("marginLeft") === '50px') {
        } else {
            $('.pool-standing-slider .slider').animate({
                marginLeft: moveSlide + slideWidth + 50
            });
            moveSlide = moveSlide + slideWidth;
        }
    });
    $('.slider--right-arrow .graphics').on('click', function () {
        //check if animated
        if ($('.pool-standing-slider .slider').is(':animated')) {
            $(this).stop();
        }
        //check if it is last-slide
        else if ($(".slider").css("marginLeft") === (-noAnimate + 'px')) {
            //do nothing
        } else {
            $('.pool-standing-slider .slider').animate({
                marginLeft: moveSlide - slideWidth + 50
            });
            moveSlide = moveSlide - slideWidth;
        }
    });
    //slider End
    function getPoolStandings(url) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                //checking which pool type/cat is this
                if (response.pool_type == poolTypes.h2h) {
                    if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                        var poolToShow = 11;
                    } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly) {
                        var poolToShow = 12;
                    } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly_daily) {
                        var poolToShow = 13;
                    }
                } else if (response.pool_type == poolTypes.box) {
                    var poolToShow = 2;
                } else if (response.pool_type == poolTypes.standard) {
                    if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_standard) {
                        var poolToShow = 31;
                    } else if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                        var poolToShow = 32;
                    }
                }
                //END checking which pool type/cat is this

                var appendlocation = $('#pool_standings_all');
                appendlocation.empty();
                if (!$.isEmptyObject(response)) {
                    var poolStandingsTemplate = $('#pool_standings_template').html();
                    poolStandingsTemplate = _.template(poolStandingsTemplate);
                    var pool_standings_data = {
                        teams: response.pool_teams,
                        type: response.pool_type,
                        poolToShow: poolToShow
                    };
                    appendlocation.append(poolStandingsTemplate(pool_standings_data));
                } else {
                    appendlocation.append('<p class="no-result-present">No results found.</p>')
                }
                hideLoader();
            },
            error: function (err) {
                $('#pool_standings_all').append('<p class="no-result-present">No results found.</p>');
                hideLoader();
            }
        });
    }
    getPoolStandings(url);
    //HEIGHTS CHECK
    if ($(window).width() > 768) {
        var leftSide = $('.pool-standing').height(),
                rightSide = $('.adblocks').height();
        if (rightSide > leftSide) {
            $('.pool-standing').css('min-height', rightSide);
        }
    }
});
