$(document).ready(function () {
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    function getBoxDraft(url) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (!$.isEmptyObject(response)) {
                    var appendlocation = $('#box-score-all');
                    appendlocation.empty();
                    var boxScoreTemplate = $('#box-score-template').html();
                    boxScoreTemplate = _.template(boxScoreTemplate);
                    var box_score_data = {
                        boxes: response.pool_boxes
                    };
                    appendlocation.append(boxScoreTemplate(box_score_data));
                    //Remove loader
                    $('.scorer-loader-logo').hide();
                    $('.site-container').css({'background-color': '#fff'});
                    $('#pool--content').css({'opacity': 1});
                } else {
                    appendlocation.append('<p class="no-result-present">' + error.messages.no_box_result + '</p>');
                }
            },
            error: function (error) {
                $('#box-score-all').append('<p class="no-result-present">' + error.messages.no_results_found + '</p>');
            }
        });
    }
    getBoxDraft(get_box_draft_data);
    $(document).on('change', '.custom-checkbox', function () {
        var $this = $(this);
        if ($this.is(":checked")) {
            $this.addClass('checked').closest('tr').siblings().find('.custom-checkbox').attr('checked', false).removeClass('checked');
            $this.closest('.box-score-single').addClass('checked');
        } else {
            $this.removeClass('checked');
        }
        if (!$this.closest('.box-score-single').find('.custom-checkbox').children().hasClass('checked')) {
            $this.closest('.box-score-single').removeClass('checked');
        }
    });
    $('#submit-btn').on('click', function (e) {
        e.preventDefault();
        var player_ids = [],
                all_checked = true,
                unchecked_box = '';
        $('.box-score-single').each(function (i, box) {
            if (!$(this).hasClass('checked')) {
                all_checked = false;
                if (!(unchecked_box)) {
                    unchecked_box = $(this).data('box_name');
                } else {
                    unchecked_box = unchecked_box + ', ' + $(this).data('box_name');
                }
            }
        });
        $('.custom-checkbox').each(function (i, val) {
            $this = $(this);
            if ($this.is(":checked")) {
                player_ids.push($this.data('player_id'));
            }
        });
        if (all_checked) {
            $.ajax({
                url: save_box_draft_data,
                type: 'POST',
                data: {
                    box_player_ids: player_ids
                },
                dataType: 'json',
                success: function (response) {
                    location.href = pool_dashboard_link;
                },
                error: function (error) {
                    hpApp.ajaxSwalError(error);
                }
            });
        } else {
            swal({
                title: 'Oops!!',
                text: error.messages.checkbox_error + unchecked_box,
                type: 'error',
                customClass: 'sweat-alert-confirm'
            });
        }
    });
    $('#clear-selection').on('click', function () {
        $('.custom-checkbox').attr('checked', false).removeClass('checked');
        $('.box-score-single').removeClass('checked');
    });
});