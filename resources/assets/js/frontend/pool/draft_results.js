$(document).ready(function () {
    function fillEmptyTable(sel, message) {
        if (hpApp.isElementEmpty(sel.find('tbody'))) {
            var colspan = sel.find('thead').find('th').length;
            sel.find('tbody').append('<tr><td colspan="' + colspan + '" class="no-result-present">' + message + '</td><tr>');
        }
    }
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    function getDraftResults(url) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (!$.isEmptyObject(response)) {
                    if (response.pool_type == pool_types.h2h || response.pool_type == pool_types.standard) {
                        var poolToShow = 00;
                        if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                            poolToShow = 11;
                        } else if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                            poolToShow = 32;
                        }

                        var appendlocation = $('#draft-results-all');
                        appendlocation.empty();
                        var draftResultsTemplate = $('#draft-results-template').html();
                        draftResultsTemplate = _.template(draftResultsTemplate);
                        var draft_results_data = {
                            rounds: response.pool_rounds,
                            poolToShow: poolToShow
                        };
                        appendlocation.append(draftResultsTemplate(draft_results_data));
                    } else if (response.pool_type == pool_types.box) {
                        var appendlocation = $('#box-score-all');
                        appendlocation.empty();
                        var boxScoreTemplate = $('#box-score-template').html();
                        boxScoreTemplate = _.template(boxScoreTemplate);
                        var box_score_data = {
                            boxes: response.pool_boxes,
                            poolTeamPlayers: response.pool_team_players
                        };
                        appendlocation.append(boxScoreTemplate(box_score_data));
                    }

                    //Remove loader
                    $('.table').each(function (i, v) {
                        fillEmptyTable($(this), draft_result.messages.no_player_present);
                    });

                    $('.scorer-loader-logo').hide();
                    $('.site-container').css({'background-color': '#fff'});
                    $('#pool--content').css({'opacity': 1});
                } else {
                    appendlocation.append('<p class="no-result-present">' + draft_result.messages.no_draft_result + '</p>');
                }
            },
            error: function (error) {
                $('#injury-report-all').append('<p class="no-result-present">' + draft_result.messages.no_draft_result + '</p>');
            }
        });
    }
    getDraftResults(url);
});