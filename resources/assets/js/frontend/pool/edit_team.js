$(document).ready(function () {
    $('#html-image-upload').html5imageupload({
        onSave: function (data) {
            var $this = $('#html-image-upload');
            var base64 = data.data;
            var imagename = data.name;
            if (imagename.indexOf('/') > -1) {
                imagename = imagename.split('/');
                imagename = imagename.pop();
            }
            if (imagename.length > 20) {
                imagename = imagename.substr(imagename.length - 20);
            }
            base64 = base64.split(',');
            $this.append('<input type="hidden" name="logo[name]" value=' + imagename + ' required/>');
            $this.append('<input type="hidden" name="logo[data]" value=' + base64[1] + ' required/>');
        }
    });
    $('#edit-team').on('submit', function (e) {
        var $this = $(this);
        //disabling submit button
        $this.find('.submit-btn').attr('disabled', 'disabled').addClass('active').find('span').text(buttons_general.submitting);
        $.ajax({
            url: updateUrl,
            type: 'POST',
            dataType: 'json',
            data: $this.serialize(),
            success: function (response) {
                //removing all error classes
                $this.find('.has-error').removeClass('has-error');
                $this.find('.error-msg').remove();

                $this.find('.submit-btn').removeAttr('disabled').removeClass('active').find('span').text(buttons_general.submit);
                $this.find('#alert-box').addClass('success').text(response.message);

                setTimeout(function () {
                    $this.find('#alert-box').removeClass('success').text('');
                }, 3000);
            },
            error: function (err) {
                var formSel = $('#edit-team');
                formSel.find('.submit-btn').removeAttr('disabled').removeClass('active').find('span').text(buttons_general.submit);
                hpApp.ajaxInputError(error, formSel);
            }
        });
        e.preventDefault();
    });
});

