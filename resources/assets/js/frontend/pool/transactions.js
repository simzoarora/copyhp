$(document).ready(function () {
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    //Getting Transaction data
    $.ajax({
        url: transactionsUrl,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            if (!$.isEmptyObject(response)) {
                var allTransactionOuter = $('#all-transaction-outer'),
                        allTransactionTemplate = $('#all-transaction-template').html();
                allTransactionOuter.empty();
                allTransactionTemplate = _.template(allTransactionTemplate);
                var allTransactionData = {
                    trans: response,
                    transactionsTypes: transactionsTypes
                };
                allTransactionOuter.append(allTransactionTemplate(allTransactionData));
            } else {
                //empty div
                $('#all-transaction-outer').empty().append('<tr class="no-result-present"><td colspan="4">' + transaction.messages.no_transactions + '</td></tr>');
            }

            $('.scorer-loader-logo').hide();
            $('.site-container').css({'background-color': '#fff'});
            $('#pool--content').css({'opacity': 1}).fadeIn('500');
        },
        error: function (error) {
            hpApp.ajaxSwalError(error);
        }
    });
    //END Getting Transaction data
//    my-trades
    var allTrades = $('#allTrades'),
            tradesTemplate = $('#trades-template').html(),
            tradesTemplate = _.template(tradesTemplate);
    $.ajax({
        url: tradesUrl,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            var tradeData = {
                trade: response
            };
            allTrades.append(tradesTemplate(tradeData));
        },
        error: function (error) {
            hpApp.ajaxSwalError(error);
        }
    });
    $('body').on('click', '.trade-status a', function () {
        var $this = $(this),
                tradeStatus = $this.attr('data-status'),
                tradeId = $this.parents('.trade-status').attr('data-tradeid'),
                newChnageStatusUrl = chnageStatusUrl.replace('tradeid', tradeId);
        if ($(this).hasClass('league_votes')) {
            var data = {
                status: tradeStatus,
                type: $(this).attr('data-type')
            };
        } else {
            var data = {
                status: tradeStatus
            };
        }
        $.ajax({
            url: newChnageStatusUrl,
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function (response) {
                var tradeType = $this.parents('.trades').attr('data-type');
                var managementstatus = $this.attr('data-managementstatus');
                var ownerId = $this.attr('data-owner');
                //offered-trades-accepted--and-commissioner-decides
                if (tradeType == 'offered' && tradeStatus == acceptTradeStatus) {
                    var tradeToAppend = $this.parents('.trades').find('.trades-content').clone();
                    tradeToAppend.find('.trade-status a').remove();
                    //commisioner-accepted-vote
                    if ((managementstatus == commissionerManagemnetStatus) && (ownerId != loggedInUser)) {
                        tradeToAppend.find('.trade-status p').after('<p>' + commissionerWait + '</p>');
                        //league-accepted-vote
                    } else if (managementstatus == leagueVoteManagemnetStatus) {
                        tradeToAppend.find('.trade-status p').after('<p>' + votingWait + '</p>');
                    }
                    //if no-trades-exist
                    if ($('.trades[data-type="pending"]:visible').length == 0) {
                        if (ownerId != loggedInUser) {
                            $this.parents('.trades').siblings('.empty-pending-trades[data-type="pending"]').before(tradeToAppend);
                            setTimeout(function () {
                                $this.parents('.trades').siblings('.empty-pending-trades[data-type="pending"]').hide();
                            }, 2000);
                        } else if ($this.parents('.trades').siblings('.trades[data-type="' + tradeType + '"]:visible').length == 0) {
                            $this.parents('.trades').siblings('.empty-pending-trades[data-type="' + tradeType + '"]').show();
                            setTimeout(function () {
                                $this.parents('.trades').hide();
                            }, 1000);
                        } else {
                            setTimeout(function () {
                                $this.parents('.trades').hide();
                            }, 1000);
                        }


                    } else {
                        //if pending-trades-exist-append-after-last
                        $('.trades[data-type="pending"]').last().after(tradeToAppend);
                    }
                }
                if (tradeType == 'pending') {
                    if (tradeStatus == acceptTradeStatus) {
                        $this.parent('.trade-status').find('p').after('<p>' + acceptedWaitVoting + '</p>');
                    } else if (tradeStatus == rejectTradeStatus) {
                        $this.parent('.trade-status').find('p').after('<p>' + rejectedWaitVoting + '</p>');
                    } else {
                        if ($this.parents('.trades').siblings('.trades[data-type="' + tradeType + '"]:visible').length == 0) {
                            $this.parents('.trades').siblings('.empty-pending-trades[data-type="' + tradeType + '"]').show();
                            setTimeout(function () {
                                $this.parents('.trades').hide();
                            }, 1000);
                        } else {
                            setTimeout(function () {
                                $this.parents('.trades').hide();
                            }, 1000);
                        }
                    }
                    setTimeout(function () {
                        $this.siblings('a').hide();
                        $this.hide();
                    }, 1000);
                }
                //offered-trades-accepted--and-commissioner-decides--ENDS
                if (tradeType != 'pending') {
                    //if-no-trades-left-shows-empty-trade-text-else-hide-parent-trade
                    if ($this.parents('.trades').siblings('.trades[data-type="' + tradeType + '"]:visible').length == 0) {
                        $this.parents('.trades').siblings('.empty-pending-trades[data-type="' + tradeType + '"]').show();
                        setTimeout(function () {
                            $this.parents('.trades').hide();
                        }, 1000);
                    } else {
                        setTimeout(function () {
                            $this.parents('.trades').hide();
                        }, 1000);
                    }
                }
                //if-no-trades-left-shows-empty-trade-text-else-hide-parent-trade-ENDS
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
    });
});