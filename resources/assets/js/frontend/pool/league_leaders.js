$('#stat_order_list a').on('click', function () {
    var $this = $(this),
            stat_key = $this.attr('data-stat_order_key');
    $this.addClass('active').parent().siblings().find('a').removeClass('active');

    $('#main-table').find('[data-stat_order_key="' + stat_key + '"]').show().siblings('.data-both').hide();
});
//main ajax function
function custom_paginate(current_page, last_page) {
    $paginateDiv = $('#page-list');
    if (last_page > 0) {
        $paginateDiv.empty();
        $('.post-pagination').show();
        var single_anchor_width = 30;
        for (var i = 1; i <= 4; i++) {
            if (i == current_page) {
                $paginateDiv.append('<a class="active" data-page=' + i + '>' + i + '</a>');
            } else {
                $paginateDiv.append('<a data-page=' + i + '>' + i + '</a>');
            }
        }
        $('.pagination-arrows').hide();
    } else {
        $('.post-pagination').hide();
    }
}
function getPlayerStats(url) {
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            //checking which pool type/cat is this
            if (!$.isEmptyObject(response.pool)) {
                var poolDetails = response.pool;
                if (poolDetails.pool_type == poolTypes.h2h) {
                    if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                        var poolToShow = 11;
                    } else if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly) {
                        var poolToShow = 12;
                    } else if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly_daily) {
                        var poolToShow = 13;
                    }
                } else if (poolDetails.pool_type == poolTypes.box) {
                    var poolToShow = 2;
                } else if (poolDetails.pool_type == poolTypes.standard) {
                    if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.standard_standard) {
                        var poolToShow = 31;
                    } else if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                        var poolToShow = 32;
                    }
                }
                //END checking which pool type/cat is this
            }

            $('#league-leaders-all').empty();
            if (!$.isEmptyObject(response.data)) {
                var appendlocation = $('#league-leaders-all'),
                        player_stats_template = $('#league-leaders-template').html();
                player_stats_template = _.template(player_stats_template);
                var player_data = {
                    players: response.data
                };
                appendlocation.append(player_stats_template(player_data));

                //Removing pool points if pool is h2h and roto
                if (poolToShow == 11 || poolToShow == 32) {
                    $('.total-pool-points').remove();
                }

                //Showing correct scoring points
                var stat_key = $('#stat_order_list .active').attr('data-stat_order_key');
                $('#main-table').find('[data-stat_order_key="' + stat_key + '"]').show().siblings('.data-both').hide();

                custom_paginate(response.current_page, response.last_page);
            } else {
                $('.post-pagination').hide();
                $('#league-leaders-all').append('<tr class="no-result-present"><td colspan="15">' + league_leaders.messages.no_teams_present + '</td></tr>');
            }
        },
        error: function (err) {
            $('#league-leaders-all').empty().append('<tr class="no-result-present"><td colspan="15">' + league_leaders.messages.no_teams_present + '</td></tr>>');
        }
    });
}
$(document).ready(function () {
    if (order_type) {
        if (order) {
            if (stat_order_key) {
                $('#stat_order_list').find('[data-stat_order_key=' + stat_order_key + ']').trigger('click');
            }
        }
    } else {
        $('#stat_order_list').find('li:first-child a').trigger('click');
    }
});
//loader
var checkHeight = setInterval(function () {
    if ($('#league-leaders-all').height() >= '40') {
        //Taking page to top if not
        $('body').animate({scrollTop: 0});

        $('.scorer-loader-logo').hide();
        $('.site-container').css({'background-color': '#fff'});
        $('#pool--content').css({'opacity': 1}).fadeIn('500');

        clearInterval(checkHeight);
    }
}, 100);
//END loader
//Giving width to score setting 
var settingLiSel = $('#stat_order_list li'),
        settingLength = settingLiSel.length,
        settingLiWidth = 100 / parseFloat(settingLength);
settingLiWidth = settingLiWidth - 2;
settingLiSel.width(settingLiWidth + '%');
//ENDGiving width to score setting 