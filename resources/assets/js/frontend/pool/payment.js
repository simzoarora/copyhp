$(document).ready(function () {
    $('#advertise-form').on('submit', function (e) {
        var $this = $(this);
        e.preventDefault();
        if (isEmail($this.find('.grey-input-field').val())) {
            window.open($this.data('link') + '?email=' + $this.find('.grey-input-field').val());
        } else {
            swal({
                title: error_title,
                text: error_message,
                type: 'error',
                customClass: 'sweat-alert-confirm'
            });
        }
    });
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
});