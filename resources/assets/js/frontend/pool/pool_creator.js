var poolTypeSelect = false,
        liveDraftActive = false,
        addUniqueRoundPlayerUrl;
$(document).ready(function () {
//adding min-height
    var pcHeight = $(window).height() - parseInt($('.livescoreboard').outerHeight() + $('header').outerHeight() + $('.footer--upper').outerHeight() + $('.footer--lower').outerHeight());
    $('.pool_creator').css({'min-height': pcHeight});
    //Selectboxit
    $('.select-field').selectBoxIt();
    $('.selectboxit.select-field').each(function () {
        var $this = $(this);
        var initialwidth = $this.width();
        if (!($this.closest('.selectboxit').hasClass('full'))) {
            $this.css('width', initialwidth + 30 + 'px');
            $this.siblings('ul').css('width', initialwidth + 30 + 'px');
        }
    });
    //END Selectboxit
    //DatePicker
    function validTime() {
        var draftDate = $('#live_draft_date').val(),
                draftTimeZone = $('.live_draft_time_zone').val(),
                timezonedDate = moment.tz(draftTimeZone).format('MM/DD/YYYY'),
                timezonedTime = moment.tz(draftTimeZone).format('HH:mm:ss');
        $(".time-interval-append").data("selectBox-selectBoxIt").remove();
//        $(".time-interval-append").data("selectBox-selectBoxIt").add({value: '', text: 'Please Select Time'});

        if (draftDate == timezonedDate) {
            $.each(timeIntervals, function (key, value) {
                if (key >= timezonedTime) {
                    $(".time-interval-append").data("selectBox-selectBoxIt").add({value: key, text: value});
                }
            });
        } else {
            $.each(timeIntervals, function (key, value) {
                $(".time-interval-append").data("selectBox-selectBoxIt").add({value: key, text: value});
            });
        }
    }

    $(".datepicker").datetimepicker({
        format: 'MM/DD/YYYY'
    });

    $('.live-draft.datepicker').data("DateTimePicker").minDate($.date);
    $('.live-draft.datepicker').data("DateTimePicker").maxDate(moment(new Date(maxLivedraftDate)));

    $('#live_draft_date').on('dp.change', function (e) {
        validTime();
    });
    $('.live_draft_time_zone').on('change', function (e) {
        validTime();
        e.preventDefault();
    });
    //END DatePicker  
    //Steps navigation
    $('.pool-create-nav li').on('click', function () {
        var $this = $(this);
        if ($this.hasClass('complete') == true) {
            var newStepNumber = $this.data('step'),
                    selector = $('#main-steps').find('*[data-step="' + newStepNumber + '"]');
            $this.addClass('selected').siblings().removeClass('selected');
            selector.show().siblings().hide();
        }
    });
    //END Steps navigation
    //Question mark hover
    $('.question-mark').on('mouseenter', function () {
        var $this = $(this);
        $this.css({
            background: '#fff',
            border: '2px solid #58a81e',
            color: '#58a81e'
        });
        $this.siblings('.pool-format-description').show();
    });
    $('.question-mark').on('mouseleave', function () {
        var $this = $(this);
        $this.siblings('.pool-format-description').hide();
        $this.css({
            background: '#58a81e',
            border: '2px solid #58a81e',
            color: '#fff'
        });
    });
    $('.pool-format-description').on('mouseleave', function () {
        $(this).hide();
        $('#question-mark').css({
            background: '#58a81e',
            border: '2px solid #58a81e',
            color: '#fff'
        });
    });
    $('.pool-format-description').on('mouseenter', function () {
        $(this).show();
        $('#question-mark').css({
            background: '#fff',
            border: '2px solid #58a81e',
            color: '#58a81e'
        });
    });
    //END Question mark hover
    //Pool name on focus pool type tooltip
    $('#pool-name-input').on('focus', function () {
        if (poolTypeSelect == false && $(this).val().length == 0) {
            $('#pool-type-desc-text').css('display', 'inline');
        }
    });
    $('#pool-name-input').on('focusout', function () {
        $('#pool-type-desc-text').hide();
    });
    //END
    //Point value increment & decrement
    $('.add-number').on('click', function () {
        var $this = $(this),
                initval = parseInt($this.closest('.point-value').find('input').val());
        $this.closest('.point-value').find('input').val(initval + 1);
        $this.closest('.point-value').siblings('.squaredThree').find('input').attr('checked', 'checked');
    });
    $('.subtract-number').on('click', function () {
        var $this = $(this),
                initval = parseInt($this.closest('.point-value').find('input').val());
        $this.closest('.point-value').find('input').val(initval - 1);
        $this.closest('.point-value').siblings('.squaredThree').find('input').attr('checked', 'checked');
    });
    $('.point-value input').on('keyup', function () {
        var $this = $(this);
        if ($this.val() > 0) {
            $this.parent().siblings('.squaredThree').find('input').attr('checked', 'checked');
        }
    });
    //END Point value increment & decrement
    //Pool format decides scring point-value show or hide
    $('.pool-format-h2h').on('change', function () {
        var $this = $(this);
        if ($this.val() == 2 || $this.val() == 3) {
            $('#minimum-goalie').show();
            $('.scoring_settings .point-value').show();
        } else {
            $('#minimum-goalie').hide();
            $('.scoring_settings .point-value').hide();
        }
        $('.scoring_settings table:nth-child(3) tr td:contains("GAA")').parent().show();
        $('.scoring_settings table:nth-child(3) tr td:contains("SV%")').parent().show();
    });
    $('.pool-format-standard').on('change', function () {
        var $this = $(this);
        if ($this.val() == 4) {
            $('.scoring_settings table:nth-child(3) tr td:contains("GAA")').parent().hide();
            $('.scoring_settings table:nth-child(3) tr td:contains("SV%")').parent().hide();
            $('.scoring_settings .point-value').show();
        } else if ($this.val() == 5) {
            $('.scoring_settings table:nth-child(3) tr td:contains("GAA")').parent().show();
            $('.scoring_settings table:nth-child(3) tr td:contains("SV%")').parent().show();
            $('.scoring_settings .point-value').hide();
        }
    });
    //END Pool format decides scring point-value show or hide
    //waiver time hide/show
    $('.waiver-mode').on('change', function () {
        var $this = $(this);
        if ($this.val() == 0) {
            $('.waiver-time').parent().hide();
        } else {
            $('.waiver-time').parent().show();
        }
    });
    //END waiver time hide/show
    //Max Acquisitions Per Team selector event handler
    $('.max-acquisition-team').on('change', function () {
        var $this = $(this);
        if ($this.val() == 0) {
            $('.max-acquisition-week').parent().hide();
            $('.waiver-mode').parent().hide();
            $('.waiver-time').parent().hide();
        } else {
            $('.max-acquisition-week').parent().show();
            $('.waiver-mode').parent().show();
            $('.waiver-time').parent().show();
        }
    });
    //END
    //Max Trades Per Season selector event handler
    $('.max_trades_season').on('change', function () {
        var $this = $(this);
        if ($this.val() == 0) {
            $('.trade_end_date').parent().hide();
            $('.trade_reject_time').parent().hide();
            $('.trade_management').parent().hide();
        } else {
            $('.trade_end_date').parent().show();
            $('.trade_reject_time').parent().show();
            $('.trade_management').parent().show();
        }
    });
    //END
    //Step 1 pool type change 
    function poolTypeChange(poolType) {
        var mainStepsSel = $('#main-steps');
        if (poolType == 1) {
            var pool = 'h2h-step-1';
            $('.scoring_settings .point-value').hide();
            $('.pool-create-nav').removeClass('box-step-1').removeClass('standard-step-1').addClass(pool);
            $('.pool-create-nav ul li:nth-child(3) .number').text('3');
            $('.pool-create-nav ul li:nth-child(4) .number').text('4');
            $('.pool-create-nav ul li:nth-child(5) .number').text('5');
            $('.pool-create-nav ul li:nth-child(6) .number').text('6');
            $('.step-1 .submit_btn').html('Save and continue to Teams <span class="arrow-right"></span>');
            mainStepsSel.find('.pool-box select').attr('disabled', 'disabled');
            mainStepsSel.find('.pool-standard select').attr('disabled', 'disabled');
            mainStepsSel.find('.pool-h2h select').removeAttr('disabled');
            $('.step-1 .playoff-format-div .right select').attr('disabled', 'disabled');
            $('.step-1 .playoff-format-div .switch-input:not("input[name=live_draft]")').attr('checked', false);
            $('.league-start-week').parent().show();
            mainStepsSel.find('.pool-h2h input').removeAttr('disabled');
        } else if (poolType == 2) {
            var pool = 'box-step-1';
            $('.scoring_settings .point-value').show();
            $('.pool-create-nav').removeClass('h2h-step-1').removeClass('standard-step-1').addClass(pool);
            $('.pool-create-nav ul li:nth-child(3) .number').text('2');
            $('.pool-create-nav ul li:nth-child(4) .number').text('3');
            $('.step-1 .submit_btn').html('Save and continue to Roster <span class="arrow-right"></span>');
            mainStepsSel.find('.pool-h2h select').attr('disabled', 'disabled');
            mainStepsSel.find('.pool-standard select').attr('disabled', 'disabled');
            mainStepsSel.find('.pool-standard input').attr('disabled', 'disabled');
            mainStepsSel.find('.pool-box select').removeAttr('disabled');
            mainStepsSel.find('.pool-box input').removeAttr('disabled');
            $('.scoring_settings table:nth-child(3) tr td:contains("GAA")').parent().hide();
            $('.scoring_settings table:nth-child(3) tr td:contains("SV%")').parent().hide();
            $('.league-start-week').parent().show();
        } else if (poolType == 3) {
            var pool = 'standard-step-1';
            $('.scoring_settings .point-value').show();
            $('.pool-create-nav').removeClass('h2h-step-1').removeClass('box-step-1').addClass(pool);
            $('.pool-create-nav ul li:nth-child(3) .number').text('3');
            $('.pool-create-nav ul li:nth-child(4) .number').text('4');
            $('.pool-create-nav ul li:nth-child(5) .number').text('5');
            $('.pool-create-nav ul li:nth-child(6) .number').text('6');
            $('.step-1 .submit_btn').html('Save and continue to Teams <span class="arrow-right"></span>');
            mainStepsSel.find('.pool-box select').attr('disabled', 'disabled');
            mainStepsSel.find('.pool-h2h select').attr('disabled', 'disabled');
            mainStepsSel.find('.pool-standard select').removeAttr('disabled');
            mainStepsSel.find('.pool-standard input').removeAttr('disabled');
            $('.scoring_settings table:nth-child(3) tr td:contains("GAA")').parent().hide();
            $('.scoring_settings table:nth-child(3) tr td:contains("SV%")').parent().hide();
            $('.league-start-week').parent().show();
            $('.standard_season_type').data("selectBox-selectBoxIt").selectOption(0);
        }
        $('#main-steps').removeClass('h2h-step-1').removeClass('box-step-1').removeClass('standard-step-1').addClass(pool);
    }
    $('.pool-type-select').on('change', function () {
        var $this = $(this);
        poolTypeSelect = true;
        poolTypeChange($this.val());
    });
    if (typeof urlPoolType != 'undefined') {
        poolTypeChange(urlPoolType);
    } else {
        //Page opened first time -- with no pool type in get variable
        poolTypeChange(1);
    }
    //End Step 1 pool type change
    //Scoring 1 and 2 by default checked
    if (typeof poolEditdata.id == 'undefined') {
        var scoringSettingsTable = $('.scoring_settings table');
        scoringSettingsTable.first().find('tbody tr:nth-child(1) .squaredThree input').attr('checked', true);
        scoringSettingsTable.first().find('tbody tr:nth-child(2) .squaredThree input').attr('checked', true);
    }
    $('.scoring-player').text('2');
    $('.scoring-total').html('2');
    //END Scoring 1 and 2 by default checked
    //Disable fields for edits and create
    setTimeout(function () {
        if (typeof poolEditdata.id != 'undefined') {
            var mainStepsSel = $('#main-steps');
            //till pool setting complete
            $('.pool-create-nav ul li:nth-child(1)').addClass('complete');
            if (poolEditdata.pool_type == 1) {
                $('.scoring_settings .point-value').hide();
                mainStepsSel.find('.pool-box select').attr('disabled', 'disabled');
                mainStepsSel.find('.pool-standard select').attr('disabled', 'disabled');
                $('.step-1 .playoff-format-div .right select').attr('disabled', 'disabled');
                mainStepsSel.find('.pool-h2h select').removeAttr('disabled');
                //checking id live draft is active
                if (typeof poolEditdata.live_draft_setting != 'undefined' && poolEditdata.live_draft_setting != null && poolEditdata.live_draft_setting.date != null) {
                    $('.step-6').remove();
                    $('.pool-create-nav ul li').css({'width': '20%'});
                    $('.pool-create-nav ul li:nth-child(6)').hide();
                    liveDraftActive = true;
                }
                $('#minimum-goalie').hide();

            } else if (poolEditdata.pool_type == 2) {
                var pool = 'box-step-1';
                $('.scoring_settings .point-value').show();
                $('.pool-create-nav').removeClass('h2h-step-1').removeClass('standard-step-1').addClass(pool);
                $('.pool-create-nav ul li:nth-child(3) .number').text('2');
                $('.pool-create-nav ul li:nth-child(4) .number').text('3');
                $('.step-1 .submit_btn').text('Save and continue to Roster');
                mainStepsSel.find('.pool-h2h select').attr('disabled', 'disabled');
                mainStepsSel.find('.pool-standard select').attr('disabled', 'disabled');
                mainStepsSel.find('.pool-standard input').attr('disabled', 'disabled');
                mainStepsSel.find('.pool-box select').removeAttr('disabled');
                mainStepsSel.find('.pool-box input').removeAttr('disabled');
                mainStepsSel.removeClass('h2h-step-1').removeClass('box-step-1').removeClass('standard-step-1').addClass(pool);
                $('.scoring_settings table:nth-child(3) tr td:contains("GAA")').parent().hide();
                $('.scoring_settings table:nth-child(3) tr td:contains("SV%")').parent().hide();
                $('#minimum-goalie').show();
            } else if (poolEditdata.pool_type == 3) {
                var pool = 'standard-step-1';
                $('.pool-create-nav').removeClass('h2h-step-1').removeClass('box-step-1').addClass(pool);
                $('.pool-create-nav ul li:nth-child(3) .number').text('3');
                $('.pool-create-nav ul li:nth-child(4) .number').text('4');
                $('.pool-create-nav ul li:nth-child(5) .number').text('5');
                $('.pool-create-nav ul li:nth-child(6) .number').text('6');
                $('.step-1 .submit_btn').text('Save and continue to Teams');
                mainStepsSel.find('.pool-box select').attr('disabled', 'disabled');
                mainStepsSel.find('.pool-h2h select').attr('disabled', 'disabled');
                mainStepsSel.find('.pool-standard select').removeAttr('disabled');
                mainStepsSel.find('.pool-standard input').removeAttr('disabled');
                mainStepsSel.removeClass('h2h-step-1').removeClass('box-step-1').removeClass('standard-step-1').addClass(pool);
                $('.scoring_settings table:nth-child(3) tr td:contains("GAA")').parent().hide();
                $('.scoring_settings table:nth-child(3) tr td:contains("SV%")').parent().hide();
                $('#minimum-goalie').show();
                //checking id live draft is active
                if (typeof poolEditdata.live_draft_setting != 'undefined' && poolEditdata.live_draft_setting != null && poolEditdata.live_draft_setting.date != null) {
                    $('.step-6').remove();
                    $('.pool-create-nav ul li').css({'width': '20%'});
                    $('.pool-create-nav ul li:nth-child(6)').hide();
                    liveDraftActive = true;
                }

                if (poolEditdata.pool_format == 4) {
                    $('.scoring_settings .point-value').show();
                } else if (poolEditdata.pool_format == 5) {
                    $('.scoring_settings .point-value').hide();
                }
            }

            if (poolEditdata.all_pool_teams.length > 0) {
                if (poolEditdata.all_pool_teams[0].sort_order == null) {
                    //till teams complete
                    $('.pool-create-nav ul li:nth-child(2)').addClass('complete');
                    $('#draggable').sortable({
                        update: function () {
                            $(this).find('li').each(function (i, v) {
                                var sort_order = parseInt(i) + 1;
                                $(this).find('input:nth-child(2)').val(sort_order);
                            });
                        }
                    });
                } else {
                    //till predraft complete
                    $('.pool-create-nav ul li:nth-child(2)').addClass('complete');
                    $('.pool-create-nav ul li:nth-child(5)').addClass('complete');
                    $('.pool-create-nav ul li:nth-child(6)').addClass('complete');
                    $.each(poolEditableSettings.draft_order.pages, function (i, v) {
                        var pageSel = $('#' + v);
                        pageSel.find('input').attr('disabled', 'disabled');
                        pageSel.find('select').attr('disabled', 'disabled');
                        pageSel.find('input + label.switch').find('.switch-input').attr('disabled', 'disabled');
                        pageSel.find('.submit_btn').attr('disabled', 'disabled');
                        $('#randomise-list').attr('disabled', 'disabled');
                    });
                    $('#maximum-no-team select').attr('disabled', 'disabled');
                }
            }
            //till roster complete
            if (poolEditdata.pool_rosters.length > 0) {
                $('.pool-create-nav ul li:nth-child(3)').addClass('complete');
            }

            //till scoring complete
            if (poolEditdata.pool_score_settings.length > 0) {
                $('.pool-create-nav ul li:nth-child(4)').addClass('complete');
                $.each(poolEditdata.pool_score_settings, function (i, v) {
                    $("input[name$='[pool_scoring_field_id]'][value=" + parseInt(v.pool_scoring_field_id) + "]").siblings('.squaredThree').find('input').attr('checked', true);
                    $("input[name$='[pool_scoring_field_id]'][value=" + parseInt(v.pool_scoring_field_id) + "]").siblings('.point-value').find('input').val(v.value);
                });
                //scoring player addition
                var player = 0;
                $('.scoring-player-checkbox').each(function () {
                    if ($(this).is(':checked')) {
                        player = player + 1;
                    }
                });
                $('.scoring-player').text(player);
                var goalieCount = $('.scoring-goalie').first().text(),
                        total = parseInt(player) + parseInt(goalieCount);
                $('.scoring-total').html('').html(total);
                //scoring player substraction   
                var player = 0;
                $('.scoring-goalie-checkbox').each(function () {
                    if ($(this).is(':checked')) {
                        player = player + 1;
                    }
                });
                $('.scoring-goalie').text(player);
                var playerCount = $('.scoring-player').first().text(),
                        total = parseInt(player) + parseInt(playerCount);
                $('.scoring-total').html('').html(total);
            }

            //till draft complete
//            if (poolEditdata.pool_rounds.length > 0 && poolEditdata.pool_rounds[0].pool_team_players.length > 0) {
//                $('.pool-create-nav ul li:nth-child(6)').addClass('complete');
//                $.each(poolEditableSettings.draft.pages, function (i, v) {
//                    var pageSel = $('#' + v);
//                    pageSel.find('input').attr('disabled', 'disabled');
//                    pageSel.find('select').attr('disabled', 'disabled');
//                    pageSel.find('input + label.switch').find('.switch-input').attr('disabled', 'disabled');
//                    pageSel.find('.submit_btn').attr('disabled', 'disabled');
//                });
//                //means pool creation complete
//                $.each(poolEditableSettings.pool_creation.fields, function (i, v) {
//                    var pageSel = $('#main-steps');
//                    pageSel.find('input[name="' + v + '"]').attr('disabled', 'disabled');
//                    pageSel.find('select[name="' + v + '"]').attr('disabled', 'disabled');
//                    pageSel.find('input[name="' + v + '"] + label.switch').find('.switch-input').attr('disabled', 'disabled');
//                });
//            }

            //till boxes complete //till review complete
            if (poolEditdata.pool_boxes.length > 0) {
                var boxComplete = ['roster', 'boxes'];
                $.each(boxComplete, function (i, v) {
                    var pageSel = $('#' + v);
                    pageSel.find('input').attr('disabled', 'disabled');
                    pageSel.find('select').attr('disabled', 'disabled');
                    pageSel.find('input + label.switch').find('.switch-input').attr('disabled', 'disabled');
                    pageSel.find('.submit_btn').attr('disabled', 'disabled');
                });
                $('.pool-create-nav ul li:nth-child(7)').addClass('complete');
                $('.pool-create-nav ul li:nth-child(8)').addClass('complete');
            }

            //check if user had filled till scoring
            if (poolEditdata.pool_rounds.length == 0 && poolEditdata.pool_score_settings.length > 0) {
                $('.pool-create-nav ul li:nth-child(7)').addClass('complete');
            }
        }
    }, 350);
    //END Disable fields for edits and create
    //Disable data for Pool Edit step 1
    setTimeout(function () {
        if (typeof poolEditdata.id != 'undefined') {
            $.each(poolEditableSettings.pool_setting.fields, function (i, v) {
                var $this = $('.step-1 form');
                $this.find('input[name="' + v + '"]').attr('disabled', 'disabled');
                $this.find('select[name="' + v + '"]').attr('disabled', 'disabled');
                $this.find('input[name="' + v + '"] + label.switch').find('.switch-input').attr('disabled', 'disabled');
                $('.pool-create-header select[name="pool_type"]').attr('disabled', 'disabled');
            });
        }
    }, 400);
    //END Disable data for Pool Edit step 1
    //Remove loader
    setTimeout(function () {
        $('.scorer-loader-logo').hide();
        $('.site-container').css({'background-color': '#fff'});
        $('.pool_creator').css({'opacity': 1}).fadeIn('500');
    }, 410);
    //END Remove loader
    //Step 1 ajax
    $('.step-1 form').on('submit', function (e) {
        var $this = $(this),
                poolType = $('.pool-type-select').val();
        if (poolEditdata.id != undefined) {
            var dataToSend = $this.serialize() + '&pool_type=' + poolType + '&id=' + poolEditdata.pool_setting.pool_id;
        } else {
            var poolId = $('.pool_id_input').val();
            if (poolId.length == 0) {
                var dataToSend = $this.serialize() + '&pool_type=' + poolType;
            } else {
                var dataToSend = $this.serialize() + '&pool_type=' + poolType + '&id=' + poolId;
            }
        }
        //disabling submit button
        $this.find('.submit_btn').attr('disabled', 'disabled');
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: dataToSend,
            dataType: 'json',
            success: function (response) {
                //removing all error classes
                $this.find('.has-error').removeClass('has-error');
                $this.find('.error-msg').remove();
                $this.find('.submit_btn').removeAttr('disabled');
                //adding create pool id 
                $('.pool_id_input').val(response.data.id);
                //disabling pool type select on top of page
                $('.pool-type-select').attr('disabled', 'disabled');
                //append player inputs fields
                if ($('#main-steps').hasClass('h2h-step-1')) {
                    var players_num = $('.h2h_teams').val();
                }
                if ($('#main-steps').hasClass('standard-step-1')) {
                    var players_num = $('.standard_teams').val();
                }
                if ($('#main-steps').hasClass('h2h-step-1') || $('#main-steps').hasClass('standard-step-1')) {
//                    if (poolId.length == 0) {
                    //Empty appended teams 
                    var teamPlayerAppend = $('.team-player-append'),
                            teamPlayerTemplate = $('.team-player-template').html();
                    teamPlayerTemplate = _.template(teamPlayerTemplate);
                    //Checking if team inputs has been filled
                    var teamEmailAdded = false;
                    $('#teams .invitee-email-input input').each(function (i, v) {
                        if ($(this).val().length != 0) {
                            teamEmailAdded = true;
                        }
                    });
                    var totalTeamsAppended = $('#teams .team-number');
                        var totalTeamEdit = 1;
                        if (totalTeamsAppended.length!=0 && !teamEmailAdded) {
                            teamPlayerAppend.parent().remove();
                        }

                    var teamPlayerData = {
                        players_num: players_num,
                        totalTeamEdit: totalTeamEdit
                    };
                    teamPlayerAppend.html(teamPlayerTemplate(teamPlayerData));
                    //removing extra steps
                    $('.step-7').remove();
                    $('.step-8').remove();
                    //disabling inputs
                    $.each(poolEditableSettings.pool_setting.fields, function (i, v) {
                        $this.find('input[name="' + v + '"]').attr('disabled', 'disabled');
                        $this.find('select[name="' + v + '"]').attr('disabled', 'disabled');
                        $this.find('input[name="' + v + '"] + label.switch').find('.switch-input').attr('disabled', 'disabled');
                    });
                    //checking if live draft is active or not
                    if ($('.live-draft-div .switch-blue input[type="checkbox"]').is(':checked')) {
                        //removing extra steps
                        $('.step-6').remove();
                        $('.pool-create-nav ul li').css({'width': '20%'});
                        $('.pool-create-nav ul li:nth-child(6)').hide();
                        liveDraftActive = true;
                    }
                    //making first team readonly 
                    var userEmail = $('#user_email').val();
                    $('.step-2 form .form-group').first().find('.invitee-email-input input').attr('readonly', 'readonly');
                    $('.step-2 form .form-group').first().find('.invitee-email-input input').val(userEmail);
                    //moving to next step
                    $('.pool-create-nav').find('[data-step="2"]').addClass('selected').siblings().removeClass('selected');
                    $('.pool-create-nav').find('[data-step="1"]').addClass('complete');
                    $('#main-steps').find('[data-step="2"]').show().siblings().hide();
//                    }
                } else {
                    //else it is box pool type
                    $('.step-2').remove();
                    $('.step-5').remove();
                    $('.step-6').remove();
                    $('.step-4 .submit_btn').text('Save and continue to Boxes');
                    //moving to next step
                    $('.pool-create-nav').find('[data-step="3"]').addClass('selected').siblings().removeClass('selected');
                    $('.pool-create-nav').find('[data-step="1"]').addClass('complete');
                    $('#main-steps').find('[data-step="3"]').show().siblings().hide();
                }

                $('html, body').animate({scrollTop: $('#main-steps').position().top}, 'slow');
            },
            error: function (error) {
                var formSel = $('.step-1 form');
                formSel.find('.submit_btn').removeAttr('disabled');
                formSel.find('.has-error').removeClass('has-error');
                formSel.find('.error-msg').remove();
                if ($.isEmptyObject(error)) {
                    swal({
                        title: 'Oops!!',
                        text: 'Something went wrong. Please try again.',
                        type: 'error',
                        customClass: 'sweat-alert-confirm'
                    });
                } else {
                    if (!$.isEmptyObject(error.responseText)) {
                        var responseText = JSON.parse(error.responseText);
                        $.each(responseText, function (i, v) {
                            if ($.isArray(v)) {
                                var textValue = '';
                                $.each(v, function (i, value) {
                                    textValue = textValue + ' ' + value;
                                });
                            } else {
                                var textValue = v;
                            }
                            if (i == 'logo.name') {
                                formSel.find('[name="logo[name]"]').last().addClass('has-error').after('<div class="error-msg">' + textValue + '</div>');
                            } else {
                                formSel.find('[name="' + i + '"]').last().addClass('has-error').after('<div class="error-msg">' + textValue + '</div>');
                            }
                        });
                    }
                }
            }
        });
        e.preventDefault();
    });
//End Step 1 ajax
//Step 2 ajax
    $('.step-2 form').on('submit', function (e) {
        var $this = $(this),
                poolId = $('.pool_id_input').val(),
                errorInForm = false;
        //removing all error classes
        $this.find('.has-error').removeClass('has-error');
        $this.find('.error-msg').remove();
        //disabling submit button
        $this.find('.submit_btn').attr('disabled', 'disabled');
        //checking if emails are not duplicate
        var inviteEmailsCheck = [];
        $.each($(this).find('[type="email"]'), function (i, v) {
            if ($(this).val() != '' && $(this).attr('name') != null) {
                var name = $(this).attr('name');
                inviteEmailsCheck.push(
                        $(this).val()
                        );
            }
        });
        inviteEmailsCheck.sort();
        var inviteEmailsDuplicate = [];
        for (var i = 0; i < inviteEmailsCheck.length - 1; i++) {
            if (inviteEmailsCheck[i + 1] == inviteEmailsCheck[i]) {
                inviteEmailsDuplicate.push(inviteEmailsCheck[i]);
            }
        }
        if (!$.isEmptyObject(inviteEmailsDuplicate)) {
            errorInForm = true;
            $.each(inviteEmailsDuplicate, function (i, v) {
                $('.step-2 form input[type="email"]').filter(function () {
                    return this.value == v
                }).addClass('has-error').after('<div class="error-msg">Duplicate email.</div>');
            });
        }
        //END checking if emails are not duplicate

        if (errorInForm == false) {
            $.ajax({
                type: "POST",
                url: $this.attr('action'),
                data: $this.serialize() + '&pool_id=' + poolId,
                dataType: 'json',
                success: function (response) {
                    $this.find('.submit_btn').removeAttr('disabled');
                    localStorage['teamList'] = JSON.stringify(response);
                    //checking if predraft is not done before
                    if ($('.draft-order .team-number ul').length != 0) {
                        var predraftCount = $('.draft-order .team-number ul').last().find('li:last-child').find('span').text();
                    } else {
                        predraftCount = 0;
                    }

                    //append player fields in step 5 predraft order
                    var poolInvitationsLength = 0, poolTeamsLength = 0;
                    if (response.data.pool_invitations.length > 0) {
                        poolInvitationsLength = response.data.pool_invitations.length;
                    }
                    if (response.data.pool_teams.length > 0) {
                        poolTeamsLength = response.data.pool_teams.length;
                    }
                    var totalLength = parseInt(poolInvitationsLength) + parseInt(poolTeamsLength);
                    //if pool first time
                    if (typeof poolEditdata.id == 'undefined') {
                        totalLength = parseInt(totalLength) - parseInt(predraftCount);
                    }
                    var draftOrderCountAppend = $('.draft-order-count-append'),
                            draftOrderCountTemplate = $('.draft-order-count-template').html();
                    draftOrderCountTemplate = _.template(draftOrderCountTemplate);
                    var draftOrderCountData = {
                        totalLength: totalLength,
                        predraftCount: predraftCount
                    };
                    draftOrderCountAppend.append(draftOrderCountTemplate(draftOrderCountData));
                    var draftOrderTeamAppend = $('.draft-order-team-append'),
                            draftOrderTeamTemplate = $('.draft-order-team-template').html();
                    draftOrderTeamTemplate = _.template(draftOrderTeamTemplate);
                    var draftOrderTeamData = {
                        poolInvitations: response.data.pool_invitations,
                        poolTeams: response.data.pool_teams
                    };
                    draftOrderTeamAppend.empty();
                    draftOrderTeamAppend.append(draftOrderTeamTemplate(draftOrderTeamData));
                    $('.predraft-draggable').sortable({
                        update: function () {
                            $(this).find('li').each(function (i, v) {
                                var sort_order = parseInt(i) + 1;
                                $(this).find('input:nth-child(2)').val(sort_order);
                            });
                        }
                    });
                    //moving to next step
                    $('.pool-create-nav').find('[data-step="3"]').addClass('selected').siblings().removeClass('selected');
                    $('.pool-create-nav').find('[data-step="2"]').addClass('complete');
                    $('#main-steps').find('[data-step="3"]').show().siblings().hide();
                    $('html, body').animate({scrollTop: $('#main-steps').position().top}, 'slow');
                },
                error: function (error) {
                    var formSel = $('.step-2 form');
                    formSel.find('.submit_btn').removeAttr('disabled');
                    hpApp.ajaxSwalError(error);
                }
            });
        } else {
            $this.find('.submit_btn').removeAttr('disabled');
        }
        e.preventDefault();
    });
//End step 2 ajax
//Step 3 total team size val calculations
    if (poolEditdata != '') {
        var total = 0;
        $('.roster_value').each(function () {
            var val = ($(this).val() > 0 ? $(this).val() : 0);
            total = parseInt(total) + parseInt(val);
        });
        $('.total_team_size').val(total);
    }
    $('.roster_value').on('keyup', function (evt) {
        var total = 0;
        var defaultToastrPreventDuplicates = toastr.options.preventDuplicates;
        toastr.options.preventDuplicates = true;
        var regEx = new RegExp("/[0-9]/");
        if ($(this).val().match(regEx)) {
            $(this).val('');
            evt.preventDefault();
            toastr.warning('Please Enter Numbers Only', 'Warning!!');
        }
        if ($(this).val() > 30 || $(this).val() < 0) {
            $(this).val('');
            toastr.warning('Please Enter Numbers Between 0 and 30', 'Warning!!');
        }
        $('.roster_value').each(function () {
            var val = ($(this).val() > 0 ? $(this).val() : 0);
            if (parseInt(total) + parseInt(val) > 30) {
                $(this).val('');
                val = 0;
                toastr.warning('Maximium team size can be 30', 'Warning!!');
            }
            total = parseInt(total) + parseInt(val);
        });
        $('.total_team_size').val(total);
        toastr.options.preventDuplicates = defaultToastrPreventDuplicates;
    });
//END Step 3 total team size val calculations
//Step 3 ajax
    $('.step-3 form').on('submit', function (e) {
        var total = 0;
        var $this = $(this),
                poolId = $('.pool_id_input').val();
        $('.roster_value').each(function () {
            $(this).val(+$(this).val());
            var val = ($(this).val() > 0 ? $(this).val() : 0);
            total = parseInt(total) + parseInt(val);
        });
        $('.total_team_size').val(total);
        if ($('.total_team_size').val() == 0) {
            toastr.warning('You must select at least one position for your roster size', 'Warning!!');
            return false;
        }
        //disabling submit button
        $this.find('.submit_btn').attr('disabled', 'disabled');
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize() + '&pool_id=' + poolId,
            dataType: 'json',
            success: function (response) {
                $this.find('.submit_btn').removeAttr('disabled');
                //for h2h and standard
                localStorage['roundList'] = JSON.stringify(response.rounds);
                //adding draft data for step 6
                if ($('#main-steps').hasClass('box-step-1')) {
                    var rosterSpots = [],
                            totalRosterCount = 0;
                    $('.rooster-settings table tbody tr').each(function (v) {
                        var $this = $(this);
                        if ($this.find('td:first-child').text() != 'Total Team Size') {
                            if ($this.find('td:last-child input').val().length > 0) {
                                rosterSpots.push({
                                    position: $this.find('td:first-child').text(),
                                    player_position_id: $this.find('td:nth-child(2) input').val(),
                                    rosterCount: $this.find('td:last-child input').val()
                                });
                                totalRosterCount = parseInt(totalRosterCount) + parseInt($this.find('td:last-child input').val());
                            }
                        }
                    });
                    var totalGoalies = ($('.step-3 form table').find('tr:contains("Goalie")').find('.roster_value').val().length > 0 ? $('.step-3 form table').find('tr:contains("Goalie")').find('.roster_value').val() : '0'),
                            totalPlayer = totalRosterCount - totalGoalies;
                    $('.step-8 .total').text(totalRosterCount);
                    $('.step-8 .players').text(totalPlayer);
                    $('.step-8 .goalie').text(totalGoalies);
                    var boxAppend = $('.all-boxes-append'),
                            boxTemplate = $('.each-box-template').html();
                    boxTemplate = _.template(boxTemplate);
                    var boxData = {
                        rosterSpots: rosterSpots
                    };
                    boxAppend.empty();
                    boxAppend.append(boxTemplate(boxData));
                }
                //hiding goalie stats form scoring if goalie is 0
                if ($('.step-3 form table').find('tr:contains("Goalie")').length > 0) {
                    var totalGoalies = ($('.step-3 form table').find('tr:contains("Goalie")').find('.roster_value').val().length > 0 ? $('.step-3 form table').find('tr:contains("Goalie")').find('.roster_value').val() : '0');
                } else {
                    var totalGoalies = 0;
                }
                if (totalGoalies == 0) {
                    $('.scoring_settings table:nth-child(3)').prev().hide();
                    $('.scoring_settings table:nth-child(3)').hide();
                    $('.scoring-goalie').parent().hide();
                } else {
                    $('.scoring_settings table:nth-child(3)').prev().show();
                    $('.scoring_settings table:nth-child(3)').show();
                    $('.scoring-goalie').parent().show();
                }

                //moving to next step
                $('.pool-create-nav').find('[data-step="4"]').addClass('selected').siblings().removeClass('selected');
                $('.pool-create-nav').find('[data-step="3"]').addClass('complete');
                $('#main-steps').find('[data-step="4"]').show().siblings().hide();
                $('html, body').animate({scrollTop: $('#main-steps').position().top}, 'slow');
            },
            error: function (error) {
                var formSel = $('.step-3 form');
                formSel.find('.submit_btn').removeAttr('disabled');
                hpApp.ajaxSwalError(error);
            }
        });
        e.preventDefault();
    });
//END Step 3 ajax
//Scoring check calc
    $('.scoring-player-checkbox').on('change', function () {
        var player = 0;
        $('.scoring-player-checkbox').each(function () {
            if ($(this).is(':checked')) {
                player = player + 1;
            }
        });
        //changing value
        var $this = $(this);
        if (!$this.is(':checked')) {
            $this.val(0);
        } else {
            $this.val(1);
        }
        $('.scoring-player').text(player);
        var goalieCount = $('.scoring-goalie').first().text(),
                total = parseInt(player) + parseInt(goalieCount);
        $('.scoring-total').html('');
        $('.scoring-total').html(total);
    });
    $('.scoring-goalie-checkbox').on('change', function () {
        var player = 0;
        $('.scoring-goalie-checkbox').each(function () {
            if ($(this).is(':checked')) {
                player = player + 1;
            }
        });
        //changing value
        var $this = $(this);
        if (!$this.is(':checked')) {
            $this.val(0);
        } else {
            $this.val(1);
        }
        $('.scoring-goalie').text(player);
        var playerCount = $('.scoring-player').first().text(),
                total = parseInt(player) + parseInt(playerCount);
        $('.scoring-total').html('');
        $('.scoring-total').html(total);
    });
//END Scoring check calc
//Step 4 ajax
    $('.step-4 form').on('submit', function (e) {
        var $this = $(this),
                poolId = $('.pool_id_input').val();
        //disabling submit button
        $this.find('.submit_btn').attr('disabled', 'disabled');
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize() + '&pool_id=' + poolId,
            dataType: 'json',
            success: function (response) {
                $this.find('.submit_btn').removeAttr('disabled');
                if ($('#main-steps').hasClass('box-step-1')) {
                    $('.pool-create-nav').find('[data-step="7"]').addClass('selected').siblings().removeClass('selected');
                    $('.pool-create-nav').find('[data-step="4"]').addClass('complete');
                    $('#main-steps').find('[data-step="7"]').show().siblings().hide();
                } else {
                    $('.pool-create-nav').find('[data-step="5"]').addClass('selected').siblings().removeClass('selected');
                    $('.pool-create-nav').find('[data-step="4"]').addClass('complete');
                    $('#main-steps').find('[data-step="5"]').show().siblings().hide();
                }

                $('html, body').animate({scrollTop: $('#main-steps').position().top}, 'slow');
            },
            error: function (error) {
                var formSel = $('.step-4 form');
                formSel.find('.submit_btn').removeAttr('disabled');
                hpApp.ajaxSwalError(error);
            }
        });
        e.preventDefault();
    });
//END Step 4 ajax
//Step 5 ajax
    $('.step-5 form').on('submit', function (e) {
        var $this = $(this),
                poolId = $('.pool_id_input').val();
        //disabling submit button
        $this.find('.submit_btn').attr('disabled', 'disabled');
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize() + '&pool_id=' + poolId,
            dataType: 'json',
            success: function (response) {
                if (liveDraftActive == false) {
                    //disabling pages 
                    $.each(poolEditableSettings.draft_order.pages, function (i, v) {
                        var pageSel = $('#' + v);
                        pageSel.find('input').attr('disabled', 'disabled');
                        pageSel.find('select').attr('disabled', 'disabled');
                        pageSel.find('input + label.switch').find('.switch-input').attr('disabled', 'disabled');
                        pageSel.find('.submit_btn').attr('disabled', 'disabled');
                    });
                    //adding draft data for step 6
                    var roundList = JSON.parse(localStorage['roundList']),
                            teamList = JSON.parse(localStorage['teamList']),
                            newteamList = [];
                    //while editing
                    if (poolEditdata.id != '') {
                        if (teamList.data.pool_invitations.length == 0 && teamList.data.pool_teams.length == 0) {
                            teamList.data.pool_invitations = poolEditdata.pool_invitations;
                            teamList.data.pool_teams = poolEditdata.pool_teams;
                        }
                    }
                    //sorting teams
                    $this.find('.team-name input:nth-child(1)').each(function (i, v) {
                        var teamValue = $(this).val();
                        $.each(teamList.data.pool_invitations, function (i, value) {
                            if (value.id == teamValue) {
                                newteamList.push({
                                    name: value.name,
                                    email: value.email,
                                    id: value.id,
                                    check: 0
                                });
                            }
                        });
                        $.each(teamList.data.pool_teams, function (i, value) {
                            if (value.id == teamValue) {
                                if (value.email != null) {
                                    var email = value.email;
                                } else {
                                    var email = value.user.email;
                                }
                                newteamList.push({
                                    name: value.name,
                                    email: email,
                                    id: value.id,
                                    check: 1
                                });
                            }
                        });
                    });
                    var roundAppend = $('.all-round-append'),
                            roundTemplate = $('.each-round-template').html();
                    roundTemplate = _.template(roundTemplate);
                    var roundData = {
                        teamList: newteamList,
                        roundList: roundList
                    };
                    roundAppend.append(roundTemplate(roundData));
                    $('.pool-create-nav').find('[data-step="6"]').addClass('selected complete').siblings().removeClass('selected');
                    $('#randomise-list').attr('disabled', 'disabled');
                    $('.pool-create-nav').find('[data-step="5"]').addClass('complete');
                    $('#main-steps').find('[data-step="6"]').show().siblings().hide();
                    $('html, body').animate({scrollTop: $('#main-steps').position().top}, 'slow');
                } else if (liveDraftActive == true) {
                    //last step for h2h and standard if live draft is active
                    var newPoolDashboardUrl = poolDashboardUrl.replace(0, poolId);
                    window.location.href = newPoolDashboardUrl;
                }
            },
            error: function (error) {
                var formSel = $('.step-5 form');
                formSel.find('.submit_btn').removeAttr('disabled');
                hpApp.ajaxSwalError(error);
            }
        });
        e.preventDefault();
    });
//END Step 5 ajax
//Step 6 ajax
    $('.step-6 form').on('submit', function (e) { 
        var $this = $(this),
                poolId = $('.pool_id_input').val(),
                errorInForm = false;
        //disabling submit button
        $this.find('.submit_btn').attr('disabled', 'disabled');
        //checking if all search fields are filled 
        $('.search_player').each(function () {
            var $this = $(this);
            if ($this.val().length != 0) {
                if ($this.prev().val().length == 0) {
                    $this.val('');
                    errorInForm = true;
                    $this.addClass('has-error').after('<div class="error-msg">Player name is required.</div>');
                }
            } else {
                errorInForm = true;
                $this.addClass('has-error').after('<div class="error-msg">Player name is required.</div>');
            }
        });
        if (errorInForm == false) {
            $.ajax({
                type: "POST",
                url: $this.attr('action'),
                data: $this.serialize() + '&pool_id=' + poolId,
                dataType: 'json',
                success: function (response) {
                    //last step for h2h and standard
                    var newPoolDashboardUrl = poolDashboardUrl.replace(0, poolId);
                    window.location.href = newPoolDashboardUrl;
                },
                error: function (error) {
                    var formSel = $('.step-6 form');
                    formSel.find('.submit_btn').removeAttr('disabled');
                    hpApp.ajaxSwalError(error);
                }
            });
        } else {
            $this.find('.submit_btn').removeAttr('disabled');
        }
        e.preventDefault();
    });
//END Step 6 ajax
//Step 7 ajax
    $('.step-7 form').on('submit', function (e) {
        var $this = $(this),
                poolId = $('.pool_id_input').val(),
                errorInForm = false;
        //disabling submit button
        $this.find('.submit_btn').attr('disabled', 'disabled');
        //checking if all search fields are filled
        $('.search_player').each(function () {
            var $this = $(this);
            if ($this.val().length != 0) {
                if ($this.prev().val().length == 0) {
                    $this.val('');
                    errorInForm = true;
                    $this.addClass('has-error').after('<div class="error-msg">Player name is required.</div>');
                }
            } else {
                errorInForm = true;
                $this.addClass('has-error').after('<div class="error-msg">Player name is required.</div>');
            }
        });
        if (errorInForm == false) {
            $.ajax({
                type: "POST",
                url: $this.attr('action'),
                data: $this.serialize() + '&pool_id=' + poolId,
                dataType: 'json',
                success: function (response) {
                    $this.find('.submit_btn').removeAttr('disabled');
                    $('.step-8 .invite_link').attr('href', response.data.invite_link).text(response.data.invite_link);
                    $('.pool-create-nav').find('[data-step="8"]').addClass('selected').siblings().removeClass('selected');
                    $('.pool-create-nav').find('[data-step="7"]').addClass('complete');
                    $('#main-steps').find('[data-step="8"]').show().siblings().hide();
                    $('.pool-create-nav ul li:nth-child(8)').addClass('complete');
                    $('html, body').animate({scrollTop: $('#main-steps').position().top}, 'slow');
                },
                error: function (error) {
                    var formSel = $('.step-7 form');
                    formSel.find('.submit_btn').removeAttr('disabled');
                    hpApp.ajaxSwalError(error);
                }
            });
        } else {
            $this.find('.submit_btn').removeAttr('disabled');
        }
        e.preventDefault();
    });
//END Step 7 ajax
//Search player
    $(document).on('keyup', '.search_player', function (e) {
        if (e.which != 38 && e.keyCode != 38 && e.which != 40 && e.keyCode != 40 && e.which != 13 && e.keyCode != 13) {
            $('.search_player_ul').removeClass('active').empty();
            var $this = $(this);
            if ($this.val().length > 1) {
                if ($this.parents('.each-box').find('h3 input:first-child').val() != undefined) {
                    //finding ignore players
                    var ignorePlayersList = [],
                            count = 1;
                    $this.parents('.steps').find('.each-box input:nth-child(1)').each(function (i, v) {
                        var playerValue = $(this).val();
                        if (playerValue != '') {
                            ignorePlayersList[count] = playerValue;
                        }
                        count++;
                    });
                    var data = {
                        ignore_player_id: ignorePlayersList,
                        player_position_id: $this.parents('.each-box').find('h3 input:first-child').val(),
                        name: $this.val()
                    };
                } else {
                    //removing if any previous added player id
                    $this.parent().find('input[name$="[player_id]"]').val('');
                    $('.search_player').each(function () {
                        var $this = $(this);
                        if ($this.val().length == 0) {
                            if ($this.prev().val().length > 0) {
                                $this.val('');
                                $this.prev().val('');
                            }
                        }
                    });
                    //finding ignore players
                    var ignorePlayersList = [],
                            count = 1;
                    $this.parents('.steps').find('.each-round input[name$="[player_id]"]').each(function (i, v) {
                        var playerValue = $(this).val();
                        if (playerValue != '') {
                            ignorePlayersList[count] = playerValue;
                        }
                        count++;
                    });
                    var data = {
                        ignore_player_id: ignorePlayersList,
                        name: $this.val()
                    };
                }

                $.ajax({
                    type: "GET",
                    url: $('.search_player_action').attr('href'),
                    data: data,
                    dataType: 'json',
                    success: function (response) {
                        if (response.length > 0) {
                            $this.siblings('ul').addClass('active').html('').height('auto');
                            $.each(response, function (i, v) {
                                var playerUlSel = $this.siblings('ul');
                                append = '<li data-id="' + v['id'] + '">' + v['first_name'] + ' ' + v['last_name'] + ' (' + v['current_player_season']['player_position']['short_name'] + ' - ' + v['current_player_season']['team']['short_name'] + ')</li>';
                                playerUlSel.append(append);
                            });
                            var searchPlayerUlHeight = 0;
                            $('.search_player_ul.active li').each(function (i, v) {
                                var $this = $(this);
                                searchPlayerUlHeight = parseInt(searchPlayerUlHeight) + parseInt($this.outerHeight());
                                if (i == 5) {
                                    $('.search_player_ul.active').height(searchPlayerUlHeight);
                                    return false;
                                }
                            });
                        } else {
                            $this.siblings('ul').addClass('active').html('');
                            var append = '<li>No Player Found.</li>';
                            $this.siblings('ul').append(append);
                        }
                    },
                    error: function (error) {
                        hpApp.ajaxSwalError(error);
                    }
                });
                e.preventDefault();
            }
        }
    });
    $(document).on('click', '.search_player_ul li', function () {
        var $this = $(this),
                player_id = $this.data('id'),
                name = $this.text();
        if (player_id != null) {
            $this.parent().parent().find('input[name$="[player_id]"]').val(player_id);
            $this.parent().parent().find('.search_player').val(name).removeClass('has-error');
            $this.parent().parent().find('.error-msg').remove();
				addUniqueRoundPlayer(player_id,$this.parent().parent().find('input[name$="[round_id]"]').val(),$('.pool_id_input').val(),$this.parent().parent().find('input[name$="[pool_team_id]"]').val());
        }
        $this.parent().removeClass('active').empty();
    });
	 function addUniqueRoundPlayer(player_id,round_id,pool_id,pool_team_id){
		 $.ajax({
                type: "POST",
                url: addUniqueRoundPlayerUrl,
                data: {
                    player_id: player_id,
                    round_id: round_id,
                    pool_id: pool_id,
                    pool_team_id: pool_team_id,
                },
                dataType: 'json',
                success: function (response) {
                    toastr.success(response.message, 'Success!!');
                },
                error: function (error) {
                    hpApp.ajaxSwalError(error);
                }
            });
	 }
    $(document).on('mouseover', '.search_player_ul li', function (e) {
        var $this = $(this);
        $this.siblings().removeClass('hovered');
        $this.addClass('hovered');
        e.preventDefault();
    });
    $(document).on('click', function (e) {
        if ($(e.target).closest(".search_player_ul li").length > 0) {
            return false;
        } else if ($(e.target).closest(".search_player").length > 0) {
            return false;
        } else {
            $('.search_player_ul').removeClass('active').empty().height('auto');
        }
    });
    var playerUlAnimate = false;
    $(document).on('keyup', function (e) {
        if (e.which == 38 || e.keyCode == 38) {
            //Arrow up
            var liSel = $(document).find('.search_player_ul li');
            if (liSel.length > 0) {
                if (liSel.hasClass('hovered')) {
                    if (!playerUlAnimate) {
                        $('.search_player_ul li.hovered').removeClass('hovered').prev().addClass('hovered');
                        var hoveredLiSel = $('.search_player_ul li.hovered');
                        if (hoveredLiSel.index() > 5) {
                            playerUlAnimate = true;
                            $('.search_player_ul.active').animate({
                                scrollTop: '-=' + hoveredLiSel.outerHeight() + 'px'
                            }, 'fast', function () {
                                playerUlAnimate = false;
                            });
                        }
                    }
                } else {
                    if (!playerUlAnimate) {
                        playerUlAnimate = true;
                        liSel.last().addClass('hovered');
                        var playerUlSel = $('.search_player_ul.active'),
                                searchPlayerUlHeight = 0;
                        $('.search_player_ul.active li').each(function (i, v) {
                            var $this = $(this);
                            searchPlayerUlHeight = parseInt(searchPlayerUlHeight) + parseInt($this.outerHeight());
                        });
                        var scrollTopValue = searchPlayerUlHeight - $('.search_player_ul li.hovered').height();
                        playerUlSel.animate({
                            scrollTop: scrollTopValue + 'px'
                        }, 'fast', function () {
                            playerUlAnimate = false;
                        });
                    }
                }
            }
            e.preventDefault();
        } else if (e.which == 40 || e.keyCode == 40) {
            //Arrow Down
            var liSel = $(document).find('.search_player_ul li');
            if (liSel.length > 0) {
                if (liSel.hasClass('hovered')) {
                    if (!playerUlAnimate) {
                        $('.search_player_ul li.hovered').removeClass('hovered').next().addClass('hovered');
                        var hoveredLiSel = $('.search_player_ul li.hovered');
                        if (hoveredLiSel.index() > 5) {
                            playerUlAnimate = true;
                            $('.search_player_ul.active').animate({
                                scrollTop: '+=' + hoveredLiSel.outerHeight() + 'px'
                            }, 'fast', function () {
                                playerUlAnimate = false;
                            });
                        }
                    }
                } else {
                    if (!playerUlAnimate) {
                        playerUlAnimate = true;
                        liSel.first().addClass('hovered');
                        $('.search_player_ul.active').animate({
                            scrollTop: '0px'
                        }, 'fast', function () {
                            playerUlAnimate = false;
                        });
                    }
                }
            }
            e.preventDefault();
        }
    });
    $(document).on('keydown', '.search_player', function (e) {
        if (e.which == 13 || e.keyCode == 13) {
            var liSel = $(document).find('.search_player_ul li');
            if ($(document).find('.search_player').val().length > 0) {
                if (liSel.hasClass('hovered')) {
                    var $this = $('.search_player_ul li.hovered'),
                            player_id = $this.data('id'),
                            name = $this.text();
                    if (player_id != null) {
                        $this.parent().parent().find('input[name$="[player_id]"]').val(player_id);
                        $this.parent().parent().find('.search_player').val(name).removeClass('has-error');
                        $this.parent().parent().find('.error-msg').remove();
                    }
						  addUniqueRoundPlayer(player_id,$this.parent().parent().find('input[name$="[round_id]"]').val(),$('.pool_id_input').val(),$this.parent().parent().find('input[name$="[pool_team_id]"]').val());
                    $this.parent().removeClass('active').empty(); 
                }
            }
            e.preventDefault();
				
        }
    });
//END Search player
//Image upload activate 
    $('.pool-image-upload').html5imageupload({
        onSave: function (data) {
            var $this = $('.pool-image-upload');
            $this.parent().find('[name="logo[name]"]').val(data.name);
            $this.parent().find('[name="logo[data]"]').val(data.data);
        }
    });
//END Image upload activate
//Show/hide playoff-format input field
//    if ('.playoff-format-div .switch-blue input[type="checkbox"]:checked') {
//        $('.playoff-format-div .switch-blue input[type="checkbox"]:checked').parents('.form-group').find('.right').show();
//        $('.step-1 .playoff-format-div .right select').removeAttr('disabled');
//    }
    $('.playoff-format-div .switch-blue input[type="checkbox"]').on('change', function () {
        var $this = $(this);
        if ($this.is(':checked')) {
            $this.parent().parent().find('.right').show();
            $('.step-1 .playoff-format-div .right select').removeAttr('disabled');
        } else {
            $this.parent().parent().find('.right').hide();
            $('.step-1 .playoff-format-div .right select').attr('disabled', 'disabled');
        }
    });
//END Show/hide playoff-format input field
//SimpleSwitch Js
    $('.switch-blue input[type="checkbox"]').on('change', function () {
        var $this = $(this);
        if ($this.is(':checked')) {
            $this.parent().parent().find('.switch-input').val('1');
        } else {
            $this.parent().parent().find('.switch-input').val('0');
        }
    });
    //END SimpleSwitch Js
    //Randamize li 
    $.fn.randomize = function (selector) {
        var $elems = selector ? $(this).find(selector) : $(this).children(),
                $parents = $elems.parent();
        $parents.each(function () {
            $(this).children(selector).sort(function () {
                return Math.round(Math.random()) - 0.5;
            }).detach().appendTo(this);
        });
        //sort number
        var count = 1;
        $parents.each(function () {
            var $this = $(this);
            $this.find("input[name$='[sort_order]']").val(count);
            count++;
        });
        return this;
    };
    $('#randomise-list').on('click', function (e) {
        $('.predraft-draggable').randomize();
        e.preventDefault();
    });
    //END Randamize li 
    //hide league start week when selected season as playoff
    $('.standard_season_type').on('change', function (e) {
        var $this = $(this);
        if ($this.val() == poolSeasonSettings.playoff) {
            $('.league-start-week').parent().hide();
        } else {
            $('.league-start-week').parent().show();
        }
        e.preventDefault();
    });
    //END
});
