function getPlayerStats(url, position_val) {
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            //checking which pool type/cat is this
            if (!$.isEmptyObject(response.pool)) {
                var poolDetails = response.pool;
                if (poolDetails.pool_type == poolTypes.h2h) {
                    if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                        var poolToShow = 11;
                    } else if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly) {
                        var poolToShow = 12;
                    } else if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly_daily) {
                        var poolToShow = 13;
                    }
                } else if (poolDetails.pool_type == poolTypes.box) {
                    var poolToShow = 2;
                } else if (poolDetails.pool_type == poolTypes.standard) {
                    if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.standard_standard) {
                        var poolToShow = 31;
                    } else if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                        var poolToShow = 32;
                    }
                }
                //END checking which pool type/cat is this
            }

            $('#player-stats-all').empty();
            if (!$.isEmptyObject(response.data)) {
                var appendlocation = $('#player-stats-all'),
                        player_stats_template = $('#player-stats-template').html();
                player_stats_template = _.template(player_stats_template);
                var player_data = {
                    players: response.data
                };
                appendlocation.append(player_stats_template(player_data));

                //Removing pool points if pool is h2h and roto
                if (poolToShow == 11 || poolToShow == 32) {
                    $('.total-pool-points').remove();
                }

                //Checking if position is player or goalie
                var mainTable = $('#main-table');
                if (typeof position_val != 'undefined' && position_val == inverse_player_position.g) {
                    mainTable.find('.data-goalie').show();
                    mainTable.find('.data-player').hide();
                    appendlocation.find('.data-goalie').show();
                    appendlocation.find('.data-player').hide();
                } else {
                    mainTable.find('.data-player').show();
                    mainTable.find('.data-goalie').hide();
                    appendlocation.find('.data-player').show();
                    appendlocation.find('.data-goalie').hide();
                }

                NAMESPACE.PAGINATE(response.current_page, response.last_page);
            } else {
                $('.post-pagination').hide();
                $('#player-stats-all').append('<tr class="no-result-present"><td colspan="15">' + player_stats.messages.no_results_found + '</td></tr>')
            }
        },
        error: function (err) {
            $('.post-pagination').hide();
            $('#player-stats-all').empty().append('<tr class="no-result-present"><td colspan="15">' + player_stats.messages.no_results_found + '</td></tr>>');
        }
    });
}
//Add button click event handler 
$(document).on('change', '#main-table tbody tr .custom-checkbox input', function () {
    var $this = $(this),
            trParent = $this.parents('tr');

    if ($this.is(':checked')) {
        var inputPresent = trParent.find('td:nth-child(2)').find('input');
        //checking if the player is free agent or waiver
        if (inputPresent.length > 0) {
            var positionId = inputPresent.val(),
                    addPlayerListSel = $('#add-players-list-send'),
                    playerList = addPlayerListSel.siblings('input').val();

            var playerCount = addPlayerListSel.find('span').attr('data-count');
            playerCount = parseInt(playerCount) + 1;
            addPlayerListSel.find('span').text('(' + playerCount + ')');
            addPlayerListSel.find('span').attr('data-count', playerCount);

            if (playerList.length == 0) {
                addPlayerListSel.siblings('input').val(positionId);
            } else {
                addPlayerListSel.siblings('input').val(playerList + ',' + positionId);
            }
        }
    } else {
        var inputPresent = trParent.find('td:nth-child(2)').find('input');
        //checking if the player is free agent or waiver
        if (inputPresent.length > 0) {
            var positionId = inputPresent.val(),
                    addPlayerListSel = $('#add-players-list-send'),
                    playerList = addPlayerListSel.siblings('input').val();

            var playerCount = addPlayerListSel.find('span').attr('data-count');
            playerCount = parseInt(playerCount) - 1;
            addPlayerListSel.find('span').text('(' + playerCount + ')');
            addPlayerListSel.find('span').attr('data-count', playerCount);

            if (playerList.indexOf(',') != -1) {
                var newPlayerList = playerList.replace(',' + positionId, '');
                addPlayerListSel.siblings('input').val(newPlayerList);
            } else {
                var newPlayerList = playerList.replace(positionId, '');
                addPlayerListSel.siblings('input').val(newPlayerList);
            }
        }
    }
});
//Add button click event handler
//Add event handler goes to add setup page
$('#add-players-list-send').on('click', function (e) {
    var $this = $(this),
            playerList = $this.siblings('input').val();
    if (playerList.length > 0) {
        location.href = addSetupUrl + '?new_added_players=' + playerList;
    }
    e.preventDefault();
});
//END Add event handler goes to add setup page
//loader
var checkHeight = setInterval(function () {
    if ($('#player-stats-all').height() >= '35') {
        //Taking page to top if not
        $('body').animate({scrollTop: 0});

        $('.scorer-loader-logo').hide();
        $('.site-container').css({'background-color': '#fff'});
        $('#pool--content').css({'opacity': 1}).fadeIn('500');

        clearInterval(checkHeight);
    }
}, 100);
//END loader