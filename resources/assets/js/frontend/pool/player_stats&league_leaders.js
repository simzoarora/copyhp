var NAMESPACE = {
    //    display pagination function
    PAGINATE: function paginate(current_page, last_page) {
        $paginateDiv = $('#page-list');
        if (last_page > 0) {
            $paginateDiv.empty();
            $('.post-pagination').show();
            var single_anchor_width = 30;
            for (var i = 1; i <= last_page; i++) {
                if (i == current_page) {
                    $paginateDiv.append('<a class="active" data-page=' + i + '>' + i + '</a>');
                } else {
                    $paginateDiv.append('<a data-page=' + i + '>' + i + '</a>');
                }
            }
            NAMESPACE.PAGINATE_ANIMATE(current_page, false);
        } else {
            $('.post-pagination').hide();
        }
    },
    //    END display pagination function
    PAGINATE_ANIMATE: function pagination_animate(activePage, status) {
        var pageList = $('#page-list');
        var lastPage = parseInt(pageList.find('a:last-child').data('page'));
        pageList.find('[data-page=' + activePage + ']').addClass('active').siblings().removeClass('active');
        if (activePage >= 3) {
            pageList.animate({marginLeft: -(activePage - 3) * 30}, 300);
        }
        if (activePage == 2) {
            pageList.animate({marginLeft: 0}, 300);
        }
        if (activePage != 1) {
            $('#pagination-left').removeClass('opacity0');
            $('#pagination-first').removeClass('opacity0');
        }
        if (lastPage <= 5) {
            $('.main-pagination').outerWidth(30 * lastPage);
            $('#pagination-right').addClass('opacity0');
            $('#pagination-last').addClass('opacity0');
        } else {
            $('#pagination-right').removeClass('opacity0');
            $('#pagination-last').removeClass('opacity0');
            $('.main-pagination').outerWidth(160);
        }
        if (activePage == lastPage) {
            $('#pagination-right').addClass('opacity0');
            $('#pagination-last').addClass('opacity0');
        }
        if (activePage == 1) {
            pageList.animate({marginLeft: 0}, 300);
            $('#pagination-left').addClass('opacity0');
            $('#pagination-first').addClass('opacity0');
        }
        if (status) {
            NAMESPACE.CHANGE_URL("paginate", "&page=" + activePage);
        }
    }, //    main change url function COMMON
    CHANGE_URL:
            function changeUrl(event, add_url) {
                var season_val = $('#season').val(),
                        position_val = $('#position').val(),
                        pool_teams_val = $('#pool_teams').val(),
                        search_input_val = $('#search-input').val(),
                        status = false,
                        new_url = url.split("?");
//        if season is selected
                if (season_val != "0") {
                    new_url = new_url[0] + "?data_retrieval=season&season_id=" + season_val;
                    status = true;
                }
//        if any of the today/week/month is selected
                if ($('#league-nav li').hasClass('active')) {
                    $this = $('#league-nav').find('.active');
                    if ($this.data('start_date')) {
                        $('#season_parent').hide();
                        new_url = new_url[0] + "?start_date=" + $this.data('start_date') + "&end_date=" + $this.data('end_date');
                        status = true;
                    } else {
                        $('#season_parent').show();
                    }
                }
                //  if custom dates are coming
                if (!status) {
                    if (start_date && end_date) {
                        new_url = new_url + "?start_date=" + start_date + '&end_date=' + end_date;
                        status = true;
                    }
                }
                if (event == 'paginate') {
                    new_url = new_url + add_url;
                } else {
                    $('#page-list').animate({marginLeft: 0}, 300);
                }
//        check whether table has sorting applied or not
                if ($('#main-table').find('th').hasClass('asc')) {
                    $this = $('#main-table').find('.asc');
                    if ($this.data("stat_order_key")) {
                        new_url = new_url + "&order_type=asc&order=" + $this.data("sortname") + "&stat_order_key=" + $this.data("stat_order_key");
                    } else {
                        new_url = new_url + "&order_type=asc&order=" + $this.data("sortname");
                    }
                }
                if ($('#main-table').find('th').hasClass('desc')) {
                    $this = $('#main-table').find('.desc');
                    if ($this.data("stat_order_key")) {
                        new_url = new_url + "&order_type=desc&order=" + $this.data("sortname") + "&stat_order_key=" + $this.data("stat_order_key");
                    } else {
                        new_url = new_url + "&order_type=desc&order=" + $this.data("sortname");
                    }
                }
                //end check whether table has sorting applied or not
                if (typeof position_val != 'undefined' && position_val != "0") {
                    new_url = new_url + "&player_position_id=" + position_val;
                }
                if (typeof pool_teams_val != 'undefined' && pool_teams_val != "0") {
                    new_url = new_url + "&team=" + pool_teams_val;
                }
                if (typeof search_input_val != 'undefined' && search_input_val != "") {
                    new_url = new_url + "&player_name=" + search_input_val;
                }
                if (status) {
                    if (typeof position_val != 'undefined' && position_val != "0") {
                        getPlayerStats(new_url, position_val);
                    } else {
                        getPlayerStats(new_url);
                    }
                    new_browser_url = new_url.split('?');
                    window.history.pushState('title', '', '?' + new_browser_url[1]);
                }
            }
}
$(document).ready(function () {
    $('.select-field').selectBoxIt();
//   END main change url function COMMON
//    search on SORTING IN TABLE
    $(document).on('click', 'th', function () {
        var $this = $(this);
        if ($this.data("sortname")) {
            if ($this.hasClass('desc')) {
//                earlier descending. changed it to ascending
                $this.removeClass('desc').addClass('asc');
            } else if ($this.hasClass('asc')) {
//                earlier ascending. changed to descending
                $this.removeClass('asc').addClass('desc');
            }
//                earlier NOTHING. changed it to ascending
            else {
                $this.addClass('asc');
            }
            $this.siblings().removeClass('asc desc');
            NAMESPACE.CHANGE_URL();
        }
    });
//    END search on SORTING IN TABLE
//    search on clicking today tomorrow month etc.. (THIS TRIGGERS THE $('#season') change function)
    $('#league-nav li').on('click', function () {
        $this = $(this);
        $this.addClass('active').siblings().removeClass('active');
        if ($this.data('start_date')) {
            var selectBox = $("#season").data("selectBox-selectBoxIt");
            selectBox.selectOption(0);
        } else {
            $('#season_parent').show();
        }
    });
//    END search on clicking today tomorrow month etc
//    search on clicking pagination buttons

//    right pagination arrow on click
    $('#pagination-right').on('click', function () {
        var activePage = parseInt($('#page-list').find('.active').data('page'));
        NAMESPACE.PAGINATE_ANIMATE(activePage + 1, true);
    });
    //    left pagination arrow on click
    $('#pagination-left').on('click', function () {
        var activePage = parseInt($('#page-list').find('.active').data('page'));
        NAMESPACE.PAGINATE_ANIMATE(activePage - 1, true);
    });
    $('#pagination-first').on('click', function () {
        NAMESPACE.PAGINATE_ANIMATE(1, true);
    });
    $('#pagination-last').on('click', function () {
        NAMESPACE.PAGINATE_ANIMATE(parseInt($('#page-list').find('a:last-child').data('page')), true);
    });
    //    pagination NUMBERS arrow on click
    $('.post-pagination').on('click', 'a', function () {
        var activePage = parseInt($(this).data('page'));
        NAMESPACE.PAGINATE_ANIMATE(activePage, true);
    });

//    END search on clicking pagination buttons
//    search on entering text in the input field
    $('#search-input').on('keyup', function () {
        NAMESPACE.CHANGE_URL();
    });
    $('#season').on('change', function () {
        NAMESPACE.CHANGE_URL();
    });
    $('#position').on('change', function () {
        NAMESPACE.CHANGE_URL();
    });
    $('#pool_teams').on('change', function () {
        NAMESPACE.CHANGE_URL();
    });
//    END search on entering text in the input field
    if (start_date || data_retrieval) {
        // sorting
        if (order_type) {
            if (order) {
                if (stat_order_key) {
                    $('#main-table').find('th').each(function () {
                        var $this = $(this);
                        if ($this.data('stat_order_key') == stat_order_key) {
                            $this.addClass(order_type);
                        }
                    });
                } else {
                    $('#main-table').find('th').each(function () {
                        $this = $(this);
                        if ($this.data('sortname') == order) {
                            $this.addClass(order_type);
                        }
                    });
                }
            }
        }
        // END sorting
        if (start_date && end_date) {
            var dateFound = false;
            $('#league-nav li').each(function () {
                var $this = $(this);
                if ($this.data('start_date') == start_date && $this.data('end_date') == end_date) {
                    $this.addClass('active');
                    dateFound = true;
                }
            });
            if (page) {
                NAMESPACE.CHANGE_URL("paginate", "&page=" + page);
            } else {
                NAMESPACE.CHANGE_URL();
            }
            $('#season_parent').hide();
        } else if (data_retrieval && season_id) {
            if (page) {
                NAMESPACE.CHANGE_URL("paginate", "&page=" + page);
            } else {
                NAMESPACE.CHANGE_URL();
            }
        }
    }
//        nothing is coming
    else {
        $('#league-nav li:last-child').trigger('click');
        var selectBox = $("#season").data("selectBox-selectBoxIt");
        selectBox.selectOption(1);
    }
});