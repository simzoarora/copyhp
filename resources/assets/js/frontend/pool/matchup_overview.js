$(document).ready(function () {
    $('.select-field').selectBoxIt();
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    //getting data
    function poolMatchups(url) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                if (!$.isEmptyObject(response)) {
                    //checking which pool type/cat is this
                    if (response.pool_type == poolTypes.h2h) {
                        if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                            var poolToShow = 11;
                        } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly) {
                            var poolToShow = 12;
                        } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly_daily) {
                            var poolToShow = 13;
                        }
                    } else if (response.pool_type == poolTypes.box) {
                        var poolToShow = 2;
                    } else if (response.pool_type == poolTypes.standard) {
                        if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_standard) {
                            var poolToShow = 31;
                        } else if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                            var poolToShow = 32;
                        }
                    }
                    //END checking which pool type/cat is this

                    //Calculating current team
                    if (!$.isEmptyObject(response.pool_team)) {
                        var currentTeamId = response.pool_team.id;
                    }
                    //END Calculating current team

                    //Append h2h data
                    $('#pools-h2h-outer').empty();
                    if (!$.isEmptyObject(response.pool_matchups)) {
                        if (poolToShow == 11) {
                            $.each(response.pool_matchups, function (i, v) {
                                var poolsH2hOuter = $('#pools-h2h-outer'),
                                        poolsH2hTemplate = $('#pools-h2h-template').html();
                                poolsH2hTemplate = _.template(poolsH2hTemplate);
                                var poolsH2hData = {
                                    poolMatchups: v,
                                    poolScoreSettings: response.pool_score_settings,
                                    siteLink: api_url
                                };
                                poolsH2hOuter.append(poolsH2hTemplate(poolsH2hData));
                            });

                            hpApp.h2hStatsSetting();
                            //bottom data colors
                            $.each(response.pool_matchups, function (i, v) {
                                $.each(v.cat_result, function (index, value) {
                                    if (value.loss == true) {
                                        $('.score-setting-' + v.id + '-' + index).addClass('blue');
                                    } else {
                                        if (value.win == currentTeamId) {
                                            $('.score-setting-' + v.id + '-' + index).addClass('green');
                                        } else {
                                            $('.score-setting-' + v.id + '-' + index).addClass('red');
                                        }
                                    }
                                });
                            });
                        } else if (poolToShow == 12 || poolToShow == 13) {
                            $.each(response.pool_matchups, function (i, v) {
                                var poolsH2hOuter = $('#pools-h2h-outer'),
                                        poolsH2hTemplate = $('#pools-h2h-template').html();
                                poolsH2hTemplate = _.template(poolsH2hTemplate);
                                var poolsH2hData = {
                                    poolMatchups: v,
                                    siteLink: api_url
                                };
                                poolsH2hOuter.append(poolsH2hTemplate(poolsH2hData));
                            });
                        }

                    } else {
                        //if the pool h2h data is not coming
                        if (poolToShow == 11 || poolToShow == 12 || poolToShow == 13) {
                            $('#pools-h2h-outer').empty().append('<p class="no-result-present">' + pool_dashboard.messages.no_pool_present + '</p>');
                        }
                    }
                    //END Append h2h data
                } else {
                    //no result
                    $('.pool--content').empty().append('<p class="no-result-present">' + pool_dashboard.messages.invalid_pool + '</p>');
                }

                $('.scorer-loader-logo').hide();
                $('.site-container').css({'background-color': '#fff'});
                $('#pool--content').css({'opacity': 1}).fadeIn('500');
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
    }
    poolMatchups(poolOverviewUrl);
    //END getting data
    //Season week select event handler
    $('.season-week-select').on('change', function () {
        poolMatchups(poolOverviewUrl + '?season_competition_week_id=' + $(this).val());
    });
    //END Season week select event handler
});