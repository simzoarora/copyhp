$(document).ready(function () {
    function fillEmptyTable(sel, message) {
        if (hpApp.isElementEmpty(sel.find('tbody'))) {
            var colspan = sel.find('thead').find('th').length;
            sel.find('tbody').append('<tr><td colspan="' + colspan + '" class="no-result-present">' + message + '</td><tr>');
        }
    }
    //taking page to top if not
    $('body').animate({scrollTop: 0});
    //getting data
    $.ajax({
        url: liveStatsUrl,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            if (!$.isEmptyObject(response)) {
                
                //checking which pool type/cat is this
                if (response.pool_type == poolTypes.h2h) {
                    if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                        var poolToShow = 11;
                    } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly) {
                        var poolToShow = 12;
                    } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly_daily) {
                        var poolToShow = 13;
                    }
                } else if (response.pool_type == poolTypes.box) {
                    var poolToShow = 2;
                } else if (response.pool_type == poolTypes.standard) {
                    if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_standard) {
                        var poolToShow = 31;
                    } else if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                        var poolToShow = 32;
                    }
                }
                //END checking which pool type/cat is this
                
                //Append team data
                $('#live-stats-head-outer').empty();
                if (!$.isEmptyObject(response.pool_teams)) {
                    //Eliminating duplicating 
                    var poolScoreSettings = [];
                    $.each(response.pool_score_settings, function (i, v) {
                        var currentFieldId = v.pool_scoring_field_id,
                                found = false;
                        //search
                        if (poolScoreSettings.length > 0) {
                            $.each(poolScoreSettings, function (index, value) {
                                if (currentFieldId == value.pool_scoring_field_id) {
                                    found = true;
                                }
                            });
                        }
                        //Adding array
                        if (!found) {
                            poolScoreSettings.push(v);
                        }
                    });
                    //END 
//                    console.log('livestats',response);
//                    console.log('pooltoshow',poolToShow);
                    var liveStatsHeadOuter = $('#live-stats-head-outer'),
                            liveStatsHeadTemplate = $('#live-stats-head-template').html();
                    liveStatsHeadTemplate = _.template(liveStatsHeadTemplate);
                    var liveStatsHeadData = {
                        poolScoreSettings: poolScoreSettings,
                        poolTeams: response.pool_teams,
                        poolToShow: poolToShow
                    };
                    liveStatsHeadOuter.append(liveStatsHeadTemplate(liveStatsHeadData));

                    $.each(response.pool_teams, function (i, v) {
                        $.each(v.team_stat, function (index, value) {
                            $('#con-' + v.id + '-' + i + '-' + index).html(hpApp.twoDecimal(value));
                        });
                        if (v.team_stat.total_points != null) {
                            $('#con-' + v.id + '-total_points').html(hpApp.twoDecimal(v.team_stat.total_points));
                        }
                    });
                    $('#live-stats-head-outer table').DataTable({
                        "paging": false,
                        "info": false,
                        "searching": false
                    });
                } else {
                    //empty response
                    var colspan = $('#live-stats-head-outer thead').find('th').length;
                    $('#live-stats-head-outer tbody').empty().append('<tr><td colspan="' + colspan + '" class="no-result-present">' + live_stats.messages.no_live_stats_prsent + '</td><tr>');
                }
                //END Append team data
                //Append each team data
                $('#each-teams-outer').empty();
                if (!$.isEmptyObject(response.pool_teams)) {
                    $.each(response.pool_teams, function (i, v) {
                        var eachTeamsOuter = $('#each-teams-outer'),
                                eachTeamsTemplate = $('#each-teams-template').html();
                        eachTeamsTemplate = _.template(eachTeamsTemplate);
                        var eachTeamsData = {
                            poolScoreSettings: poolScoreSettings,
                            poolScoringFields: poolScoringFields,
                            siteLink: api_url,
                            poolTeam: v,
                            poolToShow: poolToShow
                        };
                        eachTeamsOuter.append(eachTeamsTemplate(eachTeamsData));
                    });

                    $.each(response.pool_teams, function (i, v) {
                        $.each(v.pool_team_players, function (index, value) {
                            if (value.player_season.player_position.name != poolScoringFields.type[2]) {
                                $.each(value.player_stat, function (index2, value2) {
                                    $('#con-' + value.id + '-' + index2).html(hpApp.twoDecimal(value2));
                                });
                            } else {
                                $.each(value.player_stat, function (index2, value2) {
                                    $('#con-' + value.id + '-g-' + index2).html(hpApp.twoDecimal(value2));
                                });
                            }
                        });
                        //lineup starting data
                        $.each(v.off_starting_team_stat, function (index, value) {
                            $('#lineup-' + v.id + '-' + index).html(hpApp.twoDecimal(value));
                        });
                        if (v.off_starting_team_stat.total_points != null) {
                            $('#lineup-' + v.id + '-total_points').html(hpApp.twoDecimal(v.off_starting_team_stat.total_points));
                        }
                        //lineup goalie data
                        $.each(v.goalie_starting_team_stat, function (index, value) {
                            $('#lineup-' + v.id + '-g-' + index).html(hpApp.twoDecimal(value));
                        });
                        if (v.goalie_starting_team_stat.total_points != null) {
                            $('#lineup-' + v.id + '-g-total_points').html(hpApp.twoDecimal(v.goalie_starting_team_stat.total_points));
                        }
                    });
                    //Showing message if data is not present
                    $('.lineup-totals').each(function (i, v) {
                        var $trthis = $(this);
                        if ($trthis.siblings().length == 0) {
                            $trthis.remove();
                        }
                    });
                    $('.team-players').each(function (i, v) {
                        fillEmptyTable($(this), live_stats.messages.no_player_present);
                    });
                    $('.team-goalies').each(function (i, v) {
                        fillEmptyTable($(this), live_stats.messages.no_goalie_present);
                    });
                }
                //END Append each team data
            } else {
                //empty div
                $('.pool--content').empty().append('<p class="no-result-present">' + live_stats.messages.invalid_pool + '</p>');
            }

            //removing loader
            $('.scorer-loader-logo').hide();
            $('.site-container').css({'background-color': '#fff'});
            $('#pool--content').css({'opacity': 1});
        },
        error: function (error) {
            hpApp.ajaxSwalError(error);
        }
    });
    //END getting data

    $(window).resize(function () {
        if ($(window).width() < 768) {
            $('#live-stats-head-outer table').DataTable().destroy();
            $('#live-stats-head-outer table th').width('auto');
        }
    });
}); 