var poolToShow;
$(document).ready(function () {
    $('.select-field').selectBoxIt();
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    //Pool team season data
    $.ajax({
        url: poolTeamSeasonUrl,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            //checking which pool type/cat is this
            if (!$.isEmptyObject(response.pool)) {
                var poolDetails = response.pool;
                if (poolDetails.pool_type == poolTypes.h2h) {
                    if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                        poolToShow = 11;
                    } else if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly) {
                        poolToShow = 12;
                    } else if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly_daily) {
                        poolToShow = 13;
                    }
                } else if (poolDetails.pool_type == poolTypes.box) {
                    poolToShow = 2;
                } else if (poolDetails.pool_type == poolTypes.standard) {
                    if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.standard_standard) {
                        poolToShow = 31;
                    } else if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                        poolToShow = 32;
                    }
                }
                //END checking which pool type/cat is this
            }

            if (!$.isEmptyObject(response.stat_data)) {
                //Append players data
                var allPlayersOuter = $('#all-players-outer'),
                        allPlayersTemplate = $('#all-players-template').html();
                allPlayersTemplate = _.template(allPlayersTemplate);
                var allPlayersData = {
                    siteLink: api_url,
                    playersTeam: response,
                    poolScoringFields: poolScoringFields
                };
                allPlayersOuter.append(allPlayersTemplate(allPlayersData));
                //END Append players data

                //Checking if players and goalies tables are empty
                if ($('#all-players-outer #rosters-players tbody').find('tr').length == 0) {
                    var colspan = $('#all-players-outer #rosters-players thead').find('th').length;
                    $('#all-players-outer #rosters-players tbody').append('<tr><td colspan="' + colspan + '" class="no-result-present">' + add_setup.messages.no_player_present + '</td><tr>');
                }
                if ($('#all-players-outer #rosters-goalies tbody').find('tr').length == 0) {
                    var colspan = $('#all-players-outer #rosters-goalies thead').find('th').length;
                    $('#all-players-outer #rosters-goalies tbody').append('<tr><td colspan="' + colspan + '" class="no-result-present">' + add_setup.messages.no_goalie_present + '</td><tr>');
                }
            } else {
                //empty response
                var colspan = $('#rosters-players thead').find('th').length;
                $('#rosters-players tbody').empty().append('<tr><td colspan="' + colspan + '" class="no-result-present">' + add_setup.messages.no_player_added + '</td><tr>');
            }

            //removing loader
            $('.scorer-loader-logo').hide();
            $('.site-container').css({'background-color': '#fff'});
            $('#pool--content').css({'opacity': 1});
        },
        error: function (error) {
            hpApp.ajaxSwalError(error);
        }
    });
    //END Pool team season  data
    //Getting add players
    if (newAddedPlayers != 0) {
        $.ajax({
            url: addedPlayersUrl,
            type: 'GET',
            data: {
                player_season_ids: newAddedPlayers
            },
            dataType: 'json',
            success: function (response) {
                if (!$.isEmptyObject(response)) {
                    //Append players data
                    $.each(response, function (i, v) {
                        var addPlayersOuter = $('#add-players-outer'),
                                addPlayersTemplate = $('#add-players-template').html();
                        addPlayersTemplate = _.template(addPlayersTemplate);
                        var addPlayersData = {
                            playerTeam: v,
                            playerCount: i
                        };
                        addPlayersOuter.append(addPlayersTemplate(addPlayersData));
                        //adding new players in roster size
                        var sPosition = v.player_position.short_name,
                                sPosition = $('#at-' + sPosition),
                                transactionSel = $('#total_after_transaction'),
                                sPositionValue = sPosition.text(),
                                totalAfterTransaction = transactionSel.text();

                        sPosition.text(parseInt(sPositionValue) + 1);
                        transactionSel.text(parseInt(totalAfterTransaction) + 1);
                        //add new player name in player list at add confirmstion page
                        $('#add-player-list span').append(', ' + v.player.full_name + ' (' + v.team.short_name + ')');

                        //Removing pool points if pool is h2h and roto
                        if (poolToShow == 11 || poolToShow == 32) {
                            $('.total-pool-points').remove();
                        }
                    });
                    //END Append players data
                } else {
                    //empty response
                    var colspan = $('#add-players-outer').siblings('thead').find('th').length;
                    $('#add-players-outer').empty().append('<tr><td colspan="' + colspan + '" class="no-result-present">' + add_setup.messages.no_player_added + '</td><tr>');
                }
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
    } else {
        var colspan = $('#add-players-outer').siblings('thead').find('th').length;
        $('#add-players-outer').empty().append('<tr><td colspan="' + colspan + '" class="no-result-present">' + add_setup.messages.no_player_added + '</td><tr>');
    }

    //Drop/Trade players
    $(document).on('change', '#rosters-players .pool-player .custom-checkbox input', function () {
        var $this = $(this),
                trParent = $this.parents('.pool-player'),
                sPosition = trParent.attr('data-pos');

        if ($this.is(':checked')) {
            trParent.addClass('drop-player');
            //If bench comes and the selector is not present
            if ($('#at-' + sPosition).length != 0) {
                var sPositionSel = $('#at-' + sPosition),
                        sPositionValue = sPositionSel.text();
                sPositionSel.text(parseInt(sPositionValue) - 1);
            }

            var transactionSel = $('#total_after_transaction'),
                    totalAfterTransaction = transactionSel.text();
            transactionSel.text(parseInt(totalAfterTransaction) - 1);

            //Add player name to player list
            $('#drop-player-list span').append(', ' + trParent.find('td:nth-child(4)').text());

            //add player id in input
            var tr2child = trParent.find('td:nth-child(2)'),
                    playerId = tr2child.attr('data-id');
            tr2child.find('input').val(playerId);

        } else {
            trParent.removeClass('drop-player');

            if ($('#at-' + sPosition).length != 0) {
                var sPositionSel = $('#at-' + sPosition),
                        sPositionValue = sPositionSel.text();
                sPositionSel.text(parseInt(sPositionValue) + 1);
            }

            var transactionSel = $('#total_after_transaction'),
                    totalAfterTransaction = transactionSel.text();
            transactionSel.text(parseInt(totalAfterTransaction) + 1);

            //remove player name to player list
            var playerName = trParent.find('td:nth-child(4)').text(),
                    addPlayerList = $('#drop-player-list span').text(),
                    newAddPlayerList = addPlayerList.replace(', ' + playerName, '');
            $('#drop-player-list span').text(newAddPlayerList);

            //remove player id in input
            trParent.find('td:nth-child(2) input').val('');
        }

        var totalAfterTransaction = parseInt($('#total_after_transaction').text()),
                totalAvailableSpots = parseInt($('#total_available_spots').text());
        if (totalAfterTransaction > totalAvailableSpots) {
            $('#review-trans').attr('disabled', 'disabled');
        } else {
            $('#review-trans').removeAttr('disabled');
        }
    });
    $(document).on('change', '#rosters-goalies .pool-goalie .custom-checkbox input', function () {
        var $this = $(this),
                trParent = $this.parents('.pool-goalie'),
                sPosition = trParent.attr('data-pos');

        if ($this.is(':checked')) {
            trParent.addClass('drop-player');
            var sPositionValue = $('#at-' + sPosition).text(),
                    totalAfterTransaction = $('#total_after_transaction').text();
            $('#at-' + sPosition).text(parseInt(sPositionValue) - 1);
            $('#total_after_transaction').text(parseInt(totalAfterTransaction) - 1);

            //Add player name to player list
            $('#drop-player-list span').append(', ' + trParent.find('td:nth-child(4)').text());

            //add player id in input
            var tr2child = trParent.find('td:nth-child(2)'),
                    playerId = tr2child.attr('data-id');
            tr2child.find('input').val(playerId);
        } else {
            trParent.removeClass('drop-player');
            var sPositionValue = $('#at-' + sPosition).text(),
                    totalAfterTransaction = $('#total_after_transaction').text();
            $('#at-' + sPosition).text(parseInt(sPositionValue) + 1);
            $('#total_after_transaction').text(parseInt(totalAfterTransaction) + 1);

            //remove player name to player list
            var playerName = trParent.find('td:nth-child(4)').text(),
                    addPlayerList = $('#drop-player-list span').text(),
                    newAddPlayerList = addPlayerList.replace(', ' + playerName, '');
            $('#drop-player-list span').text(newAddPlayerList);

            //remove player id in input
            trParent.find('td:nth-child(2) input').val('');
        }

        var totalAfterTransaction = parseInt($('#total_after_transaction').text()),
                totalAvailableSpots = parseInt($('#total_available_spots').text());
        if (totalAfterTransaction > totalAvailableSpots) {
            $('#review-trans').attr('disabled', 'disabled');
        } else {
            $('#review-trans').removeAttr('disabled');
        }
    });
    //END Drop/Trade players
    //Review Transaction click
    $('#review-trans').on('click', function (e) {
        var totalAfterTransaction = parseInt($('#total_after_transaction').text()),
                totalAvailableSpots = parseInt($('#total_available_spots').text());
        if (totalAfterTransaction <= totalAvailableSpots) {
            $('#pool--content').addClass('add-confirmation');

            var noRosterPlayer = false;
            $('#rosters-players tbody tr').each(function (i, v) {
                if ($(this).hasClass('drop-player')) {
                    noRosterPlayer = true;
                }
            });
            if (noRosterPlayer == false) {
                $('#rosters-players').hide();
            } else {
                $('#rosters-players').show();
            }

            var noRosterGoalie = false;
            $('#rosters-goalies tbody tr').each(function (i, v) {
                if ($(this).hasClass('drop-player')) {
                    noRosterGoalie = true;
                }
            });
            if (noRosterGoalie == false) {
                $('#rosters-goalies').hide();
            } else {
                $('#rosters-goalies').show();
            }

            if (noRosterPlayer == false && noRosterGoalie == false) {
                $('#my-team-head').hide();
            }
            //Checking if no player text is not present from last click
            var addPlayerList = $('#add-player-list span').text(),
                    newAddPlayerList = addPlayerList.replace(add_setup.messages.no_player, '');
            $('#add-player-list span').text(newAddPlayerList);
            var addPlayerList = $('#drop-player-list span').text(),
                    newAddPlayerList = addPlayerList.replace(add_setup.messages.no_player, '');
            $('#drop-player-list span').text(newAddPlayerList);

            //check if add player or drop list span are not empty
            if ($('#add-player-list span').text() == 0) {
                $('#add-player-list span').text(add_setup.messages.no_player);
            }
            if ($('#drop-player-list span').text() == 0) {
                $('#drop-player-list span').text(add_setup.messages.no_player);
            }
        }
        e.preventDefault();
    });
    $('#cancel-confirmation').on('click', function (e) {
        $('#pool--content').removeClass('add-confirmation');
        $('#rosters-players').show();
        $('#rosters-goalies').show();
        $('#my-team-head').show();

        $('body').animate({
            scrollTop: $('.select__players--drop').offset().top
        });
        e.preventDefault();
    });
    //END Review Transaction click
    //Cancel trans and go back to add aetup page
    $('#cancel-trans').on('click', function (e) {
        location.href = playerStatsUrl;
        e.preventDefault();
    });
    //Cancel trans and go back to add aetup page
    //Accept click for confirm players
    $('#accept-confirmation').on('click', function (e) {
        var $this = $('#add-player-form');
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (response) {
                location.href = playerStatsUrl;
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
        e.preventDefault();
    });
    //END Accept click for confirm players
}); 