$(document).ready(function () {
    $('.select-field').selectBoxIt();
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    //Team players data func
    function getTeamData(url, outerContainer, loaderRemove, teamFirst) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function (response) {
                //checking which pool type/cat is this
                var poolDetails = response.pool;
                if (poolDetails.pool_type == poolTypes.h2h) {
                    if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                        var poolToShow = 11;
                    } else if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly) {
                        var poolToShow = 12;
                    } else if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly_daily) {
                        var poolToShow = 13;
                    }
                } else if (poolDetails.pool_type == poolTypes.box) {
                    var poolToShow = 2;
                } else if (poolDetails.pool_type == poolTypes.standard) {
                    if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.standard_standard) {
                        var poolToShow = 31;
                    } else if (poolDetails.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                        var poolToShow = 32;
                    }
                }
                //END checking which pool type/cat is this
                if (!$.isEmptyObject(response.stat_data)) {
                    //Append players data
                    outerContainer.empty();
                    var allPlayersOuter = outerContainer,
                            allPlayersTemplate = $('#all-players-template').html();
                    allPlayersTemplate = _.template(allPlayersTemplate);
                    var allPlayersData = {
                        playersTeam: response,
                        poolScoringFields: poolScoringFields,
                        teamFirst: teamFirst
                    };
                    allPlayersOuter.append(allPlayersTemplate(allPlayersData));

                    //Removing pool points if pool is h2h and roto
                    if (poolToShow == 11 || poolToShow == 32) {
                        $('.total-pool-points').remove();
                    }

                    //Counting team players
                    var totalPlayers = 0;
                    $.each(response.pool.pool_team_first.pool_team_players, function (i, v) {
                        if (!$.isEmptyObject(v)) {
                            totalPlayers = totalPlayers + 1;
                        }
                    });
                    if (teamFirst == true) {
                        //Adding team name
                        $('#team-name-f').text(response.pool.pool_team_first.name);
                        //Adding team count
                        $('#team-players-f').text(totalPlayers);
                        $('#team-trade-f').text(0);
                        $('#team-dropped-f').text(0);
                    } else {
                        //Adding team name
                        $('#team-name-s').text(response.pool.pool_team_first.name);
                        //Adding team count
                        $('#team-players-s').text(totalPlayers);
                        $('#team-trade-s').text(0);
                        $('#team-dropped-s').text(0);
                    }
                    //Changing the accepted graphics if needed
                    if (teamFirst == true) {
                        var teamSpotsCount = $('#team-spots-f').text();
                        if (teamSpotsCount > totalPlayers) {
                            $('#team-graphics-f span').removeClass('right-cross').addClass('right-tick');
                            $('#propose-trade-submit').removeAttr('disabled');
                        } else {
                            $('#team-graphics-f span').removeClass('right-tick').addClass('right-cross');
                            $('#propose-trade-submit').attr('disabled', 'disabled');
                        }
                    } else {
                        var teamSpots2Count = $('#team-spots-s').text();
                        if (teamSpots2Count > totalPlayers) {
                            $('#team-graphics-s span').removeClass('right-cross').addClass('right-tick');
                            $('#propose-trade-submit').removeAttr('disabled');
                        } else {
                            $('#team-graphics-s span').removeClass('right-tick').addClass('right-cross');
                            $('#propose-trade-submit').attr('disabled', 'disabled');
                        }
                    }
                    //Checking if players and goalies tables are empty
                    if (outerContainer.find('#rosters-players tbody').find('tr').length == 0) {
                        var colspan = outerContainer.find('#rosters-players thead').find('th').length;
                        outerContainer.find('#rosters-players tbody').empty().append('<tr><td colspan="' + colspan + '" class="no-result-present">' + trades.messages.no_player_present + '</td><tr>');
                    }
                    if (outerContainer.find('#rosters-goalies tbody').find('tr').length == 0) {
                        var colspan = outerContainer.find('#rosters-goalies thead').find('th').length;
                        outerContainer.find('#rosters-goalies tbody').empty().append('<tr><td colspan="' + colspan + '" class="no-result-present">' + trades.messages.no_goalie_present + '</td><tr>');
                    }
                    //END Append players data
                } else {
                    //empty response
                    outerContainer.empty();
                    var allPlayersOuter = outerContainer,
                            allPlayersTemplate = $('#all-players-template').html();
                    allPlayersTemplate = _.template(allPlayersTemplate);
                    var allPlayersData = {
                        playersTeam: [],
                        teamFirst: teamFirst
                    };
                    allPlayersOuter.append(allPlayersTemplate(allPlayersData));

                    var colspan = outerContainer.find('#rosters-players thead').find('th').length;
                    outerContainer.find('#rosters-players tbody').empty().append('<tr><td colspan="' + colspan + '" class="no-result-present">' + trades.messages.no_player_present + '</td><tr>');
                    var colspan = outerContainer.find('#rosters-goalies thead').find('th').length;
                    outerContainer.find('#rosters-goalies tbody').empty().append('<tr><td colspan="' + colspan + '" class="no-result-present">' + trades.messages.no_goalie_present + '</td><tr>');

                    //Adding team name
                    $('#team-name-s').text(response.pool.pool_team_first.name);
                    //Adding team count
                    $('#team-players-s').text(0);
                }

                //removing loader
                if (loaderRemove == true) {
                    $('.scorer-loader-logo').hide();
                    $('.site-container').css({'background-color': '#fff'});
                    $('#pool--content').css({'opacity': 1});
                }
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
    }
    //END Team players data func

    //Get team data with pool id
    getTeamData(poolTeamSeasonUrl, $('#all-players-outer'), true, true);
    //END Get team data with pool id

    //Get team data with pool id and team id
    getTeamData(teamIndividualFirstUrl, $('#team-players-outer'), false, false);
    //END Get team data with pool id and team id

    //Get team data with click on team select
    $(document).on('change', '.team-select-list', function () {
        var $this = $(this),
                teamId = $this.val(),
                newTeamIndividualUrl = teamIndividualUrl.replace(0, teamId);
        getTeamData(newTeamIndividualUrl, $('#team-players-outer'), false, false);
        getTeamData(poolTeamSeasonUrl, $('#all-players-outer'), true, true);
    });
    //END Get team data with click on team select

    //Drop event handler
    function dropHandle($this, teamId) {
        var teamSel = $('#team-players-' + teamId),
                teamCount = teamSel.text(),
                teamDropSel = $('#team-dropped-' + teamId),
                teamDropCount = teamDropSel.text();

        if ($this.is(':checked')) {
            teamCount = parseInt(teamCount) - 1;
            teamSel.text(teamCount);
            teamDropSel.text(parseInt(teamDropCount) + 1);
            //Adding value
            var tdParent = $this.parents('td'),
                    playerId = tdParent.attr('data-id');
            tdParent.find('[type="hidden"]').val(playerId);
            //Hide trade
            if (teamId == 'f') {
                $this.parents('tr').find('td:nth-child(1) > div').fadeOut();
            }
        } else {
            //Unchecked
            teamCount = parseInt(teamCount) + 1;
            teamSel.text(teamCount);
            teamDropSel.text(parseInt(teamDropCount) - 1);
            //Removing value
            $this.parents('td').find('[type="hidden"]').val('');
            //Show drop
            if (teamId == 'f') {
                $this.parents('tr').find('td:nth-child(1) > div').fadeIn();
            }
        }
        //Changing the accepted graphics if needed
        var teamSpotsCount = $('#team-spots-' + teamId).text();
        if (teamSpotsCount >= teamCount) {
            $('#team-graphics-' + teamId + ' span').removeClass('right-cross').addClass('right-tick');
            $('#propose-trade-submit').removeAttr('disabled');
        } else {
            $('#team-graphics-' + teamId + ' span').removeClass('right-tick').addClass('right-cross');
            $('#propose-trade-submit').attr('disabled', 'disabled');
        }
    }
    function tradeHandle($this, teamId) {
        if (teamId == 'f') {
            var teamId2 = 's';
        } else {
            var teamId2 = 'f';
        }
        var teamSel = $('#team-players-' + teamId),
                teamCount = teamSel.text(),
                team2Sel = $('#team-players-' + teamId2),
                team2Count = team2Sel.text(),
                teamTradeSel = $('#team-trade-' + teamId),
                teamTradeCount = teamTradeSel.text();

        if ($this.is(':checked')) {
            teamCount = parseInt(teamCount) - 1;
            teamSel.text(teamCount);
            team2Count = parseInt(team2Count) + 1;
            team2Sel.text(team2Count);
            teamTradeSel.text(parseInt(teamTradeCount) + 1);
            //Adding value
            var tdParent = $this.parents('td'),
                    playerId = tdParent.attr('data-id');
            tdParent.find('[type="hidden"]').val(playerId);
            //Hide drop
            if (teamId == 'f') {
                $this.parents('tr').find('td:nth-child(2) > div').fadeOut();
            }
        } else {
            //Unchecked
            teamCount = parseInt(teamCount) + 1;
            teamSel.text(teamCount);
            team2Count = parseInt(team2Count) - 1;
            team2Sel.text(team2Count);
            teamTradeSel.text(parseInt(teamTradeCount) - 1);
            //Removing value
            $this.parents('td').find('[type="hidden"]').val('');
            //Show drop
            if (teamId == 'f') {
                $this.parents('tr').find('td:nth-child(2) > div').fadeIn();
            }
        }
        //Changing the accepted graphics if needed
        var teamSpotsCount = $('#team-spots-' + teamId).text(),
                teamSpots2Count = $('#team-spots-' + teamId2).text();
        if (teamSpotsCount >= teamCount) {
            $('#team-graphics-' + teamId + ' span').removeClass('right-cross').addClass('right-tick');
            $('#propose-trade-submit').removeAttr('disabled');
        } else {
            $('#team-graphics-' + teamId + ' span').removeClass('right-tick').addClass('right-cross');
            $('#propose-trade-submit').attr('disabled', 'disabled');
        }
        if (teamSpots2Count >= team2Count) {
            $('#team-graphics-' + teamId2 + 'span').removeClass('right-cross').addClass('right-tick');
            $('#propose-trade-submit').removeAttr('disabled');
        } else {
            $('#team-graphics-' + teamId2 + 'span').removeClass('right-tick').addClass('right-cross');
            $('#propose-trade-submit').attr('disabled', 'disabled');
        }
    }
    $(document).on('change', '#all-players-outer .custom-checkbox input', function () {
        var $this = $(this);
        if ($this.hasClass('drop')) {
            dropHandle($this, 'f');
        } else {
            tradeHandle($this, 'f');
        }
    });
    $(document).on('change', '#team-players-outer .custom-checkbox input', function () {
        var $this = $(this);
        tradeHandle($this, 's');
    });
    //END Drop event handler
    //Propose trade submit 
    $('#propose-trade-submit').on('click', function (e) {
        if($(this).attr('disabled')) {
           e.preventDefault();
           return;
        }
        var $this = $('#create-trade-form');
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (response) {
                var newProposeTradeUrl = proposeTradeUrl.replace('tradeid', response.data.id);
                location.href = newProposeTradeUrl;
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
        e.preventDefault();
    });
    //END Propose trade submit 
}); 