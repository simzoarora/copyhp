$(document).ready(function () {
    $('.select-field').selectBoxIt();
    //Taking page to top if not
    $('body').animate({scrollTop: 0});
    //getting data
    $.ajax({
        url: poolOverviewUrl,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            if (!$.isEmptyObject(response)) {
                //checking which pool type/cat is this
                if (response.pool_type == poolTypes.h2h) {
                    if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                        var poolToShow = 11;
                    } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly) {
                        var poolToShow = 12;
                    } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly_daily) {
                        var poolToShow = 13;
                    }
                } else if (response.pool_type == poolTypes.box) {
                    var poolToShow = 2;
                } else if (response.pool_type == poolTypes.standard) {
                    if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_standard) {
                        var poolToShow = 31;
                    } else if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                        var poolToShow = 32;
                    }
                }
                //END checking which pool type/cat is this

                //Calculating current team
                if (!$.isEmptyObject(response.pool_team)) {
                    var currentTeamId = response.pool_team.id;
                }
                //END Calculating current team

                //Append h2h data 
                $('#pools-h2h-outer').empty();
                if (!$.isEmptyObject(response.pool_matchups)) {
                    if (poolToShow == 11) {
                        $.each(response.pool_matchups, function (i, v) {
                            var poolsH2hOuter = $('#pools-h2h-outer'),
                                    poolsH2hTemplate = $('#pools-h2h-template').html(),
                                    poolMatchupLink = $('#pool-matchup-link').attr('href'),
                                    newPoolMatchupLink = poolMatchupLink.replace(0, v.id),
                                    myTeamTeamLink = $('#myteam-team-link').attr('href'),
                                    newMyTeamTeamLink = myTeamTeamLink.replace('pool_id', response.id).replace('team_id', v.pool_team1.id),
                                    newMyTeamTeamLink2 = myTeamTeamLink.replace('pool_id', response.id).replace('team_id', v.pool_team2.id);

                            poolsH2hTemplate = _.template(poolsH2hTemplate);
                            var poolsH2hData = {
                                poolMatchups: v,
                                poolScoreSettings: response.pool_score_settings,
                                siteLink: api_url,
                                poolMatchupLink: newPoolMatchupLink,
                                myTeamTeamLink: newMyTeamTeamLink,
                                myTeamTeamLink2: newMyTeamTeamLink2
                            };
                            poolsH2hOuter.append(poolsH2hTemplate(poolsH2hData));
                        });

                        hpApp.h2hStatsSetting();
                        //bottom data colors
                        $.each(response.pool_matchups, function (i, v) {
                            if (v.team1 == currentTeamId || v.team2 == currentTeamId) {
                                $.each(v.cat_result, function (index, value) {
                                    if (value.tie == true) {
                                        $('.score-setting-' + v.id + '-' + index).addClass('blue');//means grey color
                                    } else {
                                        if (value.win == currentTeamId) {
                                            $('.score-setting-' + v.id + '-' + index).addClass('green');
                                        } else {
                                            $('.score-setting-' + v.id + '-' + index).addClass('red');
                                        }
                                    }
                                });
                            }
                        });
                    } else if (poolToShow == 12 || poolToShow == 13) {
                        $.each(response.pool_matchups, function (i, v) {
                            var poolsH2hOuter = $('#pools-h2h-outer'),
                                    poolsH2hTemplate = $('#pools-h2h-template').html(),
                                    poolMatchupLink = $('#pool-matchup-link').attr('href'),
                                    newPoolMatchupLink = poolMatchupLink.replace(0, v.id),
                                    myTeamTeamLink = $('#myteam-team-link').attr('href'),
                                    newMyTeamTeamLink = myTeamTeamLink.replace('pool_id', response.id).replace('team_id', v.pool_team1.id),
                                    newMyTeamTeamLink2 = myTeamTeamLink.replace('pool_id', response.id).replace('team_id', v.pool_team2.id);

                            poolsH2hTemplate = _.template(poolsH2hTemplate);
                            var poolsH2hData = {
                                poolMatchups: v,
                                siteLink: api_url,
                                poolMatchupLink: newPoolMatchupLink,
                                myTeamTeamLink: newMyTeamTeamLink,
                                myTeamTeamLink2: newMyTeamTeamLink2
                            };
                            poolsH2hOuter.append(poolsH2hTemplate(poolsH2hData));
                        });
                    }

                } else {
                    //if the pool h2h data is not coming
                    if (poolToShow == 11 || poolToShow == 12 || poolToShow == 13) {
                        $('#pools-h2h-outer').append('<p class="no-result-present">' + pool_dashboard.messages.no_pool_present + '</p>');
                    }
                }
                //END Append h2h data
                //Append each team data
                if (poolToShow == 11 || poolToShow == 12 || poolToShow == 13) {
                    var standingH2hTemplate = $('#standing-h2h-template').html();
                    standingH2hTemplate = _.template(standingH2hTemplate);
                    var standingH2hData = {
                        poolTeams: response.pool_teams
                    };
                    $('#standing-h2h-outer').append(standingH2hTemplate(standingH2hData));
                } else {
                    var standingTemplate = $('#standing-box-standard-template').html();
                    standingTemplate = _.template(standingTemplate);
                    var standingData = {
                        poolTeams: response.pool_teams,
                        poolScoreSettings: response.pool_score_settings
                    };
                    $('#standing-box-standard-outer').append(standingTemplate(standingData));
                    $.each(response.pool_teams, function (i, v) {
                        if (typeof v.pool_standard_scores != 'undefined') {
                            var standardScores = v.pool_standard_scores;
                        } else {
                            var standardScores = v.pool_standard_rotto_results;
                        }
                        $.each(standardScores, function (index, value) {
                            $('#standing-' + v.id + '-' + value.pool_score_setting.pool_scoring_field_id).text(hpApp.twoDecimal(value.original_stat));
                        });
                    });
                }

                //Trial period
                var active_till_moment = moment(response.active_till),
                        active_till_moment_proper = active_till_moment.format('YYYY-MM-DD HH:mm');
                var activeTillMOld = moment.tz(active_till_moment_proper, gameTimezone),
                        activeTillM = activeTillMOld.clone().tz(moment.tz.guess());
                if (response.is_active == 0 && moment() > activeTillM) {
                    var trialPeriodOuter = $('#trial-period-outer'),
                            trialPeriodTemplate = $('#trial-period-template').html();
                    trialPeriodTemplate = _.template(trialPeriodTemplate);
                    var trialPeriodData = {
                        userId: response.user_id
                    };
                    trialPeriodOuter.append(trialPeriodTemplate(trialPeriodData));
                }
            } else {
                //no result
                $('.pool--content').empty().append('<p class="no-result-present">' + pool_dashboard.messages.invalid_pool + '</p>');
            }

            $('.scorer-loader-logo').hide();
            $('.site-container').css({'background-color': '#fff'});
            $('#pool--content').css({'opacity': 1}).fadeIn('500');
        },
        error: function (error) {
            hpApp.ajaxSwalError(error);
        }
    });
    //END getting data
    //Getting Transaction data
    $.ajax({
        url: transactionsUrl,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            if (!$.isEmptyObject(response)) {
                var allTransactionOuter = $('#all-transaction-outer'),
                        allTransactionTemplate = $('#all-transaction-template').html();
                allTransactionOuter.empty();
                allTransactionTemplate = _.template(allTransactionTemplate);
                var allTransactionData = {
                    trans: response,
                    transactionsTypes: transactionsTypes
                };
                allTransactionOuter.append(allTransactionTemplate(allTransactionData));
            } else {
                //No result
                $('#all-transaction-outer').append('<tr class="no-result-present"><td colspan="4">' + pool_dashboard.messages.no_league_feed_present + '</td></tr>');
            }
        },
        error: function (error) {
            $('#all-transaction-outer').append('<tr class="no-result-present"><td colspan="4">' + pool_dashboard.messages.no_league_feed_present + '</td></tr>');
        }
    });
    //END Getting Transaction data
});