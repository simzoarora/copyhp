var poolToShowCat;
$(document).ready(function () {
    //taking page to top if not
    $('body').animate({scrollTop: 0});
    //DatePicker
    $('#team-datepicker').datetimepicker({
        format: 'MM/DD/YYYY'
    });
    $('#team-datepicker').on('dp.change', function (e) {
        lineupDate = moment(new Date($(this).val())).format('YYYY-MM-DD');
        var $this = $(this),
                startDate = $this.parent().attr('data-start_date'),
                endDate = $this.parent().attr('data-end_date');
        $this.parent().addClass('active').siblings().removeClass('active');
        getTeamData(myTeamUrl, startDate, endDate, lineupDate, false);
    });
    setTimeout(function () {
        $('#team-datepicker').val(moment().format('MM/DD/YYYY'));
    }, 200);
    //END 
    //goToPrevRoster for appending empty player in a defined sequence
    function goToPrevRoster(response, index, emptyPlayersTemplate, emptyPlayersData) {
        if (index > 0) {
            var presentArr = response.pool_rosters[index - 1];
            if (presentArr['value'] > 0) {
                $('[data-send-pos="' + presentArr['player_position_id'] + '"]').last().after(emptyPlayersTemplate(emptyPlayersData));
            } else {
                goToPrevRoster(response, index, emptyPlayersTemplate, emptyPlayersData)
            }
        } else {
            $('#rosters-players tbody .sub-header').after(emptyPlayersTemplate(emptyPlayersData));
        }
    }
    //END
    //getting data
    function getTeamData(myTeamUrl, startDate, endDate, lineupDate, pageLoader) {
        if (pageLoader == false) {
            $('#all-players-outer table').remove();
            $('#circle-loader').show();
        }
        $.ajax({
            url: myTeamUrl,
            type: 'GET',
            data: {
                start_date: startDate,
                end_date: endDate,
                lineup_date: lineupDate
            },
            dataType: 'json',
            success: function (response) {
                if (!$.isEmptyObject(response)) {
                    //Initialize poolScoreSettings var
                    poolScoreSettings = response.pool_score_settings;

                    //checking which pool type is this
                    if (response.pool_type == poolTypes.h2h) {
                        var poolToShow = 1;
                    } else if (response.pool_type == poolTypes.box) {
                        var poolToShow = 2;
                    } else if (response.pool_type == poolTypes.standard) {
                        var poolToShow = 3;
                    }
                    //END checking which pool type is this
                    //checking which pool cat is this
                    if (!$.isEmptyObject(response.pool_setting)) {
                        if (response.pool_type == poolTypes.h2h) {
                            if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_cat) {
                                poolToShowCat = 11;
                            } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly) {
                                poolToShowCat = 12;
                            } else if (response.pool_setting.poolsetting.pool_format == poolFormat.h2h_weekly_daily) {
                                poolToShowCat = 13;
                            }
                        } else if (response.pool_type == poolTypes.box) {
                            poolToShowCat = 2;
                        } else if (response.pool_type == poolTypes.standard) {
                            if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_standard) {
                                poolToShowCat = 31;
                            } else if (response.pool_setting.poolsetting.pool_format == poolFormat.standard_rotisserie) {
                                poolToShowCat = 32;
                            }
                        }
                    }
                    //END checking which pool cat is this

                    //Append players data
                    if (otherTeam) {
                        var responseTeamPlayers = response.pool_team_first.pool_team_players,
                                responsePoolTeam = response.pool_team_first;
                    } else {
                        var responseTeamPlayers = response.pool_team.pool_team_players,
                                responsePoolTeam = response.pool_team;
                    }

                    if (!$.isEmptyObject(responseTeamPlayers)) {
                        var allPlayersOuter = $('#all-players-outer'),
                                allPlayersTemplate = $('#all-players-template').html();
                        allPlayersTemplate = _.template(allPlayersTemplate);
                        var allPlayersData = {
                            poolScoreSettings: response.pool_score_settings,
                            siteLink: api_url,
                            playersTeam: responseTeamPlayers,
                            poolTeam: responsePoolTeam,
                            poolScoringFields: poolScoringFields
                        };
                        allPlayersOuter.append(allPlayersTemplate(allPlayersData));
                        //making array for starting lineup totals
                        var lineupPlayer = [],
                                lineupGoalie = [];
                        $.each(response.pool_score_settings, function (index, value) {
                            if (value.type == poolScoringFields.inverse_type.player) {
                                lineupPlayer[value.pool_scoring_field_id] = [];
                            }
                        });
                        lineupPlayer['total-points'] = [];

                        $.each(response.pool_score_settings, function (index, value) {
                            if (value.type == poolScoringFields.inverse_type.goalie) {
                                lineupGoalie[value.pool_scoring_field_id] = [];
                            }
                        });
                        lineupGoalie['total-points'] = [];

                        $.each(responseTeamPlayers, function (i, v) {
                            var totalPoints = 0;
                            if (poolToShow == 1) {
                                var pool_scores = v.pool_h2h_scores;
                            } else if (poolToShow == 2 || poolToShow == 3) {
                                var pool_scores = v.pool_standard_scores;
                            }
                            if (v.player_season.player_position.name != poolScoringFields.type[2]) {
                                $.each(pool_scores, function (index, value) {
                                    if (value.pool_score_setting.type == poolScoringFields.inverse_type.player) {
                                        $('#con-' + v.id + '-' + value.pool_score_setting.pool_scoring_field_id).html(hpApp.twoDecimal(value.total_stat));
                                        totalPoints = hpApp.twoDecimal(totalPoints) + hpApp.twoDecimal(value.total_value);
                                        //the lineup totals not contains bench and injury
                                        if (v.player_season.player_position.id != benchId || v.player_season.player_position.id != inguryReserveId) {
                                            lineupPlayer[value.pool_score_setting.pool_scoring_field_id].push(hpApp.twoDecimal(value.total_stat));
                                        }
                                    }
                                });
                                $('#con-' + v.id + '-total_points').html(totalPoints);
                                lineupPlayer['total-points'].push(totalPoints);
                            } else {
                                $.each(pool_scores, function (index, value) {
                                    if (value.pool_score_setting.type == poolScoringFields.inverse_type.goalie) {
                                        $('#con-' + v.id + '-' + value.pool_score_setting.pool_scoring_field_id).html(hpApp.twoDecimal(value.total_stat));
                                        totalPoints = hpApp.twoDecimal(totalPoints) + hpApp.twoDecimal(value.total_value);
                                        //the lineup totals not contains bench and injury
                                        if (v.player_season.player_position.id != benchId || v.player_season.player_position.id != inguryReserveId) {
                                            lineupGoalie[value.pool_score_setting.pool_scoring_field_id].push(hpApp.twoDecimal(value.total_stat));
                                        }
                                    }
                                });
                                $('#con-' + v.id + '-total_points').html(totalPoints);
                                lineupGoalie['total-points'].push(totalPoints);
                            }
                        });
                        //lineup starting data
                        function getSum(a, b) {
                            if (typeof a == 'undefined') {
                                a = 0;
                            }
                            if (typeof b == 'undefined') {
                                b = 0;
                            }
                            return a + b;
                        }
                        $.each(response.pool_score_settings, function (index, value) {
                            if (value.type == poolScoringFields.inverse_type.player) {
                                var totalSum = lineupPlayer[value.pool_scoring_field_id].reduce(getSum, 0);
                                $('#lineup-' + responsePoolTeam.id + '-' + value.pool_scoring_field_id).html(hpApp.twoDecimal(totalSum));
                            }
                        });
                        var totalPoints = lineupPlayer['total-points'].reduce(getSum, 0);
                        $('#lineup-' + responsePoolTeam.id + '-total_points').html(totalPoints);
                        //lineup starting goalie data
                        $.each(response.pool_score_settings, function (index, value) {
                            if (value.type == poolScoringFields.inverse_type.goalie) {
                                var totalSum = lineupGoalie[value.pool_scoring_field_id].reduce(getSum, 0);
                                $('#lineup-' + responsePoolTeam.id + '-g-' + value.pool_scoring_field_id).html(hpApp.twoDecimal(totalSum));
                            }
                        });
                        var totalPoints = lineupGoalie['total-points'].reduce(getSum, 0);
                        $('#lineup-' + responsePoolTeam.id + '-g-total_points').html(totalPoints);

                        //Adding total player positions
                        $.each(response.pool_rosters, function (index, v) {
                            //No bench position need to be added
                            if (v.player_position_id != benchId) {
                                var alreadyAppended = $('[data-send-pos="' + v.player_position_id + '"]').length;
                                if (alreadyAppended <= v.value) {
                                    var neededToappend = parseInt(v.value) - parseInt(alreadyAppended);

                                    for (var i = 0; i < neededToappend; i++) {
                                        var emptyPlayersTemplate = $('#empty-players-template').html();
                                        emptyPlayersTemplate = _.template(emptyPlayersTemplate);
                                        var emptyGoaliesTemplate = $('#empty-goalies-template').html();
                                        emptyGoaliesTemplate = _.template(emptyGoaliesTemplate);
                                        var emptyPlayersData = {
                                            poolScoreSettings: response.pool_score_settings,
                                            positionName: v.player_position.short_name,
                                            positionId: v.player_position_id,
                                            neededClass: ''
                                        };
                                        if (alreadyAppended != 0) {
                                            if (v.player_position.name != poolScoringFields.type[2]) {
                                                $('[data-send-pos="' + v.player_position_id + '"]').last().after(emptyPlayersTemplate(emptyPlayersData));
                                            } else {
                                                $('[data-send-pos="' + v.player_position_id + '"]').last().after(emptyGoaliesTemplate(emptyPlayersData));
                                            }
                                        } else {
                                            if (v.player_position.name != poolScoringFields.type[2]) {
                                                goToPrevRoster(response, index, emptyPlayersTemplate, emptyPlayersData);
                                            } else {
                                                $('#rosters-goalies tbody .bench').first().before(emptyGoaliesTemplate(emptyPlayersData));
                                            }
                                        }
                                    }
                                }
                            }
                        });

                        //Removing pool points if pool is h2h and roto
                        if (poolToShowCat == 11 || poolToShowCat == 32) {
                            $('.total-pool-points').remove();
                        }

                        //Checking if players and goalies tables are empty
                        setTimeout(function () {
                            if ($('#all-players-outer #rosters-players tbody').find('tr:not(.sub-header,.total_row)').length == 0) {
                                var colspan = $('#all-players-outer #rosters-players .sub-header').find('td').length;
                                $('#all-players-outer #rosters-players .total_row').before('<tr><td colspan="' + colspan + '" class="no-result-present">' + add_setup.messages.no_player_present + '</td><tr>');
                            }
                            if ($('#all-players-outer #rosters-goalies tbody').find('tr:not(.sub-header,.total_row)').length == 0) {
                                var colspan = $('#all-players-outer #rosters-goalies .sub-header').find('td').length;
                                $('#all-players-outer #rosters-goalies .total_row').before('<tr><td colspan="' + colspan + '" class="no-result-present">' + add_setup.messages.no_goalie_present + '</td><tr>');
                            }
                        }, 100);

                        //chnage pool name header team select value
                        setTimeout(function () {
                            if (otherTeam) {
                                var select = $(".team-select");
                                select.find("option[value=" + otherTeamId + "]").attr('selected', 'selected');
                                select.data("selectBox-selectBoxIt").refresh();
                            }
                        }, 100);
                    } else {
                        //empty div
                        $('#all-players-outer').empty().append('<p class="no-result-present">No team players present.</p>');
                    }
                    //END Append players data
                } else {
                    //empty div
                    $('.pool--content').empty().append('<p class="no-result-present">Invalid pool.</p>');
                }

                //removing loader
                if (pageLoader == true) {
                    setTimeout(function () {
                        $('.scorer-loader-logo').hide();
                        $('#circle-loader').hide();

                        $('#league-nav ul li:last-child').addClass('active');
                        $('.site-container').css({'background-color': '#fff'});
                        $('#pool--content').css({'opacity': 1});
                    }, 300);
                } else {
                    $('#circle-loader').hide();
                }
            },
            error: function (error) {
                hpApp.ajaxSwalError(error);
            }
        });
    }
    //END getting data
    getTeamData(myTeamUrl, startDate, endDate, lineupDate, true);

    $('#league-nav li:not(.team-datepicker)').on('click', function () {
        var $this = $(this),
                startDate = $this.attr('data-start_date'),
                endDate = $this.attr('data-end_date');
        $this.addClass('active').siblings().removeClass('active');
        getTeamData(myTeamUrl, startDate, endDate, lineupDate, false);
    });

    //Exchange player
    $(document).on('click', '#rosters-players tbody tr,#rosters-goalies tbody tr', function () {
        if (otherTeam) {
            //other team show no player exchange on this
            if (teamId == otherTeamId) {

            } else {
                return;
            }
        }
        var $this = $(this),
                positionId = $this.attr('data-pos');
        //changing positionId if player is on bench
        if ($this.hasClass('bench')) {
            positionId = $this.attr('data-bench-check');
        }
        //remove higlights if other tables 
        $this.parent().parent().siblings('table').removeClass('gray-table').find('tr').removeClass('selected').removeClass('gray-row');

        if ($this.hasClass('sub-header')) {
            //Do nothing
        } else if ($this.hasClass('match-start')) {
            //Do nothing
        } else if ($this.hasClass('empty') && $this.hasClass('gray-row') == false) {
            //Do nothing
        } else if ($this.hasClass('total_row')) {
            //Do nothing
        } else if ($this.find('td').hasClass('no-result-present')) {
            //Do nothing
        } else if ($this.hasClass('selected')) {
            $this.removeClass('selected').removeClass('gray-row');
            $this.siblings().removeClass('gray-row');
        } else if ($this.hasClass('gray-row') == false) {
            //remove highlighting form siblings rows
            $this.siblings().removeClass('selected');
            $this.siblings().removeClass('gray-row');
            //highlighting table 
            $this.parents('.table').addClass('gray-table');
            //highlighting row
            $this.addClass('selected');
            $this.addClass('gray-row');
            //highlighting similar rows of same position
            $('.pos-' + positionId).each(function (i, v) {
                var $thisV = $(this);
                if ($thisV.hasClass('bench') == false) {
                    $thisV.addClass('gray-row');
                }
            });
            //highlighting different positions
            $.each(playerPositionGrouping[positionId], function (i, positionValue) {
                $('.pos-' + positionValue).each(function (index, value) {
                    var $thisV = $(this);
//                    if ($thisV.hasClass('bench') == false) {//This is commented by the order of Craig Vanderlinden :)
                        $thisV.addClass('gray-row');
//                    }
                });
            });
            //adding new bench row--only if present row is not empty and bench
            //first remove extra empty and bench if created
            $this.parent().find('.empty.bench').remove();
            if ($this.hasClass('bench') == false && $this.hasClass('empty') == false) {
                var emptyPlayersTemplate = $('#empty-players-template').html();
                emptyPlayersTemplate = _.template(emptyPlayersTemplate);
                var emptyPlayersData = {
                    poolScoreSettings: poolScoreSettings,
                    positionName: 'BN',
                    positionId: benchId,
                    neededClass: 'bench gray-row'
                };
                $this.siblings('.total_row').before(emptyPlayersTemplate(emptyPlayersData));

                //Removing pool points if pool is h2h and roto
                if (poolToShowCat == 11 || poolToShowCat == 32) {
                    $('.total-pool-points').remove();
                }
            }
        } else if ($this.hasClass('gray-row') && $this.hasClass('selected') == false) {
            var swapPlayer = $this.find('td:nth-child(n+2)'),
                    selectedPlayerSel = $('.gray-row.selected'),
                    selectedPlayer = selectedPlayerSel.find('td:nth-child(n+2)');

            //making player array
            if (typeof $this.attr('data-player-id') != 'undefined' && typeof selectedPlayerSel.attr('data-player-id') != 'undefined') {
                var playerExchangeData = [
                    {
                        pool_team_player_id: $this.attr('data-player-id'),
                        position: selectedPlayerSel.attr('data-pos')
                    },
                    {
                        pool_team_player_id: selectedPlayerSel.attr('data-player-id'),
                        position: $this.attr('data-pos')
                    }
                ];
            } else if (typeof $this.attr('data-player-id') != 'undefined') {
                var playerExchangeData = [
                    {
                        pool_team_player_id: $this.attr('data-player-id'),
                        position: selectedPlayerSel.attr('data-pos')
                    }
                ];
            } else if (typeof selectedPlayerSel.attr('data-player-id') != 'undefined') {
                var playerExchangeData = [
                    {
                        pool_team_player_id: selectedPlayerSel.attr('data-player-id'),
                        position: $this.attr('data-pos')
                    }
                ];
            }

            $.ajax({
                url: changeLineupUrl,
                type: 'POST',
                data: {
                    players: playerExchangeData,
                    lineup_date: lineupDate
                },
                dataType: 'json',
                success: function (response) {
                    if (!$.isEmptyObject(response)) {
                        //exchange data
                        selectedPlayerSel.append(swapPlayer);
                        $this.append(selectedPlayer);
                        //remove classes
                        $this.parents('.table').removeClass('gray-table');
                        $this.siblings().removeClass('gray-row');
                        $this.siblings().removeClass('selected');
                        $this.removeClass('gray-row').removeClass('empty');
                        $this.siblings('tr.bench.empty').remove();
                        //changing player ids
                        selectedPlayerSel.attr('data-player-id', $this.attr('data-player-id'));
                        $this.attr('data-player-id', selectedPlayerSel.attr('data-player-id'));
                    }
                },
                error: function (error) {
                    hpApp.ajaxSwalError(error);
                }
            });
        }
    });
    //END Exchange player
});