<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Navs Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in menu items throughout the system.
      | Regardless where it is placed, a menu item can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'backend' => [
        'button' => 'Button',
    ],
    'general' => [
        'logout' => 'Logout',
    ],
    'frontend' => [
        'dashboard' => 'Dashboard',
        'home' => 'Home',
        'login' => 'Login',
        'macros' => 'Macros',
        'register' => 'Register',
        'user' => [
            'administration' => 'Administration',
            'change_password' => 'Change Password',
            'my_information' => 'My Information',
        ],
        'livescoreboard' => [
            'yesterday' => 'Yesterday',
            'today' => 'Today',
            'tomorrow' => 'Tomorrow',
            'date' => 'May 16',
            'final' => 'Final',
        ],
        'home_nav' => [
            'my_pools' => 'My Pools',
            'nhl' => 'NHL',
            'stats' => 'Stats',
            'news' => 'News',
            'draft' => 'Draft',
            'tools' => 'Tools',
            'draft_kit' => 'Draft Kit',
            'custom_draft_kit' => 'Custom Draft Kit',
            'login' => 'Login',
            'login_button' => 'LOGIN',
            'email_address' => 'Email address...',
            'password' => 'Password...',
            'use_social_sign' => 'Or use your social sign in',
            'register' => 'Register',
            'register_button' => 'REGISTER',
            'my_account' => 'My Account',
            'edit_account' => 'Edit Account',
            'logout' => 'Logout',
            'messages' => 'Messages',
            'blog' => 'Blog',
            'notif' => 'Notif',
            'notifications' => 'Notifications',
            
            'submenus' => [
                'nhl' => [
                    'scores' => 'Scores',
                    'standings' => 'Standings',
                    'schedule' => 'Schedule',
                    'player_stats' => 'Player Stats',
                ],
            ],
        ],
        'footer' => [
            'how_it_works' => 'How it Works',
            'faq' => 'FAQ',
            'support' => 'Support',
            'contact' => 'Contact',
            'advertise' => 'Advertise',
            'terms_of_use' => 'Terms of Use',
            'privacy_policy' => 'Privacy Policy',
            'copyright' => 'All Rights Reserved',
        ],
        'upper_footer' => [
            'live_scores_stats' => 'LIVE ONLINE DRAFT',
            'live_scores_stats_p' => 'Not able to get together with your friends in person? Connect online from anywhere and do a fantasy draft in real-time.',
            'trash_talks' => 'TRASH TALK',
            'trash_talks_p' => 'Chirp like the pros with our brand new message boards. Live up your latest wins and defend your losses.',
            'manage_team' => 'MANAGE YOUR TEAM',
            'manage_team_p' => 'Set your lines and tinker with your roster. Or set it to auto and let us take care of you.',
        ],
    ],
];