<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Nhl Pages Language Lines : English
    |--------------------------------------------------------------------------
    | The following language lines are used in pools throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    */
    
    /*
    |--------------------------------------------------------------------------
    | Route : /standings/{season_name?}
    |--------------------------------------------------------------------------
    */
    'standings' => [
        'title' => 'Standings',
        'division' => 'Division',
        'conference' => 'Conference',
        'overall' => 'Overall',
        'gp' => 'GP',
        'w' => 'W',
        'l' => 'L',
        'otl' => 'OTL',
        'sol' => 'SOL',
        'pts' => 'PTS',
        'gf' => 'GF',
        'ga' => 'GA',
        'diff' => 'DIFF',
        'home' => 'HOME',
        'away' => 'AWAY',
        'l10' => 'L10',
        'strk' => 'STRK',
        'messages' => [
            'no_results_found_for_selected_year' => 'No results found for the selected year.',
            'internal_server_error' => 'Internal Server Error.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : /schedule/{schedule_start_date?}
    |--------------------------------------------------------------------------
    */
    'schedule' => [
        'title' => 'Schedule',
        'matchup' => 'Matchup',
        'result' => 'Result',
        'time' => 'Time',
        'date' => 'Date',
        'ot' => '(OT)',
        'so' => '(SO)',
        'messages' => [
            'no_schedule_present' => 'No schedule present.',
            'invalid_date_provided' => 'Invalid Date Provided',
        ],
    ],
    'scores' => [
        'title' => 'Scores',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : /player-stats?season_id={season_id}&game_type={regular}&position={position}&page={page_no}
    |--------------------------------------------------------------------------
    */
    'player_stats' => [
        'title' => 'Statistics',
        'view_by' => 'View Statistics By:',
        'players_position' => 'Player\'s Position',
        'seasons' => 'Seasons',
        'game_type' => 'Game Type',
        'team' => 'Team',
        'player' => 'Player',
        'pos' => 'Pos',
        'gp' => 'GP',
        'gs' => 'GS',
        'w' => 'W',
        'l' => 'L',
        't' => 'T',
        'otl' => 'OTL',
        'sol' => 'SOL',
        'sa' => 'SA',
        'svs' => 'SVS',
        'ga' => 'GA',
        'sv_cent' => 'SV%',
        'gaa' => 'GAA',
        'toi' => 'TOI',
        'so' => 'SO',
        'g' => 'G',
        'a' => 'A',
        'p' => 'P',
        'pim' => 'PIM',
        'p_gp' => 'P/GP',
        'ppg' => 'PPG',
        'ppp' => 'PPP',
        'shg' => 'SHG',
        'shp' => 'SHP',
        'gwg' => 'GWG',
        'otg' => 'OTG',
        's' => 'S',
        's_cent' => 'S%',
        'toi_gp' => 'TOI/GP',
        'shifts_gp' => 'Shifts/GP',
        'fow_cent' => 'FOW%',
        'svs2' => 'SVS',
        'sv_cent2' => 'SV%',
        'messages' => [
            'no_players_found' => 'No players found for selected season.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : /injury-report   {{trans('nhl.injury_report.')}}   injury_report.messages.
    |--------------------------------------------------------------------------
    */
    'injury_report' => [
        'nhl_injury_report' => 'NHL Injury Report',
        'last_updated' => 'Last updated:',
        'first_report' => 'First report:',
        'messages' => [
            'no_results_found' => 'No results found.',
            'no_injury_report_present' => 'No injury report present.',
        ],
    ],
];    