<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'activate' => 'Activate',
                'change_password' => 'Change Password',
                'deactivate' => 'Deactivate',
                'delete_permanently' => 'Delete Permanently',
                'resend_email' => 'Resend Confirmation E-mail',
                'restore_user' => 'Restore User',
            ],
        ],
    ],

    'general' => [ 
        'cancel' => 'Cancel',

        'crud' => [
            'create' => 'Create',
            'delete' => 'Delete',
            'edit' => 'Edit',
            'update' => 'Update',
        ],

        'save' => 'Save',
        'view' => 'View',
        'submit' => 'Submit',
        'submitting' => 'Submitting',
        'send' =>'Send',
    ],
    
    'home' => [
        'join_a_pool' => 'Join a pool',
        'create_pool' => 'Create a Pool',
        'createpool' => 'Create Pool',
        'create' => 'Create',
        'join_pool' => 'Join Pool',        
    ],
];
