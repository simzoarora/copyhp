<?php

return [

    /*
      |--------------------------------------------------------------------------
      | HTTP Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in the views/errors files.
      |
     */

    '404' => [
        'title' => "Sorry, the page you are looking for doesn't exist.",
        'description' => 'It looks like this page no longer exists.',
    ],
    '503' => [
        'title' => 'We are currently upgrading HockeyPool.com.  Please try again momentarily.',
        'description' => 'Be right back.',
    ],
];
