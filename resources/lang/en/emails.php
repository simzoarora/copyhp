<?php

return [
    'subjects' => [
        'pool_team_invite' => 'You have been invited to join pool on HockeyPool.com',
        'pool_team_add' => 'You have been added to pool on HockeyPool.com',
        'draft_kit' => 'Thank You for purchasing Draft Kit.',
    ],
    'pool_added' => [
        'text1' => 'You have been invited to join ',
        'text2' => ' on',
        'join_now' => 'Join Now',
        'link_text' => 'or click/copy paste the link below:',
        'register_now' => 'Register Now',
        'pool_invitation' => 'Pool Invitation',
    ],
    'confirm' => [
        'title' => 'Verify Your Email',
        'thank_you' => 'Thank you for creating your account on ',
        'text1' => '- Please verify your email to activate your account.',
        'verify_email_now' => 'Verify Email Now',
        'text2' => 'or click/copy paste the link below:',
    ],
    'passwords' => [
        'title' => 'Reset Your Password',
        'text1' => 'Please reset your password to access your account.',
        'reset_password_now' => 'Reset Password Now',
        'text2' => 'or click/copy paste the link below:',
    ],
    'draft_kit'=>[
        'thank_you'=>'Thank You for purchasing Draft Kit. Please find it in attachment.',
        'header'=>'Draft Kit'
    ]
];

