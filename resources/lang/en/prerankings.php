<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pre Ranking Pages Language Lines : English
    |--------------------------------------------------------------------------
    | The following language lines are used in pools throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    */
    'messages' => [ 
        'success' => [  // trans('prerankings.messages.success.')
            'file_downloaded' => 'Your file is being downloaded.',
            'data_inserted' => 'Data entered successfully.',
            'data_updated' => 'Data updated successfully.',
        ],
        'danger' => [  // trans('prerankings.messages.danger.')
            'no_players' => 'No players for this season.',
            'invalid_season' => 'Invalid season ID. Please try again.',
            'unsupported_file_format' => 'This file format is not supported. Allowed formats are .xls, .csv, .xlsx',
            'wrong_season' => 'The season ID chosen does not match with the import file. Please review your file.',
            'faulty_data' => 'The file contains faulty data. Please review your file.',
            'wrong_player_total' => 'The number of players in the import file are not consistent with the database. Please review your file.',
            'wrong_player_ids' => 'The player ids in the import file are not consistent with the database.',
            'file_not_present' => 'Please choose a file for import.',
        ],
        'choose_season' => 'Please choose a season!',
    ]
];    