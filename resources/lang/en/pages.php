<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Pages Language Lines : English
      |--------------------------------------------------------------------------
      | The following language lines are used in home throughout the system.
      | Regardless where it is placed, a label can be listed here so it is easily
      | found in a intuitive way.
     */

    /*
      |--------------------------------------------------------------------------
      | Route : poolTypes
      |--------------------------------------------------------------------------
     */
    'poolTypes' => [
        'title' => 'Hockey Pool Formats',
        'content' => 'HockeyPool.com offer three main types of pools, and different variations within those pools. 
                They way we do pools may be different from other sites so be sure to read through the 
                explanations to find the right pool for you and your group.',
        'head_to_head' => [
            'title' => 'Head To Head',
            'content' => 'Head to head pools allow you to compete against other teams in the pool one on one in weekly matchups.
                    You will also have the option to add playoffs to your pools which means at the end of the season the
                    rank the teams have achieved through the regular season will determine the placement for the single
                    elimination playoff matchups.',
            'categories' => [
                'title' => 'Head To Head - Total Points',
                'content' => 'Head to head categories is the most popular weekly match up pool type.  The league is setup
                            with different scoring and/or goaltending categories. Each week all teams teams compete in 
                            one on one matchups.Trying to get the best score in each category, every category you win 
                            adds to your total record.  Teams are ranked based.',
            ],
            'weekly_win' => [
                'title' => 'Head To Head - Total Points with Daily Rosters',
                'content' => 'Head to head weekly win is the style most similar to Fantasy Football.  Instead of just 
                            competing in scoring categories, the categories are assigned a point value.  Each player 
                            on your active roster over the week will add points to your weekly total.  At the end of
                            the week the team with the highest point total in the matchup gets the win.',
            ],
            'weekly_win_daily_rosters' => [
                'title' => 'Head To Head - Category Wins (Rotisserie)',
                'content' => 'A variation of weekly win for the fantasy player that really wants to get involved.
                            In regular weekly win, you choose your roster and set it for the week.  With daily
                            rosters you can change your roster daily.  It takes a lot of team management to win.
                            Put in the effort, manage your team every step of the way.',
            ],
        ],
        'standard' => [
            'title' => 'Standard',
            'content' => 'Standard pools are the original variations of fantasy hockey.  Compete against 
                    everyone in the league.  We have two different variations of the pool with
                    our Full Standard and Roto leagues.',
            'standard' => [
                'title' => 'Standard - Total Points',
                'content' => 'Choose the scoring categories and assign their point values.  Keep it
                            simple with goals and assists or add in some more advanced stats to
                            create something unique.  At the end of the season the team with highest
                            point total wins.',
            ],
            'rotisserie' => [
                'title' => 'Standard - Category Wins (Rotisserie)',
                'content' => 'Over the regular season you are competing to be the best in each category. 
                            You will need a well rounded team to place well, having just scoring won\'t 
                            win it for you.  This is a popular style because it changes the value of a 
                            lot of players, sometimes hits and penalty minutes can be just as important
                            as goals and assists.',
            ],
        ],
        'box' => [
            'title' => 'Box',
            'content' => 'Standard pools are the original variations of fantasy hockey.  Compete against 
                    everyone in the league.  We have two different variations of the pool with
                    our Full Standard and Roto leagues.',
            'box' => [
                'title' => 'Box - Total Points',
                'content' => 'Up to 150 teams per league.  Set up your own personalized boxes for users 
                            to select from.  You also get to set the scoring categories for your league 
                            and the point values for each.  Endless variations to create the perfect 
                            league for your pool.',
            ],
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Partial : hockeypool_news
      |--------------------------------------------------------------------------
     */
    'comment_placeholder' => 'Add a reply...',
    /*
      |--------------------------------------------------------------------------
      | Route : howItWorks
      |--------------------------------------------------------------------------
     */
    'howItWorks' => [
        'title' => 'How It Works',
        'content' => 'Welcome to HockeyPool.com. We built this site because we found that the fantasy hockey experience that exists today is a bit lacklustre. A good analogy would be regular season hockey vs playoff hockey. We want to be like game 7 double-overtime.',
        'beta_info' => [
            'title' => 'Beta Information',
            'content' => 'Since this is our first year out of the gate, we know there may be bugs that are discovered. We will be do our best to resolve these in a timely fashion. To help compensate you while we are testing out the site, we are offering a special beta price to all pools at 50% off the regular price.',
        ],
        'cost' => [
            'title' => 'Cost',
            'content' => 'All pools created on HockeyPool.com start as a free trial. You can upgrade at any point. Once the trial is over, payment is required in order to gain access again. We offer two price points. Our premium pool for $29.95 gives you full access and removes all advertisements within the pool. That means all users accessing your league will be able to use it ad-free. The other option is priced lower at $19.95 but will include advertisements placed throughout the pool.',
        ],
        'creating_a_pool' => [
            'title' => 'Creating A Pool',
            'content' => [
                'string1' => 'The first step in getting started on HockeyPool.com is to create a new pool. Make sure you ',
                'register' => 'register',
                'string2' => 'first, after that head over to our ',
                'pool_creator' => 'pool creator',
                'string3' => 'to get started. Take a look at our ',
                'pool_types' => 'pool types',
                'string4' => 'to find the one right for you and your group.',
            ],
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : faq
      |--------------------------------------------------------------------------
     */
    'faq' => [
        'title' => 'HockeyPool FAQs',
        'faqs' => [
            'ques1' => 'How much does a pool cost?',
            'ans1' => 'We offer a 7 day free trial which starts once the pool has been created for both regular season and playoff pools. After 7 days the pool will be locked for all users until a purchase is made. We have two options for purchase, either $29.95 for the premium pool, or an ad supported version for $19.95.',
            'ques2' => 'How often are stats updated?',
            'ans2' => 'Stats are updated at 2:00 AM Pacific Time or 5:00 AM Eastern Time. We also have real-time stats which are updated throughout the day, however those stats are unofficial and may change overnight.',
            'ques3' => 'Are all stat categories tracked live in real-time?',
            'ans3' => 'No. While we can track most of the stats in real-time the following stats do not have live updates: faceoffs lost, faceoffs won, shifts, time on ice, goals against average, goalie losses, goalie wins, and shutouts.',
            'ques4' => 'I created a box pool but I want to change it to head to head. How can I change it.',
            'ans4' => 'Unfortunately, once you have created a specific type of pool there is no way to change it. You will need to delete the pool and create the pool in the type you want.',
            'ques5' => 'Can I display a stat that doesn’t have a point value?',
            'ans5' => 'Technically yes. When creating a pool, you could assign it a value of ‘0’. However we don’t recommend doing this, you should only add stats to your pool that are actually going to count towards the total pool points.',
            'ques6' => 'Do stat categories stack? For example, if a player scores a power play goal, does it count for both goals and power play goals?',
            'ans6' => 'Yes, stat categories do stack. So if you want to just count all types of goals (excluding shootout goals), simply use the goals category only. If you want to add extra points for a goal scored on a power play, then assign the power play goals category that incremental value.',
            'ques7' => 'I am in a keeper league, how do I continue it on for next year?',
            'ans7' => 'When creating a pool you can reload all the settings from last year into a new pool. This will keep allthe settings and players on the teams. It will also generate the draft order based on the final standings of the previous season.',
            'ques8' => 'Can I continue my regular season pool into the playoffs?',
            'ans8' => 'Regular season pools and playoff pools are kept separate. You will need to create a new pool for the playoffs.',
            'ques9' => 'How do the bench positions work?',
            'ans9' => 'Our pools allow you to set up bench positions. These are spots on your roster to keep players on your team, but not actively collecting points for your team. While a player is on the bench position, you still own him but the points he collects will not be added to your score. You can switch him into an active position at anytime as long as one is available.',
            'ques10' => 'Can I trade players?',
            'ans10' => "If trading has been enabled for your pool, then yes, we have a trading interface where you can offer a trade to another player. Depending on your pool's settings, the trade must be accepted by your pool commissioner or by the majority of owners through a league vote. Newly acquired players will be placed on your bench and must be manually assigned to your active roster.",
            'ques11' => 'Can I Add/Drop players?',
            'ans11' => 'If Add/Drops have been enabled, then yes. These roster changes will be processed overnight and will appear on your roster the next day.',
            'ques12' => 'We started our pool late. How will that affect us?',
            'ans12' => 'When you create your pool a start date is selected. Pool points will only count from that date forward.',
            'ques13' => 'What type of pools does HockeyPool.com support?',
            'ans13' => 'We have standard pools, head to head pools and box pools. We also support rotisserie scoring for all formats. To learn more about these pools, head over to our <a href="'.route("frontend.pages.poolTypes").'">Pool Types</a> page.',
            'ques14' => 'Does HockeyPool.com have any message boards?',
            'ans14' => 'Yes, each pool comes with its own personal message board. Log in to your pool and you will see the link to start trash talking.',
            'ques15' => 'I’m pretty sure my goalie just got a shutout, but I wasn’t awarded one on the stats?',
            'ans15' => 'Shutouts are only awarded if they officially receive a shutout by the NHL. If a goalie shares duties with another goalie then neither get credited with a shutout. This is an official NHL stat rule.',
            'ques16' => 'How do I continue a keeper league each season?',
            'ans16' => 'When creating a pool you can reload all the settings from last year into a new pool. This will keep all the settings and players on the teams. It will also generate the draft order based on the final standings of the previous season.'
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : advertise
      |--------------------------------------------------------------------------
     */
    'advertise' => [
        'title' => 'Advertise on HockeyPool.com',
        'content' => 'If you would like to advertise on HockeyPool.com please get in touch with us using the form below',
        'form_fields' => [
            'name' => 'Name',
            'name_placeholder' => 'Enter Your Name',
            'email' => 'Email Address',
            'email_placeholder' => 'Enter Your Email Address',
            'information' => 'Information',
            'information_placeholder' => 'Ask For Information',
        ],
        'mail' => [
            'title' => 'Advertise E-mail',
            'subject' => 'HockeyPool: Advertise Form',
            'content' => [
                'name' => 'Name:',
                'email' => 'Email:',
                'information' => 'Information:',
                'thanks' => 'Thank you,',
            ],
        ],
        'messages' => [
            'success' => 'Great! Your Information has been submitted successfully.',
            'failure' => 'There has been some problem sending mail. Please try again.',
        ],
        'validations' => [
            'name' => 'A name is required.',
            'email' => 'An email address is required.',
            'information' => 'A valid information is required.',
            'captcha' => 'The captcha is required.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : contact
      |--------------------------------------------------------------------------
     */
    'contact' => [
        'title' => 'Contact',
        'content' => [
            'line1' => 'If you have a question please review our ',
            'faqs_link' => 'FAQs',
            'line2' => 'If your question is not answered by our FAQs, send us an email using the form below. 
                Please wait 24 to 48 hours for a response.',
        ],
        'form_fields' => [
            'name' => 'Name',
            'name_placeholder' => 'Enter Your Name',
            'email' => 'Email Address',
            'email_placeholder' => 'Enter Your Email Address',
            'type' => 'Type',
            'type1' => 'Support',
            'type2' => 'Marketing & Biz Dev',
            'type3' => 'Other General Inquiry',
            'question' => 'Question',
            'question_placeholder' => 'Ask Your Question',
        ],
        'mail' => [
            'title' => 'E-mail',
            'subject' => ' Form',
            'content' => [
                'type' => 'Type:',
                'name' => 'Name:',
                'email' => 'Email:',
                'question' => 'Question:',
            ],
        ],
        'messages' => [
            'success' => 'Great! Your Question has been submitted successfully.',
            'failure' => 'There has been some problem sending mail. Please try again.',
        ],
        'validations' => [
            'type' => 'The question type is required.',
            'name' => 'A name is required.',
            'email' => 'An email address is required.',
            'question' => 'A valid question is required.',
            'captcha' => 'The captcha is required.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : terms-of-use
      |--------------------------------------------------------------------------
     */
    'terms_of_use' => [
        'title' => 'Terms of Use',
        'content' => 'Terms of Use Coming Soon.',
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : privacy-policy
      |--------------------------------------------------------------------------
     */
    'privacy_policy' => [
        'title' => 'Privacy Policy',
//        'content' => 'Privacy Policy Coming Soon',
        'content' => "<div id='ppBody'>
    <div class='innerText'>
        This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable 
        Information' (PII) is being used online. PII, as described in US privacy law and information security, is information 
        that can be used on its own or with other information to identify, contact, or locate a single person, or to identify 
        an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use,
        protect or otherwise handle your Personally Identifiable Information in accordance with our website.<br>
    </div>
    <span id='infoCo'></span>
    <br>
    <div class='grayText'>
        <strong>What personal information do we collect from the people that visit our blog, website or app?</strong>
    </div>
    <br />
    <div class='innerText'>
        When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address  or other 
        details to help you with your experience.
    </div>
    <br>
    <div class='grayText'>
        <strong>When do we collect information?</strong>
    </div>
    <br />
    <div class='innerText'>
        We collect information from you when you register on our site, place an order, subscribe to a newsletter, fill out a 
        form or enter information on our site.
    </div>
    <br>
    Provide us with feedback on our products or services  
    <span id='infoUs'></span>
    <br>
    <div class='grayText'>
        <strong>How do we use your information? </strong>
    </div>
    <br />
    <div class='innerText'> 
        We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond 
        to a survey or marketing communication, surf the website, or use certain other site features in the following ways:
        <br><br>
    </div>
    <div class='innerText'>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> To personalize your experience and to allow us to deliver the 
        type of content and product offerings in which you are most interested.
    </div>
    <div class='innerText'>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> To improve our website in order to better serve you.
    </div>
    <div class='innerText'>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> To allow us to better service you in responding to your customer
        service requests.
    </div>
    <div class='innerText'>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> To quickly process your transactions.
    </div>
    <div class='innerText'>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> To send periodic emails regarding your order or other products
        and services.
    </div>
    <span id='infoPro'></span>
    <br>
    <div class='grayText'>
        <strong>How do we protect your information?</strong>
    </div>
    <br />
    <div class='innerText'>
        Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to 
        our site as safe as possible.<br><br>
    </div>
    <div class='innerText'>We use regular Malware Scanning.<br><br>
    </div>
    <div class='innerText'>Your personal information is contained behind secured networks and is only accessible by a 
        limited number of persons who have special access rights to such systems, and are required to keep the information 
        confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) 
        technology. 
    </div><br>
    <div class='innerText'>We implement a variety of security measures when a user places an order enters, submits, or 
        accesses their information to maintain the safety of your personal information.
    </div><br>
    <div class='innerText'>All transactions are processed through a gateway provider and are not stored or processed on our 
        servers.
    </div>
    <span id='coUs'></span><br>
    <div class='grayText'>
        <strong>Do we use 'cookies'?</strong>
    </div><br />
    <div class='innerText'>Yes. Cookies are small files that a site or its service provider transfers to your computer's hard 
        drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your
        browser and capture and remember certain information. For instance, we use cookies to help us remember and process 
        the items in your shopping cart. They are also used to help us understand your preferences based on previous or 
        current site activity, which enables us to provide you with improved services. We also use cookies to help us 
        compile aggregate data about site traffic and site interaction so that we can offer better site experiences and 
        tools in the future.
    </div>
    <div class='innerText'><br><strong>We use cookies to:</strong></div>
    <div class='innerText'>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Help remember and process the items in the shopping cart.
    </div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Keep track of advertisements.</div>
    <div class='innerText'><br>You can choose to have your computer warn you each time a cookie is being sent, or you can 
        choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look 
        at your browser's Help Menu to learn the correct way to modify your cookies.<br>
    </div><br>
    <div class='innerText'>If you turn cookies off, some features will be disabled. It won't affect the user's experience 
        that make your site experience more efficient and may not function properly.
    </div><br>
    <div class='innerText'>However, you will still be able to place orders .</div><br>
    <span id='trDi'></span><br>
    <div class='grayText'>
        <strong>Third-party disclosure</strong>
    </div><br />
    <div class='innerText'>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable 
        Information.
    </div>
    <span id='trLi'></span><br>
    <div class='grayText'><strong>Third-party links</strong></div><br />
    <div class='innerText'>Occasionally, at our discretion, we may include or offer third-party products or services on our 
        website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility 
        or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of 
        our site and welcome any feedback about these sites.
    </div>
    <span id='gooAd'></span><br>
    <div class='blueText'><strong>Google</strong></div>
    <br />
    <div class='innerText'>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put
        in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en 
        <br><br>
    </div>
    <div class='innerText'>We use Google AdSense Advertising on our website.</div>
    <div class='innerText'><br>Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART
        cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users
        may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.<br>
    </div>
    <div class='innerText'><br><strong>We have implemented the following:</strong></div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Remarketing with Google AdSense</div><br>
    <div class='innerText'>We, along with third-party vendors such as Google use first-party cookies (such as the Google 
        Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together
        to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our 
        website. 
    </div>
    <div class='innerText'>
        <br><strong>Opting out:</strong><br>
        Users can set preferences for how Google advertises to you using the Google Ad Settings
        page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt
        Out page or by using the Google Analytics Opt Out Browser add on.
    </div>
    <span id='calOppa'></span><br>
    <div class='blueText'>
        <strong>California Online Privacy Protection Act</strong>
    </div><br />
    <div class='innerText'>
        CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy.
        The law's reach stretches well beyond California to require any person or company in the United States (and conceivably 
        the world) that operates websites collecting Personally Identifiable Information from California consumers to post a 
        conspicuous privacy policy on its website stating exactly the information being collected and those individuals or 
        companies with whom it is being shared. -  
        See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf<br>
    </div>
    <div class='innerText'><br>
        <strong>According to CalOPPA, we agree to the following:</strong><br>
    </div>
    <div class='innerText'>Users can visit our site anonymously.</div>
    <div class='innerText'>Once this privacy policy is created, we will add a link to it on our home page or as a minimum, 
        on the first significant page after entering our website.<br>
    </div>
    <div class='innerText'>Our Privacy Policy link includes the word 'Privacy' and can be easily be found on the page specified
        above.
    </div>
    <div class='innerText'><br>You will be notified of any Privacy Policy changes:</div>
    <div class='innerText'>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> On our Privacy Policy Page<br>
    </div>
    <div class='innerText'>Can change your personal information:</div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> By logging in to your account</div>
    <div class='innerText'><br><strong>How does our site handle Do Not Track signals?</strong><br></div>
    <div class='innerText'>We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not 
        Track (DNT) browser mechanism is in place. 
    </div>
    <div class='innerText'><br><strong>Does our site allow third-party behavioral tracking?</strong><br></div>
    <div class='innerText'>It's also important to note that we allow third-party behavioral tracking</div>
    <span id='coppAct'></span><br>
    <div class='blueText'><strong>COPPA (Children Online Privacy Protection Act)</strong></div><br />
    <div class='innerText'>When it comes to the collection of personal information from children under the age of 13 years old,
        the Children's Online Privacy Protection Act (COPPA) puts parents in control.  The Federal Trade Commission, United States'
        consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services 
        must do to protect children's privacy and safety online.<br><br>
    </div>
    <div class='innerText'>We do not specifically market to children under the age of 13 years old.</div>
    <span id='ftcFip'></span><br>
    <div class='blueText'><strong>Fair Information Practices</strong></div><br />
    <div class='innerText'>
        The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they 
        include have played a significant role in the development of data protection laws around the globe. Understanding the 
        Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy
        laws that protect personal information.<br><br>
    </div>
    <div class='innerText'>
        <strong>In order to be in line with Fair Information Practices we will take the following responsive action, should 
            a data breach occur:</strong>
    </div>
    <div class='innerText'>We will notify you via email</div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Within 7 business days</div>
    <div class='innerText'><br>We also agree to the Individual Redress Principle which requires that individuals have the 
        right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. 
        This principle requires not only that individuals have enforceable rights against data users, but also that individuals 
        have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.
    </div>
    <span id='canSpam'></span><br>
    <div class='blueText'><strong>CAN SPAM Act</strong></div><br />
    <div class='innerText'>
        The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages,
        gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.
        <br><br>
    </div>
    <div class='innerText'><strong>We collect your email address in order to:</strong></div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Send information, respond to inquiries, 
        and/or other requests or questions
    </div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Process orders and to send information and 
        updates pertaining to orders.
    </div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Market to our mailing list or continue to 
        send emails to our clients after the original transaction has occurred.
    </div>
    <div class='innerText'><br><strong>To be in accordance with CANSPAM, we agree to the following:</strong></div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Not use false or misleading subjects or 
        email addresses.
    </div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Identify the message as an advertisement in 
        some reasonable way.
    </div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Include the physical address of our business 
        or site headquarters.
    </div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Monitor third-party email marketing services
        for compliance, if one is used.
    </div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Honor opt-out/unsubscribe requests quickly.</div>
    <div class='innerText'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> Allow users to unsubscribe by using the
        link at the bottom of each email.
    </div>
    <div class='innerText'><strong><br>If at any time you would like to unsubscribe from receiving future emails, you can 
            email us at</strong>
    </div>
    <div>info@HockeyPool.com and we will promptly remove you from <strong>ALL</strong> correspondence.</div><br>
    <span id='ourCon'></span><br>
    <div class='blueText'><strong>Contacting Us</strong></div><br />
    <div class='innerText'>If there are any questions regarding this privacy policy, you may contact us using the information 
        below.<br><br>
    </div>
    <div class='innerText'>https://HockeyPool.com</div>
    <div class='innerText'>4198 Kashtan Place</div>
    Victoria, British Columbia V8X 4L7 
    <div class='innerText'>Canada</div>
    <div class='innerText'>info@HockeyPool.com</div>
    <div class='innerText'><br>Last Edited on 2016-09-02</div>
</div>",
    ],
    /*
      |--------------------------------------------------------------------------
      | General
      |--------------------------------------------------------------------------
     */
    'general' => [
        'faq' => 'FAQ',
        'advertise' => 'Advertise',
        'contact' => 'Contact',
        'view_in_browser' => 'view in Browser',
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : draft-kit {{trans('pages.draft_kit.')}}
      |--------------------------------------------------------------------------
     */
    'draft_kit' => [
        'title' => ' Draft Kit',
        'hockeypool' => 'HockeyPool ',
        'dotcom' => '.com',
        'line1' => 'Get the leg up on your opponents with our draft rankings built by the experts here at HockeyPool.com.',
        'line2' => "We took a deep look at each player, each team and the possible line combinations to determine who we think will have the best year. In other words, we did the homework so you don't have to.",
        'line3' => 'Enter your email below and complete the purchase to receive our draft rankings PDF which you can print out and take to your draft party.',
        'email_placeholder' => 'Enter Email Address*',
        'price' => 'Price : ',
        'complete_purchased' => 'Complete Purchase',
        'checkpoint1' => 'Includes Veterans and Rookies',
        'checkpoint2' => 'Instant PDF Download',
        'checkpoint3' => 'Top 300 Players',
        'checkpoint4' => 'Top 50 Goalies',
        'error_title' => 'Oops',
        'error_message' => 'Please enter a valid email',
    ],
];
