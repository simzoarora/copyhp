<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'general_error' => 'You do not have access to do that.',
    'socialite' => [
        'unacceptable' => ':provider is not an acceptable login type.',
    ],
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'unknown' => 'An unknown error occurred',
    
    /*
    |--------------------------------------------------------------------------
    | Route : editAccount
    |--------------------------------------------------------------------------
    */
    'editAccount' => [
        'title1' => 'Edit Account Info',
        'title2' => 'Change Password',
        'title3' => 'Email Notification Settings',
        'form_fields' => [
            'name' => 'Account Name',
            'email' => 'Email',
            'current_password' => 'Current Password',
            'new_password' => 'New Password',
            'retype_password' => 'Retype Password',
            'checkbox_1' => 'Weekly Standing Updates',
            'checkbox_2' => 'Trade Offers',
            'checkbox_3' => 'Injury/Roster Notifications',
            'checkbox_4' => 'Newsletter',
        ],
        'messages' => [
            'success' => 'Great! Your account info has been updated successfully.',
            'failure' => 'Wrong password.',
        ],
        'validations' => [
            'name' => 'A name is required.',
            'email' => 'An email address is required.',
            'password' => 'A password is required.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : my-account
    |--------------------------------------------------------------------------
    */
    'my_account' => [
        'my_pools' => 'My Pools',
        'current_pools' => 'Current Pools',
        'past_pools' => 'Past Pools',
        'no_pools' => [
            'text1' => 'Looks like you aren\'t in any pools. Don\'t worry, you can ',
            'text2' => 'create one!',
        ],
        'no_past_pools' => 'There are no past pools present.',
        'pool_stats' => 'Pool Stats',
        'pool_points' => 'Pool Points',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | login trans('auth.login.')
    |--------------------------------------------------------------------------
    */
    'login' => [
        'unconfirmed_email' => 'Sorry but you need to confirm your email address before you can log in.',
        'unknown_email' => 'Unknown Email.',
        'incorrect_password' => 'Password is incorrect.',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | changePasswordSubmit trans('auth.changePasswordSubmit.')
    |--------------------------------------------------------------------------
    */
    'changePasswordSubmit' => [
        'update_successful' => 'Great! Your account info has been updated successfully.',
        'wrong_password' => 'Wrong password.',
    ],
];
