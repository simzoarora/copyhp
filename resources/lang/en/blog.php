<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pages Language Lines : English
    |--------------------------------------------------------------------------
    | The following language lines are used in home throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    */
    
    /*
    |--------------------------------------------------------------------------
    | Route : blog
    |--------------------------------------------------------------------------
    |
    */
    'blog' => [
        'title' => 'HockeyPool Blog',
        'page_header' => 'Blogs',
        'by' => 'By: ',
        'hockeypool_staff' => 'HockeyPool Staff',
        'no_blogs' => 'There are currently no blog posts.',
        'back_to_blog_home' => ' Back to Blog Home',
        'share_us_on' => 'Share Us On: ',
        'comments' => 'Comments',
        'comment' => 'Comment',
        'comments_placeholder' => 'Add a comment...',
        'published' => 'Published',
        'draft' => 'Draft',
        'no_blog_entries' => 'No Blog Entries.',
        
        'validations' => [
            'title' => 'A title is required',
            'body' => 'A blog description is required',
            'thumbnail_image' => 'A thumbnail image is required',
            'featured_image' => 'A featured image is required',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | blogCommentsSubmit trans('blog.blogCommentsSubmit.')
    |--------------------------------------------------------------------------
    |
    */
    'blogCommentsSubmit' => [
        'invalid_blog_error' => 'Invalid blog.',
    ],
];