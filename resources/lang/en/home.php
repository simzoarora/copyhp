<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Page Language Lines : English
    |--------------------------------------------------------------------------
    | The following language lines are used in home throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    */
    
    /*
    |--------------------------------------------------------------------------
    | Partial : home_logged
    |--------------------------------------------------------------------------
    */
    'home_logged' => [
        'no_pools_present1' => 'Looks like you aren\'t in any pools. Don\'t worry, you can ',
        'no_pools_present2' => ' create one!',
        'edit' => 'Edit',
        'versus' => 'vs',
        'not_finished' => '(Pool creation not finished)',
        'continue' => 'Continue',
        'pool_stats' => 'Pool Stats',
        'pool_points' => 'Pool Points',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | twitterCommentsSubmit trans('home.twitterCommentsSubmit.')
    |--------------------------------------------------------------------------
    */
    'twitterCommentsSubmit' => [
        'invalid_tweet_error' => 'Invalid tweet.',
    ],
];    