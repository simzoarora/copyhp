<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Scores Page Language Lines : English
    |--------------------------------------------------------------------------
    | The following language lines are used in pools throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    */
    
    /*
    |--------------------------------------------------------------------------
    | Route : scores
    |--------------------------------------------------------------------------
    */
    'scores' => [
        'ot' => 'OT',
        'so' => 'SO',
        'box_score' => 'Box Score',
        'messages' => [
            'no_games_scheduled' => 'No games are scheduled for this date.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : scores/{competition_id} -- game details
    |--------------------------------------------------------------------------
    */
    'game_details' => [
        'ot' => 'OT',
        'so' => 'SO',
        'scoring_summary' => 'Scoring Summary',
        'team' => 'Team',
        'score' => 'Score',
        'goal' => 'Goal',
        'assist' => 'Assist',
        'time' => 'Time',
        'no_scores' => 'No Scores',
        'first_period' => '1st Period',
        'second_period' => '2nd Period',
        'third_period' => '3rd Period',
        'overtime' => 'Overtime',
        'shootouts' => 'Shootouts',
        'no_shootouts' => 'No Shootouts',
        'result' => 'Result',
        'player' => 'Player',
        'box_score' => 'Box Score',
        'sog' => 'SOG',
        'fo_cent' => 'FO%',
        'pp' => 'PP',
        'pim' => 'PIM',
        'hits' => 'HITS',
        'blks' => 'BLKS',
        'forwards' => 'Forwards',
        'g' => 'G',
        'a' => 'A',
        'p' => 'P',
        'toi' => 'TOI',
        'pp_toi' => 'PP TOI',
        'sh_toi' => 'SH TOI',
        'goalies' => 'Goalies',
        'messages' => [
            'no_player_present' => 'No player present.',
            'no_goalie_present' => 'No goalie present.',
        ],
    ],
];    