<?php

return [
    'new_message_board' => 'A new topic has been posted on the message board for :Pool',
    'reply_to_post' => 'Someone replied to your post.',
    'trade_created' => 'A trade has been proposed to you by :Team, trade will automatically expire on :Expire',
    'missing_roster' => 'Your current roster has open positions available. Please adjust your roster and fill the empty spots',
    'injured_player' => ':Player is injured and currently part of your active roster.',
    'trade_accepted' => 'The trade between you and :Team has been accepted and will be processed on :time',
    'trade_rejected' => 'Your trade to :Team has been rejected.',
    'trade_cancelled_by_parent' => 'The trade between you and :Team has been cancelled by :Team',
    'trade_cancelled_by_commissioner' => 'The trade between you and :Team has been cancelled by Pool commissioner',
    'trade_expired' => 'Your trade proposed to :Team has expired.',
];

