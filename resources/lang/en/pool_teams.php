<?php
return [
    'edit_team'=>[
        'edit_team_info' => 'Edit Team Info',
        'team_name' => 'Team Name',
        'team_logo' => 'Team Logo',
        'drop_image' => 'Drop your image here or click to add one!',
    ]
];

