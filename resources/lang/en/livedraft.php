<?php

return [
/*
  |--------------------------------------------------------------------------
  | Live Draft Pages Language Lines : English
  |--------------------------------------------------------------------------
  | The following language lines are used in pools throughout the system.
  | Regardless where it is placed, a label can be listed here so it is easily
  | found in a intuitive way.
 */
'heading' => 'Live Draft',
'round' => [
'current_pick' => 'It is currently your pick',
 'overview' => 'Draft Overview',
],
 'queue' => [
'add' => 'Add to My Queue',
 'draft' => 'Draft',
 'my_queue' => 'My Queue',
 'rank' => 'Rank',
 'player' => 'Player',
],
 'team' => [
'my_team' => 'My Team',
 'pos' => 'Pos.',
 'empty' => '--empty--'
],
 'talk' => [
'trash_talk' => 'Trash Talk',
 'input_placeholder' => 'Enter a message'
],
 'navs' => [
'players' => 'Players',
 'teams' => 'Teams',
 'draft_result' => 'Draft Results',
 'pool_setting' => 'Pool Settings'
],
 'pool_settings' => [
'pool_type' => 'Pool Type:',
 'positions' => 'Positions:',
 'points' => 'Points:',
 'max_trades' => 'Max Trades:',
 'max_acquisitions' => 'Max Acquisitions:'
],
 'draft_results' => [
'round' => 'Round',
 'pick' => 'Pick',
 'overall' => 'Overall',
 'team' => 'Team',
 'player' => 'Player',
],
 'players' => [
'search_player' => 'Search for Player',
 'rank' => 'Rank',
 'player' => 'Player'
]
];
