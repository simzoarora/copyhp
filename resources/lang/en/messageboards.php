<?php

return [
    'header'=>[
        'fan_forum'=>'Fan Forum',
        'new_topic'=>'New Topic',
        'topics'=>'Topics',
        'my_posts'=>'My Posts',
        'private_messages'=>'Private Messages',
        'fan_forum'=>'Fan Forum',
        'inbox'=>'Inbox',
        'new_message'=>'New Message',
        'private_text' => 'All private Messages'
    ],
    'form'=>[
        'subject'=>'Subject',
        'subject_private'=>'Subject of the Message Here',
        'type_message'=>'Type Message',
        'type_message_private'=>'Type your message here',
        'post'=>'Post',
        'to'=>'To',
        'reply_subject'=>'Re: Name of the Subject Here',
        'post_message'=>'Post Message',
        'message_to_user' => 'Message To Users',
        'link' => 'Link',
    ],
    'subheader'=>[
        'title'=>'Title',
        'replies'=>'Replies',
        'latest_post'=>'Latest post'
    ],
    'extra'=>[
        'by'=>'by',
        'reply'=>'Reply',
        'show_less'=>'Show less',
        'show_more'=>'Show more',
        'sort'=>'Sort',
        'newest'=>'Newest',
        'oldest'=>'Oldest',
        'expand-all'=>'Expand All',
        'collapse-all'=>'Collapse All',
        'you'=>'You'
    ],
];

