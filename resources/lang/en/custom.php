<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines : English
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in home throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
    'slider' => [
        'h1' => 'Serious. Fantasy. Hockey.',
        'h2' => 'Bar none the best place for hockey pools on the planet!',
    ],

    'pool_types' => 'Pool Types', 
    'standard_pool' => 'STANDARD HOCKEY POOL',
    'standard_pool_p' => 'Our standard hockey pool gives you the classic fantasy hockey experience. Select the roster positions available and the amount of points each statistic is worth. The team with the highest total at the end of the season wins. You always have the option of rotisserie scoring. In roto leagues, you are competing to be the best in each stat category. The team with the most well rounded squad takes home the title.',
    'head2head_pool' => 'HEAD TO HEAD HOCKEY POOL',
    'head2head_pool_p' => 'In a head to head league, you get matched up against one other opponent each week. If you win your weekly matchup, you’ll earn yourself a victory in the win column. If you lose your weekly matchup, you’ll have a loss added to your season-long record. You also have the option of rotisserie scoring. In roto leagues, you compete against your opponent in each stat category. The team that wins the most categories gets the victory.',
    'box_pool' => 'BOX HOCKEY POOL',
    'box_pool_p' => 'This is ideal for large pools. It’s simple - select the best player in each box. Up to 150 players per league. The team with the highest total at the end of the season wins. You always have the option of rotisserie scoring. In roto leagues, you are competing to be the best in each stat category. The team with the most well rounded squad takes home the title.',
    'rotisserie_pool'=>'ROTISSERIE SCORING (CATEGORY WINS)',
    'rotisserie_pool_p'=>'All of our pool formats support rotisserie scoring. For standard and box pools, your team will be ranked in each of the stat categories your pool uses. The team that ranks the best across all categories (ranking average) at the end of the season wins. For head to head roto pools, you compete against your opponent in each stat category. The team that wins the most categories for the week gets the victory.',
    'head2head' => 'Head 2 Head',
    'transcona_cup' => '8th Transcona Cup',
    'matchups' => 'Matchups May 2nd, 2016',
    'leading' => 'Leading',
    'tied' => 'Tied',
    'losing' => 'Losing',
    'view_pool_details' => 'View Pool Details',

    'hockey_pool_news' => [
        'heading' => '#HockeyPool News',
        'h3' => 'Greasy Beard',
        'p' => 'Suspendise borders between them are doubled. In regular button groups, margin-left',
        'date' => 'April 29, 2016',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : league_nav
    |--------------------------------------------------------------------------
    */
    'league_nav' => [
        'today' => 'Today',
        'week' => 'Week',
        'last_7_days' => 'Last 7 Days',
        'last_30_days' => 'Last 30 Days',
        'season' => 'Season',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : player_news
    |--------------------------------------------------------------------------
    */
    'player_news' => [
        'title' => 'Player News',
        'my_team' => 'My Pool',
        'league' => 'League',
        'read_more' => 'read more',
        'less' => 'read less',
        'messages' => [
            'no_team_news' => 'No team news present.',
            'no_league_news' => 'No league news present.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : pool_name_header
    |--------------------------------------------------------------------------
    */
    'pool_name_header' => [
        'pool_stats' => 'Pool Stats',
        'pool_points' => 'Pool Points',
        'add' => 'Add',
        'trade' => 'Trade',
        'drop' => 'Drop',
        'activate' => 'Activate',
        'versus' => 'vs',
        'payment_alert1' => 'The pool is currently in trial mode and will expire in ',
        'payment_alert2' => '. Please complete your payment to activate the pool. ',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : nhl_headlines
    |--------------------------------------------------------------------------
    */
    'nhl_headlines' => [
        'title' => 'NHL Headlines',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : pool_nav
    |--------------------------------------------------------------------------
    */
    'pool_nav' => [
        'pool_menu' => 'Pool Menu',
        'pool' => 'Pool',
        'overview' => 'Overview',
        'transaction' => 'Transaction',
        'in_action' => 'In Action',
        'live_stats' => 'Live Stats',
        'draft_results' => 'Draft Results',
        'scores' => 'Scores',
        'teams' => 'Teams',
        'roster' => 'Roster',
        'trade' => 'Trade',
        'players' => 'Players',
        'player_list' => 'Player List',
        'league_leaders' => 'League Leaders',
        'injury_report' => 'Injury Report',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : team_records
    |--------------------------------------------------------------------------
    */
    'team_records' => [
        'team' => 'Team',
        'record' => 'Record',
        'messages' => [
            'no_team_records' => 'No team records present.',
        ], 
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : trial_period
    |--------------------------------------------------------------------------
    */
    'trial_period' => [
        'expired' => 'Trial Period Expired',
        'continue' => 'The trial period for your pool has expired.  To continue using this pool, complete your purchase to reactivate this pool.',
        'activate' => 'Activate',
        'contact' => 'The trial period for your pool has expired.  Please contact your
                                pool commissioner to get them to activate the pool.',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : scoreboard
    |--------------------------------------------------------------------------
    */
    'scoreboard' => [
        'view_stats' => 'View Stats',
        'date' => 'Date',
        'view_live_stats' => 'View Live Stats',
        'et' => 'ET',
    ],
];
