<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Pool Pages Language Lines : English
      |--------------------------------------------------------------------------
      | The following language lines are used in pools throughout the system.
      | Regardless where it is placed, a label can be listed here so it is easily
      | found in a intuitive way.
     */

    /*
      |--------------------------------------------------------------------------
      | Route : pool/{id}/dashboard
      |--------------------------------------------------------------------------
     */
    'pool_dashboard' => [
        'versus' => 'vs',
        'standing' => 'Standing',
        'rank' => 'Rank',
        'team_name' => 'Team Name',
        'wlt' => 'W-L-T',
        'points' => 'Points',
        'last_week' => 'Last Week',
        'waiver' => 'Waiver',
        'moves' => 'Moves',
        'no_teams_present' => 'No teams present.',
        'league_feed' => 'League Feed',
        'type' => 'Type',
        'team' => 'Team',
        'details' => 'Details',
        'time' => 'Time',
        'add' => 'Add',
        'drop' => 'Drop',
        'trade' => 'Trade',
        'total_points' => 'Total Points',
        'messages' => [
            'no_pool_present' => 'No pool is present.',
            'invalid_pool' => 'Invalid pool.',
            'no_league_feed_present' => 'No league feed present.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : pool/{id}/transaction
      |--------------------------------------------------------------------------
     */
    'transaction' => [
        'pool_transactions' => 'Pool Transactions',
        'type' => 'Type',
        'team' => 'Team',
        'details' => 'Details',
        'time' => 'Time',
        'add' => 'Add',
        'drop' => 'Drop',
        'trade' => 'Trade',
        'pending_trades' => 'Pending Trades',
        'no_trade' => 'There are no trades at this time.',
        'accept_vote' => 'Vote To Accept',
        'accept_trade' => 'Accept Trade',
        'reject_vote' => 'Vote To Reject',
        'reject_trade' => 'Reject Trade',
        'cancel_trade' => 'Cancel Trade',
        'my_trades' => 'My Trade Offers',
        'offered_trades' => 'Trades Offered To Me',
        'completion_time' => 'This trade will complete on ,:time Eastern',
        'messages' => [
            'no_transactions' => 'No Transactions.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : pool/{id}/in_action_tonight
      |--------------------------------------------------------------------------
     */
    'in_action_tonight' => [
        'title' => 'In Action Tonight',
        'position' => 'Position',
        'player' => 'Player',
        'opponent' => 'Opponent',
        'pool_points' => 'Pool Points',
        'messages' => [
            'no_teams_present' => 'No teams are present in this pool.',
            'invalid_pool' => 'Invalid pool.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : pool/{id}/live_stats
      |--------------------------------------------------------------------------
     */
    'live_stats' => [
        'title' => 'Live Stats',
        'team' => 'Team ',
        'name' => 'Name',
        'pool_points' => 'Pool Points ',
        'status' => 'Status',
        'pos' => 'Pos',
        'skaters' => 'Skaters',
        'pool_pts' => 'Pool Pts',
        'starting_lineup_totals' => 'Starting Lineup Totals',
        'goalie' => 'Goalie',
        'messages' => [
            'no_live_stats_prsent' => 'No live stats present.',
            'invalid_pool' => 'Invalid pool.',
            'no_player_present' => 'No player present.',
            'no_goalie_present' => 'No goalie present.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : pool/{id}/trade
      |--------------------------------------------------------------------------
     */
    'trades' => [
        'create_trade' => 'Create Trade',
        'propose_trade' => 'Propose Trade',
        'select_players_text' => 'Select the players you want to trade.  You must stay below the total roster size to be able to complete a trade.',
        'team' => 'Team',
        'players_traded' => 'Players Traded',
        'players_dropped' => 'Players Dropped',
        'roster_size' => 'Roaster Size',
        'after_trade' => 'After Trade',
        'accepted' => 'Requirements Met',
        'team_name' => 'Team Name',
        'dropped' => 'Dropped',
        'traded' => 'Traded',
        'trade' => 'Trade',
        'drop' => 'Drop',
        'status' => 'Status',
        'pos' => 'Pos',
        'skaters' => 'Skaters',
        'opp' => 'Opp',
        'pre' => 'Pre',
        'current' => 'Current',
        'own' => 'Own',
        'start' => 'Start',
        'pool_pts' => 'Pool Pts',
        'pre2' => 'pre',
        'messages' => [
            'no_player_present' => 'No player present.',
            'no_goalie_present' => 'No goalie present.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : pool/{pool_id}/player-stats
      |--------------------------------------------------------------------------
     */
    'player_stats' => [
        'select_position' => 'Select Position',
        'select_team' => 'Select Team',
        'select_season' => 'Select Season',
        'find_player' => 'Find Player',
        'add' => 'Add',
        'team' => 'Pool Team',
        'pos' => 'Pos',
        'skaters' => 'Player Name',
        'current' => 'Pool Rank',
        'own' => 'Own%',
        'total_points' => 'Total Points',
        'free_agent' => 'Free Agent',
        'messages' => [
            'no_results_found' => 'No results found.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : pool/{pool_id}/add-setup
      |--------------------------------------------------------------------------
     */
    'add_setup' => [
        'select_players_to_drop' => 'Select Players To Drop',
        'content' => 'Now that you have selected the players to be added to your team, you may have to drop some players to make room for these players.  
                        Choose the players from your team that you want to drop.  Your roster size must stay below the maximum for you to continue',
        'roster_position' => 'Roster Position',
        'total_available_spots' => 'Total Available Spots',
        'before_transaction' => 'Before Transaction',
        'after_transaction' => 'After Transaction',
        'no_roster_positions' => 'No Rosters Positions',
        'total_roster_size' => 'Total Roster Size',
        'review_transaction' => 'Review Transaction',
        'cancel_transaction' => 'Cancel Transaction',
        'transaction_details' => 'Transaction Details',
        'you_will_add' => 'You will add: ',
        'you_will_drop' => 'You will drop: ',
        'transaction_processed_on' => 'This transaction will be processed on ',
        'players_to_be_added' => 'Players to be added',
        'pos' => 'Pos',
        'skaters' => 'Skaters',
        'opp' => 'Opp',
        'pre' => 'Pre',
        'current' => 'Current',
        'own' => 'Own',
        'start' => 'Start',
        'pool_points' => 'Pool Pts',
        'my_team' => 'My Team',
        'select_players_to_drop' => 'Select players to drop from your team',
        'dropped' => 'Dropped',
        'traded' => 'Traded',
        'drop' => 'Drop',
        'roster_pos' => 'Roster Pos',
        'skater_pos' => 'Skater Pos',
        'status' => 'Status',
        'action_cant_be_undone' => 'This action can not be undone',
        'accept' => 'Accept',
        'cancel' => 'Cancel',
        'messages' => [
            'no_player_present' => 'No player present.',
            'no_goalie_present' => 'No goalie present.',
            'no_player_added' => 'No player added.',
            'no_player' => ', No Player',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : pool/{pool_id}/drop-setup {{trans('pool.drop_setup.')}}
      |--------------------------------------------------------------------------
     */
    'drop_setup' => [
        'select_players_to_drop' => 'Select Players To Drop',
        'roster_position' => 'Roster Position',
        'total_available_spots' => 'Total Available Spots',
        'before_transaction' => 'Before Transaction',
        'after_transaction' => 'After Transaction',
        'no_roster_positions' => 'No Rosters Positions',
        'total_roster_size' => 'Total Roster Size ',
        'review_transaction' => 'Review Transaction',
        'cancel_transaction' => 'Cancel Transaction',
        'transaction_details' => 'Transaction Details',
        'you_will_drop' => 'You will drop: ',
        'transaction_processed_on' => 'This transaction will be processed on ',
        'select_players_to_drop' => 'Select players to drop from your team',
        'dropped' => 'Dropped',
        'traded' => 'Traded',
        'my_team' => 'My Team',
        'drop' => 'Drop',
        'roster_pos' => 'Roster Pos',
        'skater_pos' => 'Skater Pos',
        'status' => 'Status',
        'opp' => 'Opp',
        'pre' => 'Pre',
        'current' => 'Current',
        'own' => 'Own',
        'start' => 'Start',
        'pool_points' => 'Pool Pts',
        'pre' => 'pre',
        'action_cant_be_undone' => 'This action can not be undone',
        'accept' => 'Accept',
        'cancel' => 'Cancel',
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : pool/{pool_id}/league-leader {{trans('pool.league_leaders.')}} league_leaders.messages.
      |--------------------------------------------------------------------------
     */
    'league_leaders' => [
        'select_position' => 'Select Position',
        'total' => 'Total',
        'owner' => 'Owner',
        'player' => 'Player',
        'team' => 'Team',
        'position' => 'Position',
        'total_points' => 'Total Points',
        'messages' => [
            'no_teams_present' => 'No teams present.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : pool/{pool_id}/draft-result {{trans('pool.draft_result.')}} draft_result.messages.
      |--------------------------------------------------------------------------
     */
    'draft_result' => [
        'team' => 'TEAM ',
        'player' => 'PLAYER ',
        'pool_points' => 'Pool Points',
        'round_rank' => 'Round Rank',
        'pick' => 'Pick',
        'points' => 'Points',
        'messages' => [
            'no_player_present' => 'No player present.',
            'no_draft_result' => 'No draft result present.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : matchup/matchup-individual/{matchup_id} {{trans('pool.matchup_individual.')}} matchup_individual.messages.
      |--------------------------------------------------------------------------
     */
    'matchup_individual' => [
        'today' => 'Today',
        'week' => 'Week',
        'goal_tender_appearances' => 'Goal tender appearances 1/3',
        'versus' => 'vs',
        'skaters' => 'Skaters',
        'goalie' => 'Goalie',
        'messages' => [
            'no_player_present' => 'No player present.',
            'no_goalie_present' => 'No goalie present.',
            'no_results_found' => 'No results found.',
        ],
        'points' => 'Points'
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : pool/{pool_id}/box-draft {{trans('pool.box-draft.')}} box-draft.messages.
      |--------------------------------------------------------------------------
     */
    'box-draft' => [
        'submit_picks' => 'Submit Picks',
        'clear_selections' => 'Clear Selections',
        'pick' => 'Pick',
        'messages' => [
            'checkbox_error' => 'Please check checkbox in ',
            'no_box_result' => 'No result present',
            'no_results_found' => 'No results found.',
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : pool/{pool_id}
      |--------------------------------------------------------------------------
     */
    'pool_show' => [
        'pool_name' => 'Pool Name',
        'pool_format' => 'Pool Format',
        'season' => 'Season',
        'playoff_format' => 'Playoff Format',
        'lock_teams' => 'Lock Eliminated Teams',
        'no_of_teams' => 'Number of Teams',
        'start_week' => 'Start Week',
        'min_goalie' => 'Minimum Goalie Appearances',
        'max_add_season' => 'Maximum Adds per Season',
        'max_add_week' => 'Maximum Adds per Week',
        'max_trade' => 'Maximum Trades per Season',
        'trade_deadline' => 'Trade Deadline',
        'trade_reject' => 'Trade Reject Time',
        'waiver' => 'Waivers',
        'waiver_time' => 'Waiver Time',
        'draft_pick_trade' => 'Draft Pick Trades',
        'positions' => 'Positions',
        'point_categories' => 'Point Categories',
        'no' => 'No',
        'yes' => 'Yes',
        'invite_link' => 'Invite Link'
    ],
    'unauthorized_action' => 'Unauthorized action.',
    'development_error' => 'DEVELOPMENT ERROR',
    'pool_votes'=>[
        'commissioner_wait'=>'You have accepted the trade. Please wait for commissioner action.',
        'vote_wait'=>'You have accepted the trade. Please wait while voting is underway to complete the trade.',
        'vote_accept'=>'You voted to accept the trade.',
        'vote_reject'=>'You voted to reject the trade.',
    ]
];
