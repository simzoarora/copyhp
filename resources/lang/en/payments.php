<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Payments Page Language Lines : English
    |--------------------------------------------------------------------------
    | The following language lines are used in pools throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    */
    
    /*
    |--------------------------------------------------------------------------
    | Route : pool/{pool_id}/payment/{payment_item_id} {{trans('payments.payments.')}} payments.messages.
    |--------------------------------------------------------------------------
    */
    'payments' => [
        'line1' => 'Get full access to your pool for the remainder of the season. With the option you also remove all ads 
                        from inside the pool for ALL users using your pool. Be a good Commissioner and give them the Hockey Pool
                        experience they deserve.',
        'line2' => 'Since this is our first year of operations and we still may have a couple bugs to work out, we want
                        to offer you the special beta price of <b>50% our normal fees.</b>',
        'lucky_you' => 'Lucky you!',
        'reg_price' => 'Reg. Price : ',
        'beta_price' => 'Beta Price : ',
        'complete_setup' => 'Complete Setup',
        'no_ads' => 'No advertisements',
        'smartphone_optimized' => 'Smartphone Optimized',
        'live_scoring_updates' => 'Live scoring updates',
        'tech_support' => 'Technical Support',
        'prefer_ads' => 'Prefer Ads?',
        'ad_supported_version_text' => 'Use the ad supported version for $9.97',
    ],
];    