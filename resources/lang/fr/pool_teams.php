<?php
return [
    'edit_team'=>[
        'edit_team_info' => 'FR-Edit Team Info',
        'team_name' => 'FR-Team Name',
        'team_logo' => 'FR-Team Logo',
        'drop_image' => 'FR-Drop your image here or click to add one!',
    ]
];

