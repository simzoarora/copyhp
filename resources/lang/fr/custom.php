<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines : French
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in home throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
    'slider' => [
        'h1' => 'Fr-Serious. Fantasy. Hockey.',
        'h2' => 'Fr-Bar down the best place for Fantasy Hockey on the planet',
    ],

    'pool_types' => 'Fr-Pool Types', 
    'standard_pool' => 'Fr-STANDARD POOL',
    'standard_pool_p' => 'Fr-Our standard pools give you the classic fantasy hockey experience. Select the roster positions available, the amount of points each statistic is worth. The team with the highest total at the end of the season wins.',
    'head2head_pool' => 'Fr-HEAD TO HEAD POOL',
    'head2head_pool_p' => 'Fr-Two different types of Head To Head pools available. Weekly win: Set your roster for the week, which ever team does best takes home the victory. Category Win: Teams compete in a number of different categories, win the category to improve your record.',
    'box_pool' => 'Fr-BOX POOL',
    'box_pool_p' => 'Fr-Best for large pools. Select the best player available in each box depending on the categories for points. After that, best team wins. Up to 150 players per league.',
    'head2head' => 'Fr-Head 2 Head',
    'transcona_cup' => 'Fr-8th Transcona Cup',
    'matchups' => 'Fr-Matchups May 2nd, 2016',
    'leading' => 'Fr-Leading',
    'tied' => 'Fr-Tied',
    'losing' => 'Fr-Losing',
    'view_pool_details' => 'Fr-View Pool Details',

    'hockey_pool_news' => [
        'heading' => 'Fr-#HockeyPool News',
        'h3' => 'Fr-Greasy Beard',
        'p' => 'Fr-Suspendise borders between them are doubled. In regular button groups, margin-left',
        'date' => 'April 29, 2016',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : league_nav
    |--------------------------------------------------------------------------
    */    
    'league_nav' => [
        'today' => 'Fr-Today',
        'week' => 'Fr-Week',
        'last_7_days' => 'Fr-Last 7 Days',
        'last_30_days' => 'Fr-Last 30 Days',
        'season' => 'Fr-Season',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : player_news
    |--------------------------------------------------------------------------
    */
    'player_news' => [
        'title' => 'Fr-Player News',
        'my_team' => 'Fr-My Team',
        'league' => 'Fr-League',
        'read_more' => 'Fr-Read',
        'less' => 'Fr-Less',
        'messages' => [
            'no_team_news' => 'Fr-No team news present.',
            'no_league_news' => 'Fr-No league news present.',
        ], 
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : pool_name_header
    |--------------------------------------------------------------------------
    */
    'pool_name_header' => [
        'pool_stats' => 'Fr-Pool Stats',
        'pool_points' => 'Fr-Pool Points',
        'add' => 'Fr-Add',
        'trade' => 'Fr-Trade',
        'drop' => 'Fr-Drop',
        'activate' => 'Fr-Activate',
        'versus' => 'Fr-vs',
        'payment_alert1' => 'The pool is currently in trial mode and will expire in ',
        'payment_alert2' => '. Please complete your payment to activate the pool. ',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : nhl_headlines
    |--------------------------------------------------------------------------
    */
    'nhl_headlines' => [
        'title' => 'Fr-NHL Headlines',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : pool_nav
    |--------------------------------------------------------------------------
    */
    'pool_nav' => [
        'pool_menu' => 'Fr-Pool Menu',
        'pool' => 'Fr-Pool',
        'overview' => 'Fr-Overview',
        'transaction' => 'Fr-Transaction',
        'in_action' => 'Fr-In Action',
        'live_stats' => 'Fr-Live Stats',
        'draft_results' => 'Fr-Draft Results',
        'scores' => 'Fr-Scores',
        'teams' => 'Fr-Teams',
        'roster' => 'Fr-Roster',
        'trade' => 'Fr-Trade',
        'players' => 'Fr-Players',
        'player_list' => 'Fr-Player List',
        'league_leaders' => 'Fr-League Leaders',
        'injury_report' => 'Fr-Injury Report',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : team_records
    |--------------------------------------------------------------------------
    */
    'team_records' => [
        'team' => 'Fr-Team',
        'record' => 'Fr-Record',
        'messages' => [
            'no_team_records' => 'Fr-No team records present.',
        ], 
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : trial_period
    |--------------------------------------------------------------------------
    */
    'trial_period' => [
        'expired' => 'Fr-Trial Period Expired',
        'continue' => 'Fr-The trial period for your pool has expired.  To continue using this pool, complete your purchase to reactivate this pool.',
        'activate' => 'Fr-Activate',
        'contact' => 'Fr-The trial period for your pool has expired.  Please contact your
                                pool commissioner to get them to activate the pool.',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Partial : scoreboard
    |--------------------------------------------------------------------------
    */
    'scoreboard' => [
        'view_stats' => 'Fr-View Stats',
        'date' => 'Fr-Date',
        'view_live_stats' => 'Fr-View Live Stats',
        'et' => 'Fr-ET',
    ],
];
