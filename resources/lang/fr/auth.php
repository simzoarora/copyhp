<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => "Ces informations de connexion ne correspondent pas.",
    'general_error' => "Vous n'avez pas les droits requis pour cette action.",
    'socialite' => [
        'unacceptable' => 'Le login :provider est de type incorrect.',
    ],
    'throttle' => 'Vous avez effectué trop de tentatives de connexion. Veuillez ré-essayer dans :seconds secondes.',
    'unknown' => 'Une erreur inconnue a eu lieu.',
    
    /*
    |--------------------------------------------------------------------------
    | Route : editAccount
    |--------------------------------------------------------------------------
    */
    'editAccount' => [
        'title1' => 'Fr-Edit Account Info',
        'title2' => 'Fr-Change Password',
        'title3' => 'Fr-Email Notification Settings',
        'form_fields' => [
            'name' => 'Fr-Account Name',
            'email' => 'Fr-Email',
            'current_password' => 'Fr-Current Password',
            'new_password' => 'Fr-New Password',
            'retype_password' => 'Fr-Retype Password',
            'checkbox_1' => 'Fr-Weekly Standing Updates',
            'checkbox_2' => 'Fr-Trade Offers',
            'checkbox_3' => 'Fr-Injury/Roster Notifications',
            'checkbox_4' => 'Fr-Newsletter',
        ],
        'messages' => [
            'success' => 'Fr-Great! Your account info has been updated successfully.',
            'failure' => 'Fr-Wrong password.',
        ],
        'validations' => [
            'name' => 'Fr-A name is required.',
            'email' => 'Fr-An email address is required.',
            'password' => 'Fr-A password is required.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : my-account
    |--------------------------------------------------------------------------
    */
    'my_account' => [
        'my_pools' => 'Fr-My Pools',
        'current_pools' => 'Fr-Current Pools',
        'past_pools' => 'Fr-Past Pools',
        'no_pools' => [
            'text1' => 'Fr-Looks like you aren\'t in any pools. Don\'t worry, you can ',
            'text2' => 'Fr-create one!',
        ],
        'no_past_pools' => 'Fr-There are no past pools present.',
        'pool_stats' => 'Fr-Pool Stats',
        'pool_points' => 'Fr-Pool Points',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | login trans('auth.login.')
    |--------------------------------------------------------------------------
    */
    'login' => [
        'unknown_email' => 'Fr-Unknown Email.',
        'incorrect_password' => 'Fr-Password is incorrect.',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | changePasswordSubmit trans('auth.changePasswordSubmit.')
    |--------------------------------------------------------------------------
    */
    'changePasswordSubmit' => [
        'update_successful' => 'Fr-Great! Your account info has been updated successfully.',
        'wrong_password' => 'Fr-Wrong password.',
    ],
];
