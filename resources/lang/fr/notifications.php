<?php

return [
    'new_message_board' => 'Fr-A new topic has been posted on the message board for :Pool',
    'reply_to_post' => 'Fr-Someone replied to your post.',
    'trade_created' => 'Fr-A trade has been proposed to you by :Team, trade will automatically expire on :Expire',
    'missing_roster' => 'Fr-Your current roster has open positions available. Please adjust your roster and fill the empty spots',
    'injured_player' => 'Fr-:Player is injured and currently part of your active roster.',
    'trade_accepted' => 'Fr-The trade between you and :Team has been accepted and will be processed on :time',
    'trade_rejected' => 'Fr-Your trade to :Team has been rejected.',
    'trade_cancelled_by_parent' => 'Fr-The trade between you and :Team has been cancelled by :Team',
    'trade_expired' => 'Fr-Your trade proposed to :Team has expired.',
];

