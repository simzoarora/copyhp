<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Page Language Lines : French
    |--------------------------------------------------------------------------
    | The following language lines are used in home throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    */
    
    /*
    |--------------------------------------------------------------------------
    | Partial : home_logged
    |--------------------------------------------------------------------------
    */
    'home_logged' => [
        'no_pools_present1' => 'Fr-Looks like you aren\'t in any pools. Don\'t worry, you can ',
        'no_pools_present2' => ' Fr-create one!',
        'edit' => 'Fr-Edit',
        'versus' => 'Fr-vs',
        'not_finished' => 'Fr-(Pool creation not finished)',
        'continue' => 'Fr-Continue',
        'pool_stats' => 'Fr-Pool Stats',
        'pool_points' => 'Fr-Pool Points',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | twitterCommentsSubmit trans('home.twitterCommentsSubmit.')
    |--------------------------------------------------------------------------
    */
    'twitterCommentsSubmit' => [
        'invalid_tweet_error' => 'Fr-Invalid tweet.',
    ],
];    