<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pool Pages Language Lines : French
    |--------------------------------------------------------------------------
    | The following language lines are used in pools throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    */
    
    /*
    |--------------------------------------------------------------------------
    | 
    |--------------------------------------------------------------------------
    */
    'pool_dashboard' => [
        'versus' => 'Fr-vs',
        'standing' => 'Fr-Standing',
        'rank' => 'Fr-Rank',
        'team_name' => 'Fr-Team Name',
        'wlt' => 'Fr-W-L-T',
        'points' => 'Fr-Points',
        'last_week' => 'Fr-Last Week',
        'waiver' => 'Fr-Waiver',
        'moves' => 'Fr-Moves',
        'no_teams_present' => 'Fr-No teams present.',
        'league_feed' => 'Fr-League Feed',
        'type' => 'Fr-Type',
        'team' => 'Fr-Team',
        'details' => 'Fr-Details',
        'time' => 'Fr-Time',
        'add' => 'Fr-Add',
        'drop' => 'Fr-Drop',
        'trade' => 'Fr-Trade',
        'total_points' => 'Fr-Total Points',
        'messages' => [
            'no_pool_present' => 'Fr-No pool is present.',
            'invalid_pool' => 'Fr-Invalid pool.',
            'no_league_feed_present' => 'Fr-No league feed present.',
        ],
        'points'=>'Fr-Points'
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : pool/{id}/transaction
    |--------------------------------------------------------------------------
    */
    'transaction' => [
        'pool_transactions'=> 'Fr-Pool Transactions',
        'type'=> 'Fr-Type',
        'team'=> 'Fr-Team',
        'details'=> 'Fr-Details',
        'time'=> 'Fr-Time',
        'add'=> 'Fr-Add',
        'drop'=> 'Fr-Drop',
        'trade'=> 'Fr-Trade',
        'messages' => [
            'no_transactions' => 'Fr-No Transactions.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : pool/{id}/in_action_tonight
    |--------------------------------------------------------------------------
    */
    'in_action_tonight' => [
        'title' => 'Fr-In Action Tonight',
        'position' => 'Fr-Position',
        'player' => 'Fr-Player',
        'opponent' => 'Fr-Opponent',
        'pool_points' => 'Fr-Pool Points',
        'messages' => [
            'no_teams_present' => 'Fr-No teams are present in this pool.',
            'invalid_pool' => 'Fr-Invalid pool.',
            'no_player_present' => 'Fr-No player present.',
            'no_goalie_present' => 'Fr-No goalie present.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : pool/{id}/live_stats
    |--------------------------------------------------------------------------
    */
    'live_stats' => [
        'title' => 'Fr-Live Stats',
        'team' => 'Fr-Team',
        'name' => 'Fr-Name',
        'pool_points' => 'Fr-Pool Points ',
        'status' => 'Fr-Status',
        'pos' => 'Fr-Pos',
        'skaters' => 'Fr-Skaters',
        'pool_pts' => 'Fr-Pool Pts',
        'starting_lineup_totals' => 'Fr-Starting Lineup Totals',
        'goalie' => 'Fr-Goalie',
        'messages' => [
            'no_live_stats_prsent' => 'Fr-No live stats present.',
            'invalid_pool' => 'Fr-Invalid pool.',
            '' => '',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : pool/{id}/trade
    |--------------------------------------------------------------------------
    */
    'trades' => [
        'create_trade' => 'Fr-Create Trade',
        'propose_trade' => 'Fr-Propose Trade',
        'select_players_text' => 'Fr-Select the players you want to trade.  You must stay below the total roster size to be able to complete a trade.',
        'team' => 'Fr-Team',
        'players_traded' => 'Fr-Players Traded',
        'players_dropped' => 'Fr-Players Dropped',
        'roster_size' => 'Fr-Roaster Size',
        'after_trade' => 'Fr-After Trade',
        'accepted' => 'Fr-Accepted',
        'team_name' => 'Fr-Team Name',
        'dropped' => 'Fr-Dropped',
        'traded' => 'Fr-Traded',
        'trade' => 'Fr-Trade',
        'drop' => 'Fr-Drop',
        'status' => 'Fr-Status',
        'pos' => 'Fr-Pos',
        'skaters' => 'Fr-Skaters',
        'opp' => 'Fr-Opp',
        'pre' => 'Fr-Pre',
        'current' => 'Fr-Current',
        'own' => 'Fr-Own',
        'start' => 'Fr-Start',
        'pool_pts' => 'Fr-Pool Pts',
        'pre2' => 'Fr-pre',
        'messages' => [
            'no_player_present' => 'Fr-No player present.',
            'no_goalie_present' => 'Fr-No goalie present.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : pool/{pool_id}/player-stats
    |--------------------------------------------------------------------------
    */
    'player_stats' => [
        'select_position' => 'Fr-Select Position',
        'select_team' => 'Fr-Select Team',
        'select_season' => 'Fr-Select Season',
        'find_player' => 'Fr-Find Player',
        'add' => 'Fr-Add ',
        'team' => 'Fr-Team ',
        'pos' => 'Fr-Pos ',
        'skaters' => 'Fr-Skaters ',
        'current' => 'Fr-Current ',
        'own' => 'Fr-Own',
        'total_points' => 'Fr-Total Points ',
        'free_agent' => 'Fr-Free Agent',
        'messages' => [
            'no_results_found' => 'Fr-No results found.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : pool/{pool_id}/add-setup
    |--------------------------------------------------------------------------
    */
    'add_setup' => [
        'select_players_to_drop' => 'Fr-Select Players To Drop',
        'content' => 'Fr-Now that you have selected the players to be added to your team, you may have to drop some players to make room for these players.  
                        Choose the players from your team that you want to drop.  Your roster size must stay below the maximum for you to continue',
        'roster_position' => 'Fr-Roster Position',
        'total_available_spots' => 'Fr-Total Available Spots',
        'before_transaction' => 'Fr-Before Transaction',
        'after_transaction' => 'Fr-After Transaction',
        'no_roster_positions' => 'Fr-No Rosters Positions',
        'total_roster_size' => 'Fr-Total Roster Size',
        'review_transaction' => 'Fr-Review Transaction',
        'cancel_transaction' => 'Fr-Cancel Transaction',
        'transaction_details' => 'Fr-Transaction Details',
        'you_will_add' => 'Fr-You will add: ',
        'you_will_drop' => 'Fr-You will drop: ',
        'transaction_processed_on' => 'Fr-This transaction will be processed on ',
        'players_to_be_added' => 'Fr-Players to be added',
        'pos' => 'Fr-Pos',
        'skaters' => 'Fr-Skaters',
        'opp' => 'Fr-Opp',
        'pre' => 'Fr-Pre',
        'current' => 'Fr-Current',
        'own' => 'Fr-Own',
        'start' => 'Fr-Start',
        'pool_points' => 'Fr-Pool Pts',
        'my_team' => 'Fr-My Team',
        'select_players_to_drop' => 'Fr-Select players to drop from your team',
        'dropped' => 'Fr-Dropped',
        'traded' => 'Fr-Traded',
        'drop' => 'Fr-Drop',
        'roster_pos' => 'Fr-Roster Pos',
        'skater_pos' => 'Fr-Skater Pos',
        'status' => 'Fr-Status',
        'action_cant_be_undone' => 'Fr-This action can not be undone',
        'accept' => 'Fr-Accept',
        'cancel' => 'Fr-Cancel',
        'messages' => [
            'no_player_present' => 'Fr-No player present.',
            'no_goalie_present' => 'Fr-No goalie present.',
            'no_player_added' => 'Fr-No player added.',
            'no_player' => 'Fr-, No Player',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : pool/{pool_id}/drop-setup {{trans('pool.drop_setup.')}}
    |--------------------------------------------------------------------------
    */
    'drop_setup' => [
        'select_players_to_drop' => 'Fr-Select Players To Drop',
        'roster_position' => 'Fr-Roster Position',
        'total_available_spots' => 'Fr-Total Available Spots',
        'before_transaction' => 'Fr-Before Transaction',
        'after_transaction' => 'Fr-After Transaction',
        'no_roster_positions' => 'Fr-No Rosters Positions',
        'total_roster_size' => 'Fr-Total Roster Size ',
        'review_transaction' => 'Fr-Review Transaction',
        'cancel_transaction' => 'Fr-Cancel Transaction',
        'transaction_details' => 'Fr-Transaction Details',
        'you_will_drop' => 'Fr-You will drop: ',
        'transaction_processed_on' => 'Fr-This transaction will be processed on ',
        'select_players_to_drop' => 'Fr-Select players to drop from your team',
        'dropped' => 'Fr-Dropped',
        'traded' => 'Fr-Traded',
        'my_team' => 'Fr-My Team',
        'drop' => 'Fr-Drop',
        'roster_pos' => 'Fr-Roster Pos',
        'skater_pos' => 'Fr-Skater Pos',
        'status' => 'Fr-Status',
        'opp' => 'Fr-Opp',
        'pre' => 'Fr-Pre',
        'current' => 'Fr-Current',
        'own' => 'Fr-Own',
        'start' => 'Fr-Start',
        'pool_points' => 'Fr-Pool Pts',
        'pre' => 'Fr-pre',
        'action_cant_be_undone' => 'Fr-This action can not be undone',
        'accept' => 'Fr-Accept',
        'cancel' => 'Fr-Cancel',
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : pool/{pool_id}/league-leader {{trans('pool.league_leaders.')}} league_leaders.messages.
    |--------------------------------------------------------------------------
    */
    'league_leaders' => [
        'select_position' => 'Fr-Select Position',
        'total' => 'Fr-Total',
        'owner' => 'Fr-Owner',
        'player' => 'Fr-Player',
        'team' => 'Fr-Team',
        'position' => 'Fr-Position',
        'total_points' => 'Fr-Total Points',
        'messages' => [
            'no_teams_present' => 'Fr-No teams present.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : pool/{pool_id}/draft-result {{trans('pool.draft_result.')}} draft_result.messages.
    |--------------------------------------------------------------------------
    */
    'draft_result' => [
        'team' => 'Fr-TEAM ',
        'player' => 'Fr-PLAYER ',
        'pool_points' => 'Fr-Pool Points',
        'round_rank' => 'Fr-Round Rank',
        'pick' => 'Fr-Pick',
        'points' => 'Fr-Points',
        'messages' => [
            'no_player_present' => 'Fr-No player present.',
            'no_draft_result' => 'Fr-No draft result present.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : matchup/matchup-individual/{matchup_id} {{trans('pool.matchup_individual.')}} matchup_individual.messages.
    |--------------------------------------------------------------------------
    */
    'matchup_individual' => [
        'today' => 'Fr-Today',
        'week' => 'Fr-Week',
        'goal_tender_appearances' => 'Fr-Goal tender appearances 1/3',
        'versus' => 'Fr-vs',
        'skaters' => 'Fr-Skaters',
        'goalie' => 'Fr-Goalie',
        'messages' => [
            'no_player_present' => 'Fr-No player present.',
            'no_goalie_present' => 'Fr-No goalie present.',
            'no_results_found' => 'Fr-No results found.',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Route : pool/{pool_id}/box-draft {{trans('pool.box-draft.')}} box-draft.messages.
    |--------------------------------------------------------------------------
    */
    'box-draft'=>[
        'submit_picks'=>'Fr-Submit Picks',
        'clear_selections'=>'Fr-Clear Selections',
        'error'=>'Fr-Please check atleast one checkbox in '
    ],
    /*
      |--------------------------------------------------------------------------
      | Route : pool/{pool_id}
      |--------------------------------------------------------------------------
     */
    'pool_show' => [
        'pool_name' => 'Fr-Pool Name',
        'pool_format' => 'Fr-Pool Format',
        'season' => 'Fr-Season',
        'playoff_format' => 'Fr-Playoff Format',
        'lock_teams' => 'Fr-Lock Eliminated Teams',
        'no_of_teams' => 'Fr-Number of Teams',
        'start_week' => 'Fr-Start Week',
        'min_goalie' => 'Fr-Minimum Goalie Appearances',
        'max_add_season' => 'Fr-Maximum Adds per Season',
        'max_add_week' => 'Fr-Maximum Adds per Week',
        'max_trade' => 'Fr-Maximum Trades per Season',
        'trade_deadline' => 'Fr-Trade Deadline',
        'trade_reject' => 'Fr-Trade Reject Time',
        'waiver' => 'Fr-Waivers',
        'waiver_time' => 'Fr-Waiver Time',
        'draft_pick_trade' => 'Fr-Draft Pick Trades',
        'positions' => 'Fr-Positions',
        'point_categories' => 'Fr-Point Categories',
        'no' => 'Fr-No',
        'yes' => 'Fr-Yes',
        'invite_link' => 'Fr-Invite Link'
    ],
    
    'unauthorized_action' => 'Fr-Unauthorized action.',
    'development_error' => 'Fr-DEVELOPMENT ERROR',
    'pool_votes'=>[
        'commissioner_wait'=>'Fr-You have accepted the trade. Please wait for commissioner action.',
        'vote_wait'=>'Fr-You have accepted the trade. Please wait while voting is underway to complete the trade.',
        'vote_accept'=>'Fr-You voted to accept the trade.',
        'vote_reject'=>'Fr-You voted to reject the trade.',
    ]
];    