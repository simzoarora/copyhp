<?php

return [
    'subjects' => [
        'pool_team_invite' => 'Fr-You have been invited to join pool on HockeyPool.com',
        'pool_team_add' => 'Fr-You have been added to pool on HockeyPool.com',
        'draft_kit' => 'Fr-Thank You for purchasing Draft Kit.',
    ],
    'pool_added' => [
        'text1' => 'Fr-You have been invited to join ',
        'text2' => ' Fr-at',
        'join_now' => 'Fr-Join Now',
        'link_text' => 'Fr-or click/copy paste the link below:',
        'register_now' => 'Fr-Register Now',
        'pool_invitation' => 'Fr-Pool Invitation',
    ],
    'confirm' => [
        'title' => 'Fr-Verify Your Email',
        'thank_you' => 'Fr-Thank you for creating your account on ',
        'text1' => 'Fr-- Please verify your email to activate your account.',
        'verify_email_now' => 'Fr-Verify Email Now',
        'text2' => 'Fr-or click/copy paste the link below:',
    ],
    'passwords' => [
        'title' => 'Fr-Reset Your Password',
        'text1' => 'Fr-Please reset your password to access your account.',
        'reset_password_now' => 'Fr-Reset Password Now',
        'text2' => 'Fr-or click/copy paste the link below:',
    ],
    'draft_kit' => [
        'thank_you' => 'Fr-Thank You for purchasing Draft Kit. Please find it in attachment.',
        'header' => 'Fr-Draft Kit'
    ]
];

