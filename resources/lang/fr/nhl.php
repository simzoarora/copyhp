<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Nhl Pages Language Lines : French
    |--------------------------------------------------------------------------
    | The following language lines are used in pools throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    */
    
    /*
    |--------------------------------------------------------------------------
    | Route : /standings/{season_name?}
    |--------------------------------------------------------------------------
    */
    'standings' => [
        'title' => 'Fr-Standings',
        'division' => 'Fr-Division',
        'conference' => 'Fr-Conference',
        'overall' => 'Fr-Overall',
        'gp' => 'Fr-GP',
        'w' => 'Fr-W',
        'l' => 'Fr-L',
        'otl' => 'Fr-OTL',
        'sol' => 'Fr-SOL',
        'pts' => 'Fr-PTS',
        'gf' => 'Fr-GF',
        'ga' => 'Fr-GA',
        'diff' => 'Fr-DIFF',
        'home' => 'Fr-HOME',
        'away' => 'Fr-AWAY',
        'l10' => 'Fr-L10',
        'strk' => 'Fr-STRK',
        'messages' => [
            'no_results_found_for_selected_year' => 'Fr-No results found for the selected year.',
            'internal_server_error' => 'Fr-Internal Server Error.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : /schedule/{schedule_start_date?}
    |--------------------------------------------------------------------------
    */
    'schedule' => [
        'title' => 'Fr-Schedule',
        'matchup' => 'Fr-Matchup',
        'result' => 'Fr-Result',
        'time' => 'Fr-Time',
        'ot' => 'Fr-(OT)',
        'so' => 'Fr-(SO)',
        'messages' => [
            'no_schedule_present' => 'Fr-No schedule present.',
            'invalid_date_provided' => 'Fr-Invalid Date Provided',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : /player-stats?season_id={season_id}&game_type={regular}&position={position}&page={page_no}
    |--------------------------------------------------------------------------
    */
    'player_stats' => [
        'title' => 'Fr-Statistics',
        'view_by' => 'Fr-View Statistics By:',
        'players_position' => 'Fr-Player\'s Position',
        'seasons' => 'Fr-Seasons',
        'game_type' => 'Fr-Game Type',
        'team' => 'Fr-Team',
        'player' => 'Fr-Player',
        'pos' => 'Fr-Pos',
        'gp' => 'Fr-GP',
        'gs' => 'Fr-GS',
        'w' => 'Fr-W',
        'l' => 'Fr-L',
        't' => 'Fr-T',
        'otl' => 'Fr-OTL',
        'sol' => 'Fr-SOL',
        'sa' => 'Fr-SA',
        'svs' => 'Fr-Svs',
        'ga' => 'Fr-GA',
        'sv_cent' => 'Fr-Sv%',
        'gaa' => 'Fr-GAA',
        'toi' => 'Fr-TOI',
        'so' => 'Fr-SO',
        'g' => 'Fr-G',
        'a' => 'Fr-A',
        'p' => 'Fr-P',
        'pim' => 'Fr-PIM',
        'p_gp' => 'Fr-P/GP',
        'ppg' => 'Fr-PPG',
        'ppp' => 'Fr-PPP',
        'shg' => 'Fr-SHG',
        'shp' => 'Fr-SHP',
        'gwg' => 'Fr-GWG',
        'otg' => 'Fr-OTG',
        's' => 'Fr-S',
        's_cent' => 'Fr-S%',
        'toi_gp' => 'Fr-TOI/GP',
        'shifts_gp' => 'Fr-Shifts/GP',
        'fow_cent' => 'Fr-FOW%',
        'svs2' => 'Fr-SVS',
        'sv_cent2' => 'Fr-sv%',
        'messages' => [
            'no_players_found' => 'Fr-No players found for selected season.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : /injury-report   {{trans('nhl.injury_report.')}}   injury_report.messages.
    |--------------------------------------------------------------------------
    */
    'injury_report' => [
        'nhl_injury_report' => 'Fr-NHL Injury Report',
        'last_updated' => 'Fr-Last updated:',
        'first_report' => 'Fr-First report:',
        'messages' => [
            'no_results_found' => 'Fr-No results found.',
            'no_injury_report_present' => 'Fr-No injury report present.',
        ],
    ],
];    