<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Navs Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in menu items throughout the system.
      | Regardless where it is placed, a menu item can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'backend' => [
        'button' => 'Bouton',
    ],
    'general' => [
        'logout' => 'Déconnexion',
    ],
    'frontend' => [
        'dashboard' => 'Tableau de bord',
        'home' => 'Accueil',
        'login' => 'Connexion',
        'macros' => 'Macros',
        'register' => "S'enregistrer",
        'user' => [
            'administration' => 'Administration',
            'change_password' => 'Changer mon mot de passe',
            'my_information' => 'Mes informations',
        ],
        'livescoreboard' => [
            'yesterday' => 'Fr-Yesterday',
            'today' => 'Fr-Today',
            'tomorrow' => 'Fr-Tomorrow',
            'date' => 'Fr-May 16',
            'final' => 'Fr-Final',
        ],
        'home_nav' => [
            'my_pools' => 'Fr-My Pools',
            'nhl' => 'Fr-NHL',
            'stats' => 'Fr-Stats',
            'news' => 'Fr-News',
            'draft' => 'Fr-Draft',
            'tools' => 'Fr-Tools',
            'draft_kit' => 'Fr-Draft Kit',
            'custom_draft_kit' => 'Fr-Custom Draft Kit',
            'login' => 'Fr-Login',
            'login_button' => 'Fr-LOGIN',
            'email_address' => 'Fr-Email address...',
            'password' => 'Fr-Password...',
            'use_social_sign' => 'Fr-Or use your social sign in',
            'register' => 'Fr-Register',
            'register_button' => 'Fr-REGISTER',
            'my_account' => 'Fr-My Account',
            'edit_account' => 'Fr-Edit Account',
            'logout' => 'Fr-Logout',
            'messages' => 'Fr-Messages',
            'blog' => 'Fr-Blog',
            'notif' => 'Fr-Notif',
            'notifications' => 'Fr-Notifications',
            
            'submenus' => [
                'nhl' => [
                    'scores' => 'Fr-Scores',
                    'standings' => 'Fr-Standings',
                    'schedule' => 'Fr-Schedule',
                    'player_stats' => 'Fr-Player Stats',
                ],
            ],
        ],
        'footer' => [
            'how_it_works' => 'Fr-How it Works',
            'faq' => 'Fr-FAQ',
            'support' => 'Fr-Support',
            'contact' => 'Fr-Contact',
            'advertise' => 'Fr-Advertise',
            'terms_of_use' => 'Fr-Terms of Use',
            'privacy_policy' => 'Fr-Privacy Policy',
            'copyright' => 'Fr-All Rights Reserved',
        ],
        'upper_footer' => [
            'live_scores_stats' => 'Fr-LIVE ONLINE DRAFT',
            'live_scores_stats_p' => 'Fr-Not able to get together with your friends in person? Connect online from anywhere and do a fantasy draft in real-time.',
            'trash_talks' => 'Fr-TRASH TALK',
            'trash_talks_p' => 'Fr-Chirp like the pros with our brand new message boards. Live up your latest wins and defend your losses.',
            'manage_team' => 'Fr-MANAGE YOUR TEAM',
            'manage_team_p' => 'Fr-Set your lines and tinker with your roster. Or set it to auto and let us take care of you.',
        ],
    ],
];