<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pages Language Lines : French
    |--------------------------------------------------------------------------
    | The following language lines are used in home throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    */
    
    /*
    |--------------------------------------------------------------------------
    | Route : blog
    |--------------------------------------------------------------------------
    |
    */
    'blog' => [
        'title' => 'Fr-HockeyPool Blog',
        'page_header' => 'Blogs',
        'by' => 'Fr-By: ',
        'hockeypool_staff' => 'Fr-HockeyPool Staff',
        'no_blogs' => 'Fr-No Blogs.',
        'back_to_blog_home' => ' Fr-Back to Blog Home',
        'share_us_on' => 'Fr-Share Us On: ',
        'comments' => 'Fr-Comments',
        'comment' => 'Fr-Comment',
        'comments_placeholder' => 'Fr-Add a comment...',
        'published' => 'Fr-Published',
        'draft' => 'Fr-Draft',
        'no_blog_entries' => 'Fr-No Blog Entries.',
        
        'validations' => [
            'title' => 'Fr-A title is required',
            'body' => 'Fr-A blog description is required',
            'thumbnail_image' => 'Fr-A thumbnail image is required',
            'featured_image' => 'Fr-A featured image is required',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | blogCommentsSubmit trans('blog.blogCommentsSubmit.')
    |--------------------------------------------------------------------------
    |
    */
    'blogCommentsSubmit' => [
        'invalid_blog_error' => 'Fr-Invalid blog.',
    ],
];