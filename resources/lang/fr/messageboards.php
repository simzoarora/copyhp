<?php

return [
    'header'=>[
        'fan_forum'=>'Fr-Fr-Fan Forum',
        'new_topic'=>'Fr-New Topic',
        'topics'=>'Fr-Topics',
        'my_posts'=>'Fr-My Posts',
        'fan_forum'=>'Fr-Fan Forum',
        'inbox'=>'Fr-Inbox'
    ],
    'form'=>[
        'subject'=>'Fr-Subject',
        'type_message'=>'Fr-Type Message',
        'post'=>'Fr-Post'
    ],
    'subheader'=>[
        'title'=>'Fr-Title',
        'replies'=>'Fr-Replies',
        'latest_post'=>'Fr-Latest post'
    ],
    'extra'=>[
        'by'=>'Fr-by',
        'reply'=>'Fr-Reply',
        'show_less'=>'Fr-Show less',
        'show_more'=>'Fr-Show more',
        'sort'=>'Fr-Sort',
        'newest'=>'Fr-Newest',
        'oldest'=>'Fr-Oldest',
        'expand-all'=>'Fr-Expand All',
        'collapse-all'=>'Fr-Collapse All'
    ],
];

