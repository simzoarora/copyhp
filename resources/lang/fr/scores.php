<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Scores Page Language Lines : French
    |--------------------------------------------------------------------------
    | The following language lines are used in pools throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    */
    
    /*
    |--------------------------------------------------------------------------
    | Route : scores
    |--------------------------------------------------------------------------
    */
    'scores' => [
        'ot' => 'Fr-OT',
        'so' => 'Fr-SO',
        'box_score' => 'Fr-Box Score',
        'messages' => [
            'no_games_scheduled' => 'Fr-No games are scheduled for this date.',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Route : scores/{competition_id} -- game details
    |--------------------------------------------------------------------------
    */
    'game_details' => [
        'ot' => 'Fr-OT',
        'so' => 'Fr-SO',
        'scoring_summary' => 'Fr-Scoring Summary',
        'team' => 'Fr-Team',
        'score' => 'Fr-Score',
        'goal' => 'Fr-Goal',
        'assist' => 'Fr-Assist',
        'time' => 'Fr-Time',
        'no_scores' => 'Fr-No Scores',
        'first_period' => 'Fr-1st Period',
        'second_period' => 'Fr-2nd Period',
        'third_period' => 'Fr-3rd Period',
        'overtime' => 'Fr-Overtime',
        'shootouts' => 'Fr-Shootouts',
        'no_shootouts' => 'Fr-No Shootouts',
        'result' => 'Fr-Result',
        'player' => 'Fr-Player',
        'box_score' => 'Fr-Box Score',
        'sog' => 'Fr-SOG',
        'fo_cent' => 'Fr-FO%',
        'pp' => 'Fr-PP',
        'pim' => 'Fr-PIM',
        'hits' => 'Fr-HITS',
        'blks' => 'Fr-BLKS',
        'forwards' => 'Fr-Forwards',
        'g' => 'Fr-G',
        'a' => 'Fr-A',
        'p' => 'Fr-P',
        'toi' => 'Fr-TOI',
        'pp_toi' => 'Fr-PP TOI',
        'sh_toi' => 'Fr-SH TOI',
        'goalies' => 'Fr-Goalies',
        'messages' => [
            'no_player_present' => 'Fr-No player present.',
            'no_goalie_present' => 'Fr-No goalie present.',
        ],
    ],
];    