@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/pool_standing.css')) !!}
@stop
@section('content')
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">  
        @include('frontend.includes.pool_nav')
        <div class="pool--content">
            <div class="bg-trans" style="display: none"></div>
            @include('frontend.includes.pool_name_header')
            <div class="pool-standing-container">
                <!--slider-->
                <div class="matchup-individual-slider" id='slider-all'></div>
                <!--slider-ends-->
                <div class="pool-standing">
                    <h3 class="pool--main-heading">Standings</h3>
                    <table class="table light-grey-table" id="pool_standings_all">

                    </table>
                </div>
                <div class="adblocks">
                    @include('frontend.includes.adblocks')
                    @include('frontend.includes.team_records')
                    @include('frontend.includes.player_news')
                    @include('frontend.includes.adblocks')
                </div>
            </div>
        </div>
    </div>
</div>
<script id="pool_standings_template" type="text/html">
    <thead>
        <tr>
            <th>Standing</th>
            <th>Team</th>
            <%if(type!=2){ %>
            <th>Record </th>
            <% } %>
            <th class="<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %>" >Points </th>
        </tr>
    </thead>
    <%  _.each(teams, function(team,key){  %>
    <tr>
        <td><%= key+1 %></td>
        <td><%= team.name %></td>
        <%if(type!=2){ %>
        <td>
            <%if(team.pool_matchup_result!=null){ %>
            <%= team.pool_matchup_result.total_win %>-<%= team.pool_matchup_result.total_loss %>-<%= team.pool_matchup_result.total_tie %>
            <%} else{ %>
            0
            <% } %>
        </td>
        <% } %>
        <td class="<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %>" >
            <%if(team.pool_matchup_result!=null){ %>
            <%= team.pool_matchup_result.total_score %>
            <%} else if(team.standard_score !=null){ %>
            <%= team.standard_score.total_score %>
            <% }else{ %>
            0
            <% } %>
        </td>
    </tr>
    <% }) %>
</script>
@include('frontend.includes.pool_slider')
@stop 
@section('after-scripts-end')
<script>
    var url = '{{route('frontend.pool.getDataForStandings',$pool_id)}}',
            slider_url = '{{route('frontend.pool.getmatchupHeaderData', $pool_id)}}';
</script>
{!! Html::script(elixir('js/pool_standing.js')) !!}
@stop