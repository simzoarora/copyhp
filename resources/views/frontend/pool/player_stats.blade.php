@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/player_stats.css')) !!}
@stop
@section('content')
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row player-stats" id="pool--content">
        @include('frontend.includes.pool_nav') 
        <div class="player-container pool--content">
            <div class="bg-trans" style="display: none">
            </div>
            @include('frontend.includes.pool_name_header')
            <div class="team-position">
                <h3 class="pool--main-heading"> {{$pool_details->poolTeam->name}}
                    <?php if (config('pool.inverse_type.h2h') == $pool_details->pool_type) { ?>
                        (<?php echo isset($pool_details->poolTeam->poolMatchupResult->total_win) ? $pool_details->poolTeam->poolMatchupResult->total_win : 0; ?>-<?php echo isset($pool_details->poolTeam->poolMatchupResult->total_loss) ? $pool_details->poolTeam->poolMatchupResult->total_loss : 0; ?>-<?php echo isset($pool_details->poolTeam->poolMatchupResult->total_tie) ? $pool_details->poolTeam->poolMatchupResult->total_tie : 0; ?>) 
                    <?php } ?>
                    <span class="pool-name-right">{{$pool_details->name}}</span>
                </h3>
                <div class="white-small-select">
                    {{ Form::select('positions',[0=>trans('pool.player_stats.select_position')] + $positions ,isset($_GET["player_position_id"])?$_GET["player_position_id"]:null,['class' => 'select-field','id'=>'position']) }}
                    {{ Form::select('pool_teams', [0=>trans('pool.player_stats.select_team')] + $pool_teams, isset($_GET["team"])?$_GET["team"]:null, ['class' => 'select-field','id'=>'pool_teams']) }}
                </div>
                <input type="text" class="search-bar" id="search-input" value="<?php echo isset($_GET["player_name"]) ? $_GET["player_name"] : false; ?>" placeholder="{{trans('pool.player_stats.find_player')}}">
            </div>
            @include('frontend.includes.league_nav') 
            <div class="add-btn">
                <input type="hidden" name="new_added_players" value=""/>
                <button class="btn btn-backgreen" id="add-players-list-send">{{trans('pool.player_stats.add')}}<span data-count="0"></span></button>
            </div>
            <table class="table light-grey-table head-arrow flip-scroll-table" id="main-table">
                <thead>
                    <tr>
                        <th>{{trans('pool.player_stats.add')}}</th>
                        <?php if (config('pool.inverse_type.box') != $pool_details->pool_type) { ?>
                            <th data-sortname="pool_team">{{trans('Pool Team')}}<span></span></th>
                        <?php } ?>
                        <th data-sortname="position">{{trans('Pos')}}<span></span></th>
                        <th data-sortname="player">{{trans('Player Name')}}<span></span></th>
                        <th data-sortname="nba_team">{{trans('Team')}}<span></span></th>
                        <th data-sortname="stat" data-stat_order_key="current">{{trans('Pool Rank')}}<span></span></th>
                        <th>{{trans('Own %')}}</th>
                        <?php
                        foreach ($pool_details->poolScoreSettings as $key => $val) {
                            if ($val->type == config('pool.pool_scoring_field.inverse_type.player')) {
                                echo('<th class="data-player" data-sortname="stat" data-stat_order_key="' . $val->poolScoringField->id . '">' . $val->poolScoringField->stat . '<span><?span></th>');
                            }
                        }
                        foreach ($pool_details->poolScoreSettings as $key => $val) {
                            if ($val->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                                echo('<th class="data-goalie" data-sortname="stat" data-stat_order_key="' . $val->poolScoringField->id . '">' . $val->poolScoringField->stat . '<span><?span></th>');
                            }
                        }
                        ?>
                        <th data-sortname="stat" data-stat_order_key="points" class="total-pool-points">{{trans('pool.player_stats.total_points')}}<span></span></th>
                    </tr>
                </thead>
                <tbody id="player-stats-all">
                </tbody>
            </table>
            <div class="post-pagination" style="display:none;">
                <div class="pagination-child">
                    <div id="pagination-first" class="pagination-arrows">
                        <div class="double-arrow"></div>
                    </div>
                    <div id="pagination-left" class="pagination-arrows"></div>
                    <div class="main-pagination">
                        <div id="page-list">

                        </div>
                    </div>
                    <div id="pagination-right" class="pagination-arrows"></div>
                    <div id="pagination-last" class="pagination-arrows">
                        <div class="double-arrow"></div>
                    </div>
                </div>
            </div>
            <!-- Commented because we are hiding advertisement           
            <div class="row">
                            <div class="ad-3-blocks">
                                <div class="advert">
                                    <img src="{{url('')}}/img/ad_sidebar.png" class="img-responsive width100" alt="">
                                </div>
                                <div class="advert">
                                    <img src="{{url('')}}/img/ad_sidebar.png" class="img-responsive width100" alt="">
                                </div>
                                <div class="advert">
                                    <img src="{{url('')}}/img/ad_sidebar.png" class="img-responsive width100" alt="">
                                </div>
                            </div>
                        </div>-->

        </div>
    </div>
</div>
<script id="player-stats-template" type="text/html">
    <%  _.each(players, function(p){  
    //checking if player is deleted
    var cantAddPlayer = false;
    if(p.deleted_at != null){
    cantAddPlayer = true;
    }
    //checking if player is dropped or deleted by one other parameter
    if(!$.isEmptyObject(p.pool_trade_player)){
    if(p.pool_trade_player.type == tradePlayersType.trade_players){
    cantAddPlayer = true;
    }
    else if(p.pool_trade_player.type == tradePlayersType.drop_players){
    cantAddPlayer = true;
    }
    }
    %>
    <tr>
        <td>
            <% if(cantAddPlayer == false){
            if(p.waiver != null || p.free_agent===true){ %>
            <div class="custom-checkbox"> 
                <input type="checkbox" value="None" id="squaredThree<%= p.player.id %>"/>
                <label for="squaredThree<%= p.player.id %>"></label>
            </div>
            <% } } %>
        </td>
        <?php if (config('pool.inverse_type.box') != $pool_details->pool_type) { ?>
            <td>
                <% if(p.waiver != null){ %>
                <%= p.waiver %>
                <input type="hidden" name="player_id" value="<%= p.id %>"/>
                <% } else if(p.free_agent===true){%>
                {{trans('pool.player_stats.free_agent')}}
                <input type="hidden" name="player_id" value="<%= p.id %>"/>
                <% } else{ %>
                <%= p.pool_team_player.pool_team.name %>
                <% }%>
            </td>
        <?php } ?>
        <td><%= p.player_position.short_name %></td>
        <td><%= p.player.full_name %></td>
        <td><img class="team-logo" src="/img/nhl_logos/<%= p.team.logo %>"><%= p.team.short_name %></td>
        <td>
            <% if(p.player.pool_season_rank!=null){%>
            <%= p.player.pool_season_rank.rank %>
            <% } else{%>
            0
            <% }%>
        </td>
        <td><%= p.own %>%</td>
        <?php
        foreach ($pool_details->poolScoreSettings as $key => $val) {
            if ($val->type == config('pool.pool_scoring_field.inverse_type.player')) {
                ?>
                <td class="data-player">
                    <% if(p.player.player_stat[<?php echo $val->poolScoringField->id; ?>]==null){%>
                    0
                    <% } else{ %>
                    <%= hpApp.twoDecimal(p.player.player_stat[<?php echo $val->poolScoringField->id; ?>]) %>
                    <% }%>
                </td>
                <?php
            }
        }
        foreach ($pool_details->poolScoreSettings as $key => $val) {
            if ($val->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                ?>
                <td class="data-goalie">
                    <% if(p.player.player_stat[<?php echo $val->poolScoringField->id; ?>]==null){%>
                    0
                    <% } else{ %>
                    <%= hpApp.twoDecimal(p.player.player_stat[<?php echo $val->poolScoringField->id; ?>]) %>
                    <% }%>
                </td>
                <?php
            }
        }
        ?>
        <td class="total-pool-points">
            <%if(p.total_points){ %>
            <%= hpApp.twoDecimal(p.total_points) %>
            <% } else{%>
            0
            <% }%>
        </td>
    </tr>
    <% }) %>
</script>
@stop 
@section('after-scripts-end')
<script>
    var url = '{{ route('frontend.pool.getDataForPlayerStats', $pool_id) }}',
            addSetupUrl = '{{ route('frontend.pool.addSetup', $pool_id) }}',
            pool_types =<?php echo json_encode(config('pool.inverse_type')); ?>,
            inverse_player_position =<?php echo json_encode(config('pool.inverse_player_position')); ?>,
            order_type = "<?php echo isset($_GET["order_type"]) ? $_GET["order_type"] : false; ?>",
            order = "<?php echo isset($_GET["order"]) ? $_GET["order"] : false; ?>",
            stat_order_key = "<?php echo isset($_GET["stat_order_key"]) ? $_GET["stat_order_key"] : false; ?>",
            page = "<?php echo isset($_GET["page"]) ? $_GET["page"] : false; ?>",
            start_date = "<?php echo isset($_GET["start_date"]) ? $_GET["start_date"] : false; ?>",
            end_date = "<?php echo isset($_GET["end_date"]) ? $_GET["end_date"] : false; ?>",
            data_retrieval = "<?php echo isset($_GET["data_retrieval"]) ? $_GET["data_retrieval"] : false; ?>",
            season_id = "<?php echo isset($_GET["season_id"]) ? $_GET["season_id"] : false; ?>",
            player_stats = <?php echo json_encode(trans('pool.player_stats')); ?>;
</script>
{!! Html::script(elixir('js/player_stats.js')) !!}
@stop