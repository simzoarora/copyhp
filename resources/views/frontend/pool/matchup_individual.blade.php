@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/matchup_individual.css')) !!}
@stop 
@section('content')
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">
        @include('frontend.includes.pool_nav')
        <div class="pool--content">
            <div class="bg-trans" style="display: none"></div>
            @include('frontend.includes.pool_name_header')
            <div class="matchup-individual-container">
                <!--slider-->
                <div class="matchup-individual-slider" id='slider-all'></div>
                <!--slider-ends-->
                <!--pool_h2h-->
                <div class="pools--h2h" id="pool-h2h-all"></div>
                <!--pool_h2h-ends-->
                <!--table1-->
                <div id='team-table-all'>
                </div>
                <!--table1-ends-->
                <!--matchup-today/week-tabs-->
                <div class="matchup--day">
                    <ul class="list-inline">
                        <li class="active" data-type="1">
                            <a href="#">{{trans('pool.matchup_individual.today')}}</a>
                            <span class="bottom-shadow"></span>
                        </li>
                        <li data-type="0">
                            <a href="#">{{trans('pool.matchup_individual.week')}}</a>
                            <span class="bottom-shadow"></span>
                        </li>
                    </ul>
                    <!--matchup-today/week-tabs-->
                    <div class="matchup--today" id="matchup-all">
                        <div class="table-matchup">
                            <p>{{trans('pool.matchup_individual.goal_tender_appearances')}}</p>
                        </div>
                        <div class="table-matchup">
                            <p>{{trans('pool.matchup_individual.goal_tender_appearances')}}</p>
                        </div>
                    </div>
                </div>
                <div class="three-ad-blocks">
                    <div class="row">
                        <div class="block-outer">
                            @include('frontend.includes.adblocks')
                        </div>
                        <div class="block-outer">
                            @include('frontend.includes.adblocks')
                        </div>
                        <div class="block-outer">
                            @include('frontend.includes.adblocks')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--pools--h2h template-->
<script id="pool-h2h-template" type="text/html">
    <div class="upper">
        <div class="right">
            <div class="img-div">
                <img src="<%= api_url %>{{ config('pool.paths.team_logo') }}<%= team1.logo %>" alt="" class="img-responsive"/>
            </div>
            <div class="name-div">
                <p><%= team1.name %>
                    <span class="account_name"><%= team1.user ? team1.user.email:'-'%></span>
                    <span>
                        <% if(!$.isEmptyObject(team1.pool_matchup_result)){ %>
                        <%= team1.pool_matchup_result.total_win %>-<%= team1.pool_matchup_result.total_loss %>-<%= team1.pool_matchup_result.total_tie %>
                        <% } %>
                    </span>
                </p>

            </div>
            <div class="goal-div">
                <p>
                    <% if (!$.isEmptyObject(matchup_results)) { %>
                    <% if (matchup_results[0].pool_team_id == team1.id) { %>
                    <%= matchup_results[0].win %>
                    <% } else { %>
                    <%= matchup_results[1].win %>
                    <% } %>
                    <% } else { %>
                    0
                    <% } %>
                </p> 
            </div>
        </div>
        <div class="vs">
            <p>{{trans('pool.matchup_individual.versus')}}</p>
        </div>
        <div class="left">
            <div class="img-div">
                <img src="<%= api_url %>{{ config('pool.paths.team_logo') }}<%= team2.logo %>" alt="" class="img-responsive"/>
            </div>
            <div class="name-div">
                <p><%= team2.name %>
                    <span class="account_name"><%= team2.user ? team2.user.email:'-'%></span>
                    <span>
                        <% if(!$.isEmptyObject(team2.pool_matchup_result)){ %>
                        <%= team2.pool_matchup_result.total_win %>-<%= team2.pool_matchup_result.total_loss %>-<%= team2.pool_matchup_result.total_tie %>
                        <% } %>
                    </span>
                </p>
            </div>
            <div class="goal-div">
                <p>
                    <% if (!$.isEmptyObject(matchup_results)) { %>
                    <% if (matchup_results[0].pool_team_id == team2.id) { %>
                    <%= matchup_results[0].win %>
                    <% } else { %>
                    <%= matchup_results[1].win %>
                    <% } %>
                    <% } else { %>
                    0
                    <% } %>
                </p> 
            </div>
        </div>
    </div>
</script> 
<!--same template for mobile-->
<script id="pool-h2h-template-mobile" type="text/html">
    <div class="team-scores-mobile"> 
        <div class="left-team">
            <div class="team-name">
                <p><%= team1.name %></p>
                <img src="<%= api_url %>{{ config('pool.paths.team_logo') }}<%= team1.logo %>" alt="" class="img-responsive"/>
            </div>
            <div class="team-score">
                <p>
                    <% if(!$.isEmptyObject(team1.pool_matchup_result)){ %>
                    <%= team1.pool_matchup_result.total_score %>
                    <% }else{ %>
                    0
                    <% } %>
                </p>
            </div>
        </div>
        <div class="right-team">
            <div class="team-score">
                <p>
                    <% if(!$.isEmptyObject(team2.pool_matchup_result)){ %>
                    <%= team2.pool_matchup_result.total_score %>
                    <% }else{ %>
                    0
                    <% } %>
                </p>
            </div>
            <div class="team-name">
                <p><%= team2.name %></p>
                <img src="<%= api_url %>{{ config('pool.paths.team_logo') }}<%= team2.logo %>" alt="" class="img-responsive"/>
            </div>
        </div>
    </div>
</script>
<!--end same template for mobile-->
<script id="team-table-template" type="text/html">
    <table class="table light-grey-table" id="team-table-outer">
        <thead>
            <tr class="teamPlayerHeading">
                <th></th>
                <th colspan="1">Players</th>
                <th colspan="1">Goalies</th>
                <th></th>
            </tr>
            <tr>
                <th></th>
                <?php
                foreach ($matchup_details->pool->poolScoreSettings as $val) {
                    if ($val->type == 1) {
                        $add_team_data_type = 'teamPlayerData';
                    } else if ($val->type == 2) {
                        $add_team_data_type = 'teamGoalieData';
                    }
                    ?>
                    <th data-id="<?php echo $val->poolScoringField->id; ?>" class="<?php echo $add_team_data_type; ?>"><?php echo $val->poolScoringField->stat; ?></th>
                <?php }
                ?>
                <th>TOT</th>
            </tr>
        </thead>
        <tbody id="team-table">
            <tr>
                <% var totValue = 0; %>
                <td><%= team1.name%></td>
                <?php foreach ($matchup_details->pool->poolScoreSettings as $val) { ?>
                    <td>
                        <% var found=false;%>
                        <% _.each(team1_data,function(v){ 
                        if(v.pool_score_setting.pool_scoring_field_id == <?php echo $val->poolScoringField->id; ?>){%>
                        <% if(cat_result[v.pool_score_setting_id].win == v.pool_team_id){%>
                        <b>
                            <% }%>
                            <%= hpApp.twoDecimal(v.total_stat) %>
                            <% totValue = parseFloat(totValue) + parseFloat(v.total_value); %>
                            <% if(cat_result[v.pool_score_setting_id].win == v.pool_team_id){%>
                        </b>
                        <% }%>
                        <% found=true; }
                        }) 
                        if(!found){
                        %>
                        0
                        <% } %>
                    </td>
                <?php } ?>
                <td><%= hpApp.twoDecimal(totValue) %></td>
            </tr>
            <tr>
                <% var totValue2 = 0; %>
                <td><%= team2.name%></td>
                <?php foreach ($matchup_details->pool->poolScoreSettings as $val) { ?>
                    <td>
                        <% var found=false;%>
                        <% _.each(team2_data,function(v){ 
                        if(v.pool_score_setting.pool_scoring_field_id == <?php echo $val->poolScoringField->id; ?>){%>
                        <% if(cat_result[v.pool_score_setting_id].win == v.pool_team_id){%>
                        <b>
                            <% }%>
                            <%= hpApp.twoDecimal(v.total_stat) %>
                            <% totValue2 = parseFloat(totValue2) + parseFloat(v.total_value); %>
                            <% if(cat_result[v.pool_score_setting_id].win == v.pool_team_id){%>
                        </b>
                        <% }%>
                        <% found=true; }
                        }) 
                        if(!found){
                        %>
                        0
                        <% } %>
                    </td>
                <?php } ?>
                <td><%= hpApp.twoDecimal(totValue2) %></td>
            </tr>
        </tbody>
    </table>
</script>
<script id="team-table-template-mobile" type="text/html">
    <div class="team-table-outer-mobile">
        <div class="team-name">
            <p><%= team1.name%></p>
            <p><%= team2.name%></p>
        </div>
        <div class='team-score'>
            <table class='table table-bordered'>
                <thead>
                    <tr>
                        <?php foreach ($matchup_details->pool->poolScoreSettings as $val) { ?>
                            <th data-id="<?php echo $val->poolScoringField->id; ?>"><?php echo $val->poolScoringField->stat; ?></th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php foreach ($matchup_details->pool->poolScoreSettings as $val) { ?>
                            <td>
                                <% var found=false;%>
                                <% _.each(team2_data,function(v){ 
                                if(v.pool_score_setting.pool_scoring_field_id == <?php echo $val->poolScoringField->id; ?>){%>
                                <% if(cat_result[v.pool_score_setting_id].win == v.pool_team_id){%>
                                <b>
                                    <% }%>
                                    <%= hpApp.twoDecimal(v.total_stat) %>
                                    <% if(cat_result[v.pool_score_setting_id].win == v.pool_team_id){%>
                                </b>
                                <% }%>
                                <% found=true; }
                                }) 
                                if(!found){
                                %>
                                0
                                <% } %>
                            </td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <?php foreach ($matchup_details->pool->poolScoreSettings as $val) { ?>
                            <td>
                                <% var found=false;%>
                                <% _.each(team1_data,function(v){ 
                                if(v.pool_score_setting.pool_scoring_field_id == <?php echo $val->poolScoringField->id; ?>){%>
                                <% if(cat_result[v.pool_score_setting_id].win == v.pool_team_id){%>
                                <b>
                                    <% }%>
                                    <%= hpApp.twoDecimal(v.total_stat) %>
                                    <% if(cat_result[v.pool_score_setting_id].win == v.pool_team_id){%>
                                </b>
                                <% }%>
                                <% found=true; }
                                }) 
                                if(!found){
                                %>
                                0
                                <% } %>
                            </td>
                        <?php } ?>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class='arrow-right'>
            <span></span>
        </div>
    </div>
</script>
<!--END pools--h2h template-->
<script id="matchup-table-template" type="text/html">
    <div class="table-matchup">
        <table class="table light-grey-table matchup-players">
            <thead>
                <tr>
                    <th>{{trans('pool.matchup_individual.skaters')}}</th>
                    <?php
                    foreach ($matchup_details->pool->poolScoreSettings as $val) {
                        if ($val->type == config('pool.pool_scoring_field.inverse_type.player')) {
                            ?>
                            <th><?php echo $val->poolScoringField->stat; ?></th>
                            <?php
                        }
                    }
                    ?>
                    <th class="total_points" style="display:none">{{trans('pool.matchup_individual.points')}}</th>
                </tr>
            </thead>
            <tbody>
                <% _.each(team1.pool_team_players,function(p){ %>
                <% if(p.player_season.player_position.id!=<?php echo config('pool.inverse_player_position.g'); ?>) {%>
                <tr>
                    <td><%= p.player_season.player.full_name %></td>
                    <?php
                    foreach ($matchup_details->pool->poolScoreSettings as $val) {
                        if ($val->type == config('pool.pool_scoring_field.inverse_type.player')) {
                            ?>
                            <td>
                                <% var found=false,total_points=0;%>
                                <% _.each(p.pool_h2h_scores,function(v){ 
                                if(v.pool_score_setting.pool_scoring_field_id == <?php echo $val->poolScoringField->id; ?>){ %>
                                <%= hpApp.twoDecimal(v.total_stat) %>
                                <% found=true; 
                                }
                                if(v.total_value){
                                total_points=total_points+parseInt(v.total_value);
                                }
                                }) 
                                if(!found){
                                %>
                                0
                                <% } %>
                            </td>
                            <?php
                        }
                    }
                    ?>
                    <% if(typeof total_points == 'undefined') total_points = 0; %>
                    <td class="total_points" style="display: none;"><%=total_points%></td>
                </tr>
                <% } }) %>
            </tbody>
        </table>
    </div>
    <div class="table-matchup">
        <table class="table light-grey-table matchup-players2">
            <thead>
                <tr>
                    <th>{{trans('pool.matchup_individual.skaters')}}</th>
                    <?php
                    foreach ($matchup_details->pool->poolScoreSettings as $val) {
                        if ($val->type == config('pool.pool_scoring_field.inverse_type.player')) {
                            ?>
                            <th><?php echo $val->poolScoringField->stat; ?></th>
                            <?php
                        }
                    }
                    ?>
                    <th class="total_points" style="display:none">{{trans('pool.matchup_individual.points')}}</th>
                </tr>
            </thead>
            <tbody>
                <% _.each(team2.pool_team_players,function(p){%>
                <% if(p.player_season.player_position.id!=<?php echo config('pool.inverse_player_position.g'); ?>) {%>
                <tr>
                    <td><%= p.player_season.player.full_name %></td>
                    <?php
                    foreach ($matchup_details->pool->poolScoreSettings as $val) {
                        if ($val->type == config('pool.pool_scoring_field.inverse_type.player')) {
                            ?>
                            <td>
                                <% var found=false,total_points=0;%>
                                <% _.each(p.pool_h2h_scores,function(v){ 
                                if(v.pool_score_setting.pool_scoring_field_id == <?php echo $val->poolScoringField->id; ?>){ %>
                                <%= hpApp.twoDecimal(v.total_stat) %>
                                <% found=true; }
                                if(v.total_value){
                                total_points=total_points+parseInt(v.total_value);
                                }
                                }) 
                                if(!found){
                                %>
                                0
                                <% } %>
                            </td>
                            <?php
                        }
                    }
                    ?>
                    <% if(typeof total_points == 'undefined') total_points = 0; %>
                    <td class="total_points" style="display: none;"><%=total_points%></td>
                </tr>
                <% } }) %>
            </tbody>
        </table>
    </div>
    <div class='goalies'>
        <table class="table light-grey-table matchup-goalies">
            <thead>
                <tr>
                    <th>{{trans('pool.matchup_individual.goalie')}}</th>
                    <?php
                    foreach ($matchup_details->pool->poolScoreSettings as $val) {
                        if ($val->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                            ?>
                            <th><?php echo $val->poolScoringField->stat; ?></th>
                            <?php
                        }
                    }
                    ?>
                    <th class="total_points" style="display:none">{{trans('pool.matchup_individual.points')}}</th>
                </tr>
            </thead>
            <tbody>
                <% _.each(team1.pool_team_players,function(p){ %>
                <% if(p.player_season.player_position.id==<?php echo config('pool.inverse_player_position.g'); ?>) {%>
                <tr>
                    <td><%= p.player_season.player.full_name %></td>
                    <?php
                    foreach ($matchup_details->pool->poolScoreSettings as $val) {
                        if ($val->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                            ?>
                            <td>
                                <% var found=false,total_points=0;%>
                                <% _.each(p.pool_h2h_scores,function(v){ 
                                if(v.pool_score_setting.pool_scoring_field_id == <?php echo $val->poolScoringField->id; ?>){ %>
                                <%= hpApp.twoDecimal(v.total_stat) %>
                                <% found=true; }
                                if(v.total_value){
                                total_points=total_points+parseInt(v.total_value);
                                }
                                }) 
                                if(!found){
                                %>
                                0
                                <% } %>
                            </td>
                            <?php
                        }
                    }
                    ?>
                    <% if(typeof total_points == 'undefined') total_points = 0; %>
                    <td class="total_points" style="display:none"><%=total_points%></td>
                </tr>
                <% } }) %>
            </tbody>
        </table>
        <table class="table light-grey-table matchup-goalies2">
            <thead>
                <tr>
                    <th>{{trans('pool.matchup_individual.goalie')}}</th>
                    <?php
                    foreach ($matchup_details->pool->poolScoreSettings as $val) {
                        if ($val->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                            ?>
                            <th><?php echo $val->poolScoringField->stat; ?></th>
                            <?php
                        }
                    }
                    ?>
                    <th class="total_points" style="display:none">{{trans('pool.matchup_individual.points')}}</th>
                </tr>
            </thead>
            <tbody>
                <% _.each(team2.pool_team_players,function(p){ %>
                <% if(p.player_season.player_position.id==<?php echo config('pool.inverse_player_position.g'); ?>) {%>
                <tr>
                    <td><%= p.player_season.player.full_name %></td>
                    <?php
                    foreach ($matchup_details->pool->poolScoreSettings as $val) {
                        if ($val->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                            ?>
                            <td>
                                <% var found=false,total_points=0;%>
                                <% _.each(p.pool_h2h_scores,function(v){ 
                                if(v.pool_score_setting.pool_scoring_field_id == <?php echo $val->poolScoringField->id; ?>){ %>
                                <%= hpApp.twoDecimal(v.total_stat) %>
                                <% found=true; }
                                if(v.total_value){
                                total_points=total_points+parseInt(v.total_value);
                                }
                                }) 
                                if(!found){
                                %>
                                0
                                <% } %>
                            </td>
                            <?php
                        }
                    }
                    ?>
                    <% if(typeof total_points == 'undefined') total_points = 0; %>
                    <td class="total_points" style="display:none"><%=total_points%></td>
                </tr>
                <% } }) %>
            </tbody>
        </table>
    </div>
</script>
<script id="matchup-table-template-mobile" type="text/html">
    <div class='matchup--today-mobile'>
        <div class='left-team'>
            <% _.each(team1.pool_team_players,function(p){%>
            <% if(p.player_season.player_position.id!=<?php echo config('pool.inverse_player_position.g'); ?>) {%>
            <div class='player-details'>
                <p class='player-name'><%= p.player_season.player.full_name %></p>
                <p>
                    <?php
                    foreach ($matchup_details->pool->poolScoreSettings as $key => $val) {
                        if ($val->type == config('pool.pool_scoring_field.inverse_type.player')) {
                            ?>
                            <span><% var found=false,total_points=0;%>
                                <% _.each(p.pool_h2h_scores,function(v){ 
                                if(v.pool_score_setting.pool_scoring_field_id == <?php echo $val->poolScoringField->id; ?>){ %>
                                <%= hpApp.twoDecimal(v.total_stat) %><?php echo $val->poolScoringField->stat; ?>
                                <% found=true; }
                                if(v.total_value){
                                total_points=total_points+parseInt(v.total_value);
                                }
                                }) 
                                if(!found){
                                %>
                                0<?php echo $val->poolScoringField->stat; ?><% } %></span>
                            <?php
                        }
                    }
                    ?>
                </p>
            </div>
            <% } }) %>
            <% _.each(team1.pool_team_players,function(p){ 
            if(p.player_season.player_position.id==<?php echo config('pool.inverse_player_position.g'); ?>) { %>
            <div class='player-details'>
                <p class='player-name'><%= p.player_season.player.full_name %></p>
                <p>
                    <?php
                    foreach ($matchup_details->pool->poolScoreSettings as $val) {
                        if ($val->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                            ?>
                            <span><% var found=false,total_points=0;%>
                                <% _.each(p.pool_h2h_scores,function(v){ 
                                if(v.pool_score_setting.pool_scoring_field_id == <?php echo $val->poolScoringField->id; ?>){ %>
                                <%= hpApp.twoDecimal(v.total_stat) %><?php echo $val->poolScoringField->stat; ?>
                                <% found=true; }
                                if(v.total_value){
                                total_points=total_points+parseInt(v.total_value);
                                }
                                }) 
                                if(!found){
                                %>
                                0<?php echo $val->poolScoringField->stat; ?><% } %></span>
                            <?php
                        }
                    }
                    ?>
                </p>
            </div>
            <% } }) %>
        </div>
        <div class='center-div'>
            <% _.each(team1.pool_team_players,function(p){%>
            <% if(p.player_season.player_position.id!=<?php echo config('pool.inverse_player_position.g'); ?>) {%>
            <p>F</p>
            <% } }) %>
        </div>
        <div class='right-team'>
            <% _.each(team2.pool_team_players,function(p){%>
            <% if(p.player_season.player_position.id!=<?php echo config('pool.inverse_player_position.g'); ?>) {%>
            <div class='player-details'>
                <p class='player-name'><%= p.player_season.player.full_name %></p>
                <p>
                    <?php
                    foreach ($matchup_details->pool->poolScoreSettings as $val) {
                        if ($val->type == config('pool.pool_scoring_field.inverse_type.player')) {
                            ?>
                            <span><% var found=false,total_points=0;%>
                                <% _.each(p.pool_h2h_scores,function(v){ 
                                if(v.pool_score_setting.pool_scoring_field_id == <?php echo $val->poolScoringField->id; ?>){ %>
                                <%= hpApp.twoDecimal(v.total_stat) %><?php echo $val->poolScoringField->stat; ?>f
                                <% found=true; }
                                if(v.total_value){
                                total_points=total_points+parseInt(v.total_value);
                                }
                                }) 
                                if(!found){
                                %>
                                0<?php echo $val->poolScoringField->stat; ?><% } %></span>
                            <?php
                        }
                    }
                    ?>
                </p>
            </div>
            <% } }) %>
            <% _.each(team2.pool_team_players,function(p){ 
            if(p.player_season.player_position.id==<?php echo config('pool.inverse_player_position.g'); ?>) { %>
            <div class='player-details'>
                <p class='player-name'><%= p.player_season.player.full_name %></p>
                <p>
                    <?php
                    foreach ($matchup_details->pool->poolScoreSettings as $val) {
                        if ($val->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                            ?>
                            <span><% var found=false,total_points=0;%>
                                <% _.each(p.pool_h2h_scores,function(v){ 
                                if(v.pool_score_setting.pool_scoring_field_id == <?php echo $val->poolScoringField->id; ?>){ %>
                                <%= hpApp.twoDecimal(v.total_stat) %><?php echo $val->poolScoringField->stat; ?>f
                                <% found=true; }
                                if(v.total_value){
                                total_points=total_points+parseInt(v.total_value);
                                }
                                }) 
                                if(!found){
                                %>0<?php echo $val->poolScoringField->stat; ?><% } %></span>
                            <?php
                        }
                    }
                    ?>
                </p>
            </div>
            <% } }) %>
        </div>
    </script>

    <!--slider--> 
    @include('frontend.includes.pool_slider')
    <!--end slider--> 
    @stop 
    @section('after-scripts-end')
    <script>
        var url = '{{route('frontend.pool.getDataForMatchupIndividual', $matchup_id)}}',
                slider_url = '{{route('frontend.pool.getmatchupHeaderData', $pool_id)}}',
                matchup_individual = <?php echo json_encode(trans('pool.matchup_individual')); ?>,
                pool_type = '{{$matchup_details->pool->poolSetting->poolsetting->pool_format}}',
                pool_format = <?php echo json_encode(config('pool.inverse_format')) ?>;
    </script>
    {!! Html::script(elixir('js/matchup_individual.js')) !!}
    @stop 