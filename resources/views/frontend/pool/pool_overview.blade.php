@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/pool_overview.css')) !!} 
@stop
@section('content')
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">
        @include('frontend.includes.pool_nav')
        <div class="pool--content">
            <div class="bg-trans" style="display: none">
            </div>
            <?php $dashboardPage = true; ?>
            @include('frontend.includes.pool_name_header')

            <div class="pool-overview-container">
                <div class="pool-overview">

                    <a href="{{route('frontend.pool.matchupIndividual', 0)}}" id="pool-matchup-link" class="hide"></a>
                    <a href="{{route('frontend.pool.myTeamWithTeam', ['pool_id', 'team_id'])}}" id="myteam-team-link" class="hide"></a>

                    <script id="pools-h2h-template" type="text/html">
                        <div class="pools--h2h">
                            <div class="week">
                                <% if(!$.isEmptyObject(poolMatchups)){ %>
                                <%= poolMatchups.season_competition_type_week.name %> - 
                                <%= moment(poolMatchups.season_competition_type_week.start_date).format('MMM D') %> to
                                <%= moment(poolMatchups.season_competition_type_week.end_date).format('MMM D') %>
                                <% } %>
                            </div>
                            <div class="upper">
                                <div class="right">
                                    <div class="img-div">
                                        <img src="<%= siteLink %>{{ config('pool.paths.team_logo') }}<%= poolMatchups.pool_team1.logo %>" alt="<%= poolMatchups.pool_team1.name %>" class="img-responsive"/>
                                    </div>
                                    <div class="name-div"> 
                                        <p>
                                            <a href="<%= myTeamTeamLink %>"><%= poolMatchups.pool_team1.name %></a>
                                            <span>
                                                <% if(!$.isEmptyObject(poolMatchups.pool_team1.pool_matchup_result)){ %>
                                                <%= poolMatchups.pool_team1.pool_matchup_result.total_win %>-<%= poolMatchups.pool_team1.pool_matchup_result.total_loss %>-<%= poolMatchups.pool_team1.pool_matchup_result.total_tie %>
                                                <% }else{ %>
                                                0-0-0
                                                <% } %>
                                            </span>
                                        </p>
                                    </div>
                                    <div class="goal-div">
                                        <p>
                                            <% if($.isArray(poolMatchups.pool_matchup_results)){ 
                                            if(!$.isEmptyObject(poolMatchups.pool_matchup_results)){ %>
                                            <% if(poolMatchups.pool_team1.id == poolMatchups.pool_matchup_results[0].pool_team_id){ %>
                                            <%= poolMatchups.pool_matchup_results[0].win %>
                                            <% }else{ %>
                                            <%= poolMatchups.pool_matchup_results[1].win %>
                                            <% } %>
                                            <% }else{ %>
                                            0
                                            <% } } %>
                                        </p>
                                    </div>
                                </div>
                                <div class="vs">
                                    <p>{{trans('pool.pool_dashboard.versus')}}</p>
                                </div>
                                <div class="left">
                                    <div class="img-div">
                                        <img src="<%= siteLink %>{{ config('pool.paths.team_logo') }}<%= poolMatchups.pool_team2.logo %>" alt="<%= poolMatchups.pool_team2.name %>" class="img-responsive"/>
                                    </div>
                                    <div class="name-div">
                                        <p>
                                            <a href="<%= myTeamTeamLink2 %>"><%= poolMatchups.pool_team2.name %></a>
                                            <span>
                                                <% if(!$.isEmptyObject(poolMatchups.pool_team2.pool_matchup_result)){ %>
                                                <%= poolMatchups.pool_team2.pool_matchup_result.total_win %>-<%= poolMatchups.pool_team2.pool_matchup_result.total_loss %>-<%= poolMatchups.pool_team2.pool_matchup_result.total_tie %>
                                                <% }else{ %>
                                                0-0-0
                                                <% } %>
                                            </span>
                                        </p>
                                    </div>
                                    <div class="goal-div">
                                        <p>
                                            <% if($.isArray(poolMatchups.pool_matchup_results)){ 
                                            if(!$.isEmptyObject(poolMatchups.pool_matchup_results)){ %>
                                            <% if(poolMatchups.pool_team2.id == poolMatchups.pool_matchup_results[0].pool_team_id){ %>
                                            <%= poolMatchups.pool_matchup_results[0].win %>
                                            <% }else{ %>
                                            <%= poolMatchups.pool_matchup_results[1].win %>
                                            <% } %>
                                            <% }else{ %>
                                            0
                                            <% } } %>
                                        </p> 
                                    </div>
                                </div>
                                <% if(typeof poolScoreSettings != 'undefined'){ %>
                                <div class='bottom'>
                                    <% _.each(poolScoreSettings, function(value){ %>
                                    <div class='width10 score-setting-<%= poolMatchups.id %>-<%= value.id %>'>
                                        <p><%= value.pool_scoring_field.stat %></p>
                                    </div>
                                    <%  }); %> 
                                </div>
                                <% } %>
                            </div>
                            <div class="clearfix"></div>
                            <div class="pool-matchup-outer">
                                <a href="<%= poolMatchupLink %>" class="btn btn-green-line">View Matchup</a>
                            </div>
                        </div>
                        </script>
                        <div id="pools-h2h-outer"></div>

                        <h3 class="pool--main-heading">{{trans('pool.pool_dashboard.standing')}}</h3>

                        <div id="standing-h2h-outer"></div>
                        <script id="standing-h2h-template" type="text/html">
                            <table class="table light-grey-table standing flip-scroll-table" id='standing'>
                                <thead>
                                    <tr>
                                        <th>{{trans('pool.pool_dashboard.rank')}}</th>
                                        <th>{{trans('pool.pool_dashboard.team_name')}}</th>
                                        <th>{{trans('pool.pool_dashboard.wlt')}}</th>
                                        <th>{{trans('pool.pool_dashboard.points')}}</th>
                                        <th>{{trans('pool.pool_dashboard.last_week')}}</th>
    <!--                                    <th>{{trans('pool.pool_dashboard.waiver')}}</th>
                                        <th>{{trans('pool.pool_dashboard.moves')}}</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <%  
                                    var count = 1;
                                    if(!$.isEmptyObject(poolTeams)){ 
                                    _.each(poolTeams, function(value){ %>
                                    <tr>
                                        <td><%= count %></td>
                                        <td><%= value.name %></td>
                                        <td>
                                            <% if(!$.isEmptyObject(value.pool_matchup_result)){ %>
                                            <%= value.pool_matchup_result.total_win %>-<%= value.pool_matchup_result.total_loss %>-<%= value.pool_matchup_result.total_tie %>
                                            <% }else{ %>
                                            0-0-0
                                            <% }  %>
                                        </td>
                                        <td>
                                            <%= value.total_score %>
                                        </td>
                                        <td>
                                            <% if(!$.isEmptyObject(value.last_week_pool_matchup_result)){ %>
                                            <%= value.last_week_pool_matchup_result.win %>-<%= value.last_week_pool_matchup_result.loss %>-<%= value.last_week_pool_matchup_result.tie %>
                                            <% }else{ %>
                                            0-0-0
                                            <% }  %>
                                        </td>
        <!--                                <td>1</td>
                                        <td>1</td>-->
                                    </tr>
                                    <% count++; }); %> 
                                    <% }else{ %>
                                    <tr> 
                                        <td colspan="7">
                                            {{trans('pool.pool_dashboard.no_teams_present')}}
                                        </td>
                                    </tr>
                                    <% } %>
                                </tbody>
                            </table>
                            </script>

                            <div id="standing-box-standard-outer"></div>
                            <script id="standing-box-standard-template" type="text/html">
                                <table class="table light-grey-table standing flip-scroll-table" id='standing'>
                                    <thead>
                                        <tr>
                                            <th>{{trans('pool.pool_dashboard.rank')}}</th>
                                            <th>{{trans('pool.pool_dashboard.team_name')}}</th>
                                            <% _.each(poolScoreSettings, function(v){ %>
                                            <th><%= v.pool_scoring_field.stat %> <span></span></th>
                                            <%  }); %> 
                                            <th>{{trans('pool.pool_dashboard.total_points')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%  
                                        var count = 1;
                                        if(!$.isEmptyObject(poolTeams)){ 
                                        _.each(poolTeams, function(value){ %>
                                        <tr>
                                            <td><%= count %></td>
                                            <td><%= value.name %></td>
                                            <% _.each(poolScoreSettings, function(v){ %>
                                            <td id="standing-<%= value.id %>-<%= v.pool_scoring_field_id %>">0</td>
                                            <%  }); %> 
                                            <td><%= value.total_score %></td>
                                        </tr>
                                        <% count++; }); } %> 
                                    </tbody>
                                </table>
                                </script>

                                @include('frontend.includes.ad_small_block')

                                <h3 class="pool--main-heading">{{trans('pool.pool_dashboard.league_feed')}}</h3>
                                <table class="table light-grey-table flip-scroll-table" id="league-feed">
                                    <thead>
                                        <tr>
                                            <th>{{trans('pool.pool_dashboard.type')}}</th>
                                            <th>{{trans('pool.pool_dashboard.team')}}</th>
                                            <th>{{trans('pool.pool_dashboard.details')}}</th>
                                            <th>{{trans('pool.pool_dashboard.time')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody id="all-transaction-outer"></tbody>
                                </table>

                                <script id="all-transaction-template" type="text/html">
                                    <% _.each(trans, function(v){ %>
                                    <tr>
                                        <td>
                                            <% if(v.type == transactionsTypes.inverse_type.added){ %>
                                            {{trans('pool.pool_dashboard.add')}}
                                            <% }else if(v.type == transactionsTypes.inverse_type.deleted){ %>
                                            {{trans('pool.pool_dashboard.drop')}}
                                            <% }else if(v.type == transactionsTypes.inverse_type.traded){ %>
                                            {{trans('pool.pool_dashboard.trade')}}
                                            <% } %>
                                        </td>
                                        <td>
                                            <% if(!$.isEmptyObject(v.pool_team)){ %>
                                            <%= v.pool_team.name %>
                                            <% }else{ %>
                                            <%= v.parent_pool_team.name %>
                                            <% } %>
                                        </td>
                                        <td><%= v.detail_string %></td>
                                        <td><%= moment(v.sort_date).format('h:mm a MMMM D YYYY') %></td>
                                    </tr>
                                    <% }); %> 
                                    </script>
                                </div>

                                <div class="adblocks">
                                    @include('frontend.includes.adblocks')
                                    @include('frontend.includes.team_records')
                                    @include('frontend.includes.player_news')
                                    @include('frontend.includes.adblocks')
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                @include('frontend.includes.trial_period')
                @stop 
                @section('after-scripts-end')
                {!! Html::script(elixir('js/pool_overview.js')) !!}
                <script>
                    var poolOverviewUrl = '{{route('frontend.pool.getPoolDataForDashboard', $pool_id)}}',
                            transactionsUrl = '{{route('frontend.pool.getTransactionsData', $pool_id)}}',
                            poolTypes = <?php echo json_encode(config('pool.inverse_type')); ?>,
                            poolFormat = <?php echo json_encode(config('pool.inverse_format')); ?>,
                            transactionsTypes = <?php echo json_encode(config('pool.transactions')); ?>,
                            pool_dashboard = <?php echo json_encode(trans('pool.pool_dashboard')); ?>,
                            gameTimezone = '<?php echo config('app.timezone'); ?>';
                </script>
                @stop