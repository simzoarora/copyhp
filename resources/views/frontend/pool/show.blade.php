@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/pool_show.css')) !!}
@stop 
@section('content')
<?php
$pool_score_seting = array();
foreach ($pool->poolScoreSettings as $key => $setting) {
    $scoring_field = $setting->pool_scoring_field_id;
    $found = false;
    if (!empty($pool_score_seting)) {
        foreach ($pool_score_seting as $score) {
            if ($scoring_field == $score->pool_scoring_field_id) {
                $found = true;
            }
        }
    }
    if (!$found) {
        $pool_score_seting[$key] = $setting;
    }
}
?>
<div class="container">
    <div class="row" id="pool--content"> 
        @include('frontend.includes.pool_nav')
        <div class="pool--content">
            <div class="bg-trans" style="display: none">
            </div>
            @include('frontend.includes.pool_name_header')

            <div class="pool--show-container">
                <div class="pool--show-info">
                    <h3 class="pool--main-heading">Pool Information</h3>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>{{trans('pool.pool_show.pool_name')}}</td>
                                <td>{{ $pool->name }}</td>
                            </tr>
                            @if(isset($pool->poolSetting->poolsetting->pool_format))
                            <tr>
                                <td>{{trans('pool.pool_show.pool_format')}}</td>
                                <td>
                                    @if(isset(config('pool.format.h2h')[$pool->poolSetting->poolsetting->pool_format])!=null)
                                    {{config('pool.format.h2h')[$pool->poolSetting->poolsetting->pool_format] }}
                                    @else
                                    {{config('pool.format.standard')[$pool->poolSetting->poolsetting->pool_format] }}
                                    @endif
                                </td>
                            </tr>
                            @endif
                            @if(isset($pool->season->name))
                            <tr>
                                <td>{{trans('pool.pool_show.season')}}</td>
                                <td>{{ $pool->season->name }}</td>
                            </tr>
                            @endif
                            @if(isset($pool->poolSetting->poolsetting->playoff_format))
                            <tr>
                                <td>{{trans('pool.pool_show.playoff_format')}}</td>
                                <td>{{config('pool.playoff_format')[$pool->poolSetting->poolsetting->playoff_format] }}
                            </tr>
                            @endif
                            @if(isset($pool->poolSetting->poolsetting->lock_eliminated_teams))
                            <tr>
                                <td>{{trans('pool.pool_show.lock_teams')}}</td>
                                <td>
                                    @if(($pool->poolSetting->poolsetting->lock_eliminated_teams) == 0)
                                    {{trans('pool.pool_show.no')}}
                                    @elseif(($pool->poolSetting->poolsetting->lock_eliminated_teams) == 1)
                                    {{trans('pool.pool_show.yes')}}
                                    @endif  
                                </td> 
                            </tr>
                            @endif
                            @if(isset($pool->poolSetting->poolsetting->max_teams))
                            <tr>
                                <td>{{trans('pool.pool_show.no_of_teams')}}</td>
                                <td>{{ $pool->poolSetting->poolsetting->max_teams}}</td>
                            </tr>
                            @endif
                            @if(isset($season_competition_type->seasonCompetitionTypeWeek->name_with_date))
                            <tr>
                                <td>{{trans('pool.pool_show.start_week')}}</td>
                                <td>{{ $season_competition_type->seasonCompetitionTypeWeek->name_with_date }}
                            </tr>
                            @endif
                            @if(isset($pool->poolSetting->poolsetting->min_goalie_appearance))
                            <tr>
                                <td>{{trans('pool.pool_show.min_goalie')}}</td>
                                <td>{{config('pool.min_goalie_appearance')[$pool->poolSetting->poolsetting->min_goalie_appearance] }}
                            </tr>
                            @endif
                            @if(isset($pool->poolSetting->poolsetting->max_acquisition_team))
                            <tr>
                                <td>{{trans('pool.pool_show.max_add_season')}}</td>
                                <td>{{config('pool.max_acquisition_team')[$pool->poolSetting->poolsetting->max_acquisition_team] }}</td>
                            </tr>
                            @endif
                            @if(isset($pool->poolSetting->poolsetting->max_acquisition_week))
                            <tr>
                                <td>{{trans('pool.pool_show.max_add_week')}}</td>
                                <td>{{config('pool.max_acquisition_week')[$pool->poolSetting->poolsetting->max_acquisition_week] }}</td>
                            </tr>
                            @endif
                            @if(isset($pool->poolSetting->poolsetting->max_trades_season))
                            <tr>
                                <td>{{trans('pool.pool_show.max_trade')}}</td>
                                <td>{{config('pool.max_trades_season')[$pool->poolSetting->poolsetting->max_trades_season] }}</td>
                            </tr>
                            @endif
                            @if(isset($pool->poolSetting->poolsetting->trade_end_date))
                            <tr>
                                <td>{{trans('pool.pool_show.trade_deadline')}}</td>
                                <td>{{ $pool->poolSetting->poolsetting->trade_end_date->format('M d Y') }}</td>
                            </tr>
                            @endif
                            @if(isset($pool->poolSetting->poolsetting->trade_reject_time))
                            <tr>
                                <td>{{trans('pool.pool_show.trade_reject')}}</td>
                                <td>{{config('pool.trade_reject_time')[$pool->poolSetting->poolsetting->trade_reject_time] }}</td>
                            </tr>
                            @endif
                            @if(isset($pool->poolSetting->poolsetting->waiver_mode))
                            <tr>
                                <td>{{trans('pool.pool_show.waiver')}}</td>
                                <td>{{config('pool.waiver_mode')[$pool->poolSetting->poolsetting->waiver_mode] }}
                            </tr>
                            @endif
                            @if(isset($pool->poolSetting->poolsetting->waiver_time))
                            <tr>
                                <td>{{trans('pool.pool_show.waiver_time')}}</td>
                                <td>{{config('pool.waiver_time')[$pool->poolSetting->poolsetting->waiver_time] }}</td>
                            </tr>
                            @endif
                            @if(isset($pool->poolSetting->poolsetting->trading_draft_pick))
                            <tr>
                                <td>{{trans('pool.pool_show.draft_pick_trade')}}</td>
                                <td>
                                    @if(($pool->poolSetting->poolsetting->trading_draft_pick) == 0)
                                    {{trans('pool.pool_show.no')}}
                                    @elseif(($pool->poolSetting->poolsetting->trading_draft_pick) == 1)
                                    {{trans('pool.pool_show.yes')}}
                                    @endif
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                            @if(!$pool->poolRosters->isEmpty())
                            <tr>
                                <td>{{trans('pool.pool_show.positions')}}</td>
                                <td>
                                    <div class="control-commas">
                                        @foreach($pool->poolRosters as $key => $poolRosters)<?php for ($i = 0; $i < $poolRosters->value; $i++) { ?>{{ ', '.$poolRosters->playerPosition->short_name }}<?php } ?>@endforeach()
                                    </div>
                                </td>
                            </tr>
                            @endif
                            @if(!$pool->poolScoreSettings->isEmpty())
                            <tr>
                                <td>{{trans('pool.pool_show.point_categories')}}</td>
                                <td id='pool-score'>
                                    <div class="control-commas">
                                        @foreach($pool_score_seting as $key => $poolScoreSettings){{ ', '.$poolScoreSettings->poolScoringField->stat }}@endforeach()
                                    </div>
                                </td>
                            </tr>
                            @endif
                            @if($pool->pool_type == config('pool.inverse_type.box'))
                            <tr>
                                <td>{{trans('pool.pool_show.invite_link')}}</td>
                                <td id='pool-score'>
                                    <div class="control-commas">
                                        {{  route('frontend.pool.invite', $pool->invite_key) }}
                                    </div>
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="adblocks">
                @include('frontend.includes.create_pool')
                @include('frontend.includes.adblocks') 
                @include('frontend.includes.hockeypool_news')
            </div>
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
<script>
</script>
{!! Html::script(elixir('js/pool_show.js')) !!}
@stop 