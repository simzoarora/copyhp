@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/league_leaders.css')) !!}
@stop
@section('content')
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content"> 
        @include('frontend.includes.pool_nav')
        <div class='pool--content'>
            <div class="bg-trans" style="display: none"></div>
            @include('frontend.includes.pool_name_header')
            <div class="league-leaders-conatiner">
                <div class="bg-trans" style="display: none">
                </div>
                <div class="league-leaders">
                    <div class="league-leaders-header">
                        <p class="pool--main-heading"> {{$pool->poolTeam->name}}
                            <?php if (config('pool.inverse_type.h2h') == $pool->pool_type) { ?>
                                (<?php echo isset($pool->poolTeam->poolMatchupResult->total_win) ? $pool->poolTeam->poolMatchupResult->total_win : 0; ?>-<?php echo isset($pool->poolTeam->poolMatchupResult->total_loss) ? $pool->poolTeam->poolMatchupResult->total_loss : 0; ?>-<?php echo isset($pool->poolTeam->poolMatchupResult->total_tie) ? $pool->poolTeam->poolMatchupResult->total_tie : 0; ?>) 
                            <?php } ?>
                            <span class="pool-name-right">{{$pool->name}}</span>
                        </p>
                        <ul class="league_position">
                            <li>
                                {{ Form::select('positions',[0=>trans('pool.league_leaders.select_position')] + $positions ,isset($_GET["player_position_id"])?$_GET["player_position_id"]:null,['class' => 'select-field','id'=>'position']) }}
                            </li>
                        </ul>
                        @include('frontend.includes.league_nav')
                        <div class="league-standings">
                            <ul id="stat_order_list">
                                <li>
                                    <a data-stat_order_key="total_points">{{trans('pool.league_leaders.total')}}</a>
                                </li> 

                                <?php
                                $append_ids = [];
                                foreach ($pool->poolScoreSettings as $key => $val) {
                                    if (in_array($val->poolScoringField->id, $append_ids) == false) {
                                        $append_ids[] = $val->poolScoringField->id;
                                        ?>
                                        <li>
                                            <a data-stat_order_key="<?php echo $val->poolScoringField->id; ?>">
                                                <?php echo $val->poolScoringField->stat; ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <table class="table light-grey-table head-arrow flip-scroll-table" id="main-table">
                        <thead>
                            <tr>
                                <?php if (config('pool.inverse_type.box') != $pool->pool_type) { ?>
                                    <th data-sortname="pool_team"><span></span>{{trans('pool.league_leaders.owner')}}</th>
                                <?php } ?>
                                <th data-sortname="player">{{trans('pool.league_leaders.player')}} <span></span></th>
                                <th data-sortname="team">{{trans('pool.league_leaders.team')}} <span></span></th>
                                <th data-sortname="position">{{trans('pool.league_leaders.position')}} <span></span></th>
                                <th class="data-both" data-sortname="stat" data-stat_order_key="total_points" class="total-pool-points"><span></span>{{trans('pool.league_leaders.total_points')}}</th>
                                <?php
                                $append_ids = [];
                                foreach ($pool->poolScoreSettings as $key => $val) {
                                    if (in_array($val->poolScoringField->id, $append_ids) == false) {
                                        $append_ids[] = $val->poolScoringField->id;
                                        ?>
                                        <th class="data-both" data-sortname="stat" data-stat_order_key="<?php echo $val->poolScoringField->id; ?>">
                                            <?php echo $val->poolScoringField->stat; ?><span></span>
                                        </th> 
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody id="league-leaders-all">
                    </table>

                    <div class="post-pagination" style="display:none;">
                        <div class="pagination-child">
                            <div id="pagination-first" class="pagination-arrows">
                                <div class="double-arrow"></div>
                            </div>
                            <div id="pagination-left" class="pagination-arrows"></div>
                            <div class="main-pagination">
                                <div id="page-list">
                                </div>
                            </div>
                            <div id="pagination-right" class="pagination-arrows"></div>
                            <div id="pagination-last" class="pagination-arrows">
                                <div class="double-arrow"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script id="league-leaders-template" type="text/html">
    <%  _.each(players, function(p){  %>
    <tr>
        <?php if (config('pool.inverse_type.box') != $pool->pool_type) { ?>
            <td>
                <%= p.pool_team_player?p.pool_team_player.pool_team.name:'-'%>
            </td>
        <?php } ?>
        <td><%= p.player.full_name %></td>
        <td><%= p.team.display_name %></td>
        <td><%= p.player_position.short_name %></td>
        <td class="data-both" data-stat_order_key="total_points" class="total-pool-points">
            <%= p.total_points?hpApp.twoDecimal(p.total_points):0 %>
        </td>  
        <?php
        $append_ids = [];
        foreach ($pool->poolScoreSettings as $key => $val) {
            if (in_array($val->poolScoringField->id, $append_ids) == false) {
                $append_ids[] = $val->poolScoringField->id;
                ?>
                <td class="data-both"  data-stat_order_key="<?php echo $val->poolScoringField->id; ?>">
                    <%= p.player.player_stat[<?php echo $val->poolScoringField->id; ?>]?hpApp.twoDecimal(p.player.player_stat[<?php echo $val->poolScoringField->id; ?>]):0%>
                </td>
                <?php
            }
        }
        ?>
    </tr>
    <% }) %>
</script>
@stop 
@section('after-scripts-end')
<script>
    var url = '{{route('frontend.pool.getDataForLeagueLeader',$pool_id)}}',
            get_url_name = "getDataForLeagueLeader",
            normal_url_name = "league-leader",
            order_type = "<?php echo isset($_GET["order_type"]) ? $_GET["order_type"] : false; ?>",
            order = "<?php echo isset($_GET["order"]) ? $_GET["order"] : false; ?>",
            stat_order_key = "<?php echo isset($_GET["stat_order_key"]) ? $_GET["stat_order_key"] : false; ?>",
            page = "<?php echo isset($_GET["page"]) ? $_GET["page"] : false; ?>",
            start_date = "<?php echo isset($_GET["start_date"]) ? $_GET["start_date"] : false; ?>",
            end_date = "<?php echo isset($_GET["end_date"]) ? $_GET["end_date"] : false; ?>",
            data_retrieval = "<?php echo isset($_GET["data_retrieval"]) ? $_GET["data_retrieval"] : false; ?>",
            season_id = "<?php echo isset($_GET["season_id"]) ? $_GET["season_id"] : false; ?>",
            league_leaders = <?php echo json_encode(trans('pool.league_leaders')); ?>;
</script>
{!! Html::script(elixir('js/league_leaders.js')) !!}
@stop