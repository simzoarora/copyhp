@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/transactions.css')) !!}
@stop
@section('content')
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">
        @include('frontend.includes.pool_nav')
        <div class="pool--content">
            <div class="bg-trans" style="display: none">
            </div>
            <?php $add_drop_page = true; ?>
            @include('frontend.includes.pool_name_header')

            <div class="pool-overview-container">
                <div class="pool-overview">
                    <div id="allTrades">

                    </div>
                    <h3 class="pool--main-heading">{{trans('pool.transaction.pool_transactions')}}</h3> 
                    <table class="table light-grey-table flip-scroll-table">
                        <thead>
                            <tr>
                                <th>{{trans('pool.transaction.type')}}</th>
                                <th>{{trans('pool.transaction.team')}}</th>
                                <th>{{trans('pool.transaction.details')}}</th>
                                <th>{{trans('pool.transaction.time')}}</th>
                            </tr>
                        </thead>
                        <tbody id="all-transaction-outer"></tbody>
                    </table>

                    <script id="all-transaction-template" type="text/html">
                        <% _.each(trans, function(v){ %>
                        <tr>
                            <td>
                                <% if(v.type == transactionsTypes.inverse_type.added){ %>
                                {{trans('pool.transaction.add')}}
                                <% }else if(v.type == transactionsTypes.inverse_type.deleted){ %>
                                {{trans('pool.transaction.drop')}}
                                <% }else if(v.type == transactionsTypes.inverse_type.traded){ %>
                                {{trans('pool.transaction.trade')}}
                                <% } %>
                            </td>
                            <td>
                                <% if(!$.isEmptyObject(v.pool_team)){ %>
                                <%= v.pool_team.name %>
                                <% }else{ %>
                                <%= v.parent_pool_team.name %>
                                <% } %>
                            </td>
                            <td><%= v.detail_string %></td>
                            <td><%= moment(v.sort_date).format('h:mm a MMMM D YYYY') %></td>
                        </tr>
                        <% }); %> 
                        </script>

                        @include('frontend.includes.ad_small_block')
                    </div>

                    <div class="adblocks">
                        @include('frontend.includes.adblocks')
                        @include('frontend.includes.team_records')
                        @include('frontend.includes.player_news')
                        @include('frontend.includes.adblocks')
                    </div>
                </div>

            </div>

        </div>
    </div>
    <?php
    $inverse_msettings = config('pool.pool_trade_managemnet_settings_inverse');
    $inverse_tstatus = config('pool.pool_trades.inverse_status');
    $settings_bstatus = config('pool.pool_trades.pool_trade_setting_based_status');
    ?>
    <script id="trades-template" type="text/html">
        <% _.each(trade, function(tradeData, tradeType){ %>
        <%= (tradeType == 'pending_trades')?'<h3 class="pool--main-heading">{{trans('pool.transaction.pending_trades')}}</h3>':''%>
        <%= (tradeType == 'offered_trades')?'<h3 class="pool--main-heading">{{trans('pool.transaction.offered_trades')}}</h3>':''%>
        <%= (tradeType == 'my_trades')?'<h3 class="pool--main-heading">{{trans('pool.transaction.my_trades')}}</h3>':''%>

        <% _.each(tradeData, function(eachTrade, key){ %>
        <% if(!_.isNull(eachTrade.parent_pool_team)){ %>
        <div class="trades" data-type="<%= (tradeType == 'pending_trades')?'pending':(tradeType == 'offered_trades'?'offered':'mytrades') %>">
            <div class="trades-content">
                <div class="teams">
                    <div class="single-team">
                        <p class="team-name"><%= eachTrade.parent_pool_team ? eachTrade.parent_pool_team.name :'' %></p>
                        <div class="trade-details">
                            <% _.each(eachTrade.pool_trade_players, function(player, key){ %>
                            <% if(!_.isNull(player.pool_team_player)){ %>
                            <% if(player.pool_team_player.pool_team_id == eachTrade.parent_pool_team.id){%>
                            <p>
                                <span><%= player.pool_team_player.player_season.player_position.short_name %></span>-
                                <span><%= player.pool_team_player.player_season.player.full_name %></span>-
                                <span><%= player.pool_team_player.player_season.team.short_name %></span>
                            </p>
                            <% } %>
                            <% } %>
                            <% }); %>
                        </div>
                        <div class="center-arrows">
                            <i class="fa fa-long-arrow-right"></i>
                            <i class="fa fa-long-arrow-left"></i>
                        </div>
                    </div>

                    <div class="single-team" >
                        <p class="team-name"><%= eachTrade.trading_pool_team ? eachTrade.trading_pool_team.name :'' %></p>
                        <div class="trade-details">
                            <% _.each(eachTrade.pool_trade_players, function(player, key){ %>
                            <% if(!_.isNull(player.pool_team_player)){ %> 
                            <% if(player.pool_team_player.pool_team_id == eachTrade.trading_pool_team.id){%>
                            <p>
                                <span><%= player.pool_team_player.player_season.player_position.short_name %></span>-
                                <span><%= player.pool_team_player.player_season.player.full_name %></span>-
                                <span><%= player.pool_team_player.player_season.team.short_name %></span>
                            </p>
                            <% } %>
                            <% } %>
                            <% }); %>
                        </div>
                    </div>
                    <div class="trade-status" data-tradeId="<%= _.first(eachTrade.pool_trade_players).pool_trade_id %>" >
                        <p>This trade will complete on <%= moment(eachTrade.deadline).format('LLL') %> Eastern</p>
                        <!--pending-trades-->
                        <% if(tradeType == 'pending_trades' ){ %>
                        <!--pending-trades--commissioner-decides-and-accepted-trade-CASE-1--->
                        <% if((eachTrade.trade_managemnet_status == "{{$inverse_msettings['commissioner_decides']}}" ) && (eachTrade.pool_trade_setting_based_status == "{{$settings_bstatus['accepted']}}")){ %>
                        <!--trade parent user-->
                        <% if(eachTrade.trading_pool_team.user_id == "{{ Auth::id()}}"){ %>
                        <p> {{trans('pool.pool_votes.commissioner_wait')}} <p>
                            <% } else{ %>
                            <!--commisioner reject trade-->
                            <a class="btn-orange" data-status="{{$inverse_tstatus['cancelled_by_commissioner']}}" data-owner="<%= ( !_.isUndefined(eachTrade.owner_id))?eachTrade.owner_id:'' %>">{{trans('pool.transaction.reject_vote')}}</a>
                            <% } %>
                            <!--trade parent user-if-END-->
                            <% } %>
                            <!--pending-trades--commissioner-decides-and-accepted-trade-END-->
                            <!--pending-trades--league-votes-and-accepted-trade--CASE-2--->
                            <% if((eachTrade.trade_managemnet_status == "{{$inverse_msettings['league_votes']}}") && (eachTrade.pool_trade_setting_based_status == "{{$settings_bstatus['accepted']}}")){ %> 
                            <!--trade parent user-->
                            <% if(eachTrade.trading_pool_team.user_id == "{{ Auth::id()}}"){ %>
                        <p> {{trans('pool.pool_votes.vote_wait')}} <p>
                            <!--trade non-parent-user and voted-->
                            <% } else if(! _.isNull(eachTrade.vote_status)){ %>
                            <!--trade non-parent-user and voted-accept-->
                            <%= (eachTrade.vote_status == "{{config('pool.pool_trade_vote_status.Accept')}}")?"<p> {{trans('pool.pool_votes.vote_accept')}} <p>":'' %>
                            <!--trade non-parent-user and voted-reject-->
                            <%= (eachTrade.vote_status == "{{config('pool.pool_trade_vote_status.Reject')}}")?"<p> {{trans('pool.pool_votes.vote_reject')}} <p>":'' %> 
                            <% } else{ %>
                            <!--trade non-parent-user and not-voted-->
                            <a class="btn-green league_votes" data-type="{{$inverse_msettings['league_votes']}}" data-status="{{$inverse_tstatus['accepted']}}">{{trans('pool.transaction.accept_vote')}}</a>
                            <a class="btn-orange league_votes"  data-type="{{$inverse_msettings['league_votes']}}" data-status="{{$inverse_tstatus['rejected']}}">{{trans('pool.transaction.reject_vote')}}</a>
                            <% } %>
                            <!--trade non-parent-user and not-voted--END-->
                            <% } %> 
                            <!--pending-trades--league-votes-and-accepted-trade-END-->
                            <% } %> 
                            <!--pending-trades-END-->
                            <!--offered-trades--->
                            <% if(tradeType == 'offered_trades'){ %> 
                            <a class="btn-green" data-managementstatus="<%= ( !_.isUndefined(eachTrade.trade_managemnet_status))?eachTrade.trade_managemnet_status:'' %>"  data-status="{{$inverse_tstatus['accepted']}}" data-owner="<%= ( !_.isUndefined(eachTrade.owner_id))?eachTrade.owner_id:'' %>">{{trans('pool.transaction.accept_trade')}}</a>
                            <a class="btn-orange" data-managementstatus="<%= ( !_.isUndefined(eachTrade.trade_managemnet_status))?eachTrade.trade_managemnet_status:'' %>"  data-status="{{$inverse_tstatus['rejected']}}" data-owner="<%= ( !_.isUndefined(eachTrade.owner_id))?eachTrade.owner_id:'' %>">{{trans('pool.transaction.reject_trade')}}</a>
                            <% } %> 
                            <!--offered-trades-END-->
                            <!--my-trades-END-->
                            <% if(tradeType == 'my_trades'){ %> 
                            <a class="btn-orange" data-status="{{$inverse_tstatus['cancelled_by_parent']}}">{{trans('pool.transaction.cancel_trade')}}</a>
                            <% } %> 
                            <!--my-trades-END-->
                        <div id="alert-box"></div>                   
                    </div>
                </div>  
            </div>
        </div>
        <% } %>
        <% }); %>
        <div class="empty-pending-trades" data-type="<%= (tradeType == 'pending_trades')?'pending':(tradeType == 'offered_trades'?'offered':'mytrades') %>"  data-u="<%= tradeData.length %>" style="display:<%= tradeData.length == '0'?'block':'none'%>">
            <p>{{trans("pool.transaction.no_trade")}}</p>
        </div>
        <% }); %>
    </script>

    @stop 
    @section('after-scripts-end')
    {!! Html::script(elixir('js/transactions.js')) !!}
    <script>
        var transactionsUrl = "{{route('frontend.pool.getTransactionsData', $pool_id)}}",
                chnageStatusUrl = "{{route('frontend.pool_teams.changeTradeStatus',[$pool_id,'tradeid'])}}",
                tradesUrl = "{{route('frontend.pool.getAllTrades', $pool_id)}}",
                transactionsTypes = <?php echo json_encode(config('pool.transactions')); ?>,
                transaction = <?php echo json_encode(trans('pool.transaction')); ?>,
                acceptTradeStatus = "{{$inverse_tstatus['accepted']}}",
                rejectTradeStatus = "{{$inverse_tstatus['rejected']}}",
                commissionerManagemnetStatus = "{{$inverse_msettings['commissioner_decides']}}",
                leagueVoteManagemnetStatus = "{{$inverse_msettings['league_votes']}}",
                loggedInUser = "{{Auth::id()}}",
                commissionerWait = "{{trans('pool.pool_votes.commissioner_wait')}}",
                votingWait = "{{trans('pool.pool_votes.vote_wait')}}",
                acceptedWaitVoting = "{{trans('pool.pool_votes.vote_accept')}}",
                rejectedWaitVoting = "{{trans('pool.pool_votes.vote_reject')}}";
    </script> 
    @stop