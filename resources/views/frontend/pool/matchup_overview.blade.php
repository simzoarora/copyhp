@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/matchup_overview.css')) !!} 
@stop
@section('content')
<?php // dd($pool_details->toArray());?>
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">
        @include('frontend.includes.pool_nav')
        <div class="pool--content">
            <div class="bg-trans" style="display: none">
            </div>
            <?php $dashboardPage = true; ?>
            @include('frontend.includes.pool_name_header')

            <div class="pool-overview-container">
                <div class="pool-overview">
                    <!--slider-->
                    <div class="matchup-individual-slider" id='slider-all'></div>
                    @include('frontend.includes.pool_slider')
                    <!--slider-ends-->

                    <div class="season-week-outer">
                        {{ Form::select('season_weeks', $season_weeks, null, ['class' => 'select-field season-week-select']) }}
                    </div>

                    <script id="pools-h2h-template" type="text/html">
                        <div class="pools--h2h">
                            <div class="week">
                                <%= poolMatchups.season_competition_type_week.name %>
                            </div>
                            <div class="upper">
                                <div class="right">
                                    <div class="img-div">
                                        <img src="<%= siteLink %>{{ config('pool.paths.team_logo') }}<%= poolMatchups.pool_team1.logo %>" alt="<%= poolMatchups.pool_team1.name %>" class="img-responsive"/>
                                    </div>
                                    <div class="name-div">
                                        <p><%= poolMatchups.pool_team1.name %>
                                            <span>
                                                <% if($.isArray(poolMatchups.pool_matchup_results)){
                                                if(!$.isEmptyObject(poolMatchups.pool_matchup_results)){ %>
                                                <% if(poolMatchups.pool_team1.id == poolMatchups.pool_matchup_results[0].pool_team_id){ %>
                                                <%= poolMatchups.pool_matchup_results[0].win %>-
                                                <%= poolMatchups.pool_matchup_results[0].loss %>-
                                                <%= poolMatchups.pool_matchup_results[0].tie %>
                                                <% }else{ %>
                                                <%= poolMatchups.pool_matchup_results[1].win %>-
                                                <%= poolMatchups.pool_matchup_results[1].loss %>-
                                                <%= poolMatchups.pool_matchup_results[1].tie %>
                                                <% } %>
                                                <% }else{ %>
                                                0-0-0
                                                <% } } %>
                                            </span>
                                        </p>
                                    </div>
                                    <div class="goal-div">
                                        <p>
                                            <% if($.isArray(poolMatchups.pool_matchup_results)){ 
                                            if(!$.isEmptyObject(poolMatchups.pool_matchup_results)){ %>
                                            <% if(poolMatchups.pool_team1.id == poolMatchups.pool_matchup_results[0].pool_team_id){ %>
                                            <%= poolMatchups.pool_matchup_results[0].score %>
                                            <% }else{ %>
                                            <%= poolMatchups.pool_matchup_results[1].score %>
                                            <% } %>
                                            <% }else{ %>
                                            0
                                            <% } } %>
                                        </p>
                                    </div>
                                </div>
                                <div class="vs">
                                    <p>{{trans('pool.pool_dashboard.versus')}}</p>
                                </div>
                                <div class="left">
                                    <div class="img-div">
                                        <img src="<%= siteLink %>{{ config('pool.paths.team_logo') }}<%= poolMatchups.pool_team2.logo %>" alt="<%= poolMatchups.pool_team2.name %>" class="img-responsive"/>
                                    </div>
                                    <div class="name-div">
                                        <p><%= poolMatchups.pool_team2.name %>
                                            <span>
                                                <% if($.isArray(poolMatchups.pool_matchup_results)){
                                                if(!$.isEmptyObject(poolMatchups.pool_matchup_results)){ %>
                                                <% if(poolMatchups.pool_team2.id == poolMatchups.pool_matchup_results[0].pool_team_id){ %>
                                                <%= poolMatchups.pool_matchup_results[0].win %>-
                                                <%= poolMatchups.pool_matchup_results[0].loss %>-
                                                <%= poolMatchups.pool_matchup_results[0].tie %>
                                                <% }else{ %>
                                                <%= poolMatchups.pool_matchup_results[1].win %>-
                                                <%= poolMatchups.pool_matchup_results[1].loss %>-
                                                <%= poolMatchups.pool_matchup_results[1].tie %>
                                                <% } %>
                                                <% }else{ %>
                                                0-0-0 
                                                <% } } %>
                                            </span>
                                        </p>
                                    </div>
                                    <div class="goal-div">
                                        <p>
                                            <% if($.isArray(poolMatchups.pool_matchup_results)){ 
                                            if(!$.isEmptyObject(poolMatchups.pool_matchup_results)){ %>
                                            <% if(poolMatchups.pool_team2.id == poolMatchups.pool_matchup_results[0].pool_team_id){ %>
                                            <%= poolMatchups.pool_matchup_results[0].score %>
                                            <% }else{ %>
                                            <%= poolMatchups.pool_matchup_results[1].score %>
                                            <% } %>
                                            <% }else{ %>
                                            0
                                            <% } } %>
                                        </p> 
                                    </div>
                                </div>
                                <% if(typeof poolScoreSettings != 'undefined'){ %>
                                <div class='bottom'>
                                    <% _.each(poolScoreSettings, function(value){ %>
                                    <div class='width10 score-setting-<%= poolMatchups.id %>-<%= value.id %>'>
                                        <p><%= value.pool_scoring_field.stat %></p>
                                    </div>
                                    <%  }); %> 
                                </div>
                                <% } %>
                            </div>
                        </div>
                        </script>
                        <div id="pools-h2h-outer" class="pool-h2h-full-width"></div>

                        @include('frontend.includes.ad_small_block')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('frontend.includes.trial_period')
    @stop 
    @section('after-scripts-end')
    <script>
        var poolOverviewUrl = '{{route('frontend.pool.getPoolDataForDashboard', $pool_id)}}',
                slider_url = '{{route('frontend.pool.getmatchupHeaderData', $pool_id)}}',
                poolTypes = <?php echo json_encode(config('pool.inverse_type')); ?>,
                poolFormat = <?php echo json_encode(config('pool.inverse_format')); ?>,
                pool_dashboard = <?php echo json_encode(trans('pool.pool_dashboard')); ?>;
    </script>
    {!! Html::script(elixir('js/matchup_overview.js')) !!}
    @stop