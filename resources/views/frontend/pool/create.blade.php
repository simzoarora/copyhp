@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/pool_creator.css')) !!}
@stop
@section('content')
<?php
//editing changes
if (!empty($pool)) {
    $pool = $pool->toArray();
    $pool = array_merge($pool, $pool['pool_setting']['poolsetting']);
    //adding pool teams and pool invitations
    $pool['all_pool_teams'] = $pool['pool_teams'];

    //changing pool_rosters
    $total_pool_rosters = 0;
    if (!empty($pool['pool_rosters'])) {
        foreach ($pool['pool_rosters'] as $key => $roster) {
            if ($roster['value'] == 1) {
                $total_pool_rosters = $total_pool_rosters + 1;
            }
            if ($key == 7) {
                $pool_goalie_rosters = $roster['value'];
            }
            foreach ($player_positions as $positions) {
                if ($positions->id == $roster['player_position_id']) {
                    $pool['pool_rosters'][$key]['name'] = $positions->name;
                    $pool['pool_rosters'][$key]['short_name'] = $positions->short_name;
                }
            }
        }
        if (isset($pool_goalie_rosters)) {
            $pool_player_rosters = $total_pool_rosters - $pool_goalie_rosters;
        } else {
            $pool_player_rosters = $total_pool_rosters;
        }
    }

    //changing pool boxes
    //sorting array for predraft in h2h and standard
    if (!empty($pool['all_pool_teams']) && ($pool['pool_type'] == 1 || $pool['pool_type'] == 3)) {
        //checking if predraft is submitted
        if ($pool['all_pool_teams'][0]['sort_order'] != null) {
            $sort_array = array();
            foreach ($pool['all_pool_teams'] as $key => $team) {
                $sort_array[$key] = $team['sort_order'];
            }
            array_multisort($sort_array, SORT_ASC, $pool['all_pool_teams']);
        }
    }
} else {
    $pool = array();
    //getting pool variables from url if there are -- only present in create pool --not in edit pool
    if (!empty($_GET['pool_type'])) {
        $pool['pool_type'] = $_GET['pool_type'];
    }
    if (!empty($_GET['pool_format'])) {
        $get_pool_format = $_GET['pool_format'];
    }
}
?>
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="pool_creator">
        <div class="row pool-create-header">
            <h4>Pool Creator</h4>
            <div class="pool-types">
                <p>Pool Type:</p>
                {{ Form::select('pool_type', config('pool.type'), (isset($pool['pool_type'])?$pool['pool_type']:'1'), ['class' => 'select-field pool-type-select']) }}
            </div>
        </div>
        <div class="row pool-create-nav h2h-step-1">
            <ul>
                <li class='selected' data-step="1">
                    <span class='number'>1</span>
                    <span style="display:none;"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span  class='step-label'>Setup</span>
                    <span class='right-arrow'></span>
                </li>
                <li data-step="2">
                    <span class='number'>2</span>
                    <span style="display:none;"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span class='step-label'>Teams</span>
                    <span class='right-arrow'></span>
                </li>
                <li data-step="3">
                    <span class='number'>3</span>
                    <span style="display:none;"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span class='step-label'>Roster</span>
                    <span class='right-arrow'></span>
                </li>
                <li data-step="4">
                    <span class='number'>4</span>
                    <span style="display:none;"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span class='step-label'>Scoring</span>
                    <span class='right-arrow'></span>
                </li>
                <li data-step="5">
                    <span class='number'>5</span>
                    <span style="display:none;"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span class='step-label'>Predraft</span>
                    <span class='right-arrow'></span>
                </li>
                <li data-step="6">
                    <span class='number'>6</span>
                    <span style="display:none;"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span class='step-label'>Draft</span>
                    <span class='right-arrow'></span>
                </li>
                <li data-step="7">
                    <span class='number'>4</span>
                    <span style="display:none;"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span class='step-label'>Boxes</span>
                    <span class='right-arrow'></span>
                </li>
                <li data-step="8">
                    <span class='number'>5</span>
                    <span style="display:none;"><i class="fa fa-check" aria-hidden="true"></i></span>
                    <span class='step-label'>Review</span>
                    <span class='right-arrow'></span>
                </li>
            </ul>
        </div>
        <div id="main-steps" class="h2h-step-1">
            <input type="hidden" name="pool_id" class="pool_id_input" value="{{ (isset($pool['pool_setting']['pool_id'])?$pool['pool_setting']['pool_id']:'') }}"/>

            <input type="hidden" id="user_email" value="{{ Auth::user()->email }}"/>
            <div class="row steps step-1" data-step="1">
                {!! Form::model($pool, ['route' => 'pool.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
                <h3>Pool Settings</h3>
                <div class="form-group pool-all">
                    <label><span class="main-label">Pool Name</span> <span class="horizontal-line"></span></label>
                    <div id="pool-type-desc-text">
                        <p>Select Pool Type First</p>
                    </div>
                    {{ Form::text('name', null, array('class' => 'text-field', 'id'=>'pool-name-input','placeholder'=>'Enter pool name...','required')) }}
                </div>
                <div class="form-group pool-all">
                    <label><span class="main-label">Pool Image</span> <span class="horizontal-line"></span></label>
                    <div class="pool-image-upload dropzone" data-width="200" data-height="150" data-save="false" data-image="{{ (isset($pool['logo'])?URL::asset('/img/pool_logos/'.$pool['logo']):'') }}">
                        <input type="file"/>
                    </div>
                    <input type="hidden" name="logo[name]"/>
                    <input type="hidden" name="logo[data]"/>
                </div>
                <div class="form-group pool-h2h">
                    <label><span class="main-label">Pool Format </span><span class="horizontal-line"></span></label>
                    {{ Form::select('pool_format', config('pool.format.h2h'), (isset($get_pool_format)?$get_pool_format:'null'), ['class' => 'select-field full pool-format-h2h','id'=>'full','required']) }}
                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;">
                        <p>We have three different types of Head To Head pools for you to choose from:</p>
                        <p>1) <b>Categories</b> : Choose the categories for the pool. Teams then compete weekly in these categories. Each category is worth a win for your record.</p>
                        <p>2) <b>Weekly Win</b> : Similar to Fantasy Football style. Teams compete weekly where each category is worth a point amount. Only one team can win for each matchup.</p>
                        <p>3) <b>Weekly Win (Daily)</b> : The same as weekly win, but users can set their roster daily instead of weekly.</p>
                    </div>
                </div>
                <div class="form-group pool-standard">
                    <label><span class="main-label">Pool Format </span><span class="horizontal-line"></span></label>
                    {{ Form::select('pool_format', config('pool.format.standard'), (isset($get_pool_format)?$get_pool_format:'null'), ['class' => 'select-field full pool-format-standard','id'=>'full','required']) }}
                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;">
                        <p>We have two different standard leagues for you to choose from:</p>
                        <p>1) <b>Standard</b> : Choose the categories and give them a point value. The team with the highest point value at the end of the season wins.</p>
                        <p>2) <b>Rotisserie</b> : Choose the categories teams compete in. The team that leads a category gets the most points. Total points from each category are added for your ranking.</p>
                    </div>
                </div>
                <div class="form-group pool-standard pool-box">
                    <label><span class="main-label">Season </span><span class="horizontal-line"></span></label>
                    {{ Form::select('season_type', config('pool.season'), (isset($pool['season_type'])?$pool['season_type']:null), ['class' => 'select-field standard_season_type', 'id'=>'full']) }}
                </div> 
                <div class="form-group pool-h2h playoff-format-div">
                    <label><span class="main-label">Playoffs </span><span class="horizontal-line"></span></label>
                    {{ Form::checkbox('playoff_format', (isset($pool['playoff_format'])?$pool['playoff_format']:'1'), true, ['class' => 'switch-input checkbox-field playoff-format']) }}
                    <label class="switch switch-blue">
                        <input type="checkbox" class="switch-input" value="1">
                        <span class="switch-label" data-on="Yes" data-off="No"></span>
                        <span class="switch-handle"></span>
                    </label> 
                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;">
                        <p>
                            Turn this on if you want the final weeks of your pool to be playoffs. Teams regular season records will determine the playoff bracket.
                            Playoffs are held during the regular NHL season but considered playoffs for the purposes of the pool.
                        </p>
                    </div>

                    <div class="right" <?php echo (isset($pool['playoff_schedule']) ? 'style="display:block;"' : ''); ?>>
                        <label><span class="main-label">Playoff Finish Week</span></label>
                        {{ Form::select('playoff_schedule', config('pool.playoff_schedule'), null, ['class' => 'select-field', 'id'=>'full']) }} <br>
                        <label class="team"><span class="main-label">Playoff Teams </span></label>
                        {{ Form::select('playoff_teams_number', config('pool.playoff_teams_number'), null, ['class' => 'select-field team_no', 'id'=>'full']) }}
                    </div>
                </div>
                <div class="form-group pool-h2h">
                    <label><span class="main-label">Lock Eliminated Teams? </span><span class="horizontal-line"></span></label>
                    {{ Form::checkbox('lock_eliminated_teams', (isset($pool['lock_eliminated_teams'])?$pool['lock_eliminated_teams']:'1'), true, ['class' => 'switch-input checkbox-field']) }}
                    <label class="switch switch-blue">
                        <input type="checkbox" class="switch-input" value='1' checked>
                        <span class="switch-label" data-on="Yes" data-off="No"></span>
                        <span class="switch-handle"></span>
                    </label> 

                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;">
                        <p>
                            Locking eliminated teams means they can do longer make any adjustments to their roster once they have been eliminated.
                        </p>
                    </div>
                </div>
                <div class="form-group pool-h2h" id="maximum-no-team">
                    <label><span class="main-label">Maximum Number of Teams </span><span class="horizontal-line"></span></label>
                    {{ Form::select('max_teams', config('pool.max_teams'), (isset($pool['max_teams'])?$pool['max_teams']:'12'), ['class' => 'select-field h2h_teams', 'id'=>'full']) }}
                </div>
                <div class="form-group pool-standard" id="maximum-no-team">
                    <label><span class="main-label">Maximum Number of Teams </span><span class="horizontal-line"></span></label>
                    {{ Form::select('max_teams', config('pool.max_teams_standard'), (isset($pool['max_teams'])?$pool['max_teams']:'12'), ['class' => 'select-field standard_teams', 'id'=>'full']) }}
                </div>
                <div class="form-group pool-all">
                    <label><span class="main-label">League Start Week </span><span class="horizontal-line"></span></label>
                    {{ Form::select('league_start_week', $season_weeks, (isset($pool['league_start_week'])?$pool['league_start_week']:'1'), ['class' => 'select-field league-start-week', 'id'=>'full']) }}
                </div>
                <div class="form-group pool-h2h" id="minimum-goalie">
                    <label><span class="main-label">Minimum Goalie appearances </span><span class="horizontal-line"></span></label>
                    {{ Form::select('min_goalie_appearance', config('pool.min_goalie_appearance'), (isset($pool['min_goalie_appearance'])?$pool['min_goalie_appearance']:'3'), ['class' => 'select-field', 'id'=>'full']) }}

                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;">
                        <p>
                            The teams goalies must play in a minimum number of games per week before their stats are included in the stat calculation. If a team does
                            not reach this minimum they will receive zeroes for all goalie categories that week.
                        </p>
                    </div>
                </div>

                <h3 class="pool-h2h pool-standard">Acquisition Settings</h3>
                <div class="form-group pool-h2h pool-standard">
                    <label><span class="main-label">Max Acquisitions Per Team </span><span class="horizontal-line"></span></label>
                    {{ Form::select('max_acquisition_team', config('pool.max_acquisition_team'), (isset($pool['max_acquisition_team'])?$pool['max_acquisition_team']:'-1'), ['class' => 'select-field max-acquisition-team','id'=>'full']) }}

                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;">
                        <p>
                            The limit of acquisitions a team can use over the entire season.
                        </p>
                    </div>
                </div>
                <div class="form-group pool-h2h pool-standard">
                    <label><span class="main-label">Max Acquisitions Per Week </span><span class="horizontal-line"></span></label>
                    {{ Form::select('max_acquisition_week', config('pool.max_acquisition_week'), (isset($pool['max_acquisition_week'])?$pool['max_acquisition_week']:'6'), ['class' => 'select-field max-acquisition-week', 'id'=>'full']) }}

                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;">
                        <p>
                            The limit of acquisitions a team can use over a weekly period.
                        </p>
                    </div>
                </div>
                <div class="form-group pool-h2h pool-standard">
                    <label><span class="main-label">Max Trades Per Season </span><span class="horizontal-line"></span></label>
                    {{ Form::select('max_trades_season', config('pool.max_trades_season'), null, ['class' => 'select-field max_trades_season', 'id'=>'full']) }}

                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;">
                        <p>
                            The number of trades a team is allowed over the entire season.
                        </p>
                    </div>
                </div>
                <?php $trade_end_date_default = config('pool.trade_deadline'); ?>

                <div class="form-group pool-h2h pool-standard">
                    <div>
                        <label><span class="main-label">Trade Deadline </span><span class="horizontal-line"></span></label>
                        {{ Form::text('trade_end_date', null, ['class' => 'datepicker text-field trade_end_date']) }}
                        <span class="input-group-addon calender-icon">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                        </span>
                    </div>
                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;">
                        <p>
                            The final day on which trades may be proposed. After this date no new trades may be created.
                        </p>
                    </div>
                </div>

                <div class="form-group pool-h2h pool-standard">
                    <label><span class="main-label">Trade Approval </span><span class="horizontal-line"></span></label>
                    {{ Form::select('trade_management', config('pool.pool_trade_managemnet_settings'), (isset($pool['pool_setting']['poolsetting']['trade_management'])?$pool['pool_setting']['poolsetting']['trade_management']:'0'), ['class' => 'select-field trade_management', 'id'=>'full']) }}
                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;">
                        <p>
                            Commissioner Decides - You will have the ability to cancel pending trades.<br><br>
                            League Votes - Pool members vote on trades to either accept or reject them.  If 50% or more of the pool members vote to reject a trade, the trade is canceled.

                        </p>
                    </div>
                </div>

                <div class="form-group pool-h2h pool-standard hide">
                    <label><span class="main-label">Allow Trading Draft Picks </span><span class="horizontal-line"></span></label>
                    {{ Form::checkbox('trading_draft_pick', (isset($pool['trading_draft_pick'])?$pool['trading_draft_pick']:'0'), false, ['class' => 'switch-input checkbox-field']) }}
                    <label class="switch switch-blue">
                        <input type="checkbox" class="switch-input" value='0'>
                        <span class="switch-label" data-on="Yes" data-off="No"></span>
                        <span class="switch-handle"></span>
                    </label>
                </div>

                <div class="form-group pool-h2h pool-standard">
                    <label><span class="main-label">Trade Reject Time </span><span class="horizontal-line"></span></label>
                    {{ Form::select('trade_reject_time', config('pool.trade_reject_time'), (isset($pool['trade_reject_time'])?$pool['trade_reject_time']:'2'), ['class' => 'select-field trade_reject_time', 'id'=>'full']) }}

                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;">
                        <p>
                            The number of days it takes for a trade to be processed.
                        </p>
                    </div>
                </div>
                <div class="form-group pool-h2h pool-standard">
                    <label><span class="main-label">Waiver Mode </span><span class="horizontal-line"></span></label>
                    {{ Form::select('waiver_mode', config('pool.waiver_mode'), (isset($pool['waiver_mode'])?$pool['waiver_mode']:'1'), ['class' => 'select-field waiver-mode', 'id'=>'full']) }}

                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;">
                        <p>
                            Standard - Includes a waiver system and waiver priority.
                        </p>
                    </div>
                </div>
                <div class="form-group pool-h2h pool-standard">
                    <label><span class="main-label">Waiver Time </span><span class="horizontal-line"></span></label>
                    {{ Form::select('waiver_time', config('pool.waiver_time'), null, ['class' => 'select-field waiver-time', 'id'=>'full']) }}

                    <span id="question-mark" class="question-mark">?</span>
                    <div class="pool-format-description" style="display: none;"> 
                        <p>
                            The number of days that a player will be on waivers until the waiver is processed or the player is released to free agents.
                        </p>
                    </div>
                </div>
                <div class="form-group pool-h2h pool-standard live-draft-div playoff-format-div">
                    <label><span class="main-label">Live Draft </span><span class="horizontal-line"></span></label>
                    {{ Form::checkbox('live_draft', (isset($pool['live_draft_setting'])?'1':'0'), true, ['class' => 'switch-input checkbox-field']) }}
                    <label class="switch switch-blue">
                        <input type="checkbox" class="switch-input" value="1">
                        <span class="switch-label" data-on="Yes" data-off="No"></span>
                        <span class="switch-handle"></span>
                    </label> 
                    <?php
                    $timezones = [];
                    $timezones[''] = 'Please Select a Timezone';
                    $timezones += timezones();
                    ?>
                    <div class="right">
                        <div class="relative">
                            <label><span class="main-label">Live Draft Day</span></label>
                            {{ Form::text('live_draft_date', (isset($pool['live_draft_setting']['date'])?date('m/d/Y', strtotime($pool['live_draft_setting']['date'])):null), ['class' => 'datepicker text-field live-draft','id'=>'live_draft_date']) }}
                            <span class="input-group-addon calender-icon">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </span>
                        </div>
                        <br>
                        <div>
                            <label class="team"><span class="main-label">Time Zone </span></label>
                            {{ Form::select('live_draft_time_zone', $timezones, (isset($pool['live_draft_setting']['time_zone'])?$pool['live_draft_setting']['time_zone']:null), ['class' => 'select-field live_draft_time_zone', 'id'=>'full']) }}
                        </div>
                        <br>
                        <div>
                            <label class="team"><span class="main-label">Live Draft Time </span></label>
                            {{ Form::select('live_draft_time', liveDraftTimeIntervals(), (isset($pool['live_draft_setting']['time'])?$pool['live_draft_setting']['time']:null), ['class' => 'select-field time-interval-append', 'id'=>'full']) }}
                        </div>
                    </div>
                </div>

                <button class="submit_btn btn-backgreen">Save and continue to teams <span class="arrow-right"></span></button>
                {{ Form::close() }}
            </div>
            <div class="row steps step-2" id='teams' data-step="2" style="display:none">
                <?php if (!empty($pool['all_pool_teams'])) { ?>
                    <h3>Edit teams</h3>
                <?php } else { ?>
                    <h3>Create teams</h3>  
                <?php } ?>
                <div class="extra">
                    <p class="invisible">a</p>
                </div>
                <div class="team-name">
                    <h4>Team Name</h4>
                </div>
                <div class="invitee-email-address">
                    <h4>Invitee Email Address</h4>
                </div> 
                {!! Form::open(['route' => 'frontend.pool.addTeams', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
                <!--Append team in edit-->
                <?php
                if (!empty($pool['all_pool_teams'])) {
                    $team_num = 0;
                    foreach ($pool['all_pool_teams'] as $team) {
                        ?>
                        <div class="form-group">
                            <div class="team-number"><p><?php echo $team_num + 1; ?>.</p></div>
                            <div class="team-name-input">
                                <input type="text" class="text-field" placeholder="Enter Team Name..." value="<?php echo $team['name'] ?>" disabled/>
                            </div>
                            <div class="invitee-email-input">
                                <input type="email" class="text-field" placeholder="Enter Invitee Email..." value="<?php echo (!empty($team['email']) ? $team['email'] : $team['user']['email']) ?>" disabled/>
                            </div>
                        </div>
                        <?php
                        $team_num++;
                    }
                    ?>
                <?php } ?>
                <!--END Append team in edit-->
                <script class="team-player-template" type="text/html">
                    <% for(var i = totalTeamEdit;i<=players_num; i++){ %>
                    <div class="form-group">
                        <div class="team-number"><p><%= i %>.</p></div>
                        <div class="team-name-input">
                            <input type="text" name="teams[<%= i %>][name]" class="text-field" placeholder="Enter Team Name..."/>
                        </div>
                        <div class="invitee-email-input">
                            <input type="email" name="teams[<%= i %>][email]" class="text-field" placeholder="Enter Invitee Email..."/>
                        </div>
                    </div>
                    <% } %>
                    </script>
                    <div class="team-player-append"></div>

                    <button class="submit_btn btn-backgreen">Save and continue to Rosters <span class="arrow-right"></span></button>
                    {{ Form::close() }}
                </div>
                <div class="row steps step-3" id="roster" data-step="3" style="display:none">
                    <h3>Roster Settings</h3>
                    {!! Form::open(['route' => 'frontend.pool.addRosters', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
                    <div class="rooster-settings">
                        <div class="row">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Position</th>
                                        <th>POS</th>
                                        <th>Roster Spots</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (empty($pool['pool_rosters'])) {
                                        $position_count = 1;
                                        foreach ($player_positions as $positions) {
                                            ?>
                                            <tr>
                                                <td><?php echo $positions->name; ?></td>
                                                <td><?php echo $positions->short_name; ?>
                                                    {{ Form::hidden('roster['.$position_count.'][player_position_id]', $positions->id) }}
                                                </td>
                                                <td>
                                                    {{ Form::number('roster['.$position_count.'][value]', null, ['class' => 'text-field roster_value', 'placeholder'=>'Set...','min'=>0,'max'=>30])}}
                                                </td>
                                            </tr>
                                            <?php
                                            $position_count = $position_count + 1;
                                        }
                                    } else {
                                        //edit roster data
                                        foreach ($pool['pool_rosters'] as $key => $roster) {
                                            ?>
                                            <tr>
                                                <td><?php echo $roster['name']; ?></td>
                                                <td><?php echo $roster['short_name']; ?>
                                                    {{ Form::hidden('roster['.$key.'][id]', $roster['id']) }}
                                                    {{ Form::hidden('roster['.$key.'][player_position_id]', $roster['player_position_id']) }}
                                                </td>
                                                <td>
                                                    {{ Form::number('roster['.$key.'][value]', $roster['value'], ['class' => 'text-field roster_value', 'placeholder'=>'Set...','min'=>0,'max'=>30])}}
                                                </td>
                                            </tr> 
                                            <?php
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <td>Total Team Size</td>
                                        <td></td> 
                                        <td>
                                            {{ Form::text('total_team_size', 0, ['class' => 'text-field total_team_size', 'readonly'=>true]) }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <button class="submit_btn btn-backgreen pull-left">Save and continue to Scoring <span class="arrow-right"></span></button>
                    {{ Form::close() }}
                </div>

                <div class="row steps step-4" data-step="4" style="display:none">
                    <div class="step-4-header">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="row">
                                    <h3>Scoring Settings</h3>
                                    <p class="sub-head">Player Stat Categories</p>
                                </div>
                            </div>
                            <div class="col-sm-7 info-content">
                                <i class="red-warning"></i>
                                <p>Points are cumulative</p>
                                <span>Eg A powerplay goal will count for 1G + 1S + 1PPG + 1P + 1PPP</span>
                            </div>
                        </div>
                    </div>
                    {!! Form::open(['route' => 'frontend.pool.addPoolScoring', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
                    <div class="scoring_settings"> 
                        <div class="row"> 
                            <table class="table"> 
                                <thead>
                                    <tr>
                                        <th class="scoring-player">0</th>
                                        <th>STAT</th>
                                        <th>Title</th>
                                        <th>% Used</th>
                                        <th class="point-value">Point Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $pool_scoring_fields = $pool_scoring_fields->sortByDesc('used.skater_percent');
                                    $scoring_count = 1;
                                    foreach ($pool_scoring_fields as $scoring) {
                                        if ($scoring->type == config('pool.pool_scoring_field.inverse_type.player') || $scoring->type == config('pool.pool_scoring_field.inverse_type.both')) {
                                            ?>
                                            <tr>
                                                {{ Form::hidden('score_settings['.$scoring_count.'][pool_scoring_field_id]', $scoring->id) }}
                                                {{ Form::hidden('score_settings['.$scoring_count.'][type]', config('pool.pool_scoring_field.inverse_type.player')) }}
                                                <td class="squaredThree">
                                                    <input type="checkbox" id="squaredThree<?php echo $scoring_count; ?>" name="score_settings[<?php echo $scoring_count; ?>][check]" value="1" class="scoring-player-checkbox"/>
                                                    <label for="squaredThree<?php echo $scoring_count; ?>"></label> 
                                                </td>
                                                <td><?php echo $scoring->stat; ?></td>
                                                <td><?php echo $scoring->title; ?></td>
                                                <td><?php echo (isset($scoring->used->skater_percent) && $scoring->used->skater_percent != NULL) ? $scoring->used->skater_percent : 0; ?>%</td>
                                                <td class="point-value">
                                                    {{ Form::number('score_settings['.$scoring_count.'][value]', '0') }}
                                                    <span class="add-number"></span>
                                                    <span class="subtract-number"></span>
                                                </td>
                                            </tr>
                                            <?php
                                            $scoring_count++;
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <p class="sub-head">Goalie Stat Categories</p>
                            <table class="table"> 
                                <thead>
                                    <tr>
                                        <th class="scoring-goalie">0</th>
                                        <th>STAT</th>
                                        <th>Title</th>
                                        <th>% Used</th>
                                        <th class="point-value">Point Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $pool_scoring_fields = $pool_scoring_fields->sortByDesc('used.goalie_percent');
                                    foreach ($pool_scoring_fields as $scoring) {
                                        if ($scoring->type == config('pool.pool_scoring_field.inverse_type.goalie') || $scoring->type == config('pool.pool_scoring_field.inverse_type.both')) {
                                            ?>
                                            <tr>
                                                {{ Form::hidden('score_settings['.$scoring_count.'][pool_scoring_field_id]', $scoring->id) }}
                                                {{ Form::hidden('score_settings['.$scoring_count.'][type]', config('pool.pool_scoring_field.inverse_type.goalie')) }}
                                                <td class="squaredThree">
                                                    <input type="checkbox" id="squaredThree<?php echo $scoring_count; ?>" class="scoring-goalie-checkbox" name="score_settings[<?php echo $scoring_count; ?>][check]" value="1"/>
                                                    <label for="squaredThree<?php echo $scoring_count; ?>"></label> 
                                                </td>
                                                <td><?php echo $scoring->stat; ?></td>
                                                <td><?php echo $scoring->title; ?></td>
                                                <td><?php echo (isset($scoring->used->goalie_percent) && $scoring->used->goalie_percent != NULL) ? $scoring->used->goalie_percent : 0; ?>%</td>
                                                <td class="point-value">
                                                    <?php
                                                    if ($scoring->stat == config('pool.pool_creator_scoring_stats_values.0.stat')) {
                                                        $stat_value = config('pool.pool_creator_scoring_stats_values.0.value');
                                                    } else {
                                                        $stat_value = 0;
                                                    }
                                                    ?>
                                                    {{ Form::number('score_settings['.$scoring_count.'][value]', $stat_value) }}
                                                    <span class="add-number"></span>
                                                    <span class="subtract-number"></span>
                                                </td>
                                            </tr>
                                            <?php
                                            $scoring_count++;
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="row helper-index">
                            <span class="yellow-bg scoring-player">0</span>
                            <p>Player Stat Categories</p>
                        </div>
                        <div class="row helper-index">
                            <span class="yellow-bg scoring-goalie">0</span>
                            <p>Goalie Stat Categories</p>
                        </div>
                        <div class="row helper-index">
                            <span class="black-bg scoring-total">0</span>
                            <p class="dark-text"><b>Total</b></p>
                        </div>
                    </div>
                    <button class="submit_btn btn-backgreen">Save and continue to Draft Setup <span class="arrow-right"></span></button>
                    {{ Form::close() }}
                </div>
                <div class="row steps step-5" id="draft_order" data-step="5" style="display:none">
                    <div class="step-5-header">
                        <div class="row">
                            <h3>Draft Order Selection</h3>
                        </div>
                    </div>
                    {!! Form::open(['route' => 'frontend.pool.addDraft', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
                    <div class="draft-order"> 
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <?php if (!empty($pool['all_pool_teams'])) { ?>
                                        <div class="team-number">
                                            <ul>
                                                <?php foreach ($pool['all_pool_teams'] as $key => $team) { ?>
                                                    <li><span><?php echo $key + 1; ?></span></li>
                                                <?php } ?>   
                                            </ul>
                                        </div>
                                    <?php } ?>   
                                    <?php if (!empty($pool['all_pool_teams'])) { ?>
                                        <ul id="draggable" class="predraft-draggable">
                                            <?php
                                            $sort_count = 0;
                                            foreach ($pool['all_pool_teams'] as $key => $team) {
                                                ?>
                                                <li>
                                                    <div class="team-name">
                                                        <input type="hidden" name="pool_draft[<?php echo $key; ?>][pool_team_id]" value="<?php echo $team['id']; ?>"/>
                                                        <input type="hidden" name="pool_draft[<?php echo $key; ?>][sort_order]" value="<?php
                                                        if (empty($team['sort_order'])) {
                                                            echo $sort_count;
                                                        } else {
                                                            echo $team['sort_order'];
                                                        }
                                                        ?>"/>
                                                        <h2><?php echo $team['name']; ?></h2>
                                                        <p>
                                                            <?php
                                                            if (!empty($team['email'])) {
                                                                echo $team['email'];
                                                            } else {
                                                                echo $team['user']['email'];
                                                            }
                                                            ?>
                                                        </p>
                                                        <i class="fa fa-arrows" aria-hidden="true"></i>
                                                    </div>
                                                </li>
                                                <?php
                                                $sort_count++;
                                            }
                                            ?>
                                        </ul>
                                    <?php } ?>                                        

                                    <div class="team-number">
                                        <script class="draft-order-count-template" type="text/html">
                                            <ul>
                                                <% for(var i=1;i<=totalLength;i++){ %>
                                                <li><span><%= i + parseInt(predraftCount) %></span></li>
                                                <% } %>
                                            </ul>
                                            </script>
                                            <div class="draft-order-count-append"></div>
                                        </div>

                                        <script class="draft-order-team-template" type="text/html">
                                            <ul id="draggable" class="predraft-draggable">
                                                <%  var count = 1;
                                                if(poolTeams.length > 0){
                                                _.each(poolTeams, function(v){ %>
                                                <li>
                                                    <div class="team-name">
                                                        <input type="hidden" name="pool_draft[<%= count %>][pool_team_id]" value="<%= v.id %>"/>
                                                        <input type="hidden" name="pool_draft[<%= count %>][sort_order]" value="<%= count %>"/>
                                                        <h2><%= v.name %></h2>
                                                        <p>
                                                            <% if(v.email != null){ %>
                                                            <%= v.email %>
                                                            <% }else{ %>
                                                            <%= v.user.email %>
                                                            <% } %>
                                                        </p>
                                                        <i class="fa fa-arrows" aria-hidden="true"></i>
                                                    </div>
                                                </li>
                                                <% count++; }); } %>
                                            </ul>
                                            </script>
                                            <div class="draft-order-team-append"></div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="submit_btn btn-backgreen">Save and Draft Players <span class="arrow-right"></span></button>
                            <button class="btn btn-green-line pull-right" id="randomise-list">Randomize</button>
                            {{ Form::close() }}
                        </div>
                        <div class="row steps step-6" id="draft_rounds" data-step="6" style="display:none">
                            <div class="step-6-header">
                                <div class="row">
                                    <h3>Player Draft</h3>
                                </div>
                            </div>
                            <div class="draft-round">
                                <div class="row">
                                    {!! Form::open(['route' => 'frontend.pool.addRoundPlayers', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
                                    <a href="{{route('frontend.player.search')}}" class="hidden search_player_action"></a>

                                    <?php
                                    if (!empty($pool['all_pool_teams'])) {
                                        //check if user has filled till predraft
                                        if ($pool['all_pool_teams'][0]['sort_order'] != null) {
                                            foreach ($pool['pool_rounds'] as $key => $rounds) {
//                                                if (empty($rounds['pool_team_players'])) {

                                                    $check_even = (($key + 1) % 2);
                                                    if ($check_even === 0) {
                                                        $pool_teams = array_reverse($pool['all_pool_teams']);
                                                    } else {
                                                        if ($check_even != 1) {
                                                            $pool_teams = array_reverse($pool['all_pool_teams']);
                                                        } else {
                                                            $pool_teams = $pool['all_pool_teams'];
                                                        }
                                                    }
                                                    ?>
                                                    <div class="each-round">
                                                        <h3><?php echo $rounds['name']; ?></h3>
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Pick</th>
                                                                    <th>Team Name</th> 
                                                                    <th>Player</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                foreach ($pool_teams as $player_key => $player) {
                                                                    if (empty($player['user'])) {
                                                                        ?>
                                                                        <tr>
                                                                            <td><?php echo $player_key + 1; ?></td>
                                                                            <td><?php echo $player['name']; ?></td>
                                                                            <td>
                                                                                <input type="hidden" name="pool_team_data[<?php echo $player['id']; ?>][pool_invitation_players][<?php echo $key + 1; ?>][pool_team_id]" value="<?php echo $player['id']; ?>"/>
                                                                                <input type="hidden" name="pool_team_data[<?php echo $player['id']; ?>][pool_invitation_players][<?php echo $key + 1; ?>][round_id]" value="<?php echo $rounds['id']; ?>"/>
                                                                                <input type="hidden" name="pool_team_data[<?php echo $player['id']; ?>][pool_invitation_players][<?php echo $key + 1; ?>][player_id]" value="<?php if(!empty($rounds['pool_team_players'][$player_key])) { echo $rounds['pool_team_players'][$player_key]['player_season']['player']['id']; } ?>"/>

                                                                                <input type="text" class="text-field search_player search_player_save" placeholder="Type player name" value="<?php if(!empty($rounds['pool_team_players'][$player_key])) { echo $rounds['pool_team_players'][$player_key]['player_season']['player']['full_name'].'('.$rounds['pool_team_players'][$player_key]['player_season']['player_position']['short_name'].' - '.$rounds['pool_team_players'][$player_key]['player_season']['team']['short_name'].')'; } ?>" required />
                                                                                <ul class="search_player_ul"></ul>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } else { ?>
                                                                        <tr>
                                                                            <td><?php echo $player_key + 1; ?></td>
                                                                            <td><?php echo $player['name']; ?></td>
                                                                            <td>
                                                                                <input type="hidden" name="pool_team_data[<?php echo $player['id']; ?>][pool_team_players][<?php echo $key + 1; ?>][pool_team_id]" value="<?php echo $player['id']; ?>"/>
                                                                                <input type="hidden" name="pool_team_data[<?php echo $player['id']; ?>][pool_team_players][<?php echo $key + 1; ?>][round_id]" value="<?php echo $rounds['id']; ?>"/>
                                                                                <input type="hidden" name="pool_team_data[<?php echo $player['id']; ?>][pool_team_players][<?php echo $key + 1; ?>][player_id]" value="<?php if(!empty($rounds['pool_team_players'][$player_key])) { echo $rounds['pool_team_players'][$player_key]['player_season']['player']['id']; } ?>"/>

                                                                                <input type="text" class="text-field search_player search_player_save" placeholder="Type player name" value="<?php if(!empty($rounds['pool_team_players'][$player_key])) { echo $rounds['pool_team_players'][$player_key]['player_season']['player']['full_name'].'('.$rounds['pool_team_players'][$player_key]['player_season']['player_position']['short_name'].' - '.$rounds['pool_team_players'][$player_key]['player_season']['team']['short_name'].')'; } ?>" required />
                                                                                <ul class="search_player_ul"></ul>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <?php
//                                                }
                                            }
                                        } else {
                                            if (!empty($pool['pool_rounds'])) {
                                                foreach ($pool['pool_rounds'] as $key => $rounds) {
                                                    if (!empty($rounds['pool_team_players'])) {
                                                        $check_even = (($key + 1) % 2);
                                                        if ($check_even === 0) {
                                                            $round_players = array_reverse($pool['all_pool_teams']);
                                                        } else {
                                                            if ($check_even != 1) {
                                                                $round_players = array_reverse($pool['all_pool_teams']);
                                                            } else {
                                                                $round_players = $pool['all_pool_teams'];
                                                            }
                                                        }
                                                        ?>
                                                        <div class="each-round">
                                                            <h3><?php echo $rounds['name']; ?></h3>
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Pick</th>
                                                                        <th>Team Name</th>
                                                                        <th>Player</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php foreach ($round_players as $player_key => $player) { ?>
                                                                        <tr>
                                                                            <td><?php echo $player_key + 1; ?></td>
                                                                            <td><?php echo $player['pool_team']['name']; ?></td>
                                                                            <td>
                                                                                <input type="hidden" name="pool_team_data[<?php echo $player['pool_team_id']; ?>][pool_team_players][<?php echo $key + 1; ?>][pool_team_id]" value="<?php echo $player['pool_team_id']; ?>"/>
                                                                                <input type="hidden" name="pool_team_data[<?php echo $player['pool_team_id']; ?>][pool_team_players][<?php echo $key + 1; ?>][round_id]" value="<?php echo $player['pool_round_id']; ?>"/>
                                                                                <input type="hidden" name="pool_team_data[<?php echo $player['pool_team_id']; ?>][pool_team_players][<?php echo $key + 1; ?>][player_id]" value="<?php echo $player['player_season']['player']['id']; ?>"/>

                                                                                <input type="text" class="text-field search_player search_player_save" placeholder="Type player name" value="<?php echo $player['player_season']['player']['full_name']; ?>" required/>
                                                                                <ul class="search_player_ul"></ul>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    ?>

                                    <script class="each-round-template" type="text/html">
                                        <% if(roundList.length > 0){
                                        for(var i=1;i<=roundList.length;i++){

                                        var checkEven = parseInt(i) % 2;
                                        if(checkEven === 0){
                                        teamList = teamList.reverse();
                                        }else{
                                        if(i != 1){
                                        teamList = teamList.reverse();
                                        }
                                        }
                                        %>
                                        <div class="each-round">
                                            <h3>Round <%= i %>
                                            </h3>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Pick</th>
                                                        <th>Team Name</th>
                                                        <th>Player</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <% 
                                                    var roundNumber = parseInt(i) - 1,
                                                    roundId = roundList[roundNumber]['id'],
                                                    count = 1;
                                                    if(teamList.length > 0){
                                                    _.each(teamList, function(v){
                                                    if(v.check == 0){
                                                    %>
                                                    <tr>
                                                        <td><%= count %></td>
                                                        <td><%= v.name %></td>
                                                        <td>
                                                            <input type="hidden" name="pool_team_data[<%= v.id %>][pool_invitation_players][<%= i %>][round_id]" value="<%= roundId %>"/>
                                                            <input type="hidden" name="pool_team_data[<%= v.id %>][pool_invitation_players][<%= i %>][pool_team_id]" value="<%= v.id %>"/>
                                                            <input type="hidden" name="pool_team_data[<%= v.id %>][pool_invitation_players][<%= i %>][player_id]"/>

                                                            <input type="text" class="text-field search_player search_player_save" placeholder="Type player name" required/>
                                                            <ul class="search_player_ul"></ul>
                                                        </td>
                                                    </tr>
                                                    <% }else{ %>
                                                    <tr>
                                                        <td><%= count %></td>
                                                        <td><%= v.name %></td>
                                                        <td>
                                                            <input type="hidden" name="pool_team_data[<%= v.id %>][pool_team_players][<%= i %>][round_id]" value="<%= roundId %>"/>
                                                            <input type="hidden" name="pool_team_data[<%= v.id %>][pool_team_players][<%= i %>][pool_team_id]" value="<%= v.id %>"/>
                                                            <input type="hidden" name="pool_team_data[<%= v.id %>][pool_team_players][<%= i %>][player_id]"/>

                                                            <input type="text" class="text-field search_player search_player_save" placeholder="Type player name" required/>
                                                            <ul class="search_player_ul"></ul>
                                                        </td> 
                                                    </tr>
                                                    <% } count++; }); } %>
                                                </tbody> 
                                            </table>
                                        </div>
                                        <% } } %>
                                        </script>
                                        <div class="all-round-append clearfix"></div>
                                        <button class="submit_btn btn-backgreen"> Launch Pool <span class="arrow-right"></span></button>
                                        <div class="col-sm-6 info-content">
                                            <i class="red-warning"></i>
                                            <span>Invitations will be emailed to pool entrants</span>
                                        </div>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                            <div class="row steps step-7" data-step="7" id="boxes" style="display:none">
                                <div class="step-7-header">
                                    <div class="row">
                                        <h3>Boxes</h3>
                                    </div>
                                </div>
                                <div class="boxes">
                                    <div class="row">
                                        {!! Form::open(['route' => 'frontend.pool.addBoxPlayers', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}
                                        <a href="{{route('frontend.player.search')}}" class="hidden search_player_action"></a>

                                        <?php
//if pool boxes have been filled
                                        if (!empty($pool['pool_boxes'])) {
                                            foreach ($pool['pool_boxes'] as $key => $boxes) {
                                                if (!empty($boxes['pool_box_players'])) {
                                                    ?>
                                                    <div class="each-box"> 
                                                        <h3>
                                                            <?php echo $boxes['name']; ?>
                                                            <input type="hidden" value="<?php echo $boxes['id']; ?>"/>
                                                            <input type="hidden" name="box[<?php echo $key + 1; ?>][name]" value="<?php echo $boxes['name']; ?>"/>
                                                        </h3>
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th>Pick</th>
                                                                    <th>Player</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($boxes['pool_box_players'] as $box_key => $box) { ?>
                                                                    <tr>
                                                                        <td><?php echo $box_key + 1; ?></td>
                                                                        <td>
                                                                            <input type="hidden" class="text-field" name="box[<?php echo $key + 1; ?>][players][<?php echo $box_key + 1; ?>][player_id]" value="<?php echo $box['player_season']['player_id']; ?>"/>
                                                                            <input type="text" class="text-field search_player" name="" placeholder="Find player" value="<?php echo $box['player_season']['player']['full_name']; ?>" required/>
                                                                            <ul class="search_player_ul"></ul>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                        } else {
                                            //checking if user had filled till scoring
                                            if (empty($pool['pool_rounds']) && !empty($pool['pool_score_settings'])) {
                                                foreach ($pool['pool_rosters'] as $key => $rosters) {
                                                    if ($rosters['value'] == 1) {
                                                        ?>
                                                        <div class = "each-box">
                                                            <h3>
                                                                <?php echo $rosters['name']; ?>
                                                                <input type="hidden" value="<?php echo $rosters['player_position_id']; ?>"/>
                                                                <input type="hidden" name="box[<?php echo $key + 1; ?>][name]" value="<?php echo $rosters['name']; ?>"/>
                                                            </h3>
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Pick</th>
                                                                        <th>Player</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php for ($i = 1; $i <= $rosters['value']; $i++) { ?>
                                                                        <tr>
                                                                            <td><?php echo $i; ?></td>
                                                                            <td>
                                                                                <input type="hidden" class="text-field" name="box[<?php echo $key + 1; ?>][players][<?php echo $i; ?>][player_id]"/>
                                                                                <input type="text" class="text-field search_player" name="" placeholder="Find player" required/>
                                                                                <ul class="search_player_ul"></ul>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                        ?>

                                        <script class="each-box-template" type="text/html">
                                            <% var count = 1;
                                            if(rosterSpots.length > 0){
                                            _.each(rosterSpots, function(v){  
                                            if(v.rosterCount > 0){ 
                                            for(var rnum = 1;rnum <= v.rosterCount; rnum++){ %>
                                            <div class="each-box"> 
                                                <h3>
                                                    <%= v.position %> <%= rnum %>
                                                    <input type="hidden" value="<%= v.player_position_id %>"/>
                                                    <input type="hidden" name="box[<%= count %>][name]" value="<%= v.position %> <%= rnum %>"/>
                                                </h3>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Pick</th>
                                                            <th>Player</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <% for(var i=1;i<=5;i++){ %>
                                                        <tr>
                                                            <td><%= i %></td>
                                                            <td>
                                                                <input type="hidden" class="text-field" name="box[<%= count %>][players][<%= i %>][player_id]"/>
                                                                <input type="text" class="text-field search_player" name="" placeholder="Find player" required/>
                                                                <ul class="search_player_ul"></ul>
                                                            </td>
                                                        </tr>
                                                        <% } %>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <% count++; } } }); } %>
                                            </script>
                                            <div class="all-boxes-append clearfix"></div>
                                            <button class="submit_btn btn-backgreen">Save and Review Pool <span class="arrow-right"></span></button>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row steps step-8" data-step="8" id="review" style="display:none">
                                    <div class="step-8-header">
                                        <div class="row">
                                            <h2>Your pool has been created!</h2>
                                        </div>
                                    </div>
                                    <div class="review">
                                        <div class="row">
                                            <h3>Summary</h3>
                                            <div class="review-table">
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <td>Players</td>
                                                            <td class="players">
                                                                <?php
                                                                if (!empty($pool_player_rosters)) {
                                                                    echo $pool_player_rosters;
                                                                } else {
                                                                    echo '0';
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Goalies</td>
                                                            <td class="goalie">
                                                                <?php
                                                                if (isset($pool_goalie_rosters) && !empty($pool_goalie_rosters)) {
                                                                    echo $pool_goalie_rosters;
                                                                } else {
                                                                    echo '0';
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Boxes</td>
                                                            <td class="total">
                                                                <?php
                                                                if (!empty($total_pool_rosters)) {
                                                                    echo $total_pool_rosters;
                                                                } else {
                                                                    echo '0';
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="clearfix"></div>
                                            <h2>Invite link:</h2>
                                            <?php if (!empty($pool['invite_key'])) { ?>
                                                <a href="{{ route('frontend.pool.invite', $pool['invite_key']) }}" class="invite_link" target="_blank"><?php echo route('frontend.pool.invite', $pool['invite_key']); ?></a>
                                            <?php } else { ?>
                                                <a href="" class="invite_link" target="_blank">link</a>
                                            <?php } ?>
                                            <p class="description">
                                                Send the "INVITE LINK" to anyone that is joining the pool. They will just need to click the link, create their account (if they are new), then setup their box in three simple steps. "
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @stop 
                    @section('after-scripts-end')
                    {!! Html::script(elixir('js/pool_creator.js')) !!}
                    <script>
                        var poolEditableSettings = <?php echo json_encode(config('pool.pool_editable_settings')); ?>,
                         poolSeasonSettings = <?php echo json_encode(config('pool.inverse_season')); ?>,
                                poolEditdata = <?php echo json_encode($pool); ?>,
                                poolDashboardUrl = '{{route('frontend.pool.dashboard', 0)}}',
                                addUniqueRoundPlayerUrl = '{{route('frontend.pool.addUniqueRoundPlayers')}}',
                                urlPoolType = {{(!empty($pool['pool_type'])?$pool['pool_type']:'1')}},
                                maxLivedraftDate = moment("{{ $current_season_end_date['end_date']->addDay()->toDateString() }}").format('L'),
                                timeIntervals = <?php echo json_encode(liveDraftTimeIntervals()); ?>;
                        timeZones = <?php echo json_encode(timezones()); ?>;
                        
                        //Trade Deadline datetimepicker intialize
                        $(".trade_end_date").datetimepicker({format: 'MM/DD/YYYY'});
                        $('.trade_end_date').data("DateTimePicker").date(new Date('<?php echo (isset($pool['trade_end_date']) ? date('m/d/Y', strtotime($pool['trade_end_date'])) : $trade_end_date_default); ?>'));
                        //Add value to live draft checkbox
                        setTimeout(function () {
<?php if (isset($pool['playoff_teams_number']) && $pool['playoff_teams_number'] != 0) { ?>
                            $('.playoff-format-div').first().find('.switch-blue input[type="checkbox"]').click();
<?php } ?>
<?php if (!empty($pool['live_draft_setting'])) { ?>
                            $('.live-draft-div .switch-blue input[type="checkbox"]').click();
<?php } ?>
                        }, 350);
                        //Live draft date datetimepicker intialize
                        $("#live_draft_date").datetimepicker({format: 'MM/DD/YYYY'});
                        $('#live_draft_date').data("DateTimePicker").date(new Date('<?php echo (isset($pool['live_draft_setting']['date']) ? date('m/d/Y', strtotime($pool['live_draft_setting']['date'])) : null); ?>'));
                    </script>
                    @stop
