@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/my_team.css')) !!}
@stop 
@section('content')
<?php // dd($pool_details->toArray());
?>
<div class="mypool-teams container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id='pool--content'>
        @include('frontend.includes.pool_nav')
        <div class="pool--content">
            <div class="bg-trans" style="display: none"></div>
            @include('frontend.includes.pool_name_header')

            <div class="pool-team-container">
                <?php
                $today = strtotime(date("Y-m-d"));
                $today_date = date("Y-m-d");
                ?>
                <div class="league-nav" id="league-nav">
                    <ul>
                        <li class="team-datepicker" data-start_date="<?php echo $today_date; ?>"  data-end_date="<?php echo $today_date; ?>">
                            <input type="text" id="team-datepicker" value="<?php echo date("m/d/Y", $today); ?>"/>
                            <span class="input-group-addon">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                            </span>
                        </li>
                        <li data-start_date="<?php echo $today_date; ?>" data-end_date="<?php echo $today_date; ?>">
                            <a>Today</a>
                        </li>
                        <li data-start_date="<?php echo date("Y-m-d", strtotime('monday this week')); ?>" data-end_date="<?php echo $today_date; ?>">
                            <a>Week</a>
                        </li>
                        <li data-end_date="<?php echo date("Y-m-d", $today); ?>" data-start_date="<?php echo date("Y-m-d", strtotime("-1 week", $today)); ?>">
                            <a>Last 7 Days</a>
                        </li>
                        <li data-end_date="<?php echo date("Y-m-d", $today); ?>" data-start_date="<?php echo date("Y-m-d", strtotime("-30 days", $today)); ?>">
                            <a>Last 30 Days</a>
                        </li>
                        <li data-start_date="<?php echo date("Y-m-d", strtotime($season->start_date)); ?>" data-end_date="<?php echo date("Y-m-d", strtotime($season->end_date)); ?>">
                            <a>Season</a>
                        </li>
                    </ul>
                </div>

                <script id="all-players-template" type="text/html">
                    <table class="table light-grey-table flip-scroll-table" id="rosters-players">
                        <thead>
                            <tr>
                                <th colspan="4">Roster Players</th>
                                <th colspan="2">Ranking</th>
                                <th colspan="2">Trend</th>
                                <% var poolScoreSettingsLength = 0;
                                _.each(poolScoreSettings, function(v){ 
                                if(v.type == poolScoringFields.inverse_type.player){
                                poolScoreSettingsLength = parseInt(poolScoreSettingsLength) + 1;
                                } }); %>
                                <th colspan="<%= poolScoreSettingsLength %>">Stats</th>
                                <th class="total-pool-points">Actual</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="sub-header"> 
                                <td>Status</td>
                                <td>Pos</td>
                                <td>Skaters</td>
                                <td>Opp</td>
                                <td>Pre</td>
                                <td>Current</td>
                                <td>Own</td>
                                <td>Start</td>
                                <% _.each(poolScoreSettings, function(v){ 
                                if(v.type == poolScoringFields.inverse_type.player){
                                %>
                                <td><%= v.pool_scoring_field.stat %></td>
                                <% } }); %>
                                <td class="total-pool-points">Pool Pts</td>
                            </tr>
                            <% _.each(playersTeam, function(value){
                            if(value.player_season.player_position.name != poolScoringFields.type[2] ){ %>

                            <% 
                            //Checking if user is on bench
                            if(!$.isEmptyObject(value.pool_team_lineup)){ 
                            var statusShortName = value.pool_team_lineup.player_position.short_name;
                            if(value.pool_team_lineup.player_position.id == benchId){
                            var addClassStatus = 'bench';
                            }else{
                            var addClassStatus = '';
                            }
                            }else{
                            var statusShortName = '';
                            } %>

                            <%
                            //Checking if match is going to start in 5 min.
                            var addClassMatch = '';
                            if(!$.isEmptyObject(value.player_season.team.home_competition)){
                            var mmt = moment(), //your moment
                            mmtGame = moment.tz(new Date(value.player_season.team.home_competition.match_start_date), gameTimezone), //moment of game
                            minutesLeft = mmt.diff(mmtGame, 'minutes'),
                            minutesLeft = minutesLeft + '',
                            minutesLeft = minutesLeft.replace('-', '');

                            if(minutesLeft <= 5){
                            var addClassMatch = 'match-start';
                            }
                            } else if(!$.isEmptyObject(value.player_season.team.away_competition)){
                            var mmt = moment(), //your moment
                            mmtGame = moment.tz(new Date(value.player_season.team.away_competition.match_start_date), gameTimezone), //moment of game
                            minutesLeft = mmt.diff(mmtGame, 'minutes'),
                            minutesLeft = minutesLeft + '',
                            minutesLeft = minutesLeft.replace('-', '');

                            if(minutesLeft <= 5){
                            var addClassMatch = 'match-start';
                            }
                            }
                            %>

                            <%
                            //Check if lineupDate is before today
                            var lineupMoment = moment(lineupDate).tz('<?php echo Config('app.timezone_for_calculation'); ?>'),
                            todayMoment = moment(new Date()).tz('<?php echo Config('app.timezone_for_calculation'); ?>');
                            if(lineupMoment.isBefore(todayMoment)){
                            var addClassMatch = 'match-start';
                            }

                            //check if h2h weekly and day is monday then you can exchange players 
                            if(poolToShowCat == 12){
                            //checking if lineup is within current week
                            var lineupMonday = lineupMoment.startOf('isoweek'),
                            todayMonday = todayMoment.startOf('isoweek');
                            if(lineupMonday.diff(todayMonday) == 0){
                            if(lineupMoment.day() != 1){ //checking if today is not monday
                            var addClassMatch = 'match-start';
                            }
                            }
                            }
                            %>
                            <tr class="pos-<%= value.player_season.player_position.id %> <%= addClassStatus %> <%= addClassMatch %>" data-bench-check="<%= value.player_season.player_position.id %>" data-pos="<%= value.pool_team_lineup.player_position.id %>" data-send-pos="<%=  value.pool_team_lineup.player_position.id %>"  data-player-id="<%= value.pool_team_lineup.pool_team_player_id %>">
                                <td><div class="status-mover"><%= statusShortName %></div></td> 
                                <td><%= value.player_season.player_position.short_name %></td>
                                <td><%= value.player_season.player.full_name + ' (' + value.player_season.team.short_name + ')' %></td>
                                <td><% var oppPresent = false; 
                                    if(!$.isEmptyObject(value.player_season.team.home_competition)){
                                    oppPresent = true; %>
                                    <%= '@'+value.player_season.team.home_competition.away_team.short_name %>
                                    <% }
                                    if(!$.isEmptyObject(value.player_season.team.away_competition)){ 
                                    oppPresent = true; %>
                                    <%= '@'+value.player_season.team.away_competition.home_team.short_name %>
                                    <% } 
                                    if(oppPresent == false){ %>
                                    -
                                    <% } %>
                                </td>
                                <td><%= value.player_season.player.player_pre_ranking?value.player_season.player.player_pre_ranking.rank:0 %></td>
                                <td><%= value.player_season.player.pool_season_rank?value.player_season.player.pool_season_rank.rank:0 %></td>
                                <td><%= value.own?value.own:0 %>%</td>
                                <td><%= value.start?value.start:0 %>%</td>
                                <% _.each(poolScoreSettings, function(scoreSettingValue){ 
                                if(scoreSettingValue.type == poolScoringFields.inverse_type.player){
                                %>
                                <td id="con-<%= value.id %>-<%= scoreSettingValue.pool_scoring_field_id %>">0</td>
                                <% } }); %> 
                                <td id="con-<%= value.id %>-total_points" class="total-pool-points">0</td>
                            </tr>
                            <% } }); %> 
                            <tr class="total_row">
                                <td colspan="8">Starting Lineup Totals</td>
                                <% _.each(poolScoreSettings, function(scoreSettingValue){ 
                                if(scoreSettingValue.type == poolScoringFields.inverse_type.player){
                                %>
                                <td id="lineup-<%= poolTeam.id %>-<%= scoreSettingValue.pool_scoring_field_id %>">0</td>
                                <% } }); %> 
                                <td id="lineup-<%= poolTeam.id %>-total_points" class="total-pool-points">0</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table light-grey-table flip-scroll-table"  id="rosters-goalies">
                        <tbody>
                            <tr class="sub-header">
                                <td>Status</td>
                                <td>Pos</td>
                                <td>Skaters</td>
                                <td>Opp</td>
                                <td>Pre</td>
                                <td>Current</td>
                                <td>Own</td>
                                <td>Start</td>
                                <% _.each(poolScoreSettings, function(v){ 
                                if(v.type == poolScoringFields.inverse_type.goalie){
                                %>
                                <td><%= v.pool_scoring_field.stat %></td>
                                <% } }); %>
                                <td class="total-pool-points">Pool Pts</td>
                            </tr>
                            <% _.each(playersTeam, function(value){
                            if(value.player_season.player_position.name == poolScoringFields.type[2]){ %>
                            <% 
                            //Checking if user is on bench
                            if(!$.isEmptyObject(value.pool_team_lineup)){ 
                            var statusShortName = value.pool_team_lineup.player_position.short_name;
                            if(value.pool_team_lineup.player_position.id == benchId){
                            var addClassStatus = 'bench';
                            }else{
                            var addClassStatus = '';
                            }
                            }else{
                            var statusShortName = '';
                            } %>

                            <%
                            //Checking if match is going to start in 5 min.
                            var addClassMatch = '';
                            if(!$.isEmptyObject(value.player_season.team.home_competition)){
                            var mmt = moment(), //your moment
                            mmtGame = moment.tz(new Date(value.player_season.team.home_competition.match_start_date), gameTimezone), //moment of game
                            minutesLeft = mmt.diff(mmtGame, 'minutes'),
                            minutesLeft = minutesLeft + '',
                            minutesLeft = minutesLeft.replace('-', '');

                            if(minutesLeft <= 5){
                            var addClassMatch = 'match-start';
                            }
                            } else if(!$.isEmptyObject(value.player_season.team.away_competition)){
                            var mmt = moment(), //your moment
                            mmtGame = moment.tz(new Date(value.player_season.team.away_competition.match_start_date), gameTimezone), //moment of game
                            minutesLeft = mmt.diff(mmtGame, 'minutes'),
                            minutesLeft = minutesLeft + '',
                            minutesLeft = minutesLeft.replace('-', '');

                            if(minutesLeft <= 5){
                            var addClassMatch = 'match-start';
                            }
                            }
                            %>

                            <%
                            //Check if lineupDate is before today
                            var lineupMoment = moment(lineupDate).tz('<?php echo Config('app.timezone_for_calculation'); ?>'),
                            todayMoment = moment(new Date()).tz('<?php echo Config('app.timezone_for_calculation'); ?>');
                            if(lineupMoment.isBefore(todayMoment)){
                            var addClassMatch = 'match-start';
                            }

                            //check if h2h weekly and day is monday then you can exchange players 
                            if(poolToShowCat == 12){
                            //checking if lineup is within current week
                            var lineupMonday = lineupMoment.startOf('isoweek'),
                            todayMonday = todayMoment.startOf('isoweek');
                            if(lineupMonday.diff(todayMonday) == 0){
                            if(lineupMoment.day() != 1){ //checking if today is not monday
                            var addClassMatch = 'match-start';
                            }
                            }
                            }
                            %>
                            <tr class="pos-<%= value.player_season.player_position.id %> <%= addClassStatus %> <%= addClassMatch %>" data-bench-check="<%= value.player_season.player_position.id %>" data-pos="<%= value.pool_team_lineup.player_position.id %>" data-send-pos="<%= value.pool_team_lineup.player_position.id %>" data-player-id="<%= value.pool_team_lineup.pool_team_player_id %>">
                                <td><div class="status-mover"><%= statusShortName %></div></td> 
                                <td><%= value.player_season.player_position.short_name %></td>
                                <td><%= value.player_season.player.full_name + ' (' + value.player_season.team.short_name + ')' %></td>
                                <td><% var oppPresent = false; 
                                    if(!$.isEmptyObject(value.player_season.team.home_competition)){ 
                                    oppPresent = true; %>
                                    <%= '@'+value.player_season.team.home_competition.away_team.short_name %>
                                    <% }
                                    if(!$.isEmptyObject(value.player_season.team.away_competition)){ 
                                    oppPresent = true; %>
                                    <%= '@'+value.player_season.team.away_competition.home_team.short_name %>
                                    <% } 
                                    if(oppPresent == false){ %>
                                    -
                                    <% } %>
                                </td>
                                <td><%= value.player_season.player.player_pre_ranking?value.player_season.player.player_pre_ranking.rank:0 %></td>
                                <td><%= value.player_season.player.pool_season_rank?value.player_season.player.pool_season_rank.rank:0 %></td>
                                <td><%= value.own?value.own:0 %>%</td>
                                <td><%= value.start?value.start:0 %>%</td>
                                <% _.each(poolScoreSettings, function(scoreSettingValue){ 
                                if(scoreSettingValue.type == poolScoringFields.inverse_type.goalie){
                                %>
                                <td id="con-<%= value.id %>-<%= scoreSettingValue.pool_scoring_field_id %>">0</td>
                                <% } }); %> 
                                <td id="con-<%= value.id %>-total_points" class="total-pool-points">0</td>
                            </tr>
                            <% } }); %> 
                            <tr class="total_row">
                                <td colspan="8">Starting Lineup Totals</td>
                                <% _.each(poolScoreSettings, function(scoreSettingValue){ 
                                if(scoreSettingValue.type == poolScoringFields.inverse_type.goalie){
                                %>
                                <td id="lineup-<%= poolTeam.id %>-g-<%= scoreSettingValue.pool_scoring_field_id %>">0</td>
                                <% } }); %> 
                                <td id="lineup-<%= poolTeam.id %>-g-total_points" class="total-pool-points">0</td>
                            </tr>
                        </tbody>
                    </table>
                    </script>
                    <div id="all-players-outer">
                        <div id="circle-loader"> 
                            {{ Html::image('img/loading.gif', 'Hockeypool Loader') }}
                        </div>
                    </div>

                    <script id="empty-players-template" type="text/html">
                        <tr class="pos-<%= positionId %> empty <%= neededClass %>" data-pos="<%= positionId %>">
                            <td><div class="status-mover"><%= positionName %></div></td>
                            <td></td>
                            <td>(Empty)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <% _.each(poolScoreSettings, function(v){ 
                            if(v.type == poolScoringFields.inverse_type.player){
                            %>
                            <td></td>
                            <% } }); %>
                            <td class="total-pool-points"></td>
                        </tr>
                        </script>

                        <script id="empty-goalies-template" type="text/html">
                            <tr class="pos-<%= positionId %> empty <%= neededClass %>" data-pos="<%= positionId %>">
                                <td><div class="status-mover"><%= positionName %></div></td>
                                <td></td>
                                <td>(Empty)</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <% _.each(poolScoreSettings, function(v){ 
                                if(v.type == poolScoringFields.inverse_type.goalie){
                                %>
                                <td></td>
                                <% } }); %>
                                <td class="total-pool-points"></td>
                            </tr>
                            </script>
                        </div>

                    </div>
                </div>
            </div>

            <div class="container-fluid hide">
                <div class="row">
                    <div class="team-feed-bg">
                    </div>
                    <div class="adblocks">
                        @include('frontend.includes.adblocks')
                        <!--@include('frontend.includes.hockeypool_news')-->
                    </div>
                    <div class="container">
                        <div class="team-feeds">
                            <div class="row">
                                <p class="team-feed-heading">
                                    Team Feed
                                </p>
                                <div class="arrow-down"></div>
                                <?php for ($i = 1; $i < 5; $i++) { ?>
                                    <div class="single-team-feed">
                                        <h3 class="pool--main-heading">Goaltender Games <span class="list-element"></span> <span class="date">March 1, 2016</span></h3>
                                        <p class="sub-feed-heading">Licence To Ilya</p>
                                        <p class="main-text">Due to the specific HTML and CSS used to justify buttons (namely display: table-cell), the borders between them are doubled. In regular button groups, margin-left: -1px is used to stack the borders instead of removing them.</p>
                                        <div class="team-feed-comments">
                                            <div class="player-img">
                                                <img src="{{url('')}}/img/profile-pic1.jpg" class="img-responsive" alt="Player Img">
                                            </div>
                                            <div class="player-comment">
                                                <h3 class="player-name">
                                                    Ilya Kovalchuk <span class="date">March 1, 2016</span>
                                                </h3>
                                                <p class="main-text">Due to the specific HTML and CSS used to justify buttons (namely display: table-cell), the borders between them are doubled. In reg</p>
                                                <a href="#"><b>Read More</b></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $pool_weekly_win = false;
            if ($pool_details->pool_type == config('pool.inverse_type.h2h')) {
                if ($pool_details->poolSetting->poolsetting->pool_format == config('pool.inverse_format.h2h_weekly')) {
                    $pool_weekly_win = true;
                }
            }
            ?>
            @stop 
            @section('after-scripts-end')
            {!! Html::script(elixir('js/my_team.js')) !!}
            <script>
                var currentRoute = '<?php echo \Request::route()->getName(); ?>';
                if (currentRoute.indexOf('myTeamWithTeam') == - 1){
                var myTeamUrl = '{{route('frontend.pool.getDataForMyTeam', $pool_id)}}',
                        otherTeam = false;
                } else{
                var myTeamUrl = '{{route('frontend.pool.getDataForMyTeamWithTeam', [$pool_id, $pool_team_id])}}',
                        otherTeamId = '<?php echo $pool_team_id; ?>',
                        teamId = '<?php echo $pool_details->poolTeam->id; ?>',
                        otherTeam = true;
                }
                var changeLineupUrl = '{{route('frontend.pool.changeTeamLineup', $pool_id)}}',
                        startDate = '<?php echo date("Y-m-d", strtotime($season->start_date)); ?>',
                        endDate = '<?php echo date("Y-m-d", strtotime($season->end_date)); ?>',
                        lineupDate = '<?php echo $today_date; ?>',
                        benchId = {{ config('pool.inverse_player_position.bn') }},
                        inguryReserveId = {{ config('pool.inverse_player_position.ir') }},
                        poolScoringFields = <?php echo json_encode(config('pool.pool_scoring_field')); ?>,
                        poolScoreSettings = [],
                        gameTimezone = '<?php echo config('app.timezone'); ?>',
                        playerPositionGrouping = <?php echo json_encode(config('pool.player_position_team')); ?>,
                        add_setup = <?php echo json_encode(trans('pool.add_setup')); ?>,
                        poolTypes = <?php echo json_encode(config('pool.inverse_type')); ?>;
            </script>
            @stop 