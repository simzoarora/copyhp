@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/box_draft.css')) !!}
@stop
@section('content')
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content"> 
        @include('frontend.includes.pool_nav') 
        <div class="box-score pool--content">
            <div class="bg-trans" style="display: none">
            </div>
            @include('frontend.includes.pool_name_header')
            <div class="row" id="box-score-all">
            </div>
            @include('frontend.includes.ad_small_block')
            <div class="btn-list">
                <button class="btn-backgreen" id="submit-btn"><span>{{ trans('pool.box-draft.submit_picks') }}</span> <i class="fa fa-spinner fa-pulse" style="display: none;"></i></button>
                <button class="btn btn-green-line" id="clear-selection"><span>{{ trans('pool.box-draft.clear_selections') }}</span></button>
            </div>
        </div>
    </div>
</div>
<script id="box-score-template" type="text/html">
    <%  _.each(boxes, function(box,key){  %>
    <div class="box-score-single" data-box_name='<%= box.name %>'>
        <table class="table light-grey-table">
            <tr>
                <th><%= box.name %></th>
                <th>{{trans('pool.box-draft.pick')}}</th>
            </tr>
            <%  _.each(box.pool_box_players, function(p,key2){  %>
            <tr>
                <td>
                    <%= p.player_season.player.full_name %>
                </td>
                <td>
                    <div class="custom-checkbox">
                        <input type="checkbox" class="custom-checkbox" data-player_id="<%= p.id %>" value="None" id="squaredThree<%=key%><%= p.player_season.player.id %>" name="check">
                        <label for="squaredThree<%=key%><%= p.player_season.player.id %>"></label>
                    </div>
                </td>
            </tr>
            <% }) %>
        </table>
    </div>
    <% }) %>
</script>
@stop 
@section('after-scripts-end')
<script>
    var get_box_draft_data = '{{route('frontend.pool.getDataForBoxDraft',$pool_id)}}',
            save_box_draft_data = '{{route('frontend.pool.saveBoxPoolTeamPlayers',$pool_id)}}',
            pool_dashboard_link = '{{route('frontend.pool.dashboard',$pool_id)}}',
            error =<?php echo json_encode(trans('pool.box-draft')); ?>;
</script>
{!! Html::script(elixir('js/box_draft.js')) !!}
@stop