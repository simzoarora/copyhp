@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/in_action_tonight.css')) !!}
@stop
@section('content')
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">
        @include('frontend.includes.pool_nav')
        <div class="pool--content">
            <div class="bg-trans" style="display: none">
            </div>
            @include('frontend.includes.pool_name_header')
            <div class="in-action-tonight-container">
                <div class="in-action-tonight">

                    <h3 class="pool--main-heading">{{trans('pool.in_action_tonight.title')}}</h3>
                    <script id="all-teams-template" type="text/html">
                        <% _.each(poolTeams, function(v){ %>
                        <div class="each-team"> 
                            <div class="team-name">
                                <img src="<%= siteLink %>{{ config('pool.paths.team_logo') }}<%= v.logo %>" alt="<%= v.name %>" />
                                <span><%= v.name %></span>
                            </div>
                            <div class="goal">
                                <%= v.total_player %>
                            </div>
                        </div>
                        <% }); %>
                        </script>
                        <div class="all-teams" id="all-teams-outer"></div>
                        <div class="clearfix"></div>

                        <script id="each-teams-template" type="text/html">
                            <div class="team--name">
                                <img src="<%= siteLink %>{{ config('pool.paths.team_logo') }}<%= poolTeam.logo %>" alt="<%= poolTeam.name %>" />
                                <h3 class="pool--main-heading"><%= poolTeam.name %></h3>
                            </div>
                            <table class="table light-grey-table flip-scroll-table">
                                <thead>
                                    <tr>
                                        <th>{{trans('pool.in_action_tonight.position')}}</th>
                                        <th>{{trans('pool.in_action_tonight.player')}}</th>
                                        <th>{{trans('pool.in_action_tonight.opponent')}}</th>
                                        <th class="<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %>">{{trans('pool.in_action_tonight.pool_points')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% _.each(poolTeamsPlayers, function(value){ %>
                                    <tr> 
                                        <td><%= value.player_season.player_position.short_name %></td>
                                        <td><%= value.player_season.player.first_name %> <%= value.player_season.player.last_name %></td>
                                        <% if(!$.isEmptyObject(value.player_season.team.home_competitions)){ %>
                                        <td><%= moment(value.player_season.team.home_competitions[0].match_start_date).format("H:mm a") %> @ <%= value.player_season.team.home_competitions[0].away_team.short_name %></td>
                                        <td class=<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %>>
                                            <%= (value.player_season.team.home_competitions[0].away_team_score?value.player_season.team.home_competitions[0].away_team_score:0) %>
                                    </td>
                                    <% }else{ %>
                                    <td><%= moment(value.player_season.team.away_competitions[0].match_start_date).format("H:mm a") %> @ <%= value.player_season.team.away_competitions[0].home_team.short_name %></td>
                                    <td class=<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %> >
                                        <% if(value.total_pool_standard_score != null){ %>
                                        <%= value.total_pool_standard_score+'trf' %>
                                        <% }else{ %>
                                        0 
                                        <% } %>
                                </td> 
                                <% } %>
                            </tr> 
                            <%  }); %>  
                        </tbody>
                    </table> 
                    </script>
                    <div id="each-teams-outer"></div>

                </div>

                <div class="adblocks">
                    @include('frontend.includes.adblocks')
                    @include('frontend.includes.team_records')
                    @include('frontend.includes.player_news')
                    @include('frontend.includes.adblocks')
                </div>
            </div>

        </div>

    </div>
</div>

@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/in_action_tonight.js')) !!}
<script>
    var inActionTonightUrl = '{{route('frontend.pool.getDataForInActionTonight', $pool_id)}}',
            in_action_tonight = <?php echo json_encode(trans('pool.in_action_tonight')); ?>;
</script>
@stop