@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/draft_results.css')) !!}
@stop
@section('content')
<?php
$pool_score_seting = array();
foreach ($pool_details->poolScoreSettings as $key => $setting) {
    $scoring_field = $setting->pool_scoring_field_id;
    $found = false;
    if (!empty($pool_score_seting)) {
        foreach ($pool_score_seting as $score) {
            if ($scoring_field == $score->pool_scoring_field_id) {
                $found = true;
            }
        }
    }
    if (!$found) {
        $pool_score_seting[$key] = $setting;
    }
}
?>
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content"> 
        @include('frontend.includes.pool_nav') 
        <div class="draft-results-container pool--content">
            <div class="bg-trans" style="display: none">
            </div>
            @include('frontend.includes.pool_name_header')
            <div class="draft-results">
                <script id="draft-results-template" type="text/html">
                    <%  _.each(rounds, function(round,key){  %>
                    <div class="draft-rounds">
                        <h3 class="pool--main-heading"><%=round.name%></h3>
                        <table class="table light-grey-table flip-scroll-table">
                            <thead>
                                <tr>
                                    <th><span></span></th>
                                    <th>{{trans('pool.draft_result.team')}}<span></span></th>
                                    <th>{{trans('pool.draft_result.player')}}<span></span></th>
                                    <th class="<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %>" >{{trans('pool.draft_result.pool_points')}}<span></span></th>
                                    <?php
                                    foreach ($pool_score_seting as $key => $val) {
                                        echo('<th>' . $val->poolScoringField->stat . '</th>');
                                    }
                                    ?>
                                    <th>{{trans('pool.draft_result.round_rank')}}<span></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%  _.each(round.pool_team_players, function(player,player_key){ %>
                                <tr>
                                    <td data-title="#"><%=player_key+1 %></td>
                                    <td data-title="TEAM"><%= player.pool_team.name %></td>
                                    <td data-title="PLAYER"><%= player.player_season.player.full_name %></td>
                                    <td data-title="Pool Points" class="<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %>" >
                                        <% if(player.scores!=undefined){ %>
                                        <%= hpApp.twoDecimal(player.pool_points) %>
                                        <% } else{%>
                                        0
                                        <% }%>
                                    </td>
                                    <?php foreach ($pool_score_seting as $key => $val) { ?>
                                        <td data-title="<?php echo $val->poolScoringField->stat; ?>">
                                            <% if(player.scores!=undefined){ %>
                                            <% if(player.scores[<?php echo $val->poolScoringField->id; ?>]==null){%>
                                            0
                                            <% } else{ %>
                                            <%= hpApp.twoDecimal(player.scores[<?php echo $val->poolScoringField->id; ?>].count) %>
                                            <% }%>
                                            <%} else{%>
                                            0
                                            <%}%>
                                        </td>
                                    <?php } ?>
                                    <td data-title="Round Rank"><%= player.rank %></td>
                                </tr>
                                <% })%>
                            </tbody>
                        </table>
                    </div>
                    <% })%>
                    </script>
                    <div id="draft-results-all">
                    </div>
                </div>
            </div>

            <script id="box-score-template" type="text/html">
                <%  _.each(boxes, function(box,key){  %>
                <div class="box-score-single">
                    <table class="table light-grey-table">
                        <tr>
                            <th><%= box.name %></th>
                            <th>{{trans('pool.draft_result.pick')}}</th>
                            <th>{{trans('pool.draft_result.points')}}</th>
                        </tr>
                        <%  _.each(box.pool_box_players, function(p,key2){  %>
                        <tr>
                            <td>
                                <%= p.player_season.player.full_name %>
                            </td>
                            <td>
                                <div class="custom-checkbox">
                                    <input type="checkbox" id="squaredThree<%=key%><%= p.player_season.player.id %>" disabled
                                           <% if(poolTeamPlayers.indexOf(''+p.player_season.id) != -1){ %>
                                           checked
                                           <% } %> >
                                           <label for="squaredThree<%=key%><%= p.player_season.player.id %>"></label>
                                </div>
                            </td>
                            <td>
                                <% if(p.pool_points){%>
                                <%= hpApp.twoDecimal(p.pool_points) %>
                                <% } else{%>
                                0
                                <% } %>
                            </td>
                        </tr>
                        <% }) %>
                    </table>
                </div>
                <% }) %>
                </script>
                <div class="box-score pool--content">
                    <div class="bg-trans" style="display: none">
                    </div>
                    <div class="row" id="box-score-all">

                    </div>
                    @include('frontend.includes.ad_small_block')
                </div>
            </div>
        </div>
        @stop 
        @section('after-scripts-end')
        <script>
            var url = '{{route('frontend.pool.getDataForDraftResult',$pool_id)}}',
                    pool_types =<?php echo json_encode(config('pool.inverse_type')); ?>,
                    draft_result = <?php echo json_encode(trans('pool.draft_result')); ?>;
        </script>
        {!! Html::script(elixir('js/draft_results.js')) !!}
        @stop