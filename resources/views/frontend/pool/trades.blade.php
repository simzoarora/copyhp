@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/trades.css')) !!}
@stop
@section('content')
<?php
if (!empty($pool_details->poolRosters)) {
    $total_available_spots = 0;
    foreach ($pool_details->poolRosters as $rosters) {
        $total_available_spots = $total_available_spots + $rosters->value;
    }
}
?>
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">
        @include('frontend.includes.pool_nav')
        <div class="trades-container pool--content">
            <div class="bg-trans" style="display: none"></div>
            <?php $add_drop_page = true; ?>
            @include('frontend.includes.pool_name_header')

            <div class="create-trade">
                <p class="pool--main-heading">{{trans('pool.trades.create_trade')}}</p>
                <a href="#" class="btn btn-backgreen" id="propose-trade-submit">{{trans('pool.trades.propose_trade')}}</a>
                <div class="clearfix"></div>
                <p>{{trans('pool.trades.select_players_text')}}</p>

                <table class="table light-grey-table large-checkbox roster-players-count flip-scroll-table">
                    <thead>
                        <tr>
                            <th>{{trans('pool.trades.team')}}</th>
                            <th>{{trans('pool.trades.players_traded')}}</th>
                            <th>{{trans('pool.trades.players_dropped')}}</th>
                            <th><span class="hidden-xs">{{trans('pool.trades.roster_size')}} </span>{{trans('pool.trades.after_trade')}}</th>
                            <th>{{trans('pool.trades.accepted')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="team-name-f">{{trans('pool.trades.team_name')}}</td>
                            <td id="team-trade-f">0</td>
                            <td id="team-dropped-f">0</td>
                            <td><span id="team-players-f">0</span>/<span id="team-spots-f"><?php echo $total_available_spots; ?></span></td>
                            <td id="team-graphics-f">
                                <span class="right-tick"></span>
                            </td>
                        </tr>
                        <tr>
                            <td id="team-name-s">{{trans('pool.trades.team_name')}}</td>
                            <td id="team-trade-s">0</td>
                            <td id="team-dropped-s">0</td>
                            <td><span id="team-players-s">0</span>/<span id="team-spots-s"><?php echo $total_available_spots; ?></span></td>
                            <td id="team-graphics-s">
                                <span class="right-tick"></span>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <script id="all-players-template" type="text/html">
                    <div class="color-explain">
                        <span class="light-red"></span>
                        <span>{{trans('pool.trades.dropped')}}</span>
                        <span class="light-green"></span>
                        <span>{{trans('pool.trades.traded')}}</span>
                    </div>
                    <% if(!$.isEmptyObject(playersTeam)){
                    if(teamFirst == true){ %>
                    <input type="hidden" value="<%= playersTeam.pool.pool_team_first.id %>" name="parent_pool_team_id"/>
                    <% }else{ %>
                    <input type="hidden" value="<%= playersTeam.pool.pool_team_first.id %>" name="trading_pool_team_id"/>
                    <% } } %>
                    <table class="table light-grey-table flip-scroll-table" id="rosters-players">
                        <thead>
                            <tr>
                                <th>{{trans('pool.trades.trade')}}</th>
                                <% if(teamFirst == true){ %>
                                <th>{{trans('pool.trades.drop')}}</th>
                                <% } %>
                                <th>{{trans('pool.trades.status')}}</th>
                                <th>{{trans('pool.trades.pos')}}</th>
                                <th>{{trans('pool.trades.skaters')}}</th>
                                <th>{{trans('pool.trades.opp')}}</th>
                                <th>{{trans('pool.trades.pre')}}</th>
                                <th>{{trans('pool.trades.current')}}</th>
                                <th>{{trans('pool.trades.own')}}</th>
                                <th>{{trans('pool.trades.start')}}</th>
                                <?php
                                if (!empty($pool_details->poolScoreSettings)) {
                                    foreach ($pool_details->poolScoreSettings as $score_setting) {
                                        if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.player')) {
                                            ?>
                                            <th><?php echo $score_setting->poolScoringField->stat; ?></th>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <th class="total-pool-points">{{trans('pool.trades.pool_pts')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% if(!$.isEmptyObject(playersTeam)){
                            var playerCount = 0;
                            _.each(playersTeam.pool.pool_team_first.pool_team_players, function(value){
                            var playerStats = playersTeam.stat_data[value.player_season_id];
                            if(playerStats.player_position.name != poolScoringFields.type[2]){
                            //checking if player is deleted
                            var deletedClass = '', tradedClass = '';
                            if(value.deleted_at != null){
                            deletedClass = 'player-deleted';
                            }
                            //checking if player is dropped or deleted by one other parameter
                            if(!$.isEmptyObject(value.pool_trade_player)){
                            if(value.pool_trade_player.type == tradePlayersType.trade_players){
                            tradedClass = 'player-traded';
                            }
                            else if(value.pool_trade_player.type == tradePlayersType.drop_players){
                            deletedClass = 'player-deleted';
                            }
                            }
                            %>
                            <tr class="pool-player <%= deletedClass %> <%= tradedClass %>">
                                <td data-id="<%= value.id %>">
                                    <% if(value.deleted_at == null){ %>
                                    <% if(teamFirst == true){ %>
                                    <input type="hidden" name="parent_pool_team_players_id[<%= playerCount %>]"/> 
                                    <% }else{ %>
                                    <input type="hidden" name="trading_pool_team_players_id[<%= playerCount %>]"/> 
                                    <% } %>

                                    <div class="custom-checkbox">
                                        <input type="checkbox" id="squared-t<%= playerStats.player_id %>">
                                        <label for="squared-t<%= playerStats.player_id %>"></label>
                                    </div>
                                    <% } %>
                                </td>

                                <% if(teamFirst == true){ %>
                                <td data-id="<%= value.id %>">
                                    <% if(value.deleted_at == null){ %>
                                    <input type="hidden" name="drop_pool_team_players_id[<%= playerCount %>]"/> 

                                    <div class="custom-checkbox">
                                        <input type="checkbox" id="squared<%= playerStats.player_id %>" class="drop">
                                        <label for="squared<%= playerStats.player_id %>"></label>
                                    </div>
                                    <% } %>
                                </td>
                                <% } %>

                                <td>
                                    <% if(!$.isEmptyObject(value.pool_team_lineup)){ %>
                                    <%= value.pool_team_lineup.player_position.short_name %>
                                    <% } %>
                                </td> 
                                <td><%= playerStats.player_position.short_name %></td>
                                <td><%= playerStats.player.full_name + ' (' + playerStats.team.short_name + ')' %></td>
                                <td>                                   
                                    <% var oppPresent = false; 
                                    if(!$.isEmptyObject(playerStats.team.home_competition)){
                                    oppPresent = true; %>
                                    <%= '@'+playerStats.team.home_competition.away_team.short_name %>
                                    <% }
                                    if(!$.isEmptyObject(playerStats.team.away_competition)){ 
                                    oppPresent = true; %>
                                    <%= '@'+playerStats.team.away_competition.home_team.short_name %>
                                    <% } 
                                    if(oppPresent == false){ %>
                                    -
                                    <% } %>
                                </td>
                                <td><%= playerStats.player.player_pre_ranking?playerStats.player.player_pre_ranking.rank:0 %></td>
                                <td>
                                    <% if(!$.isEmptyObject(playerStats.player.pool_season_rank)){ %>
                                    <%= playerStats.player.pool_season_rank.rank %>
                                    <% }else{ %>
                                    0
                                    <% } %>
                                </td>
                                <td><%= playerStats.own %>%</td>
                                <td><%= playerStats.start %>%</td>
                                <?php
                                if (!empty($pool_details->poolScoreSettings)) {
                                    foreach ($pool_details->poolScoreSettings as $score_setting) {
                                        if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.player')) {
                                            ?>
                                            <td>
                                                <% if(playerStats.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']==null){ %>
                                                0
                                                <% } else { %>
                                                <%= hpApp.twoDecimal(playerStats.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']) %>
                                                <% } %>
                                            </td>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <td class="total-pool-points">
                                    <% if(!$.isEmptyObject(playerStats.player.season_pool_point)){ %>
                                    <%= hpApp.twoDecimal(playerStats.player.season_pool_point.total_points) %>
                                    <% }else{ %>
                                    0
                                    <% } %>
                                </td>
                            </tr>
                            <% } playerCount++; }); } %> 
                        </tbody>
                    </table>

                    <table class="table light-grey-table flip-scroll-table" id="rosters-goalies">
                        <thead>
                            <tr>
                                <th>{{trans('pool.trades.trade')}}</th>
                                <% if(teamFirst == true){ %>
                                <th>{{trans('pool.trades.drop')}}</th>
                                <% } %>
                                <th>{{trans('pool.trades.status')}}</th>
                                <th>{{trans('pool.trades.pos')}}</th>
                                <th>{{trans('pool.trades.skaters')}}</th>
                                <th>{{trans('pool.trades.opp')}}</th>
                                <th>{{trans('pool.trades.pre')}}</th>
                                <th>{{trans('pool.trades.current')}}</th>
                                <th>{{trans('pool.trades.own')}}</th>
                                <th>{{trans('pool.trades.start')}}</th>
                                <?php
                                if (!empty($pool_details->poolScoreSettings)) {
                                    foreach ($pool_details->poolScoreSettings as $score_setting) {
                                        if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                                            ?>
                                            <th><?php echo $score_setting->poolScoringField->stat; ?></th>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <th class="total-pool-points">{{trans('pool.trades.pool_pts')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% if(!$.isEmptyObject(playersTeam)){
                            var playerCount = 0;
                            _.each(playersTeam.pool.pool_team_first.pool_team_players, function(value){

                            var playerStats = playersTeam.stat_data[value.player_season_id];
                            if(playerStats.player_position.name == poolScoringFields.type[2]){
                            //checking if player is deleted
                            var deletedClass = '', tradedClass = '';
                            if(value.deleted_at != null){
                            deletedClass = 'player-deleted';
                            }
                            //checking if player is dropped or deleted by one other parameter
                            if(!$.isEmptyObject(value.pool_trade_player)){
                            if(value.pool_trade_player.type == tradePlayersType.trade_players){
                            tradedClass = 'player-traded';
                            }
                            else if(value.pool_trade_player.type == tradePlayersType.drop_players){
                            deletedClass = 'player-deleted';
                            }
                            }
                            %>
                            <tr class="pool-goalie <%= deletedClass %> <%= tradedClass %>">
                                <td data-id="<%= value.id %>">
                                    <% if(value.deleted_at == null){ %>
                                    <% if(teamFirst == true){ %>
                                    <input type="hidden" name="parent_pool_team_players_id[<%= playerCount %>]"/> 
                                    <% }else{ %>
                                    <input type="hidden" name="trading_pool_team_players_id[<%= playerCount %>]"/> 
                                    <% } %>

                                    <div class="custom-checkbox">
                                        <input type="checkbox" id="squared-t<%= playerStats.player_id %>">
                                        <label for="squared-t<%= playerStats.player_id %>"></label>
                                    </div>
                                    <% } %>
                                </td>

                                <% if(teamFirst == true){ %>
                                <td data-id="<%= value.id %>">
                                    <% if(value.deleted_at == null){ %>
                                    <input type="hidden" name="drop_pool_team_players_id[<%= playerCount %>]"/> 

                                    <div class="custom-checkbox">
                                        <input type="checkbox" id="squared<%= playerStats.player_id %>" class="drop">
                                        <label for="squared<%= playerStats.player_id %>"></label>
                                    </div>
                                    <% } %>
                                </td>
                                <% } %>

                                <td>
                                    <% if(!$.isEmptyObject(value.pool_team_lineup)){ %>
                                    <%= value.pool_team_lineup.player_position.short_name %>
                                    <% } %>
                                </td> 
                                <td><%= playerStats.player_position.short_name %></td>
                                <td><%= playerStats.player.full_name + ' (' + playerStats.team.short_name + ')' %></td>
                                <td>                                   
                                    <% var oppPresent = false; 
                                    if(!$.isEmptyObject(playerStats.team.home_competition)){
                                    oppPresent = true; %>
                                    <%= '@'+playerStats.team.home_competition.away_team.short_name %>
                                    <% }
                                    if(!$.isEmptyObject(playerStats.team.away_competition)){ 
                                    oppPresent = true; %>
                                    <%= '@'+playerStats.team.away_competition.home_team.short_name %>
                                    <% } 
                                    if(oppPresent == false){ %>
                                    -
                                    <% } %>
                                </td>
                                <td><%= playerStats.player.player_pre_ranking?playerStats.player.player_pre_ranking.rank:0 %></td>
                                <td>
                                    <% if(!$.isEmptyObject(playerStats.player.pool_season_rank)){ %>
                                    <%= playerStats.player.pool_season_rank.rank %>
                                    <% }else{ %>
                                    0
                                    <% } %>
                                </td>
                                <td><%= playerStats.own %>%</td>
                                <td><%= playerStats.start %>%</td>
                                <?php
                                if (!empty($pool_details->poolScoreSettings)) {
                                    foreach ($pool_details->poolScoreSettings as $score_setting) {
                                        if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                                            ?>
                                            <td>
                                                <% if(playerStats.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']==null){ %>
                                                0
                                                <% } else { %>
                                                <%= hpApp.twoDecimal(playerStats.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']) %>
                                                <% } %>
                                            </td>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <td class="total-pool-points">
                                    <% if(!$.isEmptyObject(playerStats.player.season_pool_point)){ %>
                                    <%= hpApp.twoDecimal(playerStats.player.season_pool_point.total_points) %>
                                    <% }else{ %>
                                    0
                                    <% } %>
                                </td>
                            </tr>
                            <% } playerCount++; }); } %> 
                        </tbody>
                    </table>
                    </script>
                    {!! Form::open(['route' => 'frontend.pool.createTrade', 'id' => 'create-trade-form', 'role' => 'form', 'method' => 'post']) !!}
                    <div id="all-players-outer"></div>

                    <div class="select-team">
                        {{ Form::select('team-list', $pool_teams, null, ['class' => 'select-field team-select-list']) }}
                    </div>

                    <div id="team-players-outer"></div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    @stop 
    @section('after-scripts-end')
    {!! Html::script(elixir('js/trades.js')) !!}
    <script>
        var poolTeamSeasonUrl = '{{ route('frontend.pool.getPoolTeamSeasonStat', $pool_id) }}',
                teamIndividualUrl = '{{ route('frontend.pool.getPoolTeamSeasonStatWithTeam', [$pool_id, 0]) }}',
                teamIndividualFirstUrl = '{{ route('frontend.pool.getPoolTeamSeasonStatWithTeam', [$pool_id, key($pool_teams)]) }}',
                proposeTradeUrl = '{{ route('frontend.pool_teams.trade', [$pool_id, 'tradeid']) }}',
                poolScoringFields = <?php echo json_encode(config('pool.pool_scoring_field')); ?>,
                tradePlayersType = <?php echo json_encode(config('pool.pool_trades.inverse_trade_player_type')); ?>,
                trades = <?php echo json_encode(trans('pool.trades')); ?>;
    </script>
    @stop