@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/messages.css')) !!}
@stop
@section('content')
<div class='reply_main'></div>
<div class="container">
    <div class="scorer-loader-logo">
        <?php // dd(Auth::user()); ?>
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">
        @include('frontend.includes.pool_nav')
        <div class="pool--content">
            <div class="bg-trans" style="display: none">
            </div>
            <?php $dashboardPage = true; ?>
            @include('frontend.includes.pool_name_header')
        </div>

        <div class="message1">
            <div class="bg-trans" style="display: none">
            </div>
            <div class="message-header">
                <h4>{{trans('messageboards.header.fan_forum')}}</h4>
                <a href="#" class="btn btn-backgreen" id="new-topic">{{trans('messageboards.header.new_topic')}}<span class="plus-sign"></span></a>
            </div>
            <div class="message-nav">
                <ul>  
                    <li class="active" style="display:none">
                        <a href="">{{trans('messageboards.header.new_message')}}</a>
                        <span class="bottom-shadow"></span>
                    </li>
                    <li class="active" data-count="0" data-messagetype="{{config('message_board.message_type.public')}}">
                        <a href="">{{trans('messageboards.header.topics')}}</a>
                        <span class="bottom-shadow"></span>
                    </li>
                    <li data-count="0" data-type="myposts"  data-messagetype="{{config('message_board.message_type.public')}}">
                        <a href="">{{trans('messageboards.header.my_posts')}}</a>
                        <span class="bottom-shadow"></span>
                    </li>
                    <li data-count="1" data-messagetype="{{config('message_board.message_type.private')}}">
                        <a href="">{{trans('messageboards.header.private_messages')}}</a>
                        <span class="bottom-shadow"></span>
                        <span class="notification">10</span>
                    </li>
                    <li style="display:none" data-type="" data-messagetype=""> 
                        <a href=""></a>
                        <span class="bottom-shadow"></span>
                        <span class="close-sign"></span>
                    </li>
                </ul>
                <!--message-nav-mobile--> 
                <div class="bootstrap_btn_mobile">
                    <button type="button" class="navbar-toggle" id="open-navbar" data-toggleid="mobilelist">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="crossbtn"></span>
                    </button>
                    <ul>
                        <li class="active" style="display:none">
                            <a href="">{{trans('messageboards.header.new_message')}}</a>
                            <span class="bottom-shadow"></span>
                        </li>
                        <li class="active" id="newtopic" data-count="0" data-messagetype="{{config('message_board.message_type.public')}}">
                            <a href="">{{trans('messageboards.header.topics')}}</a>
                            <span class="bottom-shadow"></span>
                        </li>
                        <li data-count="0" data-type="myposts"  data-messagetype="{{config('message_board.message_type.public')}}">
                            <a href="">{{trans('messageboards.header.my_posts')}}</a>
                            <span class="bottom-shadow"></span>
                        </li>
                        <li data-count="1" data-messagetype="{{config('message_board.message_type.private')}}">
                            <a href="">{{trans('messageboards.header.private_messages')}}</a>
                            <span class="bottom-shadow"></span>
                            <span class="notification">10</span>
                        </li>
                        <li style="display:none">
                            <a href=""></a>
                            <span class="bottom-shadow"></span>
                            <span class="close-sign"></span>
                        </li>
                        <span class="close-sign"></span>
                    </ul>
                </div>
                <!--message-nav-mobile ends-->
            </div>
            <div class="message-box msg-post"  id="message-box" style="display: none">
                {{ Form::open(['route'=>'frontend.message_board.store','class' => 'form-horizontal','id' =>'new-message-form', 'role' => 'form', 'method' => 'post']) }}
                <div id="alert-box"></div> 
                {{ Form::hidden('pool_id', $pool_id) }}   
                {{ Form::text('title', null, array('class' => 'form-control grey-field', 'placeholder' => trans('messageboards.form.subject'), 'required')) }}   
                {{ Form::textarea('description', null, array('class' => 'form-control grey-field', 'placeholder' => trans('messageboards.form.type_message'), 'required')) }}   
                <!--                <div class="identity">
                                    <label>Identity:<span>Rick</span></label>
                                    <a href="#">Change</a>
                                </div>-->
                <div class="post-btn">
                    <button class="btn-backgreen submit-post-btn"><span>{{trans('messageboards.form.post')}}</span> <i class="fa fa-spinner fa-pulse"></i></button>
                </div>
                {{ Form::close() }}
            </div>
            <div class="message-posts msg-post" data-messageDiv="0">
                <div class="message-post-headings">
                    <p class="post-headings-title">{{trans('messageboards.subheader.title')}}</p>
                    <p class="post-headings-like"><span></span></p>
                    <p class="post-headings-dislike"><span></span></p>
                    <p class="post-headings-replies">{{trans('messageboards.subheader.replies')}}</p>
                    <p class="post-headings-latest desc" data-sort_by="latest_reply" data-sort_by_type="asc" id="all-messages-sorting">{{trans('messageboards.subheader.latest_post')}}</p>
                </div>
                <div class="message-post-details" id="message-post">

                </div>
                <div class="message-post-details"  data-type="myposts" id="my-message-post" style="display:none">

                </div>
            </div>
            <div class="champions msg-post" id="detailed-message" style="display:none ">
            </div>
            <!--private-messages-START-->
            <div class="private-messages msg-post" id="private-message" data-messageDiv="1" style="display:none ">
                <div class="private-message-header">
                    <div class="private-message-form" style="display:none">
                        {{ Form::open(['route'=>'frontend.message_board.store','class' => 'form-horizontal private-message','id' =>'private-message-form', 'role' => 'form', 'method' => 'post']) }}
                        <div id="alert-box"></div> 
                        {{ Form::hidden('pool_id', $pool_id) }}   
                        {{ Form::select('to', $user_data,null, array('class' => 'form-control input-field', 'placeholder' => trans('messageboards.form.to'), 'required')) }}   
                        {{ Form::text('title', null, array('class' => 'form-control input-field', 'placeholder' => trans('messageboards.form.subject_private'), 'required')) }}   
                        {{ Form::textarea('description', null, array('class' => 'form-control input-field', 'placeholder' => trans('messageboards.form.type_message_private'), 'required')) }}   
                        <div class="submit-btn">
                            <button class="btn-backgreen submit-message-btn"><span>{{trans('messageboards.form.post_message')}}</span><i class="fa fa-spinner fa-pulse"></i></button>
                        </div>
                        {{ Form::close() }}
                        <div class="header-text">{{trans('messageboards.header.private_text')}}</div>
                    </div>
                    <a class="btn-backgreen new-message">{{trans('messageboards.header.new_message')}}
                        <span class="icon inbox-icon"></span>
                    </a>
                </div>
                <div class="private-message-box" id="private-message-box">
                    <!--private-messages-->
                </div>
                <!--new-message-->
                <div class="single-message new-private-message" data-boardid="" style="display:none"> 
                    <div class="message-details" data-user="{{trans('messageboards.extra.you')}}">
                        <a class="message-subject" data-messagetype='{{config("message_board.message_type.private")}}' data-msgid=""></a>
                        <div class="message">
                            <p></p> 
                            <div class="message-actions" data-msgboardid="">
                                <span class="icon reply-icon"></span> 
                                <span class="trash-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                            </div>
                        </div>
                        <div class="message-sender">
                            <span class="from">From: {{trans('messageboards.extra.you')}}</span>
                            <span class="vertical-line"></span>
                            <span class="date">Date: </span>
                        </div>
                    </div>
                </div>
                <!--new-message-->
                <div class="single-message-thread">
                    <!--private-message-reply-form-->
                    <div class="new-private-message-reply" style="display:none">
                        {{ Form::open(['route'=>'frontend.message_board.reply','class' => 'form-horizontal private-reply','id' =>'private-message-reply-form', 'role' => 'form', 'method' => 'post']) }}
                        <div id="alert-box"></div> 
                        {{ Form::hidden('message_board_id',null, array('class' =>'board-id')) }}
                        {{ Form::select('to',$user_data,null, array('class' => 'form-control input-field', 'required', 'disabled')) }}   
                        {{ Form::text('title', null, array('class' => 'form-control input-field', 'placeholder' => trans('messageboards.form.reply_subject'), 'required', 'disabled')) }}   
                        {{ Form::textarea('reply', null, array('class' => 'form-control input-field', 'placeholder' => trans('messageboards.form.type_message_private'), 'required')) }}   
                        <div class="submit-btn">
                            <button class="btn-backgreen submit-message-btn"><span>{{trans('messageboards.form.post_message')}}</span><i class="fa fa-spinner fa-pulse"></i></button>
                        </div>
                        {{ Form::close() }}
                    </div>
                    <div class="all-replies" id="all-replies">
                        <!--all-replies-->
                    </div>
                    <!--new-private-message-reply-->
                    <div class="panel panel-default message-reply new-reply" style="display:none">
                        <div class="panel-heading" role="tab" id="">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapse"  class="collapsed">
                                    <span class="icon user-icon"></span><span class="sender-name">{{trans('messageboards.extra.you')}}</span>
                                    <span class="reply-subject" data-msgid=""><span class="vertical-line"></span><span class="text"></span></span>
                                    <span class="reply-time"></span>
                                    <span class="message-actions" data-msgboardid="">
                                        <span class="icon reply-icon"></span> 
                                        <span class="trash-icon"><i class="fa fa-trash" aria-hidden="true"></i></span> 
                                    </span>
                                </a>
                            </h4>
                        </div>
                        <div id="" class="panel-collapse collapse" role="tabpanel" aria-labelledby="" aria-expanded="false">
                            <div class="panel-body">
                                <div class="reply-text"> 
                                </div>
                            </div> 
                        </div>
                    </div>
                    <!--new-private-message-reply-->
                </div>
            </div>
        </div>
    </div>
    <!--private-messages-END-->
    <div class="post-pagination" style="display:block;">
        <div class="pagination-child">
            <div id="pagination-first" class="pagination-arrows">
                <div class="double-arrow"></div>
            </div>
            <div id="pagination-left" class="pagination-arrows"></div>
            <div class="main-pagination">
                <div id="page-list">

                </div>
            </div>
            <div id="pagination-right" class="pagination-arrows"></div>
            <div id="pagination-last" class="pagination-arrows">
                <div class="double-arrow"></div>
            </div>
        </div>
    </div> 
    @include('frontend.includes.ad_small_block')
</div>
</div>
</div>
<div class="reply-form" id="reply-form-template" style="display: none"> 
    {{ Form::open(['route'=>'frontend.message_board.reply','class' => 'form-horizontal','id' =>'reply-message-form', 'role' => 'form', 'method' => 'post']) }}
    <div id="alert-box"></div> 
    {{ Form::hidden('message_board_id', null, array('class' =>'board-id')) }}
    {{ Form::hidden('reply_id', null, array('class' =>'reply-id')) }}
    {{ Form::textarea('reply', null, array('class' => 'form-control grey-field', 'placeholder' => trans('messageboards.extra.reply'), 'required','size' => '10x5')) }}   
    <div class="post-btn">
        <button class="btn-backgreen submit-btn"><span>{{trans('messageboards.extra.reply')}}</span> <i class="fa fa-spinner fa-pulse"></i></button>
    </div>
</div>
<!--new-post-div-->
<div class="new-post" id="new-post-template" style="display:none">
    <div class="post added-post">
        <div class="post-message">
            <a class="message-title" data-msgid=""></a>
            <div>
                <p class="vertical-line"> </p>
                <p> <span> {{trans('messageboards.extra.by')}} </span></p>
            </div>
        </div>
        <div class="post-options">
            <div class="post-likes">
            </div>
            <div class="post-dislikes">
            </div>
            <div class="post-replies">
            </div>
            <div class="post-latest">
            </div>
        </div>
    </div>
</div>
<!--new-post-div-->
<!--new-reply-div-->
<div class="public-new-reply" style="display:none">
    <div class="reply-post champion-post">
        <div class="champion-pic">
            {{ HTML::image('img/profile-pic1.jpg', 'profile-img', array('class' => 'champion-pic')) }}
        </div>
        <div class="champion-details">
            <p class="champion-name">
            </p>
            <div class="post-date">
                <p></p>
                <span>{{trans('messageboards.extra.by')}}</span>
            </div>
            <h3></h3>
            <div class="description">
                <p></p><span></span>
            </div>
            <a class="show-text" data-text="{{trans('messageboards.extra.show_less')}}">{{trans('messageboards.extra.show_more')}}</a>
            <div class="post-reponses">
                <p class="likes" data-id='' data-reactions="noreaction">
                    <span></span></p>
                <p class="dislikes" data-id=''  data-reactions="noreaction">
                    <span></span></p>
                <p class="replies" data-messageboardid="" data-replyid="" data-reactions="null"><span></span>{{trans('messageboards.extra.reply')}}</p>
            </div>
        </div>
    </div> 
</div>
<!--new-reply-div-ends-->
<!--main-message-board-->
<script type="text/html" id="message-board">
    <% _.each(msgsdata.data, function(msgdata, key){  %>
    <div class="post">
        <div class="post-message">
            <a class="message-title" data-msgid="<%= msgdata.id %>"><%= msgdata.title %></a>
            <div>
                <p class="vertical-line"><%= moment(msgdata.created_at).format("MMM DD, YYYY")%></p>
                <p><%= moment(msgdata.created_at).format("hh:mm a") %><span> by </span><%= msgdata.user.name %><p>
            </div>
        </div>
        <div class="post-options">
            <div class="post-likes">
                <%= (msgdata.likes) ?  msgdata.likes.total :'0' %> 
            </div>
            <div class="post-dislikes">
                <%= (msgdata.dislikes) ?  msgdata.dislikes.total :'0' %>
            </div>
            <div class="post-replies">
                <%= (msgdata.total_replies) ?  msgdata.total_replies.total :'0' %>
            </div>
            <div class="post-latest">
                <% if (msgdata.latest_reply){ %> 
                <p><%= moment(msgdata.latest_reply.created_at).format("MMM DD, YYYY") %></p>
                <p><%= moment(msgdata.latest_reply.created_at).format("hh:mm a")%><span> {{trans('messageboards.extra.by')}} </span> <%= msgdata.latest_reply.user.name %> </p>
                <% } else{ %>
                <%= '' %> 
                <% }%> 
            </div>
        </div>
    </div> 
    <% }) %>
</script>
<!--my-posts-->
<script type="text/html" id="my-post-message-board">
    <% _.each(msgsdata.data, function(msgdata, key){  %>
    <div class="post">
        <div class="post-message">
            <a class="message-title" data-msgid="<%= msgdata.id %>"><%= msgdata.title %></a>
            <div>
                <p class="vertical-line"><%= moment(msgdata.created_at).format("MMM DD, YYYY")%></p>
                <p><%= moment(msgdata.created_at).format("hh:mm a") %><span> by </span><%= msgdata.user.name %><p>
            </div>
        </div>
        <div class="post-options">
            <div class="post-likes">
                <%= (msgdata.likes) ?  msgdata.likes.total :'0' %> 
            </div>
            <div class="post-dislikes">
                <%= (msgdata.dislikes) ?  msgdata.dislikes.total :'0' %>
            </div>
            <div class="post-replies">
                <%= (msgdata.total_replies) ?  msgdata.total_replies.total :'0' %>
            </div>
            <div class="post-latest">
                <% if (msgdata.latest_reply){ %> 
                <p><%= moment(msgdata.latest_reply.created_at).format("MMM DD, YYYY") %></p>
                <p><%= moment(msgdata.latest_reply.created_at).format("hh:mm a")%><span> {{trans('messageboards.extra.by')}} </span> <%= msgdata.latest_reply.user.name %> </p>
                <% } else{ %>
                <%= '' %> 
                <% }%> 
            </div>
        </div>
    </div> 
    <% }) %>
</script>
<!--main-message-board-ends-->
<!--reply-board-->
<script type="text/html" id="reply-template">
    <div class="champion-post"  id="reply-details" >
        <div class="champion-pic">
            {{ HTML::image('img/team_logos/<%=data.user.pool_team.logo %>', 'profile-img', array('class' => 'champion-pic')) }}

        </div>
        <div class="champion-details">
            <p class="champion-name">
                <%= data.user.pool_team.name %> 
            </p>
            <div class="post-date">
                <p><%= moment(data.created_at).format("MMM DD, YYYY") %></p>
                <span><%= moment(data.created_at).format("hh:mm a") %> </span>
            </div>
            <h3><%= data.title %></h3> 
            <div class="description">
                <p><%= (data.description) ? data.description : '' %></p><span></span></div>
            <a class="show-text" data-text="{{trans('messageboards.extra.show_less')}}">{{trans('messageboards.extra.show_more')}}</a>
            <div class="post-reponses" >
                <p class="likes mainmessage" data-id='<%= data.id %>'  data-reactions="<%= (data.current_user_reaction) ? 'already_reacted' : 'noreaction' %>">
                    <%= (data.likes) ?  data.likes.total :'' %> 
                    <span></span></p>
                <p class="dislikes mainmessage" data-id='<%= data.id %>' data-reactions="<%= (data.current_user_reaction) ? 'already_reacted' : 'noreaction' %>">
                    <%= (data.dislikes) ?  data.dislikes.total :'' %> 
                    <span></span></p>
                <p class="replies" data-messageboardid="<%= data.id %>"><span></span>{{trans('messageboards.extra.reply')}}</p>
            </div>
        </div>
        <div class="sort-posts"> 
            <label>{{trans('messageboards.extra.sort')}} :</label>
            <p class="active" data-id="<%= data.id %>" data-replies_sort_by="created_at" data-replies_sort_by_type="asc">{{trans('messageboards.extra.oldest')}}</p>
            <p data-id="<%= data.id %>" data-replies_sort_by="created_at" data-replies_sort_by_type="desc">{{trans('messageboards.extra.newest')}}</p>
            <!--<p>Most replied</p>-->
            <p class="expand-all expanded" data-text="{{trans('messageboards.extra.expand-all')}}">{{trans('messageboards.extra.collapse-all')}}</p>
        </div>
    </div>
</script>
<!--reply-board-ends-->
<!--reply-board-reply-->
<script type="text/html" id="msg-replies">
    <div class="champion-post">
        <div class="champion-pic">
            {{ HTML::image('img/team_logos/<%=data.user.pool_team.logo %>', 'profile-img', array('class' => 'champion-pic')) }}
        </div>
        <div class="champion-details cp<%=data.id %>">
            <input type="hidden">
            <p class="champion-name">
                <%=  data.user.pool_team.name %> 
            </p>
            <div class="post-date">
                <p><%= moment(data.created_at).format("MMM DD, YYYY") %></p>
                <p><%= moment(data.created_at).format("hh:mm a") %> </p>
            </div>
            <h3><%= data.title%></h3>
            <div class="description">
                <p><%= data.reply%></p><span></span>
            </div>
            <a class="show-text" data-text="{{trans('messageboards.extra.show_less')}}">{{trans('messageboards.extra.show_more')}}</a>
            <div class="post-reponses">
                <p class="likes" data-id='<%= data.id %>' data-reactions="<%= (data.current_user_reaction) ? data.current_user_reaction : 'noreaction' %>">
                    <%= (data.likes) ?  data.likes.total :'' %> 
                    <span></span></p>
                <p class="dislikes" data-id='<%= data.id %>'  data-reactions="<%= (data.current_user_reaction) ? data.current_user_reaction : 'noreaction'%>">
                    <%= (data.dislikes) ?  data.dislikes.total :'' %> 
                    <span></span></p>
                <p class="replies" data-messageboardid="<%= data.message_board_id %>" data-replyid="<%= data.id %>"><span></span>{{trans('messageboards.extra.reply')}}</p>
            </div>
            </script>
            <!--reply-board-reply-ends-->
            <!--all-private-messages-->
            <script type="text/html" id="all-private-messages">
                <% _.each(msgsdata.data, function(msgdata, key){  %> 
                <div class="<%= _.contains(msgsdata.data_unread, msgdata.id)?'single-message grey':'single-message'%>" data-boardid="<%= msgdata.id %>"> 
                    <div class="message-details" data-user="<%= msgdata.user.name %>">
                        <a class="message-subject" data-messagetype='<%= msgdata.type %>' data-msgid="<%= msgdata.id %>"><%= msgdata.title %></a>
                        <div class="message">
                            <p><%= msgdata.description %></p> 
                            <div class="message-actions" data-msgboardid="<%= msgdata.id %>">
                                <span class="icon reply-icon"></span> 
                                <%= (msgdata.user_id == {{access()->user()->id}})?'<span class="trash-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>':'' %>
                            </div>
                        </div>
                        <div class="message-sender">
                            <span class="from">From: <%= (msgdata.user_id == {{access()->user()->id}})?'{{trans('messageboards.extra.you')}}':(msgdata.user.name?msgdata.user.name:(msgdata.user.email.length<18?msgdata.user.email:msgdata.user.email.substring(0,18)+'...')) %></span>
                            <span class="vertical-line"></span>
                            <span class="date">Date: <%= msgdata.created_at %></span>
                        </div>
                    </div>
                </div>
                <% }) %>
                </script>
                <!--all-private-messages-ends-->
                <!--single-private-message-thread-->
                <script type="text/html" id="private-message-thread">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default message-reply">
                            <div class="panel-heading" role="tab" id="heading">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="" data-parent="#accordion" aria-expanded="true" aria-controls="collapse"  class="">
                                        <span class="icon user-icon"></span><span class="sender-name"><%= (data.user_id == {{access()->user()->id}})?'{{trans('messageboards.extra.you')}}':(data.user.name?data.user.name:(data.user.email.length<18?data.user.email:data.user.email.substring(0,18)+'...')) %></span>
                                        <span class="reply-subject" data-msgid="<%= data.id %>"><span class="vertical-line"></span><%= data.description.length<60?data.description:data.description.substring(0, 60)+"..."%></span>
                                        <span class="reply-time"><%= moment(data.created_at).format("MMM Do") %>(<%= moment(data.created_at).startOf('day').fromNow() %>)</span>
                                        <span class="message-actions" data-msgboardid="<%= data.id %>">
                                            <span class="icon reply-icon"></span> 
                                        </span>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse<%= data.id %>" class="panel-collapse" role="tabpanel" aria-labelledby="heading<%= data.id %>">
                                <div class="panel-body">
                                    <div class="reply-text"> 
                                        <%= data.description %>
                                    </div>
                                </div> 
                            </div>
                        </div> 
                        <div class="new-private-reply-append"></div>
                        <% _.each(data.direct_replies, function(reply, key){  %>
                        <div class="panel panel-default message-reply">
                            <div class="panel-heading" role="tab" id="mheading<%= key%>">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#mcollapse<%= key%>" aria-expanded="true" aria-controls="mcollapse<%= key%>"  class="collapsed">
                                        <span class="icon user-icon"></span><span class="sender-name"><%= (reply.user_id == {{access()->user()->id}})?'{{trans('messageboards.extra.you')}}':(reply.user.name?reply.user.name:(reply.user.email.length<18?reply.user.email:reply.user.email.substring(0,18)+'...')) %></span>
                                        <span class="reply-subject" data-msgid="<%= data.id %>"><span class="vertical-line"></span><%= reply.reply.length<60?reply.reply:reply.reply.substring(0, 60)+"..."%></span>
                                        <span class="reply-time"><%= moment(reply.created_at).format("MMM Do") %>(<%= moment(reply.created_at).startOf('day').fromNow() %>)</span>
                                        <span class="message-actions" data-replyId="<%= reply.id %>" data-msgboardid="<%= reply.message_board_id %>">
                                            <span class="icon reply-icon"></span> 
                                            <%= (reply.user_id == {{access()->user()->id}})?'<span class="trash-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>':'' %>
                                        </span>
                                    </a> 
                                </h4>
                            </div>
                            <div id="mcollapse<%= key%>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="mheading<%= key%>">
                                <div class="panel-body"> 
                                    <div class="reply-text">
                                        <%= reply.reply %>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <% }); %>
                    </div>
                    </script>
                    <!--single-private-message-thread-->
                    @stop                                                                                                
                    @section('after-scripts-end')
                    <script>
                        var url = '{{route('frontend.message_board.getAllMessageBoards',$pool_id)}}',
                                reactionsUrl = '{{route('frontend.message_board.saveMessageReaction')}}',
                                replyUrl = '{{route('frontend.message_board.reply')}}',
                                messageBoardDataUrl = '{{route('frontend.message_board.getDataForMessageBoard',0)}}',
                                messageBoardUrl = '{{route('frontend.pool.message_board.index',$pool_id,0)}}',
                                username = '{{$pool_details->poolTeam->name}}',
                                team_logo = '/img/team_logos/{{ $pool_details->poolTeam->logo }}',
                                message_board_id = '{{$message_board_id}}',
                                message_limit = '{{config('message_board.paginations.replies.per_page_limit')}}',
                                already_reacted = '<?php echo(trans('messages.message_board.already_reacted')); ?>',
                                by = '<?php echo trans('messageboards.extra.by'); ?>',
                                pool_id = {{$pool_id}},
                                privateMessageType = '<?php echo(config("message_board.message_type.private")); ?>',
                                publicMessageType = '<?php echo(config("message_board.message_type.public")); ?>',
                                deleteMsgUrl = '{{route('frontend.message_board.delete')}}';
                    </script>
                    {!! Html::script(elixir('js/messages.js')) !!}
                    @stop