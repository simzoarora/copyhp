@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/live_stats.css')) !!}
@stop
@section('content')
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id='pool--content'>
        @include('frontend.includes.pool_nav')

        <div class="pool--content">
            <div class="bg-trans" style="display: none">
            </div>
            @include('frontend.includes.pool_name_header')

            <div class="live-stats-container">
                <h3 class="pool--main-heading">{{trans('pool.live_stats.title')}}</h3>

                <script id="live-stats-head-template" type="text/html">
                    <table class="table light-grey-table head-arrow flip-scroll-table">
                        <thead>
                            <tr>
                                <th>{{trans('pool.live_stats.team')}}<p class="hidden-xs">{{trans('pool.live_stats.name')}}</p> <span></span></td>
                                    <% _.each(poolScoreSettings, function(v){ %>
                                <th><%= v.pool_scoring_field.stat %> <span></span></th>
                                <%  }); %>
                                <th class=<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %>>{{trans('pool.live_stats.pool_points')}}<span></span></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <% $.each(poolTeams, function(i, v){ %>
                            <tr>
                                <td><%= v.name %></td>
                                <% _.each(poolScoreSettings, function(value){ %>
                                <td id="con-<%= v.id %>-<%= i %>-<%= value.pool_scoring_field_id %>">0</td>
                                <%  }); %> 
                                <td  id="con-<%= v.id %>-total_points"  class=<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %>>0</td>
                            </tr> 
                            <% }) %>  
                        </tbody>
                    </table>
                    </script>
                    <div id="live-stats-head-outer"></div>

                    <script id="each-teams-template" type="text/html">
                        <div class="team--name">
                            <img src="<%= siteLink %>{{ config('pool.paths.team_logo') }}<%= poolTeam.logo %>" alt="<%= poolTeam.name %>" />
                            <h3 class="pool--main-heading"><%= poolTeam.name %></h3>
                        </div>
                        <table class="table light-grey-table flip-scroll-table team-players">
                            <thead>
                                <tr>
                                    <th>{{trans('pool.live_stats.status')}}</th>
                                    <th>{{trans('pool.live_stats.pos')}}</th>
                                    <th>{{trans('pool.live_stats.skaters')}}</th>
                                    <% _.each(poolScoreSettings, function(v){ 
                                    if(v.type == poolScoringFields.inverse_type.player){ %>
                                    <th><%= v.pool_scoring_field.stat %></th>
                                    <% } }); %> 
                                    <th class=<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %>>{{trans('pool.live_stats.pool_pts')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% _.each(poolTeam.pool_team_players, function(value){ 
                                if(value.player_season.player_position.name != poolScoringFields.type[2]){ %>
                                <tr>
                                    <td><% if(!$.isEmptyObject(value.pool_team_lineup)){ %>
                                        <%= value.pool_team_lineup.player_position.name %>
                                        <% } %></td>
                                    <td><%= value.player_season.player_position.name %></td>
                                    <td><%= value.player_season.player.full_name %></td>

                                    <% _.each(poolScoreSettings, function(scoreSettingValue){ 
                                    if(scoreSettingValue.type == poolScoringFields.inverse_type.player){ %>
                                    <td id="con-<%= value.id %>-<%= scoreSettingValue.pool_scoring_field_id %>">0</td>
                                    <% } }); %> 

                                    <td class=<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %>><% if(!$.isEmptyObject(value.player_season.player.calculated_pool_point)){ %>
                                        <%= hpApp.twoDecimal(value.player_season.player.calculated_pool_point.total_points) %>
                                        <% }else{ %>
                                        0
                                        <% } %>
                                    </td>
                                </tr>
                                <% } %> 
                                <% }); %> 
                                <tr class='lineup-totals'>
                                    <td colspan="2"></td>
                                    <td>{{trans('pool.live_stats.starting_lineup_totals')}}</td>
                                    <% _.each(poolScoreSettings, function(scoreSettingValue){
                                    if(scoreSettingValue.type == poolScoringFields.inverse_type.player){  %>
                                    <td id="lineup-<%= poolTeam.id %>-<%= scoreSettingValue.pool_scoring_field_id %>">0</td>
                                    <% } }); %> 
                                    <td id="lineup-<%= poolTeam.id %>-total_points" class=<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %> >0</td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table light-grey-table flip-scroll-table team-goalies">
                            <thead>
                                <tr>
                                    <th>{{trans('pool.live_stats.status')}}</th>
                                    <th>{{trans('pool.live_stats.pos')}}</th>
                                    <th>{{trans('pool.live_stats.goalie')}}</th>
                                    <% _.each(poolScoreSettings, function(v){ 
                                    if(v.type == poolScoringFields.inverse_type.goalie){
                                    %>
                                    <th><%= v.pool_scoring_field.stat %></th>
                                    <% } }); %> 
                                    <th class=<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %>>{{trans('pool.live_stats.pool_pts')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% _.each(poolTeam.pool_team_players, function(value){ 
                                if(value.player_season.player_position.name == poolScoringFields.type[2]){ %>
                                <tr>
                                    <td><% if(!$.isEmptyObject(value.pool_team_lineup)){ %>
                                        <%= value.pool_team_lineup.player_position.name %>
                                        <% } %></td>
                                    <td><%= value.player_season.player_position.name %></td>
                                    <td><%= value.player_season.player.full_name %></td>

                                    <% _.each(poolScoreSettings, function(scoreSettingValue){ 
                                    if(scoreSettingValue.type == poolScoringFields.inverse_type.goalie){ %>
                                    <td id="con-<%= value.id %>-g-<%= scoreSettingValue.pool_scoring_field_id %>">0</td>
                                    <% } }); %> 

                                    <td  class=<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %>><% if(!$.isEmptyObject(value.player_season.player.calculated_pool_point)){ %>
                                        <%= hpApp.twoDecimal(value.player_season.player.calculated_pool_point.total_points) %>
                                        <% }else{ %>
                                        0
                                        <% } %>
                                    </td>
                                </tr>
                                <% } %> 
                                <% }); %>
                                <tr class='lineup-totals'>
                                    <td colspan="2"></td>
                                    <td>{{trans('pool.live_stats.starting_lineup_totals')}}</td>
                                    <% _.each(poolScoreSettings, function(scoreSettingValue){ 
                                    if(scoreSettingValue.type == poolScoringFields.inverse_type.goalie){ %>
                                    <td id="lineup-<%= poolTeam.id %>-g-<%= scoreSettingValue.pool_scoring_field_id %>">0</td>
                                    <% } }); %> 
                                    <td id="lineup-<%= poolTeam.id %>-g-total_points" class=<% if(poolToShow == 11 || poolToShow == 32){ %><%= 'pool_points_hide' %><% } %> >0</td>
                                </tr>
                            </tbody>
                        </table>
                        </script>
                        <div id="each-teams-outer"></div>

                    </div>
                </div>
            </div>
        </div>
        @stop 
        @section('after-scripts-end')
        {!! Html::script(elixir('js/live_stats.js')) !!}
        <script>
            var liveStatsUrl = '{{route('frontend.pool.getDataForLiveStats', $pool_id)}}',
                    poolScoringFields = <?php echo json_encode(config('pool.pool_scoring_field')); ?>,
                    live_stats = <?php echo json_encode(trans('pool.live_stats')); ?>;
        </script>
        @stop