@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/add_setup.css')) !!}
@stop 
@section('content')
<?php
if (!empty($_GET['new_added_players'])) {
    $new_added_players = $_GET['new_added_players'];
} else {
    $new_added_players = 0;
}
?>
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">
        @include('frontend.includes.pool_nav')
        <div class="add__setup--content pool--content">
            <div class="bg-trans" style="display: none"></div>
            <?php $add_drop_page = true; ?>
            @include('frontend.includes.pool_name_header')

            <div class="setup_container">
                <div class="select__players--drop">
                    <p class="pool--main-heading">{{trans('pool.add_setup.select_players_to_drop')}}</p>
                    <p>{{trans('pool.add_setup.content')}}</p>
                    <?php
                    $today_date = date("Y-m-d");
                    ?>
                    <div class="roster__table">
                        <table class="table light-grey-table flip-scroll-table">
                            <tbody>
                                <tr>
                                    <th>{{trans('pool.add_setup.roster_position')}}</th>
                                    <th>{{trans('pool.add_setup.total_available_spots')}}</th>
                                    <th>{{trans('pool.add_setup.before_transaction')}}</th>
                                    <th>{{trans('pool.add_setup.after_transaction')}}</th>
                                </tr>
                                <?php
                                if (!empty($pool_details->poolRosters)) {
                                    $total_available_spots = 0;
                                    $total_after_transaction = 0;
                                    foreach ($pool_details->poolRosters as $rosters) {
                                        if ($rosters->player_position_id != config('pool.inverse_player_position.bn')) {
                                            ?>
                                            <tr>
                                                <td>{{ $rosters->playerPosition->short_name }}</td>
                                                <td>{{ $rosters->value }}
                                                    <?php
                                                    $total_available_spots = $total_available_spots + $rosters->value;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php if (!empty($rosters->playerPosition->poolTeamLineupCount)) { ?>
                                                        {{ $rosters->playerPosition->poolTeamLineupCount->total }}
                                                    <?php } else { ?>
                                                        0
                                                    <?php } ?>
                                                </td>
                                                <td id="at-{{ $rosters->playerPosition->short_name }}">
                                                    <?php if (!empty($rosters->playerPosition->poolTeamLineupCount)) { ?>
                                                        {{ $rosters->playerPosition->poolTeamLineupCount->total }}
                                                        <?php
                                                        $total_after_transaction = $total_after_transaction + $rosters->playerPosition->poolTeamLineupCount->total;
                                                    } else {
                                                        ?>
                                                        0
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <?php
                                        } else {
                                            $total_available_spots = $total_available_spots + $rosters->value;
                                        }
                                    }
                                } else {
                                    ?>
                                    <tr><td colspan="4">{{trans('pool.add_setup.no_roster_positions')}}</td></tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="roster__size">
                        <div class="pool--main-heading"><p>{{trans('pool.add_setup.total_roster_size')}} 
                                <span id="total_after_transaction"><?php echo $total_after_transaction; ?></span>
                                <span>/</span>
                                <span id="total_available_spots"><?php echo $total_available_spots; ?></span>
                            </p></div>
                        <ul class="list-inline">
                            <?php
                            if ($total_after_transaction <= $total_available_spots) {
                                $add_attr = '';
                            } else {
                                $add_attr = 'disabled';
                            }
                            ?>
                            <li>
                                <a id="review-trans" class="btn btn-backgreen" href="#" <?php echo $add_attr; ?>>{{trans('pool.add_setup.review_transaction')}}</a>
                            </li>

                            <li>
                                <a class="btn btn-orange-line" href="#" id="cancel-trans">{{trans('pool.add_setup.cancel_transaction')}}</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <!--add confirmation small header-->
                <div class="select__players--confirmation">
                    <p class="pool--main-heading">{{trans('pool.add_setup.transaction_details')}}</p>
                    <p id="add-player-list">{{trans('pool.add_setup.you_will_add')}}<span></span></p>
                    <p id="drop-player-list">{{trans('pool.add_setup.you_will_drop')}}<span></span></p>
                    <p>{{trans('pool.add_setup.transaction_processed_on')}}<span><?php echo date('M j, Y', strtotime('+1 day')); ?></span></p>
                </div>
                <!--END add confirmation small header-->

                {!! Form::open(['route' => 'frontend.pool.confirmAddPlayers', 'id' => 'add-player-form', 'role' => 'form', 'method' => 'post']) !!}
                <p class="players__added small-grey-title">{{trans('pool.add_setup.players_to_be_added')}}</p>
                <table class="table light-grey-table flip-scroll-table">
                    <thead>
                        <tr>
                            <th>{{trans('pool.add_setup.pos')}}</th>
                            <th>{{trans('pool.add_setup.skaters')}}</th>
                            <th>{{trans('pool.add_setup.opp')}}</th>
                            <th>{{trans('pool.add_setup.pre')}}</th>
                            <th>{{trans('pool.add_setup.current')}}</th>
                            <th>{{trans('pool.add_setup.own')}}</th>
                            <th>{{trans('pool.add_setup.start')}}</th>
                            <?php
                            if (!empty($pool_details->poolScoreSettings)) {
                                foreach ($pool_details->poolScoreSettings as $score_setting) {
                                    if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.player')) {
                                        ?>
                                        <th><?php echo $score_setting->poolScoringField->stat; ?></th>
                                        <?php
                                    }
                                }
                            }
                            ?>
                            <th class="total-pool-points">{{trans('pool.add_setup.pool_points')}}</th>
                        </tr>
                    </thead>
                    <tbody id="add-players-outer"></tbody>
                </table>
                <script type="text/html" id="add-players-template">
                    <tr>
                        <td>
                            <input type="hidden" name="add_player_season_id[<%= playerCount %>]" value="<%= playerTeam.id %>"/>
                            <%= playerTeam.player_position.short_name %></td>
                        <td><%= playerTeam.player.full_name + ' (' + playerTeam.team.short_name + ')' %></td>
                        <td>                           
                            <% var oppPresent = false; 
                            if(!$.isEmptyObject(playerTeam.team.home_competition)){
                            oppPresent = true; %>
                            <%= '@'+playerTeam.team.home_competition.away_team.short_name %>
                            <% }
                            if(!$.isEmptyObject(playerTeam.team.away_competition)){ 
                            oppPresent = true; %>
                            <%= '@'+playerTeam.team.away_competition.home_team.short_name %>
                            <% } 
                            if(oppPresent == false){ %>
                            -
                            <% } %>
                        </td>
                        <td><%=playerTeam.player.player_pre_ranking?playerTeam.player.player_pre_ranking.rank:0 %></td>
                        <td>
                            <% if(!$.isEmptyObject(playerTeam.player.pool_season_rank)){ %>
                            <%= playerTeam.player.pool_season_rank.rank %>
                            <% }else{ %>
                            0
                            <% } %>
                        </td>
                        <td><%= playerTeam.own %>%</td>
                        <td><%= playerTeam.start %>%</td>
                        <?php
                        if (!empty($pool_details->poolScoreSettings)) {
                            foreach ($pool_details->poolScoreSettings as $score_setting) {
                                if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.player')) {
                                    ?>
                                    <td>
                                        <% if(playerTeam.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']==null){ %>
                                        0
                                        <% } else { %>
                                        <%= hpApp.twoDecimal(playerTeam.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']) %>
                                        <% } %>
                                    </td>
                                    <?php
                                }
                            }
                        }
                        ?>
                        <td class="total-pool-points">
                            <% if(!$.isEmptyObject(playerTeam.player.season_pool_point)){ %>
                            <%= hpApp.twoDecimal(playerTeam.player.season_pool_point.total_points) %>
                            <% }else{ %>
                            0
                            <% } %>
                        </td>
                    </tr>
                    </script>

                    <div class="pool--main-heading" id="my-team-head">
                        {{trans('pool.add_setup.my_team')}}
                        <span class="players--drop small-grey-title">{{trans('pool.add_setup.select_players_to_drop')}}</span>
                        <div class="color-explain">
                            <span class="light-red"></span>
                            <span>{{trans('pool.add_setup.dropped')}}</span>
                            <span class="light-green"></span>
                            <span>{{trans('pool.add_setup.traded')}}</span>
                        </div>
                    </div>

                    <script id="all-players-template" type="text/html">
                        <input type="hidden" value="<%= playersTeam.pool.pool_team_first.id %>" name="pool_team_id"/>
                        <table class="table light-grey-table flip-scroll-table" id="rosters-players">
                            <thead>
                                <tr class="sub-header">
                                    <th>{{trans('pool.add_setup.drop')}}</th>
                                    <th>{{trans('pool.add_setup.roster_pos')}}</th>
                                    <th>{{trans('pool.add_setup.skater_pos')}}</th>
                                    <th>{{trans('pool.add_setup.skaters')}}</th>
                                    <th>{{trans('pool.add_setup.opp')}}</th>
                                    <th>{{trans('pool.add_setup.pre')}}</th>
                                    <th>{{trans('pool.add_setup.current')}}</th>
                                    <th>{{trans('pool.add_setup.own')}}</th>
                                    <th>{{trans('pool.add_setup.start')}}</th>
                                    <?php
                                    if (!empty($pool_details->poolScoreSettings)) {
                                        foreach ($pool_details->poolScoreSettings as $score_setting) {
                                            if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.player')) {
                                                ?>
                                                <th><?php echo $score_setting->poolScoringField->stat; ?></th>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    <th class="total-pool-points">{{trans('pool.add_setup.pool_points')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% var playerCount = 0;
                                _.each(playersTeam.pool.pool_team_first.pool_team_players, function(value){
                                var playerStats = playersTeam.stat_data[value.player_season_id];
                                if(playerStats.player_position.name != poolScoringFields.type[2]){
                                //checking if player is deleted
                                var deletedClass = '', tradedClass = '';
                                if(value.deleted_at != null){
                                deletedClass = 'player-deleted';
                                }
                                //checking if player is dropped or deleted by one other parameter
                                if(!$.isEmptyObject(value.pool_trade_player)){
                                if(value.pool_trade_player.type == tradePlayersType.trade_players){
                                tradedClass = 'player-traded';
                                }
                                else if(value.pool_trade_player.type == tradePlayersType.drop_players){
                                deletedClass = 'player-deleted';
                                }
                                }
                                %>
                                <tr class="pool-player <%= deletedClass %> <%= tradedClass %>" data-pos="<%= playerStats.player_position.short_name %>">
                                    <td>
                                        <% if(value.deleted_at == null){ %>
                                        <div class="custom-checkbox">
                                            <input type="checkbox" id="squared<%= playerStats.player_id %>" name="">
                                            <label for="squared<%= playerStats.player_id %>"></label>
                                        </div>
                                        <% } %>
                                    </td>
                                    <td data-id="<%= value.id %>">
                                        <input type="hidden" name="drop_pool_team_players_id[<%= playerCount %>]" value=""/> 
                                        <% if(!$.isEmptyObject(value.pool_team_lineup)){ %>
                                        <%= value.pool_team_lineup.player_position.short_name %>
                                        <% } %>
                                    </td> 
                                    <td><%= playerStats.player_position.short_name %></td>
                                    <td><%= playerStats.player.full_name + ' (' + playerStats.team.short_name + ')' %></td>
                                    <td>
                                        <% var oppPresent = false; 
                                        if(!$.isEmptyObject(playerStats.team.home_competition)){
                                        oppPresent = true; %>
                                        <%= '@'+playerStats.team.home_competition.away_team.short_name %>
                                        <% }
                                        if(!$.isEmptyObject(playerStats.team.away_competition)){ 
                                        oppPresent = true; %>
                                        <%= '@'+playerStats.team.away_competition.home_team.short_name %>
                                        <% } 
                                        if(oppPresent == false){ %>
                                        -
                                        <% } %>
                                    </td>
                                    <td><%=playerStats.player.player_pre_ranking?playerStats.player.player_pre_ranking.rank:0 %></td>
                                    <td>
                                        <% if(!$.isEmptyObject(playerStats.player.pool_season_rank)){ %>
                                        <%= playerStats.player.pool_season_rank.rank %>
                                        <% }else{ %>
                                        0
                                        <% } %>
                                    </td>
                                    <td><%= playerStats.own %>%</td>
                                    <td><%= playerStats.start %>%</td>
                                    <?php
                                    if (!empty($pool_details->poolScoreSettings)) {
                                        foreach ($pool_details->poolScoreSettings as $score_setting) {
                                            if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.player')) {
                                                ?>
                                                <td>
                                                    <% if(playerStats.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']==null){ %>
                                                    0
                                                    <% } else { %>
                                                    <%= hpApp.twoDecimal(playerStats.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']) %>
                                                    <% } %>
                                                </td>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    <td class="total-pool-points">
                                        <% if(!$.isEmptyObject(playerStats.player.season_pool_point)){ %>
                                        <%= hpApp.twoDecimal(playerStats.player.season_pool_point.total_points) %>
                                        <% }else{ %>
                                        0
                                        <% } %>
                                    </td>
                                </tr>
                                <% } playerCount++; }); %> 
                            </tbody>
                        </table>

                        <table class="table light-grey-table flip-scroll-table" id="rosters-goalies">
                            <thead>
                                <tr class="sub-header">
                                    <th>{{trans('pool.add_setup.drop')}}</th>
                                    <th>{{trans('pool.add_setup.status')}}</th>
                                    <th>{{trans('pool.add_setup.pos')}}</th>
                                    <th>{{trans('pool.add_setup.skaters')}}</th>
                                    <th>{{trans('pool.add_setup.opp')}}</th>
                                    <th>{{trans('pool.add_setup.pre')}}</th>
                                    <th>{{trans('pool.add_setup.current')}}</th>
                                    <th>{{trans('pool.add_setup.own')}}</th>
                                    <th>{{trans('pool.add_setup.start')}}</th>
                                    <?php
                                    if (!empty($pool_details->poolScoreSettings)) {
                                        foreach ($pool_details->poolScoreSettings as $score_setting) {
                                            if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                                                ?>
                                                <th><?php echo $score_setting->poolScoringField->stat; ?></th>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    <th class="total-pool-points">{{trans('pool.add_setup.pool_points')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% var playerCount = 0;
                                _.each(playersTeam.pool.pool_team_first.pool_team_players, function(value){

                                var playerStats = playersTeam.stat_data[value.player_season_id];
                                if(playerStats.player_position.name == poolScoringFields.type[2]){
                                //checking if player is deleted
                                var deletedClass = '', tradedClass = '';
                                if(value.deleted_at != null){
                                deletedClass = 'player-deleted';
                                }
                                //checking if player is dropped or deleted by one other parameter
                                if(!$.isEmptyObject(value.pool_trade_player)){
                                if(value.pool_trade_player.type == tradePlayersType.trade_players){
                                tradedClass = 'player-traded';
                                }
                                else if(value.pool_trade_player.type == tradePlayersType.drop_players){
                                deletedClass = 'player-deleted';
                                }
                                }
                                %>
                                <tr class="pool-goalie <%= deletedClass %> <%= tradedClass %>" data-pos="<%= playerStats.player_position.short_name %>">
                                    <td>
                                        <% if(value.deleted_at == null){ %>
                                        <div class="custom-checkbox">
                                            <input type="checkbox" id="squared<%= playerStats.player_id %>" name="">
                                            <label for="squared<%= playerStats.player_id %>"></label>
                                        </div>
                                        <% } %>
                                    </td>
                                    <td data-id="<%= value.id %>">
                                        <input type="hidden" name="drop_pool_team_players_id[<%= playerCount %>]" value=""/> 
                                        <% if(!$.isEmptyObject(value.pool_team_lineup)){ %>
                                        <%= value.pool_team_lineup.player_position.short_name %>
                                        <% } %>
                                    </td> 
                                    <td><%= playerStats.player_position.short_name %></td>
                                    <td><%= playerStats.player.full_name + ' (' + playerStats.team.short_name + ')' %></td>
                                    <td>                                        
                                        <% var oppPresent = false; 
                                        if(!$.isEmptyObject(playerStats.team.home_competition)){
                                        oppPresent = true; %>
                                        <%= '@'+playerStats.team.home_competition.away_team.short_name %>
                                        <% }
                                        if(!$.isEmptyObject(playerStats.team.away_competition)){ 
                                        oppPresent = true; %>
                                        <%= '@'+playerStats.team.away_competition.home_team.short_name %>
                                        <% } 
                                        if(oppPresent == false){ %>
                                        -
                                        <% } %>
                                    </td>
                                    <td><%=playerStats.player.player_pre_ranking?playerStats.player.player_pre_ranking.rank:0 %></td>
                                    <td>
                                        <% if(!$.isEmptyObject(playerStats.player.pool_season_rank)){ %>
                                        <%= playerStats.player.pool_season_rank.rank %>
                                        <% }else{ %>
                                        0
                                        <% } %>
                                    </td>
                                    <td><%= playerStats.own %>%</td>
                                    <td><%= playerStats.start %>%</td>
                                    <?php
                                    if (!empty($pool_details->poolScoreSettings)) {
                                        foreach ($pool_details->poolScoreSettings as $score_setting) {
                                            if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                                                ?>
                                                <td>
                                                    <% if(playerStats.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']==null){ %>
                                                    0
                                                    <% } else { %>
                                                    <%= hpApp.twoDecimal(playerStats.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']) %>
                                                    <% } %>
                                                </td>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    <td class="total-pool-points">
                                        <% if(!$.isEmptyObject(playerStats.player.season_pool_point)){ %>
                                        <%= hpApp.twoDecimal(playerStats.player.season_pool_point.total_points) %>
                                        <% }else{ %>
                                        0
                                        <% } %>
                                    </td>
                                </tr>
                                <% } playerCount++; }); %> 
                            </tbody>
                        </table>
                        </script>
                        <div id="all-players-outer"></div>
                        {{ Form::close() }}

                        <!--add confirmation action-->
                        <div class="player__action">
                            <p>{{trans('pool.add_setup.action_cant_be_undone')}}</p>
                            <ul class="list-inline">
                                <li>
                                    <a class="btn btn-backgreen" href="#" id="accept-confirmation">{{trans('pool.add_setup.accept')}}</a>
                                </li>
                                <li>
                                    <a class="btn btn-orange-line" href="#" id="cancel-confirmation">{{trans('pool.add_setup.cancel')}}</a>
                                </li>
                            </ul>
                        </div>
                        <!--END add confirmation action-->
                        @include('frontend.includes.ad_small_block')
                    </div>
                </div>
            </div>
        </div>
        @stop  
        @section('after-scripts-end')
        {!! Html::script(elixir('js/add_setup.js')) !!}
        <script>
            var poolTeamSeasonUrl = '{{ route('frontend.pool.getPoolTeamSeasonStat', $pool_id) }}',
                    poolScoringFields = <?php echo json_encode(config('pool.pool_scoring_field')); ?>,
                    newAddedPlayers = '<?php echo $new_added_players; ?>',
                    addedPlayersUrl = '{{ route('frontend.pool.getPoolStatForPlayers', $pool_id) }}',
                    playerStatsUrl = '{{ route('frontend.pool.playerStats', $pool_id) }}',
                    tradePlayersType = <?php echo json_encode(config('pool.pool_trades.inverse_trade_player_type')); ?>,
                    add_setup = <?php echo json_encode(trans('pool.add_setup')); ?>;
        </script>
        @stop 