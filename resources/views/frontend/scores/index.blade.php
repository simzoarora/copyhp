@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/scores_index.css')) !!}
@stop
@section('content') 
<div class="container scores">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">
        <?php
        $competition_dates = $competition_dates->toArray();
        $current_date = (!empty($competition_dates)) ? $competition_dates[0] : date('Y-m-d');
        $selected_competition_date = date('Y-m-d', strtotime($current_date));
        $previous_date = date('Y-m-d H:i:s', strtotime('-1 day', strtotime($current_date)));
        $next_date = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($current_date)));
        ?>
        <div class="col-sm-8" id="scores"> 
            <div class="row" id="select-dates">
                <div class="schedule-header">
                    <h1 class="pool--top-heading">{{trans('nhl.scores.title')}}</h1>
                    <div class="schedule-dates">
                        <input type="text" id="datetimepicker"><i class="fa fa-calendar-o"></i>
                    </div>
                </div>
            </div>

            <script class="team-details-template" type="text/html">
                <div class="team-details selected">
                    <div class="row">
                        <div class="team-scores">
                            <div class="team-div">
                                <span class="team-logo">
                                    <img src="<%= awayTeamLogo %>">
                                </span>
                                <span class="team-name">
                                    <span><%= awayTeamFirstName %></span>
                                    <span><%= awayTeamNickName %></span>
                                    <span><%= awayTeamStats %></span>
                                </span>
                                <span class="team-score">
                                    <%= awayTeamScore %>

                                    <% if(awayTeamGoals[4] > 0){ %>
                                    <span>{{trans('scores.scores.ot')}}</span>
                                    <% } %>

                                    <% if(awayTeamGoals[5] > 0){ %>
                                    <span>{{trans('scores.scores.so')}}</span>
                                    <% } %>
                                </span>
                            </div>
                            <div class="team-div">
                                <span class="team-logo">
                                    <img src="<%= homeTeamLogo %>">
                                </span>
                                <span class="team-name">
                                    <span><%= homeTeamFirstName %></span>
                                    <span><%= homeTeamNickName %></span>
                                    <span><%= homeTeamStats %></span>
                                </span>
                                <span class="team-score">
                                    <%= homeTeamScore %>

                                    <% if(homeTeamGoals[4] > 0){ %>
                                    <span>{{trans('scores.scores.ot')}}</span>
                                    <% } %>

                                    <% if(homeTeamGoals[5] > 0){ %>
                                    <span>{{trans('scores.scores.so')}}</span>
                                    <% } %>
                                </span>
                            </div>
                        </div>
                        <div class="score-details">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>1</th>
                                        <th>2</th>
                                        <th>3</th>
                                        <% if(homeTeamGoals[4]>0 || awayTeamGoals[4]>0) {%>
                                        <th><b>{{trans('scores.scores.ot')}}</b></th>
                                        <% } %>
                                        <% if(homeTeamGoals[5]>0 || awayTeamGoals[5]>0) {%>
                                        <th><b>{{trans('scores.scores.so')}}</b></th>
                                        <% } %>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><%= awayTeamGoals[1] %></td>
                                        <td><%= awayTeamGoals[2] %></td>
                                        <td><%= awayTeamGoals[3] %></td>
                                        <% if(homeTeamGoals[4]>0 || awayTeamGoals[4]>0) {%>
                                        <td><b><%= awayTeamGoals[4] %></b></td>
                                        <% } %>
                                        <% if(homeTeamGoals[5]>0 || awayTeamGoals[5]>0) {%>
                                        <td><b><%= awayTeamGoals[5] %></b></td>
                                        <% } %>
                                    </tr> 
                                    <tr>
                                        <td><%= homeTeamGoals[1] %></td>
                                        <td><%= homeTeamGoals[2] %></td>
                                        <td><%= homeTeamGoals[3] %></td>
                                        <% if(homeTeamGoals[4]>0 || awayTeamGoals[4]>0) {%>
                                        <td><b><%= homeTeamGoals[4] %></b></td>
                                        <% } %>
                                        <% if(homeTeamGoals[5]>0 || awayTeamGoals[5]>0) {%>
                                        <td><b><%= homeTeamGoals[5] %></b></td>
                                        <% } %>
                                    </tr>
                                </tbody>
                            </table> 
                        </div>
                    </div>
                    <p class='game-player-details mt15'><span><%= awayTeamShortName %></span>:<span class='goal-players'><%  _.each(competitionGoals, function(v){ 
                            if(v.scorer.team_id == awayTeamId){ %>; <%= v.scorer.player.first_name.substring(0,1)+'. '+v.scorer.player.last_name %>
                            <% if(!$.isEmptyObject(v.first_assistance) || !$.isEmptyObject(v.second_assistance)){ %>
                            <%= 
                            ' ('
                            +(v.first_assistance?v.first_assistance.player.first_name.substring(0,1)+'. '+v.first_assistance.player.last_name:'')
                            +(v.second_assistance?', '+v.second_assistance.player.first_name.substring(0,1)+'. '+v.second_assistance.player.last_name:'')
                            +')'
                            %><% } %><% } }); %></span>
                    </p>
                    <p class='game-player-details mt15'><span><%= homeTeamShortName %></span>:<span class='goal-players'><%  _.each(competitionGoals, function(v){ 
                            if(v.scorer.team_id == homeTeamId){ %>; <%= v.scorer.player.first_name.substring(0,1)+'. '+v.scorer.player.last_name %>
                            <% if(!$.isEmptyObject(v.first_assistance) || !$.isEmptyObject(v.second_assistance)){ %>
                            <%= 
                            ' ('
                            +(v.first_assistance?v.first_assistance.player.first_name.substring(0,1)+'. '+v.first_assistance.player.last_name:'')
                            +(v.second_assistance?', '+v.second_assistance.player.first_name.substring(0,1)+'. '+v.second_assistance.player.last_name:'')
                            +')'
                            %><% } %><% } }); %></span>
                    </p>
                    <a href="{{route('frontend.scores.index')}}/<%= competitionId %>" class="btn btn-blue pull-right">{{trans('scores.scores.box_score')}}</a>
                </div>
                </script>

                <div class="row" id="team-details-all">  

                </div>
            </div>
            <div class="adblocks">
                @include('frontend.includes.create_pool')
                @include('frontend.includes.adblocks')
                @include('frontend.includes.hockeypool_news')
            </div>
        </div>
    </div>
    @stop
    @section('after-scripts-end')
    <script>
        var dayScoresUrl = '{{route('frontend.scores.dayScores', $selected_competition_date)}}',
                CurrentDate = '{{$selected_competition_date}}',
                scores = <?php echo json_encode(trans('scores.scores')); ?>;
    </script>
    {!! Html::script(elixir('js/scores_index.js')) !!}
    @stop