@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/game_details.css')) !!}
@stop
@section('content') 
<div class="container game-details">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }} 
    </div>
    <div class="row" id="pool--content">
        <div class="col-sm-8" id="game_details">
            <div class="row">

                <script class="team-details-template" type="text/html">
                    <h2 class='heading'><%= formattedMatchDate %></h2>
                    <div class="team-details">
                        <div class="row">
                            <div class="team-scores">   
                                <div class="team-div">
                                    <table>
                                        <tr>
                                            <td class='team-logo'><img src="<%= awayTeamLogo %>"></td>
                                            <td class='team-name'>
                                                <span><%= awayTeamFirstName %></span><br>
                                                <span class='large-font'><%= awayTeamNickName %></span>
                                                <%= awayTeamStats %>
                                            </td>
                                            <td class='team-score'>
                                                <%= awayTeamScore %>

                                                <% if(awayTeamGoals[4] > 0){ %>
                                                <span>{{trans('scores.game_details.ot')}}</span>
                                                <% } %>

                                                <% if(awayTeamGoals[5] > 0){ %>
                                                <span>{{trans('scores.game_details.so')}}</span>
                                                <% } %>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="team-div">
                                    <table>
                                        <tr>
                                            <td class='team-logo'><img src="<%= homeTeamLogo %>"></td>
                                            <td class='team-name'><span><%= homeTeamFirstName %></span><br>
                                                <span class='large-font'><%= homeTeamNickName %></span>
                                                <%= homeTeamStats %>
                                            </td>
                                            <td class='team-score'>
                                                <%= homeTeamScore %>

                                                <% if(homeTeamGoals[4] > 0){ %>
                                                <span>{{trans('scores.game_details.ot')}}</span>
                                                <% } %>

                                                <% if(homeTeamGoals[5] > 0){ %>
                                                <span>{{trans('scores.game_details.so')}}</span>
                                                <% } %>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="score-details">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>1</th>
                                            <th>2</th>
                                            <th>3</th>
                                            <% if(homeTeamGoals[4]>0 || awayTeamGoals[4]>0) {%>
                                            <th>{{trans('scores.game_details.ot')}}</th>
                                            <% } %>
                                            <% if(homeTeamGoals[5]>0 || awayTeamGoals[5]>0) {%>
                                            <th>{{trans('scores.game_details.so')}}</th>
                                            <% } %>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><%= awayTeamGoals[1] %></td>
                                            <td><%= awayTeamGoals[2] %></td>
                                            <td><%= awayTeamGoals[3] %></td>
                                            <% if(homeTeamGoals[4]>0 || awayTeamGoals[4]>0) {%>
                                            <td><%= awayTeamGoals[4] %></td>
                                            <% } %>
                                            <% if(homeTeamGoals[5]>0 || awayTeamGoals[5]>0) {%>
                                            <td><%= awayTeamGoals[5] %></td>
                                            <% } %>
                                        </tr>
                                        <tr>
                                            <td><%= homeTeamGoals[1] %></td>
                                            <td><%= homeTeamGoals[2] %></td>
                                            <td><%= homeTeamGoals[3] %></td>
                                            <% if(homeTeamGoals[4]>0 || awayTeamGoals[4]>0) {%>
                                            <td><%= homeTeamGoals[4] %></td>
                                            <% } %>
                                            <% if(homeTeamGoals[5]>0 || awayTeamGoals[5]>0) {%>
                                            <td><%= homeTeamGoals[5] %></td>
                                            <% } %>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <p class="mt15"><span><%= awayTeamShortName %></span>:<span class='goal-players'><%  _.each(competitionGoals, function(v){ 
                                if(v.scorer.team_id == awayTeamId){ %>; <%= v.scorer.player.first_name.substring(0,1)+'. '+v.scorer.player.last_name %>
                                <% if(!$.isEmptyObject(v.first_assistance) || !$.isEmptyObject(v.second_assistance)){ %>
                                <%= 
                                ' ('+
                                (v.first_assistance?v.first_assistance.player.first_name.substring(0,1)+'. '+v.first_assistance.player.last_name:'')+
                                (v.second_assistance?', '+v.second_assistance.player.first_name.substring(0,1)+'. '+v.second_assistance.player.last_name:'')
                                +')'
                                %><% } %><% } }); %></span>
                        </p>
                        <p class="mt15"><span><%= homeTeamShortName %></span>:<span class='goal-players'><%  _.each(competitionGoals, function(v){ 
                                if(v.scorer.team_id == homeTeamId){ %>; <%= v.scorer.player.first_name.substring(0,1)+'. '+v.scorer.player.last_name %>
                                <% if(!$.isEmptyObject(v.first_assistance) || !$.isEmptyObject(v.second_assistance)){ %>
                                <%= 
                                ' ('+
                                (v.first_assistance?v.first_assistance.player.first_name.substring(0,1)+'. '+v.first_assistance.player.last_name:'')+
                                (v.second_assistance?', '+v.second_assistance.player.first_name.substring(0,1)+'. '+v.second_assistance.player.last_name:'')
                                +')'
                                %><% } %><% } }); %></span>
                        </p>
                    </div>
                    </script> 
                    <div class="team-details-outer">
                    </div>

                    <script class="scoring-summary-template" type="text/html">
                        <% var homeTeamScoreCount = 0, awayTeamScoreCount = 0; 
                        var playerGoalsArr = [],
                        playerGoalsAssistArr = [];
                        // Getting all competition goals and assists
                        _.each(competitionGoals, function(v, index){
                            if(!$.isEmptyObject(v)){
                                if(!$.isEmptyObject(v.scorer.player) && !$.isEmptyObject(v.scorer.player.player_stat)){ 
                                    var goals = playerGamePointsArrSearch(v.scorer.player.id,playerGoalsArr);
                                }
                                if(!$.isEmptyObject(v.first_assistance)){
                                    if(!$.isEmptyObject(v.first_assistance.player) && !$.isEmptyObject(v.first_assistance.player.player_stat)){
                                        var first_assist = playerGamePointsArrSearch(v.first_assistance.player.id,playerGoalsAssistArr);
                                    }
                                }
                                if(!$.isEmptyObject(v.second_assistance)){
                                    if(!$.isEmptyObject(v.second_assistance.player) && !$.isEmptyObject(v.second_assistance.player.player_stat)){ 
                                        var seccond_assist = playerGamePointsArrSearch(v.second_assistance.player.id,playerGoalsAssistArr);
                                    }
                                }
                            }
                        });
                        console.log('playerGoalsArr: ', playerGoalsArr);
                        console.log('playerGoalsAssistArr: ', playerGoalsAssistArr);

                        function playerGamePointsArrSearch(player_id,pointsArr){
                            var found = false;
                            for( var i = 0, len = pointsArr.length; i < len; i++ ) {
                                if( pointsArr[i]['playerId'] === player_id ) {
                                    found = player_game_goals = pointsArr.filter(function ( obj ) {
                                        return obj.playerId === player_id;
                                    })[0];
                                }
                            }
                            if(found != false){
                                var index = pointsArr.indexOf(found);
                                return pointsArr[index]['goals'] = pointsArr[index]['goals'] + 1;
                            }else{
                                pointsArr.push({
                                    playerId : player_id,
                                    goals : 1,
                                    current_point : 0
                                });
                                return 1;
                            }
                        }
                        function playerPointsCalc(player_id,player_stats){
                            var total = 0;
                            var goals = parseInt(player_stats.goals);
                            var assists = parseInt(player_stats.assists);

                            //Subtracting competition goals or assists from total points
                            if (!isNaN(goals)) {
                                total = goals;

                                //Finding the player in array
                                var player_game_goals = playerGoalsArr.filter(function ( obj ) {
                                    return obj.playerId === player_id;
                                })[0];
                                var index = playerGoalsArr.indexOf(player_game_goals);
                                var players_index = index;
                                playerGoalsArr.fill(player_game_goals.current_point = player_game_goals.current_point + 1, index, index++);
                                var current_point = playerGoalsArr[players_index].current_point;

                                //Calculating current players points
                                total = total - player_game_goals.goals + current_point;

                            } else if (!isNaN(assists)) {
                                total = assists;

                                //Finding the player in array
                                var player_game_assists = playerGoalsAssistArr.filter(function ( obj ) {
                                    return obj.playerId === player_id;
                                })[0];
                                var index = playerGoalsAssistArr.indexOf(player_game_assists);
                                var players_index = index;
                                playerGoalsAssistArr.fill(player_game_assists.current_point = player_game_assists.current_point + 1, index, index++);
                                var current_point = playerGoalsAssistArr[players_index].current_point;

                                //Calculating current players points
                                total = total - player_game_assists.goals + current_point;
                            }

                            if(total == 0) total = 1;
                            return total;
                        }
                        //END
                        %>
                        <div class='scoring-summary'>
                            <div class='row'>
                                <h3 class='heading'>{{trans('scores.game_details.scoring_summary')}}</h3>
                                <div class='game-periods'>
                                    <h2>{{trans('scores.game_details.first_period')}}</h2>
                                    <% if(homeTeamGoals[1] > 0 || awayTeamGoals[1] > 0){ %>
                                    <table class="table flip-scroll-table">
                                        <thead>
                                            <tr>
                                                <th>{{trans('scores.game_details.team')}}</th>
                                                <th>{{trans('scores.game_details.score')}}</th>
                                                <th>{{trans('scores.game_details.goal')}}</th>
                                                <th>{{trans('scores.game_details.assist')}}</th>
                                                <th>{{trans('scores.game_details.assist')}}</th>
                                                <th>{{trans('scores.game_details.time')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                            _.each(competitionGoals, function(v, index){ 
                                            if(v.period == 1){ %>
                                            <tr>
                                                <td><% if(homeTeamId == v.scorer.team_id){ %> 
                                                    <%= homeTeamShortName %>
                                                    <% }else{ %>
                                                    <%= awayTeamShortName %>
                                                    <% } %>
                                                </td>
                                                <td><% if(homeTeamId == v.scorer.team_id){ %>
                                                    <%= ++homeTeamScoreCount %>-<%= awayTeamScoreCount %> 
                                                    <% }else{ %>
                                                    <%= homeTeamScoreCount %>-<%= ++awayTeamScoreCount %> 
                                                    <% } %>
                                                </td>
                                                <td><%= v.scorer.player.first_name.substring(0,1)+'. '+v.scorer.player.last_name %>
                                                    <% if(!$.isEmptyObject(v.scorer.player) && !$.isEmptyObject(v.scorer.player.player_stat)){ 
                                            
                                                    var playerPoints = playerPointsCalc(v.scorer.player.id,v.scorer.player.player_stat); %>
                                                    <%= '('+playerPoints+')' %>
                                                    <% } else { %>
                                                    <%= '('+1+')' %>
                                                    <% } %>
                                                </td>
                                                <td>
                                                    <% if(!$.isEmptyObject(v.first_assistance)){ %>
                                                    <%= v.first_assistance.player.first_name.substring(0,1)+'. '+v.first_assistance.player.last_name %>
                                                    <% if(!$.isEmptyObject(v.first_assistance.player) && !$.isEmptyObject(v.first_assistance.player.player_stat)){
                                                    
                                                    var playerPoints = playerPointsCalc(v.first_assistance.player.id,v.first_assistance.player.player_stat); %>
                                                    <%= '('+playerPoints+')' %>
                                                    <% } else { %>
                                                    <%= '('+1+')' %>
                                                    <% } } %>
                                                </td>
                                                <td>
                                                    <% if(!$.isEmptyObject(v.second_assistance)){ %>
                                                    <%= v.second_assistance.player.first_name.substring(0,1)+'. '+v.second_assistance.player.last_name %>
                                                    <% if(!$.isEmptyObject(v.second_assistance.player) && !$.isEmptyObject(v.second_assistance.player.player_stat)){ 
                                                    
                                                    var playerPoints = playerPointsCalc(v.second_assistance.player.id,v.second_assistance.player.player_stat); %>
                                                    <%= '('+playerPoints+')' %>
                                                    <% } else { %>
                                                    <%= '('+1+')' %>
                                                    <% } }%>
                                                </td>
                                                <td><%= _.changeTime(_.stringToTime(v.goal_time)) %></td>
                                            </tr>
                                            <% } }); %>
                                        </tbody>
                                    </table>
                                    <% }else{ %>
                                    <p>{{trans('scores.game_details.no_scores')}}</p>
                                    <% } %>
                                </div>

                                <div class='game-periods'>
                                    <h2>{{trans('scores.game_details.second_period')}}</h2>
                                    <% if(homeTeamGoals[2] > 0 || awayTeamGoals[2] > 0){ %>
                                    <table class="table flip-scroll-table">
                                        <thead>
                                            <tr>
                                                <th>{{trans('scores.game_details.team')}}</th>
                                                <th>{{trans('scores.game_details.score')}}</th>
                                                <th>{{trans('scores.game_details.goal')}}</th>
                                                <th>{{trans('scores.game_details.assist')}}</th>
                                                <th>{{trans('scores.game_details.assist')}}</th>
                                                <th>{{trans('scores.game_details.time')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%  _.each(competitionGoals, function(v){ 
                                            if(v.period == 2){ %>
                                            <tr>
                                                <td><% if(homeTeamId == v.scorer.team_id){ %>
                                                    <%= homeTeamShortName %>
                                                    <% }else{ %>
                                                    <%= awayTeamShortName %>
                                                    <% } %>
                                                </td>
                                                <td><% if(homeTeamId == v.scorer.team_id){ %>
                                                    <%= ++homeTeamScoreCount %>-<%= awayTeamScoreCount %> 
                                                    <% }else{ %>
                                                    <%= homeTeamScoreCount %>-<%= ++awayTeamScoreCount %> 
                                                    <% } %>
                                                </td>
                                                <td><%= v.scorer.player.first_name.substring(0,1)+'. '+v.scorer.player.last_name %>
                                                    <% if(!$.isEmptyObject(v.scorer.player) && !$.isEmptyObject(v.scorer.player.player_stat)){
                                                    <!-- var playerGoals = playerGoalsArrSearch(v.scorer.player.id,playerGoalsArr); -->
                                                    var playerPoints = playerPointsCalc(v.scorer.player.id,v.scorer.player.player_stat); %>
                                                    <%= '('+playerPoints+')' %>
                                                    <% } else { %>
                                                    <%= '('+1+')' %>
                                                    <% } %>
                                                </td>
                                                <td>
                                                    <% if(!$.isEmptyObject(v.first_assistance)){ %>
                                                    <%= v.first_assistance.player.first_name.substring(0,1)+'. '+v.first_assistance.player.last_name %>
                                                    <% if(!$.isEmptyObject(v.first_assistance.player) && !$.isEmptyObject(v.first_assistance.player.player_stat)){
                                                    <!-- var playerGoals = playerGoalsArrSearch(v.first_assistance.player.id,playerGoalsAssistArr); -->
                                                    var playerPoints = playerPointsCalc(v.first_assistance.player.id,v.first_assistance.player.player_stat); %>
                                                    <%= '('+playerPoints+')' %>
                                                    <% } else { %>
                                                    <%= '('+1+')' %>
                                                    <% } } %>
                                                </td>
                                                <td>
                                                    <% if(!$.isEmptyObject(v.second_assistance)){ %>
                                                    <%= v.second_assistance.player.first_name.substring(0,1)+'. '+v.second_assistance.player.last_name %>
                                                    <% if(!$.isEmptyObject(v.second_assistance.player) && !$.isEmptyObject(v.second_assistance.player.player_stat)){ 
                                                    <!-- var playerGoals = playerGoalsArrSearch(v.second_assistance.player.id,playerGoalsAssistArr); -->
                                                    var playerPoints = playerPointsCalc(v.second_assistance.player.id,v.second_assistance.player.player_stat); %>
                                                    <%= '('+playerPoints+')' %>
                                                    <% } else { %>
                                                    <%= '('+1+')' %>
                                                    <% } } %>
                                                </td>
                                                <td><%= _.changeTime(_.stringToTime(v.goal_time)) %></td>
                                            </tr>
                                            <% } }); %>
                                        </tbody>
                                    </table>
                                    <% }else{ %>
                                    <p>{{trans('scores.game_details.no_scores')}}</p>
                                    <% } %>
                                </div>

                                <div class='game-periods'>
                                    <h2>{{trans('scores.game_details.third_period')}}</h2>
                                    <% if(homeTeamGoals[3] > 0 || awayTeamGoals[3] > 0){ %>
                                    <table class="table flip-scroll-table">
                                        <thead>
                                            <tr>
                                                <th>{{trans('scores.game_details.team')}}</th>
                                                <th>{{trans('scores.game_details.score')}}</th>
                                                <th>{{trans('scores.game_details.goal')}}</th>
                                                <th>{{trans('scores.game_details.assist')}}</th>
                                                <th>{{trans('scores.game_details.assist')}}</th>
                                                <th>{{trans('scores.game_details.time')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%  _.each(competitionGoals, function(v){ 
                                            if(v.period == 3){ %>
                                            <tr>
                                                <td><% if(homeTeamId == v.scorer.team_id){ %>
                                                    <%= homeTeamShortName %>
                                                    <% }else{ %>
                                                    <%= awayTeamShortName %>
                                                    <% } %>
                                                </td>
                                                <td><% if(homeTeamId == v.scorer.team_id){ %>
                                                    <%= ++homeTeamScoreCount %>-<%= awayTeamScoreCount %> 
                                                    <% }else{ %>
                                                    <%= homeTeamScoreCount %>-<%= ++awayTeamScoreCount %> 
                                                    <% } %>
                                                </td>
                                                <td><%= v.scorer.player.first_name.substring(0,1)+'. '+v.scorer.player.last_name %>
                                                    <% if(!$.isEmptyObject(v.scorer.player) && !$.isEmptyObject(v.scorer.player.player_stat)){ 
                                                    <!-- var playerGoals = playerGoalsArrSearch(v.scorer.player.id,playerGoalsArr); -->
                                                    var playerPoints = playerPointsCalc(v.scorer.player.id,v.scorer.player.player_stat); %>
                                                    <%= '('+playerPoints+')' %>
                                                    <% } else { %>
                                                    <%= '('+1+')' %>
                                                    <% } %>
                                                </td>
                                                <td>
                                                    <% if(!$.isEmptyObject(v.first_assistance)){ %>
                                                    <%= v.first_assistance.player.first_name.substring(0,1)+'. '+v.first_assistance.player.last_name %>
                                                    <% if(!$.isEmptyObject(v.first_assistance.player) && !$.isEmptyObject(v.first_assistance.player.player_stat)){ 
                                                    <!-- var playerGoals = playerGoalsArrSearch(v.first_assistance.player.id,playerGoalsAssistArr); -->
                                                    var playerPoints = playerPointsCalc(v.first_assistance.player.id,v.first_assistance.player.player_stat); %>
                                                    <%= '('+playerPoints+')' %>
                                                    <% } else { %>
                                                    <%= '('+1+')' %>
                                                    <% } } %>
                                                </td>
                                                <td>
                                                    <% if(!$.isEmptyObject(v.second_assistance)){ %>
                                                    <%= v.second_assistance.player.first_name.substring(0,1)+'. '+v.second_assistance.player.last_name %>
                                                    <% if(!$.isEmptyObject(v.second_assistance.player) && !$.isEmptyObject(v.second_assistance.player.player_stat)){
                                                    <!-- var playerGoals = playerGoalsArrSearch(v.second_assistance.player.id,playerGoalsAssistArr); -->
                                                    var playerPoints = playerPointsCalc(v.second_assistance.player.id,v.second_assistance.player.player_stat); %>
                                                    <%= '('+playerPoints+')' %>
                                                    <% } else { %>
                                                    <%= '('+1+')' %>
                                                    <% } } %>
                                                </td>
                                                <td><%= _.changeTime(_.stringToTime(v.goal_time)) %></td>
                                            </tr>
                                            <% } }); %>
                                        </tbody>
                                    </table>
                                    <% }else{ %>
                                    <p>{{trans('scores.game_details.no_scores')}}</p>
                                    <% } %>
                                </div>

                                <div class='game-periods'>
                                    <h2>{{trans('scores.game_details.overtime')}}</h2>
                                    <% if(homeTeamGoals[4] > 0 || awayTeamGoals[4] > 0){ %>
                                    <table class="table flip-scroll-table">
                                        <thead>
                                            <tr>
                                                <th>{{trans('scores.game_details.team')}}</th>
                                                <th>{{trans('scores.game_details.score')}}</th>
                                                <th>{{trans('scores.game_details.goal')}}</th>
                                                <th>{{trans('scores.game_details.assist')}}</th>
                                                <th>{{trans('scores.game_details.assist')}}</th>
                                                <th>{{trans('scores.game_details.time')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%  _.each(competitionGoals, function(v){ 
                                            if(v.period == 4){ %>
                                            <tr>
                                                <td><% if(homeTeamId == v.scorer.team_id){ %>
                                                    <%= homeTeamShortName %>
                                                    <% }else{ %>
                                                    <%= awayTeamShortName %>
                                                    <% } %>
                                                </td>
                                                <td><% if(homeTeamId == v.scorer.team_id){ %>
                                                    <%= ++homeTeamScoreCount %>-<%= awayTeamScoreCount %> 
                                                    <% }else{ %>
                                                    <%= homeTeamScoreCount %>-<%= ++awayTeamScoreCount %> 
                                                    <% } %>
                                                </td>
                                                <td><%= v.scorer.player.first_name.substring(0,1)+'. '+v.scorer.player.last_name %>
                                                    <% if(!$.isEmptyObject(v.scorer.player) && !$.isEmptyObject(v.scorer.player.player_stat)){ 
                                                    <!-- var playerGoals = playerGoalsArrSearch(v.scorer.player.id,playerGoalsArr); -->
                                                    var playerPoints = playerPointsCalc(v.scorer.player.id,v.scorer.player.player_stat); %>
                                                    <%= '('+playerPoints+')' %>
                                                    <% } else { %>
                                                    <%= '('+1+')' %>
                                                    <% } %>
                                                </td>
                                                <td>
                                                    <% if(!$.isEmptyObject(v.first_assistance)){ %>
                                                    <%= v.first_assistance.player.first_name.substring(0,1)+'. '+v.first_assistance.player.last_name %>
                                                    <% if(!$.isEmptyObject(v.first_assistance.player) && !$.isEmptyObject(v.first_assistance.player.player_stat)){
                                                    <!-- var playerGoals = playerGoalsArrSearch(v.first_assistance.player.id,playerGoalsAssistArr); -->
                                                    var playerPoints = playerPointsCalc(v.first_assistance.player.id,v.first_assistance.player.player_stat); %>
                                                    <%= '('+playerPoints+')' %>
                                                    <% } else { %>
                                                    <%= '('+1+')' %>
                                                    <% } } %>
                                                </td>
                                                <td>
                                                    <% if(!$.isEmptyObject(v.second_assistance)){ %>
                                                    <%= v.second_assistance.player.first_name.substring(0,1)+'. '+v.second_assistance.player.last_name %>
                                                    <% if(!$.isEmptyObject(v.second_assistance.player) && !$.isEmptyObject(v.second_assistance.player.player_stat)){
                                                    <!-- var playerGoals = playerGoalsArrSearch(v.second_assistance.player.id,playerGoalsAssistArr); -->
                                                    var playerPoints = playerPointsCalc(v.second_assistance.player.id,v.second_assistance.player.player_stat); %>
                                                    <%= '('+playerPoints+')' %>
                                                    <% } else { %>
                                                    <%= '('+1+')' %>
                                                    <% } } %>
                                                </td>
                                                <td><%= _.changeTime(_.stringToTime(v.goal_time)) %></td>
                                            </tr>
                                            <% } }); %>
                                        </tbody>
                                    </table>
                                    <% }else{ %>
                                    <p>{{trans('scores.game_details.no_scores')}}</p>
                                    <% } %>
                                </div>

                            </div>
                        </div>
                        </script>
                        <div class="scoring-summary-outer"></div>

                        <script class="shootout-template" type="text/html">
                            <div class='shootout'>
                                <h2>{{trans('scores.game_details.shootouts')}}</h2>
                                <div class='row'>
                                    <% if(competitionShootouts.length > 0 ){ %>
                                    <table class="table flip-scroll-table">
                                        <thead>
                                            <tr>
                                                <th>{{trans('scores.game_details.so')}}</th>
                                                <th>{{trans('scores.game_details.team')}}</th>
                                                <th>{{trans('scores.game_details.result')}}</th>
                                                <th>{{trans('scores.game_details.player')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%  _.each(competitionShootouts, function(v){ %>
                                            <tr>
                                                <td><%= v.order %></td>
                                                <td><% if(v.skater_season.team_id == homeTeamId){var name=homeTeamShortName;}else{var name=awayTeamShortName;} %>
                                                    <%= name %></td>
                                                <td><% if(v.scored==1){var score = 'GOAL';}else{var score = 'SAVE';} %>
                                                    <%= score %></td>
                                                <td><%= v.skater_season.player.first_name+' '+v.skater_season.player.last_name %></td>
                                            </tr>
                                            <%  }); %>
                                        </tbody>
                                    </table>
                                    <% }else{ %>
                                    <p>{{trans('scores.game_details.no_shootouts')}}</p>
                                    <% } %>
                                </div>
                            </div>
                            </script>
                            <div class="shootout-outer">

                            </div>

                            <script class="box-score-template" type="text/html">
                                <div class="box-score">
                                    <div class="row">
                                        <h3 class='heading'>{{trans('scores.game_details.box_score')}}</h3>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>{{trans('scores.game_details.sog')}}</th>
                                                    <th>{{trans('scores.game_details.fo_cent')}}</th>
                                                    <th>{{trans('scores.game_details.pp')}}</th>
                                                    <th>{{trans('scores.game_details.pim')}}</th>
                                                    <th>{{trans('scores.game_details.hits')}}</th>
                                                    <th>{{trans('scores.game_details.blks')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%  _.each(competitionTeamStats, function(v){ 
                                                if(awayTeamId == v.team_id){ %>
                                                <tr>
                                                    <td class='teamlabel'><img src='<%= awayTeamLogo %>'><span><%= awayTeamNickName %></span></td>
                                                    <td><%= (v.shots?v.shots:0) %></td>
                                                    <td>
                                                        <% var foValue = (v.faceoff_total_wins/(v.faceoff_total_losses+v.faceoff_total_wins))*100;
                                                        if(foValue > 0){
                                                        foValue = Math.round(hpApp.twoDecimal(foValue));
                                                        }else{
                                                        foValue = 0;
                                                        } %>
                                                        <%= foValue %>%
                                                    </td>
                                                    <td><%= (v.goals_power_play==null?0:v.goals_power_play)+'/'+(v.power_plays?v.power_plays:0) %></td>
                                                    <td><%= (v.penalty_minutes?v.penalty_minutes:0) %></td>
                                                    <td><%= (v.player_hits?v.player_hits:0) %></td>
                                                    <td><%= (v.player_blocked_shots?v.player_blocked_shots:0) %></td>
                                                </tr>
                                                <% } }); %>
                                                <%  _.each(competitionTeamStats, function(v){ 
                                                if(homeTeamId == v.team_id){ %>
                                                <tr>
                                                    <td class='teamlabel'><img src='<%= homeTeamLogo %>'><span><%= homeTeamNickName %></span></td>
                                                    <td><%= (v.shots?v.shots:0) %></td>
                                                    <td>
                                                        <% var foValue = (v.faceoff_total_wins/(v.faceoff_total_losses+v.faceoff_total_wins))*100;
                                                        if(foValue > 0){
                                                        foValue = Math.round(hpApp.twoDecimal(foValue));
                                                        }else{
                                                        foValue = 0;
                                                        } %>
                                                        <%= foValue %>%
                                                    </td>
                                                    <td>
                                                        <%= (v.goals_power_play==null?0:v.goals_power_play)+'/'+(v.power_plays?v.power_plays:0) %>
                                                    </td>
                                                    <td><%= (v.penalty_minutes?v.penalty_minutes:0) %></td>
                                                    <td><%= (v.player_hits?v.player_hits:0) %></td>
                                                    <td><%= (v.player_blocked_shots?v.player_blocked_shots:0) %></td>
                                                </tr>
                                                <% } }); %>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                </script>
                                <div class="box-score-outer"></div>

                                <script class="team-player-details-template" type="text/html">
                                    <div class='team-player-details'>
                                        <div class='row'>
                                            <%  _.each(teamDetails, function(v){ var teamId = v.team_id; %>
                                            <h3 class='heading'><%= v.name %></h3>
                                            <table class="table flip-scroll-table">
                                                <thead>
                                                    <tr>
                                                        <th class="hidden-xs">#</th>
                                                        <th>{{trans('scores.game_details.forwards')}}</th>
                                                        <th>{{trans('scores.game_details.g')}}</th>
                                                        <th>{{trans('scores.game_details.a')}}</th>
                                                        <th>{{trans('scores.game_details.p')}}</th>
                                                        <th>+/-</th>
                                                        <th>{{trans('scores.game_details.pim')}}</th>
                                                        <th>{{trans('scores.game_details.sog')}}</th>
                                                        <th>{{trans('scores.game_details.hits')}}</th>
                                                        <th>{{trans('scores.game_details.blks')}}</th>
                                                        <th>{{trans('scores.game_details.fo_cent')}}</th>
                                                        <th>{{trans('scores.game_details.toi')}}</th>
                                                        <th>{{trans('scores.game_details.pp_toi')}}</th>
                                                        <th>{{trans('scores.game_details.sh_toi')}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <% 
                                                    _.each(competitionSkaterStats, function(v){ 
                                                    if(!$.isEmptyObject(v.player_season)){
                                                    if(teamId == v.player_season.team_id){ %>
                                                    <tr>
                                                        <td class="hidden-xs"><%= v.player_season.number %></td>
                                                        <td><%= v.player_season.player.first_name.substring(0,1)+'. '+v.player_season.player.last_name %></td>
                                                        <td><%= (v.goals==null?0:v.goals) %></td>
                                                        <td><%= (v.assists==null?0:v.assists) %></td>
                                                        <td><% if(v.goals==null){var goals=0}else{var goals=v.goals}
                                                            if(v.assists==null){var assists=0}else{var assists=v.assists} %>
                                                            <%= goals+assists %>
                                                        </td>
                                                        <td><%= (v.plus_minus==null?0:v.plus_minus) %></td>
                                                        <td><%= (v.penalty_minutes?v.penalty_minutes:0) %></td>
                                                        <td><%= (v.shots==null?0:v.shots) %></td>
                                                        <td><%= (v.hits==null?0:v.hits) %></td>
                                                        <td><%= (v.blocked_shots==null?0:v.blocked_shots) %></td>
                                                        <td><% if(v.faceoffs_won==null){faceoffs_won=0}else{faceoffs_won=v.faceoffs_won}
                                                            if(v.faceoffs_lost==null){faceoffs_lost=0}else{faceoffs_lost=v.faceoffs_lost} %>
                                                            <% if(faceoffs_won!=0 && faceoffs_lost!=0){ %>
                                                            <%= Math.round(hpApp.twoDecimal((faceoffs_won/(faceoffs_lost+faceoffs_won))*100)) %>
                                                            <% }else{ %>0<% } %>
                                                        </td>
                                                        <td><%= _.changeTime(v.time_on_ice_secs) %></td>
                                                        <td><%= _.changeTime(v.time_on_ice_power_play_secs) %></td>
                                                        <td><%= _.changeTime(v.time_on_ice_short_handed_secs) %></td>
                                                    </tr>
                                                    <% } } }); %>
                                                </tbody>
                                            </table>
                                            <table class="table flip-scroll-table">
                                                <thead>
                                                    <tr>
                                                        <th class="hidden-xs">#</th>
                                                        <th>{{trans('scores.game_details.goalies')}}</th>
                                                        <th>W</th>
                                                        <th>SA</th>
                                                        <th>GA</th>
                                                        <th>GAA</th>
                                                        <th>SV%</th>
                                                        <th>TOI</th>
                                                        <th>G</th>
                                                        <th>A</th>
                                                        <th>P</th>
                                                        <th>PIM</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%  _.each(competitionGoalieStats, function(v){ 
                                                    if(!$.isEmptyObject(v.data) && !$.isEmptyObject(v.player_season)){
                                                    if(teamId == v.player_season.team_id){ 
                                                    //checking goalie played
                                                    if(v.data.time_on_ice_secs != null && v.data.time_on_ice_secs > 0){
                                                    var noDataConstant = 0;
                                                    }else{
                                                    var noDataConstant = '-';
                                                    } 
                                                    %>
                                                    <tr>
                                                        <td class="hidden-xs"><%= v.player_season.number %></td>
                                                        <td><%= v.player_season.player.first_name.substring(0,1)+'. '+v.player_season.player.last_name %></td>
                                                        <td><%= (v.wins?v.wins:noDataConstant) %></td>
                                                        <td><%= (v.shots_against?v.shots_against:noDataConstant) %></td>
                                                        <td><%= (v.goals_against?v.goals_against:noDataConstant) %></td>
                                                        <td><%= (v.goals_against_average?hpApp.twoDecimal(v.goals_against_average):noDataConstant) %></td>
                                                        <td>
                                                            <%if(v.shots_against && v.goals_against){ 
                                                            var sPercentage = ((v.shots_against - v.goals_against)/v.shots_against); %>
                                                            <%= hpApp.threeDecimal(sPercentage) %>
                                                            <% } else{ %>
                                                            <%= noDataConstant %>
                                                            <% } %>
                                                        </td>
                                                        <td><%= (v.data.time_on_ice_secs?_.changeTime(v.data.time_on_ice_secs):noDataConstant) %></td>
                                                        <td><%= (v.data.goals==null?noDataConstant:v.data.goals) %></td>
                                                        <td><%= (v.data.assists==null?noDataConstant:v.data.assists) %></td>
                                                        <td><% if(v.data.goals==null){var goals=0}else{var goals=v.data.goals}
                                                            if(v.data.assists==null){var assists=0}else{var assists=v.data.assists}
                                                            var pValue = goals+assists; %>
                                                            <% if(pValue != 0){ %>
                                                            <%= pValue %>
                                                            <% }else{ %>
                                                            <%= noDataConstant %>
                                                            <% } %>
                                                        </td>
                                                        <td><%= (v.penalty_minutes?v.penalty_minutes:noDataConstant) %></td>
                                                    </tr>
                                                    <% } } }); %>
                                                </tbody>
                                            </table>
                                            <% }); %>
                                        </div>
                                    </div>
                                    </script>
                                    <div class="team-player-details-outer"></div>

                                </div>
                            </div>
                            <div class="adblocks">
                                @include('frontend.includes.adblocks')
                                @include('frontend.includes.create_pool')
                                @include('frontend.includes.hockeypool_news')
                            </div>
                        </div>
                    </div>
                    @stop
                    @section('after-scripts-end')
                    <script>
                        var getCompetitionScoreUrl = '{{route('frontend.scores.gameScores', $competition_id)}}',
                                game_details = <?php echo json_encode(trans('scores.game_details')); ?>;
                    </script>
                    {!! Html::script(elixir('js/game_details.js')) !!}
                    @stop