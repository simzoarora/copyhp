@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/trade.css')) !!}
@stop 
@section('content')
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">
        @include('frontend.includes.pool_nav')
        <div class="proposed__trade--container">
            <div class="bg-trans" style="display: none"></div>
            <?php $add_drop_page = true; ?>
            @include('frontend.includes.pool_name_header')

            <p class="pool--main-heading">Proposed Trade</p>
            <div class="propose__trade" id="propose__trade"></div>
            <script id="propose__trade-template" type="text/html">
                <% if(parentPoolTeamId == responseParentPoolTeamId){ %>
                <p>You proposed a trade to <span class="traded-team-name"><%= teamName %></span> on <span><%= createDate %></span>.
                    It will automatically expire on <span><%= expireDate %></span>
                </p>
                <% }else{ %>
                <p>A trade has been proposed to by <span class="traded-team-name"><%= teamName %></span> on <span><%= createDate %></span>.
                    It will automatically expire on <span><%= expireDate %></span>
                </p>
                <% } %> 
                </script>

                <p class="players__status title-diff">Your players to be traded</p>
                <div id="your-traded-outer"></div>

                <p class="players__status">Your players to be dropped</p>
                <div id="your-dropped-outer"></div>

                <p class="players__status title-diff"><span class="traded-team-name"></span> players to be traded</p>
                <div id="team-traded-outer"></div>


                <script id="all-players-template" type="text/html">
                    <table class="table light-grey-table flip-scroll-table" id="rosters-players">
                        <thead>
                            <tr>
                                <th>Status</th>
                                <th>Pos</th>
                                <th>Skaters</th>
                                <th>Opp</th>
                                <th>Pre</th>
                                <th>Current</th>
                                <th>Own</th>
                                <th>Start</th>
                                <?php
                                if (!empty($pool_details->poolScoreSettings)) {
                                    foreach ($pool_details->poolScoreSettings as $score_setting) {
                                        if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.player')) {
                                            ?>
                                            <th><?php echo $score_setting->poolScoringField->stat; ?></th>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <th>Pool Pts</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% if(!$.isEmptyObject(playersTeam)){
                            _.each(playersTeam, function(value){
                            var playerStats = poolTrade.stat_data[value.pool_team_player.player_season_id];
                            if(playerStats.player_position.name != poolScoringFields.type[2]){
                            if(value.type == type){
                            %>
                            <tr class="pool-player">
                                <td>
                                    <% if(!$.isEmptyObject(value.pool_team_player.pool_team_lineup)){ %>
                                    <%= value.pool_team_player.pool_team_lineup.player_position.short_name %>
                                    <% } %>
                                </td> 
                                <td><%= playerStats.player_position.short_name %></td>
                                <td><%= playerStats.player.full_name + ' (' + playerStats.team.short_name + ')' %></td>
                                <td>@<% if(!$.isEmptyObject(playerStats.team.home_competitions)){ %>
                                    <%= playerStats.team.home_competitions[0].away_team.short_name %>
                                    <% } %>
                                    <% if(!$.isEmptyObject(playerStats.team.away_competitions)){ %>
                                    <%= playerStats.team.away_competitions[0].home_team.short_name %>
                                    <% } %>
                                </td>
                                <td>pre</td>
                                <td>
                                    <% if(!$.isEmptyObject(playerStats.player.pool_season_rank)){ %>
                                    <%= playerStats.player.pool_season_rank.rank %>
                                    <% }else{ %>
                                    0
                                    <% } %>
                                </td>
                                <td><%= playerStats.own %>%</td>
                                <td><%= playerStats.start %>%</td>
                                <?php
                                if (!empty($pool_details->poolScoreSettings)) {
                                    foreach ($pool_details->poolScoreSettings as $score_setting) {
                                        if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.player')) {
                                            ?>
                                            <td>
                                                <% if(playerStats.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']==null){ %>
                                                0
                                                <% } else { %>
                                                <%= hpApp.twoDecimal(playerStats.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']) %>
                                                <% } %>
                                            </td>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <td>
                                    <% if(!$.isEmptyObject(playerStats.player.season_pool_point)){ %>
                                    <%= hpApp.twoDecimal(playerStats.player.season_pool_point.total_points) %>
                                    <% }else{ %>
                                    0
                                    <% } %>
                                </td>
                            </tr>
                            <% } } }); } %> 
                        </tbody>
                    </table>

                    <table class="table light-grey-table flip-scroll-table" id="rosters-goalies">
                        <thead>
                            <tr>
                                <th>Status</th>
                                <th>Pos</th>
                                <th>Skaters</th>
                                <th>Opp</th>
                                <th>Pre</th>
                                <th>Current</th>
                                <th>Own</th>
                                <th>Start</th>
                                <?php
                                if (!empty($pool_details->poolScoreSettings)) {
                                    foreach ($pool_details->poolScoreSettings as $score_setting) {
                                        if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                                            ?>
                                            <th><?php echo $score_setting->poolScoringField->stat; ?></th>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <th>Pool Pts</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% if(!$.isEmptyObject(playersTeam)){
                            _.each(playersTeam, function(value){
                            var playerStats = poolTrade.stat_data[value.pool_team_player.player_season_id];
                            if(playerStats.player_position.name == poolScoringFields.type[2]){
                            if(value.type == type){
                            %>
                            <tr class="pool-goalie">
                                <td>
                                    <% if(!$.isEmptyObject(value.pool_team_player.pool_team_lineup)){ %>
                                    <%= value.pool_team_player.pool_team_lineup.player_position.short_name %>
                                    <% } %>
                                </td> 
                                <td><%= playerStats.player_position.short_name %></td>
                                <td><%= playerStats.player.full_name + ' (' + playerStats.team.short_name + ')' %></td>
                                <td>@<% if(!$.isEmptyObject(playerStats.team.home_competitions)){ %>
                                    <%= playerStats.team.home_competitions[0].away_team.short_name %>
                                    <% } %>
                                    <% if(!$.isEmptyObject(playerStats.team.away_competitions)){ %>
                                    <%= playerStats.team.away_competitions[0].home_team.short_name %>
                                    <% } %>
                                </td>
                                <td>pre</td>
                                <td>
                                    <% if(!$.isEmptyObject(playerStats.player.pool_season_rank)){ %>
                                    <%= playerStats.player.pool_season_rank.rank %>
                                    <% }else{ %>
                                    0
                                    <% } %>
                                </td>
                                <td><%= playerStats.own %>%</td>
                                <td><%= playerStats.start %>%</td>
                                <?php
                                if (!empty($pool_details->poolScoreSettings)) {
                                    foreach ($pool_details->poolScoreSettings as $score_setting) {
                                        if ($score_setting->type == config('pool.pool_scoring_field.inverse_type.goalie')) {
                                            ?>
                                            <td>
                                                <% if(playerStats.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']==null){ %>
                                                0
                                                <% } else { %>
                                                <%= hpApp.twoDecimal(playerStats.player_stat['<?php echo $score_setting->poolScoringField->id; ?>']) %>
                                                <% } %>
                                            </td>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <td>
                                    <% if(!$.isEmptyObject(playerStats.player.season_pool_point)){ %>
                                    <%= hpApp.twoDecimal(playerStats.player.season_pool_point.total_points) %>
                                    <% }else{ %>
                                    0
                                    <% } %>
                                </td>
                            </tr>
                            <% } } }); } %> 
                        </tbody>
                    </table>
                    </script>

                    <div class="player__action" id="player__action"></div>
                    <script id="player__action-template" type="text/html">
                        <% if(parentPoolTeamId == responseParentPoolTeamId){ %>
                        <ul class="list-inline">
                            <li>
                                <a class="btn btn-orange-line action-status" href="#" data-status="<?php echo config('pool.pool_trades.inverse_status.cancelled_by_parent'); ?>">Cancel</a>
                            </li>
                        </ul>
                        <p>Trade automatically expires in <span id="traded-team-expire-time"><%= expireDate %></span>.</p>
                        <% }else{ %>
                        <ul class="list-inline">
                            <li>
                                <a class="btn btn-backgreen action-status" href="#" data-status="<?php echo config('pool.pool_trades.inverse_status.accepted'); ?>">Accept</a>
                            </li>
                            <li>
                                <a class="btn btn-orange-line action-status" href="#" data-status="<?php echo config('pool.pool_trades.inverse_status.rejected'); ?>">Reject</a>
                            </li>
                        </ul>
                        <p>Trade automatically expires in <span id="traded-team-expire-time"><%= expireDate %></span>.</p>
                        <% } %>
                        </script>

                        @include('frontend.includes.ad_small_block')
                    </div>
                </div>
            </div>
            @stop 
            @section('after-scripts-end')
            {!! Html::script(elixir('js/trade.js')) !!}
            <script>
                var proposeTradeUrl = '{{ route('frontend.pool_teams.tradeDetails', [$pool_id, $trade_id]) }}',
                        tradeStatusUrl = '{{ route('frontend.pool_teams.changeTradeStatus', [$pool_id, $trade_id]) }}',
                        playerStatsUrl = '{{ route('frontend.pool.playerStats', $pool_id) }}',
                        poolScoringFields = <?php echo json_encode(config('pool.pool_scoring_field')); ?>,
                        dropTradeType = <?php echo json_encode(config('pool.pool_trades')); ?>,
                        parentPoolTeamId = <?php echo $pool_details->poolTeam->id; ?>;
            </script>
            @stop 