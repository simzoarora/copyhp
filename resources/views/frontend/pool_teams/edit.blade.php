@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/edit_team.css')) !!}
<style>
    .dropzone:after{
        content:<?php echo trans('pool_teams.edit_team.drop_image'); ?>
    }
</style>
@stop 
@section('content')
<div class="container">
    <div class="row" id="pool--content">
        @include('frontend.includes.pool_nav')

        <div class="pool--content">
            <div class="bg-trans" style="display: none">
            </div> 
            <?php $dashboardPage = true; ?>
            @include('frontend.includes.pool_name_header')

            <div class="edit-pool-content">
                <p class="pool--main-heading"> {{trans('pool_teams.edit_team.edit_team_info')}}</p>
                {{Form::open(['id'=>'edit-team'])}}
                <div id="alert-box"></div>
                <div class="form-field">
                    {{ Form::label('name', trans('pool_teams.edit_team.team_name')) }}
                    <div class="input-field">
                        {{ Form::text('name',$pool_team->name, array('class' => 'field','required')) }}
                    </div>
                </div>
                <div class="form-field">
                    {{ Form::label('name', trans('pool_teams.edit_team.team_logo')) }}
                    <div class="input-field">
                        <div class="dropzone" id='html-image-upload' data-width="200"  data-height="150" data-save="false" data-button-edit="true" data-button-del="true" data-image="{{URL::asset('img/team_logos/'.$pool_team->logo)}}">
                            <input type="file" name="logo[file]"/>
                            <input type="hidden" name="logo[name]" value="{{$pool_team->logo}}"/>
                        </div>
                    </div>
                </div>
                <div class="form-field">
                    <button class="btn-backgreen submit-btn"><span>{{trans('buttons.general.submit')}}</span> <i class="fa fa-spinner fa-pulse"></i></button>
                </div>
                {{ Form::close() }}
            </div>
            <div class="adblocks">
                @include('frontend.includes.create_pool')
                @include('frontend.includes.adblocks') 
                @include('frontend.includes.hockeypool_news')
            </div>
        </div>

    </div>
</div>
@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/edit_team.js')) !!}
<script>
    var updateUrl = '{{route('frontend.pool.team.update',array('pool_id'=>$pool_team->pool_id,'team_id'=>$pool_team->id))}}',
            buttons_general = <?php echo json_encode(trans('buttons.general')); ?>;
</script>
@stop