@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/nhl_player_stats.css')) !!}
@stop 
@section('content')
<div class="container"> 
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">
        <div class="player--content">
            <h1 class="pool--top-heading">{{trans('nhl.player_stats.title')}}</h1>
            <div class="view-statistics">
                <p class="type-subtitle">{{trans('nhl.player_stats.view_by')}}</p>
            </div>
            <div class="player-select-box">
                <p class="type-subtitle">{{trans('nhl.player_stats.players_position')}}</p>
                <?php 
                    if (in_array('Bench', $player_positions)) {
                        unset($player_positions[array_search('Bench',$player_positions)]);
                    }
                ?>
                {{ Form::select('positions',$player_positions ,isset($_GET["position"])?$_GET["position"]:null,['class' => 'select-field','id'=>'position']) }}
            </div>
            <div class="player-select-box">
                <p class="type-subtitle">{{trans('nhl.player_stats.seasons')}}</p>
                {{ Form::select('season', $seasons, isset($_GET["season_id"])?$_GET["season_id"]:null, ['class' => 'select-field','id'=>'season']) }}
            </div>
            <div class="player-select-box">
                <p class="type-subtitle">{{trans('nhl.player_stats.game_type')}}</p>
                {{ Form::select('game_type',config('constant.nhl_play_stat.game_type'),isset($_GET["game_type"])?$_GET["game_type"]:null, ['class' => 'select-field','id'=>'game_type']) }}
            </div>
            <div class="player-select-box">
                <p class="type-subtitle">{{trans('nhl.player_stats.team')}}</p>
                {{ Form::select('team', [0=>'All Teams'] + $teams, isset($_GET["team"])?$_GET["team"]:null, ['class' => 'select-field','id'=>'team']) }}
            </div>
            <table class="table light-grey-table head-arrow flip-scroll-table" id="main-table">
                <thead>
                    <tr>
                        <th data-sortname="player">{{trans('nhl.player_stats.player')}}<span></span></th>
                        <th data-sortname="team">{{trans('nhl.player_stats.team')}}<span></span></th>
                        <th data-sortname="position" class="hide-if-goalie">{{trans('nhl.player_stats.pos')}}<span></span></th>
                        <th data-sortname="stat" data-stat_order_key="games_played">{{trans('nhl.player_stats.gp')}}<span></span></th>
                        <th data-sortname="stat" class="show-if-goalie" data-stat_order_key="games_started">{{trans('nhl.player_stats.gs')}}<span></span></th>
                        <th data-sortname="stat" class="show-if-goalie" data-stat_order_key="wins">{{trans('nhl.player_stats.w')}}<span></span></th>
                        <th data-sortname="stat" class="show-if-goalie" data-stat_order_key="losses">{{trans('nhl.player_stats.l')}}<span></span></th>
                        <th data-sortname="stat" class="show-if-goalie" data-stat_order_key="ties">{{trans('nhl.player_stats.t')}}<span></span></th>
                        <th data-sortname="stat" class="show-if-goalie" data-stat_order_key="overtime_losses">{{trans('nhl.player_stats.otl')}}<span></span></th>
                        <th data-sortname="stat" class="show-if-goalie" data-stat_order_key="shootout_losses">{{trans('nhl.player_stats.sol')}}<span></span></th>
                        <th data-sortname="stat" class="show-if-goalie" data-stat_order_key="shots_against">{{trans('nhl.player_stats.sa')}}<span></span></th>
                        <th data-sortname="stat" class="show-if-goalie" data-stat_order_key="saves">{{trans('nhl.player_stats.svs')}}<span></span></th>
                        <th data-sortname="stat" class="show-if-goalie" data-stat_order_key="goals_against">{{trans('nhl.player_stats.ga')}}<span></span></th>
                        <th class="show-if-goalie">{{trans('nhl.player_stats.sv_cent')}}</th>
                        <th class="show-if-goalie">{{trans('nhl.player_stats.gaa')}}</th>
                        <th class="show-if-goalie">{{trans('nhl.player_stats.toi')}}</th>
                        <th class="show-if-goalie">{{trans('nhl.player_stats.so')}}</th>
                        <th data-sortname="stat" data-stat_order_key="goals">{{trans('nhl.player_stats.g')}}<span></span></th>
                        <th data-sortname="stat" data-stat_order_key="assists">{{trans('nhl.player_stats.a')}}<span></span></th>
                        <th data-sortname="stat" class="hide-if-goalie" data-stat_order_key="points">{{trans('nhl.player_stats.p')}}<span></span></th>
                        <th data-sortname="stat" class="hide-if-goalie" data-stat_order_key="plus_minus">+/-<span></span></th>
                        <th data-sortname="stat" data-stat_order_key="penalty_minutes">{{trans('nhl.player_stats.pim')}}<span></span></th>
                        <th class="hide-if-goalie">{{trans('nhl.player_stats.p_gp')}}<span></span></th>
                        <th data-sortname="stat" class="hide-if-goalie" data-stat_order_key="goals_power_play">{{trans('nhl.player_stats.ppg')}}<span></span></th>
                        <th data-sortname="stat" class="hide-if-goalie" data-stat_order_key="points_power_play">{{trans('nhl.player_stats.ppp')}}<span></span></th>
                        <th data-sortname="stat" class="hide-if-goalie" data-stat_order_key="goals_short_handed">{{trans('nhl.player_stats.shg')}}<span></span></th>
                        <th data-sortname="stat" class="hide-if-goalie" data-stat_order_key="points_short_handed">{{trans('nhl.player_stats.shp')}}<span></span></th>
                        <th data-sortname="stat" class="hide-if-goalie" data-stat_order_key="game_winning_goals">{{trans('nhl.player_stats.gwg')}}<span></span></th>
                        <th data-sortname="stat" class="hide-if-goalie" data-stat_order_key="goals_overtime">{{trans('nhl.player_stats.otg')}}<span></span></th>
                        <th data-sortname="stat" class="hide-if-goalie" data-stat_order_key="shots">{{trans('nhl.player_stats.s')}}<span></span></th>
                        <th class="hide-if-goalie">{{trans('nhl.player_stats.toi_gp')}}</th>
                        <th class="hide-if-goalie">{{trans('nhl.player_stats.shifts_gp')}}</th>
                        <th class="hide-if-goalie" data-sortname="stat" data-stat_order_key="faceoffs_won">{{trans('nhl.player_stats.fow_cent')}}<span></span></th>
                    </tr>
                </thead>
                <tbody id="player-stats-all">
                </tbody>
            </table>

            <div class="post-pagination" style="display:none;">
                <div class="pagination-child">
                    <div id="pagination-first" class="pagination-arrows">
                        <div class="double-arrow"></div>
                    </div>
                    <div id="pagination-left" class="pagination-arrows"></div>
                    <div class="main-pagination">
                        <div id="page-list">

                        </div>
                    </div>
                    <div id="pagination-right" class="pagination-arrows"></div>
                    <div id="pagination-last" class="pagination-arrows">
                        <div class="double-arrow"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script id="player-stats-template" type="text/html">
    <%  _.each(players, function(player){  %>
    <tr>
        <td><%= player.full_name %></td>
        <td><%=player.player_season.team.display_name %></td>
        <td class="hide-if-goalie">
            <%if(player.player_season.player_position.short_name){ %>
            <%= player.player_season.player_position.short_name %>
            <% }else{ %>
            0
            <% } %>
        </td>
        <td>
            <%if(player.player_stat.games_played){ %>
            <%= player.player_stat.games_played %>
            <% }else{ %>
            0
            <% } %>
        </td>
        <td class="show-if-goalie">
            <%if(player.player_stat.games_started){ %>
            <%= player.player_stat.games_started %>
            <% }else{ %>
            0
            <% } %>
        </td>
        <td class="show-if-goalie">
            <%if(player.player_stat.wins) {%>
            <%= player.player_stat.wins %>
            <%} else{ %>
            0
            <% } %>
        </td>
        <td class="show-if-goalie">
            <%if(player.player_stat.losses){ %>
            <%= player.player_stat.losses %>
            <%} else{ %>
            0
            <% } %>
        </td>
        <td class="show-if-goalie">
            <%if(player.player_stat.ties){ %>
            <%= player.player_stat.ties %>
            <% }else{ %>
            0
            <% } %>
        </td>
        <td class="show-if-goalie">
            <%if(player.player_stat.overtime_losses) {%>
            <%= player.player_stat.overtime_losses %>
            <%} else{ %>
            0
            <% } %>
        </td>
        <td class="show-if-goalie">
            <%if(player.player_stat.shootout_losses) {%>
            <%= player.player_stat.shootout_losses %>
            <%} else{ %>
            0
            <% } %>
        </td>
        <td class="show-if-goalie">
            <%if(player.player_stat.shots_against) {%>
            <%= player.player_stat.shots_against %>
            <% }else{ %>
            0
            <% } %>
        </td>
        <td class="show-if-goalie">
            <%= player.player_stat.shots_against - player.player_stat.goals_against %>
        </td>
        <td class="show-if-goalie">
            <%if(player.player_stat.goals_against){ %>
            <%= player.player_stat.goals_against %>
            <%} else{ %>
            0
            <% } %>
        </td>
        <td class="show-if-goalie">
            <%if(player.player_stat.shots_against && player.player_stat.goals_against){ 
            var sPercentage = ((player.player_stat.shots_against - player.player_stat.goals_against)/player.player_stat.shots_against); %>
            <%= hpApp.threeDecimal(sPercentage) %>
            <% } else{ %>
            0
            <% } %>
        </td>
        <td class="show-if-goalie">
            <%if(player.player_stat.goals_against_average){ %>
            <%= hpApp.twoDecimal(player.player_stat.goals_against_average) %>
            <%} else{ %>
            0
            <% } %>
        </td>
        <td class="show-if-goalie">
            <%if(player.player_stat.time_on_ice_secs) {%>
            <%=Math.floor(player.player_stat.time_on_ice_secs/60) %>:<% if(Math.floor((player.player_stat.time_on_ice_secs)%60)<=9){%>
            0<%}%><%=Math.floor((player.player_stat.time_on_ice_secs)%60) %>
            <% } else{ %>
            0
            <% } %>
        </td>
        <td class="show-if-goalie">
            <%if(player.player_stat.shutouts){ %>
            <%= player.player_stat.shutouts %>
            <%} else{ %>
            0
            <% } %>
        </td>
        <td>
            <%if(player.player_stat.goals) {%>
            <%=player.player_stat.goals %>
            <%} else{ %>
            0
            <% } %>
        </td>
        <td>
            <%if(player.player_stat.assists){ %>
            <%=player.player_stat.assists %>
            <%} else{ %>
            0
            <% } %>
        </td>
        <td class="hide-if-goalie">
            <%if(player.points){ %>
            <%=player.points %>
            <%} else{ %>
            0
            <% } %>
        <td class="hide-if-goalie">
            <%if(player.player_stat.plus_minus){ %>
            <%=player.player_stat.plus_minus %>
            <%} else{ %>
            0
            <% } %>
        </td>
        <td>
            <%if(player.player_stat.penalty_minutes){ %>
            <%=player.player_stat.penalty_minutes %></td>
        <% }else{ %>
        0
        <% } %>
        <td class="hide-if-goalie">
            <%if(player.points && player.player_stat.games_played){ %>
            <%=(player.points/player.player_stat.games_played).toFixed(2) %></td>
        <% }else{ %>
        0
        <% } %>
    </td>
    <td class="hide-if-goalie">
        <%if(player.player_stat.goals_power_play) {%>
        <%= player.player_stat.goals_power_play %>
        <% }else{ %>
        0
        <% } %>
    <td class="hide-if-goalie">
        <%if(player.player_stat.points_power_play) {%>
        <%= player.player_stat.points_power_play %>
        <% }else{ %>
        0
        <% } %>
    </td>
    <td class="hide-if-goalie">
        <%if(player.player_stat.goals_short_handed) {%>
        <%= player.player_stat.goals_short_handed %>
        <%} else{ %>
        0
        <% } %>
    </td>
    <td class="hide-if-goalie"><%if(player.player_stat.points_short_handed){ %>
        <%= player.player_stat.points_short_handed %>
        <% }else{ %>
        0
        <% } %>
    </td>
    <td class="hide-if-goalie">
        <%if(player.player_stat.game_winning_goals){ %>
        <%= player.player_stat.game_winning_goals %>
        <% } else{ %>
        0
        <% } %>
    </td>
    <td class="hide-if-goalie">
        <%if(player.player_stat.goals_overtime){ %>
        <%= player.player_stat.goals_overtime %>
        <% }else{ %>
        0
        <% } %>
    </td>
    <td class="hide-if-goalie"><%=player.player_stat.shots %></td>
    <td class="hide-if-goalie">
        <%if(player.player_stat.time_on_ice_secs && player.player_stat.games_played) {%>
        <%=Math.floor((player.player_stat.time_on_ice_secs/player.player_stat.games_played)/60) %>:<% if(Math.floor((player.player_stat.time_on_ice_secs/player.player_stat.games_played)%60)<=9){%>
        0<%}%><%=Math.floor((player.player_stat.time_on_ice_secs/player.player_stat.games_played)%60) %>
        <% } else{ %>
        0
        <% } %>
    </td>
    <td class="hide-if-goalie">
        <%if(player.player_stat.shifts && player.player_stat.games_played) {%>
        <%=(player.player_stat.shifts/player.player_stat.games_played).toFixed(2) %>
        <%} else{ %>
        0
        <% } %>
    </td>
    <td class="hide-if-goalie">
        <%if(player.player_stat.faceoffs_won && player.player_stat.faceoffs_lost ) {
        %>
        <%= ((parseInt(player.player_stat.faceoffs_won)/(parseInt(player.player_stat.faceoffs_won)+parseInt(player.player_stat.faceoffs_lost)))*100).toFixed(2) %>
        <%} else{ %>
        0
        <% } %>
    </td>
</tr>
<% }) %>
</script>
@stop 
@section('after-scripts-end')
<script>
    var url = '{{route('frontend.nhl.getDataForPlayerStats')}}',
            get_url_name = "getDataForPlayerStats",
            normal_url_name = "player-stats",
            goalie_position_id = {{config('pool.inverse_player_position.g')}},
            order_type = "<?php echo isset($_GET["order_type"]) ? $_GET["order_type"] : false; ?>",
            order = "<?php echo isset($_GET["order"]) ? $_GET["order"] : false; ?>",
            stat_order_key = "<?php echo isset($_GET["stat_order_key"]) ? $_GET["stat_order_key"] : false; ?>",
            page = "<?php echo isset($_GET["page"]) ? $_GET["page"] : false; ?>",
            season_id = "<?php echo isset($_GET["season_id"]) ? $_GET["season_id"] : false; ?>",
            game_type = "<?php echo isset($_GET["game_type"]) ? $_GET["game_type"] : false; ?>",
            position = "<?php echo isset($_GET["position"]) ? $_GET["position"] : false; ?>",
            player_stats = <?php echo json_encode(trans('nhl.player_stats')); ?>;
</script>
{!! Html::script(elixir('js/nhl_player_stats.js')) !!}
@stop 