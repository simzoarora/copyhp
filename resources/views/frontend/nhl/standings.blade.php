@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/standings.css')) !!}
@stop
@section('content')
<?php
//$selected_year=isset($_GET["year"])?$_GET["year"]:null;
$selected_year = null;
?>
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="main-content"> 
        <div class="standings-container">
            <div class="standings-header">
                <h1 class="pool--top-heading">{{trans('nhl.standings.title')}}</h1>
                {{ Form::select('dates',$seasons, null, ['class' => 'select-field','id'=>'select_year']) }}

                <ul class="list-inline page-sub-nav">
                    <li><a class="selected" data-show="divisions-all">{{trans('nhl.standings.division')}}</a></li>
                    <li><a data-show="conference-all">{{trans('nhl.standings.conference')}}</a></li>
                    <li><a data-show="overall-all">{{trans('nhl.standings.overall')}}</a></li>
                </ul>
            </div>
            <script class="standings-template" type="text/html">
                <table class="table blue-table flip-scroll-table">
                    <thead>
                        <tr>
                            <th><%= division_name%></th>
                            <th>{{trans('nhl.standings.gp')}}</th>
                            <th>{{trans('nhl.standings.w')}}</th>
                            <th>{{trans('nhl.standings.l')}}</th>
                            <th>{{trans('nhl.standings.otl')}}</th>
                            <th>{{trans('nhl.standings.sol')}}</th>
                            <th>{{trans('nhl.standings.pts')}}</th>
                            <th>{{trans('nhl.standings.gf')}}</th>
                            <th>{{trans('nhl.standings.ga')}}</th>
                            <th>{{trans('nhl.standings.diff')}}</th>
                            <th>{{trans('nhl.standings.home')}}</th>
                            <th>{{trans('nhl.standings.away')}}</th>
                            <th>{{trans('nhl.standings.l10')}}</th>
                            <th>{{trans('nhl.standings.strk')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%  _.each(team_data, function(v){ %>
                        
                        <tr>
                            <td>
                                {{ Html::image('img/nhl_logos/<%=v.official.team.logo%>') }} 
                                <%=  v.official.manipulated_team_name %>
                            </td>
                            <td><%= v.official.games_played %></td>
                            <td><%= v.official.games_won %></td>
                            <td><%= v.official.games_lost %></td>
                            <td><%= v.official.games_lost_overtime %></td>
                            <td><%= v.official.games_lost_shootout %></td>
                            <td><%= v.official.calculated_points %></td>
                            <td><%= v.official.goals_for %></td>
                            <td><%= v.official.goals_against %></td>
                            <td><%= v.official.diff %></td>
                            <td><%= v.extra.home_league_standings ? v.extra.home_league_standings :'0-0-0-0' %></td>
                            <td><%= v.extra.away_league_standings ? v.extra.away_league_standings :'0-0-0-0' %></td>
                            <td><%= v.extra.last_10_league_standings ? v.extra.last_10_league_standings :'0-0-0-0' %></td>
                            <td>
                                <% if(v.official.loss_streak!=null){%>
                                L<%= v.official.loss_streak %>
                                <% } else if(v.official.win_streak!=null){%>
                                W<%= v.official.win_streak %>
                                <% } else{%>
                                -
                                <%}%>
                            </td>
                        </tr>
                        <% }) %>
                    <tbody>
                </table>
                </script>
                <div class="col-sm-12">
                    <div class="row" id="divisions-all">
                    </div>
                    <div class="row" id="conference-all" style="display: none;">
                    </div>
                    <div class="row" id="overall-all" style="display: none;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop 
    @section('after-scripts-end')
    <script>
        var getStandingData_url = '{{route('frontend.nhl.getStandingData')}}',
                standings = <?php echo json_encode(trans('nhl.standings')); ?>;
    </script>
    {!! Html::script(elixir('js/standings.js')) !!}
    @stop