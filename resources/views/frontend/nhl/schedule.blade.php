@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/schedule.css')) !!}
@stop
@section('content') 
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="main-content">
        <div class="schedule-container">
            <div class="schedule-header">
                <h1 class="pool--top-heading">{{trans('nhl.schedule.title')}}</h1>
                <div class="schedule-dates">
                    <input type="text" id="datetimepicker"><i class="fa fa-calendar-o"></i>
                </div>
                <div class="schedule-dropdowns">
                    {{ Form::select('teams',[0=>'Select Team']+$teams->toArray(), null, ['class' => 'select-field','id'=>'teams']) }}
                    {{ Form::select('competition_type',config('competition.schedule_competition_type'), null, ['class' => 'select-field','id'=>'competition-type']) }}
                </div>
            </div>
            <script id="matchup_data" type="text/html">
                <%  _.each(schedules, function(schedule,key){%>
                <%  if(_.isArray(schedule)){ %>
                <h3 class="pool--main-heading"><%= key %></h3>
                <table class="table blue-table">
                    <tr>
                        <th>{{trans('nhl.schedule.matchup')}}</th>
                        <% var scheduleResultClass = hpApp.replaceAll(key,' ','-'),
                        scheduleResultClass = hpApp.replaceAll(scheduleResultClass,',',''); %>
                        <th class="schedule-result <%= scheduleResultClass %>">{{trans('nhl.schedule.result')}}</th>
                        <th>{{trans('nhl.schedule.time')}}</th>
                    </tr>
                    <%  _.each(schedule, function(v){ %>
                    <tr>
                        <td>
                            <div>{{ Html::image('img/nhl_logos/<%= v.away_team.logo%>') }} 
                                <span class="name1"><%= v.away_team.display_name%></span></div><span>@</span><div>
                                {{ Html::image('img/nhl_logos/<%= v.home_team.logo%>') }} 
                                <span class="name2"><%= v.home_team.display_name%></span></div>
                        </td>

                        <% if(v.home_team_score != null && v.away_team_score != null){ %>
                        <td class="schedule-result <%= scheduleResultClass %>">
                            
                            
                             <% if(v.home_team_score > v.away_team_score){ %>
                             <div><span class="winner-bold">  <%= v.home_team.short_name %> <%= v.home_team_score %></span> </div>,<div> <%= v.away_team.short_name %> <%= v.away_team_score %> </div>
                                <%  }else if(v.home_team_score < v.away_team_score){ %>
                                <div> <%= v.home_team.short_name %> <%= v.home_team_score %> </div>,  <div><span class="winner-bold"> <%= v.away_team.short_name %> <%= v.away_team_score %> </span> </div>
                                <% }else{ %>
                                <div> <%= v.home_team.short_name %> <%= v.home_team_score %> </div>,  <div><%= v.away_team.short_name %> <%= v.away_team_score %>  </div>
                                <% }%>
                           
                            <% if(v.match_status.indexOf('OT') !== -1){ %>
                            {{trans('nhl.schedule.ot')}}
                            <% } else if(v.match_status.indexOf('SO') !== -1){ %>
                            {{trans('nhl.schedule.so')}}
                            <% } %>
                        </td>
                        <% }else{ %>
                        <td class="schedule-result <%= scheduleResultClass %>">-</td>
                        <% } %>
                        <td class="text-uppercase">
                            <%= moment(v.match_start_date).format("h:mm a")%> 
                        </td>
                    </tr>
                    <% }) %>
                    <% }else{ %>
                    <table class="table blue-table">
                        <tr class="<%= key==0?'show-heading':'hide' %>">
                            <th>{{trans('nhl.schedule.matchup')}}</th>
                            <th class="schedule-result">{{trans('nhl.schedule.result')}}</th>
                            <th>{{trans('nhl.schedule.time')}}</th>
                        </tr>
                        <tr>
                            <td>
                                <div>{{ Html::image('img/nhl_logos/<%= schedule.away_team.logo%>') }} 
                                    <span class="name1"><%= schedule.away_team.display_name%></span> </div><span>@</span><div>{{ Html::image('img/nhl_logos/<%= schedule.home_team.logo%>') }} 
                                    <span class="name2"><%= schedule.home_team.display_name%></span></div>
                            </td>
                            <% if(schedule.home_team_score != null && schedule.away_team_score != null){ %>
                            <td class="schedule-result <%= scheduleResultClass %>">
                                <% if(schedule.home_team_score > schedule.away_team_score){ %>
                               <div>  <span class="winner-bold">  <%= schedule.home_team.short_name %> <%= schedule.home_team_score %></span></div> , <div> <%= schedule.away_team.short_name %> <%= schedule.away_team_score %> </div>
                                <%  }else if(schedule.home_team_score < schedule.away_team_score){ %>
                               <div>  <%= schedule.home_team.short_name %> <%= schedule.home_team_score %> </div>, <div> <span class="winner-bold"> <%= schedule.away_team.short_name %> <%= schedule.away_team_score %> </span> </div>
                                <% }else{ %>
                                <div> <%= schedule.home_team.short_name %> <%= schedule.home_team_score %> </div>,  <div><%= schedule.away_team.short_name %> <%= schedule.away_team_score %>  </div>
                                <% }%> 

                                <% if(schedule.match_status.indexOf('OT') !== -1){ %>
                                {{trans('nhl.schedule.ot')}}
                                <% } else if(schedule.match_status.indexOf('SO') !== -1){ %>
                                {{trans('nhl.schedule.so')}}
                                <% } %>
                            </td>
                            <% }else{ %>
                            <td class="schedule-result <%= scheduleResultClass %>">-</td>
                            <% } %>
                            <td>
                                <%= schedule.formatted_match_date %> 
                            </td>
                        </tr>
                        <% } %>
                    </table>
                    <% }) %>
                    </script>
                    <div id="matchup-all"></div>
            </div>
            <div class="adblocks">
                @include('frontend.includes.create_pool')
                @include('frontend.includes.adblocks')
                @include('frontend.includes.hockeypool_news')
            </div>
        </div>
    </div>
    @stop 
    @section('after-scripts-end')
    <script>
        var url = '{{route('frontend.nhl.getScheduleData')}}',
                page_url = '{{route('frontend.nhl.schedule')}}',
                schedule = <?php echo json_encode(trans('nhl.schedule')); ?>,
                site_url = '<?php echo URL::to("/"); ?>',
                page_url_path = page_url.replace(site_url, ''),
                get_url_path = url.replace(site_url, '');
    </script>
    {!! Html::script(elixir('js/schedule.js')) !!}
    @stop