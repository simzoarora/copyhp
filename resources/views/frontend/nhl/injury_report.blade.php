@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/injury_report.css')) !!}
@stop
@section('content') 
<div class="container"> 
     <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content"> 
        <div class="injury-report-container">
            <div class="injury-report">
                <p class="pool--main-heading">{{trans('nhl.injury_report.nhl_injury_report')}}</p>
                <script id="injury_report_data" type="text/html">
                    <%  _.each(injuries, function(injury,key){%>
                    <div class="injury-report-post">
                        <div class="pic">
                            {{ HTML::image('img/nhl_logos/<%= injury.player_season.team.logo %>', 'profile-img', array('class' => 'player-pic')) }}
                        </div>
                        <div class="injury-report-details">
                            <div class="details"><span class="player"><%= injury.player_season.player.full_name %></span>&nbsp;-&nbsp;<span class="injury"><%= injury.injury_location %></span>&nbsp;-&nbsp;<span class="status"><%= injury.injury_display_status %></span></div>
                            <div class="description">
                                <p>
                                    <%= injury.injury_note %>
                                </p>
                            </div>
                            <div class="last-updated">
                                <p>{{trans('nhl.injury_report.last_updated')}}<span> <%= moment(injury.last_updated).format("MMM DD, YYYY")%></span></p>
                            </div>
                            <div class="first-report">
                                <p>{{trans('nhl.injury_report.first_report')}}<span> <%= moment(injury.injury_start_date).format("MMM DD, YYYY")%></span></p>
                            </div>
                        </div>
                    </div>
                    <% }) %>
                    </script>
                    <div id="injury-report-all">

                    </div>
                    @include('frontend.includes.ad_small_block')
                </div>
                <div class="adblocks">
                    @include('frontend.includes.create_pool')
                    @include('frontend.includes.adblocks') 
                    @include('frontend.includes.hockeypool_news')
                </div>
            </div>
        </div>
    </div>
    @stop 
    @section('after-scripts-end')
    <script>
        var url = '{{route('frontend.nhl.getDataForInjuryReport')}}',
                injury_report = <?php echo json_encode(trans('nhl.injury_report')); ?>;
    </script>
    {!! Html::script(elixir('js/injury_report.js')) !!}
    @stop