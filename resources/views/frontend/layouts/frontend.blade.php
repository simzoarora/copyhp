<?php 
use \Firebase\JWT\JWT;
if(Auth::id()){
    $encryptedUserId = JWT::encode(Auth::id(), env('JWT_KEY'));
}
else{
    $encryptedUserId=false;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{ csrf_token() }}" />
<?php getPageTitle(); ?>
        <title>{{ (isset($title))?$title:getPageTitle() }}</title>

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', '')">
        <meta name="author" content="@yield('meta_author', '')">
        <link href='//fonts.googleapis.com/css?family=Titillium+Web:400,600,700,300,200' rel='stylesheet' type='text/css'>
        @yield('meta')

        <?php if (!empty($share_data)) { ?>
            @include('frontend.includes.share_tags')  
        <?php } ?>
        <!-- Styles -->
        @yield('before-styles-end')

        @yield('after-styles-end')
   
    </head>
    <body>   
        <div class='container-fluid site-container'> 
            <div class='row'>
                @include('frontend.includes.scoreboard')  
                @include('frontend.includes.navigation')
                @include('includes.partials.messages')  
                @yield('content')
                @include('frontend.includes.footer')
            </div>
        </div>   
        <!-- JavaScripts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="{{asset('js / vendor / jquery / jquery - 2.1.4.min.js')}}"><\/script>')</script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.3.4/socket.io.js"></script>
        <script>
var api_url = '{{url('')}}',
        socket_url = '{{env('SOCKET_SERVER_IP')}}',
        socket_port = '{{env('SOCKET_SERVER_PORT')}}',
        user_id = '<?= \Auth::id(); ?>',
        encrypted_user_id='<?= $encryptedUserId;?>';
        
@if (Auth::user())
        var user_notification_url = '{{route('frontend.notifications.getUserNotifications')}}';
var user_notification_url_read = '{{route('frontend.notifications.markRead',0)}}';
@else
        var user_notification_url = false;
var user_notification_url_read = false;
@endif()
        </script> 

        @if (env('APP_ENV') == "development")
        {{ HTML::script("//cdn.trackduck.com/toolbar/prod/td.js", array('async' => 'async', 'data-trackduck-id' => '57d2e804d0344ee376e74873')) }}
        @endif
        @yield('before-scripts-end') 

        @yield('after-scripts-end')
        
        <script>
            Userback = window.Userback || {};
            Userback.access_token = '7799|12753|QxEMZY0mZWPickr5iOlu8WiykQuPWTcS7Osp6BJ7VtoU00O3op';
            (function(id) {
                var s = document.createElement('script');
                s.async = 1;s.src = 'https://static.userback.io/widget/v1.js';
                var parent_node = document.head || document.body;parent_node.appendChild(s);
            })('userback-sdk');
        </script>

        @include('includes.partials.ga')
    </body>
</html>
