<?php 
use \Firebase\JWT\JWT;
if(Auth::id()){
    $encryptedUserId = JWT::encode(Auth::id(), env('JWT_KEY'));
}
else{
    $encryptedUserId=false;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{ csrf_token() }}" />

        <title>@yield('title', app_name())</title>

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', '')">
        <meta name="author" content="@yield('meta_author', '')">
        @yield('meta')

        <!-- Styles -->
        @yield('before-styles-end')
        {{ Html::style(elixir('css/basic_user.css')) }}
        @yield('after-styles-end')

        <link href='//fonts.googleapis.com/css?family=Titillium+Web:400,600,700,300,200' rel='stylesheet' type='text/css'>
    </head>
    <body id="app-layout">

        <div class='container-fluid site-container'> 
            <div class='row'>
                @include('includes.partials.logged-in-as')
                @include('frontend.includes.scoreboard')  
                @include('frontend.includes.navigation')
                @include('includes.partials.messages') 
                @yield('content')
                @include('frontend.includes.footer')
            </div>
        </div>  

        <!-- Scripts -->
        {{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js') }}
        
        <script>window.jQuery || document.write('<script src="{{asset('js / vendor / jquery / jquery - 2.1.4.min.js')}}"><\/script>')</script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.3.4/socket.io.js"></script>
        <script>
            var api_url = '{{url('')}}',
                    socket_url = '{{env('SOCKET_SERVER_IP')}}',
                    socket_port = '{{env('SOCKET_SERVER_PORT')}}',
                    user_id = '<?= \Auth::id(); ?>',
                    encrypted_user_id='<?= $encryptedUserId;?>';
                    
            @if (Auth::user())
                    var user_notification_url = '{{route('frontend.notifications.getUserNotifications')}}';
            var user_notification_url_read = '{{route('frontend.notifications.markRead',0)}}';
            @else
                    var user_notification_url = false;
            var user_notification_url_read = false;
            @endif()
        </script>

        @if (env('APP_ENV') == "development")
        {{ HTML::script("//cdn.trackduck.com/toolbar/prod/td.js", array('async' => 'async', 'data-trackduck-id' => '57d2e804d0344ee376e74873')) }}
        @endif

        @yield('before-scripts-end')
        {!! Html::script(elixir('js/basic_user.js')) !!}
        @yield('after-scripts-end')

        @include('includes.partials.ga')
    </body>
</html>
