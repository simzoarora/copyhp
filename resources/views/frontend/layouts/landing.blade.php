<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Hockeypool.com - Coming Soon</title>
        <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="google-site-verification" content="bPByJVle_7Se6WvGLuPtlud5TVbVKBwIRIrIytdMnJw" />
        <link href='//fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
        <!-- Styles -->
        @yield('before-styles-end')
        {!! Html::style(elixir('css/landing.css')) !!}
        @yield('after-styles-end')
    </head> 
    <body>
        @yield('content') 

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        
        @yield('scripts');
        @yield('before-scripts-end') 
        {!! Html::script(elixir('js/landing.js')) !!}
        @yield('after-scripts-end')
    </body>
</html>