@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/payment.css')) !!}
@stop 
@section('content')
<div class="container">
    <div class="row">
        <div class="payment-content">
            <div class="payment-content--left">
                <div class="premium-heading">
                    <span class="crown-icon"></span>
                    {{ $pool_payment_item->name }}
                </div>
                <div class="premium-description">
                    <p>{{trans('payments.payments.line1')}}
                    </p>
                    <p>{!! trans('payments.payments.line2') !!}
                        
                    </p>
                    <h2>{{trans('payments.payments.lucky_you')}}</h2>
                    <div class="premium-price--rate">{{trans('payments.payments.reg_price')}}<strike>${{ $pool_payment_item->actual_price }}</strike></div>
                    <div class="premium-price--rate btn-blue">{{trans('payments.payments.beta_price')}}<b>${{ $pool_payment_item->price }}</b></div>
                    <a href="{{ $link }}" class="btn-backgreen" target="_blank">{{trans('payments.payments.complete_setup')}}</a>
                </div>
            </div>
            <div class="payment-content--right">
                <ul>
                    <li>
                        <div>
                            <span><i class="fa fa-laptop"></i></span>
                            {{trans('payments.payments.no_ads')}}
                        </div>
                    </li>
                    <li>
                        <div>
                            <span><i class="fa fa-mobile"></i></span>
                            {{trans('payments.payments.smartphone_optimized')}}
                        </div>
                    </li>
                    <li>
                        <div>
                            <span><i class="fa fa-wifi"></i></span>
                            {{trans('payments.payments.live_scoring_updates')}}
                        </div>
                    </li>
                    <li>
                        <div>
                            <span><i class="fa fa-user"></i></span>
                            {{trans('payments.payments.tech_support')}}
                        </div>
                    </li>
                </ul>
                <?php if (!empty($red)) { //$ad_link ?>
                    <p>
                        <a href="{{ $ad_link }}">{{trans('payments.payments.prefer_ads')}}</a>
                        {{trans('payments.payments.ad_supported_version_text')}}
                    </p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/payment.js')) !!}
@stop 