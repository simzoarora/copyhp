@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/payment.css')) !!}
@stop 
@section('content')
<div class="container">
    <div class="row">
        <div class="payment-content draft-kit-content">
            <div class="payment-content--left">
                <div class="premium-heading">
                    <span class="crown-icon"></span>
                    {{trans('pages.draft_kit.hockeypool')}}<span class="hidden-xxs">{{trans('pages.draft_kit.dotcom')}}</span>{{trans('pages.draft_kit.title')}}
                </div>
                <div class="premium-description">
                    <p>
                        {{trans('pages.draft_kit.line1')}}
                    </p>
                    <p>
                        {{trans('pages.draft_kit.line2')}}
                    </p>
                    <p>
                        {{trans('pages.draft_kit.line3')}}
                    </p>
                    {!! Form::open(['route' => 'frontend.pages.advertiseSubmit', 'id' => 'advertise-form','data-link'=>$link, 'role' => 'form', 'method' => 'post']) !!}
                    {{ Form::email('email', null, array('class' => 'grey-input-field','placeholder'=>trans('pages.draft_kit.email_placeholder'),'required')) }}
                    <div class="premium-price--rate btn-blue">{{trans('pages.draft_kit.price')}}<b>${{$price->value}}</b></div>
                    <button disabled="disabled" type="submit" class="btn btn-backgreen disabled">{{trans('pages.draft_kit.complete_purchased')}}</button>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="payment-content--right">
                <ul>
                    <li>
                        <div>
                            <span><i class="fa fa-check-circle-o"></i></span>
                            {{trans('pages.draft_kit.checkpoint1')}}
                        </div>
                    </li>
                    <li>
                        <div>
                            <span><i class="fa fa-check-circle-o"></i></span>
                            {{trans('pages.draft_kit.checkpoint2')}}
                        </div>
                    </li>
                    <li>
                        <div>
                            <span><i class="fa fa-check-circle-o"></i></span>
                            {{trans('pages.draft_kit.checkpoint3')}}
                        </div>
                    </li>
                    <li>
                        <div>
                            <span><i class="fa fa-check-circle-o"></i></span>
                            {{trans('pages.draft_kit.checkpoint4')}}
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/payment.js')) !!}
<script>
    var error_title = '{{trans('pages.draft_kit.error_title')}}',
            error_message = '{{trans('pages.draft_kit.error_message')}}';

</script>
@stop 