<div class="my-team">
    <h3 class="pool--small-heading">
        <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>{{trans('livedraft.team.my_team')}}
    </h3>
    <div class="all-teams">
        <table class="table">
            <thead>
                <tr>
                    <th>{{trans('livedraft.team.pos')}}</th>
                    <th>{{trans('livedraft.queue.player')}}</th>
                </tr>
            </thead>
            <tbody id="my-team-list-all"></tbody>
        </table>
        <script id="my-team-list-template" type="text/html">
            <% _.each(myTeams, function(team){ %>
            <tr>
                <td><%= team.position %></td>
                <td><%= team.pool_team_player!=null?team.pool_team_player.player_season.player.full_name:'-- empty --' %></td>
            </tr>
            <% }) %>
            </script>
        </div>
    </div>