<table>
    <tr>
        <td>{{trans('livedraft.pool_settings.pool_type')}}</td>
        <td>
            @if(isset($pool_details->poolSetting->poolsetting->pool_format))
            @if(isset(config('pool.format.h2h')[$pool_details->poolSetting->poolsetting->pool_format])!=null)
            {{config('pool.format.h2h')[$pool_details->poolSetting->poolsetting->pool_format] }}
            @else
            {{config('pool.format.standard')[$pool_details->poolSetting->poolsetting->pool_format] }}
            @endif
            @endif
        </td>
    </tr>
    @if(!$pool_details->poolRosters->isEmpty())
    <tr>
        <td>{{trans('livedraft.pool_settings.positions')}}</td>
        <td>
            <div class="control-commas">
                @foreach($pool_details->poolRosters as $key => $poolRosters)<?php for ($i = 0; $i < $poolRosters->value; $i++) { ?>{{ ', '.$poolRosters->playerPosition->short_name }}<?php } ?>@endforeach()
            </div>
        </td>
    </tr>
    @endif
    <tr>
        <td>{{trans('livedraft.pool_settings.points')}}</td>
        <td>
            <?php foreach ($pool_details->poolScoreSettings as $score_setting) { ?>
                <span><?php echo $score_setting->poolScoringField->title . " (" . $score_setting->poolScoringField->stat . ")"; ?> - <?php echo $score_setting->value; ?> point</span>
            <?php } ?>
        </td>
    </tr>
    <tr>
        <td>{{trans('livedraft.pool_settings.max_trades')}}</td>
        <td>{{config('pool.max_trades_season')[$pool_details->poolSetting->poolsetting->max_trades_season] }}</td>
    </tr>
    <tr>
        <td>Max Acquisition Per Week:</td>
        <td>{{ config('pool.max_acquisition_week')[$pool_details->poolSetting->poolsetting->max_acquisition_week] }}</td>
    </tr>
    <tr>
        <td>Max Acquisition Per Team:</td>
        <td>{{ config('pool.max_acquisition_team')[$pool_details->poolSetting->poolsetting->max_acquisition_team] }}</td>
    </tr>
</table>