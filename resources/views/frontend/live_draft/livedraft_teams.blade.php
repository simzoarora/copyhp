<div id="teams-list-all"></div>
<script id="teams-list-template" type="text/html">
    <% _.each(teams, function(team){ %>
    <h3 class="pool--main-heading"><%= team.team.name %></h3>
    <table class="table light-grey-table head-arrow flip-scroll-table">
        <thead>
            <tr>
                <th>Round</th>
                <th>Pick</th>
                <th>Overall</th>
                <th>Name</th>
                <th>Team</th>
                <th>Position</th>
            </tr>
        </thead>
        <tbody>
            <% var count = 1;
            _.each(team.rounds, function(round){ %>
            <tr>
                <td><%= count++ %></td>
                <td><%= round.pick %></td>
                <td><%= round.overall %></td>
                <td><%= round.player!=null?round.player.player_season.player.full_name:'' %></td>
                <td><%= round.player!=null?round.player.player_season.team.short_name:'' %></td>
                <td><%= round.player!=null?round.player.player_season.player_position.short_name:'' %></td>
            </tr>
            <% }) %>
        </tbody>
    </table>
    <% }) %>
</script>