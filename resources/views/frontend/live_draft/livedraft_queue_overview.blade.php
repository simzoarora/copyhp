<div class="queue-overview">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#players" aria-controls="players" role="tab" data-toggle="tab">{{trans('livedraft.navs.players')}}</a></li>
        <li role="presentation"><a href="#teams" aria-controls="teams" role="tab" data-toggle="tab">{{trans('livedraft.navs.teams')}}</a></li>
        <li role="presentation"><a href="#draft-results" aria-controls="draft-results" role="tab" data-toggle="tab">{{trans('livedraft.navs.draft_result')}}</a></li>
        <li role="presentation"><a href="#pool-settings" aria-controls="pool-settings" role="tab" data-toggle="tab">{{trans('livedraft.navs.pool_setting')}}</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="players">
            @include('frontend.live_draft.livedraft_players') 
        </div>
        <div role="tabpanel" class="tab-pane" id="teams">
          @include('frontend.live_draft.livedraft_teams') 
        </div>
        <div role="tabpanel" class="tab-pane" id="draft-results">
            @include('frontend.live_draft.livedraft_draft_results') 
        </div>
        <div role="tabpanel" class="tab-pane" id="pool-settings">
          @include('frontend.live_draft.livedraft_pool_settings') 
        </div>
    </div>
</div>
