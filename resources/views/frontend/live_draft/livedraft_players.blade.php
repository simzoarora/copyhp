<div class="player-header">
    {{ Form::select('positions',[0=>trans('pool.player_stats.select_position')] + $positions ,null,['class' => 'select-field','id'=>'position']) }}
    {{ Form::text('search', null, ['class' => 'grey-input-field search-field', 'placeholder'=>trans('livedraft.players.search_player'),'id'=>'search-player-stats']) }}
    {{ Form::select('teams', [0=>trans('pool.player_stats.select_team')] + $teams, null, ['class' => 'select-field team-select2','id'=>'team']) }}
    {{ Form::text('search', null, ['class' => 'grey-input-field search-field mob', 'placeholder'=>trans('livedraft.players.search_player'),'id'=>'search-player-stats-mob']) }}
    {{ Form::reset('Reset', ['class' => 'btn-grey', 'id' => 'search-player-reset']) }}
</div>
<div id="player-table-outer">
    <table class="table player-stats-table flip-scroll-table">
        <thead>
            <tr>
                <th class="player-stats-sort-th" data-order_type="desc" data-stat_order_key="pre_ranking">{{trans('livedraft.players.rank')}} <span></span></th>
                <th>{{trans('livedraft.players.player')}}</th>
                <th class="player-stats-sort-th" data-order_type="desc" data-stat_order_key="games_played">GP <span></span></th><!--make it trans-->
                <?php
                $both_stats_list_heading = [];
                foreach ($pool_details->poolScoreSettings as $key => $val) {
                    if ($val->poolScoringField->type == config('pool.pool_scoring_field.inverse_type.both')) {
                        if (array_search($val->poolScoringField->stat, $both_stats_list_heading) === false) {
                            $both_stats_list_heading[] = $val->poolScoringField->stat;
                            echo('<th class="data-both player-stats-sort-th" data-order_type="desc"  data-stat_order_key="' . $val->poolScoringField->id . '">' . $val->poolScoringField->stat . ' <span></span></th>');
                        }
                    } else {
                        echo('<th class="data-player-goalie player-stats-sort-th" data-order_type="desc"  data-stat_order_key="' . $val->poolScoringField->id . '">' . $val->poolScoringField->stat . ' <span></span></th>');
                    }
                }
                ?>
            </tr>
        </thead>
        <tbody id="player-stats-all"></tbody>
    </table>
</div>
<div class="post-pagination" style="display:none;">
    <div class="pagination-child">
        <div id="pagination-first" class="pagination-arrows">
            <div class="double-arrow"></div>
        </div>
        <div id="pagination-left" class="pagination-arrows"></div>
        <div class="main-pagination">
            <div id="page-list">

            </div>
        </div>
        <div id="pagination-right" class="pagination-arrows"></div>
        <div id="pagination-last" class="pagination-arrows">
            <div class="double-arrow"></div>
        </div>
    </div>
</div>

<script id="player-stats-template" type="text/html">
    <% _.each(players, function(p){ %>
    <tr data-player_season_id="<%= p.id %>">
        <td>
            <!--Need to chnage anik will provide-->
            <% if(p.player.player_pre_ranking!=null){ %>
            <%= p.player.player_pre_ranking.rank %>
            <% } else{%>
            0
            <% }%>
        </td>
        <td>
            <%= p.player.full_name %> <span><%= p.team.short_name %> - <%= p.player_position.short_name %></span>
        </td>
        <td>
            <!--is this correct GP-->
            <% if(p.player.player_stat.games_played==null){ %>
            0
            <% } else{ %>
            <%= hpApp.twoDecimal(p.player.player_stat.games_played) %>
            <% }%>
        </td>
        <?php

        function showPlayerStat($class, $val) {
            ?>
            <td class="<?php echo $class; ?>">
                <% if(p.player.player_stat[<?php echo $val->poolScoringField->id; ?>]==null){%>
                0
                <% } else{ %>
                <%= hpApp.twoDecimal(p.player.player_stat[<?php echo $val->poolScoringField->id; ?>]) %>
                <% }%>
            </td>
            <?php
        }

        $both_stats_list_td = [];
        foreach ($pool_details->poolScoreSettings as $key => $val) {
            if ($val->poolScoringField->type == config('pool.pool_scoring_field.inverse_type.both')) {
                if (array_search($val->poolScoringField->stat, $both_stats_list_td) === false) {
                    $both_stats_list_td[] = $val->poolScoringField->stat;
                    showPlayerStat('data-both', $val);
                }
            } else {
                showPlayerStat('data-player-goalie', $val);
            }
        }
        ?>
    </tr>
    <% }) %>
</script>
