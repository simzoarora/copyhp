<div id="draft-results-outer">
    <div id="draft-results-list-all"></div>
</div>
<script id="draft-results-list-template" type="text/html">
    <table class="table light-grey-table head-arrow flip-scroll-table">
        <thead>
            <tr>
                <th>{{trans('livedraft.draft_results.round')}}<span></span></th>
                <th>{{trans('livedraft.draft_results.pick')}}<span></span></th>
                <th>{{trans('livedraft.draft_results.overall')}}<span></span></th>
                <th>{{trans('livedraft.draft_results.team')}}<span></span></th>
                <th>{{trans('livedraft.draft_results.player')}}<span></span></th>
            </tr>  
        </thead>
        <tbody>
            <% _.each(drafts, function(draft){ %>
            <tr>
                <td><%= draft.round %></td>
                <td><%= draft.pick %></td>
                <td><%= draft.overall %></td>
                <td><%= draft.team!=null?draft.team:'' %></td>
                <td><%= draft.player!=null&&draft.player.player_season!=null?draft.player.player_season.player.full_name:'' %></td>
            </tr>
            <% }) %>
        </tbody>
    </table>
</script>