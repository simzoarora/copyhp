<div class="my-queue">
    <h3 class="pool--small-heading"><i class="fa fa-star-o" aria-hidden="true"></i>{{trans('livedraft.queue.my_queue')}}</h3>
    <div class="all-queues">
        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>{{trans('livedraft.queue.rank')}}</th>
                    <th>{{trans('livedraft.queue.player')}}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="player-queue-body">
                <tr>
                    <td colspan="4" class="empty">This is your my queue area. All the player you want to draft will list here.</td>
                </tr>
            </tbody>
        </table> 
    </div>
</div>
<script id="player-queue-list-template" type="text/html">
    <tr data-player_queue_id="<%= playerQueueId %>" data-player_season_id="<%= playerSeasonId %>" class="live-draft-queue-row">
        <td class="player-sort-handle">
            <span>
                <i class="fa fa-bars" aria-hidden="true"></i>
            </span>
        </td>
        <td>
            <%= playerRank %>
        </td>
        <td class="player-name">
            <%= playerName %>
        </td>
        <td class="player-queue-remove-list">
            <span>
                <i class="fa fa-times " aria-hidden="true"></i>
            </span>
        </td>
    </tr>
</script>
<script id="player-queue-db-list-template" type="text/html">
    <tr data-player_queue_id="<%= player.id %>" data-player_season_id="<%= player.player_season_id %>" class="live-draft-queue-row">
        <td class="player-sort-handle">
            <span>
                <i class="fa fa-bars" aria-hidden="true"></i>
            </span>
        </td>
        <td>
            <%= rank %>
        </td>
        <td class="player-name">
            <%= player.player_season.player.full_name %> <span><%= player.player_season.team.short_name %> - <%= player.player_season.player_position.short_name %></span>
        </td>
        <td class="player-queue-remove-list">
            <span>
                <i class="fa fa-times" aria-hidden="true"></i>
            </span>
        </td>
    </tr>
</script>