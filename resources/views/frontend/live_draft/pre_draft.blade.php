@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/pre_draft.css')) !!} 
@stop
@section('content')  
<div class="container-fluid">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <?php $pool_id = $pool_details->id; ?>
    <div class="container">
        @include('frontend.includes.pool_nav')
        <div class="live-draft-container">
            <div class="bg-trans" style="display: none">
            </div>
            @include('frontend.includes.pool_name_header')
            <div class="clearfix"></div>
            <div class="center-block">
                <div class="clearfix"></div>
                <div class="center-header">
                    <div class="queue-details">
                        <a class="btn btn-yellow" id="player-add-queue" data-player_season_id="<%= playerSeasonId %>">Add to Queue</a>
                        <a class="btn btn-yellow" id="player-remove-queue" data-player_queue_id="">Remove From Queue</a>
                        <h3><%= playerDetails %></h3>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="queue-overview">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="players">
                            @include('frontend.live_draft.livedraft_players')  
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-block">
                <div class="clearfix"></div>
                @include('frontend.live_draft.livedraft_my_queue')  
            </div>
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
<script>
    var getPlayerStatsUrl = '{{ route('frontend.pool.getDataForPlayerStats', $pool_details->id) }}',
            currentSeasonId = '{{ $current_season->id }}',
            currentPoolId = '{{ $pool_details->id }}',
            currentTeamId = '{{ $pool_details->poolTeam->id }}',
            liveDraftSetting = <?php echo json_encode($pool_details->liveDraftSetting); ?>,
            newYorkTimezone = '<?php echo config('app.timezone'); ?>',
            allSeasons = <?php echo json_encode($seasons); ?>,
            playerStatsMessages = <?php echo json_encode(trans('pool.player_stats')); ?>,
            addPlayerQueueUrl = '{{ route('frontend.live_draft.queue.store') }}',
            getPlayerQueueUrl = '{{ route('frontend.live_draft.queue.index') }}',
            getLiveDraftMessagesRoute = '{{ route('frontend.live_draft.getChatMessages') }}',
            removePlayerQueueUrl = '{{ route('frontend.live_draft.queue.destroy', 'player_queue_id') }}',
            playerQueueSortOrderUrl = '{{ route('frontend.live_draft.queue.changeSortOrder') }}',
            maxAjaxCount = 2,
            currentAjaxCount = 0;
</script>
{!! Html::script(elixir('js/pre_draft.js')) !!}
@stop