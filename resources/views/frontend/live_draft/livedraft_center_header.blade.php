<div id="player-full-desc-all">
    <p>This is the player action area. Please select a player from the list below to view more information about the player and preform actions such as drafting and adding the player to your queue.</p>
</div>
<script id="player-full-desc-template" type="text/html">
    <div class="center-header">
        <div class="queue-details">
            <a class="btn btn-yellow" id="player-add-queue" data-player_season_id="<%= playerSeasonId %>">{{trans('livedraft.queue.add')}}</a>
            <a class="btn btn-yellow" id="player-remove-queue" data-player_queue_id="">Remove From My Queue</a>
            <a class="btn btn-backgreen" id="player-final-draft" data-player_season_id="<%= playerSeasonId %>">{{trans('livedraft.queue.draft')}}</a>
            <h4 id="player-drafted">Drafted</h4>
            <h3 data-player-rank="<%= playerRank %>"><%= playerDetails %></h3>
        </div>
        <table class="table light-grey-table head-arrow">
            <thead>
                <tr>
                    <th>Season</th><!--make it trans-->
                    <th>GP</th><!--make it trans-->
                    <?php
                    $both_stats_list_heading = [];
                    foreach ($pool_details->poolScoreSettings as $key => $val) {
                        if ($val->poolScoringField->type == config('pool.pool_scoring_field.inverse_type.both')) {
                            if (array_search($val->poolScoringField->stat, $both_stats_list_heading) === false) {
                                $both_stats_list_heading[] = $val->poolScoringField->stat;
                                echo('<th class="data-both">' . $val->poolScoringField->stat . '</th>');
                            }
                        } else {
                            echo('<th class="data-player-goalie">' . $val->poolScoringField->stat . ' </th>');
                        }
                    }
                    ?>
                </tr>
            </thead>
            <tbody id="player-full-desc-body"></tbody>
        </table>
    </div>
</script>

<script id="player-season-stat-template" type="text/html">
    <tr>
        <td><%= seasonName %></td>
        <td>
            <!--is this correct GP-->
            <% if(seasonStats.player.player_stat.games_played==null){ %>
            0
            <% } else{ %>
            <%= hpApp.twoDecimal(seasonStats.player.player_stat.games_played) %>
            <% }%>
        </td>
        <?php

        function showPlayerStat2($class, $val) {
            ?>
            <td class="<?php echo $class; ?>">
                <% if(seasonStats.player.player_stat[<?php echo $val->poolScoringField->id; ?>]==null){%>
                0
                <% } else{ %>
                <%= hpApp.twoDecimal(seasonStats.player.player_stat[<?php echo $val->poolScoringField->id; ?>]) %>
                <% }%>
            </td>
            <?php
        }

        $both_stats_list_td = [];
        foreach ($pool_details->poolScoreSettings as $key => $val) {
            if ($val->poolScoringField->type == config('pool.pool_scoring_field.inverse_type.both')) {
                if (array_search($val->poolScoringField->stat, $both_stats_list_td) === false) {
                    $both_stats_list_td[] = $val->poolScoringField->stat;
                    showPlayerStat2('data-both', $val);
                }
            } else {
                showPlayerStat2('data-player-goalie', $val);
            }
        }
        ?>
    </tr>
</script>