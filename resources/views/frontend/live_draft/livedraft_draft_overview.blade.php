<div class="draft-overview">
    <h3 class="draft-heading">{{trans('livedraft.round.overview')}}</h3>
    <div class="all-rounds" id="rounds-list-all"></div>
</div>

<script id="rounds-list-template" type="text/html">
    <% var currentRound = null;
    _.each(drafts, function(draft){ 
    if(currentRound==null || currentRound != draft.round){
    currentRound = draft.round;
    %>
    <div class="clearfix"></div>
    <h3 class="round-heading">
        Round <%= currentRound %>
    </h3>
    <div class="draft">
        <div class="draft-number"> 
            <!--<i class="fa fa-dot-circle-o green"></i> removed by client -->
            <span><%= draft.pick %></span>
        </div>
        <div class="draft-details">
            <p><%= draft.team!=null?draft.team:''  %></p>
            <p>   
                <span><%= draft.player!=null&&draft.player.player_season!=null?draft.player.player_season.player_position.short_name+' -':'' %></span>
                <span><%= draft.player!=null&&draft.player.player_season!=null?draft.player.player_season.player.full_name+' -':'' %></span>
                <span><%= draft.player!=null&&draft.player.player_season!=null?draft.player.player_season.team.short_name:'' %></span>
            </p>
        </div>
    </div>
    <% }else{ %>
    <div class="draft">
        <div class="draft-number">
            <i class="fa fa-dot-circle-o green"></i>
            <span><%= draft.pick %></span>
        </div>
        <div class="draft-details">
            <p><%= draft.team!=null?draft.team:''  %></p>
            <p>   
                <span><%= draft.player!=null&&draft.player.player_season!=null?draft.player.player_season.player_position.short_name+' -':'' %></span>
                <span><%= draft.player!=null&&draft.player.player_season!=null?draft.player.player_season.player.full_name+' -':'' %></span>
                <span><%= draft.player!=null&&draft.player.player_season!=null?draft.player.player_season.team.short_name:'' %></span>
            </p>
        </div>
    </div>
    <% } %>
    <% }) %>
</script>