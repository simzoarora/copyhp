@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/live_draft.css')) !!} 
@stop
@section('content') 
<?php

//use \Firebase\JWT\JWT;
//
//$encryptedPoolId = JWT::encode($pool_details->id, env('JWT_KEY'));
?>
<div class="live-draft-container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="left-block">
        <div class="logo-img"> 
            {{ HTML::image('img/logo.png', 'Hockeypool Logo', array('class' => 'img-responsive')) }}
        </div>
        <div class="livedraft-heading visible-sm visible-xs">
            <h3>{{trans('livedraft.heading')}}</h3>
            <h4><?php echo $pool_details->name; ?></h4>
        </div>
        <div class="clearfix"></div>
        @include('frontend.live_draft.livedraft_round_header')  
        @include('frontend.live_draft.livedraft_draft_overview')  
    </div>
    <div class="center-block">
        <div class="livedraft-heading hidden-sm hidden-xs">
            <h3>{{trans('livedraft.heading')}}</h3>
            <h4><?php echo $pool_details->name; ?></h4>
            <div class="anik" style="display:inline-block;float:right">
                <label><span class="main-label">Automatic Selection </span><span class="horizontal-line"></span></label>
                {{ Form::checkbox('automatic_selection', (($pool_details->poolTeam->liveDraftTeamSetting != NULL) ? $pool_details->poolTeam->liveDraftTeamSetting->automatic_selection : '0'), true, ['class' => 'switch-input checkbox-field ', 'checked' => (($pool_details->poolTeam->liveDraftTeamSetting != NULL && $pool_details->poolTeam->liveDraftTeamSetting->automatic_selection == 1) ? 'checked' : 'gggjjj')]) }}
                <label class="switch switch-blue" style="background-color: #0c283e;margin-top: -5px;">
                    <input type="checkbox" class="switch-input" value="1" <?php echo (($pool_details->poolTeam->liveDraftTeamSetting != NULL && $pool_details->poolTeam->liveDraftTeamSetting->automatic_selection == 1) ? 'checked' : ''); ?>>
                    <span class="switch-label" data-on="Yes" data-off="No"></span>
                    <span class="switch-handle"></span>
                </label> 
            </div>
        </div>
        @include('frontend.live_draft.livedraft_center_header')  
        @include('frontend.live_draft.livedraft_queue_overview')  
    </div>
    <div class="right-block">
        @include('frontend.live_draft.livedraft_my_queue')  
        @include('frontend.live_draft.livedraft_my_team')  
        @include('frontend.live_draft.livedraft_trash_talk')  
    </div>
</div>
@stop 
@section('after-scripts-end')
<script>
    var getPlayerStatsUrl = '{{ route('frontend.pool.getDataForPlayerStats', $pool_details->id) }}',
            currentSeasonId = '{{ $current_season->id }}',
            currentPoolId = '{{ $pool_details->id }}',
            currentTeamId = '{{ $pool_details->poolTeam->id }}',
            liveDraftSetting = <?php echo json_encode($pool_details->liveDraftSetting); ?>,
            newYorkTimezone = '<?php echo config('app.timezone'); ?>',
            allSeasons = <?php echo json_encode($seasons); ?>,
            playerStatsMessages = <?php echo json_encode(trans('pool.player_stats')); ?>,
            addPlayerQueueUrl = '{{ route('frontend.live_draft.queue.store') }}',
            getPlayerQueueUrl = '{{ route('frontend.live_draft.queue.index') }}',
            getLiveDraftMessagesRoute = '{{ route('frontend.live_draft.getChatMessages') }}',
            removePlayerQueueUrl = '{{ route('frontend.live_draft.queue.destroy', 'player_queue_id') }}',
            playerQueueSortOrderUrl = '{{ route('frontend.live_draft.queue.changeSortOrder') }}',
            getTeamsUrl = '{{ route('frontend.live_draft.getTeamsTabData', $pool_details->id) }}',
            getDraftResultUrl = '{{ route('frontend.live_draft.getDraftResultsTabData', $pool_details->id) }}',
            getMyTeamUrl = '{{ route('frontend.live_draft.getMyTeam', $pool_details->id) }}',
            getDraftTiming = '{{ route('frontend.live_draft.getDraftTiming', $pool_details->id) }}',
            draftplayerUrl = '{{ route('frontend.live_draft.draftPlayer', $pool_details->id) }}',
            poolOverviewUrl = '{{ route('frontend.pool.dashboard', $pool_details->id) }}',
            automaticSelectionUrl = '{{ route('frontend.live_draft.changeAutomaticSelection', $pool_details->id) }}',
            encrypted_pool_id = '<?php echo $encryptedPoolId; ?>',
            socketChannel = '<?php echo $socket_channel; ?>',
            socketChannelAutoselect = '<?php echo $socket_channel_autoselect; ?>',
            draftCountdownMins = 1,
            draftCountdownSecs = 30,
            maxAjaxCount = 4,
            currentAjaxCount = 0,
            currentPickText = '{{trans('livedraft.round.current_pick')}}';
</script>
{!! Html::script(elixir('js/live_draft.js')) !!} 
@stop