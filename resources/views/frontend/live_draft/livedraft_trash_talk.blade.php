<div class="trash-talk">
    <h3 class="pool--small-heading">
        <i class="fa fa-weixin" aria-hidden="true"></i>{{trans('livedraft.talk.trash_talk')}}
    </h3>
    <div class="all-messages" id="trash-talk-messages">
        <div id="all-messages-container">
        </div>
    </div>
    {{ Form::open(array('route' => 'frontend.live_draft.saveChatMessages','id'=>'liveDraftMessage')) }}
    <input type="hidden" name="pool_id" value="<?php echo $pool_details->id; ?>"/>
    {{ Form::text('message_text', null, array('class' => 'grey-input-field', 'placeholder'=>trans('livedraft.talk.input_placeholder'))) }}
    {{ Form::close() }}
</div>