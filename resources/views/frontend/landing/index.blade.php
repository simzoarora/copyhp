@extends('frontend.layouts.landing')

@section('content') 
<div id="wrapper">

    <div class="right">

        <section> 

            <div class="wide">

                <h1 class="logo">
                    {{ Html::image('img/landing/hockeypool_text_stacked.png', 'Subscribe') }}
                </h1>

                <div class="title align-center">

                    <h1>SERIOUS. FANTASY. HOCKEY</h1>
                    <p class="titlecopy">We're launching a brand new fantasy hockey site for the 2016-2017 season and we want you to be part
                        of the beta. Sign up for updates to know when the we are open and for a special beta offer.</p>

                </div>

                <div class="form-box subscribe">		

                    <div id="success">
                        <div class="green align-center">
                            <p>Your message was sent successfully!</p>
                        </div>
                    </div>

                    <div id="error">
                        <div>
                            <p>Something went wrong. Please refresh and try again.</p>
                        </div>
                    </div>
                    <div id="mc_embed_signup">
                        {!! Form::open(array('url'=>'mailchimpsubscribe','method'=>'POST', 'id'=>'mc-embedded-subscribe-form')) !!}
                        <div class="form-row">
                            <input type="email" value="" name="email" class="email text login_input" id="mce-EMAIL" placeholder="email address" required>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_af724b6701a3e4283bc3a32d8_4735159d33" tabindex="-1" value=""></div>
                            <div class="clear">
                                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn">
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                    <div class="clearfix"></div>	
                </div>

                <div class="shadow"></div>

                <h4 class="subcopy">We are a brand new fantasy hockey pool manager that gives you full control of your
                    league.  Our standard draft, rotisserie, head to head and box selection pool formats with full control
                over the scoring systems give you full control.  All hockey pools have live scoring updates, trash talk
                message boards, trades, free agent signings, keeper league support and more.  Manage your team from your
                desktop, tablet, or mobile device.  Looking forward to the 2016-2017 season!</h4>

            </div>

        </section>

    </div>

</div>
@stop

@section('scripts')
<script>
    var api_url = '{{url('')}}';
    $('#mc-embedded-subscribe-form').on('submit', function (e) {
        var $this = $(this);
        $this.find('[type="submit"]').attr('disabled', 'disabled');
        $.ajax({
            url: api_url + '/mailchimpsubscribe',
            type: 'POST',
            data: {email: $this.find('[type="email"]').val(), '_token': $this.find('input[name=_token]').val()},
            dataType: 'JSON',
            success: function (response) {
                $this.find('[type="submit"]').removeAttr('disabled');
                $this.find('[type="email"]').val('');
                swal({
                    title: 'Great!',
                    text: response.message,
                    type: 'success',
                    customClass: 'sweat-alert-confirm'
                });
            },
            error: function (error) {
                $this.find('[type="submit"]').removeAttr('disabled');
                if ($.isEmptyObject(error)) {
                    swal({
                        title: 'Oops!!',
                        text: 'Something went wrong. Please try again.',
                        type: 'error',
                        customClass: 'sweat-alert-confirm'
                    });
                } else {
                    var error_text = '';
                    if (!$.isEmptyObject(error.responseText)) {
                        var responseText = JSON.parse(error.responseText);
                        $.each(responseText, function (i, v) {
                            error_text = error_text + ' ' + v;
                        });
                    } else {
                        error_text = 'Something went wrong. Please try again.'
                    }
                    swal({
                        title: 'Oops!!',
                        text: error_text,
                        type: 'error',
                        customClass: 'sweat-alert-confirm'
                    });
                }
            }
        });
        e.preventDefault();
    });
</script>
@stop
