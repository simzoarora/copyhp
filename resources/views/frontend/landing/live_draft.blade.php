<?php 
use \Firebase\JWT\JWT;
$poolId=259;
$encryptedPoolId=JWT::encode($poolId, env('JWT_KEY')); 
?>
@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/live_draft.css')) !!} 
@stop
@section('content')
    <script>
        var encrypted_pool_id='{{$encryptedPoolId}}';
    </script>
<div class="live-draft-container">
    <div class="live-draft-header">
        {{ HTML::image('https://placeholdit.imgix.net/~text?txtsize=16&txt=LOGO&w=200&h=40', 'livedraft-logo', array('class' => 'main-img')) }}
        <h3 class="pool--main-heading">{{trans('livedraft.heading')}}</h3>
    </div>
    <div class="left-block">
        @include('frontend.livedraft.livedraft_round_header')  
        @include('frontend.livedraft.livedraft_draft_overview')  
    </div>
    <div class="center-block">
        @include('frontend.livedraft.livedraft_center_header')  
        @include('frontend.livedraft.livedraft_queue_overview')  
    </div>
    <div class="right-block">
        @include('frontend.livedraft.livedraft_my_queue')  
        @include('frontend.livedraft.livedraft_my_team')  
        @include('frontend.livedraft.livedraft_trash_talk')  
    </div>

</div>
@stop 
@section('after-scripts-end')
<script>
var getLiveDraftMessagesRoute = '{{route("getLiveDraftMessages")}}';

</script>
{!! Html::script(elixir('js/live_draft.js')) !!} 
@stop