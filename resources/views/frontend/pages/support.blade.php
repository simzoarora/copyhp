@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/support.css')) !!} 
@stop 
@section('content')
<div class="container">
    <div class="row" id="pool--content">
        <div class="support--content">
            <p class="pool--main-heading">{{ Route::getCurrentRoute()->getPath() }}</p>
            <p>{{trans('pages.contact.content.line1')}}<a href="{{ route('frontend.pages.faq') }}">{{trans('pages.contact.content.faqs_link')}}</a>.<br><br>
                {{trans('pages.contact.content.line2')}}</p>
            <div class="submit-form">
                <div id="alert-box"></div>
                {!! Form::open(['route' => 'frontend.pages.supportSubmit', 'id' => 'support-form', 'role' => 'form', 'method' => 'post']) !!}
                {{ Form::hidden('page_type', Route::getCurrentRoute()->getPath()) }}
                <div class="input-fields">
                    <label>{{trans('pages.contact.form_fields.name')}}<sup>*</sup></label>
                    {{ Form::text('name', null, array('class' => 'namefield','placeholder'=>trans('pages.contact.form_fields.name_placeholder').'*','required')) }}
                </div>
                <div class="input-fields">
                    <label>{{trans('pages.contact.form_fields.email')}}<sup>*</sup></label>
                    {{ Form::email('email', null, array('class' => 'emailaddress','placeholder'=>trans('pages.contact.form_fields.email_placeholder').'*','required')) }}
                </div>
                <div class="textarea-field selectboxit-grey">
                    <label>{{trans('pages.contact.form_fields.type')}}<sup>*</sup></label>
                    <?php
                    $contact_type = array(
                        trans('pages.contact.form_fields.type1') => trans('pages.contact.form_fields.type1'),
                        trans('pages.contact.form_fields.type2') => trans('pages.contact.form_fields.type2'),
                        trans('pages.contact.form_fields.type3') => trans('pages.contact.form_fields.type3'),
                    );
                    ?>
                    {{ Form::select('type', $contact_type, null, ['class' => 'select-field']) }}
                </div>
                <div class="textarea-field">
                    <label>{{trans('pages.contact.form_fields.question')}}<sup>*</sup></label>
                    {{ Form::textarea('question', null, ['class' => 'informationfield', 'placeholder'=>trans('pages.contact.form_fields.question_placeholder').'*', 'required']) }}
                </div>
                <div class="clearfix"></div>
                <div class="recaptcha-field">
                    {!! app('captcha')->display(); !!}
                </div>
                <button class="btn-backgreen submit-btn"><span>{{trans('buttons.general.submit')}}</span> <i class="fa fa-spinner fa-pulse"></i></button>
                {{ Form::close() }}
            </div> 
        </div>
        <div class="adblocks">
            @include('frontend.includes.create_pool')
            @include('frontend.includes.adblocks')  
            @include('frontend.includes.hockeypool_news')
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
<script>
var buttons_general = <?php echo json_encode(trans('buttons.general')); ?>;
</script>
{!! Html::script(elixir('js/support.js')) !!}
@stop 