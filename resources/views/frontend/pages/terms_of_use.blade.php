@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/terms_of_use.css')) !!}
@stop  
@section('content')
<div class="container"> 
    <div class="row" id="pool--content">
        <div class="termsofuse--content">
            <p class="pool--main-heading">{{trans('pages.terms_of_use.title')}}</p>
            <p>{{trans('pages.terms_of_use.content')}}</p>
        </div>
        <div class="adblocks">
            @include('frontend.includes.create_pool')
            @include('frontend.includes.adblocks') 
            @include('frontend.includes.hockeypool_news')
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/terms_of_use.js')) !!}
@stop 