@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/faq.css')) !!}
@stop 
@section('content')
<div class="container">
    <div class="row" id="pool--content">
        <div class="faq--content">
            <p class="pool--main-heading">{{trans('pages.faq.title')}}</p>
            <div class="clearfix"></div>
            <div class="panel-group" role="tablist" id="accordion" aria-multiselectable="true">
                <?php for($counter = 1; $counter <= Config::get('constant.total_faqs') ; $counter++){ ?>
                <div class="panel panel-default"> 
                    <div class="panel-heading" role="tab" id="qus"<?php echo $counter; ?>> 
                        <h4 class="panel-title  btn-grey">
                            <a href="#collapse<?php echo $counter; ?>" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapse<?php echo $counter; ?>"> 
                                {{trans('pages.faq.faqs.ques'.$counter)}}
                                <span><i class="fa fa-angle-down"></i></span>
                            </a>

                        </h4>
                    </div> 
                    <div class="panel-collapse collapse" role="tabpanel" id="collapse<?php echo $counter; ?>" aria-labelledby="qus<?php echo $counter; ?>" aria-expanded="false" style="height: 0px;"> 
                        <div class="panel-body">
                            <p>{!! trans("pages.faq.faqs.ans".$counter) !!}</p>
                        </div> 
                    </div> 
                </div> 
                <?php } ?>
            </div>
        </div>
        <div class="adblocks">
            @include('frontend.includes.create_pool')
            @include('frontend.includes.adblocks') 
            @include('frontend.includes.hockeypool_news')
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/faq.js')) !!}
@stop 