@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/how_it_works.css')) !!}
@stop 
@section('content') 
<div class="container">
    <div class="row" id="pool--content">
        <div class="how-it-works--content">
            <p class="pool--main-heading">{{trans('pages.howItWorks.title')}}</p>
            <p>{{trans('pages.howItWorks.content')}}</p>
            <p class="pool--small-heading">{{trans('pages.howItWorks.beta_info.title')}}</p>
            <p>{{trans('pages.howItWorks.beta_info.content')}}</p>
            <p class="pool--small-heading">{{trans('pages.howItWorks.cost.title')}}</p>
            <p>{{trans('pages.howItWorks.cost.content')}}</p>
            <p class="pool--small-heading">{{trans('pages.howItWorks.creating_a_pool.title')}}</p>
            <p>{{trans('pages.howItWorks.creating_a_pool.content.string1')}}
                <a href="{{ route('auth.register') }}">{{trans('pages.howItWorks.creating_a_pool.content.register')}}</a>
                {{trans('pages.howItWorks.creating_a_pool.content.string2')}}
                <a href="{{ route('pool.create') }}">{{trans('pages.howItWorks.creating_a_pool.content.pool_creator')}}</a>
                {{trans('pages.howItWorks.creating_a_pool.content.string3')}}
                <a href="{{ route('frontend.pages.poolTypes') }}">{{trans('pages.howItWorks.creating_a_pool.content.pool_types')}}</a>
                {{trans('pages.howItWorks.creating_a_pool.content.string4')}}
            </p>

        </div>
        <div class="adblocks">
            @include('frontend.includes.create_pool')
            @include('frontend.includes.adblocks') 
            @include('frontend.includes.hockeypool_news')
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/how_it_works.js')) !!}
@stop 