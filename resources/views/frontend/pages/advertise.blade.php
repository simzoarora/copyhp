@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/advertise.css')) !!}
@stop 
@section('content')
<div class="container">
    <div class="row" id="pool--content">
        <div class="advertise--content">
            <p class="pool--main-heading">{{trans('pages.advertise.title')}}</p>
            <p>{{trans('pages.advertise.content')}}</p>
            <div class="submit-form">
                <div id="alert-box"></div>
                {!! Form::open(['route' => 'frontend.pages.advertiseSubmit', 'id' => 'advertise-form', 'role' => 'form', 'method' => 'post']) !!}
                <div class="input-fields">
                    <label>{{trans('pages.advertise.form_fields.name')}}<sup>*</sup></label>
                    {{ Form::text('name', null, array('class' => 'namefield','placeholder'=>trans('pages.advertise.form_fields.name_placeholder').'*','required')) }}
                </div>
                <div class="input-fields">
                    <label>{{trans('pages.advertise.form_fields.email')}}<sup>*</sup></label>
                    {{ Form::email('email', null, array('class' => 'emailaddress','placeholder'=>trans('pages.advertise.form_fields.email_placeholder').'*','required')) }}
                </div>
                <div class="textarea-field">
                    <label>{{trans('pages.advertise.form_fields.information')}}<sup>*</sup></label>
                    {{ Form::textarea('information', null, ['class' => 'informationfield', 'placeholder'=>trans('pages.advertise.form_fields.information_placeholder').'*', 'required']) }}
                </div>
                <div class="clearfix"></div>
                <div class="recaptcha-field">
                    {!! app('captcha')->display(); !!}
                </div>
                <button class="btn-backgreen submit-btn"><span>{{trans('buttons.general.submit')}}</span> <i class="fa fa-spinner fa-pulse"></i></button>
                {{ Form::close() }}
            </div>
        </div>
        <div class="adblocks">
            @include('frontend.includes.create_pool')
            @include('frontend.includes.adblocks') 
            @include('frontend.includes.hockeypool_news')
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
<script>
var buttons_general = <?php echo json_encode(trans('buttons.general')); ?>;
</script>
{!! Html::script(elixir('js/advertise.js')) !!}
@stop 