@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/pool_types.css')) !!}
@stop 
@section('content') 
<div class="container">
    <div class="row" id="pool--content">  
        <div class="pool-type--content">
            <p class="pool--main-heading">{{ trans('pages.poolTypes.title') }}</p>
            <p>{{trans('pages.poolTypes.content')}}</p>
            <!--pool-type-->
            <div class="pool-type" id="HeadToHead">
                <h3 class="pool--main-heading">{{trans('pages.poolTypes.head_to_head.title')}}</h3>
                <p>{{trans('pages.poolTypes.head_to_head.content')}}</p>
                <div class="pool-type--types">
                    <div class="pool-type--description"> 
                        <h3 class="pool--small-heading">{{trans('pages.poolTypes.head_to_head.categories.title')}}</h3>
                        <p>{{trans('pages.poolTypes.head_to_head.categories.content')}}</p>
                    </div>
                    <div class="pool-type--create"> 
                      <a href="{{ route('pool.create', 'pool_type='.config("pool.inverse_type.h2h").'&pool_format='.config("pool.inverse_format.h2h_cat")) }}" class="btn-backgreen">{{trans('buttons.general.crud.create')}}
                           <span class="plus-sign"></span>
                       </a>
                    </div>
                </div>
                <div class="pool-type--types">
                    <div class="pool-type--description"> 
                        <h3 class="pool--small-heading">{{trans('pages.poolTypes.head_to_head.weekly_win.title')}}</h3>
                        <p>{{trans('pages.poolTypes.head_to_head.weekly_win.content')}}</p>
                    </div>
                    <div class="pool-type--create"> 
                       <a href="{{ route('pool.create', 'pool_type='.config("pool.inverse_type.h2h").'&pool_format='.config("pool.inverse_format.h2h_weekly")) }}" class="btn-backgreen">{{trans('buttons.general.crud.create')}}
                           <span class="plus-sign"></span>
                       </a>
                    </div>
                </div>
                <div class="pool-type--types">
                    <div class="pool-type--description"> 
                        <h3 class="pool--small-heading">{{trans('pages.poolTypes.head_to_head.weekly_win_daily_rosters.title')}}</h3>
                        <p>{{trans('pages.poolTypes.head_to_head.weekly_win_daily_rosters.content')}}</p>
                    </div>
                    <div class="pool-type--create"> 
                         <a href="{{ route('pool.create', 'pool_type='.config("pool.inverse_type.h2h").'&pool_format='.config("pool.inverse_format.h2h_weekly_daily")) }}" class="btn-backgreen">{{trans('buttons.general.crud.create')}}
                           <span class="plus-sign"></span>
                       </a>
                    </div>
                </div>
            </div>
            <!--pool-type-->
            <div class="pool-type" id="Standard">
                <h3 class="pool--main-heading">{{trans('pages.poolTypes.standard.title')}}</h3>
                <p>{{trans('pages.poolTypes.standard.content')}}</p>
                <div class="pool-type--types">
                    <div class="pool-type--description"> 
                        <h3 class="pool--small-heading">{{trans('pages.poolTypes.standard.standard.title')}}</h3>
                        <p>{{trans('pages.poolTypes.standard.standard.content')}}</p>
                    </div>
                    <div class="pool-type--create"> 
                        <a href="{{ route('pool.create', 'pool_type='.config("pool.inverse_type.standard").'&pool_format='.config("pool.inverse_format.standard_standard")) }}" class="btn-backgreen">{{trans('buttons.general.crud.create')}}
                           <span class="plus-sign"></span>
                       </a>
                    </div>
                </div>
                <div class="pool-type--types">
                    <div class="pool-type--description"> 
                        <h3 class="pool--small-heading">{{trans('pages.poolTypes.standard.rotisserie.title')}}</h3>
                        <p>{{trans('pages.poolTypes.standard.rotisserie.content')}}</p>
                    </div>
                    <div class="pool-type--create"> 
                        <a href="{{ route('pool.create', 'pool_type='.config("pool.inverse_type.standard").'&pool_format='.config("pool.inverse_format.standard_rotisserie")) }}" class="btn-backgreen">{{trans('buttons.general.crud.create')}}
                           <span class="plus-sign"></span>
                       </a>
                    </div>
                </div>
            </div>
            <!--pool-type-->
            <div class="pool-type" id="Box">
                <h3 class="pool--main-heading">{{trans('pages.poolTypes.box.title')}}</h3>
                <p>{{trans('pages.poolTypes.box.content')}}</p>
                <div class="pool-type--types">
                    <div class="pool-type--description"> 
                        <h3 class="pool--small-heading">{{trans('pages.poolTypes.box.box.title')}}</h3>
                        <p>{{trans('pages.poolTypes.box.box.content')}}</p>
                    </div>
                    <div class="pool-type--create"> 
                        <a href="{{ route('pool.create', 'pool_type='.config("pool.inverse_type.box")) }}" class="btn-backgreen">{{trans('buttons.general.crud.create')}}
                            <span class="plus-sign"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--pool-content-ends-->
        <div class="adblocks">
            @include('frontend.includes.create_pool')
            @include('frontend.includes.adblocks')  
            @include('frontend.includes.hockeypool_news')
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/pool_types.js')) !!}
@stop 