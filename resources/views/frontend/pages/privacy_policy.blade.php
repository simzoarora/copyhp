@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/privacy_policy.css')) !!}
@stop 
@section('content')
<div class="container">
    <div class="row" id="pool--content">
        <div class="privacy-policy--content">
            <p class="pool--main-heading">{{trans('pages.privacy_policy.title')}}</p>
            <!--privacy-policy-content STARTS-->
            {!! trans('pages.privacy_policy.content') !!}
            <!--privacy-policy-content ENDS-->
        </div>
        <div class="adblocks">
            @include('frontend.includes.create_pool')
            @include('frontend.includes.adblocks') 
            @include('frontend.includes.hockeypool_news')
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/privacy_policy.js')) !!}
@stop 