<div class="pool-name-header">
    <script id="pool-left-header-template" type="text/html">
        <div class="pool-name">
            <div class="image">
                <img src="<%= siteLink %>{{ config('pool.paths.logo') }}<%= response.logo %>" alt="<%= response.name %>" />
            </div>
            <div class="details">
                <select name="team_list" class="select-field team-select">
                    <% _.each(poolTeams, function(value){ %>
                    <option value="<%= value.id %>" ><%= value.value %></option>
                    <%  }); %> 
                </select>

                <p class="p1">{{ Auth::user()->email }}</p>

                <% if(!$.isEmptyObject(currentTeam.pool_matchup_result)){ %>
                <p class="p2"><%= currentTeam.pool_matchup_result.total_win %>-<%= currentTeam.pool_matchup_result.total_loss %>-<%= currentTeam.pool_matchup_result.total_tie %></p>
                <% } %>

                <% if(poolToShow == 11 || poolToShow == 12 || poolToShow == 13){ 
                if(response.pool_setting.poolsetting.min_goalie_appearance > 0){ %>
                <% if(response.goalie_games < response.pool_setting.poolsetting.min_goalie_appearance){ %>
                <span class='red-warning'></span>
                <% }else{ %>
                <span class='right-tick'></span>
                <% } %>
                <p class="p3">(<%= response.goalie_games %>/<%= response.pool_setting.poolsetting.min_goalie_appearance %>) Goalie Games</p>
                <% } } %>
            </div>
        </div>
    </script>
    <div id="pool-left-header-outer"></div>

    <script id="pool-right-header-template" type="text/html">
        <div class="pools--h2h">
            <div class="week">
                <% if(!$.isEmptyObject(poolMatchup)){ %>
                <%= poolMatchup.season_competition_type_week.name %> - 
                <%= moment(poolMatchup.season_competition_type_week.start_date).format('MMM D') %> to
                <%= moment(poolMatchup.season_competition_type_week.end_date).format('MMM D') %>
                <% } %>
            </div>
            <div class="upper">
                <div class="right">
                    <div class="img-div">
                        <img src="<%= siteLink %>{{ config('pool.paths.team_logo') }}<%= poolMatchup.pool_team1.logo %>" alt="<%= poolMatchup.pool_team1.name %>" class="img-responsive"/>
                    </div>
                    <div class="name-div">
                        <p><%= poolMatchup.pool_team1.name %>
                            <span>
                                <% if(!$.isEmptyObject(poolMatchup.pool_team1.pool_matchup_result)){ %>
                                <%= poolMatchup.pool_team1.pool_matchup_result.total_win %>-<%= poolMatchup.pool_team1.pool_matchup_result.total_loss %>-<%= poolMatchup.pool_team1.pool_matchup_result.total_tie %>
                                <% }else{ %>
                                0-0-0
                                <% } %>
                            </span></p>
                    </div>
                    <div class="goal-div">
                        <p>
                            <% if($.isArray(poolMatchup.pool_matchup_results)){ 
                            if(!$.isEmptyObject(poolMatchup.pool_matchup_results)){ %>
                            <% if(poolMatchup.pool_team1.id == poolMatchup.pool_matchup_results[0].pool_team_id){ %>
                            <%= poolMatchup.pool_matchup_results[0].win %>
                            <% }else{ %>
                            <%= poolMatchup.pool_matchup_results[1].win %>
                            <% } %>
                            <% }else{ %>
                            0
                            <% } } %>
                        </p>
                    </div>
                </div>
                <div class="vs">
                    <p>{{trans('custom.pool_name_header.versus')}}</p>
                </div>
                <div class="left">
                    <div class="img-div">
                        <img src="<%= siteLink %>{{ config('pool.paths.team_logo') }}<%= poolMatchup.pool_team2.logo %>" alt="<%= poolMatchup.pool_team2.name %>" class="img-responsive"/>
                    </div>
                    <div class="name-div">
                        <p><%= poolMatchup.pool_team2.name %>
                            <span>
                                <% if(!$.isEmptyObject(poolMatchup.pool_team2.pool_matchup_result)){ %>
                                <%= poolMatchup.pool_team2.pool_matchup_result.total_win %>-<%= poolMatchup.pool_team2.pool_matchup_result.total_loss %>-<%= poolMatchup.pool_team2.pool_matchup_result.total_tie %>
                                <% }else{ %>
                                0-0-0
                                <% } %>
                            </span></p>
                    </div>
                    <div class="goal-div">
                        <p>
                            <% if($.isArray(poolMatchup.pool_matchup_results)){ 
                            if(!$.isEmptyObject(poolMatchup.pool_matchup_results)){ %>
                            <% if(poolMatchup.pool_team2.id == poolMatchup.pool_matchup_results[0].pool_team_id){ %>
                            <%= poolMatchup.pool_matchup_results[0].win %>
                            <% }else{ %>
                            <%= poolMatchup.pool_matchup_results[1].win %>
                            <% } %>
                            <% }else{ %>
                            0
                            <% } } %>
                        </p> 
                    </div>
                </div>
                <% if(typeof poolScoreSettings != 'undefined'){ %>
                <div class='bottom'>
                    <% _.each(poolScoreSettings, function(value){ %>
                    <div class='width10' id='score-setting-<%= poolMatchup.id %>-<%= value.id %>'>
                        <p><%= value.pool_scoring_field.stat %></p>
                    </div>
                    <%  }); %> 
                </div>
                <% } %>
            </div>
            <% if(typeof poolScoreSettings != 'undefined'){ %>
            <div class='text-right leading-losing'>
                <span class='green circle'></span>
                <span>{{ trans('custom.leading') }}</span>
                <span class='gray circle'></span>
                <span>{{ trans('custom.tied') }}</span>
                <span class='maroon circle'></span>
                <span>{{ trans('custom.losing') }}</span>
            </div>
            <% } %>
        </div>
    </script>
    <script id="pool-right-header-box-template" type="text/html">
        <div class="pools--box">
            <div class="week">
                {{trans('custom.pool_name_header.pool_stats')}}
            </div>
            <div class="pools--box-inner">
                <div class="ranks">
                    <%= hpApp.getOrdinal(poolTeamFirst.rank) %>
                </div>
                <% if(!$.isEmptyObject(poolTeamFirst.standard_score)){ %>
                <div class="points">
                    <span>
                        <%= parseInt(poolTeamFirst.standard_score.total_score) %>
                    </span>
                    <span>{{trans('custom.pool_name_header.pool_points')}}</span>
                </div>
                <% } %>
                <div class="stats">
                    <table class="table light-grey-table"  id="no-more-tables">
                        <thead>
                            <tr>
                                <% _.each(poolScoreSettings, function(value){ %>
                                <th><%= value.pool_scoring_field.stat %></th>
                                <%  }); %> 
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <% _.each(poolScoreSettings, function(value){ %>
                                <td data-title="<%= value.pool_scoring_field.stat %>" id="score-setting-<%= poolTeamFirst.id %>-<%= value.pool_scoring_field_id %>-<%= value.type %>">0</td>
                                <%  }); %> 
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</script>
<div id="pool-right-header-outer"></div>

<?php
if (!isset($add_drop_page)) {
    $add_drop_page = false;
}
if ($add_drop_page == false) {
    ?>
    <div class="pool-action">
        <div id="pool-action-outer"></div>
        <script id="pool-action-template" type="text/html">
            <% if(poolToShow != 2){ %>
            <ul class="list-inline">
                <li>
                    <a class="btn btn-backgreen" href="{{ route('frontend.pool.playerStats', $pool_id) }}">
                        <span class='add'></span>
                        {{trans('custom.pool_name_header.add')}}
                    </a>
                </li> 
                <li>
                    <a class="btn btn-green-line" href="{{ route('frontend.pool.trade', $pool_id) }}">
                        <span class='trade'></span>
                        {{trans('custom.pool_name_header.trade')}}
                    </a>
                </li>
                <li>
                    <a class="btn btn-orange-line" href="{{ route('frontend.pool.dropSetup', $pool_id) }}">
                        <span class='drop'></span>
                        {{trans('custom.pool_name_header.drop')}}
                    </a>
                </li>
            </ul>
            <% } %>
        </script>
    </div>
<?php } ?>
</div>
<div id="pool-name-header-outer"></div>

<!--payment activation warning-->
<script id="payment-warning-template" type="text/html">
    <% if(poolComplete == 1){ %>
    <% if(typeof poolToShow != 'undefined' && typeof poolTeamPlayers != 'undefined'){
    if(poolToShow == 2 && $.isEmptyObject(poolTeamPlayers)){ %>
    <div class="alert alert-danger">
        <p> 
            {{trans('custom.pool_name_header.payment_alert1')}}<%= activeTill %>{{trans('custom.pool_name_header.payment_alert2')}}
        </p>
        <a href="<%= paymentLink %>" class="btn btn-orange-line">{{trans('custom.pool_name_header.activate')}}</a>
    </div> 
    <% } 
    } 
    } %>
</script>
<div id="payment-warning-outer"></div>
<!--END payment activation warning-->

<!--Draft team alert-->
<a href="{{ route('frontend.pool.boxDraft', 'draftLink') }}" id="pool-boxdraft-link" class="hide"></a>
<script id="draft-team-alert-template" type="text/html">
    <div class="alert alert-danger">
        <p>Please make your box selections for this pool.</p>
        <a href="<%= boxDraftLink %>" class="btn btn-orange-line">Draft Here</a>
    </div> 
</script>
<div id="draft-team-alert-outer"></div>
<!--END Draft team alert-->
<?php
//Creating poolHeaderUrl
$currentRoute = \Request::route()->getName();
if (strpos($currentRoute, 'myTeamWithTeam') == false) {
    $poolHeaderUrl = route('frontend.pool.getPoolHeader', $pool_id);
} else {
    $poolHeaderUrl = route('frontend.pool.getPoolHeaderWithTeam', [$pool_id, $pool_team_id]);
}
?>
<script>
    var poolHeaderUrl = '<?php echo $poolHeaderUrl; ?>',
            myTeamWithTeamUrl = '{{route('frontend.pool.myTeamWithTeam', [$pool_id, 0])}}',
            poolPaymentUrl = '{{route('frontend.pool.payment', [$pool_id, config('pool.inverse_payment_items.premium')])}}',
            poolTypes = <?php echo json_encode(config('pool.inverse_type')); ?>,
            poolFormat = <?php echo json_encode(config('pool.inverse_format')); ?>,
            dashboardPage = <?php
if (isset($dashboardPage)) {
    echo $dashboardPage;
} else {
    echo 'false';
}
?>;
</script>