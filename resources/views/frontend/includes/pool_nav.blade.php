<div class="pool-nav">
    <div class="select-box">
        {{ Form::select('pool_list', $user_pools, $pool_id, ['class' => 'select-field pool-list-select']) }}
    </div>
    <div class="mobile-list" style="display: none;">
        <button type="button" id="open-navbar2">
            {{trans('custom.pool_nav.pool_menu')}} 
            <span class="btn-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="crossbtn" style="display: none;"></span>
            </span>
            <span class="box-shadow"> 
            </span>
        </button>
        <ul class="main-ul" style='display: none'>
            <li>
                <a>{{trans('custom.pool_nav.pool')}}<span class="arrowdown"></span></a>
                <ul class="sub-ul" style='display: none'>
                    <li>
                        <a href="{{ route('frontend.pool.dashboard', $pool_id) }}">{{trans('custom.pool_nav.overview')}}</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.pool.transaction', $pool_id) }}">{{trans('custom.pool_nav.transaction')}}</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.pool.inActionTonight', $pool_id) }}">{{trans('custom.pool_nav.in_action')}}</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.pool.liveStats', $pool_id) }}">{{trans('custom.pool_nav.live_stats')}}</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.pool.draftResult', $pool_id) }}">{{trans('custom.pool_nav.draft_results')}}</a>
                    </li>
                    <li>
                        <a href="{{ route('pool.show', $pool_id) }}">Pool Info</a>
                    </li>  
                </ul>
            </li>
            <li><a href="{{ ($pool_details->pool_type == config('pool.inverse_type.h2h')) ? route('frontend.pool.matchups', $pool_id) : route('frontend.pool.standings', $pool_id) }}">{{trans('custom.pool_nav.scores')}}</a></li>
            <li><a>{{trans('custom.pool_nav.teams')}}<span class="arrowdown"></span></a>
                <ul class="sub-ul" style='display: none'>
                    <li>
                        <a href="{{ route('frontend.pool.myTeam', $pool_id) }}">{{trans('custom.pool_nav.roster')}}</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.pool.trade', $pool_id) }}">{{trans('custom.pool_nav.trade')}}</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.pool.message_board.index', $pool_id) }}">{{ trans('navs.frontend.home_nav.messages') }}</a> 
                    </li>
                    <li>
                        <a href="{{ route('frontend.pool.team.edit', $pool_id) }}">Edit Team</a> 
                    </li>
                </ul> 
            </li>
            <li>
                <a>{{trans('custom.pool_nav.players')}}<span class="arrowdown"></span></a>
                <ul class="sub-ul" style='display: none'>
                    <li>
                        <a href="{{ route('frontend.pool.playerStats', $pool_id) }}">{{trans('custom.pool_nav.player_list')}}</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.pool.leagueLeader', $pool_id) }}">{{trans('custom.pool_nav.league_leaders')}}</a>
                    </li>
                </ul> 
            </li>
        </ul>
    </div>
    <ul class="list-inline" id="pool_nav">
        <li class="">
            <a>{{trans('custom.pool_nav.pool')}}</a>
            <span class="bottom-shadow"></span>
            <!--ul-sub-links-->
            <ul class="sub-links"> 
                <li>
                    <a href="{{ route('frontend.pool.dashboard', $pool_id) }}">{{trans('custom.pool_nav.overview')}}<span></span></a>
                </li>
                <li>
                    <a href="{{ route('frontend.pool.transaction', $pool_id) }}">{{trans('custom.pool_nav.transaction')}}<span></span></a>
                </li>
                <li>
                    <a href="{{ route('frontend.pool.inActionTonight', $pool_id) }}">{{trans('custom.pool_nav.in_action')}}<span></span></a>
                </li>
                <li>
                    <a href="{{ route('frontend.pool.liveStats', $pool_id) }}">{{trans('custom.pool_nav.live_stats')}}<span></span></a>
                </li>
                <li>
                    <a href="{{ route('frontend.pool.draftResult', $pool_id) }}">{{trans('custom.pool_nav.draft_results')}}<span></span></a>
                </li>
                <li>
                    <a href="{{ route('pool.show', $pool_id) }}">Pool Info<span></span></a>
                </li>
            </ul>
            <!--ul-sub-links-ends-->
        </li>
        <li>
            <a href="{{ ($pool_details->pool_type == config('pool.inverse_type.h2h')) ? route('frontend.pool.matchups', $pool_id) : route('frontend.pool.standings', $pool_id) }}">{{trans('custom.pool_nav.scores')}}</a>
            <span class="bottom-shadow"></span>
        </li>
        <li> 
            <a>{{trans('custom.pool_nav.teams')}}</a>
            <span class="bottom-shadow"></span>
            <!--ul-sub-links-->
            <ul class="sub-links"> 
                <li>
                    <a href="{{ route('frontend.pool.myTeam', $pool_id) }}">{{trans('custom.pool_nav.roster')}}<span></span></a>
                </li>
                <li>
                    <a href="{{ route('frontend.pool.trade', $pool_id) }}">{{trans('custom.pool_nav.trade')}}<span></span></a>
                </li>
                <li>
                    <a href="{{ route('frontend.pool.message_board.index', $pool_id) }}">{{ trans('navs.frontend.home_nav.messages') }}<span></span></a> 
                </li>
                <li>
                    <a href="{{ route('frontend.pool.team.edit', $pool_id) }}">Edit Team</a> 
                </li>
            </ul>
            <!--ul-sub-links-ends-->
        </li>
        <li>
            <a>{{trans('custom.pool_nav.players')}}</a>
            <span class="bottom-shadow"></span>
            <!--ul-sub-links-->
            <ul class="sub-links"> 
                <li>
                    <a href="{{ route('frontend.pool.playerStats', $pool_id) }}">{{trans('custom.pool_nav.player_list')}}<span></span></a>
                </li>
                <li>
                    <a href="{{ route('frontend.pool.leagueLeader', $pool_id) }}">{{trans('custom.pool_nav.league_leaders')}}<span></span></a>
                </li>
            </ul>
            <!--ul-sub-links-ends-->
        </li>
    </ul>
</div>
<script>
    var poolDashboardUrl = '{{route('frontend.pool.dashboard', 0)}}';
</script>