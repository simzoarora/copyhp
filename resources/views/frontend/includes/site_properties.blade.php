<!--col-sm-12 darkdiv-->
<div class="site-properties">
    <div class="site-properties--container">
        <div class="site-properties--container--property">
            <div class='property--left'>
                <div class="property--left--graphics mobile"></div>
            </div>
            <div class='property--right'>
                <h3 class="text-uppercase">{{ trans('navs.frontend.upper_footer.live_scores_stats') }}</h3>
                <p>{{ trans('navs.frontend.upper_footer.live_scores_stats_p') }}</p>
            </div>
        </div>
        <div class="site-properties--container--property">
            <div class='property--left'>
                <div class="property--left--graphics stats"></div>
            </div>
            <div class='property--right'>
                <h3 class="text-uppercase">{{ trans('navs.frontend.upper_footer.trash_talks') }}</h3>
                <p>{{ trans('navs.frontend.upper_footer.trash_talks_p') }}</p>
            </div>
        </div>
        <div class="site-properties--container--property">
            <div class='property--left'>
                <div class="property--left--graphics live_scores"></div>
            </div>
            <div class='property--right'>
                <h3 class="text-uppercase">{{ trans('navs.frontend.upper_footer.manage_team') }}</h3>
                <p>{{ trans('navs.frontend.upper_footer.manage_team_p') }}</p>
            </div>
        </div>
    </div>
</div>

