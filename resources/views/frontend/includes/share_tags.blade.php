<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="<?php echo $share_data['title']; ?>">
<meta itemprop="description" content="<?php echo $share_data['short_description']; ?>">
<meta itemprop="image" content="<?php echo URL::asset('img/blog/'.$share_data['image']); ?>">

<!-- Twitter Card data -->
<meta name="twitter:card" content="product">
<meta name="twitter:site" content="@HP">
<meta name="twitter:title" content="<?php echo $share_data['title']; ?>">
<meta name="twitter:description" content="<?php echo $share_data['short_description']; ?>">
<meta name="twitter:creator" content="@HP">
<meta name="twitter:image" content="<?php echo URL::asset('img/blog/'.$share_data['image']); ?>">

<!-- Open Graph data -->
<meta property="og:title" content="<?php echo $share_data['title']; ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo Request::fullUrl(); ?>" />
<meta property="og:image" content="<?php echo URL::asset('img/blog/'.$share_data['image']); ?>" />
<meta property="og:description" content="<?php echo $share_data['short_description']; ?>" /> 
<meta property="og:site_name" content="<?php echo url(''); ?>" />
<meta property="fb:app_id" content="app_idhere"/>