<div class='row'>
    <div class='team-records' id="team-records" data-url="{{route('frontend.pool.getDataForSidebarPoolTeams',$pool_id)}}">
        <table class="table light-grey-table head-arrow" id="main-table">
            <thead>
                <tr>
                    <th>{{trans('custom.team_records.team')}}</th>
                    <th>{{trans('custom.team_records.record')}}</th>
                </tr>
            </thead>
            <tbody id='team-records-all'>

            </tbody>
        </table>
    </div>
</div>
<script id="team-records-template" type="text/html">
    <% _.each(records,function(record){%>
    <tr>
        <td><%= record.name %></td>
        <td>
            <% if(record.standard_score !=null){ %>
            <%= hpApp.twoDecimal(record.standard_score.total_score) %>
            <% } else if(record.pool_matchup_result!=null){ %>
            <%= record.pool_matchup_result.total_win%>-<%= record.pool_matchup_result.total_loss%>-<%= record.pool_matchup_result.total_tie %> 
            <% } else{ %>
            0
            <% } %>
        </td>
    </tr>
    <% })%>
</script>
<script>
    var team_records = <?php echo json_encode(trans('custom.team_records')); ?>;
</script>