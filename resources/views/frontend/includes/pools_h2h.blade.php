<div class="pools--h2h">
    <div class="upper">
        <div class="right">
            <div class="img-div">
                {{ Html::image('img/profile-pic1.jpg', 'Pool Name', array('class'=>'img-responsive')) }}
            </div>
            <div class="name-div">
                <p>Obi Wan Kanobi<span>10-1-2</span></p>
            </div>
            <div class="goal-div">
                <p>7</p>
            </div>
        </div>
        <div class="vs">
            <p>vs</p>
        </div>
        <div class="left">
            <div class="img-div">
                {{ Html::image('img/profile-pic1.jpg', 'Pool Name', array('class'=>'img-responsive')) }}
            </div>
            <div class="name-div">
                <p>Elbow master<span>10-1-2</span></p>
            </div>
            <div class="goal-div">
                <p>3</p> 
            </div>
        </div>
        <div class='bottom'>
            <div class='width10'>
                <p>G</p>
            </div>
            <div class='width10'>
                <p>A</p>
            </div> 
            <div class='width10'>
                <p>P</p>
            </div>
            <div class='width10'>
                <p>+/-</p>
            </div>
            <div class='width10 red'>
                <p>H</p>
            </div>
            <div class='width10'>
                <p>GWG</p>
            </div>
            <div class='width10 red'>
                <p>PIM</p>
            </div>
            <div class='width10 blue'>
                <p>W</p>
            </div>
            <div class='width10 red'>
                <p>SV</p>
            </div>
            <div class='width10 blue'>
                <p>S/O</p>
            </div>
        </div>
    </div>
    <div class='text-right leading-losing'>
        <span class='green circle'></span>
        <span>{{ trans('custom.leading') }}</span>
        <span class='gray circle'></span>
        <span>{{ trans('custom.tied') }}</span>
        <span class='maroon circle'></span>
        <span>{{ trans('custom.losing') }}</span>
    </div>
</div>
