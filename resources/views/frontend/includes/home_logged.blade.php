@section('after-styles-end')
{!! Html::style(elixir('css/home_loggedin.css')) !!}
@stop
<div class="container mypools-container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="mypools" id="pool--content">
        <div class="row">
            <div class="col-sm-12 mypools--head2head">
                <div class="row">
                    <h4 class="head2head--main-heading">{{ trans('navs.frontend.home_nav.my_pools') }}</h4>
                    <div class="head2head--info" id="user-pools-all">

                        <div id="pool-right-header-outer"></div>

                        <p class="no-result-present" id="no-pools-present">
                            {{ trans('home.home_logged.no_pools_present1') }}
                            <a href="{{ route('pool.create') }}">{{ trans('home.home_logged.no_pools_present2') }}</a>
                        </p>
                    </div>
                </div>
            </div>

            <div id="pool-pagination"></div>
            <script id="pool-pagination-template" type="text/html">
                <% if(lastPage!= 1){ %>
                <div class="post-pagination">
                    <div class="pagination-child">
                        <% if(currentPage != 1){ %>
                        <% if((parseInt(currentPage)-1) != 1){ %>
                        <div id="pagination-first" class="pagination-arrows">
                            <a href="{{ route('frontend.pool.getUserPools') }}?page=1" id="get-pool-link">
                                <div class="double-arrow"></div>
                            </a>
                        </div>
                        <% } %>
                        <a href="{{ route('frontend.pool.getUserPools') }}<%= prevPageUrl %>" id="get-pool-link">
                            <div id="pagination-left" class="pagination-arrows"></div>
                        </a>
                        <% } %>
                        <div class="main-pagination">
                            <div id="page-list">
                                <% var prevPage = parseInt(currentPage) - 2;
                                if(prevPage > 0){ %>
                                <a href="{{ route('frontend.pool.getUserPools') }}?page=<%= prevPage %>"><%= prevPage %></a>
                                <% } %>
                                <% var prevPage = parseInt(currentPage) - 1;
                                if(prevPage > 0){ %>
                                <a href="{{ route('frontend.pool.getUserPools') }}?page=<%= prevPage %>"><%= prevPage %></a>
                                <% } %>

                                <% for(var i=currentPage; i<=lastPage; i++){ %>
                                <a href="{{ route('frontend.pool.getUserPools') }}?page=<%= i %>"
                                   class="<% if(currentPage == i){ %>active<% } %>" ><%= i %></a>
                                <% } %>
                            </div>
                        </div>
                        <% if(currentPage != lastPage){ %>
                        <a href="{{ route('frontend.pool.getUserPools') }}<%= nextPageUrl %>" id="get-pool-link">
                            <div id="pagination-right" class="pagination-arrows"></div>
                        </a>
                        <% if(lastPage != (parseInt(currentPage)+1)){ %>
                        <div id="pagination-last" class="pagination-arrows">
                            <a href="{{ route('frontend.pool.getUserPools') }}?page=<%= lastPage %>" id="get-pool-link">
                                <div class="double-arrow"></div>
                            </a>
                        </div>
                        <% } %>
                        <% } %>
                    </div>
                </div>
                <% } %>
                </script>

            </div>
            <div class="twitter-feeds"> 
                @include('frontend.includes.hockeypool_news')
            </div>

        </div>
        <div class="adblocks">
            @include('frontend.includes.create_pool')
            @include('frontend.includes.adblocks')
        </div>
    </div>
    <a href="{{ route('frontend.pool.dashboard', 0) }}" id="pool-overview-link" class="hide"></a>
    <a href="{{ route('pool.edit', 0) }}" id="pool-edit-link" class="hide"></a>
    <a href="{{ route('pool.destroy', 0) }}" id="pool-delete-link" class="hide"></a>
    <a href="{{ route('frontend.pool.boxDraft', 0) }}" id="pool-boxdraft-link" class="hide"></a>
    <a href="{{ route('frontend.pool.liveDraft', 'ld') }}" id="pool-livedraft-link" class="hide"></a>
    <a href="{{ route('frontend.pool.preDraft', 'pd') }}" id="pool-predraft-link" class="hide"></a>
    <script id="pool-right-header-template" type="text/html">
        <div class="pools--h2h">
            <% if(poolComplete == 1){ 
            //checking livedraft has started or not
            if(!$.isEmptyObject(poolLiveDraftSetting)){
            var liveDraftMoment = moment.tz(poolLiveDraftSetting.date + ' ' + poolLiveDraftSetting.time,moment.ISO_8601,poolLiveDraftSetting.time_zone).subtract(15, 'minutes'),
            currentMomentNew = moment().tz(poolLiveDraftSetting.time_zone);
            }
            %>
            <h4 class="head2head--heading-description">
                <a href="<%= poolLink %>">
                    <%= poolName %>
                </a>
                <% 
                if(!$.isEmptyObject(poolUser)){
                    if(!$.isEmptyObject(poolLiveDraftSetting) && liveDraftMoment.diff(currentMomentNew) > 0){
                %>
                <a href="<%= poolEditLink %>" class="btn btn-green-line">
                    {{trans('home.home_logged.edit')}}
                </a>
                <%
                    }else if($.isEmptyObject(poolLiveDraftSetting)){
                %>
                <a href="<%= poolEditLink %>" class="btn btn-green-line">
                    {{trans('home.home_logged.edit')}}
                </a>
                <%
                    }
                } 
                %>
            </h4>
            <div class="week">
                <%= poolMatchup.season_competition_type_week.name %>
            </div>
            <div class="upper">
                <% if (!$.isEmptyObject(poolLiveDraftSetting) && liveDraftMoment.diff(currentMomentNew) > 0) { %>
                <div class="draft">
                    <img src="<%= siteLink %>/img/pool_logos/<%= poolLogo %>" alt="<%= poolName %>"/>
                    <div class="text">Live Draft is scheduled for <%= moment(poolLiveDraftSetting.date).format('MMMM D,YYYY') %> at <%= moment(poolLiveDraftSetting.date + ' ' + poolLiveDraftSetting.time).format('H:mm a zz') %></div>
                    <a href="<%= poolPredraftLink %>" class="btn btn-backgreen">Pre-Draft</a>  
                </div>
                <%
                }else if(!$.isEmptyObject(poolLiveDraftSetting) && liveDraftMoment.diff(currentMomentNew) < 0 && poolLiveDraftActive != null){
                %>
                <div class="draft">
                    <img src="<%= siteLink %>/img/pool_logos/<%= poolLogo %>" alt="<%= poolName %>"/>
                    <div class="text">Draft is live NOW!</div>
                    <a href="<%= poolLivedraftLink %>" class="btn btn-backgreen">Enter Draft</a>
                </div>
                <%
                }else{
                %>
                <div class="right">
                    <div class="img-div">
                        <img src="<%= siteLink %>/img/team_logos/<%= poolMatchup.pool_team1.logo %>" alt="<%= poolMatchup.pool_team1.name %>" class="img-responsive"/>
                    </div>
                    <div class="name-div">
                        <p><%= poolMatchup.pool_team1.name %>
                            <span>
                                <% if(!$.isEmptyObject(poolMatchup.pool_team1.pool_matchup_result)){ %>
                                <%= poolMatchup.pool_team1.pool_matchup_result.total_win %>-<%= poolMatchup.pool_team1.pool_matchup_result.total_loss %>-<%= poolMatchup.pool_team1.pool_matchup_result.total_tie %>
                                <% }else{ %>
                                0-0-0
                                <% } %>
                            </span>
                        </p>
                    </div>
                    <div class="goal-div">
                        <p>
                            <% if($.isArray(poolMatchup.pool_matchup_results)){ 
                            if(!$.isEmptyObject(poolMatchup.pool_matchup_results)){ %>
                            <% if(poolMatchup.pool_team1.id == poolMatchup.pool_matchup_results[0].pool_team_id){ %>
                            <%= poolMatchup.pool_matchup_results[0].win %>
                            <% }else{ %>
                            <%= poolMatchup.pool_matchup_results[1].win %>
                            <% } %>
                            <% }else{ %>
                            0
                            <% } } %>
                        </p>
                    </div>
                </div>
                <div class="vs">
                    <p>{{trans('home.home_logged.versus')}}</p>
                </div>
                <div class="left">
                    <div class="img-div">
                        <img src="<%= siteLink %>/img/team_logos/<%= poolMatchup.pool_team2.logo %>" alt="<%= poolMatchup.pool_team2.name %>" class="img-responsive"/>
                    </div>
                    <div class="name-div">
                        <p><%= poolMatchup.pool_team2.name %>
                            <span>
                                <% if(!$.isEmptyObject(poolMatchup.pool_team2.pool_matchup_result)){ %>
                                <%= poolMatchup.pool_team2.pool_matchup_result.total_win %>-<%= poolMatchup.pool_team2.pool_matchup_result.total_loss %>-<%= poolMatchup.pool_team2.pool_matchup_result.total_tie %>
                                <% }else{ %>
                                0-0-0
                                <% } %>
                            </span>
                        </p>
                    </div>
                    <div class="goal-div">
                        <p>
                            <% if($.isArray(poolMatchup.pool_matchup_results)){ 
                            if(!$.isEmptyObject(poolMatchup.pool_matchup_results)){ %>
                            <% if(poolMatchup.pool_team2.id == poolMatchup.pool_matchup_results[0].pool_team_id){ %>
                            <%= poolMatchup.pool_matchup_results[0].win %>
                            <% }else{ %>
                            <%= poolMatchup.pool_matchup_results[1].win %>
                            <% } %>
                            <% }else{ %>
                            0
                            <% } } %>
                        </p> 
                    </div>
                </div>
                <% 
                }
                if(typeof poolScoreSettings != 'undefined'){ %>
                <div class='bottom'>
                    <% _.each(poolScoreSettings, function(value){ %>
                    <div class='width10' id='score-setting-<%= poolMatchup.id %>-<%= value.id %>'>
                        <p><%= value.pool_scoring_field.stat %></p>
                    </div>
                    <%  }); %> 
                </div>
                <% } %>
            </div>
            <% if(typeof poolScoreSettings != 'undefined'){ %>
            <div class='text-right leading-losing'>
                <span class='green circle'></span>
                <span>{{ trans('custom.leading') }}</span>
                <span class='gray circle'></span>
                <span>{{ trans('custom.tied') }}</span>
                <span class='maroon circle'></span>
                <span>{{ trans('custom.losing') }}</span>
            </div>
            <% } %>
            <% } else { %>
            <div class="upper pool-increate__upper">
                <div class="pool-increate">
                    <h4><%= poolName %> <span>{{trans('home.home_logged.not_finished')}}</span></h4>

                    <% if(!$.isEmptyObject(poolUser)){ %>
                    <a href="<%= poolEditLink %>" class="btn btn-backgreen">
                        {{trans('home.home_logged.continue')}}
                        <span class="arrow-right"></span>
                    </a>
                    <a href="<%= poolDeleteLink %>" class="btn btn-orange-line" id="pool-destroy">
                        Delete
                    </a>
                    <% } %>
                </div>
            </div>
            <% } %>
        </div>
    </script>
    <script id="pool-right-header-box-template" type="text/html">
        <div class="pools--box">
            <% if(poolComplete == 1){ 
            //checking livedraft has started or not
            if(!$.isEmptyObject(poolLiveDraftSetting)){
            var liveDraftMoment = moment(poolLiveDraftSetting.date + ' ' + poolLiveDraftSetting.time).tz(poolLiveDraftSetting.time_zone).subtract(15, 'm'),
            currentMomentNew = moment().tz(newYorkTimezone);
            }
            %>

            <% if(typeof poolToShow != 'undefined' && typeof poolTeamPlayers != 'undefined'){
            if(poolToShow == 2 && $.isEmptyObject(poolTeamPlayers)){ %>
            <div class="upper pool-increate__upper">
                <div class="pool-increate">
                    <h4><%= poolName %> <span>(You need to draft your team)</span></h4>

                    <a href="<%= boxDraftLink %>" class="btn btn-green-line" >
                        Draft Team
                    </a>
                </div>
            </div>
            <% } else{ %>
            <h4 class="head2head--heading-description">
                <a href="<%= poolLink %>">
                    <%= poolName %>
                </a>
                <% 
                if(!$.isEmptyObject(poolUser)){
                    if(!$.isEmptyObject(poolLiveDraftSetting) && liveDraftMoment.diff(currentMomentNew) > 0){
                %>
                <a href="<%= poolEditLink %>" class="btn btn-green-line">
                    {{trans('home.home_logged.edit')}}
                </a>
                <%
                    }else if($.isEmptyObject(poolLiveDraftSetting)){
                %>
                <a href="<%= poolEditLink %>" class="btn btn-green-line">
                    {{trans('home.home_logged.edit')}}
                </a>
                <%
                    }
                } 
                %>
            </h4>
            
            <div class="week">
                {{trans('home.home_logged.pool_stats')}}
            </div>
            <div class="pools--box-inner">
                <% if (!$.isEmptyObject(poolLiveDraftSetting) && liveDraftMoment.diff(currentMomentNew) > 0) { %>
                <div class="draft">
                    <img src="<%= siteLink %>/img/pool_logos/<%= poolLogo %>" alt="<%= poolName %>"/>
                    <div class="text">Live Draft is scheduled for <%= moment(poolLiveDraftSetting.date).format('MMMM D,YYYY') %> at <%= moment(poolLiveDraftSetting.date + ' ' + poolLiveDraftSetting.time).format('H:mm a zz') %></div>
                    <a href="<%= poolPredraftLink %>" class="btn btn-backgreen">Pre-Draft</a> 
                </div>
                <%
                }else if(!$.isEmptyObject(poolLiveDraftSetting) && liveDraftMoment.diff(currentMomentNew) < 0 && poolLiveDraftActive != null){
                %>
                <div class="draft">
                    <img src="<%= siteLink %>/img/pool_logos/<%= poolLogo %>" alt="<%= poolName %>"/>
                    <div class="text">Draft is live NOW!</div>
                    <a href="<%= poolLivedraftLink %>" class="btn btn-backgreen">Enter Draft</a>
                </div>
                <%
                }else{
                %>
                <div class="ranks">
                    <%= hpApp.getOrdinal(poolTeamFirst.rank) %>
                </div>
                <% if(!$.isEmptyObject(poolTeamFirst.standard_score)){ %>
                <div class="points">
                    <span>
                        <%= parseInt(poolTeamFirst.standard_score.total_score) %>
                    </span>
                    <span>{{trans('home.home_logged.pool_points')}}</span>
                </div>
                <% } %>
                <div class="stats">
                    <table class="table light-grey-table" id="no-more-tables">
                        <thead>
                            <tr>
                                <% _.each(poolScoreSettings, function(value){ %>
                                <th><%= value.pool_scoring_field.stat %></th>
                                <%  }); %> 
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <% _.each(poolScoreSettings, function(value){ %>
                                <td data-title="<%= value.pool_scoring_field.stat %>" id="score-setting-<%= poolTeamFirst.id %>-<%= value.pool_scoring_field_id %>">0</td>
                                <%  }); %> 
                            </tr>
                        </tbody>
                    </table>
                </div>
                <% } %>
                
            </div>
            <% } } %>
        </div>
        <% } else { %>
        <div class="upper pool-increate__upper">
            <div class="pool-increate">
                <h4><%= poolName %> <span>{{trans('home.home_logged.not_finished')}}</span></h4>

                <% if(!$.isEmptyObject(poolUser)){ %>
                <a href="<%= poolEditLink %>" class="btn btn-backgreen">
                    {{trans('home.home_logged.continue')}}
                    <span class="arrow-right"></span>
                </a>
                <a href="<%= poolDeleteLink %>" class="btn btn-orange-line" id="pool-destroy">
                    Delete
                </a>
                <% } %>
            </div>
        </div>
        <% } %>
    </div>
</script>
@include('frontend.includes.site_properties') 
@section('after-scripts-end')
<script>
    var poolOverviewUrl = '{{route('frontend.pool.getUserPools')}}';
    poolTypes = <?php echo json_encode(config('pool.inverse_type')); ?>,
            poolFormat = <?php echo json_encode(config('pool.inverse_format')); ?>,
            newYorkTimezone = '<?php echo config('app.timezone'); ?>';
</script>
{!! Html::script(elixir('js/home_loggedin.js')) !!}
@stop