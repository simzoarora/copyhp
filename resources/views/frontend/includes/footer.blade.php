<footer>
    <div class="bg-trans" style="display: none">
    </div>
    <div class="footer--upper">
        <div class="footer--upper--container">
            <div class="footer--upper--container--footernav">
                <div class="upperfooternav--item">
                    <ul>
                        <li><a href="{{ route('frontend.pages.poolTypes') }}">{{ trans('custom.pool_types') }}</a></li>
                        <li><a href="{{ route('frontend.pages.howItWorks') }}">{{ trans('navs.frontend.footer.how_it_works') }}</a></li>
                        <li><a href="{{ route('frontend.pages.faq') }}">{{ trans('navs.frontend.footer.faq') }}</a></li>
                    </ul>
                </div> 
                <div class="upperfooternav--item">
                    <ul>
                        <li><a href="{{ route('frontend.pages.contact') }}">{{ trans('navs.frontend.footer.contact') }}</a></li>
                        <li><a href="{{ route('frontend.pages.advertise') }}">{{ trans('navs.frontend.footer.advertise') }}</a></li>
                    </ul>
                </div>
                <div class="upperfooternav--item">
                    <ul class="inline">
                        <li><a href="https://twitter.com/hockeypools" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/hockeypools/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer--lower">
        <div class="footer--lower--col">
            <div class="footer--lower--col--container">
                <div class="footer--lower--col--container--footernav">
                    <div class="lowerfooternav--item">
                        <div class="row">
                            {{ Html::image('img/footer_logo.png', 'Hockeypool Logo', array('class' => 'img-responsive')) }}
                        </div>
                    </div>
                    <div class="lowerfooternav--item">
                        <div class="row">
                            <ul>
                                <li><a href="{{ route('frontend.pages.termsOfUse') }}">{{ trans('navs.frontend.footer.terms_of_use') }}</a></li>
                                <li><a href="{{ route('frontend.pages.privacyPolicy') }}">{{ trans('navs.frontend.footer.privacy_policy') }}</a></li>
                                <li>&copy; <a href="{{route('frontend.home.index')}}">HockeyPool.com</a>. {{ trans('navs.frontend.footer.copyright') }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>