<script type="text/javascript">
    var weekNameArr = [],
            weekCountArr = {};
</script>
<script id="slider-template" type="text/html">
    <div class="slider--left-arrow"><div class="graphics"></div></div>
    <div class="slider--right-arrow"><div class="graphics"></div></div>
    <div class="slider">
        <% _.each(pool_matchups,function(matchup){ 
        if($.inArray(matchup.season_competition_type_week.name, weekNameArr) == -1){
        weekNameArr.push(matchup.season_competition_type_week.name);
        var weekName = matchup.season_competition_type_week.name;
        weekCountArr[weekName] = 1;
        var weekNameClass = weekName.replace(' ', '-');
        %>
        <div class="matchup-weeks <%= weekNameClass %>">
            <%= matchup.season_competition_type_week.name %>
        </div>
        <% }else{ 
        var weekName = matchup.season_competition_type_week.name;
        weekCountArr[weekName] = weekCountArr[weekName] + 1;
        } }) %>
        <div class="clearfix"></div>
        <% _.each(pool_matchups,function(matchup){ %>
        <div class="matchup-teams">
            <div class="team">
                <p class="team-name"><%= matchup.pool_team1.name %></p>
                <p class="team-score"><%= matchup.pool_matchup_results.length!=0?matchup.pool_matchup_results[matchup.team1].total_score:'0' %></p>
            </div>
            <div class="team">
                <p class="team-name"><%= matchup.pool_team2.name %></p>
                <p class="team-score"><%= matchup.pool_matchup_results.length!=0?matchup.pool_matchup_results[matchup.team2].total_score:'0' %></p>
            </div>
        </div>
        <% }) %>
    </div>
</script>

