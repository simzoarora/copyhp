<div id="trial-period-outer"></div>
<script id="trial-period-template" type="text/html">
    <div class="modal fade in" role="dialog" id="trial_expired" tabindex="-1" aria-labelledby="trial_expiredLabel" data-backdrop="false" style="display: block; padding-right: 17px;"> 
        <div class="modal-dialog" role="document"> 
            <div class="modal-content"> 
                <div class="modal-body">
                    <% if(userId == user_id){ %>
                    <div class="modal-box"> 
                        <div class="inner-wrapper">
                            <i class="fa fa-lock"></i>
                            <h1 class="pool--main-heading">{{trans('custom.trial_period.expired')}}</h1>
                            <p>
                                {{trans('custom.trial_period.continue')}}
                            </p>
                            <a href="<%= poolPaymentUrl %>" class="btn btn-backgreen">{{trans('custom.trial_period.activate')}}</a>
                        </div>
                    </div>
                    <% } else { %>
                    <div class="modal-box">
                        <div class="inner-wrapper">
                            <i class="fa fa-lock"></i>
                            <h1 class="pool--main-heading">{{trans('custom.trial_period.expired')}}</h1>
                            <p>
                                {{trans('custom.trial_period.contact')}}
                            </p>
                        </div> 
                    </div> 
                    <% } %>
                </div>
            </div>
        </div>
    </div>
</script>
