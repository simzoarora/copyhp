<?php $today = strtotime(date("Y-m-d")); ?>
<div class="league-nav" id="league-nav">
    <ul>
        <li data-start_date="<?php echo date("Y-m-d", $today); ?>"  data-end_date="<?php echo date("Y-m-d", $today); ?>">
            <a>{{ trans('custom.league_nav.today') }}</a>
        </li>
        <li data-start_date="<?php echo date("Y-m-d", strtotime('monday this week')); ?>"  data-end_date="<?php echo date("Y-m-d", $today); ?>">
            <a>{{ trans('custom.league_nav.week') }}</a>
        </li>
        <li data-end_date="<?php echo date("Y-m-d", $today); ?>"  data-start_date="<?php echo date("Y-m-d", strtotime("-1 week", $today)); ?>">
            <a>{{ trans('custom.league_nav.last_7_days') }}</a>
        </li>
        <li data-end_date="<?php echo date("Y-m-d", $today); ?>"  data-start_date="<?php echo date("Y-m-d", strtotime("-30 days", $today)); ?>">
            <a>{{ trans('custom.league_nav.last_30_days') }}</a>
        </li>
        <li>
            <a>{{ trans('custom.league_nav.season') }}</a>
        </li>
    </ul>
    <div id="season_parent">
        {{ Form::select('season', [0=>trans('pool.player_stats.select_season')] + $seasons, isset($_GET["season_id"])?$_GET["season_id"]:null, ['class' => 'select-field','id'=>'season']) }}
    </div>
</div>