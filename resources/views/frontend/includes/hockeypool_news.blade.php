<div class="row"> 
    <aside class="home-public--news">
        <div class="home-public--news--heading">
            <p>{{ trans('custom.hockey_pool_news.heading') }}</p>
            <div class="arrow-down"></div>
        </div>
        <div class="upcoming-tweets">
            <div class="tweets--loader">
                {{ Html::image('img/loading.gif', 'Hockeypool Loader') }}
            </div>
        </div>
        <script class="tweet_tem" type="text/html">
            <div class="home-public--news--tweet" data-tweet_id="<%= tweet_id %>">
                <div class="image">
                    <img src="<%= image %>">
                </div>
                <div class="description"> 
                    <h3><%= name %></h3>
                    <p><%= tweet %></p>
                    <p><%= created %>
                        @if(Auth::user())
                        <a class="pull-right showreply">{{ trans('labels.general.reply') }}</a>
                        @endif()
                    </p>
                    @if(Auth::user())
                    <div class="replay">
                        {!! Form::open(['route' => 'frontend.twitter.commentsSubmit', 'id' => 'twitter-comment-form', 'role' => 'form', 'method' => 'post']) !!}
                        <input type="hidden" name="tweet_id" value="<%= tweet_id %>"/>
                        <input type="text" name="comment" placeholder="{{trans('pages.comment_placeholder')}}" required/>
                        <button class="btn-backgreen">{{trans('labels.general.reply')}}</button>
                        {{ Form::close() }}
                    </div>
                    @endif()
                </div> 
                <div class="discussion"></div>
            </div>
            </script>
        </aside>
    </div>