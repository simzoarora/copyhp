<div class='row'>
    <div class='player-news'>
        <h3 class="pool--main-heading">{{trans('custom.player_news.title')}}</h3> 
        <ul class="list-name" id='player_news_tabs' data-url="{{route('frontend.player.getDataForSidebarInjuryReport',$pool_id)}}">
            <li data-tab='myteam' class='active'>{{trans('custom.player_news.my_team')}}</li>
            <li data-tab='league'>{{trans('custom.player_news.league')}}</li>
        </ul>
        <div class="list-content">
            <div id="myteam">
            </div>
            <div id="league" style='display: none;'>
            </div>
        </div>
    </div>
</div>
<script id="player-news-template" type="text/html">
    <% if(injury.player_season != null) { %>
    <div class='news-single'>
        <div class='img-div'>
           
            <img src="<%= api_url %>/img/nhl_logos/<%=injury.player_season.team.logo%>">
          
        </div>
        <div class='news-main'>
            <h3><%= injury.player_season.player.full_name%> - <%= injury.injury_location%> - <%= injury.injury_display_status%></h3>
            <p><span class="init-shown"></span><span class="init-hidden"><%= injury.injury_note %></span></p>
        </div>
        <!--<div class='read-more'>-->
            <!--<a href='#'>{{trans('custom.player_news.read_more')}}</a>-->
        <!--</div>-->
    </div>
    <% } %>
</script>
<script>
var player_news = <?php echo json_encode(trans('custom.player_news')); ?>;
</script>
