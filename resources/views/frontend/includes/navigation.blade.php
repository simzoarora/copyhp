<header>
    <div class="header--navigation" role="group" aria-label="...">
        <div class="hnavigation--item">
            <a href="{{ route('frontend.home.index') }}">
                {{ Html::image('img/logo.png', 'Hockeypool Logo', array('class' => 'img-responsive')) }}
                {{ Html::image('img/logo-768.png', 'Hockeypool Logo', array('class' => 'img-responsive')) }}
                {{ Html::image('img/footer_logo.png', 'Hockeypool Logo', array('class' => 'img-responsive')) }}
            </a>
        </div>
        @if(Auth::user())
        <div class="hnavigation--item" role="group">
            <a class="btn btn-default" href="{{ route('frontend.home.myPool') }}">{{ trans('navs.frontend.home_nav.my_pools') }}</a>
        </div>
        @endif
        <div class="hnavigation--item" role="group">
            <a class="hnavitem-dropdown-trigger dropdown">{{ trans('navs.frontend.home_nav.nhl') }}
                <span class="hidden-xs">{{ trans('navs.frontend.home_nav.stats') }}</span>
            </a> 
            <div class="hnavitem-dropdown normaldropdown">
                <ul>
                    <li><a href="{{ route('frontend.scores.index') }}">{{ trans('navs.frontend.home_nav.nhl') }} {{ trans('navs.frontend.home_nav.submenus.nhl.scores') }}</a></li>
                    <li><a href="{{ route('frontend.nhl.standings') }}">{{ trans('navs.frontend.home_nav.nhl') }} {{ trans('navs.frontend.home_nav.submenus.nhl.standings') }}</a></li>
                    <li><a href="{{ route('frontend.nhl.schedule') }}">{{ trans('navs.frontend.home_nav.nhl') }} {{ trans('navs.frontend.home_nav.submenus.nhl.schedule') }}</a></li>
                    <li><a href="{{ route('frontend.nhl.playerStats') }}">{{ trans('navs.frontend.home_nav.nhl') }} {{ trans('navs.frontend.home_nav.submenus.nhl.player_stats') }}</a></li>
                    <li><a href="{{ route('frontend.nhl.injuryReport') }}">{{ trans('navs.frontend.home_nav.nhl') }} {{ trans('custom.pool_nav.injury_report') }}</a></li>
                </ul>
            </div>
        </div>
        <div class="hnavigation--item" role="group">
            <a class="hnavitem-dropdown-trigger dropdown">
                <span class="hidden-xs">{{ trans('navs.frontend.home_nav.draft') }}</span>
                {{trans('navs.frontend.home_nav.tools') }}
            </a>
            <div class="hnavitem-dropdown normaldropdown">
                <ul>
                    <li><a href="{{ route('frontend.payment.draft_kit') }}">{{ trans('navs.frontend.home_nav.draft_kit') }}</a></li>
                </ul>
            </div>
        </div>
        <div class="hnavigation--item" role="group">
            <a class="btn btn-default" href="{{ route('frontend.blog.index') }}">{{trans('navs.frontend.home_nav.blog') }}</a>
        </div>
        @if(Auth::guest())
        <div class="hnavigation--item" role="group">
            <a class="hnavitem-dropdown-trigger dropdown loginClick">{{ trans('navs.frontend.home_nav.login') }}</a>
            <div class="hnavitem-dropdown login-form">
                {!! Form::open(array('route' => 'postlogin','id'=>'login-nav-form')) !!}
                <div class="email-field">
                    {!!Form::email('email',null, array('class' => 'emailaddress','placeholder'=>trans('navs.frontend.home_nav.email_address')))!!}
                </div>
                <div class="password-field">
                    {!!Form::password('password', array('class' => 'password','placeholder'=>trans('navs.frontend.home_nav.password')));!!}
                </div>
                {!!Form::submit(trans('navs.frontend.home_nav.login_button'),array('class'=>'loginbtn'));;!!}
                {!! Form::close() !!}
            </div>
        </div>
        <div class="hnavigation--item" role="group">
            <a class="btn btn-default" href="{{ route('auth.register') }}">{{ trans('navs.frontend.home_nav.register') }}</a>
        </div>
        @endif
        @if(Auth::user())
        <div class="hnavigation--item" role="group">
            <a class="hnavitem-dropdown-trigger dropdown">{{ trans('navs.frontend.home_nav.my_account') }}</a>
            <div class="hnavitem-dropdown normaldropdown"> 
                <ul>
                    <li><a href="{{ route('auth.myAccount') }}">{{ trans('navs.frontend.home_nav.my_account') }}</a></li>
                    <li><a href="{{ route('auth.edit.account') }}">{{ trans('navs.frontend.home_nav.edit_account') }}</a></li>
                    <li><a href="{{ route('auth.logout') }}">{{ trans('navs.frontend.home_nav.logout') }}</a></li>
                </ul>
            </div>
        </div>        
        <div class="hnavigation--item dropdown-click notification" role="group">
            <a class="hnavitem-dropdown-trigger"><span class="msgimage"></span><span class="noti-number"></span></a>
            <div class="notification-dropdown"> 
                <div class="notification-header">
                    {{ trans('navs.frontend.home_nav.notifications') }}
                </div>
                <div class="notification-all">
                    <!--appended via underscore-->
                </div>
            </div>
        </div>        
        @endif()
        <div class="hnavigation--item" role="group">
            <a class="btn btn-default activegreen hidden-sm" href="{{ route('pool.create') }}">{{ trans('buttons.home.create_pool') }}</a>
            <a class="btn btn-default activegreen visible-sm" href="{{ route('pool.create') }}">{{ trans('buttons.home.createpool') }}</a>
        </div>
    </div>
</header>
<!--Mobile header-->
<header>
    <div class="header--mobilenavigation">
        <div class="row">
            <div class="logo_div_mobile">
                <a href="{{ route('frontend.home.index') }}">
                    {{ Html::image('img/logo.png', 'Hockeypool Logo', array('class' => 'img-responsive')) }}
                </a>
            </div>
            <div class="bootstrap_btn_mobile">
                <button type="button" class="navbar-toggle" id="open-navbar" data-toggleid="mobilelist">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="crossbtn"></span>
                </button>
            </div>
        </div>
    </div>
    <div class="header--mobilenavigation-btns">
        <div class="row">
            <div class="header--navigation" role="group" aria-label="...">
                @if(Auth::user())
                <div class="hnavigation--item" role="group">
                    <a href="{{ route('frontend.home.myPool') }}" class="mypools">{{ trans('navs.frontend.home_nav.my_pools') }}</a>
                </div>
                <div class="hnavigation--item dropdown-click notification" role="group">
                    <a class="hnavitem-dropdown-trigger"><span class="msgimage"></span><span class="noti-number"></span></a>
                    <div class="notification-dropdown"> 
                        <div class="notification-header">
                            {{ trans('navs.frontend.home_nav.notifications') }}
                        </div>
                        <div class="notification-all"> 
                            <!--appended via underscore-->
                        </div>
                    </div>
                </div> 
                @endif()
                <div class="hnavigation--item" role="group">
                    <a href="{{ route('pool.create') }}" class="createpool activegreen" href="{{ route('pool.create') }}">{{ trans('buttons.home.createpool') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="header--mobilenavigation-list" id="mobilelist">
        <a class="open-sub-menu">{{ trans('navs.frontend.home_nav.draft') }} <span class="pull-right arrowdown"></span></a>
        <div class="sub-menu">
            <a href="{{ route('frontend.payment.draft_kit') }}">{{ trans('navs.frontend.home_nav.draft_kit') }}</a>
        </div>
        <a class="open-sub-menu">{{ trans('navs.frontend.home_nav.stats') }} <span class="pull-right arrowdown"></span></a>
        <div class="sub-menu">
            <a href="{{ route('frontend.scores.index') }}">{{ trans('navs.frontend.home_nav.submenus.nhl.scores') }}</a>
            <a href="{{ route('frontend.nhl.standings') }}">{{ trans('navs.frontend.home_nav.submenus.nhl.standings') }}</a>
            <a href="{{ route('frontend.nhl.schedule') }}">{{ trans('navs.frontend.home_nav.submenus.nhl.schedule') }}</a>
            <a href="{{ route('frontend.nhl.playerStats') }}">{{ trans('navs.frontend.home_nav.submenus.nhl.player_stats') }}</a>
            <a href="{{ route('frontend.nhl.injuryReport') }}">{{ trans('custom.pool_nav.injury_report') }}</a>
        </div>
        @if(Auth::user())
        <a class="open-sub-menu">{{ trans('navs.frontend.home_nav.my_account') }}<span class="pull-right arrowdown"></span></a>
        <div class="sub-menu">
            <a href="{{ route('auth.myAccount') }}">{{ trans('navs.frontend.home_nav.my_account') }}</a>
            <a href="{{ route('auth.edit.account') }}">{{ trans('navs.frontend.home_nav.edit_account') }}</a>
            <a href="{{ route('auth.logout') }}">{{ trans('navs.frontend.home_nav.logout') }}</a>
        </div>
        @endif()
        @if(Auth::guest())
        <a href="{{ route('auth.login') }}">{{ trans('navs.frontend.home_nav.login') }}</a>
        <a href="{{ route('auth.register') }}">{{ trans('navs.frontend.home_nav.register') }}</a>
        @endif()
        <div class="other-links">
<!--            <div class="language-bar-mobile">
                Language:
                <input type="radio" class="switch-input"/>
                <label class="switch switch-blue">
                    <input type="checkbox" class="switch-input" 
                    <?php
//                    if (Config::get('app.locale') == 'en') {
//                        echo 'checked';
//                    }
                    ?> >
                    <span class="switch-label" data-on="Eng" data-off="Fr"></span>
                    <span class="switch-handle"></span>
                </label> 
            </div>-->
            <a href="{{ route('frontend.pages.poolTypes') }}">{{ trans('custom.pool_types') }}</a>
            <a href="{{ route('frontend.pages.howItWorks') }}">{{ trans('navs.frontend.footer.how_it_works') }}</a>
            <a href="{{ route('frontend.pages.faq') }}">{{ trans('navs.frontend.footer.faq') }}</a>
            <a href="{{ route('frontend.pages.contact') }}">{{ trans('navs.frontend.footer.contact') }}</a>
            <a href="{{ route('frontend.pages.advertise') }}">{{ trans('navs.frontend.footer.advertise') }}</a>
            <div class="social-links">
                <a href="https://twitter.com/hockeypools" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="https://www.facebook.com/hockeypools/" target="_blank"><i class="fa fa-facebook"></i></a>
            </div>
        </div>
    </div>
</header>
<script id="notification-template" type="text/html">
    <div class="notification-single">
        <a href="<%= notification.link%>">
            <% if(notification.pool){%>
            <div class="pool-logo">
                {{ Html::image(config('pool.paths.logo').'<%=notification.pool.logo%>', 'Pool Name', array('class'=>'img-responsive')) }}
            </div>
            <% }else{ %>
            <div class="pool-logo-default">
                {{ Html::image('img/logo-768.png', 'Pool Name', array('class'=>'img-responsive notification-pool-logo')) }}
            </div>
            <% } %>
            <div class="pool-notification">
                <h3><%= notification.title%></h3>
                <p><%= notification.message%></p>
            </div>
            <div class="date-time"><%= moment(notification.created_at).format("h:mma MMMM DD, YYYY")%></div>
        </a>
        <div class="cross-btn" data-id="<%= notification.id%>">+</div>
    </div>
</script>
<!--END Mobile header-->