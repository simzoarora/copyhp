<div class="row">
    <section class="welcome-slider">
        <div class="welcome-slider--logo">
            {{ Html::image('img/hp_icon.png', '', array('class' => 'img-responsive')) }}
        </div>
        <div class="row">
            <div class="welcome-slider--heading">    
                <h2>{{ trans('custom.slider.h1') }}</h2>
            </div>
        </div>
        <p class="welcome-slider--subtext">
            {{ trans('custom.slider.h2') }}
        </p>
        <div class="welcome-slider--button">
            <!--<a href="#" class="btn-bordergreen">{{ trans('buttons.home.join_a_pool') }}</a>-->
            <a href="{{ route('pool.create') }}" class="btn-backgreen">{{ trans('buttons.general.crud.create') }} 
                <span class="plus-sign"></span>
            </a>
        </div>
    </section>
</div>