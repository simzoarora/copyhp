@section('after-styles-end')
{!! Html::style(elixir('css/home_public.css')) !!}
@stop
<section class="welcome-slider">
    <div class="welcome-slider--logo">
        {{ Html::image('img/HomeBannerLogo.png', 'Hockeypool Banner Logo') }}
    </div>
    <div class="row"> 
        <div class="welcome-slider--sidelines"></div> 
        <div class="welcome-slider--heading">  
            <h2>{{ trans('custom.slider.h1') }}</h2>
        </div>
        <div class="welcome-slider--sidelines right"></div>
    </div>
    <p class="welcome-slider--subtext">{{ trans('custom.slider.h2') }}</p>
    <div class="welcome-slider--button">
        <!--<a href="#" class="btn-bordergreen">{{ trans('buttons.home.join_a_pool') }}</a>-->
        <a href="{{ route('pool.create') }}" class="btn-backgreen">{{ trans('buttons.home.create_pool') }} <span class="plus-sign"></span></a>
    </div>
</section>
@include('frontend.includes.site_properties') 
<section class="home-public--content">  
    <div class="row">
        <div class="pool-type-duplicate">
            <p class="pool-type--header"><span>1</span></p>
        </div>
        <div class="pool-news-duplicate">
            <p class="pool-type--footer"><span>1</span></p>
        </div>
        <div class="home-public--types">
            <div class="row">
                <div class="home-public--pool-type">
                    <div class="home-public--pool-type--heading">
                        <p>{{ trans('custom.pool_types') }}</p>
                        <div class="arrow-down"></div>
                    </div>
                    <div class="home-public--each-pool-type">
                        <div class="each-pool-type--col1"> 
                            <h4>{{ trans('custom.standard_pool') }}</h4>
                            <p>{{ trans('custom.standard_pool_p') }}</p>
                            <a href="{{ route('frontend.pages.poolTypes','#Standard') }}"><b>{{ trans('labels.general.learn_more') }}</b></a>
                        </div> 
                        <div class="each-pool-type--col2">
                            <a href="{{ route('pool.create', 'pool_type='.config("pool.inverse_type.standard")) }}" class="btn-backgreen">{{ trans('buttons.home.create') }} <span class="plus-sign"></span></a><br>
                            <!--<a href="#" class="btn-bordergreen">{{ trans('buttons.home.join_pool') }}</a>-->
                        </div>
                    </div>
                    <div class="home-public--each-pool-type">
                        <div class="each-pool-type--col1"> 
                            <h4>{{ trans('custom.head2head_pool') }}</h4>
                            <p>{{ trans('custom.head2head_pool_p') }}</p>
                            <a href="{{ route('frontend.pages.poolTypes','#HeadToHead') }}"><b>{{ trans('labels.general.learn_more') }}</b></a>
                        </div> 
                        <div class="each-pool-type--col2">
                            <a href="{{ route('pool.create', 'pool_type='.config("pool.inverse_type.h2h")) }}" class="btn-backgreen">{{ trans('buttons.home.create') }} <span class="plus-sign"></span></a><br>
                            <!--<a href="#" class="btn-bordergreen">{{ trans('buttons.home.join_pool') }}</a>-->
                        </div>
                    </div>
                    <div class="home-public--each-pool-type">
                        <div class="each-pool-type--col1"> 
                            <h4>{{ trans('custom.box_pool') }}</h4>
                            <p>{{ trans('custom.box_pool_p') }}</p>
                            <a href="{{ route('frontend.pages.poolTypes','#Box') }}"><b>{{ trans('labels.general.learn_more') }}</b></a>
                        </div> 
                        <div class="each-pool-type--col2">
                            <a href="{{ route('pool.create', 'pool_type='.config("pool.inverse_type.box")) }}" class="btn-backgreen">{{ trans('buttons.home.create') }} <span class="plus-sign"></span></a><br>
                            <!--<a href="#" class="btn-bordergreen">{{ trans('buttons.home.join_pool') }}</a>-->
                        </div>
                    </div>
                    <div class="home-public--each-pool-type">
                        <div class="each-pool-type--col1"> 
                            <h4>{{ trans('custom.rotisserie_pool') }}</h4>
                            <p>{{ trans('custom.rotisserie_pool_p') }}</p>
                            <a href="{{ route('frontend.pages.poolTypes','#Standard') }}"><b>{{ trans('labels.general.learn_more') }}</b></a>
                        </div> 
                        <div class="each-pool-type--col2">
                            <a href="{{ route('pool.create', 'pool_type='.config("pool.inverse_type.standard")) }}" class="btn-backgreen">{{ trans('buttons.home.create') }} <span class="plus-sign"></span></a><br>
                            <!--<a href="#" class="btn-bordergreen">{{ trans('buttons.home.join_pool') }}</a>-->
                        </div>
                    </div>
                </div>
                <div class="adblocks">
                    <div class="row"> 
                        <aside class="home-public--news">
                            <div class="home-public--news--heading">
                                <p>{{ trans('custom.hockey_pool_news.heading') }}</p>
                                <div class="arrow-down"></div>
                            </div>
                            <div class="upcoming-tweets">
                                <div class="tweets--loader">
                                    {{ Html::image('img/loading.gif', 'Hockeypool Loader') }}
                                </div>
                            </div>
                            <script class="tweet_tem" type="text/html">
                                <div class="home-public--news--tweet" data-tweet_id="<%= tweet_id %>">
                                    <div class="image">
                                        <img src="<%= image %>">
                                    </div>
                                    <div class="description"> 
                                        <h3><%= name %></h3>
                                        <p><%= tweet %></p>
                                        <p><%= created %></p>
                                    </div> 
                                    <div class="discussion"></div>
                                </div>
                                </script>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @section('after-scripts-end')
    {!! Html::script(elixir('js/home_public.js')) !!}
    @stop