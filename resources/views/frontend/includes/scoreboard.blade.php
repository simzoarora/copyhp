<div class="livescoreboard">
    <div class="language-bar {{ Config::get('app.locale') }}">
        <!--        <a href="{{ url('lang/fr') }}" class="fr">En Francais</a>
                <a href="{{ url('lang/en') }}" class="en">English</a>-->
    </div> 
    <div class="livescoreboard--left-arrow"><div class="graphics"></div></div>
    <div class="livescoreboard--right-arrow"><div class="graphics"></div></div>
    <div class="main_header">
        <div class="livescoreboard--scores">
            <div class="outer-parent">
                <div class="livescoreboard--loader"> 
                    {{ Html::image('img/loading.gif', 'Hockeypool Loader') }}
                </div>
                <script class="scoreboard_past" type="text/html">
                    <div class="livescoreboard--scores-parts each--slide" data-match_id="<%= match_id %>">
                        <span class="img"><img src="<%= away_image %>"></span>
                        <span class="team hidden-xxs hidden-xs"><%= away_abbr %></span>
                        <span class="score"><%= away_goals %></span>
                        <span class="final text-right hidden-xxs"><%= match_status %></span>
                        <span class="final visible-xxs"><%= match_complition_type %></span>
                        <br class="hidden-xxs">
                        <span class="img"><img src="<%= home_image %>"></span>
                        <span class="team hidden-xxs hidden-xs"><%= home_abbr %></span>
                        <span class="score"><%= home_goals %></span>
                        <span class="final text-right hidden-xxs"><%= match_complition_type %></span>
                        <a href="{{route('frontend.scores.index')}}/<%= competition_id %>">
                            <div class="live_stats">
                                <div class="graphics mobile"></div>
                                <span>
                                    {{trans('custom.scoreboard.view_stats')}}
                                </span>
                            </div>
                        </a>
                    </div>
                    </script>
                    <div class="livescoreboard--scores--previous2">
                        <div class="today previous"> 
                            <span>{{trans('custom.scoreboard.date')}}</span>
                        </div>
                    </div>
                    <div class="livescoreboard--scores--previous">
                        <div class="today previous"> 
                            <span>Date</span>
                        </div>
                    </div>

                    <div class="livescoreboard--scores--yesterday">
                        <div class="today yesterday">
                            <span>{{ trans('navs.frontend.livescoreboard.yesterday') }}</span>
                        </div>
                    </div>
                    <script class="scoreboard_today" type="text/html">
                        <div class="livescoreboard--scores-parts today-text each--slide" data-match_id="<%= match_id %>">
                            <span class="img"><img src="<%= away_image %>"></span>
                            <span class="team hidden-xxs hidden-xs"><%= away_abbr %></span>
                            <span class="score"><%= away_goals %></span>
                            <span class="final text-right hidden-xxs"><%= match_status %></span>
                            <span class="final visible-xxs"><%= match_complition_type %></span>
                            <br class="hidden-xxs">
                            <span class="img"><img src="<%= home_image %>"></span>
                            <span class="team hidden-xxs hidden-xs"><%= home_abbr %></span>
                            <span class="score"><%= home_goals %></span>
                            <span class="final text-right hidden-xxs"><%= match_complition_type %></span>
                            <a href="{{route('frontend.scores.index')}}/<%= competition_id %>">
                                <div class="live_stats">
                                    <div class="graphics mobile"></div>
                                    <span>
                                        {{trans('custom.scoreboard.view_live_stats')}}
                                    </span>
                                </div>
                            </a>
                        </div>
                        </script>
                        <div class="livescoreboard--scores--today">
                            <div class="today">
                                <span>{{ trans('navs.frontend.livescoreboard.today') }}</span>
                            </div>
                        </div>

                        <script class="scoreboard_future" type="text/html">
                            <div class="livescoreboard--scores-parts graybg each--slide" data-match_id="<%= match_id %>">
                                <span class="img"><img src="<%= away_image %>"></span>
                                <span class="team hidden-xxs hidden-xs"><%= away_abbr %></span>
                                <span class="time"><%= match_start_date %></span>
                                <br class="hidden-xxs">
                                <span class="img"><img src="<%= home_image %>"></span>
                                <span class="team hidden-xxs hidden-xs"><%= home_abbr %></span>
                                <span class="time">{{trans('custom.scoreboard.et')}}</span>
                                <a href="{{route('frontend.scores.index')}}/<%= competition_id %>">
                                    <div class="live_stats">
                                        <div class="graphics mobile"></div>
                                        <span>
                                            {{trans('custom.scoreboard.view_stats')}}
                                        </span>
                                    </div>
                                </a>
                            </div>
                            </script>
                            <div class="livescoreboard--scores--future">
                                <div class="tomorrow">
                                    <span>{{ trans('navs.frontend.livescoreboard.tomorrow') }}</span>
                                </div>
                            </div>
                            <div class="livescoreboard--scores--future--dayafter">
                                <div class="dayafter">
                                    <span>null</span>
                                </div>
                            </div>
                            <div class="livescoreboard--scores--future--dayafter--second">
                                <div class="dayafter">
                                    <span>null</span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>