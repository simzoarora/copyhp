@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/player_transaction.css')) !!}
@stop
@section('content')

<div class="container">
    <div class="row"> 
        @include('frontend.includes.pool_nav') 
        <div class="player-transaction-conatiner">
            <div class="player-transaction">
                <p class="pool--main-heading">NHL Player Transactions</p>
                <div class="player-transaction-slider">
                    <div class="slider--left-arrow"><div class="graphics"></div></div>
                    <div class="slider--right-arrow"><div class="graphics"></div></div>
                    <div class="slider">
                    <div class="transaction">
                        <p class="day">sun</p>
                        <p class="month">Feb</p>
                        <p class="count">5</p>
                    </div>
                    <div class="transaction">
                        <p class="day">sat</p>
                        <p class="month">Feb</p>
                        <p class="count">5</p>
                    </div>
                    <div class="transaction">
                        <p class="day">mon</p>
                        <p class="month">March</p>
                        <p class="count">5</p>
                    </div>
                    <div class="transaction">
                        <p class="day">tues</p>
                        <p class="month">March</p>
                        <p class="count">5</p>
                    </div>
                    <div class="transaction">
                        <p class="day">wed</p>
                        <p class="month">March</p>
                        <p class="count">5</p>
                    </div>
                    <div class="transaction">
                        <p class="day">thu</p>
                        <p class="month">March</p>
                        <p class="count">5</p>
                    </div>
                    <div class="transaction">
                        <p class="day">fri</p>
                        <p class="month">March</p>
                        <p class="count">5</p>
                    </div>
                    <div class="transaction">
                        <p class="day">fri</p>
                        <p class="month">March</p>
                        <p class="count">5</p>
                    </div>
                    <div class="transaction">
                        <p class="day">fri</p>
                        <p class="month">March</p>
                        <p class="count">5</p>
                    </div>
                    <div class="transaction">
                        <p class="day">thu</p>
                        <p class="month">March</p>
                        <p class="count">5</p>
                    </div>
                    <div class="transaction">
                        <p class="day">thu</p>
                        <p class="month">March</p>
                        <p class="count">5</p>
                    </div>
                </div>
                </div>
                <div class="calender">
                <input class="datepicker" type="text">
                <div class="see-calender">
                   <i class="fa fa-calendar-o"></i>
                    <span>See Calender</span>
                </div>
                </div>
                <h3 class="pool--main-heading">Monday, February 2016</h3>
                <table class="table light-grey-table  head-arrow">
                    <tr>
                        <th>Pool Team</th>
                        <th>NHL Team</th>
                        <th>Player</th>
                        <th>Position</th>
                        <th>Transactions</th>
                    </tr>
                    <tr>
                        <td>Team1</td>
                        <td>Lorem Ipsum</td>
                        <td>Lorem</td>
                        <td>C</td>
                        <td>Transactions Transactions</td>
                    </tr>
                    <tr class="gray-row">
                        <td>Team2</td>
                        <td>Lorem Ipsum</td>
                        <td>Lorem</td>
                        <td>C</td>
                        <td>Transactions Transactions</td>
                    </tr>
                    <tr>
                        <td>Team3</td>
                        <td>Lorem Ipsum</td>
                        <td>Lorem</td>
                        <td>C</td>
                        <td>Transactions Transactions</td>
                    </tr>
                    <tr>
                        <td>Team4</td>
                        <td>Lorem Ipsum</td>
                        <td>Lorem</td>
                        <td>C</td>
                        <td>Transactions Transactions</td>
                    </tr>
                    <tr>
                        <td>Team5</td>
                        <td>Lorem Ipsum</td>
                        <td>Lorem</td>
                        <td>C</td>
                        <td>Transactions Transactions</td>
                    </tr>
                    <tr>
                        <td>Team6</td>
                        <td>Lorem Ipsum</td>
                        <td>Lorem</td>
                        <td>C</td>
                        <td>Transactions Transactions</td>
                    </tr>
                    <tr>
                        <td>Team7</td>
                        <td>Lorem Ipsum</td>
                        <td>Lorem</td>
                        <td>C</td>
                        <td>Transactions Transactions</td>
                    </tr>
                    <tr>
                        <td>Team6</td>
                        <td>Lorem Ipsum</td>
                        <td>Lorem</td>
                        <td>C</td>
                        <td>Transactions Transactions</td>
                    </tr>
                    <tr>
                        <td>Team7</td>
                        <td>Lorem Ipsum</td>
                        <td>Lorem</td>
                        <td>C</td>
                        <td>Transactions Transactions</td>
                    </tr>
                    <tr>
                        <td>Team6</td>
                        <td>Lorem Ipsum</td>
                        <td>Lorem</td>
                        <td>C</td>
                        <td>Transactions Transactions</td>
                    </tr>
                    <tr>
                        <td>Team7</td>
                        <td>Lorem Ipsum</td>
                        <td>Lorem</td>
                        <td>C</td>
                        <td>Transactions Transactions</td>
                    </tr>
                </table>
                <div class="post-pagination" style="display:block;">
                <div class="pagination-child">
                    <div id="pagination-first" class="pagination-arrows">
                        <div class="double-arrow"></div>
                    </div>
                    <div id="pagination-left" class="pagination-arrows"></div>
                    <div class="main-pagination">
                        <div id="page-list">
 <a href="">1</a>
                    <a href="">2</a>
                    <a href="">3</a>
                    <a href="">4</a>
                    <a href="">5</a>
                        </div>
                    </div>
                    <div id="pagination-right" class="pagination-arrows"></div>
                    <div id="pagination-last" class="pagination-arrows">
                        <div class="double-arrow"></div>
                    </div>
                </div>
            </div>
            </div>
        </div>

    </div>
</div>

@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/player_transaction.js')) !!}
@stop