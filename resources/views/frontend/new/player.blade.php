@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/player.css')) !!}
@stop 
@section('content')
<div class="container"> 
    <div class="row">
        <div class="player--content">
            <p class="pool--main-heading">Statistics</p>
            <ul class="list-inline page-sub-nav">
                <li><a class="" data-show="divisions-all">Players</a></li>
                <li><a data-show="conference-all" class="">Teams</a></li>
                <li><a data-show="overall-all" class="selected">Enhanced</a></li>
            </ul>
            <div class="view-statistics">
                <div class="player-select-heading">
                 <p class="head2head--heading">View Statistics By</p>
                <p class="pool--main-heading">Seasons</p>
                </div>
                <div class="player-select-box">
                    <p class="head2head--heading">Player's Position</p>
                    {{ Form::select('player_position',['Skaters','Goalies','Forwards'],null, ['class' => 'select-field','id'=>'player_position']) }}
                </div>
                <div class="statistics-buttons">
                    <a href="#" class="btn btn-green-line"><span class="glyphicon glyphicon-print"></span> Print</a>
                </div>
            </div>
            <div class="player-select-box">
                <p class="head2head--heading">Seasons</p>
                {{ Form::select('select_season',['2015-2016','2016-2017','2017-2018'],null, ['class' => 'select-field','id'=>'select_season']) }}
            </div>
            <div class="player-select-box">
                <p class="head2head--heading">Game Type</p>
                {{ Form::select('regular_playoffs',['Regular Seasons','Goalies','Forwards'],null, ['class' => 'select-field','id'=>'regular_playoffs']) }}
            </div>
            <div class="player-select-box">
                <p class="head2head--heading">Team</p>
                {{ Form::select('list_team',['All Teams','Goalies','Forwards'],null, ['class' => 'select-field','id'=>'list_team']) }}
            </div>
            <table class="table light-grey-table">
                <tbody>
                    <tr>
                        <th></th>
                        <th>Player</th>
                        <th>Season</th>
                        <th>Team</th>
                        <th>Pos</th>
                        <th>GP</th>
                        <th>G</th>
                        <th>A</th>
                        <th>P<span class="arrow-down"></span></th>
                        <th>+/-</th>
                        <th>PIM</th>
                        <th>P/GP</th>
                        <th>PPG</th>
                        <th>PPP</th>
                        <th>SHG</th>
                        <th>SHP</th>
                        <th>GWG</th>
                        <th>OTG</th>
                        <th>S</th>
                        <th>S%</th>
                        <th>FOW%</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Patrick Cane</td>
                        <td>2015-2016</td>
                        <td>CHI</td>
                        <td>R</td>
                        <td>82</td>
                        <td>46</td>
                        <td>60</td>
                        <td>106</td>
                        <td>17</td>
                        <td>30</td>
                        <td>1.29</td>
                        <td>17</td>
                        <td>37</td>
                        <td>0</td>
                        <td>0</td>
                        <td>9</td>
                        <td>1</td>
                        <td>287</td>
                        <td>16.0</td>
                        <td>21.6</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Patrick Cane</td>
                        <td>2015-2016</td>
                        <td>CHI</td>
                        <td>R</td>
                        <td>82</td>
                        <td>46</td>
                        <td>60</td>
                        <td>106</td>
                        <td>17</td>
                        <td>30</td>
                        <td>1.29</td>
                        <td>17</td>
                        <td>37</td>
                        <td>0</td>
                        <td>0</td>
                        <td>9</td>
                        <td>1</td>
                        <td>287</td>
                        <td>16.0</td>
                        <td>21.6</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Patrick Cane</td>
                        <td>2015-2016</td>
                        <td>CHI</td>
                        <td>R</td>
                        <td>82</td>
                        <td>46</td>
                        <td>60</td>
                        <td>106</td>
                        <td>17</td>
                        <td>30</td>
                        <td>1.29</td>
                        <td>17</td>
                        <td>37</td>
                        <td>0</td>
                        <td>0</td>
                        <td>9</td>
                        <td>1</td>
                        <td>287</td>
                        <td>16.0</td>
                        <td>21.6</td>
                    </tr>
                </tbody>
            </table>
            <table class="table light-grey-table">
                <tbody>
                    <tr>
                        <th></th>
                        <th>Player</th>
                        <th>Season</th>
                        <th>Team</th>
                        <th>Pos</th>
                        <th>GP</th>
                        <th>G</th>
                        <th>A</th>
                        <th>P</th>
                        <th>+/-</th>
                        <th>PIM</th>
                        <th>P/GP</th>
                        <th>PPG</th>
                        <th>PPP</th>
                        <th>SHG</th>
                        <th>SHP</th>
                        <th>GWG</th>
                        <th>OTG</th>
                        <th>S</th>
                        <th>S%</th>
                        <th>FOW%</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Patrick Cane</td>
                        <td>2015-2016</td>
                        <td>CHI</td>
                        <td>R</td>
                        <td>82</td>
                        <td>46</td>
                        <td>60</td>
                        <td>106</td>
                        <td>17</td>
                        <td>30</td>
                        <td>1.29</td>
                        <td>17</td>
                        <td>37</td>
                        <td>0</td>
                        <td>0</td>
                        <td>9</td>
                        <td>1</td>
                        <td>287</td>
                        <td>16.0</td>
                        <td>21.6</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Patrick Cane</td>
                        <td>2015-2016</td>
                        <td>CHI</td>
                        <td>R</td>
                        <td>82</td>
                        <td>46</td>
                        <td>60</td>
                        <td>106</td>
                        <td>17</td>
                        <td>30</td>
                        <td>1.29</td>
                        <td>17</td>
                        <td>37</td>
                        <td>0</td>
                        <td>0</td>
                        <td>9</td>
                        <td>1</td>
                        <td>287</td>
                        <td>16.0</td>
                        <td>21.6</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Patrick Cane</td>
                        <td>2015-2016</td>
                        <td>CHI</td>
                        <td>R</td>
                        <td>82</td>
                        <td>46</td>
                        <td>60</td>
                        <td>106</td>
                        <td>17</td>
                        <td>30</td>
                        <td>1.29</td>
                        <td>17</td>
                        <td>37</td>
                        <td>0</td>
                        <td>0</td>
                        <td>9</td>
                        <td>1</td>
                        <td>287</td>
                        <td>16.0</td>
                        <td>21.6</td>
                    </tr>
                </tbody>
            </table>
            <div class="post-pagination" style="display:block;">
                <div class="pagination-child">
                    <div id="pagination-first" class="pagination-arrows">
                        <div class="double-arrow"></div>
                    </div>
                    <div id="pagination-left" class="pagination-arrows"></div>
                    <div class="main-pagination">
                        <div id="page-list">
                        </div>
                    </div>
                    <div id="pagination-right" class="pagination-arrows"></div>
                    <div id="pagination-last" class="pagination-arrows">
                        <div class="double-arrow"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/player.js')) !!}
@stop 