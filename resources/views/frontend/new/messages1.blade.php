@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/messages.css')) !!}
@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="message1">
            <div class="message-header">
                <h4>Fan Forum</h4>
            </div>
            <div class="message-nav">
                <ul>
                    <li class="active">
                        <a href="#">New Topic</a>
                        <span class="bottom-shadow"></span>
                    </li>
                    <li>
                        <a href="#">Inbox</a>
                        <span class="bottom-shadow"></span>
                        <span class="notification">2</span>
                    </li>
                    <li>
                        <a href="#">My posts</a>
                        <span class="bottom-shadow"></span>
                    </li>
                </ul>
                <!--message-nav-mobile-->
                <div class="bootstrap_btn_mobile">
                    <button type="button" class="navbar-toggle" id="open-navbar" data-toggleid="mobilelist">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="crossbtn"></span>
                    </button>
                    <ul>
                       <li class="active">
                        <a href="#">New Topic</a>
                        <span class="bottom-shadow"></span>
                    </li>
                    <li>
                        <a href="#">Inbox</a>
                        <span class="bottom-shadow"></span>
                        <span class="notification">2</span>
                    </li>
                    <li>
                        <a href="#">My posts</a>
                        <span class="bottom-shadow"></span>
                    </li>
                     <span class="close-sign"></span>
                    </ul>
                </div>
                <!--message-nav-mobile ends-->
            </div>
            <div class="message-box">
                <form>
                    <input type="text" class="grey-field" name="subject" placeholder="Subject">
                    <textarea class="grey-field" rows="5" name="message" placeholder="Type Message"></textarea>
                    <div class="identity">
                        <label>Identity:<span>Rick</span></label>
                        <a href="#">Change</a>
                    </div>
                    <div class="post-btn">
                        <a href="#" class="btn-backgreen">Post</a>
                    </div>
                </form>
            </div>
            <div class="message-posts">
                <div class="message-post-headings">
                    <p class="post-headings-title">Title</p>
                    <p class="post-headings-like"><span></span></p>
                    <p class="post-headings-dislike"><span></span></p>
                    <p class="post-headings-replies">Replies</p>
                    <p class="post-headings-latest">Latest post</p>
                </div>
                <div class="message-post-details">
                    <div class="post">
                        <div class="post-message">
                            <p>Switching People Out</p>
                            <div>
                                <p class="vertical-line">May 16,2016</p>
                                <p>11:00am by Daniel<p>
                            </div>
                        </div>
                        <div class="post-options">
                            <div class="post-likes">2</div>
                            <div class="post-dislikes">3</div>
                            <div class="post-replies">3</div>
                            <div class="post-latest">
                                <p>May 16,2016</p>
                                <p>11:00am by Daniel</p>
                            </div>
                        </div>
                    </div>
                    <!--post ends-->
                    <!--post-->
                    <div class="post">
                        <div class="post-message">
                            <p>Switching People Out</p>
                            <div>
                                <p class="vertical-line">May 16,2016</p>
                                <p>11:00am by Daniel<p>
                            </div>
                        </div>
                        <div class="post-options">
                            <div class="post-likes">2</div>
                            <div class="post-dislikes">3</div>
                            <div class="post-replies">3</div>
                            <div class="post-latest">
                                <p>May 16,2016</p>
                                <p>11:00am by Daniel</p>
                            </div>
                        </div>
                    </div>
                    <!--post ends-->
                    <!--post-->
                    <div class="post">
                        <div class="post-message">
                            <p>Switching People Out</p>
                            <div>
                                <p class="vertical-line">May 16,2016</p>
                                <p>11:00am by Daniel<p>
                            </div>
                        </div>
                        <div class="post-options">
                            <div class="post-likes">2</div>
                            <div class="post-dislikes">3</div>
                            <div class="post-replies">3</div>
                            <div class="post-latest">
                                <p>May 16,2016</p>
                                <p>11:00am by Daniel</p>
                            </div>
                        </div>
                    </div>
                    <!--post ends-->
                    <!--post-->
                    <div class="post">
                        <div class="post-message">
                            <p>Switching People Out</p>
                            <div>
                                <p class="vertical-line">May 16,2016</p>
                                <p>11:00am by Daniel<p>
                            </div>
                        </div>
                        <div class="post-options">
                            <div class="post-likes">2</div>
                            <div class="post-dislikes">3</div>
                            <div class="post-replies">3</div>
                            <div class="post-latest">
                                <p>May 16,2016</p>
                                <p>11:00am by Daniel</p>
                            </div>
                        </div>
                    </div>
                    <!--post ends-->
                </div>
            </div>
            <div class="post-pagination">
                <a href="">1</a>
                <a href="">2</a>
                <a href="">3</a>
                <a href="">4</a>
                <a href="">5</a>
            </div>
        @include('frontend.includes.ad_small_block')
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/messages.js')) !!}
@stop