@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/messages.css')) !!}
@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="message2">
            <div class="message-header">
                <h4>Fan Forum</h4>
                <a href="#" class="btn-backgreen"><span>&#43;</span>new topic</a>
            </div>
            <div class="message-nav">
                <ul>
                    <li>
                        <a href="#">Topics</a>
                        <span class="bottom-shadow"></span>
                    </li>
                    <li>
                        <a href="#">Inbox</a>
                        <span class="bottom-shadow"></span>
                        <span class="notification">2</span>
                    </li>
                    <li>
                        <a href="#">My posts</a>
                        <span class="bottom-shadow"></span>
                    </li>
                    <li class="active">
                        <a href="#">Champions of Champions</a>
                        <span class="bottom-shadow"></span>
                        <span class="close-sign"></span>
                    </li>
                </ul>
                <!--message-nav-mobile-->
                <div class="bootstrap_btn_mobile">
                    <button type="button" class="navbar-toggle" id="open-navbar" data-toggleid="mobilelist">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="crossbtn"></span>
                    </button>
                    <ul>
                        <li>
                            <a href="#">Topics</a>
                            <span class="bottom-shadow"></span>
                        </li>
                        <li>
                            <a href="#">Inbox</a>
                            <span class="bottom-shadow"></span>
                            <span class="notification">2</span>
                        </li>
                        <li>
                            <a href="#">My posts</a>
                            <span class="bottom-shadow"></span>
                        </li>
                        <li class="active">
                            <a href="#">Champions of Champions</a>
                            <span class="bottom-shadow"></span>
                        </li>
                            <span class="close-sign"></span>
                    </ul>
                </div>
                <!--message-nav-mobile ends-->
            </div>
            <div class="champions">
                <!--post1-->
                <div class="champion-post">
                    <div class="champion-pic">
                        {{ HTML::image('img/profile-pic1.jpg', 'profile-img', array('class' => 'champion-pic')) }}
                    </div>
                    <div class="champion-details">
                        <p class="champion-name">jim</p>
                        <div class="post-date">
                            <p>May 16,2016</p>
                            <p>11:00am</p>
                        </div>
                        <h3>Champions of Champions</h3>
                        <div class="description"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                It has survived not only five centuries
                            </p></div>
                        <a href="#" class="show-text">Show less</a>
                        <div class="post-reponses">
                            <p class="likes">2<span></span></p>
                            <p class="dislikes">3<span></span></p>
                            <p class="replies"><span></span>Reply</p>
                        </div>
                    </div>
                    <div class="sort-posts">
                        <label>sort:</label>
                        <p class="active">newest</p>
                        <p>oldest</p>
                        <p>Most replied</p>
                        <p class="expand-all">expand all</p>
                    </div>
                </div>
                <!--post1 ends-->
                <!--post2-->
                <div class="champion-post">
                    <div class="champion-pic">
                        {{ HTML::image('img/profile-pic1.jpg', 'profile-img', array('class' => 'champion-pic')) }}
                    </div>
                    <div class="champion-details">
                        <p class="champion-name">sah</p>
                        <div class="post-date">
                            <p>May 16,2016</p>
                            <p>11:00am</p>
                        </div>
                        <div class="description"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry to make a type specimen book. 
                                It has survived not only five centuries
                            </p></div>
                        <a href="#" class="show-text">Show more</a>
                        <div class="post-reponses">
                            <p class="likes">2<span></span></p>
                            <p class="dislikes">3<span></span></p>
                            <p class="replies"><span></span>Reply</p>
                        </div>
                    </div>
                </div>
                <!--post2 ends-->
                <!--post3-->
                <div class="champion-post">
                    <div class="champion-pic">
                        {{ HTML::image('img/profile-pic1.jpg', 'profile-img', array('class' => 'champion-pic')) }}
                    </div>
                    <div class="champion-details">
                        <p class="champion-name">thejamezer</p>
                        <div class="post-date">
                            <p>May 16,2016</p>
                            <p>11:00am</p>
                        </div>
                        <div class="description">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry to make a type specimen book. 
                                It has survived not only five centuries
                            </p>
                        </div>
                        <a href="#" class="show-text">Show more</a>
                        <div class="post-reponses">
                            <p class="likes">2<span></span></p>
                            <p class="dislikes">3<span></span></p>
                            <p class="replies"><span></span>Reply</p>
                        </div>
                        <!--post3 inner-post-->
                        <div class="champion-post">
                            <div class="champion-pic">
                                {{ HTML::image('img/profile-pic1.jpg', 'profile-img', array('class' => 'champion-pic')) }}
                            </div>
                            <div class="champion-details">
                                <p class="champion-name">dan</p>
                                <div class="post-date">
                                    <p>May 16,2016</p>
                                    <p>11:00am</p>
                                </div>
                                <div class="description">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        Lorem Ipsum has been the industry to make a type specimen book. 
                                        It has survived not only five centuries
                                    </p>
                                </div>
                                <a href="#" class="show-text">Show more</a>
                                <div class="post-reponses">
                                    <p class="likes">2<span></span></p>
                                    <p class="dislikes">3<span></span></p>
                                    <p class="replies"><span></span>Reply</p>
                                </div>
                            </div>
                        </div>
                        <!--post3 inner-post-->
                    </div>
                </div>
                <!--post3 ends-->
                <!--post4-->
                <div class="champion-post">
                    <div class="champion-pic">
                        {{ HTML::image('img/profile-pic1.jpg', 'profile-img', array('class' => 'champion-pic')) }}
                    </div>
                    <div class="champion-details">
                        <p class="champion-name">dan</p>
                        <div class="post-date">
                            <p>May 16,2016</p>
                            <p>11:00am</p>
                        </div>
                        <div class="description">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry to make a type specimen book. 
                                It has survived not only five centuries
                            </p>
                        </div>
                        <a href="#" class="show-text">Show more</a>
                        <div class="post-reponses">
                            <p class="likes">2<span></span></p>
                            <p class="dislikes">3<span></span></p>
                            <p class="replies"><span></span>Reply</p>
                        </div>
                    </div>
                </div>
                <!--post4 ends-->
            </div>
            <div class="post-pagination">
                <a href="">1</a>
                <a href="">2</a>
                <a href="">3</a>
                <a href="">4</a>
                <a href="">5</a>
            </div>
        @include('frontend.includes.ad_small_block')
        </div>
    </div>
</div>
@stop 
@section('after-scripts-end')
{!! Html::script(elixir('js/messages.js')) !!}
@stop