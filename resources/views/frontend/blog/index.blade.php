@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/blog.css')) !!}
@stop
@section('title')
   {{trans('blog.blog.page_header')}}
@stop
@section('content')
<div class="index--content">
    <div class="row" id="pool--content">
        <div class="index--posts blog-post">
            <p class="pool--main-heading">{{trans('blog.blog.title')}}</p>
            @forelse($blogs as $blog)
            <div class="index--post">
                <div class="index--post-left">
                    <a href="{{ route('frontend.blog.post',$blog->slug) }}">
                        {{ Html::image(config('backend.blog.img_path').$blog->thumbnail_image, $blog->title) }}
                    </a>
                </div>
                <div class="index--post-right body-outer">
                    <div class="index--post-title">
                        <h3><a href="{{ route('frontend.blog.post',$blog->slug) }}">{{ $blog->title }}</a></h3>
                    </div>
                    <div class="index--post-author">{{trans('blog.blog.by')}}<span>{{trans('blog.blog.hockeypool_staff')}}</span></div>
                    <div class="index--post-time">{{ date('M d, Y', strtotime($blog->created_at)) }}</div>
                    <div class="index--post-body">
                        <?php
                        if (strlen($blog->body) > 320) {
                            $pos = strpos($blog->body, ' ', 320);
                            echo substr($blog->body, 0, $pos) . '...';
                        } else {
                            echo $blog->body;
                        }
                        ?>
                    </div>
                    <a href="{{ route('frontend.blog.post',$blog->slug) }}" class="underline mt10">{{ trans('labels.backend.blog.full_story_link') }}</a>
                </div>
            </div>
            @empty
            <p class="no-result-present">{{trans('blog.blog.no_blogs')}}</p>
            @endforelse
        </div>
        <div class="adblocks">
            @include('frontend.includes.create_pool')
            @include('frontend.includes.adblocks')   
            @include('frontend.includes.hockeypool_news')
        </div>
    </div>
</div>
@stop
@section('after-scripts-end')
{!! Html::script(elixir('js/blog.js')) !!}
@stop