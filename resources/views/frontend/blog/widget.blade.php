@extends('frontend.layouts.frontend')

@section('after-styles-end')
{!! Html::style(elixir('css/blog.css')) !!}
@stop

@section('content')
<div class="widget--content mt20 mb20">
    <div class="row">
        <div class="widget--box">
            <div class="row">
                @forelse($blogs as $blog)
                <div class="widget--post">
                    <a href="{{ route('frontend.blog.post',$blog->slug) }}"><div style="background:url('/img/blog/{{$blog->featured_image}}') no-repeat; background-size: cover;"></div></a>
                    <h3><a href="{{ route('frontend.blog.post',$blog->slug) }}">{{ $blog->title }}</a></h3>
                    <p>{!! $blog->excerpt !!}</p>
                    <a href="{{ route('frontend.blog.post',$blog->slug) }}" class="underline mt10 displayblock">{{ trans('labels.backend.blog.full_story_link') }}</a>
                </div>
                @empty

                @endforelse
            </div>
        </div>
    </div>
</div>
@stop

@section('after-scripts-end')
{!! Html::script(elixir('js/blog.js')) !!}
@stop