@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/blog.css')) !!}
@stop
@section('title')
    {{ $blog->title }}
@stop
@section('content')
<div class="single_post--content">
    <div class="row" id="pool--content">
        <div class="post--content blog-post">
            <a href="{{ route('frontend.blog.index') }}" class="back-link"><i class="fa fa-angle-left"></i>{{trans('blog.blog.back_to_blog_home')}}</a>
            <div class="single_post--post">
                @if($blog)
                {{ Html::image(config('backend.blog.img_path').$blog->featured_image, $blog->title, ['class'=>'post-pic']) }}

                <h2 class="pool--main-heading">{{ $blog->title }}</h2>

                <div class="share-div">
                    <span>{{trans('blog.blog.share_us_on')}}</span>
                    <?php
                    if (strlen($blog->body) > 180) {
                        $pos = strpos($blog->body, ' ', 180);
                        $share_text = strip_tags(substr($blog->body, 0, $pos)) . '...';
                    } else {
                        $share_text = strip_tags($blog->body);
                    }
                    ?>
                    <a id="facebook-share">
                        <i class='fa fa-facebook'>
                            <div data-url="<?php echo Request::fullUrl(); ?>" data-text="<?php echo $share_text; ?>"></div>
                        </i>
                    </a>
                    <a id="twitter-share">
                        <i class='fa fa-twitter'>
                            <div data-url="<?php echo Request::fullUrl(); ?>" data-text="<?php echo $share_text; ?>"></div>
                        </i>
                    </a>
                    <a id="google-share">
                        <i class="fa fa-google-plus">
                            <div data-url="<?php echo Request::fullUrl(); ?>" data-text="<?php echo $share_text; ?>"></div>
                        </i>
                    </a>
                </div>
                <div class="clearfix"></div>

                <div class="post-author">{{trans('blog.blog.by')}}<span>{{trans('blog.blog.hockeypool_staff')}}</span></div>
                <div class="post-time">{{ date('M d, Y', strtotime($blog->created_at)) }}</div>
                <div class="post-body">{!! $blog->body !!}</div>
                <div class="post-comment">
                    <h2 class="pool--main-heading">{{trans('blog.blog.comments')}}</h2>

                    {!! Form::open(['route' => 'frontend.blog.commentsSubmit', 'id' => 'post-comment-form', 'role' => 'form', 'method' => 'post']) !!}
                    <input type="hidden" name="blog_id" value="{{ $blog->id }}"/>
                    <textarea name="comment" placeholder="{{trans('blog.blog.comments_placeholder')}}" rows="3" required></textarea>
                    <button class="btn-backgreen">{{trans('blog.blog.comment')}}</button>
                    {{ Form::close() }}

                    <div class="all-comments">
                        <?php
                        if (!empty($blog->blogComments)) {
                            foreach ($blog->blogComments as $comment) {
                                ?>
                                <div class="each-comment">
                                    <?php echo $comment->comment; ?>
                                    <div>
                                        <span class="name"><?php echo $comment->user->name; ?></span>
                                        <span class="created"><?php echo $comment->created_at->diffForHumans() ?></span>
                                    </div> 
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
                @else
                <p class="no-result-present">{{ trans('labels.backend.blog.blog_non_existent') }}</p>
                @endif
            </div> 
        </div>
        <div class="adblocks">
            @include('frontend.includes.create_pool')
            @include('frontend.includes.adblocks') 
            @include('frontend.includes.hockeypool_news')
        </div>
        @include('frontend.includes.ad_small_block')
    </div>
</div>
@stop
@section('after-scripts-end')
{!! Html::script(elixir('js/blog.js')) !!}
<script type="text/javascript" src="http://development.luminogurus.com/mysask/js/jquery.sharrre.min.js"></script>
@stop