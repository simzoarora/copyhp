@extends('frontend.layouts.frontend')   
@section('after-styles-end')
{!! Html::style(elixir('css/my_account.css')) !!}
@stop 
@section('content')
<div class="container">
    <div class="scorer-loader-logo">
        {{ Html::image('img/site-loader.png', 'Hockeypool loader') }}
    </div>
    <div class="row" id="pool--content">
        <div class="history--container">
            <p class="pool--main-heading">{{trans('auth.my_account.my_pools')}}</p>
            <!--select-pool-->
            <div class="select__pools">
                <ul class="list-inline">
                    <li class="active">
                        <a href="#">{{trans('auth.my_account.current_pools')}}</a>
                        <span class="bottom-shadow"></span>
                    </li>
                    <li>
                        <a href="#">{{trans('auth.my_account.past_pools')}}</a>
                        <span class="bottom-shadow"></span>
                    </li>
                </ul>
            </div>
            <!--select-pool-ends-->
            <!--current-pool-->
            <div class="history--content current--pool">

                <div id="pool-right-header-outer"></div>

                <p class="no-result-present" id="no-pools-present">
                    {{trans('auth.my_account.no_pools.text1')}}
                    <a href="{{ route('pool.create') }}">{{trans('auth.my_account.no_pools.text2')}}</a>
                </p>

                <div id="pool-pagination"></div>
            </div>
            <!--current-pool-ends-->
            <!--past-pool-->
            <div class="history--content past--pool displaynone">

                <div id="pool-past-outer"></div>

                <p class="no-result-present" id="history-no-result">
                    {{trans('auth.my_account.no_past_pools')}}
                </p>
                
                <div id="pool-pagination"></div>
            </div>
            <!--past-pool-ends-->
            @include('frontend.includes.ad_small_block')

        </div>
        <div class="adblocks">
            @include('frontend.includes.create_pool')
            @include('frontend.includes.adblocks')  
            @include('frontend.includes.hockeypool_news')
        </div>
    </div>
</div>
<script id="pool-pagination-template" type="text/html">
    <% if(lastPage!= 1){ %>
    <div class="post-pagination">
        <div class="pagination-child">
            <% if(currentPage != 1){ %>
            <% if((parseInt(currentPage)-1) != 1){ %>
            <div id="pagination-first" class="pagination-arrows">
                <a href="{{ route('frontend.pool.getUserPools') }}?page=1" id="get-pool-link">
                    <div class="double-arrow"></div>
                </a>
            </div>
            <% } %>
            <a href="{{ route('frontend.pool.getUserPools') }}<%= prevPageUrl %>" id="get-pool-link">
                <div id="pagination-left" class="pagination-arrows"></div>
            </a>
            <% } %>
            <div class="main-pagination">
                <div id="page-list">
                    <% var prevPage = parseInt(currentPage) - 2;
                    if(prevPage > 0){ %>
                    <a href="{{ route('frontend.pool.getUserPools') }}?page=<%= prevPage %>"><%= prevPage %></a>
                    <% } %>
                    <% var prevPage = parseInt(currentPage) - 1;
                    if(prevPage > 0){ %>
                    <a href="{{ route('frontend.pool.getUserPools') }}?page=<%= prevPage %>"><%= prevPage %></a>
                    <% } %>

                    <% for(var i=currentPage; i<=lastPage; i++){ %>
                    <a href="{{ route('frontend.pool.getUserPools') }}?page=<%= i %>"
                       class="<% if(currentPage == i){ %>active<% } %>" ><%= i %></a>
                    <% } %>
                </div>
            </div>
            <% if(currentPage != lastPage){ %>
            <a href="{{ route('frontend.pool.getUserPools') }}<%= nextPageUrl %>" id="get-pool-link">
                <div id="pagination-right" class="pagination-arrows"></div>
            </a>
            <% if(lastPage != (parseInt(currentPage)+1)){ %>
            <div id="pagination-last" class="pagination-arrows">
                <a href="{{ route('frontend.pool.getUserPools') }}?page=<%= lastPage %>" id="get-pool-link">
                    <div class="double-arrow"></div>
                </a>
            </div>
            <% } %>
            <% } %>
        </div>
    </div>
    <% } %>
</script>

<a href="{{ route('frontend.pool.dashboard', 0) }}" id="pool-overview-link" class="hide"></a>
<a href="{{ route('frontend.pool.boxDraft', 0) }}" id="pool-boxdraft-link" class="hide"></a>
<script id="pool-right-header-template" type="text/html">
    <% if(poolComplete == 1){ %>
    <div class="pools--h2h">
        <h4 class="head2head--heading-description">
            <a href="<%= poolLink %>">
                <%= poolName %>
            </a>
        </h4>
        <div class="week">
            <%= poolMatchup.season_competition_type_week.name %>
        </div>
        <div class="upper">
            <div class="right">
                <div class="img-div">
                    <img src="<%= siteLink %>/img/team_logos/<%= poolMatchup.pool_team1.logo %>" alt="<%= poolMatchup.pool_team1.name %>" class="img-responsive"/>
                </div>
                <div class="name-div">
                    <p><%= poolMatchup.pool_team1.name %>
                        <span>
                            <% if(!$.isEmptyObject(poolMatchup.pool_team1.pool_matchup_result)){ %>
                            <%= poolMatchup.pool_team1.pool_matchup_result.total_win %>-<%= poolMatchup.pool_team1.pool_matchup_result.total_loss %>-<%= poolMatchup.pool_team1.pool_matchup_result.total_tie %>
                            <% }else{ %>
                            0-0-0
                            <% } %>
                        </span>
                    </p>
                </div>
                <div class="goal-div">
                    <p>
                        <% if(!$.isEmptyObject(poolMatchup.pool_team1.pool_matchup_result)){ %>
                        <%= (poolMatchup.pool_team1.pool_matchup_result.total_score!=null?poolMatchup.pool_team1.pool_matchup_result.total_score:'0') %>
                        <% }else{ %>
                        0
                        <% } %>
                    </p>
                </div>
            </div>
            <div class="vs">
                <p>{{trans('home.home_logged.versus')}}</p>
            </div>
            <div class="left">
                <div class="img-div">
                    <img src="<%= siteLink %>/img/team_logos/<%= poolMatchup.pool_team2.logo %>" alt="<%= poolMatchup.pool_team2.name %>" class="img-responsive"/>
                </div>
                <div class="name-div">
                    <p><%= poolMatchup.pool_team2.name %>
                        <span>
                            <% if(!$.isEmptyObject(poolMatchup.pool_team2.pool_matchup_result)){ %>
                            <%= poolMatchup.pool_team2.pool_matchup_result.total_win %>-<%= poolMatchup.pool_team2.pool_matchup_result.total_loss %>-<%= poolMatchup.pool_team2.pool_matchup_result.total_tie %>
                            <% }else{ %>
                            0-0-0
                            <% } %>
                        </span>
                    </p>
                </div>
                <div class="goal-div">
                    <p>
                        <% if(!$.isEmptyObject(poolMatchup.pool_team2.pool_matchup_result)){ %>
                        <%= (poolMatchup.pool_team2.pool_matchup_result.total_score!=null?poolMatchup.pool_team2.pool_matchup_result.total_score:'0') %>
                        <% }else{ %>
                        0
                        <% } %>
                    </p> 
                </div>
            </div>
            <% if(typeof poolScoreSettings != 'undefined'){ %>
            <div class='bottom'>
                <% _.each(poolScoreSettings, function(value){ %>
                <div class='width10' id='score-setting-<%= poolMatchup.id %>-<%= value.id %>'>
                    <p><%= value.pool_scoring_field.stat %></p>
                </div>
                <%  }); %> 
            </div>
            <% } %>
        </div>
        <% if(typeof poolScoreSettings != 'undefined'){ %>
        <div class='text-right leading-losing'>
            <span class='green circle'></span>
            <span>{{ trans('custom.leading') }}</span>
            <span class='gray circle'></span>
            <span>{{ trans('custom.tied') }}</span>
            <span class='maroon circle'></span>
            <span>{{ trans('custom.losing') }}</span>
        </div>
        <% } %>
    </div>
    <% } %>
</script>
<script id="pool-right-header-box-template" type="text/html">
    <% if(poolComplete == 1){ %>
    <div class="pools--box">
        <% if(typeof poolToShow != 'undefined' && typeof poolTeamPlayers != 'undefined'){
        if(poolToShow == 2 && $.isEmptyObject(poolTeamPlayers)){ %>
        <div class="upper pool-increate__upper">
            <div class="pool-increate">
                <h4><%= poolName %> <span>(You need to draft your team)</span></h4>

                <% if(!$.isEmptyObject(poolUser)){ %>
                <a href="<%= boxDraftLink %>" class="btn btn-green-line" >
                    Draft Team
                </a>
                <% } %>
            </div>
        </div>
        <% } else{ %>

        <h4 class="head2head--heading-description">
            <a href="<%= poolLink %>">
                <%= poolName %>
            </a>
        </h4>
        <div class="week">
            {{trans('home.home_logged.pool_stats')}}
        </div>
        <div class="pools--box-inner">
            <div class="ranks">
                <%= hpApp.getOrdinal(poolTeamFirst.rank) %>
            </div>
            <% if(!$.isEmptyObject(poolTeamFirst.standard_score)){ %>
            <div class="points">
                <span>
                    <%= parseInt(poolTeamFirst.standard_score.total_score) %>
                </span>
                <span>{{trans('home.home_logged.pool_points')}}</span>
            </div>
            <% } %>
            <div class="stats">
                <table class="table light-grey-table" id="no-more-tables">
                    <thead>
                        <tr>
                            <% _.each(poolScoreSettings, function(value){ %>
                            <th><%= value.pool_scoring_field.stat %></th>
                            <%  }); %> 
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <% _.each(poolScoreSettings, function(value){ %>
                            <td data-title="<%= value.pool_scoring_field.stat %>" id="score-setting-<%= poolTeamFirst.id %>-<%= value.pool_scoring_field_id %>">0</td>
                            <%  }); %> 
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <% } } %>
    </div>
    <% } %>
</script>
@stop 
@section('after-scripts-end')
<script>
    var poolOverviewUrl = '{{route('frontend.pool.getUserPools')}}';
    poolTypes = <?php echo json_encode(config('pool.inverse_type')); ?>,
            poolFormat = <?php echo json_encode(config('pool.inverse_format')); ?>;
</script>
{!! Html::script(elixir('js/my_account.js')) !!}
@stop 