@extends('frontend.layouts.frontend')
@section('after-styles-end')
{!! Html::style(elixir('css/edit_account.css')) !!}
@stop 
@section('content')
<div class="container">
    <div class="row" id="pool--content">
        <div class="account-content">

            <p class="pool--main-heading">{{trans('auth.editAccount.title1')}}</p>
            {{ Form::open(array('route'=> 'auth.edit.accountSubmit', 'method' => 'post','id' => 'account-info-form')) }}
            <div id="alert-box"></div>
            <div class="form-field">
                {{ Form::label('name', trans('auth.editAccount.form_fields.name')) }}
                <div class="input-field">
                    {{ Form::text('name', Auth::user()->name, array('class' => 'field','required')) }}
                </div>
            </div>
            <div class="form-field">
                {{ Form::label('email', trans('auth.editAccount.form_fields.email')) }}
                <div class="input-field">
                    {{ Form::email('email', Auth::user()->email, array('class' => 'field','required')) }}
                </div>
            </div>
            <div class="form-field">
                {{ Form::label('password', trans('auth.editAccount.form_fields.current_password')) }}
                <div class="input-field">
                    {{ Form::password('password', array('class' => 'field','required')) }}
                </div>
            </div>
            <div class="form-field">
                <button class="btn-backgreen submit-btn"><span>{{trans('buttons.general.submit')}}</span> <i class="fa fa-spinner fa-pulse"></i></button>
            </div>
            {{ Form::close() }}

            <p class="pool--main-heading">{{trans('auth.editAccount.title2')}}</p>
            {{ Form::open(array('route'=>'auth.password.update', 'method' => 'post','id' => 'change-password-form')) }}
            <div id="alert-box"></div>
            <div class="form-field">
                {{ Form::label('password', trans('auth.editAccount.form_fields.current_password')) }}
                <div class="input-field">
                    {{ Form::password('old_password', array('class' => 'field','required')) }}
                </div>
            </div>
            <div class="form-field">
                {{ Form::label('new_password', trans('auth.editAccount.form_fields.new_password')) }}
                <div class="input-field">
                    {{ Form::password('password', array('class' => 'field','required')) }}
                </div>
            </div>
            <div class="form-field">
                {{ Form::label('password_confirmation', trans('auth.editAccount.form_fields.retype_password')) }}
                <div class="input-field">
                    {{ Form::password('password_confirmation', array('class' => 'field','required')) }}
                </div>
            </div>
            <div class="form-field">
                <button class="btn-backgreen submit-btn"><span>{{trans('buttons.general.submit')}}</span> <i class="fa fa-spinner fa-pulse"></i></button>
            </div>
            {{ Form::close() }}

            <p class="pool--main-heading">{{trans('auth.editAccount.title3')}}</p>
            {{ Form::open(array('route'=> 'frontend.user.updateEmailNotificationSetting', 'method' => 'post','id' => 'email-settings-form')) }}
            <div id="alert-box"></div>
            <?php
            if (!empty(config('constant.notification_settings'))) {
                $count = 0;
                foreach (config('constant.notification_settings') as $input_name => $notifications) {
                    $checked = FALSE;
                    $email_notifications->each(function($notifications) use($input_name, &$checked) {
                        if ($notifications['type'] == $input_name) {
                            if ($notifications['value'] == 1) {
                                $checked = 'checked';
                            }
                        }
                    });
                    ?>
                    <div class="form-field">
                        <div class="custom-checkbox">
                            <input type="hidden" name="data[{{$count}}][type]" value="{{$input_name}}"/>
                            {{ Form::checkbox('data['.$count++.'][value]',1, null, array('class' => 'field', 'id'=>'squaredThree-'.$input_name, $checked)) }} 
                            <label for="squaredThree-{{$input_name}}"><span>{{trans($notifications)}}</span></label>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
            <div class="form-field">
                <button class="btn-backgreen submit-btn"><span>{{trans('buttons.general.submit')}}</span> <i class="fa fa-spinner fa-pulse"></i></button>
            </div>
            {{ Form::close() }}

        </div>
        <div class="adblocks">
            @include('frontend.includes.create_pool')
            @include('frontend.includes.adblocks') 
            @include('frontend.includes.hockeypool_news')
        </div>
    </div> 
</div>
@stop 
@section('after-scripts-end')
<script>
    var buttons_general = <?php echo json_encode(trans('buttons.general')); ?>;
</script>
{!! Html::script(elixir('js/edit_account.js')) !!}
@stop 