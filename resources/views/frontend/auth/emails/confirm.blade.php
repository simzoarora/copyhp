@include('emails.email_header')
<h4 style="text-transform:capitalize; color:#0F6AAE; font-size:28px; margin-bottom:19px;">{{trans('emails.confirm.title')}}</h4>

<p>
    {{trans('emails.confirm.thank_you')}}
    <a href="{{ route('frontend.home.index') }}" style="text-decoration:none; color:#0F6AAE;">{{ route('frontend.home.index') }}</a>
    {{trans('emails.confirm.text1')}}
</p>
<a href="{{ url('account/confirm/' . $token) }}" target='_blank' style="background:#6db72e; box-shadow:0px 2px 6px 0px rgba(0, 0, 0, 0.4); margin:15px 0 10px 0; border:1px solid #6db72e; border-radius:4px; padding:9px 22px; display:inline-block; text-decoration: none; font-weight:300; outline: none;font-size: 24px;text-align: center;border-bottom:2px solid #438627;color:#fff;text-transform:uppercase;">{{trans('emails.confirm.verify_email_now')}}</a>
<p>{{trans('emails.confirm.text2')}}</p>
<a href="{{ url('account/confirm/' . $token) }}" target='_blank' style="text-decoration: none; color: #0F6AAE;">{{ url('account/confirm/' . $token) }}</a>
@include('emails.email_footer')            