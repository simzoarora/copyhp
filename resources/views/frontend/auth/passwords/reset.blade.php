@extends('frontend.layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="login-form-container">
                <div class="login-register-header clearfix">
                    <h4 class='single-header'>{{ trans('labels.frontend.passwords.reset_password_box_title') }}</h4>
                </div>

                {!! Form::open(['url' => 'password/reset', 'class' => 'login-form clearfix']) !!}

                <input type="hidden" name="token" value="{{ $token }}">

                @include('includes.partials.messages') 
                
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
                
                <div class="form-group">
                    {!! Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email'), 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password'), 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password_confirmation'), 'required']) !!}
                </div>

                <div class="form-group clearfix">
                    <button class="btn btn-primary btn-backgreen" type="submit">
                        {{ trans('labels.frontend.passwords.reset_password_button') }}
                        <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
                    </button>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection