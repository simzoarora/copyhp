@extends('frontend.layouts.master')
@section('after-styles-end')
{!! Html::style(elixir('css/register.css')) !!}
@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="register-form-container">

                <div class="login-register-header clearfix">
                    <div class="login-header">
                        <h4><a href="{{ route('auth.login') }}">{{ trans('labels.frontend.auth.login_box_title') }}</a></h4>
                    </div>
                    <div class="register-header active-header">
                        <h4><a href="#">{{ trans('labels.frontend.auth.register_box_title') }}</a></h4>
                    </div>
                </div>
                {!! Form::open(['route' => 'postregister', 'class' => 'register-form clearfix']) !!}
                <div id="alert-box"></div> 

                <div class="form-group">
                    {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.name'), 'required']) !!}
                    <span class="help-block">{{trans('labels.frontend.auth.name_help_text')}}</span>
                </div><!--form-group-->

                <div class="form-group">
                    {!! Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email'), 'required']) !!}
                    <span class="help-block">{{trans('labels.frontend.auth.email_help_text')}}</span>
                </div><!--form-group-->

                <div class="form-group">
                    {!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password'), 'required']) !!}
                    <span class="help-block">{{trans('labels.frontend.auth.password_help_text')}}</span>
                </div><!--form-group-->

                <div class="form-group">
                    {!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password_confirmation'), 'required']) !!}
                    <span class="help-block">{{trans('labels.frontend.auth.password_confirm_help_text')}}</span>
                </div><!--form-group-->

                <div class="form-group clearfix"> 

                    <div class="checkbox-custom">
                        {!! Form::checkbox('subscribe', 1, true, ['id' => 'subscribe']) !!}
                        <label for="subscribe">
                            <span>{{trans('labels.frontend.auth.subscribe_checkbox')}}</span>
                        </label>
                    </div>
                    <button class="btn btn-primary btn-backgreen" type="submit">
                        {{ trans('labels.frontend.auth.register_button') }}
                        <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
                        <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                    </button>

                </div><!--form-group-->
                {!! Form::close() !!}
            </div><!-- panel -->
        </div><!-- col-md-8 -->
    </div>
</div>
@endsection
@section('after-scripts-end')
{!! Html::script(elixir('js/register.js')) !!}
@stop