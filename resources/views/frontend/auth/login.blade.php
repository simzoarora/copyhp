@extends('frontend.layouts.master')
@section('after-styles-end')
{!! Html::style(elixir('css/login.css')) !!}
@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="login-form-container">

                <div class="login-register-header clearfix">
                    <div class="login-header">
                        <h4><a href="#">{{ trans('labels.frontend.auth.login_box_title') }}</a></h4>
                    </div>
                    <div class="register-header">
                        <h4><a href="{{ route('auth.register') }}">{{ trans('labels.frontend.auth.register_box_title') }}</a></h4>
                    </div>
                </div>

                {!! Form::open(['route' => 'postlogin', 'class' => 'login-form clearfix']) !!}

                <div class="form-group">
                    {!! Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.email'), 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.password'), 'required']) !!}
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4 md-mb30">
                            <div class="checkbox-custom">
                                {!! Form::checkbox('remember', 1, false, ['id' => 'rememberMe']) !!}
                                <label for="rememberMe">
                                    <span>{{ trans('labels.frontend.auth.remember_me') }}</span>
                                </label>
                            </div>
                        </div>

                        <div class="col-md-5 md-mb30">
                            <a class="forgot-password" href="{{ route('auth.password.reset') }}"><i class="fa fa-lock" aria-hidden="true"></i> <span>{!! trans('labels.frontend.passwords.forgot_password') !!}</span></a>
                        </div>

                        <div class="col-md-3 md-mb30">
                            <button class="btn btn-backgreen btn-primary " type="submit">
                                {{ trans('labels.frontend.auth.login_button') }}
                                <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
                                <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div><!-- login-form-container -->
        </div><!-- col-md-8 -->
    </div>
</div>
@endsection
@section('after-scripts-end')
{!! Html::script(elixir('js/login.js')) !!}
@stop