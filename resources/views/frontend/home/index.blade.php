@extends('frontend.layouts.frontend')

@section('content')

@if(Auth::user())
@include('frontend.includes.home_logged')
@endif

@if(Auth::guest())
@include('frontend.includes.home_open')
@endif

@stop
