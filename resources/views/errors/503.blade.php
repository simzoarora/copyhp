<!DOCTYPE html>
<html>
    <head>
        <title>{{ trans('http.503.title') }}</title>
        <link href='https://fonts.googleapis.com/css?family=Titillium+Web:600,400' rel='stylesheet' type='text/css'>

        <style>
            html, body {
                height: 100%;
            }
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: "Titillium Web",sans-serif;
            }
            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            .content {
                text-align: center;
                display: inline-block;
            }
            .title {
                font-size:40px;
                margin-bottom: 40px;
                color: #0F6AAE;
                font-weight: 600;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">{{ trans('http.503.title') }}</div>
            </div>
        </div>
    </body>
</html>