<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{ trans('http.404.title') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href='https://fonts.googleapis.com/css?family=Titillium+Web:600,400' rel='stylesheet' type='text/css'>
        <style>
            * {
                line-height: 1.2;
                margin: 0;
            }
            html {
                color: #888;
                display: table;
                font-family: "Titillium Web",sans-serif;
                height: 100%;
                text-align: center;
                width: 100%;
            }
            body {
                display: table-cell;
                vertical-align: middle;
                margin: 2em auto;
            }
            h1 {
                color: #0F6AAE;
                font-size: 40px;
                font-weight: 600;
                margin-bottom: 12px;
            }
            p {
                margin: 0 auto;
                font-weight: 400;
                color: #516a73;
                font-size: 22px;
            }
            @media only screen and (max-width: 280px) {
                body, p {
                    width: 95%;
                }
                h1 {
                    font-size: 1.5em;
                    margin: 0 0 0.3em;
                }
            }
        </style>
    </head>
    <body>
        <h1>{{ trans('http.404.title') }}</h1>
        <p>{{ trans('http.404.description') }}</p>
    </body>
</html>