@include('emails.email_header')
<h4 style="text-transform:capitalize; color:#0F6AAE; font-size:28px; margin-bottom:19px;">{{trans('emails.draft_kit.header')}}</h4>

<p>
    {{trans('emails.draft_kit.thank_you')}}
</p>
@include('emails.email_footer')            