@include('emails.email_header')
<h4 style="text-transform:capitalize; color:#0F6AAE; font-size:28px; margin-bottom:19px;">{{trans('emails.pool_added.pool_invitation')}}</h4>

<p>
    {{trans('emails.pool_added.text1')}}"<?php echo $pool_name; ?>"{{trans('emails.pool_added.text2')}}
    <a href="{{ route('frontend.home.index') }}" style="text-decoration:none; color:#0F6AAE;">HockeyPool.com</a>
</p>
<a href="{{ route('auth.login') }}" target='_blank' style="background:#6db72e; box-shadow:0px 2px 6px 0px rgba(0, 0, 0, 0.4); margin:15px 0 10px 0; border:1px solid #6db72e; border-radius:4px; padding:9px 22px; display:inline-block; text-decoration: none; font-weight:300; outline: none;font-size: 24px;text-align: center;border-bottom:2px solid #438627;color:#fff;text-transform:uppercase;">{{trans('emails.pool_added.join_now')}}</a>
<p>{{trans('emails.pool_added.link_text')}}</p>
<a href="{{ route('auth.login') }}" target='_blank' style="text-decoration: none; color: #0F6AAE;">{{ route('auth.login') }}</a>
@include('emails.email_footer')            