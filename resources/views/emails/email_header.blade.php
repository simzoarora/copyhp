<style type="text/css">
    @import url('//fonts.googleapis.com/css?family=Titillium+Web:400,700');
    *{
        font-family:'Titillium Web',Arial, sans-serif;
    }
</style>
<div style="max-width:601px; min-width:280px; font-family:'Titillium Web',Arial, sans-serif; margin:auto; color:#4c4b4b;">
    <div style="margin: auto;float: left;">
        <div style="padding: 2px 0 22px 0px;background-color: #0C283E;">
            <a href="#" style="color:#fff;margin:0px 10px 15px 0;font-size: 11px;text-align:right;text-decoration:none;display: block;visibility: hidden;">{{trans('pages.general.view_in_browser')}}</a>
            <img src="{{ URL::asset('img/email_template/footer_logo.png') }}" width="auto" alt="img" style="padding: 0 0px 0 25px;">
        </div>
        <div style="padding: 0 30px 40px 30px;font-size:17px;float:left;border-bottom:1px solid #D2D7DA;width: 100%;box-sizing: border-box;">