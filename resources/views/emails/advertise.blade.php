@include('emails.email_header')
<h4 style="text-transform:capitalize; color:#0F6AAE; font-size:28px; margin-bottom:19px;">{{trans('pages.advertise.mail.title')}}</h4>

<p><b>{{trans('pages.advertise.mail.content.name')}}</b> {{ $data['name'] }}</p>
<p><b>{{trans('pages.advertise.mail.content.email')}}</b> {{ $data['email'] }}</p>
<p><b>{{trans('pages.advertise.mail.content.information')}}</b> {{ $data['information'] }}</p>
@include('emails.email_footer') 