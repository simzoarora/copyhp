@include('emails.email_header')
<h4 style="text-transform:capitalize; color:#0F6AAE; font-size:28px; margin-bottom:19px;">{{ $data['page_type'] }} {{trans('pages.contact.mail.title')}}</h4>

<p><b>{{trans('pages.contact.mail.content.type')}}</b> {{ $data['type'] }}</p>
<p><b>{{trans('pages.contact.mail.content.name')}}</b> {{ $data['name'] }}</p>
<p><b>{{trans('pages.contact.mail.content.email')}}</b> {{ $data['email'] }}</p>
<p><b>{{trans('pages.contact.mail.content.question')}}</b> {{ $data['question'] }}</p>

@include('emails.email_footer') 