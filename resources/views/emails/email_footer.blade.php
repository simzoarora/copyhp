<div style="margin-top: 50px;">
    <p style="margin: 0px;">{{trans('pages.advertise.mail.content.thanks')}}</p>
    <p style="margin:0px 0px 10px 0px;">HockeyPool.com</p>
</div>
</div>
<div style="text-align: center; float:left; width: 100%; padding:20px 0px">
    <div style="margin:auto; text-align:center; display:inline-block; width: 100%; margin-bottom: 15px;">
        <a target='_blank' href="{{ route('frontend.pages.faq') }}" style="color:#4c4b4b;text-decoration:none;padding:0 10px;border-right: 1px solid #4c4b4b;font-size: 14px;display: inline-block;line-height: 11px;vertical-align: middle;">{{trans('pages.general.faq')}}</a>
        <a target='_blank' href="{{ route('frontend.pages.advertise') }}" style="color:#4c4b4b;text-decoration:none;padding:0 10px;border-right: 1px solid #4c4b4b;font-size: 14px;display: inline-block;line-height: 11px;vertical-align: middle;">{{trans('pages.general.advertise')}}</a>
        <a target='_blank' href="{{ route('frontend.pages.contact') }}" style="color:#4c4b4b;text-decoration:none;padding:0 10px;font-size: 14px;display: inline-block;line-height: 11px;vertical-align: middle;">{{trans('pages.general.contact')}}</a>
    </div>
    <div style="margin:auto; text-align:center; display: inline-block;">
        <a target='_blank' href="https://www.facebook.com/hockeypools">
            <div style="background-image: url('{{ URL::asset('img/email_template/email_graphics.png') }}');background-position: 1px -2px;height: 33px;border-radius: 50%; margin-right: 20px; float: left; width: 33px;background-color: #3b5999;"></div>
        </a>
        <a target='_blank' href="https://www.twitter.com/hockeypools">
            <div style="background-image: url('{{ URL::asset('img/email_template/email_graphics.png') }}');background-position: -36px -2px;height: 33px;border-radius: 50%;float: left;width: 33px;background-color: #54abf0;"></div>
        </a>
    </div>
</div>
</div>
</div>