@if (config("analytics.google-analytics") != "UA-XXXXX-X")
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','{!! config("analytics.google-analytics") !!}','auto');ga('send','pageview');
</script>
@endif

<!-- Facebook Pixel Code -->
@if(config("app.env") == "production")
<script>
    !function (f, b, e, v, n, t, s)
    {
        if (f.fbq)
            return;
        n = f.fbq = function () {
            n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq)
            f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1947554978864449');
    fbq('track', 'PageView');
</script>

<noscript>
<img height="1" width="1" src="https://www.facebook.com/tr?id=1947554978864449&ev=PageView &noscript=1"/>
</noscript>
@endif
<!-- End Facebook Pixel Code -->