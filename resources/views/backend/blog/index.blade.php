@extends('backend.layouts.master')
@section ('title', trans('labels.backend.blog.title') . ' | ' . trans('labels.backend.blog.all_blogs_title'))
@section('page-header')
<h1>
    {{ trans('strings.backend.blog.title') }}
    <small>{{ trans('labels.backend.blog.all_blogs_title') }}</small>
</h1>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header pull-right">
                <a href="{{ route('admin.view_blog_add') }}" class="btn btn-primary btn-xs">{{ trans('labels.backend.blog.new_post') }}</a>
            </div>
            <div class="box-body">
                <table id="blogs" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.blog.blog_serial_no') }}</th>
                            <th>{{ trans('labels.backend.blog.blog_title') }}</th>
                            <th>{{ trans('labels.backend.blog.blog_status') }}</th>
                            <th>{{ trans('labels.backend.blog.blog_date') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $serial_no = 1; ?>
                        @forelse($blog_posts as $blog_post)
                        <tr>
                            <td>{{ $serial_no }}</td>
                            <td>{{ $blog_post->title }}</td>
                            <td>
                                @if($blog_post->status == config('constant.blog.status.published'))
                                <span class="label label-success">{{trans('blog.blog.published')}}</span>
                                @else
                                <span class="label label-warning">{{trans('blog.blog.draft')}}</span>
                                @endif
                            </td>
                            <td>{{ date('d-m-Y', strtotime($blog_post->created_at)) }}</td>
                            <td>
                                <a href="{{route('admin.view_edit',$blog_post->id)}}"  class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ trans('buttons.general.crud.edit') }}"></i></a>
                                <a class="btn btn-xs btn-danger delete_blog" data-blogid="{{$blog_post->id}}"><i class="fa fa-trash" data-toggle="modal" data-target="#deleteBlog" data-placement="top" title="" data-original-title="{{ trans('buttons.general.crud.delete') }}"></i></a>
                            </td>
                        </tr>
                        <?php $serial_no++; ?>
                        @empty
                        <tr>
                            <td colspan="5">No Blog Entries.</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade modal-danger" id="deleteBlog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{ trans('labels.backend.blog.delete_blog') }}</h4>
            </div>
            {!! Form::open(['id' => 'delete_blog', 'method' => 'post', 'route' => 'admin.delete_blog' ]) !!}
            <div class="modal-body">
                <p>{{ trans('labels.backend.blog.confirm_delete_blog') }}</p>
                {!! Form::hidden('id',null,['id'=>'hidden_blog_id']) !!}
            </div>
            <div class="modal-footer">
                {!! Form::reset(trans('labels.general.no'),['class'=>'btn btn-default','data-dismiss'=>'modal']) !!}
                {!! Form::submit(trans('labels.general.yes'),['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
$(function () {
    $('.delete_blog').on('click',function(e){
        var blog_id = $(this).data('blogid');
        $('#hidden_blog_id').val(blog_id);
        e.preventDefault();
    });
    $('#blogs').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true 
    });
});
</script>
@endsection