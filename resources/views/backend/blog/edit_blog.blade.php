@extends('backend.layouts.master')

@section ('title', trans('labels.backend.blog.edit_blog') . ' | ' . trans('labels.backend.blog.edit_blog'))

@section('page-header')
<h1>
    {{ trans('strings.backend.blog.title') }}
    <small>{{ trans('labels.backend.blog.edit_blog') }}</small>
</h1>
@endsection
@section('content')
{!! Form::model($blog,['id' => 'edit_blog', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post' ]) !!}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.blog.edit_blog') }}</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        {!! Form::hidden('id') !!}
        @include('backend.blog.blog_post_form')
        <!-- /.box-body -->
    </div>
</div>
<div class="box box-success">
    <div class="box-body">
        <div class="pull-left">
            <a href="{{route('admin.blog')}}" class="btn btn-danger btn-xs">{{ trans('buttons.general.cancel') }}</a>
        </div>
        <div class="pull-right">
            {!! Form::submit(trans('labels.backend.blog.update'),['class' => 'btn btn-primary btn-xs']) !!}
        </div>
        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{!! Form::close() !!}
@endsection

@section('scripts')
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('body');
});
</script> 
@endsection