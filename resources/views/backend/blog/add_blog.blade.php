@extends('backend.layouts.master')

@section ('title', trans('labels.backend.blog.add_new_blog') . ' | ' . trans('labels.backend.blog.all_blogs_title'))

@section('page-header')
<h1>
    {{ trans('strings.backend.blog.title') }}
    <small>{{ trans('labels.backend.blog.add_new_blog') }}</small>
</h1>
@endsection

@section('content')
{!! Form::open(['id' => 'add_blog', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post'  ]) !!}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('labels.backend.blog.add_new_blog') }}</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        @include('backend.blog.blog_post_form')
        <!-- /.box-body -->
    </div>
</div>
<div class="box box-success">
    <div class="box-body">
        <div class="pull-left">
            <a href="{{route('admin.blog')}}" class="btn btn-danger btn-xs">{{ trans('buttons.general.cancel') }}</a>
        </div>
        <div class="pull-right">
            {!! Form::submit(trans('labels.backend.blog.save'),['class' => 'btn btn-primary btn-xs' ]) !!}
        </div>
        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{!! Form::close() !!}
@endsection

@section('scripts')
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('body');

    $('#add_blog').on('submit', function (e) {
        e.preventDefault(e);
        var slug = $('#blog_slug');
        $.ajax({
            url: '/admin/blog/checkslug',
            type: "POST",
            data: {slug: slug.val()},
            dataType: 'json',
            success: function (response) {
                if (response.status == 0) {
                    slug.parents('.form-group').addClass('has-warning');
                } else {
                    slug.parents('.form-group').removeClass('has-warning');
                    $('#add_blog').unbind('submit').submit();
                }
                $('.slug-availabilty').html('<i class="fa fa-bell-o" style="padding-left:10px;"></i> ' + response.message);
            },
            error: function (error) {
            }
        });
    });
});
</script> 
@endsection