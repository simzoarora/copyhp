<div class="box-body">
    <div class="form-group <?php if ($errors->first('title')) echo ' has-error'; ?>">
        {!! Form::label('title',trans('labels.backend.blog.blog_title_input'), ['class' => 'col-lg-2 control-label'])!!}
        <div class="col-lg-10">
            {!! Form::text('title',null,[ 'class' => 'form-control', 'placeholder' => trans('labels.backend.blog.blog_title_input') , 'maxlength' => '75' ]) !!}
            <span class="help-block">{{ $errors->first('title') }}</span>
        </div>
    </div>
    <div class="form-group <?php if ($errors->first('slug')) echo ' has-error'; ?>">
        {!! Form::label('slug',trans('labels.backend.blog.blog_slug_input'), ['class' => 'col-lg-2 control-label'])!!}
        <div class="col-lg-10">
            <?php if(!empty($blog->slug)){ ?>
            {!! Form::text('slug',null,[ 'class' => 'form-control', 'id' => 'blog_slug', 'placeholder' => trans('labels.backend.blog.blog_slug_input') , 'maxlength' => '100', 'disabled' => 'disabled' ]) !!}
            <?php }else{ ?>
            {!! Form::text('slug',null,[ 'class' => 'form-control', 'id' => 'blog_slug', 'placeholder' => trans('labels.backend.blog.blog_slug_input') , 'maxlength' => '100' ]) !!}
            <?php } ?>
            <span class="slug-availabilty help-block"></span>
            <span class="help-block">{{ $errors->first('slug') }}</span>
        </div>
    </div>
    <div class="form-group <?php if ($errors->first('thumbnail_image')) echo ' has-error'; ?>"">
        {!! Form::label('thumbnail_image',trans('labels.backend.blog.thumbnail_image'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10 file">
            <div class="blog-image-upload dropzone" data-width="200" data-height="150" data-save="false" data-button-edit="true" data-editstart="true" data-image="{{ ((isset($blog) && $blog->thumbnail_image) ? URL::asset(config('backend.blog.img_path').$blog->thumbnail_image):'') }}">
                <input type="file" name="thumbnail_image" id="thumbnail_image" <?php echo ((isset($blog) && $blog->thumbnail_image) ? '' : 'required'); ?>/>
                <span class="help-block">{{ $errors->first('thumbnail_image') }}</span>
            </div>
            <?php if(isset($blog)){ ?>
            <input type="hidden" name="thumbnail_image_old" value="<?php echo $blog->thumbnail_image ?>"/>
            <?php } ?>
            <input type="hidden" name="thumbnail_image[name]" <?php ((isset($blog) && $blog->thumbnail_image) ? '' : 'required'); ?>/>
            <input type="hidden" name="thumbnail_image[data]" <?php ((isset($blog) && $blog->thumbnail_image) ? '' : 'required'); ?>/>
        </div>
    </div>
    <div class="form-group <?php if ($errors->first('featured_image')) echo ' has-error'; ?>"">
        {!! Form::label('featured_image',trans('labels.backend.blog.featured_image'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10 file">
            <div class="blog-image-upload2 dropzone" data-width="850" data-height="400" data-save="false" data-button-edit="true" data-editstart="true" data-image="{{ ((isset($blog) && $blog->featured_image) ? URL::asset(config('backend.blog.img_path').$blog->featured_image):'') }}" style="width:765px; height:360px;">
                <input type="file" name="featured_image" id="featured_image" <?php echo ((isset($blog) && $blog->featured_image) ? '' : 'required'); ?>/>
                <span class="help-block">{{ $errors->first('featured_image') }}</span>
            </div>
            <?php if(isset($blog)){ ?>
            <input type="hidden" name="featured_image_old" value="<?php echo $blog->featured_image ?>"/>
            <?php } ?>
            <input type="hidden" name="featured_image[name]" <?php ((isset($blog) && $blog->featured_image) ? '' : 'required'); ?>/>
            <input type="hidden" name="featured_image[data]" <?php ((isset($blog) && $blog->featured_image) ? '' : 'required'); ?>/>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('status', trans('labels.backend.blog.publish'), ['class' => 'col-lg-2 control-label'])!!}
        <div class="col-lg-10 checkbox-inline">
            {!! Form::checkbox('status') !!}
        </div>
    </div>
    <div class="form-group <?php if ($errors->first('body')) echo ' has-error'; ?>">
        {!! Form::label('body', trans('labels.backend.blog.body'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            {!! Form::textarea('body') !!}
            <span class="help-block">{{ $errors->first('body') }}</span>
        </div>
    </div>
</div>

@section('blog-scripts')
<script> 
    $(document).ready(function () {
         
        $('.blog-image-upload').html5imageupload({
            onSave: function (data) {
                var $this = $('.blog-image-upload');
                $this.closest('.file').find('[name="thumbnail_image[name]"]').val(data.name);
                $this.closest('.file').find('[name="thumbnail_image[data]"]').val(data.data);
            }
        });
        $('.blog-image-upload2').html5imageupload({
            onSave: function (data) {
                var $this = $('.blog-image-upload2');
                $this.closest('.file').find('[name="featured_image[name]"]').val(data.name);
                $this.closest('.file').find('[name="featured_image[data]"]').val(data.data);
            }
        });
        
//        var typingTimer;                //timer identifier
//        var doneTypingInterval = 300;  //time in ms
//        var $input = $('#blog_slug');
//        //on keyup, start the countdown
//        $input.on('keyup', function () {
//            clearTimeout(typingTimer);
//            typingTimer = setTimeout(doneTyping, doneTypingInterval);
//        });
//
////on keydown, clear the countdown 
//        $input.on('keydown', function () {
//            clearTimeout(typingTimer);
//        });
//
//        function doneTyping() {
//            var slug = $input.val();
//            //check DB for slug availability
//            $.ajax({
//                url: '/admin/blog/checkslug',
//                type: "POST",
//                data: {slug: slug},
//                dataType: 'json',
//                success: function (response) {
//                    if (response.status == 0) {
//                        $($input).parents('.form-group').addClass('has-warning');
//                    } else {
//                        $($input).parents('.form-group').removeClass('has-warning');
//                    }
//                    $('.slug-availabilty').html('<i class="fa fa-bell-o" style="padding-left:10px;"></i> ' + response.message);
//                },
//                error: function (error) {
//                }
//            });
//        }
    });
</script>
@stop