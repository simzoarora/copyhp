@extends('backend.layouts.master')
@section ('title', trans('labels.backend.preranking.title'))
@section('page-header')
<h1>
    {{ trans('labels.backend.preranking.title') }}
</h1>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header pull-right">
                <!--<a href="#" class="btn btn-primary btn-xs">{{ trans('labels.backend.blog.new_post') }}</a>-->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        @include('includes.partials.messages')
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <select name="season" id="season" class="form-control">
                                <option>{{trans('labels.backend.preranking.select_season')}}</option>
                                @foreach($seasons as $season_id => $season)
                                <option value="{{$season_id}}">{{$season}}</option>
                                @endforeach
                            </select>
                            <span class="season-error label label-danger"></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <a href="" class="btn btn-primary" id="export_btn" class="exportimport" data-seasonid="">
                            {{trans('labels.backend.preranking.export')}} <i class="fa fa-fw fa-download"></i>
                        </a>
                    </div>
                    {{ Form::open(['route'=>'admin.preranking.importRankings','id' => 'import_file_form','class' => 'form-horizontal','method' => 'post', 'files' => 'true'])  }}
                    <div class="col-md-3">
                        {{ Form::file('import_file') }}
                        {{ Form::hidden('season_id',null,['id'=>'hidden_season_id']) }}
                    </div>
                    <div class="col-md-3">
                        {{ Form::submit(trans('labels.backend.preranking.import'),['class'=>'btn btn-warning']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    $(function () {
        var season_id ;
        var season = $('#season'),
                export_btn = $('#export_btn'),
                import_btn = $('#import_btn'),
                hidden_season_id = $('#hidden_season_id');

        season.on('change', function () {
            season_id = season.find(':selected').val();
            export_btn.attr('href','/admin/preranking/exportRankings/'+season_id);
            $('.season-error').text('');
            hidden_season_id.val(season_id);
        });

        // VALIDATE - season should be selected before exporting AND importing
        
        export_btn.on('click',function(){
            if(!$.isNumeric(season.val())){
                export_btn.removeAttr('href');
                $('.season-error').text('Please choose a season!');
            }
        });
        $('#import_file_form').submit(function(e){
            var form = $(this);
            e.preventDefault();
            if(!$.isNumeric(season.val())){
                $('.season-error').text('Please choose a season!');
            }else{
                form.unbind('submit').submit();
            }
        });

    });
</script>
@endsection