@extends('backend.layouts.master')

@section ('title', trans('labels.backend.messages.messages') . ' | ' . trans('labels.backend.messages.create'))

@section('page-header')
<h1>
    {{ trans('labels.backend.messages.messages') }}
    <small>{{ trans('labels.backend.messages.create') }}</small>
</h1>
@endsection



@section('content')
{{ Form::open(['route' => 'admin.messages.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}

<div class="box box-success">

    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                @if (Session::get('flash_success'))
                <div class="alert alert-success">
                    {!! Session::get('flash_success') !!}</div>
                @elseif (Session::get('flash_danger'))
                <div class="alert alert-danger">
                    {!! Session::get('flash_danger') !!}
                </div>
                @endif
            </div>
        </div>
        <div class="form-group">

            {{ Form::label('title', trans('messageboards.form.subject'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('title', null, ['class' => 'form-control grey-field','required','minlength'=>'3','maxlength'=>'60']) }}
                <span class="err-messages"><?php echo $errors->first('title'); ?></span>
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('link', trans('messageboards.form.link'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('link', null, ['class' => 'form-control grey-field','rows' => '1']) }}
                <span class="err-messages"><?php echo $errors->first('link'); ?></span>
            </div>
        </div>
        <div class="form-group">

            {{ Form::label('message', trans('messageboards.form.message_to_user'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('message', null, ['class' => 'form-control grey-field', 'id'=>'message-body','required','minlength'=>'10','maxlength'=>'80']) }} 
                <span class="err-messages"><?php echo $errors->first('message'); ?></span>
            </div>
            <div class="form-group">
                <div class="col-lg-10"></div>
                <div class="col-lg-2"> <h5 id="characterLeft"></h5> </div>
            </div>
        </div>



    </div>


</div><!-- /.box-body -->

<div class="box box-success">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.dashboard', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-sm']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit(trans('buttons.general.send'), ['class' => 'btn btn-success btn-sm']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->

{{ Form::close() }}
@stop
@section('scripts')
<script>
    var maxCharacters = 80;
    $('#characterLeft').text(maxCharacters + ' characters left');
    $('#message-body').keyup(function () {
        var textLength = $(this).val().length;
        if (textLength >= maxCharacters) {
            $('#characterLeft').text('You have reached the limit of ' + maxCharacters + ' characters');
        } else {
            var count = maxCharacters - textLength;
            $('#characterLeft').text(count + ' characters left');
        }
    });
</script>
@endsection