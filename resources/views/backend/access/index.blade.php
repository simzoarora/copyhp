<?php // dd($users); ?>
@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.users.management'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.active') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.access.users.active') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <?php
                        $sort = (app('request')->input('sort') == 'asc')?'desc':'asc';
                        ?>
                        <th> <a href="{{route('admin.access.users.index',['order_by'=>'id','sort'=>$sort])}}">{{ trans('labels.backend.access.users.table.id') }}</a></th>
                        <th><a href="{{route('admin.access.users.index',['order_by'=>'name','sort'=>$sort])}}">{{ trans('labels.backend.access.users.table.name') }}</a></th>
                        <th><a href="{{route('admin.access.users.index',['order_by'=>'email','sort'=>$sort])}}">{{ trans('labels.backend.access.users.table.email') }}</a></th>
                        <th><a href="{{route('admin.access.users.index',['order_by'=>'confirmed','sort'=>$sort])}}">{{ trans('labels.backend.access.users.table.confirmed') }}</a></th>
                        <th>{{ trans('labels.backend.access.users.table.roles') }}</th>
                        <th><a href="{{route('admin.access.users.index',['order_by'=>'last_login','sort'=>$sort])}}">{{ trans('labels.backend.access.users.table.last_login') }}</a></th>
                        <th class="visible-lg"><a href="{{route('admin.access.users.index',['order_by'=>'created_at','sort'=>$sort])}}">{{ trans('labels.backend.access.users.table.created') }}</a></th>
                        <th class="visible-lg"><a href="{{route('admin.access.users.index',['order_by'=>'updated_at','sort'=>$sort])}}">{{ trans('labels.backend.access.users.table.last_updated') }}</a></th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{!! $user->id !!}</td>
                                <td>{!! $user->name !!}</td>
                                <td>{!! link_to("mailto:".$user->email, $user->email) !!}</td>
                                <td>{!! $user->confirmed_label !!}</td>
                                <td>
                                    @if ($user->roles()->count() > 0)
                                        @foreach ($user->roles as $role)
                                            {!! $role->name !!}<br/>
                                        @endforeach
                                    @else
                                        {{ trans('labels.general.none') }}
                                    @endif
                                </td>
                                <td>
                                    {{ ($user->last_login != NULL)?$user->last_login->toDayDateTimeString():"-" }}
                                </td>
                                <td class="visible-lg">{!! $user->created_at->diffForHumans() !!}</td>
                                <td class="visible-lg">{!! $user->updated_at->diffForHumans() !!}</td>
                                <td>{!! $user->action_buttons !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="pull-left">
                {!! $users->total() !!} {{ trans_choice('labels.backend.access.users.table.total', $users->total()) }}
            </div>

            <div class="pull-right">
                {!! $users->render() !!}
            </div>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
