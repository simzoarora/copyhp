<html>
    <head>
        <title>Hockey Schedule </title>
    </head>
    <body>
        <?php
        $rss = simplexml_load_file('http://xml.sportsdirectinc.com/Atom?feed=/nhl/schedules/hockey&apiKey=0995E727-5411-4E8F-B0E6-F6E8A8E524E6');
        $feed = json_decode(json_encode($rss), true);
        echo '<pre>';
//print_r($feed);
//die; 
//print_r($feed['entry']);
        $today_date = date("Y-m-d");
        $datetime = new DateTime('tomorrow');
        $tomarrow_date = $datetime->format('Y-m-d');
//echo $tomarrow_date;
//die;
        $today_feed = array();
        foreach ($feed['entry'] as $value) {
            if (preg_match("/2_days/", $value['title'])) {
//      print_r($value);
                $rss1 = simplexml_load_file($value['id']);
                $feed1 = json_decode(json_encode($rss1), true);
//            print_r($feed1);
                foreach ($feed1['team-sport-content']['league-content']['season-content']['competition'] as $key1 => $value1) {
//                print_r($value1);
                    if ((explode('T', $value1['start-date'])[0]) == $today_date) {
                        $today_feed[$key1]['match_id'] = $value1['id'];
                        $today_feed[$key1]['match_start_date'] = $value1['start-date'];
                        $today_feed[$key1]['home_team_id'] = $value1['home-team-content']['team']['id'];
                        $today_feed[$key1]['home_team_name'] = $value1['home-team-content']['team']['name'];
                        $today_feed[$key1]['away_team_id'] = $value1['away-team-content']['team']['id'];
                        $today_feed[$key1]['away_team_name'] = $value1['away-team-content']['team']['name'];
                        $today_feed[$key1]['competition_type'] = $value1['details']['competition-type'];
                        $today_feed[$key1]['venue_id'] = $value1['details']['venue']['id'];
                        $today_feed[$key1]['venue_name'] = $value1['details']['venue']['name'][0];
                        $today_feed[$key1]['venue_code'] = $value1['details']['venue']['name'][1];
                        $today_feed[$key1]['venue_city'] = $value1['details']['venue']['location']['city'];
                        $today_feed[$key1]['venue_state'] = $value1['details']['venue']['location']['state'];
                        $today_feed[$key1]['venue_country'] = $value1['details']['venue']['location']['country'];
                    } elseif ((explode('T', $value1['start-date'])[0]) == $tomarrow_date) {
                        $tomarrow_feed[$key1]['match_id'] = $value1['id'];
                        $tomarrow_feed[$key1]['match_start_date'] = $value1['start-date'];
                        $tomarrow_feed[$key1]['home_team_id'] = $value1['home-team-content']['team']['id'];
                        $tomarrow_feed[$key1]['home_team_name'] = $value1['home-team-content']['team']['name'];
                        $tomarrow_feed[$key1]['away_team_id'] = $value1['away-team-content']['team']['id'];
                        $tomarrow_feed[$key1]['away_team_name'] = $value1['away-team-content']['team']['name'];
                        $tomarrow_feed[$key1]['competition_type'] = $value1['details']['competition-type'];
                        $tomarrow_feed[$key1]['venue_id'] = $value1['details']['venue']['id'];
                        $tomarrow_feed[$key1]['venue_name'] = $value1['details']['venue']['name'];
//                        $tomarrow_feed[$key1]['venue_code'] = $value1['details']['venue']['name'][1];
                        $tomarrow_feed[$key1]['venue_city'] = $value1['details']['venue']['location']['city'];
                        $tomarrow_feed[$key1]['venue_state'] = $value1['details']['venue']['location']['state'];
                        $tomarrow_feed[$key1]['venue_country'] = $value1['details']['venue']['location']['country'];
                    }
                }
            
            }
        }
        echo '<pre>';
        print_r($today_feed);
        print_r($tomarrow_feed);
        ?>
    </body>
</html>