<?php

use Illuminate\Database\Seeder;

class PoolScoringFieldsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pool_scoring_fields')->delete();
        
        \DB::table('pool_scoring_fields')->insert(array (
            0 => 
            array (
                'id' => '1',
                'stat' => 'G',
                'title' => 'Goals',
                'sort_order' => '1',
                'type' => '3',
            ),
            1 => 
            array (
                'id' => '2',
                'stat' => 'A',
                'title' => 'Assists',
                'sort_order' => '2',
                'type' => '3',
            ),
            2 => 
            array (
                'id' => '3',
                'stat' => '+/-',
                'title' => 'Plus-Minus Rating',
                'sort_order' => '3',
                'type' => '1',
            ),
            3 => 
            array (
                'id' => '4',
                'stat' => 'PPP',
                'title' => 'Power Play Points',
                'sort_order' => '4',
                'type' => '1',
            ),
            4 => 
            array (
                'id' => '5',
                'stat' => 'PPG',
                'title' => 'Power Play Goals',
                'sort_order' => '5',
                'type' => '1',
            ),
            5 => 
            array (
                'id' => '6',
                'stat' => 'PPA',
                'title' => 'Assists on the Powerplay',
                'sort_order' => '6',
                'type' => '1',
            ),
            6 => 
            array (
                'id' => '7',
                'stat' => 'HIT',
                'title' => 'Number of Hits',
                'sort_order' => '10',
                'type' => '1',
            ),
            7 => 
            array (
                'id' => '8',
                'stat' => 'S',
                'title' => 'Shots on Net',
                'sort_order' => '11',
                'type' => '1',
            ),
            8 => 
            array (
                'id' => '9',
                'stat' => 'PIM',
                'title' => 'Penalty Minutes',
                'sort_order' => '12',
                'type' => '1',
            ),
            9 => 
            array (
                'id' => '11',
                'stat' => 'GWG',
                'title' => 'Game Winning Goals',
                'sort_order' => '13',
                'type' => '1',
            ),
            10 => 
            array (
                'id' => '13',
                'stat' => 'GO',
                'title' => 'Overtime Goals',
                'sort_order' => '15',
                'type' => '1',
            ),
            11 => 
            array (
                'id' => '15',
                'stat' => 'BS',
                'title' => 'Number of Shots Blocked ',
                'sort_order' => '17',
                'type' => '1',
            ),
            12 => 
            array (
                'id' => '16',
                'stat' => 'GP',
                'title' => 'Games Played',
                'sort_order' => '18',
                'type' => '1',
            ),
            13 => 
            array (
                'id' => '17',
                'stat' => 'SHFTS',
                'title' => 'Number of Shifts Played',
                'sort_order' => '19',
                'type' => '1',
            ),
            14 => 
            array (
                'id' => '18',
                'stat' => 'FOL',
                'title' => 'Faceoffs Lost',
                'sort_order' => '20',
                'type' => '1',
            ),
            15 => 
            array (
                'id' => '19',
                'stat' => 'FOW',
                'title' => 'Faceoffs Won',
                'sort_order' => '21',
                'type' => '1',
            ),
            16 => 
            array (
                'id' => '20',
                'stat' => 'SOA',
                'title' => 'Number of Shootouts Attempted',
                'sort_order' => '22',
                'type' => '1',
            ),
            17 => 
            array (
                'id' => '21',
                'stat' => 'SOS',
                'title' => 'Number of Shootouts Scored',
                'sort_order' => '23',
                'type' => '1',
            ),
            18 => 
            array (
                'id' => '22',
                'stat' => 'ST',
                'title' => 'Games Started',
                'sort_order' => '1',
                'type' => '2',
            ),
            19 => 
            array (
                'id' => '23',
                'stat' => 'GA',
                'title' => 'Goals Against',
                'sort_order' => '2',
                'type' => '2',
            ),
            20 => 
            array (
                'id' => '24',
                'stat' => 'GAA',
                'title' => 'Goals Against Average',
                'sort_order' => '3',
                'type' => '2',
            ),
            21 => 
            array (
                'id' => '25',
                'stat' => 'W',
                'title' => 'Wins',
                'sort_order' => '4',
                'type' => '2',
            ),
            22 => 
            array (
                'id' => '26',
                'stat' => 'SO',
                'title' => 'Shutouts',
                'sort_order' => '5',
                'type' => '2',
            ),
            23 => 
            array (
                'id' => '28',
                'stat' => 'L',
                'title' => 'Loses',
                'sort_order' => '7',
                'type' => '2',
            ),
            24 => 
            array (
                'id' => '29',
                'stat' => 'OTL',
                'title' => 'Overtime Loss',
                'sort_order' => '8',
                'type' => '2',
            ),
            25 => 
            array (
                'id' => '30',
                'stat' => 'SOL',
                'title' => 'Shootout Loss',
                'sort_order' => '9',
                'type' => '2',
            ),
            26 => 
            array (
                'id' => '31',
                'stat' => 'SA',
                'title' => 'Shots Against',
                'sort_order' => '10',
                'type' => '2',
            ),
            27 => 
            array (
                'id' => '32',
                'stat' => 'SHP',
                'title' => 'Short Handed Points',
                'sort_order' => '7',
                'type' => '1',
            ),
            28 => 
            array (
                'id' => '33',
                'stat' => 'SHG',
                'title' => 'Short Handed Goals',
                'sort_order' => '8',
                'type' => '1',
            ),
            29 => 
            array (
                'id' => '34',
                'stat' => 'SHA',
                'title' => 'Assists while Shorthanded',
                'sort_order' => '9',
                'type' => '1',
            ),
            30 => 
            array (
                'id' => '35',
                'stat' => 'SV%',
                'title' => 'Save Percentage',
                'sort_order' => '11',
                'type' => '2',
            ),
        ));
        
        
    }
}
