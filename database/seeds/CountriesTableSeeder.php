<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('countries')->delete();
        
        \DB::table('countries')->insert(array (
            0 => 
            array (
                'id' => '1',
                'sortname' => 'AF',
                'name' => 'Afghanistan',
                'alpha3' => 'AFG',
            ),
            1 => 
            array (
                'id' => '2',
                'sortname' => 'AL',
                'name' => 'Albania',
                'alpha3' => 'ALB',
            ),
            2 => 
            array (
                'id' => '3',
                'sortname' => 'DZ',
                'name' => 'Algeria',
                'alpha3' => 'DZA',
            ),
            3 => 
            array (
                'id' => '4',
                'sortname' => 'AS',
                'name' => 'American Samoa',
                'alpha3' => 'ASM',
            ),
            4 => 
            array (
                'id' => '5',
                'sortname' => 'AD',
                'name' => 'Andorra',
                'alpha3' => 'AND',
            ),
            5 => 
            array (
                'id' => '6',
                'sortname' => 'AO',
                'name' => 'Angola',
                'alpha3' => 'AGO',
            ),
            6 => 
            array (
                'id' => '7',
                'sortname' => 'AI',
                'name' => 'Anguilla',
                'alpha3' => 'AIA',
            ),
            7 => 
            array (
                'id' => '8',
                'sortname' => 'AQ',
                'name' => 'Antarctica',
                'alpha3' => 'ATA',
            ),
            8 => 
            array (
                'id' => '9',
                'sortname' => 'AG',
                'name' => 'Antigua And Barbuda',
                'alpha3' => 'ATG',
            ),
            9 => 
            array (
                'id' => '10',
                'sortname' => 'AR',
                'name' => 'Argentina',
                'alpha3' => 'ARG',
            ),
            10 => 
            array (
                'id' => '11',
                'sortname' => 'AM',
                'name' => 'Armenia',
                'alpha3' => 'ARM',
            ),
            11 => 
            array (
                'id' => '12',
                'sortname' => 'AW',
                'name' => 'Aruba',
                'alpha3' => 'ABW',
            ),
            12 => 
            array (
                'id' => '13',
                'sortname' => 'AU',
                'name' => 'Australia',
                'alpha3' => 'AUS',
            ),
            13 => 
            array (
                'id' => '14',
                'sortname' => 'AT',
                'name' => 'Austria',
                'alpha3' => 'AUT',
            ),
            14 => 
            array (
                'id' => '15',
                'sortname' => 'AZ',
                'name' => 'Azerbaijan',
                'alpha3' => 'AZE',
            ),
            15 => 
            array (
                'id' => '16',
                'sortname' => 'BS',
                'name' => 'Bahamas The',
                'alpha3' => 'BHS',
            ),
            16 => 
            array (
                'id' => '17',
                'sortname' => 'BH',
                'name' => 'Bahrain',
                'alpha3' => 'BHR',
            ),
            17 => 
            array (
                'id' => '18',
                'sortname' => 'BD',
                'name' => 'Bangladesh',
                'alpha3' => 'BGD',
            ),
            18 => 
            array (
                'id' => '19',
                'sortname' => 'BB',
                'name' => 'Barbados',
                'alpha3' => 'BRB',
            ),
            19 => 
            array (
                'id' => '20',
                'sortname' => 'BY',
                'name' => 'Belarus',
                'alpha3' => 'BLR',
            ),
            20 => 
            array (
                'id' => '21',
                'sortname' => 'BE',
                'name' => 'Belgium',
                'alpha3' => 'BEL',
            ),
            21 => 
            array (
                'id' => '22',
                'sortname' => 'BZ',
                'name' => 'Belize',
                'alpha3' => 'BLZ',
            ),
            22 => 
            array (
                'id' => '23',
                'sortname' => 'BJ',
                'name' => 'Benin',
                'alpha3' => 'BEN',
            ),
            23 => 
            array (
                'id' => '24',
                'sortname' => 'BM',
                'name' => 'Bermuda',
                'alpha3' => 'BMU',
            ),
            24 => 
            array (
                'id' => '25',
                'sortname' => 'BT',
                'name' => 'Bhutan',
                'alpha3' => 'BTN',
            ),
            25 => 
            array (
                'id' => '26',
                'sortname' => 'BO',
                'name' => 'Bolivia',
                'alpha3' => 'BOL',
            ),
            26 => 
            array (
                'id' => '27',
                'sortname' => 'BA',
                'name' => 'Bosnia and Herzegovina',
                'alpha3' => 'BIH',
            ),
            27 => 
            array (
                'id' => '28',
                'sortname' => 'BW',
                'name' => 'Botswana',
                'alpha3' => 'BWA',
            ),
            28 => 
            array (
                'id' => '29',
                'sortname' => 'BV',
                'name' => 'Bouvet Island',
                'alpha3' => 'BVT',
            ),
            29 => 
            array (
                'id' => '30',
                'sortname' => 'BR',
                'name' => 'Brazil',
                'alpha3' => 'BRA',
            ),
            30 => 
            array (
                'id' => '31',
                'sortname' => 'IO',
                'name' => 'British Indian Ocean Territory',
                'alpha3' => 'IOT',
            ),
            31 => 
            array (
                'id' => '32',
                'sortname' => 'BN',
                'name' => 'Brunei',
                'alpha3' => 'BRN',
            ),
            32 => 
            array (
                'id' => '33',
                'sortname' => 'BG',
                'name' => 'Bulgaria',
                'alpha3' => 'BGR',
            ),
            33 => 
            array (
                'id' => '34',
                'sortname' => 'BF',
                'name' => 'Burkina Faso',
                'alpha3' => 'BFA',
            ),
            34 => 
            array (
                'id' => '35',
                'sortname' => 'BI',
                'name' => 'Burundi',
                'alpha3' => 'BDI',
            ),
            35 => 
            array (
                'id' => '36',
                'sortname' => 'KH',
                'name' => 'Cambodia',
                'alpha3' => 'KHM',
            ),
            36 => 
            array (
                'id' => '37',
                'sortname' => 'CM',
                'name' => 'Cameroon',
                'alpha3' => 'CMR',
            ),
            37 => 
            array (
                'id' => '38',
                'sortname' => 'CA',
                'name' => 'Canada',
                'alpha3' => 'CAN',
            ),
            38 => 
            array (
                'id' => '39',
                'sortname' => 'CV',
                'name' => 'Cape Verde',
                'alpha3' => 'CPV',
            ),
            39 => 
            array (
                'id' => '40',
                'sortname' => 'KY',
                'name' => 'Cayman Islands',
                'alpha3' => 'CYM',
            ),
            40 => 
            array (
                'id' => '41',
                'sortname' => 'CF',
                'name' => 'Central African Republic',
                'alpha3' => 'CAF',
            ),
            41 => 
            array (
                'id' => '42',
                'sortname' => 'TD',
                'name' => 'Chad',
                'alpha3' => 'TCD',
            ),
            42 => 
            array (
                'id' => '43',
                'sortname' => 'CL',
                'name' => 'Chile',
                'alpha3' => 'CHL',
            ),
            43 => 
            array (
                'id' => '44',
                'sortname' => 'CN',
                'name' => 'China',
                'alpha3' => 'CHN',
            ),
            44 => 
            array (
                'id' => '45',
                'sortname' => 'CX',
                'name' => 'Christmas Island',
                'alpha3' => 'CXR',
            ),
            45 => 
            array (
                'id' => '46',
                'sortname' => 'CC',
            'name' => 'Cocos (Keeling) Islands',
                'alpha3' => 'CCK',
            ),
            46 => 
            array (
                'id' => '47',
                'sortname' => 'CO',
                'name' => 'Colombia',
                'alpha3' => 'COL',
            ),
            47 => 
            array (
                'id' => '48',
                'sortname' => 'KM',
                'name' => 'Comoros',
                'alpha3' => 'COM',
            ),
            48 => 
            array (
                'id' => '49',
                'sortname' => 'CG',
                'name' => 'Congo',
                'alpha3' => 'COG',
            ),
            49 => 
            array (
                'id' => '50',
                'sortname' => 'CD',
                'name' => 'Congo The Democratic Republic Of The',
                'alpha3' => 'COD',
            ),
            50 => 
            array (
                'id' => '51',
                'sortname' => 'CK',
                'name' => 'Cook Islands',
                'alpha3' => 'COK',
            ),
            51 => 
            array (
                'id' => '52',
                'sortname' => 'CR',
                'name' => 'Costa Rica',
                'alpha3' => 'CRI',
            ),
            52 => 
            array (
                'id' => '53',
                'sortname' => 'CI',
            'name' => 'Cote D\'Ivoire (Ivory Coast)',
                'alpha3' => 'CIV',
            ),
            53 => 
            array (
                'id' => '54',
                'sortname' => 'HR',
            'name' => 'Croatia (Hrvatska)',
                'alpha3' => 'HRV',
            ),
            54 => 
            array (
                'id' => '55',
                'sortname' => 'CU',
                'name' => 'Cuba',
                'alpha3' => 'CUB',
            ),
            55 => 
            array (
                'id' => '56',
                'sortname' => 'CY',
                'name' => 'Cyprus',
                'alpha3' => 'CYP',
            ),
            56 => 
            array (
                'id' => '57',
                'sortname' => 'CZ',
                'name' => 'Czech Republic',
                'alpha3' => 'CZE',
            ),
            57 => 
            array (
                'id' => '58',
                'sortname' => 'DK',
                'name' => 'Denmark',
                'alpha3' => 'DNK',
            ),
            58 => 
            array (
                'id' => '59',
                'sortname' => 'DJ',
                'name' => 'Djibouti',
                'alpha3' => 'DJI',
            ),
            59 => 
            array (
                'id' => '60',
                'sortname' => 'DM',
                'name' => 'Dominica',
                'alpha3' => 'DMA',
            ),
            60 => 
            array (
                'id' => '61',
                'sortname' => 'DO',
                'name' => 'Dominican Republic',
                'alpha3' => 'DOM',
            ),
            61 => 
            array (
                'id' => '62',
                'sortname' => 'TP',
                'name' => 'East Timor',
                'alpha3' => 'TMP',
            ),
            62 => 
            array (
                'id' => '63',
                'sortname' => 'EC',
                'name' => 'Ecuador',
                'alpha3' => 'ECU',
            ),
            63 => 
            array (
                'id' => '64',
                'sortname' => 'EG',
                'name' => 'Egypt',
                'alpha3' => 'EGY',
            ),
            64 => 
            array (
                'id' => '65',
                'sortname' => 'SV',
                'name' => 'El Salvador',
                'alpha3' => 'SLV',
            ),
            65 => 
            array (
                'id' => '66',
                'sortname' => 'GQ',
                'name' => 'Equatorial Guinea',
                'alpha3' => 'GNQ',
            ),
            66 => 
            array (
                'id' => '67',
                'sortname' => 'ER',
                'name' => 'Eritrea',
                'alpha3' => 'ERI',
            ),
            67 => 
            array (
                'id' => '68',
                'sortname' => 'EE',
                'name' => 'Estonia',
                'alpha3' => 'EST',
            ),
            68 => 
            array (
                'id' => '69',
                'sortname' => 'ET',
                'name' => 'Ethiopia',
                'alpha3' => 'ETH',
            ),
            69 => 
            array (
                'id' => '70',
                'sortname' => 'XA',
                'name' => 'External Territories of Australia',
                'alpha3' => '',
            ),
            70 => 
            array (
                'id' => '71',
                'sortname' => 'FK',
                'name' => 'Falkland Islands',
                'alpha3' => 'FLK',
            ),
            71 => 
            array (
                'id' => '72',
                'sortname' => 'FO',
                'name' => 'Faroe Islands',
                'alpha3' => 'FRO',
            ),
            72 => 
            array (
                'id' => '73',
                'sortname' => 'FJ',
                'name' => 'Fiji Islands',
                'alpha3' => 'FJI',
            ),
            73 => 
            array (
                'id' => '74',
                'sortname' => 'FI',
                'name' => 'Finland',
                'alpha3' => 'FIN',
            ),
            74 => 
            array (
                'id' => '75',
                'sortname' => 'FR',
                'name' => 'France',
                'alpha3' => 'FRA',
            ),
            75 => 
            array (
                'id' => '76',
                'sortname' => 'GF',
                'name' => 'French Guiana',
                'alpha3' => 'GUF',
            ),
            76 => 
            array (
                'id' => '77',
                'sortname' => 'PF',
                'name' => 'French Polynesia',
                'alpha3' => 'PYF',
            ),
            77 => 
            array (
                'id' => '78',
                'sortname' => 'TF',
                'name' => 'French Southern Territories',
                'alpha3' => 'ATF',
            ),
            78 => 
            array (
                'id' => '79',
                'sortname' => 'GA',
                'name' => 'Gabon',
                'alpha3' => 'GAB',
            ),
            79 => 
            array (
                'id' => '80',
                'sortname' => 'GM',
                'name' => 'Gambia The',
                'alpha3' => 'GMB',
            ),
            80 => 
            array (
                'id' => '81',
                'sortname' => 'GE',
                'name' => 'Georgia',
                'alpha3' => 'GEO',
            ),
            81 => 
            array (
                'id' => '82',
                'sortname' => 'DE',
                'name' => 'Germany',
                'alpha3' => 'DEU',
            ),
            82 => 
            array (
                'id' => '83',
                'sortname' => 'GH',
                'name' => 'Ghana',
                'alpha3' => 'GHA',
            ),
            83 => 
            array (
                'id' => '84',
                'sortname' => 'GI',
                'name' => 'Gibraltar',
                'alpha3' => 'GIB',
            ),
            84 => 
            array (
                'id' => '85',
                'sortname' => 'GR',
                'name' => 'Greece',
                'alpha3' => 'GRC',
            ),
            85 => 
            array (
                'id' => '86',
                'sortname' => 'GL',
                'name' => 'Greenland',
                'alpha3' => 'GRL',
            ),
            86 => 
            array (
                'id' => '87',
                'sortname' => 'GD',
                'name' => 'Grenada',
                'alpha3' => 'GRD',
            ),
            87 => 
            array (
                'id' => '88',
                'sortname' => 'GP',
                'name' => 'Guadeloupe',
                'alpha3' => 'GLP',
            ),
            88 => 
            array (
                'id' => '89',
                'sortname' => 'GU',
                'name' => 'Guam',
                'alpha3' => 'GUM',
            ),
            89 => 
            array (
                'id' => '90',
                'sortname' => 'GT',
                'name' => 'Guatemala',
                'alpha3' => 'GTM',
            ),
            90 => 
            array (
                'id' => '91',
                'sortname' => 'XU',
                'name' => 'Guernsey and Alderney',
                'alpha3' => 'CGY',
            ),
            91 => 
            array (
                'id' => '92',
                'sortname' => 'GN',
                'name' => 'Guinea',
                'alpha3' => 'GIN',
            ),
            92 => 
            array (
                'id' => '93',
                'sortname' => 'GW',
                'name' => 'Guinea-Bissau',
                'alpha3' => 'GNB',
            ),
            93 => 
            array (
                'id' => '94',
                'sortname' => 'GY',
                'name' => 'Guyana',
                'alpha3' => 'GUY',
            ),
            94 => 
            array (
                'id' => '95',
                'sortname' => 'HT',
                'name' => 'Haiti',
                'alpha3' => 'HTI',
            ),
            95 => 
            array (
                'id' => '96',
                'sortname' => 'HM',
                'name' => 'Heard and McDonald Islands',
                'alpha3' => 'HMD',
            ),
            96 => 
            array (
                'id' => '97',
                'sortname' => 'HN',
                'name' => 'Honduras',
                'alpha3' => 'HND',
            ),
            97 => 
            array (
                'id' => '98',
                'sortname' => 'HK',
                'name' => 'Hong Kong S.A.R.',
                'alpha3' => 'HKG',
            ),
            98 => 
            array (
                'id' => '99',
                'sortname' => 'HU',
                'name' => 'Hungary',
                'alpha3' => 'HUN',
            ),
            99 => 
            array (
                'id' => '100',
                'sortname' => 'IS',
                'name' => 'Iceland',
                'alpha3' => 'ISL',
            ),
            100 => 
            array (
                'id' => '101',
                'sortname' => 'IN',
                'name' => 'India',
                'alpha3' => 'IND',
            ),
            101 => 
            array (
                'id' => '102',
                'sortname' => 'ID',
                'name' => 'Indonesia',
                'alpha3' => 'IDN',
            ),
            102 => 
            array (
                'id' => '103',
                'sortname' => 'IR',
                'name' => 'Iran',
                'alpha3' => 'IRN',
            ),
            103 => 
            array (
                'id' => '104',
                'sortname' => 'IQ',
                'name' => 'Iraq',
                'alpha3' => 'IRQ',
            ),
            104 => 
            array (
                'id' => '105',
                'sortname' => 'IE',
                'name' => 'Ireland',
                'alpha3' => 'IRL',
            ),
            105 => 
            array (
                'id' => '106',
                'sortname' => 'IL',
                'name' => 'Israel',
                'alpha3' => 'ISR',
            ),
            106 => 
            array (
                'id' => '107',
                'sortname' => 'IT',
                'name' => 'Italy',
                'alpha3' => 'ITA',
            ),
            107 => 
            array (
                'id' => '108',
                'sortname' => 'JM',
                'name' => 'Jamaica',
                'alpha3' => 'JAM',
            ),
            108 => 
            array (
                'id' => '109',
                'sortname' => 'JP',
                'name' => 'Japan',
                'alpha3' => 'JPN',
            ),
            109 => 
            array (
                'id' => '110',
                'sortname' => 'XJ',
                'name' => 'Jersey',
                'alpha3' => 'JEY',
            ),
            110 => 
            array (
                'id' => '111',
                'sortname' => 'JO',
                'name' => 'Jordan',
                'alpha3' => 'JOR',
            ),
            111 => 
            array (
                'id' => '112',
                'sortname' => 'KZ',
                'name' => 'Kazakhstan',
                'alpha3' => 'KAZ',
            ),
            112 => 
            array (
                'id' => '113',
                'sortname' => 'KE',
                'name' => 'Kenya',
                'alpha3' => 'KEN',
            ),
            113 => 
            array (
                'id' => '114',
                'sortname' => 'KI',
                'name' => 'Kiribati',
                'alpha3' => 'KIR',
            ),
            114 => 
            array (
                'id' => '115',
                'sortname' => 'KP',
                'name' => 'Korea North',
                'alpha3' => 'PRK',
            ),
            115 => 
            array (
                'id' => '116',
                'sortname' => 'KR',
                'name' => 'Korea South',
                'alpha3' => 'KOR',
            ),
            116 => 
            array (
                'id' => '117',
                'sortname' => 'KW',
                'name' => 'Kuwait',
                'alpha3' => 'KWT',
            ),
            117 => 
            array (
                'id' => '118',
                'sortname' => 'KG',
                'name' => 'Kyrgyzstan',
                'alpha3' => 'KGZ',
            ),
            118 => 
            array (
                'id' => '119',
                'sortname' => 'LA',
                'name' => 'Laos',
                'alpha3' => 'LAO',
            ),
            119 => 
            array (
                'id' => '120',
                'sortname' => 'LV',
                'name' => 'Latvia',
                'alpha3' => 'LVA',
            ),
            120 => 
            array (
                'id' => '121',
                'sortname' => 'LB',
                'name' => 'Lebanon',
                'alpha3' => 'LBN',
            ),
            121 => 
            array (
                'id' => '122',
                'sortname' => 'LS',
                'name' => 'Lesotho',
                'alpha3' => 'LSO',
            ),
            122 => 
            array (
                'id' => '123',
                'sortname' => 'LR',
                'name' => 'Liberia',
                'alpha3' => 'LBR',
            ),
            123 => 
            array (
                'id' => '124',
                'sortname' => 'LY',
                'name' => 'Libya',
                'alpha3' => 'LBY',
            ),
            124 => 
            array (
                'id' => '125',
                'sortname' => 'LI',
                'name' => 'Liechtenstein',
                'alpha3' => 'LIE',
            ),
            125 => 
            array (
                'id' => '126',
                'sortname' => 'LT',
                'name' => 'Lithuania',
                'alpha3' => 'LTU',
            ),
            126 => 
            array (
                'id' => '127',
                'sortname' => 'LU',
                'name' => 'Luxembourg',
                'alpha3' => 'LUX',
            ),
            127 => 
            array (
                'id' => '128',
                'sortname' => 'MO',
                'name' => 'Macau S.A.R.',
                'alpha3' => 'MAC',
            ),
            128 => 
            array (
                'id' => '129',
                'sortname' => 'MK',
                'name' => 'Macedonia',
                'alpha3' => 'MKD',
            ),
            129 => 
            array (
                'id' => '130',
                'sortname' => 'MG',
                'name' => 'Madagascar',
                'alpha3' => 'MDG',
            ),
            130 => 
            array (
                'id' => '131',
                'sortname' => 'MW',
                'name' => 'Malawi',
                'alpha3' => 'MWI',
            ),
            131 => 
            array (
                'id' => '132',
                'sortname' => 'MY',
                'name' => 'Malaysia',
                'alpha3' => 'MYS',
            ),
            132 => 
            array (
                'id' => '133',
                'sortname' => 'MV',
                'name' => 'Maldives',
                'alpha3' => 'MDV',
            ),
            133 => 
            array (
                'id' => '134',
                'sortname' => 'ML',
                'name' => 'Mali',
                'alpha3' => 'MLI',
            ),
            134 => 
            array (
                'id' => '135',
                'sortname' => 'MT',
                'name' => 'Malta',
                'alpha3' => 'MLT',
            ),
            135 => 
            array (
                'id' => '136',
                'sortname' => 'XM',
            'name' => 'Man (Isle of)',
                'alpha3' => 'IMN',
            ),
            136 => 
            array (
                'id' => '137',
                'sortname' => 'MH',
                'name' => 'Marshall Islands',
                'alpha3' => 'MHL',
            ),
            137 => 
            array (
                'id' => '138',
                'sortname' => 'MQ',
                'name' => 'Martinique',
                'alpha3' => 'MTQ',
            ),
            138 => 
            array (
                'id' => '139',
                'sortname' => 'MR',
                'name' => 'Mauritania',
                'alpha3' => 'MRT',
            ),
            139 => 
            array (
                'id' => '140',
                'sortname' => 'MU',
                'name' => 'Mauritius',
                'alpha3' => 'MUS',
            ),
            140 => 
            array (
                'id' => '141',
                'sortname' => 'YT',
                'name' => 'Mayotte',
                'alpha3' => 'MYT',
            ),
            141 => 
            array (
                'id' => '142',
                'sortname' => 'MX',
                'name' => 'Mexico',
                'alpha3' => 'MEX',
            ),
            142 => 
            array (
                'id' => '143',
                'sortname' => 'FM',
                'name' => 'Micronesia',
                'alpha3' => 'FSM',
            ),
            143 => 
            array (
                'id' => '144',
                'sortname' => 'MD',
                'name' => 'Moldova',
                'alpha3' => 'MDA',
            ),
            144 => 
            array (
                'id' => '145',
                'sortname' => 'MC',
                'name' => 'Monaco',
                'alpha3' => 'MCO',
            ),
            145 => 
            array (
                'id' => '146',
                'sortname' => 'MN',
                'name' => 'Mongolia',
                'alpha3' => 'MNG',
            ),
            146 => 
            array (
                'id' => '147',
                'sortname' => 'MS',
                'name' => 'Montserrat',
                'alpha3' => 'MSR',
            ),
            147 => 
            array (
                'id' => '148',
                'sortname' => 'MA',
                'name' => 'Morocco',
                'alpha3' => 'MAR',
            ),
            148 => 
            array (
                'id' => '149',
                'sortname' => 'MZ',
                'name' => 'Mozambique',
                'alpha3' => 'MOZ',
            ),
            149 => 
            array (
                'id' => '150',
                'sortname' => 'MM',
                'name' => 'Myanmar',
                'alpha3' => 'MMR',
            ),
            150 => 
            array (
                'id' => '151',
                'sortname' => 'NA',
                'name' => 'Namibia',
                'alpha3' => 'NAM',
            ),
            151 => 
            array (
                'id' => '152',
                'sortname' => 'NR',
                'name' => 'Nauru',
                'alpha3' => 'NRU',
            ),
            152 => 
            array (
                'id' => '153',
                'sortname' => 'NP',
                'name' => 'Nepal',
                'alpha3' => 'NPL',
            ),
            153 => 
            array (
                'id' => '154',
                'sortname' => 'AN',
                'name' => 'Netherlands Antilles',
                'alpha3' => 'ANT',
            ),
            154 => 
            array (
                'id' => '155',
                'sortname' => 'NL',
                'name' => 'Netherlands The',
                'alpha3' => 'NLD',
            ),
            155 => 
            array (
                'id' => '156',
                'sortname' => 'NC',
                'name' => 'New Caledonia',
                'alpha3' => 'NCL',
            ),
            156 => 
            array (
                'id' => '157',
                'sortname' => 'NZ',
                'name' => 'New Zealand',
                'alpha3' => 'NZL',
            ),
            157 => 
            array (
                'id' => '158',
                'sortname' => 'NI',
                'name' => 'Nicaragua',
                'alpha3' => 'NIC',
            ),
            158 => 
            array (
                'id' => '159',
                'sortname' => 'NE',
                'name' => 'Niger',
                'alpha3' => 'NER',
            ),
            159 => 
            array (
                'id' => '160',
                'sortname' => 'NG',
                'name' => 'Nigeria',
                'alpha3' => 'NGA',
            ),
            160 => 
            array (
                'id' => '161',
                'sortname' => 'NU',
                'name' => 'Niue',
                'alpha3' => 'NIU',
            ),
            161 => 
            array (
                'id' => '162',
                'sortname' => 'NF',
                'name' => 'Norfolk Island',
                'alpha3' => 'NFK',
            ),
            162 => 
            array (
                'id' => '163',
                'sortname' => 'MP',
                'name' => 'Northern Mariana Islands',
                'alpha3' => 'MNP',
            ),
            163 => 
            array (
                'id' => '164',
                'sortname' => 'NO',
                'name' => 'Norway',
                'alpha3' => 'NOR',
            ),
            164 => 
            array (
                'id' => '165',
                'sortname' => 'OM',
                'name' => 'Oman',
                'alpha3' => 'OMN',
            ),
            165 => 
            array (
                'id' => '166',
                'sortname' => 'PK',
                'name' => 'Pakistan',
                'alpha3' => 'PAK',
            ),
            166 => 
            array (
                'id' => '167',
                'sortname' => 'PW',
                'name' => 'Palau',
                'alpha3' => 'PLW',
            ),
            167 => 
            array (
                'id' => '168',
                'sortname' => 'PS',
                'name' => 'Palestinian Territory Occupied',
                'alpha3' => 'PSE',
            ),
            168 => 
            array (
                'id' => '169',
                'sortname' => 'PA',
                'name' => 'Panama',
                'alpha3' => 'PAN',
            ),
            169 => 
            array (
                'id' => '170',
                'sortname' => 'PG',
                'name' => 'Papua new Guinea',
                'alpha3' => 'PNG',
            ),
            170 => 
            array (
                'id' => '171',
                'sortname' => 'PY',
                'name' => 'Paraguay',
                'alpha3' => 'PRY',
            ),
            171 => 
            array (
                'id' => '172',
                'sortname' => 'PE',
                'name' => 'Peru',
                'alpha3' => 'PER',
            ),
            172 => 
            array (
                'id' => '173',
                'sortname' => 'PH',
                'name' => 'Philippines',
                'alpha3' => 'PHL',
            ),
            173 => 
            array (
                'id' => '174',
                'sortname' => 'PN',
                'name' => 'Pitcairn Island',
                'alpha3' => 'PCN',
            ),
            174 => 
            array (
                'id' => '175',
                'sortname' => 'PL',
                'name' => 'Poland',
                'alpha3' => 'POL',
            ),
            175 => 
            array (
                'id' => '176',
                'sortname' => 'PT',
                'name' => 'Portugal',
                'alpha3' => 'PRT',
            ),
            176 => 
            array (
                'id' => '177',
                'sortname' => 'PR',
                'name' => 'Puerto Rico',
                'alpha3' => 'PRI',
            ),
            177 => 
            array (
                'id' => '178',
                'sortname' => 'QA',
                'name' => 'Qatar',
                'alpha3' => 'QAT',
            ),
            178 => 
            array (
                'id' => '179',
                'sortname' => 'RE',
                'name' => 'Reunion',
                'alpha3' => 'REU',
            ),
            179 => 
            array (
                'id' => '180',
                'sortname' => 'RO',
                'name' => 'Romania',
                'alpha3' => 'ROU',
            ),
            180 => 
            array (
                'id' => '181',
                'sortname' => 'RU',
                'name' => 'Russia',
                'alpha3' => 'RUS',
            ),
            181 => 
            array (
                'id' => '182',
                'sortname' => 'RW',
                'name' => 'Rwanda',
                'alpha3' => 'RWA',
            ),
            182 => 
            array (
                'id' => '183',
                'sortname' => 'SH',
                'name' => 'Saint Helena',
                'alpha3' => 'SHN',
            ),
            183 => 
            array (
                'id' => '184',
                'sortname' => 'KN',
                'name' => 'Saint Kitts And Nevis',
                'alpha3' => 'KNA',
            ),
            184 => 
            array (
                'id' => '185',
                'sortname' => 'LC',
                'name' => 'Saint Lucia',
                'alpha3' => 'LCA',
            ),
            185 => 
            array (
                'id' => '186',
                'sortname' => 'PM',
                'name' => 'Saint Pierre and Miquelon',
                'alpha3' => 'SPM',
            ),
            186 => 
            array (
                'id' => '187',
                'sortname' => 'VC',
                'name' => 'Saint Vincent And The Grenadines',
                'alpha3' => 'VCT',
            ),
            187 => 
            array (
                'id' => '188',
                'sortname' => 'WS',
                'name' => 'Samoa',
                'alpha3' => 'WSM',
            ),
            188 => 
            array (
                'id' => '189',
                'sortname' => 'SM',
                'name' => 'San Marino',
                'alpha3' => 'SMR',
            ),
            189 => 
            array (
                'id' => '190',
                'sortname' => 'ST',
                'name' => 'Sao Tome and Principe',
                'alpha3' => 'STP',
            ),
            190 => 
            array (
                'id' => '191',
                'sortname' => 'SA',
                'name' => 'Saudi Arabia',
                'alpha3' => 'SAU',
            ),
            191 => 
            array (
                'id' => '192',
                'sortname' => 'SN',
                'name' => 'Senegal',
                'alpha3' => 'SEN',
            ),
            192 => 
            array (
                'id' => '193',
                'sortname' => 'RS',
                'name' => 'Serbia',
                'alpha3' => 'SRB',
            ),
            193 => 
            array (
                'id' => '194',
                'sortname' => 'SC',
                'name' => 'Seychelles',
                'alpha3' => 'SYC',
            ),
            194 => 
            array (
                'id' => '195',
                'sortname' => 'SL',
                'name' => 'Sierra Leone',
                'alpha3' => 'SLE',
            ),
            195 => 
            array (
                'id' => '196',
                'sortname' => 'SG',
                'name' => 'Singapore',
                'alpha3' => 'SGP',
            ),
            196 => 
            array (
                'id' => '197',
                'sortname' => 'SK',
                'name' => 'Slovakia',
                'alpha3' => 'SVK',
            ),
            197 => 
            array (
                'id' => '198',
                'sortname' => 'SI',
                'name' => 'Slovenia',
                'alpha3' => 'SVN',
            ),
            198 => 
            array (
                'id' => '199',
                'sortname' => 'XG',
                'name' => 'Smaller Territories of the UK',
                'alpha3' => '',
            ),
            199 => 
            array (
                'id' => '200',
                'sortname' => 'SB',
                'name' => 'Solomon Islands',
                'alpha3' => 'SLB',
            ),
            200 => 
            array (
                'id' => '201',
                'sortname' => 'SO',
                'name' => 'Somalia',
                'alpha3' => 'SOM',
            ),
            201 => 
            array (
                'id' => '202',
                'sortname' => 'ZA',
                'name' => 'South Africa',
                'alpha3' => 'ZAF',
            ),
            202 => 
            array (
                'id' => '203',
                'sortname' => 'GS',
                'name' => 'South Georgia',
                'alpha3' => 'SGS',
            ),
            203 => 
            array (
                'id' => '204',
                'sortname' => 'SS',
                'name' => 'South Sudan',
                'alpha3' => 'SSD',
            ),
            204 => 
            array (
                'id' => '205',
                'sortname' => 'ES',
                'name' => 'Spain',
                'alpha3' => 'ESP',
            ),
            205 => 
            array (
                'id' => '206',
                'sortname' => 'LK',
                'name' => 'Sri Lanka',
                'alpha3' => 'LKA',
            ),
            206 => 
            array (
                'id' => '207',
                'sortname' => 'SD',
                'name' => 'Sudan',
                'alpha3' => 'SDN',
            ),
            207 => 
            array (
                'id' => '208',
                'sortname' => 'SR',
                'name' => 'Suriname',
                'alpha3' => 'SUR',
            ),
            208 => 
            array (
                'id' => '209',
                'sortname' => 'SJ',
                'name' => 'Svalbard And Jan Mayen Islands',
                'alpha3' => 'SJM',
            ),
            209 => 
            array (
                'id' => '210',
                'sortname' => 'SZ',
                'name' => 'Swaziland',
                'alpha3' => 'SWZ',
            ),
            210 => 
            array (
                'id' => '211',
                'sortname' => 'SE',
                'name' => 'Sweden',
                'alpha3' => 'SWE',
            ),
            211 => 
            array (
                'id' => '212',
                'sortname' => 'CH',
                'name' => 'Switzerland',
                'alpha3' => 'CHE',
            ),
            212 => 
            array (
                'id' => '213',
                'sortname' => 'SY',
                'name' => 'Syria',
                'alpha3' => 'SYR',
            ),
            213 => 
            array (
                'id' => '214',
                'sortname' => 'TW',
                'name' => 'Taiwan',
                'alpha3' => 'TWN',
            ),
            214 => 
            array (
                'id' => '215',
                'sortname' => 'TJ',
                'name' => 'Tajikistan',
                'alpha3' => 'TJK',
            ),
            215 => 
            array (
                'id' => '216',
                'sortname' => 'TZ',
                'name' => 'Tanzania',
                'alpha3' => 'TZA',
            ),
            216 => 
            array (
                'id' => '217',
                'sortname' => 'TH',
                'name' => 'Thailand',
                'alpha3' => 'THA',
            ),
            217 => 
            array (
                'id' => '218',
                'sortname' => 'TG',
                'name' => 'Togo',
                'alpha3' => 'TGO',
            ),
            218 => 
            array (
                'id' => '219',
                'sortname' => 'TK',
                'name' => 'Tokelau',
                'alpha3' => 'TKL',
            ),
            219 => 
            array (
                'id' => '220',
                'sortname' => 'TO',
                'name' => 'Tonga',
                'alpha3' => 'TON',
            ),
            220 => 
            array (
                'id' => '221',
                'sortname' => 'TT',
                'name' => 'Trinidad And Tobago',
                'alpha3' => 'TTO',
            ),
            221 => 
            array (
                'id' => '222',
                'sortname' => 'TN',
                'name' => 'Tunisia',
                'alpha3' => 'TUN',
            ),
            222 => 
            array (
                'id' => '223',
                'sortname' => 'TR',
                'name' => 'Turkey',
                'alpha3' => 'TUR',
            ),
            223 => 
            array (
                'id' => '224',
                'sortname' => 'TM',
                'name' => 'Turkmenistan',
                'alpha3' => 'TKM',
            ),
            224 => 
            array (
                'id' => '225',
                'sortname' => 'TC',
                'name' => 'Turks And Caicos Islands',
                'alpha3' => 'TCA',
            ),
            225 => 
            array (
                'id' => '226',
                'sortname' => 'TV',
                'name' => 'Tuvalu',
                'alpha3' => 'TUV',
            ),
            226 => 
            array (
                'id' => '227',
                'sortname' => 'UG',
                'name' => 'Uganda',
                'alpha3' => 'UGA',
            ),
            227 => 
            array (
                'id' => '228',
                'sortname' => 'UA',
                'name' => 'Ukraine',
                'alpha3' => 'UKR',
            ),
            228 => 
            array (
                'id' => '229',
                'sortname' => 'AE',
                'name' => 'United Arab Emirates',
                'alpha3' => 'ARE',
            ),
            229 => 
            array (
                'id' => '230',
                'sortname' => 'GB',
                'name' => 'United Kingdom',
                'alpha3' => 'GBR',
            ),
            230 => 
            array (
                'id' => '231',
                'sortname' => 'US',
                'name' => 'United States',
                'alpha3' => 'USA',
            ),
            231 => 
            array (
                'id' => '232',
                'sortname' => 'UM',
                'name' => 'United States Minor Outlying Islands',
                'alpha3' => 'UMI',
            ),
            232 => 
            array (
                'id' => '233',
                'sortname' => 'UY',
                'name' => 'Uruguay',
                'alpha3' => 'URY',
            ),
            233 => 
            array (
                'id' => '234',
                'sortname' => 'UZ',
                'name' => 'Uzbekistan',
                'alpha3' => 'UZB',
            ),
            234 => 
            array (
                'id' => '235',
                'sortname' => 'VU',
                'name' => 'Vanuatu',
                'alpha3' => 'VUT',
            ),
            235 => 
            array (
                'id' => '236',
                'sortname' => 'VA',
            'name' => 'Vatican City State (Holy See)',
                'alpha3' => 'VAT',
            ),
            236 => 
            array (
                'id' => '237',
                'sortname' => 'VE',
                'name' => 'Venezuela',
                'alpha3' => 'VEN',
            ),
            237 => 
            array (
                'id' => '238',
                'sortname' => 'VN',
                'name' => 'Vietnam',
                'alpha3' => 'VNM',
            ),
            238 => 
            array (
                'id' => '239',
                'sortname' => 'VG',
            'name' => 'Virgin Islands (British)',
                'alpha3' => 'VGB',
            ),
            239 => 
            array (
                'id' => '240',
                'sortname' => 'VI',
            'name' => 'Virgin Islands (US)',
                'alpha3' => 'VIR',
            ),
            240 => 
            array (
                'id' => '241',
                'sortname' => 'WF',
                'name' => 'Wallis And Futuna Islands',
                'alpha3' => 'WLF',
            ),
            241 => 
            array (
                'id' => '242',
                'sortname' => 'EH',
                'name' => 'Western Sahara',
                'alpha3' => 'ESH',
            ),
            242 => 
            array (
                'id' => '243',
                'sortname' => 'YE',
                'name' => 'Yemen',
                'alpha3' => 'YEM',
            ),
            243 => 
            array (
                'id' => '244',
                'sortname' => 'YU',
                'name' => 'Yugoslavia',
                'alpha3' => 'YUG',
            ),
            244 => 
            array (
                'id' => '245',
                'sortname' => 'ZM',
                'name' => 'Zambia',
                'alpha3' => 'ZMB',
            ),
            245 => 
            array (
                'id' => '246',
                'sortname' => 'ZW',
                'name' => 'Zimbabwe',
                'alpha3' => 'ZWE',
            ),
        ));
        
        
    }
}
