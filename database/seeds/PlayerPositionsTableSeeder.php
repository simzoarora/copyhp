<?php

use Illuminate\Database\Seeder;

class PlayerPositionsTableSeeder extends Seeder {

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run() {


        \DB::table('player_positions')->delete();

        \DB::table('player_positions')->insert(array(
            0 => array(
                'id' => '1',
                'short_name' => 'C',
                'name' => 'Center',
                'sort_order' => 3
            ),
            1 => array(
                'id' => '2',
                'short_name' => 'RW',
                'name' => 'Right Wing',
                'sort_order' => 5
            ),
            2 => array(
                'id' => '3',
                'short_name' => 'D',
                'name' => 'Defenseman',
                'sort_order' => 6
            ),
            3 => array(
                'id' => '4',
                'short_name' => 'G',
                'name' => 'Goalie',
                'sort_order' => 7
            ),
            4 => array(
                'id' => '5',
                'short_name' => 'LW',
                'name' => 'Left Wing',
                'sort_order' => 4
            ),
            5 => array(
                'id' => '6',
                'short_name' => 'S',
                'name' => 'Skater',
                'sort_order' => 1
            ),
            6 => array(
                'id' => '7',
                'short_name' => 'F',
                'name' => 'Forward',
                'sort_order' => 2
            ),
            7 => array(
                'id' => '8',
                'short_name' => 'BN',
                'name' => 'Bench',
                'sort_order' => 8
            ),
        ));
    }

}
