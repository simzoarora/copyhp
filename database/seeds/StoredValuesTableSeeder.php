<?php

use Illuminate\Database\Seeder;

class StoredValuesTableSeeder extends Seeder {

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run() {


        \DB::table('stored_values')->delete();

        \DB::table('stored_values')->insert(array(
            0 => array(
                'key' => 'box_score_update_time',
                'value' => '2015-08-29 23:54:23',
            ),
            1 => array(
                'key' => 'injury_feed_update_time',
                'value' => '2015-08-29 23:54:23',
            ),
            2 => array(
                'key' => 'standings_feed_update_time',
                'value' => '2005-08-29 23:54:23',
            ),
            3 => array(
                'key' => 'team_feed_update_time',
                'value' => '2005-08-29 23:54:23',
            ),
            4 => array(
                'key' => 'player_feed_update_time',
                'value' => '2005-08-29 23:54:23',
            ),
            5 => array(
                'key' => 'player_stats_feed_update_time',
                'value' => '2005-08-29 23:54:23',
            ),
            6 => array(
                'key' => 'current_season_pool_score_last_pool',
                'value' => 0,
            ),
            7 => array(
                'key' => 'player_draft_feed_update_time',
                'value' => '2005-08-29 23:54:23',
            ),
        ));
    }

}
