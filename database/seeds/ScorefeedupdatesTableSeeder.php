<?php

use Illuminate\Database\Seeder;

class ScorefeedupdatesTableSeeder extends Seeder {

    /** 
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = [
            [
                'livescores' => '2016-05-24 04:40:44',
                'schedules' => '2016-05-24 04:40:44',
                'results' => '2016-05-24 04:40:44'
            ]
        ];

        DB::table('scorefeedupdates')->insert($data);
    }

}
