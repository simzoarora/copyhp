<?php

use Illuminate\Database\Seeder;

class PoolPaymentItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pool_payment_items')->delete();
        
        \DB::table('pool_payment_items')->insert(array (
            0 => 
            array (
                'id' => '1',
                'sku' => 'hp-premium/hp-beta',
                'cart' => '.premium-beta',
                'trial_period' => '7',
                'price' => '14.97',
                'actual_price' => '29.95',
                'name' => 'Premium Pool',
                'is_active' => '1',
                'is_premium' => '1',
            ),
            1 => 
            array (
                'id' => '2',
                'sku' => 'hp-premium/hp-full',
                'cart' => '',
                'trial_period' => '7',
                'price' => '29.95',
                'actual_price' => '29.95',
                'name' => 'Premium Pool',
                'is_active' => '0',
                'is_premium' => '1',
            ),
            2 => 
            array (
                'id' => '3',
                'sku' => 'hp-ad/hp-ad-beta',
                'cart' => '.ad-beta',
                'trial_period' => '7',
                'price' => '9.97',
                'actual_price' => '19.95',
                'name' => 'Ad Supported Pool',
                'is_active' => '1',
                'is_premium' => '0',
            ),
            3 => 
            array (
                'id' => '4',
                'sku' => 'hp-ad/hp-ad-full',
                'cart' => '',
                'trial_period' => '7',
                'price' => '19.95',
                'actual_price' => '19.95',
                'name' => 'Ad Supported Pool',
                'is_active' => '0',
                'is_premium' => '0',
            ),
        ));
        
        
    }
}
