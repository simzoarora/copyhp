<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMessageBoardRepliesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('message_board_replies', function(Blueprint $table)
		{
			$table->foreign('message_board_id', 'message_board_replies_ibfk_1')->references('id')->on('message_boards')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_id', 'message_board_replies_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('message_board_replies', function(Blueprint $table)
		{
			$table->dropForeign('message_board_replies_ibfk_1');
			$table->dropForeign('message_board_replies_ibfk_2');
		});
	}

}
