<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLiveDraftTeamSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('live_draft_team_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_team_id')->index('pool_team_id');
			$table->boolean('automatic_selection')->default(0)->comment('1 - select player automatically after 10 seconds');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('live_draft_team_settings');
	}

}
