<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTradeManagementColumnToPoolH2hSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pool_h2h_settings', function (Blueprint $table) {
            $table->tinyInteger('trade_management')->default(0)->comment('0 - None, 1 - Commissioner Decides, 2 - League Votes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pool_h2h_settings', function (Blueprint $table) {
             $table->dropColumn('trade_management');
        });
    }
}
