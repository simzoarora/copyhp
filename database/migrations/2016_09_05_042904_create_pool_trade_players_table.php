<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolTradePlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_trade_players', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_trade_id')->index('pool_trade_id');
			$table->integer('pool_team_player_id')->index('pool_team_player_id');
			$table->boolean('type')->default(0)->comment('0 - trade players, 1 - drop players');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_trade_players');
	}

}
