<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShootoutShotsAgainstCompetitionSkater extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('competition_skater_stats', function (Blueprint $table) {
            $table->integer('shootout_shots_against')->nullable();
            $table->integer('shootout_goals_against')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('competition_skater_stats', function (Blueprint $table) {
            $table->dropColumn('shootout_shots_against');
            $table->dropColumn('shootout_goals_against');
        });
    }
}