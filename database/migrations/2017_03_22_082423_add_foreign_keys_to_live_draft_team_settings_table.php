<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLiveDraftTeamSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('live_draft_team_settings', function(Blueprint $table)
		{
			$table->foreign('pool_team_id', 'live_draft_team_settings_ibfk_1')->references('id')->on('pool_teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('live_draft_team_settings', function(Blueprint $table)
		{
			$table->dropForeign('live_draft_team_settings_ibfk_1');
		});
	}

}
