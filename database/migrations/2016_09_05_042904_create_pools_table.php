<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pools', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('season_id')->index('season_id');
			$table->boolean('season_type')->default(1)->comment('1 - Regular Season, 2 - Playoff');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->boolean('pool_type')->comment('1 - headtohead, 2 - box, 3 - standard');
			$table->string('name', 60);
			$table->string('invite_key', 60)->nullable();
			$table->string('logo')->default('pool_dashboard_default.jpg');
			$table->boolean('is_active')->default(0)->comment('would be linked through payments');
			$table->boolean('is_completed')->default(0)->comment('used to check if creator completed pool');
			$table->dateTime('active_till')->nullable()->comment('used for pool trial purpose');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pools');
	}

}
