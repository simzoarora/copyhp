<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompetitionPenaltiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('competition_penalties', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('competition_id')->unsigned()->index('competition_id');
			$table->integer('api_id');
			$table->integer('period');
			$table->string('penalty_time', 15);
			$table->integer('player_season_id')->index('player_season_id');
			$table->string('penalty_type', 50);
			$table->integer('penalty_duration');
			$table->boolean('team_penalty');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('competition_penalties');
	}

}
