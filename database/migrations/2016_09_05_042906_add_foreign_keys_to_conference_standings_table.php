<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConferenceStandingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('conference_standings', function(Blueprint $table)
		{
			$table->foreign('season_id', 'conference_standings_ibfk_1')->references('id')->on('seasons')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('team_id', 'conference_standings_ibfk_2')->references('id')->on('teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('conference_id', 'conference_standings_ibfk_3')->references('id')->on('conferences')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('conference_standings', function(Blueprint $table)
		{
			$table->dropForeign('conference_standings_ibfk_1');
			$table->dropForeign('conference_standings_ibfk_2');
			$table->dropForeign('conference_standings_ibfk_3');
		});
	}

}
