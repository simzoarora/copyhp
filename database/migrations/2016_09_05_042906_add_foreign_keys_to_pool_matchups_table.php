<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolMatchupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_matchups', function(Blueprint $table)
		{
			$table->foreign('pool_id', 'pool_matchups_ibfk_1')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('team1', 'pool_matchups_ibfk_2')->references('id')->on('pool_teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('team2', 'pool_matchups_ibfk_3')->references('id')->on('pool_teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('season_competition_type_week_id', 'pool_matchups_ibfk_4')->references('id')->on('season_competition_type_weeks')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_matchups', function(Blueprint $table)
		{
			$table->dropForeign('pool_matchups_ibfk_1');
			$table->dropForeign('pool_matchups_ibfk_2');
			$table->dropForeign('pool_matchups_ibfk_3');
			$table->dropForeign('pool_matchups_ibfk_4');
		});
	}

}
