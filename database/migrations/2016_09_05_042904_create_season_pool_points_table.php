<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSeasonPoolPointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('season_pool_points', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('season_id')->index('season_id');
			$table->integer('pool_id')->index('pool_id');
			$table->integer('player_id')->index('player_id');
			$table->float('total_points', 10, 3);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('season_pool_points');
	}

}
