<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolMatchupResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_matchup_results', function(Blueprint $table)
		{
			$table->foreign('pool_matchup_id', 'pool_matchup_results_ibfk_1')->references('id')->on('pool_matchups')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_team_id', 'pool_matchup_results_ibfk_2')->references('id')->on('pool_teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_matchup_results', function(Blueprint $table)
		{
			$table->dropForeign('pool_matchup_results_ibfk_1');
			$table->dropForeign('pool_matchup_results_ibfk_2');
		});
	}

}
