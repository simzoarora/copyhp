<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentToStatusColumnToPoolTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pool_trades', function (Blueprint $table) {
            $table->boolean('status')->default(0)->comment('0 - No Action, 1 - Accepted, 2 - Rejected, 3 - Auto Rejected, 4 - Cancelled By Parent, 5 - Cancelled By Commissioner')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pool_trades', function (Blueprint $table) {
            //
        });
    }
}
