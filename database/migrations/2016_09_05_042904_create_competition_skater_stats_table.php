<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompetitionSkaterStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('competition_skater_stats', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('player_season_id')->index('player_season_id');
			$table->integer('competition_id')->unsigned()->index('competition_id');
			$table->integer('assists')->nullable();
			$table->integer('assists_power_play')->nullable();
			$table->integer('assists_short_handed')->nullable();
			$table->integer('blocked_shots')->nullable();
			$table->integer('faceoffs_lost')->nullable();
			$table->integer('faceoffs_won')->nullable();
			$table->integer('game_winning_goals')->nullable();
			$table->integer('games_played')->nullable();
			$table->integer('goals')->nullable();
			$table->integer('goals_overtime')->nullable();
			$table->integer('goals_period_1')->nullable();
			$table->integer('goals_period_2')->nullable();
			$table->integer('goals_period_3')->nullable();
			$table->integer('goals_short_handed')->nullable();
			$table->integer('penalty_minutes')->nullable();
			$table->integer('plus_minus')->nullable();
			$table->integer('hits')->nullable();
			$table->integer('points_short_handed')->nullable();
			$table->integer('goals_power_play')->nullable();
			$table->integer('shootouts_attempted')->nullable();
			$table->integer('shifts')->nullable();
			$table->integer('points_power_play')->nullable();
			$table->integer('time_on_ice_power_play_secs')->nullable();
			$table->integer('time_on_ice_even_strength_secs')->nullable();
			$table->integer('time_on_ice_secs')->nullable();
			$table->integer('shots')->nullable();
			$table->timestamps();
			$table->integer('shootouts_scored')->nullable();
			$table->integer('time_on_ice_short_handed_secs')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('competition_skater_stats');
	}

}
