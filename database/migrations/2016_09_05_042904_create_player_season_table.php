<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayerSeasonTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('player_season', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('player_id')->index('player_id');
			$table->integer('season_id')->index('season_id');
			$table->integer('team_id')->index('team_id');
			$table->integer('number')->nullable();
			$table->integer('player_position_id')->unsigned()->index('player_position_id');
			$table->boolean('is_active');
			$table->boolean('is_draft')->default(0)->comment('used to store if player was on draft round');
			$table->integer('salary')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_season');
	}

}
