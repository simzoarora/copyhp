<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLiveDraftQueuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('live_draft_queues', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_team_id')->index('pool_team_id');
			$table->integer('player_season_id')->index('player_season_id');
			$table->integer('sort_order');
			$table->boolean('is_used')->default(0);
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('live_draft_queues');
	}

}
