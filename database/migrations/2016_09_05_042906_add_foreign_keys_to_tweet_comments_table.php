<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTweetCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tweet_comments', function(Blueprint $table)
		{
			$table->foreign('tweet_id')->references('id')->on('twitter_feeds')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tweet_comments', function(Blueprint $table)
		{
			$table->dropForeign('tweet_comments_tweet_id_foreign');
			$table->dropForeign('tweet_comments_user_id_foreign');
		});
	}

}
