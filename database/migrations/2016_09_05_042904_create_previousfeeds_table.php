<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePreviousfeedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('previousfeeds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('match_id', 100);
			$table->string('match_start_date', 35);
			$table->string('match_status', 30);
			$table->string('match_complition_type', 30);
			$table->string('home_team_id', 35);
			$table->integer('home_team_score');
			$table->string('away_team_id', 35);
			$table->integer('away_team_score');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('previousfeeds');
	}

}
