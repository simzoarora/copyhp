<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolTeamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_teams', function(Blueprint $table)
		{
			$table->foreign('pool_id', 'pool_teams_ibfk_1')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_id', 'pool_teams_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_teams', function(Blueprint $table)
		{
			$table->dropForeign('pool_teams_ibfk_1');
			$table->dropForeign('pool_teams_ibfk_2');
		});
	}

}
