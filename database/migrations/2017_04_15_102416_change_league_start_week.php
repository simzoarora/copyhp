<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLeagueStartWeek extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pool_box_settings', function (Blueprint $table) {
             $table->integer('league_start_week')->change();
        });
        Schema::table('pool_standard_settings', function (Blueprint $table) {
             $table->integer('league_start_week')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
