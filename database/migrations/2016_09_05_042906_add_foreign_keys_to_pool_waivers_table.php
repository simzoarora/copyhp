<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolWaiversTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_waivers', function(Blueprint $table)
		{
			$table->foreign('pool_id', 'pool_waivers_ibfk_1')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_season_id', 'pool_waivers_ibfk_2')->references('id')->on('player_season')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_waivers', function(Blueprint $table)
		{
			$table->dropForeign('pool_waivers_ibfk_1');
			$table->dropForeign('pool_waivers_ibfk_2');
		});
	}

}
