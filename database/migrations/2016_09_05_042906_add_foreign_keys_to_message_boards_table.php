<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMessageBoardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('message_boards', function(Blueprint $table)
		{
			$table->foreign('pool_id', 'message_boards_ibfk_2')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_id', 'message_boards_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('message_boards', function(Blueprint $table)
		{
			$table->dropForeign('message_boards_ibfk_2');
			$table->dropForeign('message_boards_ibfk_1');
		});
	}

}
