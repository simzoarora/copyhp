<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolStandardRottoResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_standard_rotto_results', function(Blueprint $table)
		{
			$table->foreign('pool_team_id', 'pool_standard_rotto_results_ibfk_1')->references('id')->on('pool_teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_scoring_field_id', 'pool_standard_rotto_results_ibfk_2')->references('id')->on('pool_scoring_fields')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_standard_rotto_results', function(Blueprint $table)
		{
			$table->dropForeign('pool_standard_rotto_results_ibfk_1');
			$table->dropForeign('pool_standard_rotto_results_ibfk_2');
		});
	}

}
