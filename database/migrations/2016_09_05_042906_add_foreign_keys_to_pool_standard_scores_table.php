<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolStandardScoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_standard_scores', function(Blueprint $table)
		{
			$table->foreign('pool_team_id', 'pool_standard_scores_ibfk_1')->references('id')->on('pool_teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_score_setting_id', 'pool_standard_scores_ibfk_2')->references('id')->on('pool_score_settings')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_team_player_id', 'pool_standard_scores_ibfk_3')->references('id')->on('pool_team_players')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('competition_id', 'pool_standard_scores_ibfk_4')->references('id')->on('competitions')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_standard_scores', function(Blueprint $table)
		{
			$table->dropForeign('pool_standard_scores_ibfk_1');
			$table->dropForeign('pool_standard_scores_ibfk_2');
			$table->dropForeign('pool_standard_scores_ibfk_3');
			$table->dropForeign('pool_standard_scores_ibfk_4');
		});
	}

}
