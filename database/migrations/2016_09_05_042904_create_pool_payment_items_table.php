<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolPaymentItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_payment_items', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('sku', 200);
			$table->string('cart', 200);
			$table->integer('trial_period')->default(0);
			$table->float('price', 6);
			$table->float('actual_price', 6);
			$table->string('name', 100);
			$table->boolean('is_active')->default(0);
			$table->boolean('is_premium');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_payment_items');
	}

}
