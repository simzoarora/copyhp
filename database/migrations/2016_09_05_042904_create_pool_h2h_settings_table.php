<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolH2hSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_h2h_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('playoff_format')->default(0)->comment('0 - no playoff, 1 - playoff format');
			$table->boolean('pool_format')->default(1)->comment('1 - H2H category, 2 - H2H weekly win, 3 - H2H weekly win daily roster');
			$table->boolean('lock_eliminated_teams')->default(1)->comment('0 - No, 1 - Yes');
			$table->boolean('max_teams')->default(0)->comment('0 - No Maximum');
			$table->boolean('league_start_week')->default(1);
			$table->boolean('min_goalie_appearance');
			$table->boolean('draft_order');
			$table->smallInteger('max_acquisition_team')->default(-1)->comment('-1 -> No max, 0 - no acquisition allowed');
			$table->boolean('max_acquisition_week')->default(6)->comment('-1 -> no max, 0 - no acquisition allowed');
			$table->boolean('max_trades_season')->default(-1)->comment('-1 -> no max, 0 - no trades allowed');
			$table->date('trade_end_date');
			$table->boolean('trading_draft_pick')->comment('0 - no, 1 - yes');
			$table->boolean('trade_reject_time')->default(2)->comment('-1 -> no protests allowed, 1, 2,3,4,5,6,7');
			$table->boolean('waiver_time');
			$table->boolean('waiver_mode')->default(0)->comment('0 - None, 1 - Standard');
			$table->boolean('playoff_teams_number')->comment('4, 6, 8');
			$table->boolean('playoff_schedule')->default(0)->comment('1 - last week, 2 - 2nd last week');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_h2h_settings');
	}

}
