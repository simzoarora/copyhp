<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolBoxSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_box_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('league_start_week')->default(1);
			$table->boolean('options_per_box');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_box_settings');
	}

}
