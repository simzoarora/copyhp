<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSeasonCompetitionTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('season_competition_types', function(Blueprint $table)
		{
			$table->foreign('season_id', 'season_competition_types_ibfk_1')->references('id')->on('seasons')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('season_competition_types', function(Blueprint $table)
		{
			$table->dropForeign('season_competition_types_ibfk_1');
		});
	}

}
