<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompetitionGoalieStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('competition_goalie_stats', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('competition_id')->unsigned()->index('competition_id');
			$table->integer('player_season_id')->index('player_season_id');
			$table->integer('games_started')->nullable();
			$table->integer('goals_against')->nullable();
			$table->integer('goals_against_average')->nullable();
			$table->integer('goals_overtime_against')->nullable();
			$table->integer('goals_period_1_against')->nullable();
			$table->integer('goals_period_2_against')->nullable();
			$table->integer('goals_period_3_against')->nullable();
			$table->integer('losses')->nullable();
			$table->integer('overtime_losses')->nullable();
			$table->integer('shootout_losses')->nullable();
			$table->integer('shots_against')->nullable();
			$table->integer('shots_overtime_against')->nullable();
			$table->integer('shots_period_1_against')->nullable();
			$table->integer('shots_period_2_against')->nullable();
			$table->integer('shutouts')->nullable();
			$table->integer('shots_period_3_against')->nullable();
			$table->integer('ties')->nullable();
			$table->integer('wins')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('competition_goalie_stats');
	}

}
