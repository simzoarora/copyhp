<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToPoolMatchups extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('pool_matchups', function (Blueprint $table) {
            $table->boolean('type')->comment('0 - regular matches, 1 - playoff matches')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('pool_matchups', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }

}
