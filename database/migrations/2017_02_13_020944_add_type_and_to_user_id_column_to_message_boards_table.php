<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeAndToUserIdColumnToMessageBoardsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('message_boards', function (Blueprint $table) {
            $table->tinyInteger('type')->default(0)->after('description')->comment('0 - Public Message, 1 - Private Message');
            $table->integer('to_user_id')->unsigned()->nullable()->after('type')->comment('Used in case of private messages.');
            $table->foreign('to_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('message_boards', function (Blueprint $table) {
            $table->dropForeign('message_boards_to_user_id_foreign');
            $table->dropColumn(['type', 'to_user_id']);
        });
    }

}
