<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompetitionGoalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('competition_goals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('competition_id')->unsigned()->index('competition_id');
			$table->integer('api_id')->unsigned();
			$table->string('period', 2);
			$table->integer('scorer_id')->index('scorer_id');
			$table->integer('first_assisted_by')->nullable()->index('first_assisted_by');
			$table->integer('second_assisted_by')->nullable()->index('second_assisted_by');
			$table->string('goal_time', 15);
			$table->string('scoring_type', 5);
			$table->dateTime('goal_timestamp');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('competition_goals');
	}

}
