<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolWaiverClaimsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_waiver_claims', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_waiver_id')->index('pool_waiver_id');
			$table->integer('pool_team_id')->index('pool_team_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_waiver_claims');
	}

}
