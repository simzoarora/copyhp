<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompetitionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('competitions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('season_id')->index('season_id');
			$table->integer('venue_id')->nullable()->index('venue_id');
			$table->integer('api_id');
			$table->dateTime('match_start_date');
			$table->string('match_status', 30);
			$table->string('match_complition_type', 30)->nullable();
			$table->integer('home_team_id')->index('home_team_id');
			$table->integer('home_team_score')->nullable();
			$table->integer('away_team_id')->index('away_team_id');
			$table->integer('away_team_score')->nullable();
			$table->boolean('type')->comment('1 - Past, 2 - current, 3 - scheduled, 4 - Yesterday');
			$table->boolean('competition_type')->default(1)->comment('0 - Pre Season, 1 - Regular Season, 2 - playoffs');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('competitions');
	}

}
