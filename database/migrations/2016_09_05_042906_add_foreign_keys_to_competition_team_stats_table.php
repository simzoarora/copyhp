<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCompetitionTeamStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('competition_team_stats', function(Blueprint $table)
		{
			$table->foreign('competition_id', 'competition_team_stats_ibfk_1')->references('id')->on('competitions')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('team_id', 'competition_team_stats_ibfk_2')->references('id')->on('teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('competition_team_stats', function(Blueprint $table)
		{
			$table->dropForeign('competition_team_stats_ibfk_1');
			$table->dropForeign('competition_team_stats_ibfk_2');
		});
	}

}
