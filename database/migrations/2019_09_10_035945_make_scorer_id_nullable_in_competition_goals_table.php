<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeScorerIdNullableInCompetitionGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('competition_goals', function (Blueprint $table) {
            $table->integer('scorer_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('competition_goals', function (Blueprint $table) {
            $table->integer('scorer_id')->nullable(false)->change();
        });
    }
}
