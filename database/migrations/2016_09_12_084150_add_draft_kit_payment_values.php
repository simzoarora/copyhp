<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\StoredValue;

class AddDraftKitPaymentValues extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        StoredValue::firstOrCreate([
            'key' => 'draft_kit_sku',
            'value' => 'draft-kit/draft-kit-offer',
        ]);
        StoredValue::firstOrCreate([
            'key' => 'draft_kit_price',
            'value' => '9.97',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        StoredValue::where('key', 'draft_kit_sku')->delete();
        StoredValue::where('key', 'draft_kit_price')->delete();
    }

}
