<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeSkaterIdNullableInCompetitionShootoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('competition_shootouts', function (Blueprint $table) {
            $table->integer('skater_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('competition_shootouts', function (Blueprint $table) {
            $table->integer('skater_id')->nullable(false)->change();
        });
    }
}
