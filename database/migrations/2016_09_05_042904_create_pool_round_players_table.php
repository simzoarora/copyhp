<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolRoundPlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_round_players', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_round_id')->index('pool_round_id');
			$table->integer('player_season_id')->index('player_season_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_round_players');
	}

}
