<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompetitionTeamStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('competition_team_stats', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('competition_id')->unsigned()->index('competition_id');
			$table->integer('team_id')->index('team_id');
			$table->integer('total_play_time')->nullable();
			$table->integer('player_assists')->nullable();
			$table->integer('player_plus_minus')->nullable();
			$table->integer('player_game_winning_goals')->nullable();
			$table->integer('player_points')->nullable();
			$table->integer('player_hits')->nullable();
			$table->integer('player_blocked_shots')->nullable();
			$table->integer('player_time_on_ice_power_play_secs')->nullable();
			$table->integer('player_time_on_ice_short_handed_secs')->nullable();
			$table->integer('goals')->nullable();
			$table->integer('goals_period_2')->nullable();
			$table->integer('goals_overtime_1')->nullable();
			$table->integer('goals_overtime')->nullable();
			$table->integer('shots')->nullable();
			$table->integer('shots_overtime')->nullable();
			$table->integer('shots_period_3')->nullable();
			$table->integer('penalties')->nullable();
			$table->integer('shots_period_1')->nullable();
			$table->integer('power_plays')->nullable();
			$table->integer('faceoff_total_losses')->nullable();
			$table->integer('penalty_minutes')->nullable();
			$table->integer('shots_overtime_1')->nullable();
			$table->integer('faceoff_total_wins')->nullable();
			$table->integer('goalie_shots_against')->nullable();
			$table->integer('goalie_goals_against')->nullable();
			$table->integer('goalie_time_on_ice_power_play_secs')->nullable();
			$table->string('goalie_goals_against_average', 10)->nullable();
			$table->integer('goalie_time_on_ice_secs')->nullable();
			$table->timestamps();
			$table->integer('goalie_time_on_ice_short_handed_secs')->nullable();
			$table->integer('goalie_saves')->nullable();
			$table->integer('shots_period_2')->nullable();
			$table->integer('goalie_assists')->nullable();
			$table->integer('goalie_goals_scored')->nullable();
			$table->integer('goalie_hits')->nullable();
			$table->integer('goalie_penalty_minutes')->nullable();
			$table->integer('goalie_points_power_play')->nullable();
			$table->integer('goals_period_1')->nullable();
			$table->integer('goalie_points_short_handed')->nullable();
			$table->integer('goals_empty_net')->nullable();
			$table->integer('goals_power_play')->nullable();
			$table->integer('goals_shootout')->nullable();
			$table->integer('goals_period_3')->nullable();
			$table->integer('player_points_power_play')->nullable();
			$table->integer('player_points_short_handed')->nullable();
			$table->integer('shootouts_attempted')->nullable();
			$table->integer('shootouts_scored')->nullable();
			$table->integer('shutouts')->nullable();
			$table->integer('games_played')->nullable();
			$table->integer('games_won')->nullable();
			$table->integer('games_lost_overtime')->nullable();
			$table->integer('games_lost_shootout')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('competition_team_stats');
	}

}
