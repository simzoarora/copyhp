<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolTradesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_trades', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('parent_pool_team_id')->index('parent_pool_team_id');
			$table->integer('trading_pool_team_id')->index('trading_pool_team_id');
			$table->boolean('status')->default(0)->comment('0 - No Action, 1 - Accepted, 2 - Rejected, 3 - Auto Rejected, 4 - Cancelled By Parent');
			$table->boolean('is_processed')->default(0)->comment('used for cron');
			$table->dateTime('status_change_at')->nullable();
			$table->dateTime('deadline')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_trades');
	}

}
