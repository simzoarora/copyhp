<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlayerSeasonTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('player_season', function(Blueprint $table)
		{
			$table->foreign('player_id', 'player_season_ibfk_1')->references('id')->on('players')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('season_id', 'player_season_ibfk_2')->references('id')->on('seasons')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('team_id', 'player_season_ibfk_3')->references('id')->on('teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_position_id', 'player_season_ibfk_5')->references('id')->on('player_positions')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('player_season', function(Blueprint $table)
		{
			$table->dropForeign('player_season_ibfk_1');
			$table->dropForeign('player_season_ibfk_2');
			$table->dropForeign('player_season_ibfk_3');
			$table->dropForeign('player_season_ibfk_5');
		});
	}

}
