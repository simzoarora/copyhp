<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayerStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('player_stats', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('season_id')->index('season_id');
			$table->integer('team_id')->index('team_id');
			$table->integer('player_id')->index('player_id');
			$table->string('stat_key', 150)->index('stat_key');
			$table->string('season-phase-from', 150);
			$table->string('season-phase-to', 150);
			$table->integer('assists')->nullable();
			$table->integer('assists_power_play')->nullable();
			$table->integer('assists_short_handed')->nullable();
			$table->integer('blocked_shots')->nullable();
			$table->integer('faceoffs_lost')->nullable();
			$table->integer('faceoffs_won')->nullable();
			$table->integer('game_winning_goals')->nullable();
			$table->integer('games_played')->nullable();
			$table->integer('goals')->nullable();
			$table->integer('goals_period_1')->nullable();
			$table->integer('plus_minus')->nullable();
			$table->integer('goals_overtime')->nullable();
			$table->integer('goals_period_2')->nullable();
			$table->integer('penalty_minutes')->nullable();
			$table->integer('goals_short_handed')->nullable();
			$table->integer('hits')->nullable();
			$table->integer('goals_power_play')->nullable();
			$table->integer('goals_period_3')->nullable();
			$table->integer('shootouts_scored')->nullable();
			$table->integer('points_short_handed')->nullable();
			$table->integer('shootouts_attempted')->nullable();
			$table->integer('shifts')->nullable();
			$table->integer('games_started')->nullable();
			$table->integer('shots')->nullable();
			$table->integer('time_on_ice_even_strength_secs')->nullable();
			$table->integer('time_on_ice_power_play_secs')->nullable();
			$table->integer('time_on_ice_secs')->nullable();
			$table->integer('time_on_ice_short_handed_secs')->nullable();
			$table->integer('points_power_play')->nullable();
			$table->timestamps();
			$table->integer('goals_against_average')->nullable();
			$table->integer('goals_overtime_against')->nullable();
			$table->integer('goals_period_2_against')->nullable();
			$table->integer('goals_period_1_against')->nullable();
			$table->integer('shootout_losses')->nullable();
			$table->integer('overtime_losses')->nullable();
			$table->integer('shots_against')->nullable();
			$table->integer('losses')->nullable();
			$table->integer('goals_period_3_against')->nullable();
			$table->integer('shots_period_1_against')->nullable();
			$table->integer('shots_period_2_against')->nullable();
			$table->integer('shots_period_3_against')->nullable();
			$table->integer('shutouts')->nullable();
			$table->integer('ties')->nullable();
			$table->integer('wins')->nullable();
			$table->integer('goals_against')->nullable();
			$table->integer('shots_overtime_against')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_stats');
	}

}
