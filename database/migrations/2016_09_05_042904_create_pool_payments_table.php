<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_id')->index('pool_id');
			$table->string('order_id');
			$table->dateTime('order_date');
			$table->integer('event_id');
			$table->string('event_ref', 50);
			$table->dateTime('event_time');
			$table->string('event_status_code', 5);
			$table->float('sale_amount_usd', 6);
			$table->float('sale_amount', 6);
			$table->string('sale_method', 10);
			$table->string('sale_currency', 5);
			$table->string('customer_name', 150);
			$table->string('customer_email', 150);
			$table->string('customer_ip', 20);
			$table->integer('pool_payment_item_id')->index('pool_payment_item_id');
			$table->dateTime('next_expected_date')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_payments');
	}

}
