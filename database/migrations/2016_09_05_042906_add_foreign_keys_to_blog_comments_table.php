<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBlogCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('blog_comments', function(Blueprint $table)
		{
			$table->foreign('user_id', 'blog_comments_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('blog_id', 'blog_comments_ibfk_2')->references('id')->on('blogs')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('blog_comments', function(Blueprint $table)
		{
			$table->dropForeign('blog_comments_ibfk_1');
			$table->dropForeign('blog_comments_ibfk_2');
		});
	}

}
