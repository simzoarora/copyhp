<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCalculatedPoolPointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('calculated_pool_points', function(Blueprint $table)
		{
			$table->foreign('pool_id', 'calculated_pool_points_ibfk_1')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_id', 'calculated_pool_points_ibfk_2')->references('id')->on('players')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('season_id', 'calculated_pool_points_ibfk_3')->references('id')->on('seasons')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('competition_id', 'calculated_pool_points_ibfk_4')->references('id')->on('competitions')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('calculated_pool_points', function(Blueprint $table)
		{
			$table->dropForeign('calculated_pool_points_ibfk_1');
			$table->dropForeign('calculated_pool_points_ibfk_2');
			$table->dropForeign('calculated_pool_points_ibfk_3');
			$table->dropForeign('calculated_pool_points_ibfk_4');
		});
	}

}
