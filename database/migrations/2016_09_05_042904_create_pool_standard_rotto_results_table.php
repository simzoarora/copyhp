<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolStandardRottoResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_standard_rotto_results', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_team_id')->index('pool_team_id');
			$table->integer('pool_scoring_field_id')->index('pool_scoring_field_id');
			$table->float('value', 6, 3);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_standard_rotto_results');
	}

}
