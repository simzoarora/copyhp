<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateScorefeedupdatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scorefeedupdates', function(Blueprint $table)
		{
			$table->increments('id');
			$table->dateTime('livescores');
			$table->dateTime('schedules');
			$table->dateTime('results');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('scorefeedupdates');
	}

}
