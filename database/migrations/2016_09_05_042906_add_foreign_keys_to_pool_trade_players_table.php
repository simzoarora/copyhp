<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolTradePlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_trade_players', function(Blueprint $table)
		{
			$table->foreign('pool_trade_id', 'pool_trade_players_ibfk_1')->references('id')->on('pool_trades')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_team_player_id', 'pool_trade_players_ibfk_2')->references('id')->on('pool_team_players')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_trade_players', function(Blueprint $table)
		{
			$table->dropForeign('pool_trade_players_ibfk_1');
			$table->dropForeign('pool_trade_players_ibfk_2');
		});
	}

}
