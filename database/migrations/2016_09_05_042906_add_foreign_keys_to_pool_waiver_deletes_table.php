<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolWaiverDeletesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_waiver_deletes', function(Blueprint $table)
		{
			$table->foreign('pool_waiver_id', 'pool_waiver_deletes_ibfk_1')->references('id')->on('pool_waivers')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_team_player_id', 'pool_waiver_deletes_ibfk_3')->references('id')->on('pool_team_players')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_waiver_deletes', function(Blueprint $table)
		{
			$table->dropForeign('pool_waiver_deletes_ibfk_1');
			$table->dropForeign('pool_waiver_deletes_ibfk_3');
		});
	}

}
