<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGoalsShortHandedToTeamStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('competition_team_stats', function (Blueprint $table) {
            $table->integer('goals_short_handed')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('competition_team_stats', function (Blueprint $table) {
            $table->dropColumn('goals_short_handed');
        });
    }
}
