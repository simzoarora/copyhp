<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeGoalieIdNullableInCompetitionShootoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('competition_shootouts', function (Blueprint $table) {
            $table->integer('goalie_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('competition_shootouts', function (Blueprint $table) {
            $table->integer('goalie_id')->nullable(false)->change();
        });
    }
}
