<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolInvitationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_invitations', function(Blueprint $table)
		{
			$table->foreign('pool_id', 'pool_invitations_ibfk_1')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_invitations', function(Blueprint $table)
		{
			$table->dropForeign('pool_invitations_ibfk_1');
		});
	}

}
