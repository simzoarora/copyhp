<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolScoreSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_score_settings', function(Blueprint $table)
		{
			$table->foreign('pool_id', 'pool_score_settings_ibfk_1')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_scoring_field_id', 'pool_score_settings_ibfk_2')->references('id')->on('pool_scoring_fields')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_score_settings', function(Blueprint $table)
		{
			$table->dropForeign('pool_score_settings_ibfk_1');
			$table->dropForeign('pool_score_settings_ibfk_2');
		});
	}

}
