<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_payments', function(Blueprint $table)
		{
			$table->foreign('pool_payment_item_id', 'pool_payments_ibfk_2')->references('id')->on('pool_payment_items')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_id', 'pool_payments_ibfk_1')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_payments', function(Blueprint $table)
		{
			$table->dropForeign('pool_payments_ibfk_2');
			$table->dropForeign('pool_payments_ibfk_1');
		});
	}

}
