<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSeasonCompetitionTypeWeeksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('season_competition_type_weeks', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('season_competition_type_id')->index('season_competition_type_id');
			$table->string('name', 10);
			$table->date('start_date');
			$table->date('end_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('season_competition_type_weeks');
	}

}
