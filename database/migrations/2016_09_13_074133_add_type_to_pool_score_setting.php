<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToPoolScoreSetting extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('pool_score_settings', function (Blueprint $table) {
            $table->boolean('type')->comment('1 - Skater, 2 - Goalie')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('pool_score_settings', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }

}
