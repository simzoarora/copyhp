<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolTradesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_trades', function(Blueprint $table)
		{
			$table->foreign('parent_pool_team_id', 'pool_trades_ibfk_1')->references('id')->on('pool_teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('trading_pool_team_id', 'pool_trades_ibfk_2')->references('id')->on('pool_teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_trades', function(Blueprint $table)
		{
			$table->dropForeign('pool_trades_ibfk_1');
			$table->dropForeign('pool_trades_ibfk_2');
		});
	}

}
