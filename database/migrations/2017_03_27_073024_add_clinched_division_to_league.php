<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClinchedDivisionToLeague extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('league_standings', function (Blueprint $table) {
            $table->integer('clinched_division')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('league_standings', function (Blueprint $table) {
            $table->dropColumn('clinched_division');
        });
    }
}
