<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLeagueStandingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('league_standings', function(Blueprint $table)
		{
			$table->foreign('season_id', 'league_standings_ibfk_1')->references('id')->on('seasons')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('team_id', 'league_standings_ibfk_2')->references('id')->on('teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('league_standings', function(Blueprint $table)
		{
			$table->dropForeign('league_standings_ibfk_1');
			$table->dropForeign('league_standings_ibfk_2');
		});
	}

}
