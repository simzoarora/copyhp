<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolRoundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_rounds', function(Blueprint $table)
		{
			$table->foreign('player_position_id', 'pool_rounds_ibfk_2')->references('id')->on('player_positions')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_id', 'pool_rounds_ibfk_1')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_rounds', function(Blueprint $table)
		{
			$table->dropForeign('pool_rounds_ibfk_2');
			$table->dropForeign('pool_rounds_ibfk_1');
		});
	}

}
