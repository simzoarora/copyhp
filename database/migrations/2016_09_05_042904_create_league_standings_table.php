<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeagueStandingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('league_standings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('season_id')->index('season_id');
			$table->integer('team_id')->index('team_id');
			$table->integer('games_won');
			$table->integer('games_tied')->nullable();
			$table->integer('games_lost');
			$table->integer('games_lost_overtime');
			$table->integer('games_lost_shootout');
			$table->integer('goals_for');
			$table->integer('goals_against');
			$table->integer('games_won_shootout');
			$table->integer('clinched_division_top_3')->nullable();
			$table->integer('clinched_wildcard')->nullable();
			$table->integer('loss_streak')->nullable();
			$table->integer('win_streak')->nullable();
			$table->integer('clinched_best_league_record')->nullable();
			$table->integer('clinched_conference')->nullable();
			$table->integer('calculated_points');
			$table->integer('rank')->comment('this is after using tie breaker logic');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('league_standings');
	}

}
