<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('players', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('api_id');
			$table->string('first_name', 150);
			$table->string('last_name', 150);
			$table->integer('height')->nullable();
			$table->integer('weight')->nullable();
			$table->date('dob')->nullable();
			$table->integer('city_id')->nullable()->index('city_id');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('players');
	}

}
