<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolTeamLineupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_team_lineups', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_team_id')->nullable()->index('pool_team_id');
			$table->date('start_time');
			$table->date('end_time');
			$table->date('prev_end_time')->nullable()->comment('this is used to save last end time of this entry. It is mainly used when a player claims a waiver player and deletes a player.');
			$table->integer('pool_team_player_id')->index('pool_team_player_id');
			$table->integer('player_position_id')->unsigned()->nullable()->index('player_position_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_team_lineups');
	}

}
