<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTwitterFeedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('twitter_feeds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('tweet_id');
			$table->string('image', 150);
			$table->string('name', 100);
			$table->string('tweet', 160);
			$table->dateTime('tweet_created');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('twitter_feeds');
	}

}
