<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPoolTradeSettingBasedStatusToPoolTradesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pool_trades', function (Blueprint $table) {
            $table->tinyInteger('pool_trade_setting_based_status')->default(0)->after('is_processed')->comment('0 - Waiting for action/No action required, 1 - Trade Accepted(Ready for voting or getting decided by commisoner depnding on pool setting), 2- Trade rejected');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pool_trades', function (Blueprint $table) {
            $table->dropColumn('pool_trade_setting_based_status');
        });
    }

}
