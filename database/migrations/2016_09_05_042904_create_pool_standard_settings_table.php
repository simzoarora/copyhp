<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolStandardSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_standard_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('max_teams')->default(12);
			$table->boolean('pool_format')->default(1)->comment('1 - standard standard, 2 - Standard Rotisserie');
			$table->smallInteger('max_acquisition_team')->default(-1)->comment('-1 -> No max, 0 - no acquisition allowed');
			$table->boolean('max_acquisition_week')->default(6)->comment('-1 -> No max, 0 - no acquisition allowed');
			$table->boolean('max_trades_season')->default(-1)->comment('-1 -> no max, 0 - no trades allowed');
			$table->date('trade_end_date');
			$table->boolean('trading_draft_pick')->comment('0 - no, 1 - yes');
			$table->boolean('trade_reject_time')->default(2)->comment('-1 -> no protests allowed, 1, 2,3,4,5,6,7');
			$table->boolean('waiver_mode')->default(0)->comment('0 - None, 1 - Standard');
			$table->boolean('waiver_time');
			$table->boolean('league_start_week')->default(1);
			$table->boolean('min_goalie_appearance');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_standard_settings');
	}

}
