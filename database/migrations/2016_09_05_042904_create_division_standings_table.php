<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDivisionStandingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('division_standings', function(Blueprint $table)
		{
			$table->integer('id');
			$table->integer('season_id')->index('season_id');
			$table->integer('team_id')->index('team_id');
			$table->integer('division_id')->index('division_id');
			$table->integer('division_ranking');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('division_standings');
	}

}
