<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlayerStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('player_stats', function(Blueprint $table)
		{
			$table->foreign('season_id', 'player_stats_ibfk_1')->references('id')->on('seasons')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('team_id', 'player_stats_ibfk_2')->references('id')->on('teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_id', 'player_stats_ibfk_3')->references('id')->on('players')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('player_stats', function(Blueprint $table)
		{
			$table->dropForeign('player_stats_ibfk_1');
			$table->dropForeign('player_stats_ibfk_2');
			$table->dropForeign('player_stats_ibfk_3');
		});
	}

}
