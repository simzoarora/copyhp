<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSeasonPoolPointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('season_pool_points', function(Blueprint $table)
		{
			$table->foreign('season_id', 'season_pool_points_ibfk_1')->references('id')->on('seasons')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_id', 'season_pool_points_ibfk_2')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_id', 'season_pool_points_ibfk_3')->references('id')->on('players')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('season_pool_points', function(Blueprint $table)
		{
			$table->dropForeign('season_pool_points_ibfk_1');
			$table->dropForeign('season_pool_points_ibfk_2');
			$table->dropForeign('season_pool_points_ibfk_3');
		});
	}

}
