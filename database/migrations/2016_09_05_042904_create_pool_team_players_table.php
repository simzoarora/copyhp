<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolTeamPlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_team_players', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_team_id')->nullable()->index('pool_team_id');
			$table->integer('pool_invitation_id')->nullable()->index('pool_invitation_id');
			$table->integer('pool_round_id')->nullable()->index('pool_round_id');
			$table->integer('player_season_id')->index('player_season_id');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_team_players');
	}

}
