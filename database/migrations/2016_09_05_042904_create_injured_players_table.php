<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInjuredPlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('injured_players', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('api_id');
			$table->integer('player_season_id')->index('player_season_id');
			$table->string('injury_location', 50);
			$table->date('injury_start_date');
			$table->string('injury_status', 20);
			$table->string('injury_display_status', 100);
			$table->string('injury_note');
			$table->dateTime('last_updated');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('injured_players');
	}

}
