<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolTeamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_teams', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_id')->index('pool_id');
			$table->integer('user_id')->unsigned()->nullable()->index('user_id');
			$table->string('email')->nullable();
			$table->string('name', 60);
			$table->string('logo')->default('team_name_default.jpg');
			$table->boolean('sort_order')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_teams');
	}

}
