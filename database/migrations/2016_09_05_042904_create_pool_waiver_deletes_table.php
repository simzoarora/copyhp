<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolWaiverDeletesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_waiver_deletes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_waiver_id')->index('pool_waiver_id');
			$table->integer('pool_team_player_id')->index('pool_team_player_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_waiver_deletes');
	}

}
