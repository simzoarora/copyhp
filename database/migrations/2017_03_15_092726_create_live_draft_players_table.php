<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLiveDraftPlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('live_draft_players', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('pool_id')->index('pool_id');
			$table->integer('pool_team_id')->index('pool_team_id');
			$table->integer('pool_round_id')->index('pool_round_id');
			$table->dateTime('start_time');
			$table->dateTime('end_time');
			$table->dateTime('drafted_at')->nullable();
			$table->integer('player_season_id')->nullable()->index('player_season_id');
			$table->boolean('type')->default(0)->comment('0 - manual, 1 - automatic without presence, 2 - automatic with presence');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('live_draft_players');
	}

}
