<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolScoringFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_scoring_fields', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('stat', 10);
			$table->string('title', 100);
			$table->boolean('sort_order')->default(0);
			$table->boolean('type')->default(1)->comment('1 - player, 2 - goalie, 3 - both');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_scoring_fields');
	}

}
