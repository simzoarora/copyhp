<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCompetitionPenaltiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('competition_penalties', function(Blueprint $table)
		{
			$table->foreign('competition_id', 'competition_penalties_ibfk_1')->references('id')->on('competitions')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_season_id', 'competition_penalties_ibfk_2')->references('id')->on('player_season')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('competition_penalties', function(Blueprint $table)
		{
			$table->dropForeign('competition_penalties_ibfk_1');
			$table->dropForeign('competition_penalties_ibfk_2');
		});
	}

}
