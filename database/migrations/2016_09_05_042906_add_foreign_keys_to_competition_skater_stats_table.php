<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCompetitionSkaterStatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('competition_skater_stats', function(Blueprint $table)
		{
			$table->foreign('competition_id', 'competition_skater_stats_ibfk_2')->references('id')->on('competitions')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_season_id', 'competition_skater_stats_ibfk_3')->references('id')->on('player_season')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('competition_skater_stats', function(Blueprint $table)
		{
			$table->dropForeign('competition_skater_stats_ibfk_2');
			$table->dropForeign('competition_skater_stats_ibfk_3');
		});
	}

}
