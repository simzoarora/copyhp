<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPoolIdToNotifications extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('notifications', function (Blueprint $table) {
            $table->integer('pool_id')->index('pool_id')->nullable();
            $table->foreign('pool_id', 'notification_ibfk_1')->references('id')->on('pools')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('notifications', function (Blueprint $table) {
            $table->dropForeign('notification_ibfk_1');
            $table->dropColumn(['pool_id']);
        });
    }

}
