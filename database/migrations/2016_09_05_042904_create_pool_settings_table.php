<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_id')->index('pool_id');
			$table->integer('poolsetting_id');
			$table->string('poolsetting_type');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_settings');
	}

}
