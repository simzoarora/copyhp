<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolH2hScoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_h2h_scores', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_score_setting_id')->index('pool_score_setting_id');
			$table->integer('pool_matchup_id')->nullable()->index('pool_matchup_id');
			$table->integer('pool_team_id')->index('pool_team_id');
			$table->integer('competition_id')->unsigned()->index('competition_id');
			$table->integer('pool_team_player_id')->index('pool_team_player_id');
			$table->float('original_stat', 6, 3);
			$table->float('value', 6, 3);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_h2h_scores');
	}

}
