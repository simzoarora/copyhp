<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLiveDraftPlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('live_draft_players', function(Blueprint $table)
		{
			$table->foreign('pool_id', 'live_draft_players_ibfk_1')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_team_id', 'live_draft_players_ibfk_2')->references('id')->on('pool_teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_round_id', 'live_draft_players_ibfk_3')->references('id')->on('pool_rounds')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_season_id', 'live_draft_players_ibfk_4')->references('id')->on('player_season')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('live_draft_players', function(Blueprint $table)
		{
			$table->dropForeign('live_draft_players_ibfk_1');
			$table->dropForeign('live_draft_players_ibfk_2');
			$table->dropForeign('live_draft_players_ibfk_3');
			$table->dropForeign('live_draft_players_ibfk_4');
		});
	}

}
