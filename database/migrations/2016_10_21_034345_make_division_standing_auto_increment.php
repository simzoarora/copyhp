<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeDivisionStandingAutoIncrement extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('division_standings', function (Blueprint $table) {
            $table->dropColumn('id');
        });
        Schema::table('division_standings', function (Blueprint $table) {
            $table->integer('id', true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('division_standings', function (Blueprint $table) {
            //
        });
    }
}