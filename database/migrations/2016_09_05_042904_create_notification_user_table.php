<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notification_user', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('notification_id')->index('notification_id');
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->dateTime('read_at')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notification_user');
	}

}
