<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolScoreSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_score_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_id')->index('pool_id');
			$table->integer('pool_scoring_field_id')->index('pool_scoring_field_id');
			$table->float('value', 6, 3)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_score_settings');
	}

}
