<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCompetitionShootoutsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('competition_shootouts', function(Blueprint $table)
		{
			$table->foreign('skater_id', 'competition_shootouts_ibfk_1')->references('id')->on('player_season')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('goalie_id', 'competition_shootouts_ibfk_2')->references('id')->on('player_season')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('competition_id', 'competition_shootouts_ibfk_3')->references('id')->on('competitions')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('competition_shootouts', function(Blueprint $table)
		{
			$table->dropForeign('competition_shootouts_ibfk_1');
			$table->dropForeign('competition_shootouts_ibfk_2');
			$table->dropForeign('competition_shootouts_ibfk_3');
		});
	}

}
