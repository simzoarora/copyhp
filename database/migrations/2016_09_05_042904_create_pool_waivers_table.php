<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolWaiversTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_waivers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_id')->index('pool_id');
			$table->integer('player_season_id')->index('player_season_id');
			$table->dateTime('waiver_till');
			$table->boolean('is_completed')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_waivers');
	}

}
