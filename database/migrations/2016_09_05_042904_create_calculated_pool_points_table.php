<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalculatedPoolPointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calculated_pool_points', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_id')->index('pool_id');
			$table->integer('player_id')->index('player_id');
			$table->integer('season_id')->index('season_id');
			$table->integer('competition_id')->unsigned()->nullable()->index('competition_id');
			$table->float('total_points', 10, 3);
			$table->date('points_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calculated_pool_points');
	}

}
