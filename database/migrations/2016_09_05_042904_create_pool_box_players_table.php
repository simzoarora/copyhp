<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolBoxPlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_box_players', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_box_id')->index('pool_box_id');
			$table->integer('player_season_id')->index('player_season_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_box_players');
	}

}
