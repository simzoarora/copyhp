<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCompetitionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('competitions', function(Blueprint $table)
		{
			$table->foreign('home_team_id', 'competitions_ibfk_1')->references('id')->on('teams')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('away_team_id', 'competitions_ibfk_2')->references('id')->on('teams')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('season_id', 'competitions_ibfk_3')->references('id')->on('seasons')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('venue_id', 'competitions_ibfk_4')->references('id')->on('venues')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('competitions', function(Blueprint $table)
		{
			$table->dropForeign('competitions_ibfk_1');
			$table->dropForeign('competitions_ibfk_2');
			$table->dropForeign('competitions_ibfk_3');
			$table->dropForeign('competitions_ibfk_4');
		});
	}

}
