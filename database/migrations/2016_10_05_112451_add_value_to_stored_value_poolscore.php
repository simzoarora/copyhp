<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\StoredValue;

class AddValueToStoredValuePoolscore extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        StoredValue::firstOrCreate([
            'key' => 'pool_score_last_pool',
            'value' => '0',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        StoredValue::where('key', 'pool_score_last_pool')->delete();
    }
}