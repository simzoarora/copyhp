<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessageBoardRepliesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('message_board_replies', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('message_board_id')->index('message_board_id');
			$table->integer('parent_id')->nullable();
			$table->integer('_lft')->nullable();
			$table->integer('_rgt')->nullable();
			$table->integer('user_id')->unsigned()->index('user_id');
			$table->text('reply', 65535);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('message_board_replies');
	}

}
