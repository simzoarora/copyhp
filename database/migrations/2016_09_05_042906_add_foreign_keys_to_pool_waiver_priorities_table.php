<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolWaiverPrioritiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_waiver_priorities', function(Blueprint $table)
		{
			$table->foreign('pool_id', 'pool_waiver_priorities_ibfk_1')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_team_id', 'pool_waiver_priorities_ibfk_2')->references('id')->on('pool_teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_waiver_priorities', function(Blueprint $table)
		{
			$table->dropForeign('pool_waiver_priorities_ibfk_1');
			$table->dropForeign('pool_waiver_priorities_ibfk_2');
		});
	}

}
