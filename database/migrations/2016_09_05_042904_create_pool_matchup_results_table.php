<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolMatchupResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_matchup_results', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_matchup_id')->index('pool_matchup_id');
			$table->integer('pool_team_id')->index('pool_team_id');
			$table->boolean('win');
			$table->boolean('loss');
			$table->boolean('tie');
			$table->boolean('score');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_matchup_results');
	}

}
