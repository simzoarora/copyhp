<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolSeasonRanksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_season_ranks', function(Blueprint $table)
		{
			$table->foreign('pool_id', 'pool_season_ranks_ibfk_1')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_id', 'pool_season_ranks_ibfk_2')->references('id')->on('players')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('season_id', 'pool_season_ranks_ibfk_3')->references('id')->on('seasons')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_season_ranks', function(Blueprint $table)
		{
			$table->dropForeign('pool_season_ranks_ibfk_1');
			$table->dropForeign('pool_season_ranks_ibfk_2');
			$table->dropForeign('pool_season_ranks_ibfk_3');
		});
	}

}
