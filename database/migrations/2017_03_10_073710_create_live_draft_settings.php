<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiveDraftSettings extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('live_draft_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pool_id');
            $table->foreign('pool_id')->references('id')->on('pools')->onDelete('CASCADE');
            $table->date('date');
            $table->time('time');
            $table->string('time_zone');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('live_draft_settings');
    }

}
