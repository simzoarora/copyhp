<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolRostersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_rosters', function(Blueprint $table)
		{
			$table->foreign('pool_id', 'pool_rosters_ibfk_1')->references('id')->on('pools')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_position_id', 'pool_rosters_ibfk_2')->references('id')->on('player_positions')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_rosters', function(Blueprint $table)
		{
			$table->dropForeign('pool_rosters_ibfk_1');
			$table->dropForeign('pool_rosters_ibfk_2');
		});
	}

}
