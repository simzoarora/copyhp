<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExtraLeagueStandingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('extra_league_standings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('season_id')->index('season_id');
			$table->integer('team_id')->index('team_id');
			$table->integer('games_won')->nullable()->default(0);
			$table->integer('games_lost')->nullable()->default(0);
			$table->integer('games_lost_overtime')->nullable()->default(0);
			$table->integer('games_lost_shootout')->nullable()->default(0);
			$table->integer('games_tied')->nullable()->default(0);
			$table->integer('type')->nullable()->comment('1 - last 10, 2 - home, 3 - away');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('extra_league_standings');
	}

}
