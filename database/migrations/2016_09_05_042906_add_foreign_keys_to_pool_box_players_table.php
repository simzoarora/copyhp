<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolBoxPlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_box_players', function(Blueprint $table)
		{
			$table->foreign('pool_box_id', 'pool_box_players_ibfk_1')->references('id')->on('pool_boxes')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_season_id', 'pool_box_players_ibfk_2')->references('id')->on('player_season')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_box_players', function(Blueprint $table)
		{
			$table->dropForeign('pool_box_players_ibfk_1');
			$table->dropForeign('pool_box_players_ibfk_2');
		});
	}

}
