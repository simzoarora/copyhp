<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolMatchupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_matchups', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('pool_id')->index('pool_id');
			$table->integer('team1')->index('team1');
			$table->integer('team2')->index('team2');
			$table->integer('team1_score')->nullable();
			$table->integer('team2_score')->nullable();
			$table->integer('season_competition_type_week_id')->default(1)->index('season_competition_type_week_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_matchups');
	}

}
