<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePoolStandardScoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pool_standard_scores', function(Blueprint $table)
		{
			$table->integer('id', true)->comment('this table is used for box and standard both');
			$table->integer('pool_team_id')->index('pool_team_id');
			$table->integer('pool_score_setting_id')->index('pool_score_setting_id');
			$table->integer('pool_team_player_id')->index('pool_team_player_id');
			$table->integer('competition_id')->unsigned()->index('competition_id');
			$table->float('original_stat', 6, 3)->unsigned();
			$table->float('value', 6, 3);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pool_standard_scores');
	}

}
