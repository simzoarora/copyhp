<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDivisionStandingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('division_standings', function(Blueprint $table)
		{
			$table->foreign('season_id', 'division_standings_ibfk_1')->references('id')->on('seasons')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('team_id', 'division_standings_ibfk_2')->references('id')->on('teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('division_id', 'division_standings_ibfk_3')->references('id')->on('divisions')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('division_standings', function(Blueprint $table)
		{
			$table->dropForeign('division_standings_ibfk_1');
			$table->dropForeign('division_standings_ibfk_2');
			$table->dropForeign('division_standings_ibfk_3');
		});
	}

}
