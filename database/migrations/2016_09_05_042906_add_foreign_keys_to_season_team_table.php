<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSeasonTeamTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('season_team', function(Blueprint $table)
		{
			$table->foreign('season_id', 'season_team_ibfk_1')->references('id')->on('seasons')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('team_id', 'season_team_ibfk_2')->references('id')->on('teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('season_team', function(Blueprint $table)
		{
			$table->dropForeign('season_team_ibfk_1');
			$table->dropForeign('season_team_ibfk_2');
		});
	}

}
