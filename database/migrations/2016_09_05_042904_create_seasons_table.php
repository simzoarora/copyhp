<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSeasonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('seasons', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('api_id');
			$table->string('name', 30);
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->boolean('current')->default(0);
			$table->boolean('sort_order')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('seasons');
	}

}
