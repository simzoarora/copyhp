<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeForStatFields extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_stats', function ($table) {
            $table->float('goals_against_average')->change();
        });
        Schema::table('competition_goalie_stats', function ($table) {
            $table->float('goals_against_average')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_stats', function ($table) {
            $table->integer('goals_against_average')->change();
        });
        Schema::table('competition_goalie_stats', function ($table) {
            $table->integer('goals_against_average')->change();
        });
    }
}