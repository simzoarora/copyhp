<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToPlayerPreRankingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_pre_rankings', function(Blueprint $table)
		{
			$table->foreign('player_id', 'player_pre_rankings_ibfk_1')->references('id')->on('players')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('season_id', 'player_pre_rankings_ibfk_2')->references('id')->on('seasons')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_pre_rankings', function(Blueprint $table)
		{
			$table->dropForeign('player_pre_rankings_ibfk_1');
			$table->dropForeign('player_pre_rankings_ibfk_2');
		});
    }
}
