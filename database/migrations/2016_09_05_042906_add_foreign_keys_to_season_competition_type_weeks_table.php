<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSeasonCompetitionTypeWeeksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('season_competition_type_weeks', function(Blueprint $table)
		{
			$table->foreign('season_competition_type_id', 'season_competition_type_weeks_ibfk_1')->references('id')->on('season_competition_types')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('season_competition_type_weeks', function(Blueprint $table)
		{
			$table->dropForeign('season_competition_type_weeks_ibfk_1');
		});
	}

}
