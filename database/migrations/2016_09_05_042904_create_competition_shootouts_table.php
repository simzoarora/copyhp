<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompetitionShootoutsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('competition_shootouts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('competition_id')->unsigned()->index('competition_id');
			$table->integer('api_id')->unsigned();
			$table->smallInteger('order')->unsigned();
			$table->boolean('scored')->default(0);
			$table->integer('skater_id')->index('skater_id');
			$table->integer('goalie_id')->index('goalie_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('competition_shootouts');
	}

}
