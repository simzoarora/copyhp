<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCompetitionGoalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('competition_goals', function(Blueprint $table)
		{
			$table->foreign('competition_id', 'competition_goals_ibfk_1')->references('id')->on('competitions')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('scorer_id', 'competition_goals_ibfk_2')->references('id')->on('player_season')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('first_assisted_by', 'competition_goals_ibfk_3')->references('id')->on('player_season')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('second_assisted_by', 'competition_goals_ibfk_4')->references('id')->on('player_season')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('competition_goals', function(Blueprint $table)
		{
			$table->dropForeign('competition_goals_ibfk_1');
			$table->dropForeign('competition_goals_ibfk_2');
			$table->dropForeign('competition_goals_ibfk_3');
			$table->dropForeign('competition_goals_ibfk_4');
		});
	}

}
