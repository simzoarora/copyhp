<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNotificationUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notification_user', function(Blueprint $table)
		{
			$table->foreign('notification_id', 'notification_user_ibfk_1')->references('id')->on('notifications')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_id', 'notification_user_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notification_user', function(Blueprint $table)
		{
			$table->dropForeign('notification_user_ibfk_1');
			$table->dropForeign('notification_user_ibfk_2');
		});
	}

}
