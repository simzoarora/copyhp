<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPoolIdColumnToMessageBoardStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('message_board_statuses', function (Blueprint $table) {
             $table->integer('pool_id');
            $table->foreign('pool_id')->references('id')->on('pools')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('message_board_statuses', function (Blueprint $table) {
            $table->dropForeign('message_board_statuses_pool_id_foreign');
            $table->dropColumn('pool_id');
        });
    }
}
