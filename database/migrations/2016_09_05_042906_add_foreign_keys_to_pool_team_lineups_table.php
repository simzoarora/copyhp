<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolTeamLineupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_team_lineups', function(Blueprint $table)
		{
			$table->foreign('pool_team_id', 'pool_team_lineups_ibfk_1')->references('id')->on('pool_teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_team_player_id', 'pool_team_lineups_ibfk_2')->references('id')->on('pool_team_players')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_position_id', 'pool_team_lineups_ibfk_3')->references('id')->on('player_positions')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_team_lineups', function(Blueprint $table)
		{
			$table->dropForeign('pool_team_lineups_ibfk_1');
			$table->dropForeign('pool_team_lineups_ibfk_2');
			$table->dropForeign('pool_team_lineups_ibfk_3');
		});
	}

}
