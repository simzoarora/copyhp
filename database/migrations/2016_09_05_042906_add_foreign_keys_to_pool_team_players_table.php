<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPoolTeamPlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pool_team_players', function(Blueprint $table)
		{
			$table->foreign('pool_team_id', 'pool_team_players_ibfk_1')->references('id')->on('pool_teams')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('player_season_id', 'pool_team_players_ibfk_2')->references('id')->on('player_season')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_invitation_id', 'pool_team_players_ibfk_3')->references('id')->on('pool_invitations')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('pool_round_id', 'pool_team_players_ibfk_4')->references('id')->on('pool_rounds')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pool_team_players', function(Blueprint $table)
		{
			$table->dropForeign('pool_team_players_ibfk_1');
			$table->dropForeign('pool_team_players_ibfk_2');
			$table->dropForeign('pool_team_players_ibfk_3');
			$table->dropForeign('pool_team_players_ibfk_4');
		});
	}

}
