<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQueueColumnToLiveDraftSettings extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('live_draft_settings', function (Blueprint $table) {
            $table->boolean('in_queue')->default(0)->comment('1 - If pool is added in queue, 0 - otherwise');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('live_draft_settings', function (Blueprint $table) {
            $table->dropColumn('in_queue');
        });
    }
}