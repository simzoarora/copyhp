<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDivisionConferenceToSeasonTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('season_team', function (Blueprint $table) {
            $table->integer('conference_id')->nullable();
            $table->integer('division_id')->nullable();
        });
        Schema::table('season_team', function (Blueprint $table) {
            $table->foreign('conference_id', 'conference_id_ibfk_1')->references('id')->on('conferences')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('division_id', 'division_id_ibfk_1')->references('id')->on('divisions')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('season_team', function (Blueprint $table) {
            $table->dropColumn('conference_id');
            $table->dropColumn('division_id');
        });
    }
}
